# Release 2.9.0

- luca location updates
 
This update includes more Luca location features that operators can activate.  

# Release 2.8.1

- Bug fixes 

This update includes, among others, bug fixes concerning the check-in at luca locations. 

# Release 2.8.0

- Check-in in a new design

With this update the check-in gets a new design! Scan the QR code from a luca location to see the new design and use the features provided by the location!

# Release 2.7.1

- luca promotion 

This update includes the luca promotion. Check out with luca and benefit from the luca promotion during the campaign period!

# Release 2.7.0

- New features by luca locations

You can now enjoy the new features provided by luca locations natively in your luca app! This includes easily reusing the data you entered as well as well as having an overview of your transactions. 

# Release 2.6.3

- Bug fixes 

This update includes, among others, bug fixes concerning the check-in at luca locations. 

# Release 2.6.2

- Bug fixes 

This update includes, among others, bug fixes concerning the new luca location functionalities. 

# Release 2.6.1

- UX improvements 
- Bug fixes 

This update includes, among others, bug fixes concerning the import of documents. Furthermore, we have made some UX improvements. 

# Release 2.6.0

- New Check-ins

Currently, contact tracing via luca is on hold, but the luca check-in will stay the same! Your contact information will no longer be tracked, but luca will still be available to you, you can check-in as before and use the features the luca location provides you in your luca app.

# Release 2.5.0

- UX improvements 
- Bug fixes

This update includes, among others, bug fixes concerning the import of documents. Furthermore, we have made some UX improvements.

# Release 2.4.3

- Enable certificate history

This update allows you to keep expired documents in the app. They will be displayed greyed out.

# Release 2.4.2

- UX improvements 
- Bug fixes 

This update includes, among others, bug fixes concerning the import of documents. Furthermore, we have made some UX improvements.

# Release 2.4.1

- Document redesign

This updates includes a redesign of the documents within the app, making it easier to review the most important information. It also includes document bug fixes.

# Release 2.4.0

- Notifications tab
- Bug fixes

In this update we present a new tab: The notifications tab! In the notifications tab you will see data requests and notifications from health departments as well as other news.

# Release 2.3.2

- bug fix

This update fixes an error related to the display of warning messages.

# Release 2.3.1

- bug fixes

This update includes bug fixes concerning the account deletion.

# Release 2.3.0

- bug fixes

This update includes bug fixes concerning the import and representation of documents.

# Release 2.2.0

- 2G/3G status in QR code 
- Verification of EU Digital COVID certificates
- Additional information provided by luca locations

This update makes it possible to share your 2G/3G status in your check-in QR code. If you choose to share your status, locations directly receive your 2G/3G status when scanning your QR code. Furthermore, you can now view additional information about a location while you are checked in. For example, you can have a look at the menu, map or schedule of the location you are checked into!

# Release 2.1.0

- Support for third vaccination certificates
- Explanation screens 

With this update we support the import of your third vaccination certificate. We also introduce explanation screens, which explain every update's new features or give more information on existing ones!

# Release 2.0.3

- UI/UX improvements

This update contains UI and UX-improvements.

# Release 2.0.2

- bug fixes

This update includes bugfixes concerning the history and notifications.

# Release 2.0.1

- Performance bugfix

This version includes a critical performance bug fix.

# Release 2.0.0

- Check-in and history redesign 
- Introduction of new notifications 

We are proud to present luca 2.0. This update includes a major redesign of the check-in and history tab. This makes it easier for you to enjoy the features of luca, so that you can check in and use it even more intuitively.
In addition, you can now add the children to your check-in and import their documents.
Further, with this update we introduce additional notifications with different levels.

# Release 1.10.0

- Optimized network usage
- Error message improvements

This update will optimize network usage and therefore reduce the traffic produced. Furthermore, we improved our error messages for a better user experience.

# Release 1.9.1

- Bug fix

This update includes a check-in bug fix.

# Release 1.9.0

- Text and UI adjustments
- Bugfixes

This update includes some bugfixes. Furthermore, we have made minor adjustments to text and UI.

# Release 1.8.1

- Bugfixes

This update includes some bugfixes.

# Release 1.8.0

- Account Tab 
- Improved Voice-Over Mode

With this update we introduce a new tab: The "Account" tab. In the "Account" tab you can find all general functions about the App. Additionally, we improved the Voice-Over mode within this update. 

# Release 1.7.2

- Text and UI adjustments
- Bugfixes

This update includes some bugfixes. Furthermore, we have made minor adjustments to text and UI.

# Release 1.7.1

- Bug fixes

This update includes some bug fixes. 

# Release 1.7.0

- minor bug fixes
- display further document and proofs 

This update supports to import further documents that can be shown during check-in, like test and vaccination proofs.

# Release 1.6.8

- Bug fixes

This update contains minor bug fixes.

# Release 1.6.7

- Bug fixes

This update contains minor bug fixes.

# Release 1.6.6

- minor bugfix of deeplink handling

# Release 1.6.5

- UI adjustments
- Bugfixes

This update includes some bugfixes. Furthermore, we have made minor UI adjustments.

# Release 1.6.4

- Text and UI adjustments
- Bugfixes

This update includes some bugfixes. Furthermore, we have made minor adjustments to text and UI.

# Release 1.6.3

- bug fix of shown history

# Release 1.6.2

- important bug fix for CheckedIn Status Screen

# Release 1.6.1

- Minor improvements and bugfixes

In this update we have made minor adjustments to the "My luca" tab.

# Release 1.6.0

- Text and UI adjustments
- Minor bugfixes
- "My luca" tab

In this update we introduce another tab: the "My luca" tab. Furthermore, we have made minor adjustments to text and UI.

# Release 1.5.0

- User is notified of data accesses from health departments
- UI/UX adjustments
- Improved VoiceOver mode 
- Adjustment of the user consent for geofencing and history sharing

In this update we made some UI/UX adjustments.
Additionally, users are now notified when a health department has accessed their contact data while tracing an infection chain.
We also noticed that visually impaired users had a hard time using the app, so we implemented an initial improvement of the VoiceOver mode. Further improvements will follow.
Furthermore, we moved our license acknowledgements from the luca settings into the app.

# 1.5.0-dev

- Data access notification
- Switch to base32

# Release 1.4.2

- List of 3rd party libraries in settings

In this update we have included our 3rd party libraries in the luca settings.

# Release 1.4.1

- Improved automatic check-out
- Minor text and UI adjustments 
- Minor bugfixes

In this update, we have made minor adjustments to text and UI. Furthermore, you can now manually check-out even if automatic check-out is enabled.

# Release 1.4.0

- History can be reset locally
- Improved security through certificate pinning 
- UI adjustments
- Improved UX
- Bugfixes

This update includes some minor and major UI changes. They mainly help make features even easier to understand and find.  Furthermore, the app is even more secure now through certificate pinning. We've also fixed some bugs.

# Release 1.3.1

- A cleaner main screen
- Some UX improvements
- Other small edits

In this update, we've cleaned up the main screen of the app.  
Additionally, we've worked on smoother checkins and private meetings. The event history has been reworked as well.

# Release 1.3.0

- Improved phone number verification
- Addresses are mandatory to meet regulations
- Automatic deletion of your history
- A new and easy way to delete all your data
- Other small improvements

We noticed that for some of you, the phone number verification was not working smoothly. Therefore, we've greatly improved this process.  
As most countries require hosts to gather guest addresses, these fields are now mandatory to enable luca usage anywhere in Germany. 
It'll also be much easier to clean up your history: you will not see check-ins that are older than 14 days. 
Other small improvements include improved screens for smaller phones.

# Release 1.2.3

- Name fields can contain hyphens

If additional data is available, it is shown on the check-in screen
Previously, name fields could not contain hyphens. We've fixed that in this update. Furthermore, if a location provides additional data with their check-in (e.g. a table number), you can now see this on your check-in screen. 

# Release 1.2.0

- Set up private meetings within the app

From now on you can create QR codes for private meetings inside the app. These QR codes can be scanned by your friends and family, so that private meetings can also be added to your history. You can already use this feature at locations that are not part of the luca system to keep track of private meetings yourself.

# Release 1.1.0

- further developed security concept
- updated texts 
- some UX bugfixes

In this update we've released the 3rd version of our security concept. We've also worked on the texts and UX of the app to make it even easier to use.

# Release 1.0.1

- You will be notified when necessary app updates are available

# Release 1.0.0

- Self check-in at restaurant tables
- Improved UX

This update introduces a new feature: checking yourself in at restaurant tables. Furthermore, an improved UX, which we are constantly working on.
