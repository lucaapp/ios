import Foundation

/**
 This script requires a plain text file: ./Scripts/POEditor/api_token (without file name extension).
 
 Make sure that this file exists and contains your POEditor API token (a read-only token is good enough).

 You can create tokens in the API access section of your POEditor account settings on https://poeditor.com
 
 Make sure that you have admin access to the projects User App and iOS App.
 */

@main
@available(iOS 15.0, *)
enum POEditorScript {

    static let languages: [String] = ["en", "de"]
    static let userAppProject: POEditorProject = POEditorProject(id: "479239", title: "UserApp")
    static let iOSAppProject: POEditorProject = POEditorProject(id: "479233", title: "iOSApp")

    static func main() async {
        do {
            try await exportAndDownloadStringsFromPOEditor()
        } catch let error {
            print("POEditorScript failed with error: \(error)")
        }
    }

    private static func exportAndDownloadStringsFromPOEditor() async throws {
        for language in languages {
            try await exportAndDownloadStrings(for: language, and: userAppProject)
            try await exportAndDownloadStrings(for: language, and: iOSAppProject)
        }
    }

    private static func exportAndDownloadStrings(for language: String, and project: POEditorProject) async throws {
        guard let export = try await exportStrings(for: language, and: project),
              export.response.status == "success" else { return }

        try await downloadStrings(from: export.result.url, for: language, and: project)
    }

    // MARK: Export

    private static func exportStrings(for languageCode: String, and project: POEditorProject) async throws -> POEditorExport? {
        guard let urlRequest = try urlRequest(for: languageCode, and: project) else { return nil }

        print("\(project.title)-\(languageCode): Requests POEditor export")
        let (data, _) = try await URLSession.shared.data(for: urlRequest)
        let export = try JSONDecoder().decode(POEditorExport.self, from: data)
        return export
    }

    private static func urlRequest(for languageCode: String, and project: POEditorProject) throws -> URLRequest? {
        guard let exportURL = URL(string: "https://api.poeditor.com/v2/projects/export"),
              let exportParameters = try exportParameters(for: languageCode, and: project) else { return nil }

        var urlRequest = URLRequest(url: exportURL)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = exportParameters
        return urlRequest
    }

    private static func exportParameters(for languageCode: String, and project: POEditorProject) throws -> Data? {
        guard let token = try apiToken() else { return nil }

        var urlComponents = URLComponents()
        urlComponents.queryItems = [
            URLQueryItem(name: "language", value: languageCode),
            URLQueryItem(name: "api_token", value: token),
            URLQueryItem(name: "id", value: project.id),
            URLQueryItem(name: "type", value: "apple_strings")
        ]
        return urlComponents.percentEncodedQuery?.data(using: .utf8)
    }

    private static func apiToken() throws -> String? {
        guard let sourceRoot = ProcessInfo.processInfo.environment["SOURCE_ROOT"],
              let data = FileManager.default.contents(atPath: "\(sourceRoot)/Scripts/POEditor/api_token") else {
            throw POEditorScriptError.apiTokenNotFound
        }
        return String(data: data, encoding: .utf8)
    }

    // MARK: Download

    private static func downloadStrings(from url: String, for languageCode: String, and project: POEditorProject) async throws {
        guard let url = URL(string: url),
              let destinationURL = destination(for: languageCode, and: project) else { return }

        print("\(project.title)-\(languageCode): Downloads .strings file from url: \(url.absoluteString)")
        let (fileURL, _) = try await URLSession.shared.download(from: url)
        if project.id == userAppProject.id {
            print("\(project.title)-\(languageCode): Replaces special characters in downloaded .strings file in userApp project")
            try replaceAndEscapeSpecialCharacters(inFile: fileURL)
        }

        try replacePlaceholders(inFile: fileURL)

        print("\(project.title)-\(languageCode): Saves .strings file in project folder")
        try removeItem(at: destinationURL)
        try FileManager.default.moveItem(at: fileURL, to: destinationURL)
    }

    private static func destination(for languageCode: String, and project: POEditorProject) -> URL? {
        guard let sourceRoot = ProcessInfo.processInfo.environment["SOURCE_ROOT"] else { return nil }

        let destinationPath = "\(sourceRoot)/Luca/\(languageCode).lproj/\(project.title).strings"
        return URL(fileURLWithPath: destinationPath)
    }

    private static func replacePlaceholders(inFile url: URL) throws {
        let keyValueSeparator: String = " = "
        var newLines: String = ""
        let fileContents: String = try String.init(contentsOf: url)
        let lines: [String] = fileContents.components(separatedBy: .newlines)
        for line in lines {
            let keyValueComponents = line.components(separatedBy: keyValueSeparator)
            if keyValueComponents.count > 1 {
                let key = keyValueComponents[0]
                let value = keyValueComponents[1]
                    .splitByMarkdownLinks
                    .map {
                        if !$0.isMarkdownLink {
                            return $0.replacingOccurrences(of: "\\[(.*?)\\]", with: "%@", options: .regularExpression, range: nil)
                        }
                        return $0
                    }
                    .reduce("", +)
                let newLine = key + keyValueSeparator + value
                newLines.append(newLine + "\n")
            } else {
                newLines.append(line + "\n")
            }
        }
        try newLines.write(to: url, atomically: true, encoding: .utf8)
    }

    private static func replaceAndEscapeSpecialCharacters(inFile url: URL) throws {
        let keyValueSeparator: String = " = "
        var newLines: String = ""
        let fileContents: String = try String.init(contentsOf: url)
        let lines: [String] = fileContents.components(separatedBy: .newlines)
        for line in lines {
            let keyValueComponents = line.components(separatedBy: keyValueSeparator)
            if keyValueComponents.count > 1 {
                let key = keyValueComponents[0]
                let value = keyValueComponents[1]
                let newKey = key.replacingOccurrences(of: "_", with: ".")
                let newValue = value.replacingOccurrences(of: "%", with: "%%")
                let newLine = newKey + keyValueSeparator + newValue
                newLines.append(newLine + "\n")
            } else {
                newLines.append(line + "\n")
            }
        }
        try newLines.write(to: url, atomically: true, encoding: .utf8)
    }

    private static func removeItem(at url: URL) throws {
        guard FileManager.default.fileExists(atPath: url.path) else { return }
        try FileManager.default.removeItem(at: url)
    }
}

enum POEditorScriptError: Error {

    case apiTokenNotFound
}

struct POEditorProject {

    let id: String
    let title: String
}

struct POEditorExport: Codable {

    var response: Response
    var result: Result

    struct Response: Codable {

        var status: String
    }

    struct Result: Codable {

        var url: String
    }
}
