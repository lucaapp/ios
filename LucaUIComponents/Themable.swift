import Foundation
import UIKit

public enum AppearanceTheme {
    case light
    case dark
}

public protocol Themable {

    /// Currently applied theme
    var theme: AppearanceTheme { get }

    /// Themes this instance is supporting
    var supportedThemes: [AppearanceTheme] { get }

    /// Entry point, where the instance can  adjust itself to the theme.
    ///
    /// This view should respect the `supportedThemes` property.
    /// This method should also set `theme` property accordingly.
    func apply(theme: AppearanceTheme)
}

public extension Themable {

    // Default implementation, where the item supports only its own theme
    var supportedThemes: [AppearanceTheme] {
        [theme]
    }

    func supports(theme: AppearanceTheme) -> Bool {
        supportedThemes.contains(theme)
    }

    func apply(theme: AppearanceTheme) {}
}

public extension UIView {
    func applyToSubviews(theme: AppearanceTheme, exceptions: [UIView] = []) {
        for subview in subviews {

            if exceptions.contains(subview) {
                continue
            }

            if let themableSubview = subview as? Themable {

                if themableSubview.supports(theme: theme) {
                    themableSubview.apply(theme: theme)
                }

            } else {

                // It should search for first Themable encounters.
                subview.applyToSubviews(theme: theme)
            }
        }
    }
}

public extension UIViewController {
    func applyToViewAndChildrenControllers(theme: AppearanceTheme) {
        var viewsOfChildren: [UIView] = []
        if let titleView = self.navigationItem.titleView {
            titleView.applyToSubviews(theme: theme)
        }
        for vc in children {
            if let themableViewController = vc as? Themable {
                if !themableViewController.supports(theme: theme) {
                    continue
                }
                themableViewController.apply(theme: theme)
            }
            vc.applyToViewAndChildrenControllers(theme: theme)
            viewsOfChildren.append(vc.view)
        }
        view.applyToSubviews(theme: theme, exceptions: viewsOfChildren)
    }
}
