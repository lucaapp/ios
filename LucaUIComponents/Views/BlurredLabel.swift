import UIKit

extension SelfSizingLabel {

    public func blurredTextAsImage() -> UIImage? {
        UIGraphicsBeginImageContext(self.bounds.size)
        guard let ctx1 = UIGraphicsGetCurrentContext() else {return nil}
        self.layer.render(in: ctx1)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        if let cgImage = image?.cgImage, let gaussianFilter = CIFilter(name: "CIGaussianBlur") {
            gaussianFilter.setDefaults()
            let input = CIImage(cgImage: cgImage)
            gaussianFilter.setValue(input, forKeyPath: kCIInputImageKey)
            gaussianFilter.setValue(5.0, forKeyPath: kCIInputRadiusKey)
            if let output = gaussianFilter.outputImage {
                let blurredImage = UIImage(ciImage: output)
                return blurredImage
            }
        }
        return nil
    }
}
