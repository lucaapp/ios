import UIKit

open class LucaDefaultTextField: UITextField {
    let padding = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }

    open override func prepareForInterfaceBuilder() {
        self.setup()
    }

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    open func set(_ textContentType: UITextContentType, autocorrection: UITextAutocorrectionType = .default) {
        autocorrectionType = autocorrection
        self.textContentType = textContentType

        switch textContentType {
        case .name, .namePrefix, .nameSuffix, .givenName, .middleName,
             .familyName, .addressCity, .addressState, .addressCityAndState,
             .streetAddressLine1, .streetAddressLine2, .fullStreetAddress, .postalCode:
            keyboardType = .webSearch
        case .telephoneNumber:
            keyboardType = .phonePad
        case .emailAddress:
            keyboardType = .emailAddress
        default:
            keyboardType = .default
        }
    }

    open func setPlaceholder(text: String? = nil, color: UIColor, font: UIFont) {
        let placeholderString = text ?? attributedPlaceholder?.string ?? ""

        let attributes = [
            NSAttributedString.Key.foregroundColor: color,
            NSAttributedString.Key.font: font
        ]

        attributedPlaceholder = NSAttributedString(string: placeholderString, attributes: attributes)
    }

    func setup() {
        borderStyle = .none
        layer.borderWidth = 1
        layer.cornerRadius = 8
        clearButtonMode = .whileEditing
    }
}
