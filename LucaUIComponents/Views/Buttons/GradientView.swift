import UIKit

@IBDesignable
public class GradientView: UIView {
	@IBInspectable public dynamic var firstColor: UIColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0) {
		didSet {
			updateView()
		}
	}
	@IBInspectable public dynamic var secondColor: UIColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0) {
		didSet {
			updateView()
		}
	}

	override public class var layerClass: AnyClass {
        return CAGradientLayer.self
	}

	@IBInspectable var startPoint: CGPoint = CGPoint(x: 0.5, y: 0.0) {
		didSet {
			updateView()
		}
	}

	@IBInspectable var endPoint: CGPoint = CGPoint(x: 0.5, y: 1.0) {
		didSet {
			updateView()
		}
	}

	override init(frame: CGRect) {
		super.init(frame: frame)
		sharedInit()
	}

	required public init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		sharedInit()
	}

	override public func prepareForInterfaceBuilder() {
		sharedInit()
	}

	func sharedInit() {
		updateView()
	}

	func updateView() {
		guard let layer = self.layer as? CAGradientLayer else {return}
		layer.colors = [firstColor, secondColor].map {$0.cgColor}
		layer.startPoint = self.startPoint
		layer.endPoint = self.endPoint
		backgroundColor = UIColor.clear
	}
}
