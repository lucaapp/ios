import Foundation
import UIKit

@IBDesignable
open class DesignableButton: UIButton {
	@IBInspectable
	public var cornerRadius: CGFloat {
		get {
			return layer.cornerRadius
		}
		set {
			layer.cornerRadius = newValue
		}
	}

    @IBInspectable
    public var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable public dynamic var uppercaseTitle: Bool = false {
        didSet {
            self.setTitle(titleLabel?.text?.uppercased(), for: .normal)
        }
    }

	@IBInspectable
	var adjustsFontForContentSizeCategory: Bool {
		get {
			return self.titleLabel?.adjustsFontForContentSizeCategory ?? false
		}
        set {
            self.titleLabel?.adjustsFontForContentSizeCategory = newValue
        }
	}
}
