import UIKit

@IBDesignable
public class CheckinSliderButton: UIControl {
	private let animationDuration = 0.1
	private let sliderImageView = UIImageView()
	private var activityIndicator = UIActivityIndicatorView(style: .gray)

	private let minValue = CGFloat(0)
	private let maxValue = CGFloat(1)

	public var valueUpdateBlock: ((CGFloat) -> Void)?
	public var completionBlock: (() -> Void)?

	public var processing: Bool = false {
		didSet {
			if processing {
				sliderImageView.isUserInteractionEnabled = false
				sliderImageView.image = sliderProcessingImage
				activityIndicator.isHidden = false
				activityIndicator.startAnimating()
			} else {
				sliderImageView.isUserInteractionEnabled = true
				sliderImageView.image = sliderImage
				activityIndicator.stopAnimating()
				activityIndicator.isHidden = true
				self.reset()
			}
		}
	}

	public override var frame: CGRect {
		didSet {
			layer.cornerRadius = frame.size.height / 2
		}
	}

	@IBInspectable public dynamic var sliderImage: UIImage? {
		didSet {
			setup()
		}
	}

	@IBInspectable public dynamic var sliderProcessingImage: UIImage? {
		didSet {
			setup()
		}
	}

	@IBInspectable public dynamic var backgroundColor1: UIColor = UIColor.black {
		didSet {
			setup()
		}
	}

	@IBInspectable public dynamic var backgroundColor2: UIColor = UIColor.white {
		didSet {
			setup()
		}
	}

	private var maxXPos: CGFloat {
		frame.width - frame.height
	}

	private var value: CGFloat {
		sliderImageView.frame.origin.x / maxXPos
	}

	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}

	open override func prepareForInterfaceBuilder() {
		self.setup()
	}

	public func reset() {
		UIView.animate(withDuration: animationDuration) { [weak self] in
			self?.setSliderFrame(x: self?.bounds.origin.x ?? 0)
		}
	}

	public func setAccessibilityLabel(text: String) {
		accessibilityLabel = text
	}

    public override func accessibilityActivate() -> Bool {
        completionBlock?()
        return true
    }

	private func setup() {
		backgroundColor = .clear

        activityIndicator =	UIActivityIndicatorView(style: .medium)
		activityIndicator.color = .black
		layer.cornerRadius = frame.size.height / 2
		sliderImageView.image = self.sliderImage
		sliderImageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
		addSubview(sliderImageView)

		addSubview(activityIndicator)
		activityIndicator.isUserInteractionEnabled = false
		activityIndicator.translatesAutoresizingMaskIntoConstraints = false
		activityIndicator.widthAnchor.constraint(equalToConstant: 40).isActive = true
		activityIndicator.heightAnchor.constraint(equalToConstant: 40).isActive = true
		activityIndicator.centerXAnchor.constraint(equalTo: sliderImageView.centerXAnchor, constant: 1.0).isActive = true
		activityIndicator.centerYAnchor.constraint(equalTo: sliderImageView.centerYAnchor, constant: 1.0).isActive = true

		reset()

		let panGesture = UIPanGestureRecognizer(target: self, action: #selector(draggedView))
		sliderImageView.isUserInteractionEnabled = true
		sliderImageView.addGestureRecognizer(panGesture)

		let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedView(_:)))
		sliderImageView.addGestureRecognizer(tapGesture)

		isAccessibilityElement = true
		accessibilityTraits = .button
	}

	@objc func tappedView(_ gesture: UITapGestureRecognizer) {
		guard sliderImageView.accessibilityElementIsFocused() && UIAccessibility.isVoiceOverRunning else { return }
		completionBlock?()
	}

	@objc func draggedView(_ gesture: UIPanGestureRecognizer) {
		if UIAccessibility.isVoiceOverRunning {
			return
		}
		let translation = gesture.translation(in: self)
		let xToComplete = maxXPos - (maxXPos * 0.2)

		if gesture.state == .began {
			reset()
		} else if gesture.state == .changed {
			let newXValue = translation.x
			let newWithinBounds = boundValue(newXValue, lowerValue: minValue, upperValue: maxXPos)
			setSliderFrame(x: newWithinBounds)
			valueUpdateBlock?(value)

			if value == maxValue {
				completionBlock?()
			}
		} else if gesture.state == .ended {
			let finalX = sliderImageView.frame.origin.x > xToComplete ? maxXPos : minValue
			UIView.animate(withDuration: animationDuration) { [weak self] in
				self?.setSliderFrame(x: finalX)
				if self?.value == self?.maxValue {
					self?.completionBlock?()
				}
			}
		}
	}

	private func setSliderFrame(x: CGFloat) {
		sliderImageView.frame = CGRect(origin: CGPoint(x: x, y: 0),
																	 size: CGSize(width: frame.height, height: frame.height))
	}

	private func boundValue(_ value: CGFloat, lowerValue: CGFloat, upperValue: CGFloat) -> CGFloat {
		return min(max(value, lowerValue), upperValue)
	}
}
