import Foundation
import UIKit

open class RoundedLucaButton: DesignableButton {
    internal func borderWidth() -> CGFloat {return 0}

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }

    open override func prepareForInterfaceBuilder() {
        self.setup()
    }

    func setup() {
        self.cornerRadius = frame.height * 0.5
        self.layer.borderWidth = borderWidth()
        if uppercaseTitle {
            self.titleLabel?.text = self.titleLabel?.text?.uppercased()
        }
        self.titleLabel?.numberOfLines = 0
        self.titleLabel?.adjustsFontForContentSizeCategory = true
        self.titleLabel?.lineBreakMode = .byWordWrapping
        self.contentEdgeInsets = UIEdgeInsets(top: 16, left: 24, bottom: 16, right: 24)
        if #available(iOS 15.0, *) {
            self.maximumContentSizeCategory = .accessibilityLarge
        }
    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        self.cornerRadius = self.frame.height * 0.5
    }

    open override var isEnabled: Bool {
        didSet {
            alpha = isEnabled ? 1.0 : 0.5
        }
    }
}
