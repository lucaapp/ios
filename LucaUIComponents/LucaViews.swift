import Foundation
import UIKit

@IBDesignable
public class LightStandardButton: RoundedLucaButton {}

@IBDesignable
public class BlackTransparentButton: RoundedLucaButton {
    internal override func borderWidth() -> CGFloat {return 1}
}

@IBDesignable
public class DarkStandardButton: RoundedLucaButton, Themable {
    public var theme: AppearanceTheme = .dark
    public var supportedThemes: [AppearanceTheme] = [.dark, .light]

	internal override func borderWidth() -> CGFloat {return 1}

    public func apply(theme: AppearanceTheme) {
        guard supports(theme: theme) else { return }
        self.theme = theme
        switch theme {
        case .dark:
            titleLabelColor = UIColor.white
            backgroundColor = UIColor.clear
            borderColor = UIColor.white
        case .light:
            titleLabelColor = UIColor.black
            backgroundColor = UIColor.white
            borderColor = UIColor.black
        }
    }
}

public class SelfSizingLabel: UILabel, Themable {

    private var defaultFont: UIFont?

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        initObserver()
    }

	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setup()
        initObserver()
	}

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    private func setup() {
        self.adjustsFontForContentSizeCategory = true
        if #available(iOS 15.0, *) {
            self.maximumContentSizeCategory = .accessibilityLarge
        }
	}

    @objc
    dynamic public func setDefaultFont(_ font: UIFont) {
        defaultFont = font
        updateBoldText()
    }

    private func initObserver() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.updateBoldText),
                                               name: UIAccessibility.boldTextStatusDidChangeNotification,
                                               object: nil)
    }

    @objc
    private func updateBoldText() {
        if UIAccessibility.isBoldTextEnabled {
            guard let defaultFont = defaultFont, let descriptor = UIFontDescriptor(name: defaultFont.familyName, size: defaultFont.pointSize).withSymbolicTraits(.traitBold) else {
                font = defaultFont
                return
            }

            font = UIFont(descriptor: descriptor, size: font.pointSize)
        } else {
            font = defaultFont
        }
    }
    public var theme: AppearanceTheme = .dark
    public var supportedThemes: [AppearanceTheme] {
        [.dark, .light]
    }

    public func apply(theme: AppearanceTheme) {
        if !supports(theme: theme) {
            return
        }
        switch theme {
        case .dark:
            textColor = .white
        case .light:
            textColor = .black
        }
        self.theme = theme
    }
}

public class Luca14PtBlackLabel: SelfSizingLabel {}
public class Luca14PtBoldBlackLabel: SelfSizingLabel {}
public class Luca14PtBoldLabel: SelfSizingLabel {}
public class Luca16PtLabel: SelfSizingLabel {}
public class Luca16PtBoldLabel: SelfSizingLabel {}
public class Luca16PtBoldBlackLabel: SelfSizingLabel {}
public class Luca20PtBoldLabel: SelfSizingLabel {}
public class Luca24PtBoldLabel: SelfSizingLabel {}
public class TANLabel: SelfSizingLabel {}
public class Luca60PtLabel: SelfSizingLabel {}
public class Luca12PtLabel: SelfSizingLabel {}
public class PrivateMeetingTimerLabel: SelfSizingLabel {}
public class Luca14PtBoldAlertLabel: SelfSizingLabel {}
public class LucaAccountTitleLabel: SelfSizingLabel {}
public class Luca20PtMediumLabel: SelfSizingLabel {}

@IBDesignable
public class WhiteButton: RoundedLucaButton {}

@IBDesignable
public class BlueButton: DesignableButton {}

public class Luca14PtLabel: SelfSizingLabel {}
