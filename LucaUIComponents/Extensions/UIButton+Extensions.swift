import Foundation
import UIKit

public extension UIButton {
	@objc dynamic var titleLabelFont: UIFont! {
		get { return titleLabel?.font }
		set { titleLabel?.font = newValue }
	}

	@objc dynamic var titleLabelColor: UIColor! {
		get { return titleLabel?.textColor }
		set {
			titleLabel?.textColor = newValue
			setTitleColor(newValue, for: .normal)
		}
	}

	@objc func setTitleLabelColor(color: UIColor, state: UIControl.State) {
		setTitleColor(color, for: state)
	}

    /// Sets spacing between image and title
    func setInsets(
        forContentPadding contentPadding: UIEdgeInsets,
        spacing: CGFloat
    ) {
        contentEdgeInsets = UIEdgeInsets(
            top: contentPadding.top,
            left: contentPadding.left,
            bottom: contentPadding.bottom,
            right: contentPadding.right + spacing
        )
        titleEdgeInsets = UIEdgeInsets(
            top: 0,
            left: spacing,
            bottom: 0,
            right: -spacing
        )
    }
}
