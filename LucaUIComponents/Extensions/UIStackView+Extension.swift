import UIKit

extension UIStackView {

    private var duration: Double { 0.35 }
    private var damping: CGFloat { 0.9 }
    private var initialSpringVelocity: CGFloat { 1.0 }

    /// Adds a view to the stacks subviews and animates the stack to adjust its size.
    /// - Parameters:
    ///   - view: View to add
    ///   - animated: Should the appearance be animated?
    ///   - completion: Completion block called after the adding. It's also called when the appearance is not animated. Can be nil
    public func addArrangedSubview(_ view: UIView, animated: Bool, completion: ((Bool) -> Void)? = nil) {
        if self.arrangedSubviews.contains(view) {
            return
        }
        if animated {
            view.isHidden = true
            view.alpha = 0.0
        } else {
            view.isHidden = false
            view.alpha = 1.0
        }

        self.addArrangedSubview(view)
        self.layoutSubviews()

        if animated {
            UIView.animate(
                withDuration: duration,
                delay: 0.1, // It allows the subview to layout itself in a clean manner
                usingSpringWithDamping: damping,
                initialSpringVelocity: 1,
                options: [],
                animations: {
                    view.isHidden = false
                    view.alpha = 1.0
                    self.layoutSubviews()
                },
                completion: completion
            )
        } else { completion?(true) }
    }

    /// Removes arranged subview from stack and animates the stack to adjust its size.
    ///
    /// To the contrary of the default `removeArrangedSubview` implementation, It removes also the view from the stacks subview.
    /// - Parameters:
    ///   - view: View to remove
    ///   - animated: Should the transition be animated?
    ///   - completion: Completion called after removal. It's also called when it's not animated. Can be nil.
    public func removeArrangedSubview(_ view: UIView, animated: Bool, completion: ((Bool) -> Void)? = nil) {
        if !self.arrangedSubviews.contains(view) {
            return
        }

        if animated {
            UIView.animate(
                withDuration: duration,
                delay: 0.1, // It allows the subview to layout itself in a clean manner
                usingSpringWithDamping: damping,
                initialSpringVelocity: initialSpringVelocity,
                options: [],
                animations: {
                    view.isHidden = true
                    view.alpha = 0.0
                    self.layoutSubviews()
                },
                completion: { success in
                    self.removeArrangedSubview(view)
                    view.removeFromSuperview()
                    completion?(success)
                }
            )
        } else {
            self.removeArrangedSubview(view)
            view.removeFromSuperview()
            completion?(true)
        }
    }

    /// Shows already inserted view.
    /// - Parameters:
    ///   - view: view to show
    ///   - animated: true if the stack view should animate itself
    public func show(view: UIView, animated: Bool = true, completion: ((Bool) -> Void)? = nil) {
        if !self.arrangedSubviews.contains(view) {
            return
        }
        if !animated {
            view.isHidden = false
            view.alpha = 1.0
            layoutSubviews()
            completion?(true)
            return
        }
        UIView.animate(
            withDuration: duration,
            delay: 0,
            usingSpringWithDamping: damping,
            initialSpringVelocity: initialSpringVelocity,
            options: [],
            animations: {
                view.isHidden = false
                view.alpha = 1.0
                self.layoutSubviews()
            },
            completion: completion
        )
    }

    /// Hides already inserted view.
    /// - Parameters:
    ///   - view: view to hide
    ///   - animated: true if the stack view should animate itself
    public func hide(view: UIView, animated: Bool = true, completion: ((Bool) -> Void)? = nil) {
        if !self.arrangedSubviews.contains(view) {
            return
        }
        if !animated {
            view.isHidden = true
            view.alpha = 0.0
            layoutSubviews()
            completion?(true)
            return
        }
        UIView.animate(
            withDuration: duration,
            delay: 0,
            usingSpringWithDamping: damping,
            initialSpringVelocity: initialSpringVelocity,
            options: [],
            animations: {
                view.isHidden = true
                view.alpha = 0.0
                self.layoutSubviews()
            },
            completion: completion
        )

    }
}

extension UIStackView {
    public func removeAllArrangedSubviews() {
        arrangedSubviews.forEach {
            self.removeArrangedSubview($0)
            NSLayoutConstraint.deactivate($0.constraints)
            $0.removeFromSuperview()
        }
    }
}
