import Foundation

protocol WeakElementProtocol {
    associatedtype Element
    var element: Element? { get set }
}

class WeakElement<T>: WeakElementProtocol where T: AnyObject {
    typealias Element = T
    weak var element: T?
    init(_ element: T?) {
        self.element = element
    }
}

extension Array where Element: WeakElementProtocol {
    var nonNil: Bool {
        filter { $0.element == nil }.isEmpty
    }
}
