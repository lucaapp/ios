import Foundation

@propertyWrapper public class InjectStatic<Provider, ValueType> where Provider: InjectionProvider, Provider.T == ValueType, Provider: AnyObject {
    private let keyPath: WritableKeyPath<DependencyContext, Provider>
    private var cached: ValueType?
    // swiftlint:disable implicit_getter
    var getOrCreate: ValueType {
        get throws {
            if let c = cached { return c }
            cached = nil
            let generated = try DependencyContext[keyPath].provide()
            cached = generated
            return generated
        }
    }

    public var wrappedValue: ValueType {
        // swiftlint:disable:next force_try
        return try! getOrCreate
    }

    public var projectedValue: InjectStatic<Provider, ValueType> {
        return self
    }

    private var observerTokens: [NSObjectProtocol] = []

    public init(_ keyPath: WritableKeyPath<DependencyContext, Provider>) {
        self.keyPath = keyPath
        observerTokens = NotificationCenter.default.addObserver(
            forNames: [DependencyContext.setNotification, DependencyContext.removeNotification],
            object: nil,
            queue: nil) { [weak self] notification in
                if let key = notification.userInfo?["key"],
                   let loadedKeyPath = key as? WritableKeyPath<DependencyContext, Provider>,
                   loadedKeyPath == keyPath {
                    self?.cached = nil
                }
            }
    }

    deinit {
        observerTokens.forEach { NotificationCenter.default.removeObserver($0) }
    }
}

@propertyWrapper public class InjectDynamic<ValueType> {
    private var key: String?
    private var cached: ValueType?
    var getOrCreate: ValueType {
        get throws {
            if let c = cached { return c }
            cached = nil
            let cachedProvider: BaseInjectionProvider<ValueType>?
            if let k = key {
                cachedProvider = try DependencyContext.retrieveProvider(key: k)
            } else {
                cachedProvider = try DependencyContext.retrieveProvider()
            }
            guard let provider = cachedProvider else {
                throw DependencyContextError.keyNotFound(key: DependencyContext.key(for: ValueType.self))
            }
            let generated: ValueType = try provider.provide()
            cached = generated
            return generated
        }
    }
    public var wrappedValue: ValueType {
        // swiftlint:disable:next force_try
        get { try! getOrCreate }
    }

    public var projectedValue: InjectDynamic<ValueType> {
        get { self }
    }

    private var observerTokens: [NSObjectProtocol] = []

    public init(_ key: String? = nil) {
        self.key = key
        observerTokens = NotificationCenter.default.addObserver(
            forNames: [DependencyContext.setNotification, DependencyContext.removeNotification],
            object: nil,
            queue: nil) { [weak self] notification in
                if let loadedKey = notification.userInfo?["key"] as? String,
                   loadedKey == key || loadedKey == DependencyContext.key(for: ValueType.self) {
                    self?.cached = nil
                }
            }
    }

    deinit {
        observerTokens.forEach { NotificationCenter.default.removeObserver($0) }
    }
}

@propertyWrapper public class InjectAllDynamic<ValueType> {
    private var cached: [ValueType]?
    private var getOrCreate: [ValueType] {
        if let c = cached { return c }
        cached = nil
        let provided: [ValueType] = DependencyContext.provideAll()
        cached = provided
        return provided
    }
    public var wrappedValue: [ValueType] {
        get { getOrCreate }
    }

    public var projectedValue: InjectAllDynamic<ValueType> {
        get { self }
    }

    private var observerTokens: [NSObjectProtocol] = []

    public init() {
        observerTokens = NotificationCenter.default.addObserver(
            forNames: [DependencyContext.setNotification, DependencyContext.removeNotification],
            object: nil,
            queue: nil) { [weak self] _ in
                self?.cached = nil
            }}

    deinit {
        observerTokens.forEach { NotificationCenter.default.removeObserver($0) }
    }
}

extension NotificationCenter {
    func addObserver(forNames names: [NSNotification.Name], object obj: Any?, queue: OperationQueue?, using block: @escaping (Notification) -> Void) -> [NSObjectProtocol] {
        names.map { addObserver(forName: $0, object: obj, queue: queue, using: block) }
    }
}
