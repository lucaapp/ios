class ReferenceCounter {
    private var references: [String: Int] = [:]
    func add(reference: AnyObject) -> Int {
        let str = pointer(of: reference)
        if references[str] == nil {
            references[str] = 0
        }
        references[str]! += 1
        return references[str]!
    }
    func remove(reference: AnyObject) -> Int {
        let str = pointer(of: reference)
        if references[str] == nil {
            references[str] = 0
        }
        references[str]! -= 1
        if references[str]! < 0 {
            print("[ReferenceCounter] [\(reference)] reference is negative!")
        }
        return references[str]!
    }

    private func pointer(of object: AnyObject) -> String {
        let ptr = Unmanaged<AnyObject>.passUnretained(object).toOpaque()
        return "\(ptr)"
    }
}
