public protocol SharedProviderConnectionEventHandler {
    func didConnectToSharedProvider()
    func didDisconnectFromSharedProvider()
}

public extension SharedProviderConnectionEventHandler {

    // Provide empty implementation (it won't be used so often)
    func didConnectToSharedProvider() {}
}
