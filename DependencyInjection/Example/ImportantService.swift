import Foundation

class ImportantService {
    private let randomId = Int.random(in: 0..<100)
    init() {
        print("ImportantService.init \(randomId)")
    }

    func someStuff() {
        print("ImportantService.someStuff \(randomId)")
    }
}

private var importantServiceProvider = SharedProvider(value: ImportantService())
private var importantServiceAlwaysNewProvider = AlwaysNewProvider(valueGenerator: { ImportantService() })

extension DependencyContext {
    var importantService: SharedProvider<ImportantService> {
        get { importantServiceProvider }
        set { importantServiceProvider = newValue }
    }
}
extension DependencyContext {
    var importantServiceAlwaysNew: AlwaysNewProvider<ImportantService> {
        get { importantServiceAlwaysNewProvider }
        set { importantServiceAlwaysNewProvider = newValue }
    }
}
