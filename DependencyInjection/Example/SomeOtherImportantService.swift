import Foundation

class SomeOtherImportantService {
    private let randomId = Int.random(in: 0..<100)
    init() {
        print("SomeOtherImportantService.init \(randomId)")
    }

    func someStuff() {
        print("SomeOtherImportantService.someStuff \(randomId)")
    }
}
