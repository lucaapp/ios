import Foundation

class InjectionExample {

    @InjectAllDynamic var dynamicImportantServices: [ImportantService]

    // This case is for services like TraceIdService etc. Always a single instance
    @InjectStatic(\.importantService) var importantGlobalService0
    @InjectStatic(\.importantService) var importantGlobalService1

    // Those cases would be for cases like Coordinators, where the instances would be generated at the injection time
    @InjectStatic(\.importantServiceAlwaysNew) var newlyGeneratedImportantService0
    @InjectStatic(\.importantServiceAlwaysNew) var newlyGeneratedImportantService1
    @InjectStatic(\.importantServiceAlwaysNew) var newlyGeneratedImportantService2

    // This injection will be resolved through type matching
    @InjectDynamic var dynamicallyInjected: SomeOtherImportantService

    // This injection will crash the app as the type is not registered. But the resolution happens lazily, so the crash would happen upon first access
    @InjectDynamic var dynamicallyInjectedMissingType: Data

    // It should be accessed later on after registering mocked service
    @InjectStatic(\.importantService) var mockedImportantService: ImportantService

    @InjectAllDynamic var importantServices: [ImportantService]

    init() {}

    func doStuff() {

        print("Before resolve")

        try? DependencyContext.register(SharedProvider(value: ImportantService()), keyIdentifier: "SomeImportantService0")
        try? DependencyContext.register(SharedProvider(value: ImportantService()), keyIdentifier: "SomeImportantService1")

        print("dynamicImportantServices count: \(dynamicImportantServices.count)")

        // Those should print the same statements
        importantGlobalService0.someStuff()
        importantGlobalService1.someStuff()

        // Those should be generated once more but later the same ID will be used (as the instance is created only for the first retrieval)
        newlyGeneratedImportantService0.someStuff()
        newlyGeneratedImportantService0.someStuff()
        newlyGeneratedImportantService0.someStuff()

        // Those should print everytime a new ID
        newlyGeneratedImportantService1.someStuff()
        newlyGeneratedImportantService2.someStuff()

        // This resolves a dynamically registered type. Should work
        dynamicallyInjected.someStuff()

        // Override default provider for testing
        DependencyContext[\.importantService] = SharedProvider(value: MockedImportantService())
        mockedImportantService.someStuff()

        print(importantServices)

        // This will crash!
//        dynamicallyInjectedMissingType.deleteUserData()
    }
}

class MockedImportantService: ImportantService {

    override func someStuff() {
        print("MockedImportantService.someStuff")
    }
}
