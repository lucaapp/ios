import Foundation

public enum InjectionProviderError: Error {
    case noValue
}

internal protocol Connectable {
    func connect()
    func disconnect()
}

public protocol InjectionProvider {
    associatedtype T

    func provide() throws -> T
}

public class BaseInjectionProvider<T>: InjectionProvider {
    public typealias T = T

    public func provide() throws -> T {
        fatalError("Not implemented")
    }
}

private var referenceCounter = ReferenceCounter()

/// Used for singletons
public class SharedProvider<T>: BaseInjectionProvider<T>, Connectable {

    func connect() {
        guard let ref = value as? AnyObject else {
            return
        }
        let referenceCount = referenceCounter.add(reference: ref)
        if let eventHandler = ref as? SharedProviderConnectionEventHandler,
           referenceCount == 1 {
            eventHandler.didConnectToSharedProvider()
        }
    }

    func disconnect() {
        guard let ref = value as? AnyObject else {
            return
        }
        let referenceCount = referenceCounter.remove(reference: ref)
        if let eventHandler = ref as? SharedProviderConnectionEventHandler,
           referenceCount == 0 {
            eventHandler.didDisconnectFromSharedProvider()
        }
    }

    private let value: T
    public init(value: T) {
        self.value = value
    }

    public override func provide() throws -> T {
        value
    }
}

/// Used when every requester should get a new instance
public class AlwaysNewProvider<T>: BaseInjectionProvider<T> {

    private let valueGenerator: () -> T
    public init(valueGenerator: @escaping () -> T) {
        self.valueGenerator = valueGenerator
    }

    public override func provide() throws -> T {
        valueGenerator()
    }
}

/// Used to invalidate previous providers
public class EmptyProvider<T>: BaseInjectionProvider<T> {
    public override func provide() throws -> T {
        throw InjectionProviderError.noValue
    }

    public override init() {}
}
