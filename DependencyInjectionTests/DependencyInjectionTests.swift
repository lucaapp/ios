import XCTest
@testable import DependencyInjection

class DependencyInjectionTests: XCTestCase {

    @InjectDynamic var dynamicUnregisteredProperty: Data
    @InjectDynamic private var dynamicSomeService: SomeService
    @InjectDynamic("SomeServiceKey") private var dynamicSomeServiceWithKey: SomeService
    @InjectAllDynamic private var dynamicSomeServices: [SomeService]

    @InjectStatic(\.someService) private var staticService

    override func setUpWithError() throws {
        continueAfterFailure = false
        DependencyContext[\.someService] = SharedProvider(value: SomeService("static"))
    }

    override func tearDownWithError() throws {
        DependencyContext.removeAllDynamic()
    }

    func test_static() {
        XCTAssertEqual(staticService.someString, "static")
    }

    func test_dynamicWithKey() throws {
        try DependencyContext.register(SharedProvider(value: SomeService("string")), keyIdentifier: "SomeServiceKey")
        XCTAssertEqual(dynamicSomeServiceWithKey.someString, "string")
    }

    func test_dynamic() throws {
        try DependencyContext.register(SharedProvider(value: SomeService("string")))
        XCTAssertEqual(dynamicSomeService.someString, "string")
    }

    func test_propertyWrapperStaticReplacement() throws {
        XCTAssertEqual(staticService.someString, "static")
        DependencyContext[\.someService] = SharedProvider(value: SomeService("newString"))
        XCTAssertEqual(staticService.someString, "newString")
    }

    func test_propertyWrapperDynamicReplacement() throws {
        try DependencyContext.register(SharedProvider(value: SomeService("string")))
        XCTAssertEqual(dynamicSomeService.someString, "string")
        DependencyContext.remove(for: SomeService.self)
        try DependencyContext.register(SharedProvider(value: SomeService("newString")))
        XCTAssertEqual(dynamicSomeService.someString, "newString")
    }

    func test_propertyWrapperDynamicWithKeyReplacement() throws {
        try DependencyContext.register(SharedProvider(value: SomeService("string")), keyIdentifier: "SomeServiceKey")
        XCTAssertEqual(dynamicSomeServiceWithKey.someString, "string")
        DependencyContext.remove(for: "SomeServiceKey")
        try DependencyContext.register(SharedProvider(value: SomeService("newString")), keyIdentifier: "SomeServiceKey")
        XCTAssertEqual(dynamicSomeServiceWithKey.someString, "newString")
    }

    func test_dynamicAllInjection() throws {
        try DependencyContext.register(SharedProvider(value: SomeService("service0")), keyIdentifier: "SomeService0")
        try DependencyContext.register(SharedProvider(value: SomeService("service1")), keyIdentifier: "SomeService1")
        XCTAssertEqual(dynamicSomeServices.count, 2)
        XCTAssertNotNil(dynamicSomeServices.first { $0.someString == "service0" })
        XCTAssertNotNil(dynamicSomeServices.first { $0.someString == "service1" })
    }

    func test_dynamicAllInjectionReplacement() throws {
        try DependencyContext.register(SharedProvider(value: SomeService("service0")), keyIdentifier: "SomeService0")
        try DependencyContext.register(SharedProvider(value: SomeService("service1")), keyIdentifier: "SomeService1")
        XCTAssertEqual(dynamicSomeServices.count, 2)
        XCTAssertNotNil(dynamicSomeServices.first { $0.someString == "service0" })
        XCTAssertNotNil(dynamicSomeServices.first { $0.someString == "service1" })

        DependencyContext.remove(for: "SomeService1")
        XCTAssertEqual(dynamicSomeServices.count, 1)
        XCTAssertEqual(dynamicSomeServices[0].someString, "service0")
    }

    func test_provideUnregisteredDynamicProperty_throwsError() throws {
        XCTAssertThrowsError(try $dynamicUnregisteredProperty.getOrCreate)
    }

    func test_registerDynamic_retrievalSuccess() throws {
        try DependencyContext.register(SharedProvider(value: SomeService("")))
        let _: SomeService = try DependencyContext.provide()
    }

    func test_registerDynamicAndRemove_retrievalFailure() throws {
        try DependencyContext.register(SharedProvider(value: SomeService("")))
        DependencyContext.remove(for: SomeService.self)
        do {
            let _: SomeService = try DependencyContext.provide()
            XCTFail("No error thrown")
        } catch let e {
            guard let error = e as? DependencyContextError else { XCTFail("Wrong error type"); return }
            guard case DependencyContextError.keyNotFound(_) = error else {
                XCTFail("Wrong error type")
                return
            }
        }
    }

    func test_overwriteKeyIdentifier_fails() throws {
        let instance = SomeService("Connection test")
        try DependencyContext.register(SharedProvider(value: instance), keyIdentifier: "instance")
        XCTAssertThrowsError(try DependencyContext.register(SharedProvider(value: instance), keyIdentifier: "instance"))
    }

    func test_connectionHandler_oneInstanceTwoProviders() throws {
        let instance = SomeService("Connection test")
        try DependencyContext.register(SharedProvider(value: instance), keyIdentifier: "instance0")
        try DependencyContext.register(SharedProvider(value: instance), keyIdentifier: "instance1")

        XCTAssertEqual(instance.referenceCounter, 1)

        DependencyContext.remove(for: "instance0")

        XCTAssertEqual(instance.referenceCounter, 1)

        DependencyContext.remove(for: "instance1")

        XCTAssertEqual(instance.referenceCounter, 0)
    }

    func test_connectionHandler_overwriteConnection_twoInstancesOneProvider() throws {
        let instance0 = SomeService("Connection test0")
        let instance1 = SomeService("Connection test1")
        DependencyContext[\.someService] = SharedProvider(value: instance0)

        XCTAssertEqual(instance0.referenceCounter, 1)
        XCTAssertEqual(instance1.referenceCounter, 0)

        DependencyContext[\.someService] = SharedProvider(value: instance1)

        XCTAssertEqual(instance0.referenceCounter, 0)
        XCTAssertEqual(instance1.referenceCounter, 1)
    }

}

public class SomeService: SharedProviderConnectionEventHandler {
    var referenceCounter = 0
    var someString: String
    init(_ string: String) {
        someString = string
    }

    public func didConnectToSharedProvider() {
        referenceCounter += 1
        print("[\(someString)] Did connect \(referenceCounter)")
    }

    public func didDisconnectFromSharedProvider() {
        referenceCounter -= 1
        print("[\(someString)] Did disconnect \(referenceCounter)")
    }
}

private var someServiceInjectionProvider: BaseInjectionProvider<SomeService> = EmptyProvider()

extension DependencyContext {
    fileprivate var someService: BaseInjectionProvider<SomeService> {
        get { someServiceInjectionProvider }
        set { someServiceInjectionProvider = newValue }
    }
}
