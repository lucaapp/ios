import UIKit
import DependencyInjection
import RxSwift

public enum FormComponentType {

    case firstName
    case lastName
    case street
    case houseNumber
    case postCode
    case city
    case phoneNumber
    case email

    var placeholder: String {
        switch self {
        case .firstName:    return L10n.IOSApp.UserData.Form.firstName
        case .lastName:     return L10n.IOSApp.UserData.Form.lastName
        case .street:       return L10n.IOSApp.UserData.Form.street
        case .houseNumber:  return L10n.IOSApp.UserData.Form.houseNumber
        case .postCode:     return L10n.IOSApp.UserData.Form.postCode
        case .city:         return L10n.IOSApp.UserData.Form.city
        case .phoneNumber:  return L10n.IOSApp.UserData.Form.phoneNumber
        case .email:        return L10n.IOSApp.UserData.Form.email
        }
    }

    func value(_ preferences: LucaPreferences) -> Single<String?> {
        switch self {
        case .firstName:    return preferences.get(\.firstName)
        case .lastName:     return preferences.get(\.lastName)
        case .street:       return preferences.get(\.street)
        case .houseNumber:  return preferences.get(\.houseNumber)
        case .postCode:     return preferences.get(\.postCode)
        case .city:         return preferences.get(\.city)
        case .phoneNumber:  return preferences.get(\.phoneNumber)
        case .email:        return preferences.get(\.email)
        }
    }

    var keyboardType: UIKeyboardType {
        print("Keyboard type for: \(self)")
        switch self {
        case .phoneNumber:  return .phonePad
        case .postCode:     return .webSearch
        case .email:        return .emailAddress
        case .lastName,
             .firstName:    return .webSearch
        default:            return .webSearch
        }
    }

    var textContentType: UITextContentType {
        switch self {
        case .firstName:    return .givenName
        case .lastName:     return .familyName
        case .street:       return .streetAddressLine1
        case .houseNumber:  return .streetAddressLine2
        case .postCode:     return .location  // .postalCode has to many restrictions e.g. letters for zip codes abroad
        case .city:         return .addressCity
        case .phoneNumber:  return .telephoneNumber
        case .email:        return .emailAddress
        }
    }

}
