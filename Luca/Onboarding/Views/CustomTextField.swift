import UIKit
import LucaUIComponents
import DependencyInjection
import RxSwift

class FormTextField: UIView {
    @InjectStatic(\.lucaPreferences) private var lucaPreferences

    var textField: LucaDefaultTextField!
    var type: FormComponentType?
    var isOptional = false

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        textField.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
    }

    init(frame: CGRect, type: FormComponentType, optional: Bool = false) {
        super.init(frame: frame)
        self.isOptional = optional
        self.type = type
        setup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }

    func setup() {
        setupTextFields()
    }

    func setupTextFields() {
        textField = LucaDefaultTextField(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
        textField.font = FontFamily.Montserrat.regular.font(size: 14)
        textField.textColor = .white
        textField.borderColor = Asset.luca747480.color
        textField.tintColor = .white

        textField.autocorrectionType = .yes
        textField.returnKeyType = .next

        if let type = type {
            textField.textContentType = type.textContentType
            textField.keyboardType = type.keyboardType
            _ = type.value(lucaPreferences)
                .observe(on: MainScheduler.asyncInstance)
                .subscribe(onSuccess: { self.set(placeholder: type.placeholder, text: $0) })
        }

        self.addSubview(textField)
    }

    func setPlaceholder(text: String, color: UIColor = .white) {
        let placeholder = isOptional ? "\(text) (optional)" : text
        textField.setPlaceholder(text: placeholder, color: color, font: FontFamily.Montserrat.regular.font(size: 14))
    }

    func setKeyboard(type: UIKeyboardType) {
        textField.keyboardType = type
    }

    func setText(_ text: String?) {
        textField.text = text ?? ""
    }

    func set(placeholder: String, text: String?) {
        setPlaceholder(text: placeholder)
        setText(text)
    }

    func setErrorMode() {
        textField.borderColor = Asset.lucaError.color
    }

    func setDefaultMode() {
        textField.borderColor = Asset.luca747480.color
    }

    func set(_ textContentType: UITextContentType, autocorrection: UITextAutocorrectionType = .default) {
        textField.set(textContentType, autocorrection: autocorrection)
    }
}
