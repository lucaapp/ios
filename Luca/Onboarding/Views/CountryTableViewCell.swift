import UIKit
import LucaUIComponents
import RxSwift

class CountryTableViewCell: UITableViewCell {

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 5

        return stackView
    }()

    private lazy var titleLabel: UILabel = {
        let titleLabel = LucaAccountTitleLabel()
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping

        return titleLabel
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupUI() {
        backgroundColor = Asset.lucaButtonBlack.color
        let bgColorView = UIView()
        bgColorView.backgroundColor = Asset.lucaWhiteLowAlpha.color
        selectedBackgroundView = bgColorView

        contentView.addSubview(stackView)
        setupStackView()
    }

    private func setupStackView() {
        stackView.addArrangedSubview(titleLabel)

        stackView.setAnchor(top: contentView.topAnchor,
                            leading: contentView.leadingAnchor,
                            bottom: contentView.bottomAnchor,
                            trailing: contentView.trailingAnchor,
                            padding: .init(top: 16, left: 16, bottom: 16, right: 16))
    }

    func configureTitle(_ title: String, accessibilityTitle: String? = nil) {
        titleLabel.text = title
        titleLabel.accessibilityLabel = accessibilityTitle ?? title
    }
}
