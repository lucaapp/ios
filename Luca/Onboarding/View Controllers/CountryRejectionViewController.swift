import UIKit
import Nantes

class CountryRejectionViewController: UIViewController {

    @IBOutlet weak var descriptionLabel: NantesLabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        descriptionLabel.buildTappableLabel(
            linkDescription: L10n.IOSApp.CountryRejectionViewController.description,
            linkTerm: L10n.IOSApp.CountryRejectionViewController.term,
            linkURL: L10n.IOSApp.CountryRejectionViewController.link,
            delegate: self)
    }
}

extension CountryRejectionViewController: NantesLabelDelegate {
    func attributedLabel(_ label: NantesLabel, didSelectLink link: URL) {
        UIApplication.shared.open(link, options: [:], completionHandler: nil)
    }
}
