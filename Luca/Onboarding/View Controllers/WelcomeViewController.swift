import UIKit
import SimpleCheckbox
import Nantes
import DependencyInjection
import RxSwift
import LucaUIComponents

class WelcomeViewController: UIViewController {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.lucaIDTermsOfUse) private var lucaIDToS

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var termsAndConditionsCheckbox: Checkbox!
    @IBOutlet weak var termsAndConditionsCheckboxError: UIImageView!
    @IBOutlet weak var termsAndConditionsTextView: NantesLabel!
    @IBOutlet weak var privacyPolicyTextView: NantesLabel!
    @IBOutlet weak var countryLabel: UILabel!

    @IBOutlet weak var countrySelectionView: UIView!
    @IBOutlet weak var okButton: LightStandardButton!

    @IBOutlet weak var logoImageView: UIImageView!

    var defaultCountryLocale: CountryLocale?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Sometimes it won't be picked up, although in IB set correctly
        logoImageView.image = logoImageView.image?.withRenderingMode(.alwaysTemplate)
        logoImageView.tintColor = UIColor.white

        setupCheckbox(termsAndConditionsCheckbox, accessibilityLabel: L10n.IOSApp.WelcomeViewController.TermsAndConditions.checkboxAccessibility)

        setupCheckboxError(termsAndConditionsCheckboxError)

        setupCountryView()
        setupDefaultLocale()

        okButton.layer.cornerRadius = okButton.frame.size.height / 2

        privacyPolicyTextView.buildTappableLabel(
            linkDescription: L10n.IOSApp.WelcomeViewController.PrivacyPolicy.message,
            linkTerm: L10n.IOSApp.WelcomeViewController.termPrivacyPolicy,
            linkURL: L10n.IOSApp.WelcomeViewController.linkPrivacyPolicy,
            delegate: self)

        termsAndConditionsTextView.buildTappableLabel(
            linkDescription: L10n.IOSApp.WelcomeViewController.TermsAndConditions.checkboxMessage,
            linkTerm: L10n.IOSApp.WelcomeViewController.termTC,
            linkURL: L10n.IOSApp.WelcomeViewController.linkTC,
            delegate: self)
    }

    private func setupCheckbox(_ checkbox: Checkbox, accessibilityLabel: String) {
        checkbox.lucaStyle()

        checkbox.accessibilityLabel = accessibilityLabel
        checkbox.isAccessibilityElement = true
    }

    private func setupCheckboxError(_ errorImage: UIImageView) {
        let image = UIImage(cgImage: #imageLiteral(resourceName: "infoIcon").cgImage!, scale: #imageLiteral(resourceName: "infoIcon").scale, orientation: .down)
        errorImage.image = image.withRenderingMode(.alwaysTemplate)
        errorImage.tintColor = Asset.lucaError.color
        errorImage.isHidden = true
    }

    private func setupCountryView() {
        countrySelectionView.layer.cornerRadius = 8
        countrySelectionView.layer.borderWidth = 1
        countrySelectionView.layer.borderColor = Asset.luca747480.color.cgColor

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(countrySelectionPressed(_:)))
        countrySelectionView.addGestureRecognizer(tapGesture)
    }

    private func setupDefaultLocale() {
        // DACH countries display Germany as default country, all others get Afganistan
        let regionCode = ["DE", "CH", "AT"].contains(NSLocale.current.regionCode) ? "DE" : "AF"

        if let displayName = Locale.current.localizedString(forRegionCode: regionCode) {
            defaultCountryLocale = CountryLocale(regionCode: regionCode, displayName: displayName)
            countryLabel.text = displayName
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupAccessibility()
    }

    @IBAction func termsAndConditionsPressed(_ sender: Checkbox) {
        sender.removeErrorStyling()
        termsAndConditionsCheckboxError.isHidden = true
        let accessibilityValueConfirmed = L10n.IOSApp.WelcomeViewController.TermsAndConditions.Checkbox.confirmed
        let accessibilityValueNotConfirmed = L10n.IOSApp.WelcomeViewController.TermsAndConditions.Checkbox.notConfirmed
        termsAndConditionsCheckbox.accessibilityValue = sender.isChecked ? accessibilityValueConfirmed : accessibilityValueNotConfirmed
    }

    @IBAction func privacyPolicyLinkClicked(_ sender: Any) {
        guard let url = URL(string: L10n.IOSApp.WelcomeViewController.linkPrivacyPolicy) else {return}
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    @IBAction func termsAndConditionsClicked(_ sender: Any) {
        guard let url = URL(string: L10n.IOSApp.WelcomeViewController.linkTC) else {return}
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }

    @objc func countrySelectionPressed(_ gesture: UITapGestureRecognizer) {
        let viewController = ViewControllerFactory.Onboarding.createCountrySelectionViewController(defaultCountryLocale: defaultCountryLocale, delegate: self)
        present(viewController, animated: true, completion: nil)
    }

    @IBAction func onOkButton(_ sender: UIButton) {
        guard validateCheckboxes() else {
            highlightUncheckedCheckboxes()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                UIAccessibility.post(notification: .announcement, argument: L10n.IOSApp.Welcome.Checkboxes.accessibilityError)
            }
            return
        }

        guard let defaultCountryLocale = defaultCountryLocale else {
            countryLabel.text = L10n.IOSApp.Welcome.Country.title
            return
        }

        if defaultCountryLocale.regionCode == "DE" {
            _ = lucaIDToS.setAsAccepted()
                .andThen(self.lucaPreferences.set(\.welcomePresented, value: true))
                .handleErrors(self)
                .subscribe(onCompleted: {
                    DispatchQueue.main.async { self.dismiss(animated: true, completion: nil) }
                })
        } else {
            let rejectionViewController = ViewControllerFactory.Onboarding.createCountryRejectionViewController()
            rejectionViewController.modalPresentationStyle = .fullScreen
            present(rejectionViewController, animated: true, completion: nil)
        }
    }

    private func validateCheckboxes() -> Bool {
        termsAndConditionsCheckbox.isChecked
    }

    private func highlightUncheckedCheckboxes() {
        if !termsAndConditionsCheckbox.isChecked {
            termsAndConditionsCheckbox.styleForError()
            termsAndConditionsCheckboxError.isHidden = false
        }
    }
}

extension WelcomeViewController: NantesLabelDelegate {
    func attributedLabel(_ label: NantesLabel, didSelectLink link: URL) {
        UIApplication.shared.open(link, options: [:], completionHandler: nil)
    }
}

extension WelcomeViewController: CountrySelectionViewControllerDelegate {
    func didSelectCountry(country: CountryLocale) {
        defaultCountryLocale = country
        countryLabel.text = country.displayName
    }
}

extension WelcomeViewController: UnsafeAddress, LogUtil {}

extension Checkbox {
    fileprivate func styleForError() {
        uncheckedBorderColor = Asset.lucaError.color
        setNeedsDisplay()
    }

    fileprivate func removeErrorStyling() {
        uncheckedBorderColor = .white
    }

    func lucaStyle() {
        checkedBorderColor = .white
        uncheckedBorderColor = .white
        borderStyle = .square
        borderLineWidth = 1
        borderCornerRadius = 2

        checkmarkStyle = .circle
        checkmarkColor = UIColor.white
    }
}

// MARK: - Accessibility
extension WelcomeViewController {

    private func setupAccessibility() {
        titleLabel.accessibilityTraits = .header

        self.view.accessibilityElements = [titleLabel, descriptionLabel, privacyPolicyTextView, countryLabel, termsAndConditionsCheckbox, termsAndConditionsTextView, okButton].map { $0 as Any }

        termsAndConditionsCheckbox.accessibilityValue = termsAndConditionsCheckbox.isChecked ?
            L10n.IOSApp.WelcomeViewController.TermsAndConditions.Checkbox.confirmed : L10n.IOSApp.WelcomeViewController.TermsAndConditions.Checkbox.notConfirmed

        UIAccessibility.setFocusTo(titleLabel, notification: .layoutChanged, delay: 0.8)
    }

}
