import UIKit
import DependencyInjection
import RxSwift

class DoneViewController: UIViewController, HandlesLucaErrors {
    @InjectStatic(\.lucaPreferences) private var lucaPreferences

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!

    var disposeBag: DisposeBag?

    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionLabel.text = L10n.IOSApp.Done.description
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupAccessibility()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposeBag = nil
    }

    @IBAction func okayPressed(_ sender: UIButton) {
        Task {
            try? await lucaPreferences.set(\.donePresented, value: true).task()

            // don't show new terms to new users
            if let currentBuildVersion = Bundle.main.buildVersionNumber,
               let version = Int(currentBuildVersion) {
                try? await lucaPreferences.set(\.termsAcceptedVersion, value: version).task()
            }
        }

        let newDisposeBag = DisposeBag()

        NotificationPermissionHandler.shared.requestAuthorizationRx()
            .handleErrors(self)
            .catch { _ in .just(false) }
            .flatMapCompletable { authorized in
                if authorized { return .empty() }
                return UIAlertController.goToApplicationSettingsRx(
                    viewController: self,
                    title: L10n.IOSApp.Notification.Permission.title,
                    message: L10n.IOSApp.Notification.Permission.description
                )
                .ignoreElementsAsCompletable()
            }
            .subscribe(onCompleted: {
                DispatchQueue.main.async { self.dismiss(animated: true, completion: nil) }
            })
            .disposed(by: newDisposeBag)
        disposeBag = newDisposeBag
    }

}
extension DoneViewController {

    private func setupAccessibility() {
        titleLabel.accessibilityTraits = .header
        UIAccessibility.setFocusTo(titleLabel, notification: .screenChanged, delay: 0.8)
    }

}
