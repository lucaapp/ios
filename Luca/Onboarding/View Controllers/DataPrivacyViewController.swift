import UIKit
import DependencyInjection
import LucaUIComponents

class DataPrivacyViewController: UIViewController {
    @InjectStatic(\.lucaPreferences) private var lucaPreferences

    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var gotItButton: LightStandardButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    func setupViews() {
        setupAccessibility()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        gotItButton.layer.cornerRadius = gotItButton.frame.size.height / 2
    }

    @IBAction func gotItButtonPressed(_ sender: UIButton) {
        _ = lucaPreferences.set(\.dataPrivacyPresented, value: true).subscribe()
        self.dismiss(animated: true, completion: nil)
    }

}

// MARK: - Accessibility
extension DataPrivacyViewController {

    private func setupAccessibility() {
        descriptionTitle.accessibilityTraits = .header
        UIAccessibility.setFocusTo(descriptionTitle, notification: .layoutChanged, delay: 0.8)
    }

}
