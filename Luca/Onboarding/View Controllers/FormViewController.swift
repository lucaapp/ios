import UIKit
import JGProgressHUD
import DependencyInjection
import RxSwift
import LucaUIComponents

class FormViewController: UIViewController, HandlesLucaErrors {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.userService) private var userService

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nextButton: LightStandardButton!
    @IBOutlet weak var formView: FormView!
    @IBOutlet weak var progressBar: UIProgressView!

    private var progressHud = JGProgressHUD.lucaLoading()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Task {
            do {
                try await reloadViews()
            } catch let error {
                processErrorMessages(error: error)
            }
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        nextButton.layer.cornerRadius = nextButton.frame.size.height / 2
        for field in formView.textFields {
            var fieldFrame = field.frame
            fieldFrame.size.width = formView.frame.size.width
            field.frame = fieldFrame
        }
    }

    @MainActor
    func reloadViews() async throws {
        let step = OnboardingStep(rawValue: try await lucaPreferences.get(\.currentOnboardingPage).task()) ?? .name
        formView.setup(step: step)
        _ = formView.textFields.map { [weak self] in $0.textField.delegate = self }
        nextButton.setTitle(step.buttonTitle, for: .normal)
        titleLabel.text = step.formTitle
        progressBar.progress = step.progress
        setupAccessibility()
    }

    @IBAction func nextButtonPressed(_ sender: UIButton) {
        showNextPage()
    }

    @IBAction func rightSwiped(_ sender: UISwipeGestureRecognizer) {
        showPreviousPage()
    }

    @IBAction func leftSwiped(_ sender: UISwipeGestureRecognizer) {
        showNextPage()
    }

    func showNextPage() {
        self.hideKeyboard()
        Task { @MainActor in

            guard let page = try? await lucaPreferences.get(\.currentOnboardingPage).task(),
                  let pageEnum = OnboardingStep(rawValue: page) else {
                print("No eligible current page!")
                return
            }
            if formView.textFieldsFilledOut {
                UIAccessibility.setFocusTo(titleLabel, notification: .screenChanged, delay: 0.8)
                if pageEnum == .phoneNumber {
                    let enteredPhoneNumberCharacterCount = (try? await lucaPreferences.get(\.phoneNumber).task())?.count ?? 0
                    let userEnteredPhoneNumber = (enteredPhoneNumberCharacterCount > 0)
                    let isEnteredPhoneNumberVerified = (try? await lucaPreferences.get(\.phoneNumberVerified).task()) ?? false
                    if userEnteredPhoneNumber && !isEnteredPhoneNumberVerified {
                        verifyPhoneNumber()
                    } else {
                        try? await lucaPreferences.set(\.currentOnboardingPage, value: page + 1).task()
                        try await reloadViews()
                    }
                } else if page + 1 <= 2 {
                    try? await lucaPreferences.set(\.currentOnboardingPage, value: page + 1).task()
                    try await reloadViews()
                } else {
                    registerUser()
                }
            } else {
                formView.showErrorStatesForEmptyFields()
            }
        }
    }

    func verifyPhoneNumber() {
        Task {
            let phoneNumberVerificationService = PhoneNumberVerificationService(presenting: self)

            guard let phoneNumber = try? await lucaPreferences.get(\.phoneNumber).task() else { return }

            _ = phoneNumberVerificationService.verify(phoneNumber: phoneNumber)
                .handleErrors(self)
                .observe(on: MainScheduler.asyncInstance)
                .subscribe(onCompleted: { [weak self] in
                    self?.showNextPage()
                })
        }
    }

    func showPreviousPage() {
        Task { @MainActor in
            let page = (try? await lucaPreferences.get(\.currentOnboardingPage).task()) ?? 0
            if page - 1 >= 0 {
                try? await lucaPreferences.set(\.currentOnboardingPage, value: page - 1).task()
                try? await reloadViews()
            }
        }
    }

    func registerUser() {
        _ = lucaPreferences.get(\.userRegistrationData)
            .observe(on: MainScheduler.asyncInstance)
            .flatMapCompletable { userData in
                guard userData != nil else {
                    throw PrintableError(
                        title: L10n.IOSApp.Navigation.Basic.error,
                        message: L10n.IOSApp.FormViewController.UserDataFailure.Unavailable.message
                    )
                }

                self.progressBar.progress = 1.0
                self.progressHud.show(in: self.view)
                return self.userService.registerIfNeeded().asCompletable()
            }
            .handleErrors(self)
            .observe(on: MainScheduler.asyncInstance)
            .do(onCompleted: {
                self.dismiss(animated: true, completion: nil)
            })
            .do(onDispose: {
                self.progressHud.dismiss()
            })
            .subscribe()
    }

    @IBAction func viewTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

}
extension FormViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let textField = textField as? LucaDefaultTextField {
            textField.setPlaceholder(color: Asset.luca747480.color, font: FontFamily.Montserrat.regular.font(size: 14))
        }
        formView.showNormalStatesForEmptyFields()
    }

    func textFieldDidEndEditing(_ textField: UITextField) {

        guard let formTextField = self.formView.textFields.first(where: { $0.textField == textField }),
              let type = formTextField.type,
              let text = textField.text?.sanitize() else {
            return
        }

        if let textField = textField as? LucaDefaultTextField,
           text.isEmpty {
            textField.setPlaceholder(color: .white, font: FontFamily.Montserrat.regular.font(size: 14))
        }
        Task {
            switch type {
            case .firstName:    try? await lucaPreferences.set(\.firstName, value: text).task()
            case .lastName:     try? await lucaPreferences.set(\.lastName, value: text).task()
            case .street:       try? await lucaPreferences.set(\.street, value: text).task()
            case .houseNumber:  try? await lucaPreferences.set(\.houseNumber, value: text).task()
            case .postCode:     try? await lucaPreferences.set(\.postCode, value: text).task()
            case .city:         try? await lucaPreferences.set(\.city, value: text).task()
            case .email:        try? await lucaPreferences.set(\.email, value: text).task()
            case .phoneNumber:
                try? await lucaPreferences.set(\.phoneNumber, value: text).task()
                try? await lucaPreferences.set(\.phoneNumberVerified, value: false).task()
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var tag = 0
        // Find first responder manually, since textField returns a tag of 0 for every textfield since we use FormTextField
        for view in formView.textFields where view.textField.isFirstResponder {
            tag = view.tag
        }

        if tag == formView.textFields.count - 1 {
            showNextPage()
        } else if let nextField = view.superview?.viewWithTag(textField.tag + 1) as? FormTextField {
            nextField.textField.becomeFirstResponder()
        }
        return false
    }

}

// MARK: - Accessibility
extension FormViewController {

    private func setupAccessibility() {
        titleLabel.accessibilityTraits = .header
        UIAccessibility.setFocusTo(titleLabel, notification: .layoutChanged)
    }

}
