import UIKit
import LucaUIComponents

struct CountryGroup {
    var section: String
    var countries: [CountryLocale]
}

struct CountryLocale {
    var regionCode: String
    var displayName: String
}

protocol CountrySelectionViewControllerDelegate: AnyObject {
    func didSelectCountry(country: CountryLocale)
}

class CountrySelectionViewController: UIViewController, Themable {
    var theme: AppearanceTheme = .dark
    var closeButton: UIBarButtonItem?

    var countryGroups: [CountryGroup]!
    var defaultLocale: CountryLocale?

    weak var delegate: CountrySelectionViewControllerDelegate?

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.backgroundColor = .black
        tableView.separatorColor = .black
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 16, right: 0)
        tableView.alwaysBounceVertical = false

        tableView.dataSource = self
        tableView.delegate = self

        tableView.register(CountryTableViewCell.self, forCellReuseIdentifier: "defaultCell")
        tableView.register(CountryTableViewHeader.self, forHeaderFooterViewReuseIdentifier: "sectionHeader")

        view.addSubview(tableView)

        return tableView
    }()

    convenience init(defaultLocale: CountryLocale?, delegate: CountrySelectionViewControllerDelegate?) {
        self.init(nibName: nil, bundle: nil)

        self.defaultLocale = defaultLocale
        self.delegate = delegate
        initCountries()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationbar()
        setupContraints()
        scrollToDefaultLocale()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.applyToViewAndChildrenControllers(theme: .dark)
    }

    private func initCountries() {
        let countryLocales = Locale.isoRegionCodes.compactMap { CountryLocale(regionCode: $0, displayName: Locale.current.localizedString(forRegionCode: $0) ?? "") }
        let groupedCountryLocales = Dictionary(grouping: countryLocales, by: {String($0.displayName.prefix(1))})
        let sortedSections = groupedCountryLocales.keys.sorted()
        countryGroups = sortedSections.map { CountryGroup(section: $0, countries: groupedCountryLocales[$0]!.sorted { $0.displayName < $1.displayName }) }
    }

    func setupNavigationbar() {
        set(title: L10n.IOSApp.Welcome.Country.title)
        closeButton = UIBarButtonItem(image: Asset.closeButton.image, style: .plain, target: self, action: #selector(closeTapped))
        navigationItem.rightBarButtonItem = closeButton
    }

    @objc func closeTapped() {
        dismiss(animated: true, completion: nil)
    }

    private func setupContraints() {
        tableView.setAnchor(top: view.topAnchor,
                            leading: view.leadingAnchor,
                            bottom: view.bottomAnchor,
                            trailing: view.trailingAnchor)
    }

    private func scrollToDefaultLocale() {
        if let defaultLocale = defaultLocale,
           let section = countryGroups.firstIndex(where: { $0.section == defaultLocale.displayName.prefix(1) }),
           let row = countryGroups[section].countries.firstIndex(where: { $0.regionCode == defaultLocale.regionCode }) {
            let indexPath = IndexPath(row: row, section: section)
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .top)
        }
    }
}

extension CountrySelectionViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return countryGroups.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countryGroups[section].countries.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let locale = countryGroups[indexPath.section].countries[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "defaultCell", for: indexPath) as? CountryTableViewCell

        cell?.configureTitle(locale.displayName)

        return cell!
    }

}

extension CountrySelectionViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let locale = countryGroups[indexPath.section].countries[indexPath.row]
        delegate?.didSelectCountry(country: locale)
        dismiss(animated: true)
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "sectionHeader") as? CountryTableViewHeader
        view?.label.text = countryGroups[section].section
        return view
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cornerRadius = 10
        var corners: UIRectCorner = []

        if indexPath.row == 0 {
            corners.update(with: .topLeft)
            corners.update(with: .topRight)
        }

        if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            corners.update(with: .bottomLeft)
            corners.update(with: .bottomRight)
        }

        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: cell.bounds,
                                      byRoundingCorners: corners,
                                      cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
        cell.layer.mask = maskLayer
        (cell as? CountryTableViewCell)?.applyToSubviews(theme: theme)
    }
}
