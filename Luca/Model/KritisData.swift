import Foundation

public class KritisData: Codable {

    var criticalInfrastructure: Bool?
    var vulnerableGroup: Bool?
    var industry: String?
    var company: String?

}
