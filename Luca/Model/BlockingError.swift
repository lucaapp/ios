import Foundation

/// Describes an symbolic error that would block current UI.
/// For example when the app version is outdated and the app must not be used anymore.
/// Should be used only in the uttermost need.
protocol BlockingError: LocalizedTitledError {
}
