import UIKit
import RxSwift
import MessageUI
import DeviceKit

class SendMailInteractor: NSObject, Interactor {
    private weak var presenter: UIViewController?
    private var recipients: String
    private var subject: String
    private var messageBody: String

    init(presenter: UIViewController, recipients: String, subject: String, messageBody: String) {
        self.presenter = presenter
        self.recipients = recipients
        self.subject = subject
        self.messageBody = messageBody
    }

    func interact() -> Completable {
        guard let presenter = presenter else { return .empty() }
        return sendMail(presenter: presenter, recipients: recipients, subject: subject, messageBody: messageBody)
    }

    private func sendMail(presenter: UIViewController, recipients: String, subject: String, messageBody: String) -> Completable {
        Single<MFMailComposeViewController>.from {
            if !MFMailComposeViewController.canSendMail() {
                throw RxMFMailComposeError.mailNotConfigured
            }
            let mail = MFMailComposeViewController()
            mail.setToRecipients([recipients])
            mail.setSubject(subject)
            mail.setMessageBody(messageBody, isHTML: true)
            presenter.present(mail, animated: true)
            return mail
        }
        .flatMapCompletable({ (mail: MFMailComposeViewController) in
            return mail.rx.sendMail.subscribe(on: MainScheduler.instance)
        })
    }
}
