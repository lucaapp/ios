import Foundation
import UIKit
import RxSwift
import DependencyInjection

class HealthDepartmentCryptoInfoViewController: UIViewController {

    @InjectStatic(\.backendDailyKeyV3) private var backendDailyKey
    @InjectStatic(\.dailyKeyRepository) private var dailyKeyRepository
    @InjectStatic(\.dailyPublicKeyVerifier) private var dailyKeyVerifier

    @IBOutlet weak var downloadCertificateChain: UIButton!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!

    /// Primitive name caching
    private static var issuerName: String?
    private static var date: String?
    private static var dateOfCache: Date?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Default values
        nameLabel.text = "-"
        dateLabel.text = ""

        _ = dailyKeyVerifier.isKeyValid
            .do(onNext: { isKeyValid in
                if isKeyValid {
                    self.setData()
                } else {
                    self.nameLabel.text = "-"
                    self.dateLabel.text = ""
                }
            })
            .asObservable()
            .take(until: self.rx.viewWillDisappear)
            .subscribe()

        setupAccessibility()
    }

    private func setData() {
        // Cached values
        if let issuerName = HealthDepartmentCryptoInfoViewController.issuerName,
           let date = HealthDepartmentCryptoInfoViewController.date,
           let dateOfCache = HealthDepartmentCryptoInfoViewController.dateOfCache,
           Calendar.current.isDateInToday(dateOfCache) {
            nameLabel.text = issuerName
            dateLabel.text = date
            return
        }

        // Generating new values
        if let newestKeyId: DailyKeyIndex = dailyKeyRepository.newestId {

            // Date string
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy"
            let dateString = dateFormatter.string(from: newestKeyId.createdAt)
            HealthDepartmentCryptoInfoViewController.date = dateString
            HealthDepartmentCryptoInfoViewController.dateOfCache = Date.now
            dateLabel.text = dateString

            // Issuer name
            _ = backendDailyKey.retrievePubKey(keyId: newestKeyId.keyId)
                .asSingle()
                .flatMap { self.backendDailyKey.retrieveIssuerKeys(issuerId: $0.issuerId).asSingle() }
                .observe(on: MainScheduler.instance)
                .do(onSuccess: { key in
                    HealthDepartmentCryptoInfoViewController.issuerName = key.name
                    self.nameLabel.text = key.name
                })
                .subscribe()
        }
    }

    private func setupViews() {
        // Setup navigationbar title
        title = L10n.IOSApp.General.healthDepartmentKey
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.montserratViewControllerTitle,
                                                                        NSAttributedString.Key.foregroundColor: UIColor.white]

        // Setup sublabel
        subtitleLabel.text = L10n.IOSApp.General.healthDepartmentKey
    }
}

// MARK: - Accessibility
extension HealthDepartmentCryptoInfoViewController {

    private func setupAccessibility() {
        UIAccessibility.setFocusTo(subtitleLabel, notification: .screenChanged, delay: 0.8)
    }

}
