import UIKit
import RxSwift
import LucaUIComponents
import RxCocoa
import RxAppState
import Nantes

struct NantesLabelData {
    var text: String
    var linkText: String?
    var url: String?
}

class ConsentViewController: UIViewController, LucaModalAppearance {

    private var consentTitle: String
    private var consentButtonTitle: String
    private var consentDescription: [NantesLabelData]
    private var backgroundColor: UIColor?

    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isPagingEnabled = true
        scrollView.bounces = false

        return scrollView
    }()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        stackView.spacing = 32

        return stackView
    }()

    private lazy var titleLabel: UILabel = {
        let titleLabel = Luca20PtBoldLabel()
        titleLabel.text = consentTitle
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.lineBreakMode = .byWordWrapping

        return titleLabel
    }()

    private lazy var button: UIButton = {
        let button = LightStandardButton()
        button.setTitle(consentButtonTitle, for: .normal)

        return button
    }()

    /// Emits `true` if accept button was clicked, `false` if the view controller has been dismissed.
    var result: Single<Bool> {
        self.loadViewIfNeeded()

        return Observable<Bool>.merge(
            button.rx.tap
                .map { _ in true },
            rx.viewDidDisappear.map { _ in false }
        )
        .take(1)
        .asSingle()
    }

    init(consentTitle: String, consentButtonTitle: String, description: [NantesLabelData], backgroundColor: UIColor?) {
        self.consentTitle = consentTitle
        self.consentButtonTitle = consentButtonTitle
        self.consentDescription = description
        self.backgroundColor = backgroundColor
        super.init(nibName: nil, bundle: nil)

        setupUI()
        setupConstraints()

        setupStackView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: L10n.IOSApp.Navigation.Basic.cancel, style: .plain, target: self, action: #selector(cancelTapped))
        applyColors()
    }

    private func setupUI() {
        view.backgroundColor = backgroundColor ?? .black

        view.addSubview(scrollView)
        view.addSubview(button)
        scrollView.addSubview(stackView)
    }

    private func setupStackView() {
        stackView.addArrangedSubview(titleLabel)

        for nantes in consentDescription {
            let nantesLabel = NantesLabel()
            nantesLabel.font = UIFont.montserratDataAccessAlertDescription

            if let linkText = nantes.linkText, let url = nantes.url {
                nantesLabel.buildTappableLabel(linkDescription: nantes.text, linkTerm: linkText, linkURL: url, delegate: self)
            } else {
                nantesLabel.numberOfLines = 0
                nantesLabel.text = nantes.text
            }
            stackView.addArrangedSubview(nantesLabel)
        }
    }

    private func setupConstraints() {
        scrollView.setAnchor(top: view.topAnchor,
                             leading: view.leadingAnchor,
                             bottom: nil,
                             trailing: view.trailingAnchor,
                             padding: UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16))

        stackView.pinToSuperView()
        stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true

        button.setAnchor(top: scrollView.bottomAnchor,
                         leading: scrollView.leadingAnchor,
                         bottom: view.bottomAnchor,
                         trailing: scrollView.trailingAnchor,
                         padding: UIEdgeInsets(top: 16, left: 16, bottom: 32, right: 16))
    }

    @objc private func cancelTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}

extension ConsentViewController: NantesLabelDelegate {

    func attributedLabel(_ label: NantesLabel, didSelectLink link: URL) {
        UIApplication.shared.open(link, options: [:], completionHandler: nil)
    }

}
