import UIKit
import JGProgressHUD
import RxSwift
import DependencyInjection
import LucaUIComponents

enum ContactViewControllerError: SilentError {
    case alreadyAsked
}

class ContactViewController: UIViewController, HandlesLucaErrors {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.documentProcessingService) private var documentProcessingService
    @InjectStatic(\.userService) private var userService
    @InjectStatic(\.lucaConnectContactDataService) private var lucaConnectContactData

    @IBOutlet weak var firstNameTextField: LucaDefaultTextField!
    @IBOutlet weak var lastNameTextField: LucaDefaultTextField!
    @IBOutlet weak var emailTextField: LucaDefaultTextField!
    @IBOutlet weak var addressStreetTextField: LucaDefaultTextField!
    @IBOutlet weak var addressHouseNumberTextField: LucaDefaultTextField!
    @IBOutlet weak var addressPostCodeTextField: LucaDefaultTextField!
    @IBOutlet weak var addressCityTextField: LucaDefaultTextField!
    @IBOutlet weak var phoneNumberTextField: LucaDefaultTextField!

    private var progressHud = JGProgressHUD.lucaLoading()

    private var saveButton: UIBarButtonItem!

    var currentData: UserRegistrationData! = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        saveButton = UIBarButtonItem(title: L10n.IOSApp.ContactViewController.save, style: .done, target: self, action: #selector(onSaveButton(_:)))

        firstNameTextField.delegate = self
        firstNameTextField.set(.givenName)

        lastNameTextField.delegate = self
        lastNameTextField.set(.familyName)

        emailTextField.delegate = self
        emailTextField.set(.emailAddress)

        addressStreetTextField.delegate = self
        addressStreetTextField.set(.streetAddressLine1)

        addressHouseNumberTextField.delegate = self
        addressHouseNumberTextField.set(.streetAddressLine2)

        addressPostCodeTextField.delegate = self
        addressPostCodeTextField.set(.postalCode)

        phoneNumberTextField.delegate = self
        phoneNumberTextField.set(.telephoneNumber)

        addressCityTextField.delegate = self
        addressCityTextField.set(.addressCity)

        Task {
            if (try? await lucaPreferences.get(\.phoneNumberVerified).observe(on: MainScheduler.asyncInstance).task()) == false {
                navigationItem.rightBarButtonItem = self.saveButton
            }
        }

        setupNavigationbar()
    }

    func setupNavigationbar() {
        // Setup navigationbar title
        set(title: L10n.IOSApp.UserData.Navigation.title)
    }

    func setupViews() {

        firstNameTextField.setPlaceholder(text: L10n.IOSApp.UserData.Form.firstName, color: Asset.luca747480.color, font: FontFamily.Montserrat.medium.font(size: 14))
        firstNameTextField.text = currentData.firstName

        lastNameTextField.setPlaceholder(text: L10n.IOSApp.UserData.Form.lastName, color: Asset.luca747480.color, font: FontFamily.Montserrat.medium.font(size: 14))
        lastNameTextField.text = currentData.lastName

        emailTextField.setPlaceholder(text: L10n.IOSApp.UserData.Form.email, color: Asset.luca747480.color, font: FontFamily.Montserrat.medium.font(size: 14))
        emailTextField.text = currentData.email

        addressStreetTextField.setPlaceholder(text: L10n.IOSApp.UserData.Form.street, color: Asset.luca747480.color, font: FontFamily.Montserrat.medium.font(size: 14))
        addressStreetTextField.text = currentData.street

        addressHouseNumberTextField.setPlaceholder(text: L10n.IOSApp.UserData.Form.houseNumber, color: Asset.luca747480.color, font: FontFamily.Montserrat.medium.font(size: 14))
        addressHouseNumberTextField.text = currentData.houseNumber

        addressPostCodeTextField.setPlaceholder(text: L10n.IOSApp.UserData.Form.postCode, color: Asset.luca747480.color, font: FontFamily.Montserrat.medium.font(size: 14))
        addressPostCodeTextField.text = currentData.postCode

        addressCityTextField.setPlaceholder(text: L10n.IOSApp.UserData.Form.city, color: Asset.luca747480.color, font: FontFamily.Montserrat.medium.font(size: 14))
        addressCityTextField.text = currentData.city

        phoneNumberTextField.setPlaceholder(text: L10n.IOSApp.UserData.Form.phoneNumber, color: Asset.luca747480.color, font: FontFamily.Montserrat.medium.font(size: 14))
        phoneNumberTextField.text = currentData.phoneNumber
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        _ = lucaPreferences.get(\.userRegistrationData)
            .observe(on: MainScheduler.asyncInstance)
            .do(onSuccess: { data in
                guard let data = data else {
                    // It doesn't have to be localized. This error should never happen.
                    // This check is there only to get rid of the optionals and data is always there after user has been registered.
                    throw PrintableError(title: "Error", message: "Local data is corrupted")
                }

                self.currentData = data
                self.setupViews()
            })
            .handleErrors(self)
            .subscribe()
        // Hide save button as the data are resetted to the last saved values
        self.navigationItem.rightBarButtonItem = nil
    }

    @IBAction func viewTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

    @IBAction func onSaveButton(_ sender: UIBarButtonItem) {
        hideKeyboard()
        guard isFormDataValid() else { return }

        _ = save()
            .do(onSubscribe: { DispatchQueue.main.async { self.progressHud.show(in: self.view) } })
            .andThen(Completable.from { self.navigationItem.rightBarButtonItem = nil }.subscribe(on: MainScheduler.instance))
            .do(onDispose: { DispatchQueue.main.async { self.progressHud.dismiss() } })
            .handleErrors(self)
            .subscribe()
    }

    func verifyPhoneNumber() -> Completable {
        Single.zip(
            lucaPreferences.get(\.phoneNumberVerified),
            Single.from { self.phoneNumberTextField.text != self.currentData.phoneNumber }.subscribe(on: MainScheduler.instance)
        )
        .observe(on: MainScheduler.instance)
        .flatMapCompletable { isPhoneNumberVerified, isPhoneNumberChanged in
            if let phoneNumber = self.phoneNumberTextField.text, phoneNumber.count > 0,
               isPhoneNumberChanged || !isPhoneNumberVerified {
                let phoneNumberVerificationService = PhoneNumberVerificationService(
                    presenting: self.tabBarController ?? self)

                return phoneNumberVerificationService.verify(phoneNumber: phoneNumber)
                    .subscribe(on: MainScheduler.instance)
            }
            return .empty()
        }
    }

    private func comparePostcode() -> Completable {
        if self.currentData.postCode != self.addressPostCodeTextField.text?.sanitize() {
            return lucaPreferences.set(\.lucaConnectNotificationSent, value: false)
        }
        return Completable.empty()
    }

    private func save() -> Completable {
        handleNameChangesIfNeeded()
        .andThen(handlePostCodeChangesIfNeeded())
        .andThen(handleOtherChanges())
        .catch(specific: ContactViewControllerError.alreadyAsked) {
            .empty()
        }
        .andThen(verifyPhoneNumber())
        .andThen(comparePostcode())
        .andThen(
            Completable.from {
                self.currentData.firstName = self.firstNameTextField.text?.sanitize()
                self.currentData.lastName = self.lastNameTextField.text?.sanitize()
                self.currentData.street = self.addressStreetTextField.text?.sanitize()
                self.currentData.houseNumber = self.addressHouseNumberTextField.text?.sanitize()
                self.currentData.postCode = self.addressPostCodeTextField.text?.sanitize()
                self.currentData.city = self.addressCityTextField.text?.sanitize()
                self.currentData.phoneNumber = self.phoneNumberTextField.text?.sanitize()
                self.currentData.email = self.emailTextField.text?.sanitize()
            }
            .subscribe(on: MainScheduler.instance)
            .andThen(userService.update(data: currentData))
            .andThen(documentProcessingService.revalidateSavedTests())
        )
    }

    /// Asks user if he wants to continue. Emits an error if user declines
    private func handleOtherChanges() -> Completable {
        UIAlertController.okAndCancelAlertRx(
            viewController: self,
            title: L10n.IOSApp.ContactViewController.ShouldSave.title,
            message: L10n.IOSApp.ContactViewController.ShouldSave.message,
            okTitle: L10n.IOSApp.Navigation.Basic.continue
        )
        .do(onSuccess: { if !$0 { throw SilentErrorDefaults.userDeclined } })
        .asCompletable()
    }

    /// It checks if post code changed and whether the luca connect is enabled. If so, an appriopriate alert will be shown and if user accepts, luca connect will be disabled.
    private func handlePostCodeChangesIfNeeded() -> Completable {
        Single.zip(
            self.lucaConnectContactData.isFeatureEnabled().take(1).asSingle(),
            Single.from {
                self.currentData.postCode != self.addressPostCodeTextField.text?.sanitize()
            }.subscribe(on: MainScheduler.instance)
        )
        .flatMapCompletable { isFeatureEnabled, isPostCodeChanged in
            if isPostCodeChanged && isFeatureEnabled {
                return UIAlertController.okAndCancelAlertRx(
                    viewController: self,
                    title: L10n.IOSApp.ContactViewController.ChangedPostCode.title,
                    message: L10n.IOSApp.ContactViewController.ChangedPostCode.message,
                    okTitle: L10n.IOSApp.ContactViewController.ChangedPostCode.action
                )
                .do(onSuccess: { if !$0 { throw SilentErrorDefaults.userDeclined } })
                .flatMapCompletable { _ in self.lucaConnectContactData.disableFeature() }
                .andThen(Completable.error(ContactViewControllerError.alreadyAsked))
            }
            return .empty()
        }
    }

    /// It checks if names changed and whether the luca connect is enabled. If so, an appriopriate alert will be shown and if user accepts, luca connect will be disabled.
    private func handleNameChangesIfNeeded() -> Completable {
        Single.zip(
            self.lucaConnectContactData.isFeatureEnabled().take(1).asSingle(),
            Single.from {
                self.currentData.firstName != self.firstNameTextField.text?.sanitize()
            }.subscribe(on: MainScheduler.instance),
            Single.from {
                self.currentData.lastName != self.lastNameTextField.text?.sanitize()
            }.subscribe(on: MainScheduler.instance)
        )
        .flatMapCompletable { isFeatureEnabled, isFirstNameChanged, isLastNameChanged in
            if isFeatureEnabled && (isFirstNameChanged || isLastNameChanged) {
                return UIAlertController.okAndCancelAlertRx(
                    viewController: self,
                    title: L10n.IOSApp.ContactViewController.ChangedName.title,
                    message: L10n.IOSApp.ContactViewController.ChangedName.message,
                    okTitle: L10n.IOSApp.ContactViewController.ChangedName.action
                )
                .do(onSuccess: { if !$0 { throw SilentErrorDefaults.userDeclined } })
                .flatMapCompletable { _ in self.lucaConnectContactData.disableFeature() }
                .andThen(Completable.error(ContactViewControllerError.alreadyAsked))
            }
            return .empty()
        }
    }

    private func isFormDataValid() -> Bool {
        let isNameFieldEmpty = firstNameTextField.isTextEmpty || lastNameTextField.isTextEmpty
        if isNameFieldEmpty {
            let alert = UIAlertController.infoAlert(title: L10n.IOSApp.Navigation.Basic.error, message: L10n.IOSApp.ContactViewController.EmptyRest.message)
            present(alert, animated: true, completion: nil)
            return false
        }
        return true
    }

}

extension ContactViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        showSaveButton()
        return true
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        showSaveButton()
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }

    private func showSaveButton() {
        if self.navigationItem.rightBarButtonItem != self.saveButton {
            self.navigationItem.rightBarButtonItem = self.saveButton
        }
    }
}

extension ContactViewController: UnsafeAddress, LogUtil {}
