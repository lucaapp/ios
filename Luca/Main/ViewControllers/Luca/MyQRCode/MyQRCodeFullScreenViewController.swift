import UIKit
import RxSwift

class MyQRCodeFullScreenViewController: UIViewController, BindableType, HandlesLucaErrors {
	weak var viewModel: MyQRCodeViewModel!
	private var disposeBag: DisposeBag = DisposeBag()
	@IBOutlet weak var qrCodeImageView: UIImageView!
	private var currentBrightness = UIScreen.main.brightness

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		UIScreen.main.brightness = CGFloat(1.0)
        setupAccessibility()
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		UIScreen.main.brightness = currentBrightness
	}

	@IBAction func dismissButtonClicked(_ sender: Any) {
		self.dismiss(animated: true, completion: nil)
	}

	func bindViewModel() {
		self.viewModel.newQRData.drive { [weak self] (image: CIImage?) in
			guard let selfRef = self,
                  let image = image else {return}
			selfRef.viewModel.setupQrImage(image: image, in: selfRef.qrCodeImageView)
		}.disposed(by: disposeBag)

		self.viewModel.errorMessageDriver.drive { [weak self] (error: PrintableError) in
			self?.processErrorMessages(error: error) {
				self?.viewModel.infoAlertDismissed.onNext(())
			}
		}.disposed(by: disposeBag)
	}
}
extension MyQRCodeFullScreenViewController {

    private func setupAccessibility() {
        qrCodeImageView.isAccessibilityElement = true
        qrCodeImageView.accessibilityLabel = L10n.IOSApp.Checkin.Qr.Title.myQRCode.string
    }

}
