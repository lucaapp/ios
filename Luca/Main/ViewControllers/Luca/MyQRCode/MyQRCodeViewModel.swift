import Foundation
import RxSwift
import UIKit
import RxCocoa
import DependencyInjection

final class MyQRCodeViewModel: LogUtil, UnsafeAddress {
	/// It will be incremented on every error and resetted on viewWillAppear. If the amount of errors surpasses the threshold, an alert will be shown.
	private var errorsCount = 0
	private static let errorsThreshold = 5

    private var ciContext = CIContext()

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.traceIdService) private var traceIdService

	// input
	var infoAlertDismissed = PublishSubject<Void>()

	// output
    var newQRData: Driver<CIImage?> {
        UIApplication.shared.rx.currentAndChangedAppState
            .flatMapLatest { appState -> Observable<Bool> in
                if appState != .active {
                    return .empty()
                }
                return self.createQRCodeSignal()
            }
            .flatMap { [weak self] in self?.handleQRCodeGeneration(shareHealthStatus: $0) ?? .just(nil) }
            .retry(delay: .seconds(1), scheduler: LucaScheduling.backgroundScheduler)
            .asDriver(onErrorJustReturn: nil)
    }
	private let errorMessagePublisher = PublishSubject<PrintableError>()
	var errorMessageDriver: Driver<PrintableError> {
		errorMessagePublisher.asDriver(onErrorJustReturn: PrintableError(error: nil, title: "", message: ""))
	}

    var shareHealthStatus: Observable<Bool> {
        Observable<Bool>.merge(
            lucaPreferences.get(\.shareHealthStatus).asObservable().map { $0 ?? false },
            lucaPreferences.changes(\.shareHealthStatus).map { $0 ?? false }
        )
        .distinctUntilChanged()
    }

	#if DEBUG
	deinit {
		print("deinit")
	}
	#endif

    /// Completes if everything went smoothly, emits an error if user didn't give his consent.
    func set(shareHealthStatus: Bool, presenter: UIViewController) -> Completable {
        ShareHealthStatusInteractor(presenter: presenter).interact(value: shareHealthStatus)
    }

    /// Emits a value when the qr code should be regenerated. The emitted value is a current value of `shareHealthStatus`
    private func createQRCodeSignal() -> Observable<Bool> {
        shareHealthStatus
            .flatMapLatest { shareHealthStatus in
            Observable<Int>.timer(.seconds(0),
                                  period: .seconds(10),
                                  scheduler: LucaScheduling.backgroundScheduler)
                .map { _ in shareHealthStatus }
        }
    }

    private func handleQRCodeGeneration(shareHealthStatus: Bool) -> Single<CIImage?> {
        traceIdService.getOrCreateQRCode(shareHealthStatus: shareHealthStatus)
			.map { $0.qrCodeData }
			.catch({ [weak self] (error) -> Single<Data> in
				guard let selfRef = self else {return .error(error)}

				defer { selfRef.errorsCount += 1 }

				if selfRef.errorsCount < MyQRCodeViewModel.errorsThreshold {
					return .error(error) // Do not consume, rely on the retry and allow the log to print this error.
				}
				let err = PrintableError(
					title: L10n.IOSApp.Navigation.Basic.error,
					message: L10n.IOSApp.QrCodeGeneration.Failure.message(error.localizedDescription)
				)
				selfRef.errorMessagePublisher.onNext(err)
				return selfRef.infoAlertDismissed
					.ignoreElementsAsCompletable()
					.andThen(.error(error))
			})
            .observe(on: MainScheduler.instance)
			.map { QRCodeGenerator.generateQRCode(data: $0) }
	}

	func setupQrImage(image: CIImage, in imageView: UIImageView) {

        let transform = CGAffineTransform(
            scaleX: imageView.frame.width / image.extent.width,
            y: imageView.frame.height / image.extent.height
        )
        let scaledQr = image.transformed(by: transform)

        if imageView.image == nil {
            imageView.alpha = 0.0
            UIView.animate(withDuration: 0.3) {
                imageView.alpha = 1.0
            }
        }
        guard let cgImage = ciContext.createCGImage(scaledQr, from: scaledQr.extent) else {
            print("Couldn't create cg image in qr code")
            return
        }
        imageView.image = UIImage(cgImage: cgImage)
	}
}
