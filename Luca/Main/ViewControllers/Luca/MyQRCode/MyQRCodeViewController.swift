import UIKit
import LucaUIComponents
import RxSwift
import DependencyInjection

class MyQRCodeViewController: UIViewController, HandlesLucaErrors, LucaModalAppearance {

    @InjectStatic(\.checkinPolling) private var checkinPolling

    @IBOutlet weak var titleLabel: Luca20PtBoldLabel!
	@IBOutlet weak var descriptionLabel: Luca14PtLabel!
    @IBOutlet weak var qrCodeImageView: UIImageView!
    @IBOutlet weak var shareHealthStatusSwitch: UISwitch!

	var viewModel: MyQRCodeViewModel!
	private var disposeBag: DisposeBag! = DisposeBag()
	@IBOutlet weak var expandButton: UIButton!

    var previousFrequency: PollingFrequency = .none

	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupViews()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		applyColors()
        previousFrequency = checkinPolling.frequency
        checkinPolling.frequency = .high

        installObservers()
	}

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        checkinPolling.frequency = previousFrequency
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        disposeBag = nil
    }

	private func setupViews() {
		self.descriptionLabel.text = L10n.IOSApp.Checkin.Qr.description
        self.titleLabel.text = L10n.IOSApp.Checkin.Qr.Title.myQRCode.string
		self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissModal))
		self.expandButton.isEnabled = false
        self.expandButton.accessibilityLabel = L10n.IOSApp.Checkin.Qr.Enlarge.accessibilty.string
        let switchState = shareHealthStatusSwitch.isOn ? L10n.IOSApp.LocationCheckinViewController.AutoCheckout.on : L10n.IOSApp.LocationCheckinViewController.AutoCheckout.off
        shareHealthStatusSwitch.accessibilityLabel = "\(L10n.IOSApp.Checkin.Qr.shareHealthStatusLabel) \(switchState)"
	}

    func installObservers() {
        disposeBag = DisposeBag()
        viewModel.newQRData.drive { [weak self] (image: CIImage?) in
            guard let selfRef = self else {return}
            selfRef.expandButton.isEnabled = image != nil
            selfRef.expandButton.accessibilityLabel = L10n.IOSApp.Checkin.Qr.Title.MyQRCode.expand
            guard let qrCodeImage = image else { return }
            selfRef.viewModel.setupQrImage(image: qrCodeImage, in: selfRef.qrCodeImageView)
        }.disposed(by: disposeBag)

        viewModel.errorMessageDriver.drive { [weak self] (error: PrintableError) in
            self?.processErrorMessages(error: error) {
                self?.viewModel.infoAlertDismissed.onNext(())
            }
        }.disposed(by: disposeBag)

        viewModel.shareHealthStatus
            .asDriver(onErrorJustReturn: false)
            .drive(shareHealthStatusSwitch.rx.isOn)
            .disposed(by: disposeBag)

        shareHealthStatusSwitch.rx.isOn
            .skip(1)
            .flatMap { newValue in
                self.viewModel.set(shareHealthStatus: newValue, presenter: self)
                    .handleErrors(self)
                    .catch { [weak self] _ in
                        guard let self = self else {return .empty()}
                        self.shareHealthStatusSwitch.isOn = !newValue
                        let switchState = self.shareHealthStatusSwitch.isOn ? L10n.IOSApp.LocationCheckinViewController.AutoCheckout.on : L10n.IOSApp.LocationCheckinViewController.AutoCheckout.off
                        self.shareHealthStatusSwitch.accessibilityLabel = "\(L10n.IOSApp.Checkin.Qr.shareHealthStatusLabel) \(switchState)"
                        return .empty()
                    }
            }
            .subscribe()
            .disposed(by: disposeBag)
    }

	@IBAction func expandButtonClicked(_ sender: Any) {
		let viewController = ViewControllerFactory.Checkin.createMyQRFullScreenViewController()
		if var vc = viewController as? MyQRCodeFullScreenViewController {
			vc.bindViewModel(to: self.viewModel)
		}
		navigationController?.present(viewController, animated: true)
	}

	@objc func dismissModal() {
		self.dismiss(animated: true, completion: nil)
	}
}

extension MyQRCodeViewController: UnsafeAddress, LogUtil {}
