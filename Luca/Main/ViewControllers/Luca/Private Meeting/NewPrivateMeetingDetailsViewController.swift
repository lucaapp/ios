import UIKit
import LucaUIComponents
import RxSwift
import RxCocoa

class NewPrivateMeetingDetailsViewController: UIViewController {
	@IBOutlet weak var startLabel: Luca14PtBoldLabel!
	@IBOutlet weak var startValueLabel: Luca14PtLabel!
	@IBOutlet weak var durationLabel: Luca14PtBoldLabel!
	@IBOutlet weak var durationValueLabel: PrivateMeetingTimerLabel!
	@IBOutlet weak var guestsLabel: Luca14PtBoldLabel!
	@IBOutlet weak var guestsStackView: UIStackView!

	private var disposeBag: DisposeBag?
	weak var viewModel: PrivateMeetingViewModel!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = L10n.IOSApp.Private.Meeting.Details.title.string
		self.startLabel.text = L10n.IOSApp.Private.Meeting.Details.start.string
		self.durationLabel.text = L10n.IOSApp.Private.Meeting.Details.duration.string
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		viewModel.startTimer()
		installObservers()
	}

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.stopTimer()
    }

	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		disposeBag = nil
	}

	private func installObservers() {
		let newDisposeBag = DisposeBag()

		viewModel.guestsDriver.drive { [weak self] (guests: [PrivateMeetingGuest]) in
			self?.guestsLabel.text = L10n.IOSApp.Private.Meeting.Details.guests(guests.count)
		}.disposed(by: newDisposeBag)

		viewModel.guestsAsStringsDriver
			.distinctUntilChanged()
			.drive { [weak self] (guests: [String]) in
				guard let selfRef = self else {return}
				for v in selfRef.guestsStackView.arrangedSubviews {
					selfRef.guestsStackView.removeArrangedSubview(v)
					v.removeFromSuperview()
				}
				for guest in guests {
					let l = Luca14PtLabel()
					l.setContentHuggingPriority(.fittingSizeLevel, for: .vertical)

					l.setContentCompressionResistancePriority(UILayoutPriority.init(1000), for: .horizontal)
					l.setContentCompressionResistancePriority(UILayoutPriority.init(1000), for: .vertical)
					l.text = guest
					l.numberOfLines = 1
					selfRef.guestsStackView.addArrangedSubview(l)
				}
			}.disposed(by: newDisposeBag)

		viewModel.timerDriver.drive { [weak self] (timerString: String) in
			self?.durationValueLabel.text = timerString
		}.disposed(by: newDisposeBag)

		viewModel.meetingDriver.drive { [weak self] meeting in
			self?.startValueLabel.text = meeting.createdAt.formattedDateTime
		}.disposed(by: newDisposeBag)

		disposeBag = newDisposeBag
	}

}
