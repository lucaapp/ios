import UIKit
import RxSwift
import LucaUIComponents

class NewPrivateMeetingViewController: UIViewController {
	@IBOutlet weak var qrImageView: UIImageView!
	@IBOutlet weak var timerLabel: PrivateMeetingTimerLabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var guestsLabel: UILabel!
	@IBOutlet weak var slider: CheckinSliderButton!
	@IBOutlet weak var sliderLabel: UILabel!
	@IBOutlet weak var infoButton: UIButton!
	@IBOutlet weak var timeLengthTitleLabel: Luca16PtLabel!
	@IBOutlet weak var infoButtonImageView: UIImageView!
	@IBOutlet weak var participantsTitleLabel: Luca16PtLabel!
    @IBOutlet weak var lengthStackView: UIStackView!
    @IBOutlet weak var guestsStackView: UIStackView!

	private var disposeBag: DisposeBag?
	var viewModel: PrivateMeetingViewModel!

	override func viewDidLoad() {
		super.viewDidLoad()
		setupViews()
		infoButton.isHidden = true
		infoButtonImageView.isHidden = true
	}

	#if DEBUG
	deinit {
		print("deinit NewPrivateMeetingViewController")
	}
	#endif

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		viewModel.startTimer()
		installObservers()
        setupAccessibility()
        UIApplication.shared.setStatusBarStyle(.darkContent, animated: animated)
        applyToViewAndChildrenControllers(theme: .light)

        configureNavigationBar(theme: .light)
	}

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.stopTimer()
    }

	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		disposeBag = nil
	}

	private func installObservers() {
		let newDisposeBag = DisposeBag()

		viewModel.guestsDriver.drive { [weak self] (guests: [PrivateMeetingGuest]) in
			self?.guestsLabel.text = "\(guests.filter { $0.isCheckedIn }.count)\\\(guests.count)"
			self?.infoButton.isHidden = guests.isEmpty
			self?.infoButton.isEnabled = !guests.isEmpty
			self?.infoButtonImageView.isHidden = guests.isEmpty
            self?.adjustAccessibility(guests: guests)
		}.disposed(by: newDisposeBag)

		viewModel.timerDriver.drive { [weak self] (timerString: String) in
			self?.timerLabel.text = timerString
            self?.lengthStackView.accessibilityLabel = L10n.IOSApp.Private.Meeting.Accessibility.length(timerString)
		}.disposed(by: newDisposeBag)

		viewModel.newQRData.drive { [weak self] image in
			self?.qrImageView.image = image
		}
		.disposed(by: newDisposeBag)

		slider.completionBlock = { [weak self] in
			self?.slider.processing = true
			let alert = UIAlertController.yesOrNo(title: L10n.IOSApp.Private.Meeting.End.title,
																						message: L10n.IOSApp.Private.Meeting.End.description,
																						onYes: self?.endMeeting,
																						onNo: {self?.slider.processing = false})
			self?.present(alert, animated: true, completion: nil)
		}
		disposeBag = newDisposeBag
	}

	private func resetCheckInSlider() {
		slider.reset()
		sliderLabel.isHidden = false
		sliderLabel.alpha = 1.0
	}

	func endMeeting() {
		viewModel.endMeeting { [weak self] in
			self?.navigationController?.popViewController(animated: true)
		} failure: { [weak self] error in
			DispatchQueue.main.async {
				if let localizedError = error as? LocalizedTitledError {
					let alert = UIAlertController.infoAlert(title: localizedError.localizedTitle, message: localizedError.localizedDescription)
					self?.present(alert, animated: true, completion: nil)
				}
			}
		}
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let dest = segue.destination as? NewPrivateMeetingDetailsViewController, segue.identifier == "showDetail" {
			dest.viewModel = self.viewModel
		}
	}

	func setupViews() {
        set(title: L10n.IOSApp.Private.Meeting.Info.title.string)
		self.participantsTitleLabel.text = L10n.IOSApp.Private.Meeting.Participants.title.string
		self.timeLengthTitleLabel.text = L10n.IOSApp.Private.Meeting.length.string
		navigationItem.hidesBackButton = true
		descriptionLabel.text = L10n.IOSApp.Private.Meeting.description
		sliderLabel.text = L10n.IOSApp.Private.Meeting.Accessibility.endMeeting.string
		setupAccessibility()

        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.hidesBackButton = true
	}
}

extension NewPrivateMeetingViewController: LogUtil, UnsafeAddress {}

 // MARK: - Accessibility
 extension NewPrivateMeetingViewController {

	private func setupAccessibility() {
        lengthStackView.isAccessibilityElement = true
        slider.setAccessibilityLabel(text: L10n.IOSApp.Private.Meeting.Accessibility.endMeeting)

        self.view.accessibilityElements = [descriptionLabel, qrImageView, lengthStackView, guestsStackView, slider].map { $0 as Any }
        UIAccessibility.setFocusTo(navigationbarTitleLabel, notification: .layoutChanged, delay: 0.8)
	}

     func adjustAccessibility(guests: [PrivateMeetingGuest]) {
         self.guestsStackView.isAccessibilityElement = true
         self.guestsStackView.accessibilityLabel = L10n.IOSApp.Private.Meeting.Accessibility.guests(guests.filter { $0.isCheckedIn }.count, guests.count)

         self.infoButton.isAccessibilityElement = !guests.isEmpty
         self.infoButton.accessibilityLabel = L10n.IOSApp.Verification.PhoneNumber.Info.title

         if !guests.isEmpty {
             self.view.accessibilityElements = [descriptionLabel, qrImageView, lengthStackView, guestsStackView, infoButton, slider].map { $0 as Any }
         }
     }

 }
