import Foundation
import RxSwift
import RxCocoa
import DependencyInjection

final class PrivateMeetingViewModel: LogUtil, UnsafeAddress {

    @InjectStatic(\.privateMeetingQRCodeBuilderV3) private var privateMeetingQRCodeBuilderV3
    @InjectStatic(\.privateMeetingService) private var privateMeetingService
    @InjectStatic(\.checkinTimer) private var checkinTimer

	private var meetingRelay: BehaviorRelay<PrivateMeetingData>
    var meetingDriver: Driver<PrivateMeetingData> {
        meetingRelay.asDriver()
    }
    var guestsAsStringsDriver: Driver<[String]> {
        meetingRelay
            .map { [weak self] meeting in
                meeting.guests.compactMap { guest in
                    try? self?.privateMeetingService.decrypt(guestData: guest, meetingKeyIndex: meeting.keyIndex)
                }
                .enumerated()
                .map { "\($0+1)  \($1.fn) \($1.ln)" }

            }
            .map { Array(Set($0)) }
            .asDriver(onErrorJustReturn: [""])
    }
    var guestsDriver: Driver<[PrivateMeetingGuest]> {
        meetingRelay.map {$0.guests}.asDriver(onErrorJustReturn: [])
    }

	private var timerRelay = BehaviorRelay<String>(value: "")
    var timerDriver: Driver<String> {
        timerRelay.asDriver()
    }

    var newQRData: Driver<UIImage?> {
        meetingDriver
            .asObservable()
            .flatMap { [weak self] (meeting: PrivateMeetingData) -> Single<UIImage?> in
                guard let self = self else {
                    throw RxError.unknown
                }
                return self.privateMeetingQRCodeBuilderV3.build(scannerId: meeting.ids.scannerId)
                    .map { $0.generatedUrl }
                    .map { $0?.data(using: .utf8) }
                    .map { data -> UIImage? in
                        if let data = data { return self.setupQrImage(qrCodeData: data) }
                        return nil
                    }
            }
            .asDriver(onErrorJustReturn: nil)
    }

	private var disposeBag = DisposeBag()

    deinit {
        print("PrivateMeetingViewModel.deinit")
    }

    init(meeting: PrivateMeetingData) {

		meetingRelay = BehaviorRelay<PrivateMeetingData>(value: meeting)

		Observable<Int>.timer(.seconds(1), period: .seconds(10), scheduler: LucaScheduling.backgroundScheduler)
            .take(until: { [weak self] _ in
                self == nil || self?.privateMeetingService == nil
            })
			.flatMap { [weak self] _ -> Single<PrivateMeetingData> in
				guard let selfRef = self else { throw RxError.disposed(object: PrivateMeetingViewModel.self) }
                return selfRef.privateMeetingService.refresh(meeting: selfRef.meetingRelay.value)
			}
			.observe(on: MainScheduler.instance)
			.do(onNext: { [weak self] refreshedMeeting in
				self?.meetingRelay.accept(refreshedMeeting)
			})
			.logError(self, "Meeting fetch")
			.retry(delay: .seconds(1), scheduler: LucaScheduling.backgroundScheduler)
			.subscribe()
			.disposed(by: disposeBag)
	}

	func startTimer() {
		checkinTimer.delegate = self
        checkinTimer.start(from: meetingRelay.value.createdAt)
	}

    func stopTimer() {
        checkinTimer.stop()
    }

	func endMeeting(completion:@escaping (() -> Void), failure:@escaping((Error) -> Void)) {
		privateMeetingService.close(meeting: meetingRelay.value) {
			DispatchQueue.main.async {
                self.stopTimer()
				completion()
			}
		} failure: { (error) in
			failure(error)
		}
	}

	private	func setupQrImage(qrCodeData: Data) -> UIImage? {

		// Temp QR Code generation.
		let qrCode = QRCodeGenerator.generateQRCode(data: qrCodeData)
		if let qr = qrCode {
			let transform = CGAffineTransform(scaleX: 10, y: 10)
			let scaledQr = qr.transformed(by: transform)

			return UIImage(ciImage: scaledQr)

		}
		return nil
	}
}

extension PrivateMeetingViewModel: TimerDelegate {
	func timerDidTick() {
		timerRelay.accept(checkinTimer.counter.formattedTimeString)
	}
}
