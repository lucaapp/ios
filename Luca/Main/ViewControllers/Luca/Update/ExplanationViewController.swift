import UIKit

class ExplanationViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!

    var screen: ExplanationScreen!

    let titleSize = 20
    let bodySize = 14
    let fontScale = 2

    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = screen.image
        setDynamicHTMLText()
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        setDynamicHTMLText()
    }

    override func viewDidLayoutSubviews() {
        // Animated is set to true so that text view scrolls to top and users with smaller screens see that the text view is scrollable.
        textView.setContentOffset(.zero, animated: true)
    }

    private func setDynamicHTMLText() {
        let accessibilitySizes: [UIContentSizeCategory] = [.extraLarge,
                                                           .extraExtraLarge,
                                                           .extraExtraExtraLarge,
                                                           .accessibilityMedium,
                                                           .accessibilityLarge,
                                                           .accessibilityExtraLarge,
                                                           .accessibilityExtraExtraLarge,
                                                           .accessibilityExtraExtraExtraLarge]

        // Sets multiplier to 0 and does not scale font if size is not found in accessibilitySizes
        let index = accessibilitySizes.firstIndex(of: self.traitCollection.preferredContentSizeCategory) ?? -1
        let multiplier = index + 1

        let titleReplaced = screen.text.replacingOccurrences(of: "titleSize", with: String(titleSize + (multiplier * fontScale)))
        let bodyReplaced = titleReplaced.replacingOccurrences(of: "bodySize", with: String(bodySize + (multiplier * fontScale)))

        textView.attributedText = bodyReplaced.htmlString
    }

}
