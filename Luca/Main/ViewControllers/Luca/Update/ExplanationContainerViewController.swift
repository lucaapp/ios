import UIKit
import RxSwift
import LucaUIComponents

class ExplanationContainerViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var previousButton: DarkStandardButton!
    @IBOutlet weak var nextButton: LightStandardButton!
    @IBOutlet weak var cancelButton: DesignableButton!
    @IBOutlet weak var buttonStackView: UIStackView!

    // Complete on skip and close buttons
    var onCompleted: (() -> Void)?
    var onDisappear: (() -> Void)?

    var topic: ExplanationTopic!
    // mandatory explanations provide skip buttons
    var showAllCloseButtons: Bool!

    var pageVC: ExplanationPageViewController!
    var disposeBag: DisposeBag?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPageVC()
        installObservers()
        setupDynamicButtons()
    }

    private func setupPageVC() {
        pageVC = ViewControllerFactory.Main.createExplanationPageViewController(topic: topic)
        addChild(pageVC)

        containerView.addSubview(pageVC.view)
        pageVC.didMove(toParent: self)
        pageVC.view.setAnchorConstraintsFullSizeTo(view: containerView)
    }

    override func viewWillDisappear(_ animated: Bool) {
        onDisappear?()
        disposeBag = nil
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        setupDynamicButtons()
    }

    func setupDynamicButtons() {
        buttonStackView.axis = self.traitCollection.preferredContentSizeCategory >= .accessibilityMedium ? .vertical : .horizontal
    }

    private func installObservers() {
        let newDisposeBag = DisposeBag()

        pageVC.currentPage
            .subscribe(on: MainScheduler.instance)
            .subscribe(onNext: { index in
                self.setup(for: index)
                UIAccessibility.setFocusTo(self.cancelButton, notification: .screenChanged)
            })
            .disposed(by: newDisposeBag)
        disposeBag = newDisposeBag
    }

    private func setup(for index: Int) {
        var previousTitle = L10n.IOSApp.Navigation.Basic.back
        var nextTitle = L10n.IOSApp.Navigation.Basic.next

        if index == 0 && showAllCloseButtons {
            previousTitle = L10n.IOSApp.Navigation.Basic.skip
            nextTitle = L10n.IOSApp.Navigation.Basic.look
        } else if index == pageVC.topic.screens.count - 1 && showAllCloseButtons {
            nextTitle = L10n.IOSApp.Navigation.Basic.close
        }

        previousButton.setTitle(previousTitle, for: .normal)
        nextButton.setTitle(nextTitle, for: .normal)

        setupButtonVisibility(for: index)
    }

    private func setupButtonVisibility(for index: Int) {
        setButton(on: true, button: previousButton)
        setButton(on: true, button: nextButton)

        if index == 0 && !showAllCloseButtons {
            setButton(on: false, button: previousButton)
        } else if index == pageVC.topic.screens.count - 1 && !showAllCloseButtons {
            setButton(on: false, button: nextButton)
        }
    }

    /// Hides button but keeps proportions in stack view
    /// - Parameters:
    ///   - status: hide or show button
    private func setButton(on status: Bool, button: UIButton) {
        button.alpha = status ? 1 : 0
        button.isUserInteractionEnabled = status
    }

    @IBAction func previousPressed(_ sender: DarkStandardButton) {
        _ = pageVC.currentPage
            .take(1)
            .asSingle()
            .subscribe(onSuccess: { index in
                // If first page dismiss view controller
                if index == 0, let completed = self.onCompleted {
                    completed()
                    DispatchQueue.main.async { self.dismiss(animated: true, completion: nil) }
                } else {
                    self.pageVC.presentPreviousViewController(atIndex: index - 1)
                }
            })
    }

    @IBAction func nextPressed(_ sender: LightStandardButton) {
        _ = pageVC.currentPage
            .take(1)
            .asSingle()
            .subscribe(onSuccess: { index in
                // If last page dismiss view controller
                if index == self.topic.screens.count - 1, let completed = self.onCompleted {
                    completed()
                    DispatchQueue.main.async { self.dismiss(animated: true, completion: nil) }
                } else {
                    self.pageVC.presentNextViewController(atIndex: index + 1)
                }
            })
    }

    @IBAction func cancelPressed(_ sender: DesignableButton) {
        dismiss(animated: true, completion: nil)
    }

}
