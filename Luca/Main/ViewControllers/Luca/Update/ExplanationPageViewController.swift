import UIKit
import RxSwift

class ExplanationPageViewController: UIPageViewController {

    private lazy var vcs: [ExplanationViewController] = {
        return topic.screens.map { screen in
            ViewControllerFactory.Main.createExplanationViewController(screen: screen)
        }
    }()

    var topic: ExplanationTopic!
    var currentPage: BehaviorSubject<Int> = BehaviorSubject(value: 0)

    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        setViewControllers([vcs[0]], direction: .forward, animated: true, completion: nil)
        setupPageControl()
    }

    func setupPageControl() {
        let pc = UIPageControl.appearance(whenContainedInInstancesOf: [ExplanationPageViewController.self])
        pc.currentPageIndicatorTintColor = Asset.lucaBlue.color
        pc.pageIndicatorTintColor = UIColor.gray
        pc.backgroundColor = .clear
    }

    func presentNextViewController(atIndex index: Int) {
        setViewControllers([vcs[index]], direction: .forward, animated: true, completion: nil)
    }

    func presentPreviousViewController(atIndex index: Int) {
        setViewControllers([vcs[index]], direction: .reverse, animated: true, completion: nil)
    }
}

extension ExplanationPageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? ExplanationViewController, let index = vcs.firstIndex(of: vc) else {
            return nil
        }

        let previous = index - 1

        guard previous >= 0, vcs.count > previous else {
            return nil
        }

        return vcs[previous]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? ExplanationViewController, let viewControllerIndex = vcs.firstIndex(of: vc) else {
            return nil
        }

        let next = viewControllerIndex + 1

        guard vcs.count != next, vcs.count > next else {
            return nil
        }

        return vcs[next]
    }

     func presentationCount(for pageViewController: UIPageViewController) -> Int {
         return vcs.count
     }

     func presentationIndex(for pageViewController: UIPageViewController) -> Int {
         guard let first = viewControllers?.first, let vc = first as? ExplanationViewController, let index = vcs.firstIndex(of: vc) else { return 0 }
         currentPage.onNext(index)
         return index
     }

     func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
         guard let first = viewControllers?.first, let vc = first as? ExplanationViewController, let index = vcs.firstIndex(of: vc) else { return }
         currentPage.onNext(index)
     }

 }
