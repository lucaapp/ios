import Foundation
import SwiftUI
import UIKit

class LocalCheckinViewController: UIHostingController<LocalCheckInView> {

    init(localCheckin: LocalCheckin, onLinkClick: @escaping (LocationURL) -> Void) {
        super.init(rootView: LocalCheckInView(localCheckin: localCheckin, onLinkClick: onLinkClick))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        rootView.presenter = self
    }

    @MainActor @objc required dynamic init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
