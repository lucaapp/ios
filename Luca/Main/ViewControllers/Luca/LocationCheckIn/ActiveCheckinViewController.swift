import UIKit
import JGProgressHUD
import CoreLocation
import RxSwift
import RxCocoa
import RxAppState
import StoreKit
import LucaUIComponents
import DependencyInjection

class ActiveCheckinViewController: UIViewController, HandlesLucaErrors {
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
	@IBOutlet weak var checkinSlider: CheckinSliderButton?
	@IBOutlet weak var timerLabel: UILabel!
	@IBOutlet weak var checkinDateLabel: UILabel!
	@IBOutlet weak var automaticCheckoutSwitch: UISwitch!
	@IBOutlet weak var checkOutLabel: UILabel!
    @IBOutlet weak var autoCheckoutInfoButton: UIButton!
	@IBOutlet weak var tableNumberLabel: UILabel!
	@IBOutlet weak var automaticCheckoutLabel: UILabel!
	@IBOutlet weak var autoCheckoutView: UIView!
	@IBOutlet weak var checkinTimeDescription: Luca14PtLabel!
    @IBOutlet weak var urlsButtonsStackView: UIStackView!

	var viewModel: LocationCheckInViewModel!

    var addChildrenButtonView: AddChildrenBarButtonView?

	private var loadingHUD = JGProgressHUD.lucaLoading()
	private var userStatusFetcherDisposeBag: DisposeBag?
	private var checkOutDisposeBag: DisposeBag?

	override func viewDidLoad() {
		super.viewDidLoad()
		NotificationPermissionHandler.shared.requestAuthorization(viewController: self)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
        UIApplication.shared.setStatusBarStyle(.darkContent, animated: true)
        configureNavigationBar(theme: .light)
		self.checkinDateLabel.textColor = Asset.lucaBlack87Percent.color
		self.automaticCheckoutLabel.textColor = Asset.lucaBlack.color
		self.checkinTimeDescription.textColor = Asset.lucaBlack.color
		self.checkOutLabel.textColor = Asset.lucaBlack.color
        setupViews()
        urlsButtonsStackView.removeAllArrangedSubviews()

		setupAccessibility()
		installObservers()

        applyToViewAndChildrenControllers(theme: .light)
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: true)
        configureNavigationBar(theme: .dark)
		removeObservers()

		userStatusFetcherDisposeBag = nil
	}

	private func checkout() {
		if checkOutDisposeBag != nil {
			return
		}

		showAppStoreReview()

		let disposeBag = DisposeBag()

		viewModel.checkOut()
			.observe(on: MainScheduler.instance)
			.logError(self, "Check out")
			.do(onError: { (error) in
				self.processErrorMessages(error: error)
			}, onDispose: {
				self.checkOutDisposeBag = nil
			})
			.onErrorComplete()
			.subscribe()
			.disposed(by: disposeBag)

		checkOutDisposeBag = disposeBag
	}

	private func resetCheckInSlider() {
		checkinSlider?.reset()
		checkOutLabel.isHidden = false
		checkOutLabel.alpha = 1.0
	}

	func showAppStoreReview() {
        Task {
            if var appStoreReviewCheckoutCounter = try? await lucaPreferences.get(\.appStoreReviewCheckoutCounter).task() {
                appStoreReviewCheckoutCounter += 1
                if appStoreReviewCheckoutCounter % 5 == 0 {
                    DispatchQueue.main.async {
                        SKStoreReviewController.requestReview()
                    }
                }
                try? await lucaPreferences.set(\.appStoreReviewCheckoutCounter, value: appStoreReviewCheckoutCounter).task()
            }
        }
	}

	// MARK: View setup functions.

	func setupViews() {

        addChildrenButtonView = AddChildrenBarButtonView(colorMode: .dark)
        addChildrenButtonView?.delegate = self
        let addPersonButton = UIBarButtonItem(customView: addChildrenButtonView!)

        parent?.navigationItem.rightBarButtonItem = addPersonButton

		checkinTimeDescription.text = L10n.IOSApp.Checkin.noun.string
		automaticCheckoutLabel.text = L10n.IOSApp.LocationCheckinViewController.autoCheckout.string
		resetCheckInSlider()

        // child VCs show back button without button title
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.hidesBackButton = true
	}

	// swiftlint:disable:next function_body_length
	private func installObservers() {

		let newDisposeBag = DisposeBag()

		viewModel.isCheckedIn
			.do { [weak self] (isCheckedIn) in
				if !isCheckedIn {
					self?.removeObservers()
                    self?.dismiss(animated: true)
				}
			}
			.drive()
			.disposed(by: newDisposeBag)

		viewModel.isBusy.do { [weak self] (busy) in
			if let s = self, busy {
				self?.loadingHUD.show(in: s.view)
			} else {
				self?.loadingHUD.dismiss()
			}
		}
		.drive()
		.disposed(by: newDisposeBag)

		viewModel.isCheckingOut.do { [weak self] (checkoutInProgress) in
			self?.checkinSlider?.processing = checkoutInProgress
		}
		.drive()
		.disposed(by: newDisposeBag)

		viewModel.alert
			.asObservable()
			.flatMapFirst { alert in
				return UIAlertController.infoAlertRx(viewController: self, title: alert.title, message: alert.message)
			}
			.subscribe()
			.disposed(by: newDisposeBag)

		viewModel.additionalDataLabelHidden
			.drive(tableNumberLabel.rx.isHidden)
			.disposed(by: newDisposeBag)

		viewModel.additionalDataLabelText
			.drive(tableNumberLabel.rx.text)
			.disposed(by: newDisposeBag)

		viewModel.time
			.drive(self.timerLabel.rx.text)
			.disposed(by: newDisposeBag)

		viewModel.isAutoCheckoutAvailable
			.map { !$0 }
			.drive(self.autoCheckoutView.rx.isHidden)
			.disposed(by: newDisposeBag)

		viewModel.checkInTime
			.drive(checkinDateLabel.rx.text)
			.disposed(by: newDisposeBag)

        viewModel.checkedInChildren
            .asObservable()
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { persons in
                self.addChildrenButtonView?.setCount(persons.count)
            })
            .disposed(by: newDisposeBag)

		Driver.combineLatest(viewModel.groupName, viewModel.locationName, viewModel.isPrivateMeeting).drive(onNext: { [weak self] (groupName, locationName, isPrivateMeeting) in
			self?.setupLocationLabels(with: groupName, and: locationName, isPrivateMeeting: isPrivateMeeting)
            self?.setupLocationLabelsAccessibility()
		}).disposed(by: newDisposeBag)

		(automaticCheckoutSwitch.rx.value <-> viewModel.isAutoCheckoutEnabled).disposed(by: newDisposeBag)

		let checkinSliderCompletionBlock:(() -> Void)? = { [weak self] in
			self?.checkout()
		}
		checkinSlider?.completionBlock = checkinSliderCompletionBlock

		viewModel.checkInTimeDate
			.subscribe(onSuccess: { time in
                self.checkinDateLabel.accessibilityLabel = L10n.IOSApp.Checkin.Slider.Date.accessibility(time.accessibilityDate)
			})
			.disposed(by: newDisposeBag)

		viewModel.connect(viewController: self)

		userStatusFetcherDisposeBag = newDisposeBag
	}

	private func removeObservers() {
		userStatusFetcherDisposeBag = nil
		viewModel.release()
	}

	private func setupLocationLabels(with groupName: String?, and locationName: String?, isPrivateMeeting: Bool) {

		let stackView = UIStackView()
		stackView.axis = .vertical
		stackView.alignment = .center
		if isPrivateMeeting || locationName != nil {
			let heading = Luca12PtLabel()
			heading.text = isPrivateMeeting ? L10n.IOSApp.Private.Meeting.Info.title.string : (locationName ?? "")
			stackView.addArrangedSubview(heading)
		}
		if let g = groupName {
			let title = Luca20PtBoldLabel()
			title.text = g
			title.textColor = Asset.lucaBlack.color
			stackView.addArrangedSubview(title)
		}
        self.parent?.navigationItem.titleView = stackView
	}

    @IBAction func infoTapped(_ sender: UIButton) {
        let link = L10n.IOSApp.LocationCheckinViewController.AutoCheckout.Permission.BeforePrompt.link
        let type = UserInputInteractorConfiguration.InteractorType.modal(linkText: link, url: try? DependencyContext[\.backendAddressV3].provide().privacyPolicyUrl)
        let configuration = UserInputInteractorConfiguration(title: L10n.IOSApp.LocationCheckinViewController.AutoCheckout.Permission.BeforePrompt.title,
                                                             message: L10n.IOSApp.LocationCheckinViewController.AutoCheckout.Permission.BeforePrompt.message(link),
                                                             type: type)

        _ = self.showDialog(configuration: configuration)
            .subscribe(on: MainScheduler.instance)
            .subscribe()
    }

}

extension ActiveCheckinViewController: UnsafeAddress, LogUtil {}

// MARK: - Accessibility
extension ActiveCheckinViewController {

	private func setupAccessibility() {
		checkinSlider?.setAccessibilityLabel(text: L10n.IOSApp.LocationCheckinViewController.Accessibility.checkoutSlider)
		timerLabel.isAccessibilityElement = false
        autoCheckoutInfoButton.accessibilityLabel = "\(L10n.IOSApp.LocationCheckinViewController.autoCheckout) \(L10n.IOSApp.Verification.PhoneNumber.Info.title)"
        automaticCheckoutSwitch.accessibilityLabel = L10n.IOSApp.LocationCheckinViewController.autoCheckout

        checkinSlider?.setAccessibilityLabel(text: L10n.IOSApp.LocationCheckinViewController.Accessibility.checkoutSlider)

        addChildrenButtonView?.button.accessibilityLabel = L10n.IOSApp.Children.List.title
        checkinTimeDescription.isAccessibilityElement = false
	}

    private func setupLocationLabelsAccessibility() {
        if let navigationbarSubtitleLabel = navigationbarSubtitleLabel {
            navigationbarSubtitleLabel.accessibilityTraits = .header
            if let navigationbarTitleLabel = navigationbarTitleLabel {
                navigationbarTitleLabel.accessibilityTraits = .header
                UIAccessibility.setFocusTo(navigationbarTitleLabel, notification: .layoutChanged, delay: 0.8)
            } else {
                UIAccessibility.setFocusTo(navigationbarSubtitleLabel, notification: .layoutChanged, delay: 0.8)
            }
        }
    }

}

extension ActiveCheckinViewController: AddChildrenBarButtonViewDelegate {
    func didTapButton() {
        navigationController?.pushViewController(
            ViewControllerFactory.Children.createEmbeddedChildrenListViewController(viewModel: self.viewModel, theme: AppearanceTheme.light),
            animated: true
        )
    }
}
