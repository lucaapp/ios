import UIKit
import RxSwift
import RxCocoa
import RxAppState
import DependencyInjection

class MainCheckinViewModel: NSObject, LogUtil, UnsafeAddress, ViewControllerOwnedVM {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.dailyPublicKeyVerifier) private var dailyPublicKeyVerifier: DailyPublicKeyVerifier
    @InjectStatic(\.traceIdService) private var traceIdService: TraceIdService
    @InjectStatic(\.localCheckinService) private var localCheckinService: LocalCheckinService

	private(set) weak var owner: UIViewController?

	// MARK: - Input
	let mainCheckinViewControllerVisible = BehaviorRelay<Bool>(value: false)

	// MARK: - Output

    var currentLocalCheckin: Driver<LocalCheckin?> {
        localCheckinService.currentCheckIn.asDriver(onErrorJustReturn: nil)
    }

    var checkin: Driver<TraceInfo> {
        traceIdService
        .onCheckInRx()
        .asDriver(onErrorDriveWith: .empty())
    }

    var showScanner: Driver<Bool> {
        Observable.combineLatest(
            UIApplication.shared.rx.currentAndChangedAppState.subscribe(on: MainScheduler.asyncInstance),
            self.mainCheckinViewControllerVisible.asObservable()
        )
        .flatMap({ (appState: AppState, visible: Bool) -> Observable<Bool> in
            return .just(appState == .active && visible)
        })
        .distinctUntilChanged()
        .asDriver(onErrorJustReturn: false)
    }

    var optionalButtonsEnabled: Driver<Bool> {
        return Driver.combineLatest(dailyPublicKeyAvailable, isUserContactDataComplete)
            .flatMapLatest { (isDailyPublicKeyValid, isUserDataComplete) in
                return Driver.just(isDailyPublicKeyValid && isUserDataComplete)
            }
            .asDriver(onErrorJustReturn: false)
    }

    var dailyPublicKeyAvailable: Driver<Bool> {
        dailyPublicKeyVerifier.isKeyValid.asDriver(onErrorJustReturn: false)
    }

    var isUserContactDataComplete: Driver<Bool> {
        lucaPreferences.currentAndChanges(\.userRegistrationData)
            .map { userRegistrationData in
                return userRegistrationData?.isDataComplete == true
            }
            .asDriver(onErrorJustReturn: false)
    }

    init(owner: UIViewController? = nil) {
		super.init()
		self.owner = owner
	}

	#if DEBUG
	deinit {
		print("MainCheckinViewModel.deinit")
	}
	#endif
}
