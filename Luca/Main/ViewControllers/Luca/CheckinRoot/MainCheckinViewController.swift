import Foundation
import UIKit
import LucaUIComponents
import RxSwift
import RxCocoa
import AVFoundation
import JGProgressHUD
import AVKit
import DependencyInjection

class MainCheckinViewController: UIViewController, HandlesLucaErrors, ContainsTorchButton {
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.qrProcessingService) private var qrProcessingService
    @InjectStatic(\.privateMeetingService) private var privateMeetingService
    @InjectStatic(\.traceIdService) private var traceIdService
    @InjectStatic(\.userService) private var userService
    @InjectStatic(\.checkinPolling) private var checkinPolling

	@IBOutlet var showQRCodeButton: LightStandardButton!
	@IBOutlet var privateMeetingButton: DarkStandardButton!
    @IBOutlet weak var lucaQRScannerView: LucaQRScanner!

    @IBOutlet weak var buttonsView: UIStackView!
    @IBOutlet weak var noDailyKeyView: UIStackView!

    lazy var viewModel: MainCheckinViewModel = MainCheckinViewModel(owner: self)
	private var onCheckInDisposeBag: DisposeBag?
	private var loadingHUD = JGProgressHUD.lucaLoading()
    private var disposeBag = DisposeBag()

    private var scanningDisposeBag: DisposeBag! = nil

	override func viewDidLoad() {
		super.viewDidLoad()
        self.bindViewModel()

        set(title: L10n.IOSApp.Checkin.noun)
		privateMeetingButton.setTitle(L10n.IOSApp.Button.Private.meeting.uppercased(), for: .normal)
        let historyButton = UIBarButtonItem(image: Asset.history.image.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(showHistory))
        historyButton.accessibilityLabel = L10n.IOSApp.Navigation.Tab.history
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = historyButton

        self.noDailyKeyView.isHidden = true
        self.buttonsView.isHidden = true

        bindLucaQRScanner()

        // Breaking subscriptions...
        userService
            .onUserDeletedRx
            .do(onNext: { _ in
                self.disposeBag = DisposeBag()
                self.scanningDisposeBag = nil
                self.onCheckInDisposeBag = nil
            })
            .subscribe()
            .disposed(by: disposeBag)

        subscribeToLocalCheckins()
	}

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: animated)
        applyToViewAndChildrenControllers(theme: .dark)
        onCheckInDisposeBag = DisposeBag()
        viewModel.mainCheckinViewControllerVisible.accept(true)
        viewModel
            .dailyPublicKeyAvailable
            .distinctUntilChanged()
            .drive { [weak self] keyValid in
                self?.noDailyKeyView.isHidden = keyValid
                self?.buttonsView.isHidden = !keyValid
            }
            .disposed(by: onCheckInDisposeBag!)

        bindShowQRCodeButton(with: onCheckInDisposeBag!)
        bindPrivateMeetingButton(with: onCheckInDisposeBag!)

        showCheckinOrMeetingViewController(animated: false)

        checkinPolling.frequency = .low

        setupAccessibility()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.mainCheckinViewControllerVisible.accept(false)

        onCheckInDisposeBag = nil
    }

    private func subscribeToLocalCheckins() {
        let subscriptionTimestamp = Date()
        viewModel.currentLocalCheckin
            .drive(onNext: { localCheckin in
                if let localCheckin = localCheckin {
                    let isOngoingCheckin = (subscriptionTimestamp.distance(to: Date()) < 1)
                    self.showLocalCheckinViewController(localCheckin: localCheckin, animated: !isOngoingCheckin)
                } else {
                    self.navigationController?.popViewController(animated: true)
                }})
            .disposed(by: disposeBag)
    }

	private func bindViewModel() {
		viewModel.checkin
            .filter { [weak self] _ in self?.viewModel.mainCheckinViewControllerVisible.value ?? false }
			.drive { [weak self] _ in
				if self?.presentedViewController != nil {
					self?.dismiss(animated: true) {
						self?.showCheckinOrMeetingViewController(animated: true)
					}
				} else {
					self?.showCheckinOrMeetingViewController(animated: true)
				}
			}
			.disposed(by: disposeBag)

        viewModel.showScanner
            .drive { [weak self] show in
                self?.showScanner(display: show)
            }
            .disposed(by: disposeBag)
	}

    private func bindLucaQRScanner() {
        lucaQRScannerView.onActivateButton
            .subscribe { _ in
                self.startScanner()
            }
            .disposed(by: disposeBag)
    }

    private func bindShowQRCodeButton(with disposeBag: DisposeBag) {
        viewModel
            .optionalButtonsEnabled
            .map { $0 ? 1.0 : 0.5 }
            .asObservable()
            .bind(to: self.showQRCodeButton.rx.alpha)
            .disposed(by: disposeBag)

        handleShowQRCodeButtonTaps(with: disposeBag)
    }

    private func handleShowQRCodeButtonTaps(with disposeBag: DisposeBag) {
        showQRCodeButton.rx.tap
            .flatMap { self.viewModel.optionalButtonsEnabled }
            .do(onNext: { [weak self] isEnabled in
                self?.showMyQRCodeButtonPressed(isEnabled)
            })
            .subscribe()
            .disposed(by: disposeBag)

    }

    private func showMyQRCodeButtonPressed(_ isButtonEnabled: Bool) {
        if isButtonEnabled {
            self.showMyQRCode()
        } else {
            self.showErrorWhenContactDataIsIncomplete()
        }
    }

    private func showMyQRCode() {
        let viewController = ViewControllerFactory.Checkin.createMyQRViewController()
        navigationController?.present(viewController, animated: true)
    }

    private func bindPrivateMeetingButton(with disposeBag: DisposeBag) {
        viewModel
            .optionalButtonsEnabled
            .map { $0 ? 1.0 : 0.5 }
            .asObservable()
            .bind(to: self.privateMeetingButton.rx.alpha)
            .disposed(by: disposeBag)

        handlePrivateMeetingButtonTaps(with: disposeBag)
    }

    private func handlePrivateMeetingButtonTaps(with disposeBag: DisposeBag) {
        privateMeetingButton.rx.tap
            .flatMap { self.viewModel.optionalButtonsEnabled }
            .do(onNext: { [weak self] isEnabled in
                self?.privateMeetingButtonPressed(isEnabled)
            })
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func privateMeetingButtonPressed(_ isButtonEnabled: Bool) {
        if isButtonEnabled {
            self.showPrivateMeetingAlert()
        } else {
            self.showErrorWhenContactDataIsIncomplete()
        }
    }

    private func showPrivateMeetingAlert() {
        UIAlertController(title: L10n.IOSApp.Private.Meeting.Start.title,
                          message: L10n.IOSApp.Private.Meeting.Start.description,
                          preferredStyle: .alert)
            .actionAndCancelAlert(
                actionText: L10n.IOSApp.Navigation.Basic.start,
                action: createPrivateMeeting,
                viewController: self
            )
    }

    private func showErrorWhenContactDataIsIncomplete() {
        guard let disposeBag = onCheckInDisposeBag else { return }
        viewModel.isUserContactDataComplete
            .asObservable()
            .take(1)
            .map { [weak self] (isComplete) in
                guard !isComplete else { return }
                self?.showIncompleteContactDataError()
            }
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func showIncompleteContactDataError() {
        let error = CheckInInteractorError.incompleteContactData
        self.processErrorMessages(error: error)
    }

	private func showScanner(display: Bool) {
        if display == false {
			self.endScanner()
		} else {
            lucaQRScannerView.showScanner(display: display)
            if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
                DispatchQueue.main.async {
                    self.startScanner()
                }
            }
		}
	}

	@objc private func showHistory() {
		let viewController = ViewControllerFactory.History.createHistoryViewController()
		navigationController?.pushViewController(viewController, animated: true)
	}

    private func showLocalCheckinViewController(localCheckin: LocalCheckin, animated: Bool) {
        let vc = LocalCheckinViewController(
            localCheckin: localCheckin,
            onLinkClick: { [weak self] locationURL in
                guard let self = self else { return }
                _ = OpenLocationURLInteractor(
                    presenter: self,
                    location: localCheckin.location,
                    locationURL: locationURL
                )
                .interact()
                .handleErrors(self)
                .subscribe()
            }
        )
        self.navigationController?.pushViewController(vc, animated: animated)
    }

	private func showPrivateMeetingViewController(meeting: PrivateMeetingData?, animated: Bool) {
		if let meeting = meeting ?? privateMeetingService.currentMeeting {
            let viewController = ViewControllerFactory.Checkin.createPrivateMeetingViewController(meeting: meeting)
            viewController.viewModel = PrivateMeetingViewModel(meeting: meeting)
            self.navigationController?.pushViewController(viewController, animated: animated)
		}
	}

	private func goToApplicationSettings() {
		UIAlertController(title: L10n.IOSApp.Camera.Access.title, message: L10n.IOSApp.Camera.Access.description, preferredStyle: .alert).goToApplicationSettings(viewController: self, pop: true)
	}

	private func startScanner() {
        scanningDisposeBag = DisposeBag()
        lucaQRScannerView.scan
            // Delay the subscription to force the appearance to continue without waiting for the session
            .delaySubscription(.milliseconds(1), scheduler: MainScheduler.asyncInstance)
            .take(1)
            .flatMap {
                self.qrProcessingService.processQRCode(qr: $0, processingStrategy: .checkinScanner(self), presenter: self)
                    .observe(on: MainScheduler.asyncInstance)
                    .do(onSuccess: { qrType in
                        if let mainTabBar = self.tabBarController as? MainTabBarViewController,
                           case QRType.document = qrType {
                            mainTabBar.changeTabBar(to: .myLuca)
                        }
                    })
                    .do(onSubscribe: { DispatchQueue.main.async { self.loadingHUD.show(in: self.view) } })
                    .do(onDispose: { self.loadingHUD.dismiss() })
            }
            .catch { (error) -> Observable in
                if let localizedError = error as? LocalizedTitledError,
                   case MetadataScannerServiceLocalizedError.captureDeviceNotAuthorized = localizedError {
                    return Completable.from {
                        DispatchQueue.main.async {
                            self.goToApplicationSettings()
                        }
                    }
                    .andThen(Observable.empty())
                } else {
                    return self.processErrorMessagesRx(error: error)
                        .andThen(Observable.error(error))
                }
            }
            .retry()
            .subscribe()
            .disposed(by: scanningDisposeBag)
	}

	private func endScanner() {
        // Delay this call to prevent hiccup on disposal
        DispatchQueue.main.asyncAfter(deadline: .now() + DispatchTimeInterval.milliseconds(1)) {
            self.scanningDisposeBag = nil
        }
	}

	private func showCheckinOrMeetingViewController(animated: Bool) {
		if privateMeetingService.currentMeeting != nil {
			showPrivateMeetingViewController(meeting: nil, animated: animated)
			return
		}
	}

	private func createPrivateMeeting() {
		if privateMeetingService.currentMeeting == nil {
			_ = privateMeetingService.createMeeting()
				.subscribe(on: MainScheduler.instance)
				.do(onSubscribe: { [weak self] in
                    guard let unwrappedSelf = self else { return }
                    self?.loadingHUD.show(in: unwrappedSelf.view) })
				.do(onDispose: { [weak self] in
                    self?.loadingHUD.dismiss() })
				.do(onSuccess: { [weak self] meeting in
					self?.showPrivateMeetingViewController(meeting: meeting, animated: true)
				})
				.do(onError: { [weak self] error in
					let alert = UIAlertController.infoAlert(title: L10n.IOSApp.Navigation.Basic.error, message: L10n.IOSApp.Private.Meeting.Create.failure(error.localizedDescription))
					self?.present(alert, animated: true, completion: nil)
				})
				.subscribe()
		}
	}
	@IBAction func activateScannerPressed(_ sender: Any) {
        self.startScanner()
	}

}

extension MainCheckinViewController: UnsafeAddress, LogUtil {}

extension MainCheckinViewController {

    private func setupAccessibility() {
        UIAccessibility.setFocusTo(navigationbarTitleLabel, notification: .layoutChanged, delay: 0.8)
    }

}
