import UIKit
import AVFoundation
import RxSwift
import RxCocoa
import JGProgressHUD

class QRScannerViewController: UIViewController, HandlesLucaErrors {

    private let session = BehaviorSubject<AVCaptureSession?>(value: nil)
    private var previewLayer: AVCaptureVideoPreviewLayer!

    var scan: Observable<String> {
        checkVideoAuthorizationStatus()
            .andThen(retrieveSession)
            .asObservable()
            .do(onNext: { session in
                let tempPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
                tempPreviewLayer.videoGravity = .resizeAspectFill
                tempPreviewLayer.frame = self.view.layer.bounds
                self.view.layer.addSublayer(tempPreviewLayer)
                self.previewLayer = tempPreviewLayer
            })
                .flatMap { MetadataScannerService.scan(with: $0) }
                .flatMap { Observable.from($0) }
                .compactMap { $0 as? AVMetadataMachineReadableCodeObject }
                .compactMap { $0.stringValue }
                .do(onDispose: {
                    self.previewLayer?.removeFromSuperlayer()
                    self.previewLayer = nil
                })
                    }

    private var retrieveSession: Single<AVCaptureSession> {
        session.flatMap { optionalSession -> Single<AVCaptureSession> in
            if let session = optionalSession {
                return Single.just(session)
            }
            return MetadataScannerService
                .createCaptureSession(for: [.qr])
                .do(onSuccess: { self.session.onNext($0) })
                    }
        .compactMap { $0 }
        .take(1)
        .asSingle()
    }

    private func checkVideoAuthorizationStatus() -> Completable {
        return Completable.create { observer -> Disposable in
            if AVCaptureDevice.authorizationStatus(for: .video) != .authorized {
                AVCaptureDevice.requestAccess(for: .video) { canAccessCamera in
                    if canAccessCamera {
                        observer(.completed)
                    } else {
                        observer(.error(MetadataScannerServiceLocalizedError.captureDeviceNotAuthorized))
                    }
                }
            } else {
                observer(.completed)
            }
            return Disposables.create()
        }
        .subscribe(on: MainScheduler.instance)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationbar()
    }

    func setupNavigationbar() {
        set(title: L10n.IOSApp.Test.Scanner.title)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        previewLayer?.frame = view.layer.bounds
    }
}
