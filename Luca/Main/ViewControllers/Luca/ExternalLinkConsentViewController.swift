import UIKit
import RxSwift
import RxCocoa
import RxAppState
import LucaUIComponents
import DependencyInjection

class ExternalLinkConsentViewController: UIViewController, LucaModalAppearance, Themable {
    @InjectStatic(\.lucaPreferences) private var lucaPreferences

    var theme: AppearanceTheme = .dark
    var supportedThemes: [AppearanceTheme] = [.dark]

    @IBOutlet private weak var button: UIButton!
    @IBOutlet weak var messageLabel: Luca14PtLabel!
    @IBOutlet weak var dontAskView: DontAskAnymoreView!

    /// Emits `true` if button was clicked, `false` if the view controller was just dismissed
    var result: Single<Bool> {
        self.loadViewIfNeeded()

        return Observable.merge(
            button.rx.tap.asObservable().map { _ in true },
            self.rx.viewDidDisappear.map { _ in false }
        )
            .take(1)
            .asSingle()
            .observe(on: MainScheduler.instance)
            .flatMap { result in

                // Save the result if user wishes to
                if self.dontAskView.isOn {
                    return self.lucaPreferences.set(\.allowOpenExternalLocationLinks, value: result).andThen(.just(result))
                }
                return .just(result)
            }
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { _ in self.dismiss(animated: true, completion: nil) })
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: L10n.IOSApp.Navigation.Basic.cancel, style: .plain, target: self, action: #selector(cancelTapped))

        applyColors()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        applyToViewAndChildrenControllers(theme: theme)

        _ = self.dontAskView.bind(
            source: lucaPreferences
                .currentAndChanges(\.allowOpenExternalLocationLinks)
                .map {  $0 ?? false }
                .take(until: self.rx.viewWillDisappear)
                .asDriver(onErrorJustReturn: false)
        )
    }

    @objc private func cancelTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

    func setup(location: Location, url: LocationURL) {
        loadViewIfNeeded()
        messageLabel.text = L10n.IOSApp.ExternalLinkConsent.message(location.groupName ?? "", url.type.localized, url.url)
    }
}
