import UIKit
import RxSwift
import RxCocoa
import RxAppState
import DependencyInjection
import LucaUIComponents

class OptionalCheckinDataViewController: UIViewController, LucaModalAppearance {
    @InjectStatic(\.lucaPreferences) private var lucaPreferences

    @IBOutlet weak var titleLabel: Luca20PtBoldLabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var switchLabel: Luca14PtLabel!
    @IBOutlet weak var `switch`: UISwitch!
    @IBOutlet private weak var button: UIButton!
    @IBOutlet weak var dontAskAnymoreView: DontAskAnymoreView!

    /// Emits the invert of the share data switch state, corresponding to the value for checkInAnonymousWhenPossible. Emits `false`when on and `true`when off.
    /// Completes if user cancels or the view controller is otherwise dismissed
    var result: Maybe<Bool> {
        self.loadViewIfNeeded()
        return button.rx.tap
            .asObservable()
            .map { _ in self.switch.isOn }
            .flatMap { result -> Observable<Bool> in
                if self.dontAskAnymoreView.isOn {
                    return self.lucaPreferences
                        .set(\.dontShowVoluntaryCheckinConsent, value: self.dontAskAnymoreView.isOn)
                        .andThen(self.lucaPreferences.set(\.checkInAnonymousWhenPossible, value: !result))
                        .andThen(Observable.just(!result))
                }
                return Observable.just(!result)
            }
            .take(1)
            .observe(on: MainScheduler.instance)
            .do(onNext: { _ in
                self.dismiss(animated: true, completion: nil)
            })
            .take(until: rx.viewDidDisappear)
            .asMaybe()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: L10n.IOSApp.Navigation.Basic.cancel, style: .plain, target: self, action: #selector(cancelTapped))

        applyColors()
        setupAccessibility()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        _ = self.switch.rx.isOn
            .take(until: self.rx.viewDidDisappear)      // Complete on viewDidDisappear
            .take(until: self.rx.viewDidAppear.skip(1)) // Complete on viewDidAppear (but skip the first value). It's a safety check to prevent multiple streams
            .asDriver(onErrorJustReturn: false)
            .drive(onNext: { isOn in
                self.imageView.image = isOn ? Asset.anonymousEnabled.image : Asset.anonymous.image
            })
    }

    @objc private func cancelTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}
extension OptionalCheckinDataViewController {

    private func setupAccessibility() {
        titleLabel.accessibilityTraits = .header
        switchLabel.isAccessibilityElement = false
        self.switch.accessibilityLabel = L10n.IOSApp.OptionalCheckin.checkBox
    }

}
