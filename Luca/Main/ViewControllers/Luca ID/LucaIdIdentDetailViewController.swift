import UIKit
import LucaUIComponents
import RxSwift
import DependencyInjection

class LucaIdIdentDetailViewController: UIViewController {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.lucaIDService) private var lucaIDService

    @IBOutlet weak var identLabel: Luca16PtBoldLabel!
    @IBOutlet weak var validityLabel: Luca14PtLabel!
    @IBOutlet weak var copyButton: UIButton!

    #if PRODUCTION || PREPROD
    private let deeplink = "https://mls.idnow.de/"
    #else
    private let deeplink = "https://mls.test.idnow.de/"
    #endif

    var disposeBag: DisposeBag?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let newDisposeBag = DisposeBag()

        lucaIDService.fetchState()
            .map { $0?.receiptJWS?.claims.autoIdentId ?? "" }
            .asDriver(onErrorJustReturn: "")
            .drive(identLabel.rx.text)
            .disposed(by: newDisposeBag)

//        lucaIDService.fetchState()
//            .map {
//                if let date = $0?.identityJWT?.claims.exp.formattedDate {
//                    return L10n.IOSApp.Id.IdRequestSuccessText._2(date)
//                }
//                return ""
//            }.asDriver(onErrorJustReturn: "")
//            .drive(validityLabel.rx.text)
//            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposeBag = nil
    }

    func setupUI() {
        copyButton.setTitle("", for: .normal)
        copyButton.accessibilityLabel = L10n.IOSApp.Id.Ident.Copy.button
        set(title: L10n.IOSApp.Id.Detail.title)
    }

    @IBAction func copyButtonPressed(_ sender: UIButton) {
        UIPasteboard.general.string = identLabel.text
        let alert = UIAlertController.infoAlert(title: "", message: L10n.IOSApp.Id.clipboard)
        present(alert, animated: true, completion: nil)
    }

    @IBAction func idNowButtonPressed(_ sender: LightStandardButton) {
        guard let ident = identLabel.text, let url = URL(string: deeplink + "\(ident)") else { return }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, completionHandler: nil)
        }
    }

}
