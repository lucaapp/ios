import UIKit
import LucaUIComponents
import RxSwift

class DescriptionAndImageModalViewController: UIViewController, LucaModalAppearance {

    @IBOutlet weak var titleLabel: Luca20PtBoldLabel!
    @IBOutlet weak var descriptionLabel: Luca14PtLabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var cancelButton: UIBarButtonItem!

    var titleText: String!
    var descriptionText: String!
    var buttonText: String!
    var image: UIImage!

    /// Emits `true` if accept button was clicked, `false` if the view controller has been dismissed.
    var result: Single<Bool> {
        self.loadViewIfNeeded()

        return Observable<Bool>.merge(
            nextButton.rx.tap
                .map { _ in true },
            rx.viewDidDisappear.map { _ in false }
        )
        .take(1)
        .asSingle()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.clipsToBounds = true
        setupUI()
    }

    func setupUI() {
        titleLabel.text = titleText
        descriptionLabel.text = descriptionText
        imageView.image = image
        nextButton.setTitle(buttonText, for: .normal)

        applyColors()
        cancelButton?.title = L10n.IOSApp.Navigation.Basic.cancel
    }

    @IBAction func cancelPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }

}
