import UIKit
import LucaUIComponents
import RxSwift
import DependencyInjection

class LucaIdSuccessDetailViewController: UIViewController {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.lucaIDService) private var lucaIDService

    @IBOutlet weak var identLabel: Luca16PtBoldLabel!
    @IBOutlet weak var copyButton: UIButton!

    var disposeBag: DisposeBag?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let newDisposeBag = DisposeBag()

        lucaIDService.fetchState()
            .map { $0?.state.revocationCode }
            .asDriver(onErrorJustReturn: "")
            .drive(identLabel.rx.text)
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposeBag = nil
    }

    func setupUI() {
        copyButton.setTitle("", for: .normal)
        copyButton.accessibilityLabel = L10n.IOSApp.Id.Ident.Copy.button
        set(title: L10n.IOSApp.Id.idVerificationSuccessTitle)
    }

    @IBAction func copyButtonPressed(_ sender: UIButton) {
        UIPasteboard.general.string = identLabel.text
        let alert = UIAlertController.infoAlert(title: "", message: L10n.IOSApp.Id.revocationCodeClipboard)
        present(alert, animated: true, completion: nil)
    }
}
