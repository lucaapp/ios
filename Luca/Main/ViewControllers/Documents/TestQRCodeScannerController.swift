import UIKit
import RxSwift
import RxCocoa
import RxAppState
import JGProgressHUD
import DependencyInjection
import LucaUIComponents

class TestQRCodeScannerController: UIViewController, HandlesLucaErrors {

    @InjectStatic(\.qrProcessingService) private var qrProcessingService

    @IBOutlet weak var descriptionLabel: Luca14PtLabel!
    @IBOutlet weak var lucaQRScanner: LucaQRScanner!

    private var loadingHUD = JGProgressHUD.lucaLoading()

    var closeButton: UIBarButtonItem?

    var scan: Observable<QRType> {
        return Observable.merge(Observable.just(Void()), rx.viewState.map { _ in Void() })
            .filter { _ in self.isViewLoaded }
            .take(1)
            .flatMap { _ in self.lucaQRScanner.scan }
            .take(1)
            .flatMap {
                self.qrProcessingService.processQRCode(qr: $0, processingStrategy: .documentScanner(self), presenter: self)
                    .do(onSubscribe: { DispatchQueue.main.async { self.loadingHUD.show(in: self.view) } })
                    .do(onDispose: { DispatchQueue.main.async { self.loadingHUD.dismiss() } })
            }
            .catch {
                if let authorizationError = $0 as? MetadataScannerServiceLocalizedError,
                   authorizationError == .captureDeviceNotAuthorized {
                    return self.handleAuthorizationError(title: L10n.IOSApp.Camera.Access.title, message: L10n.IOSApp.Camera.Access.description)
                        .andThen(Observable<QRType>.empty())
                }
                return self.processErrorMessagesRx(error: $0)
                    .andThen(Observable.error($0))
            }
            .retry()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationbar()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupAccessibility()
        applyToViewAndChildrenControllers(theme: .dark)
    }

    func setupNavigationbar() {
        set(title: L10n.IOSApp.Test.Scanner.title)
        closeButton = UIBarButtonItem(image: UIImage(named: "closeButton"), style: .plain, target: self, action: #selector(closeTapped))
        navigationItem.rightBarButtonItem = closeButton
    }

    @objc func closeTapped() {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - Accessibility
extension TestQRCodeScannerController {

    private func setupAccessibility() {
        closeButton?.accessibilityLabel = L10n.IOSApp.Test.Scanner.close
        lucaQRScanner.accessibilityLabel = L10n.IOSApp.Test.Scanner.camera
        lucaQRScanner.isAccessibilityElement = true
        UIAccessibility.setFocusTo(navigationbarTitleLabel, notification: .layoutChanged, delay: 0.8)
    }

}
