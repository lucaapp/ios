import Foundation
import RxSwift
import RxCocoa
import DependencyInjection
import UIKit
import LocalAuthentication

class DocumentListViewModel {
    @InjectStatic(\.documentPersonAssociationService) private var documentPersonAssociation
    @InjectStatic(\.documentProcessingService) private var documentProcessingService
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.keyValueRepo) private var keyValueRepo
    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.personRepo) private var personRepo
    @InjectStatic(\.documentRepo) private var documentRepo
    @InjectStatic(\.documentRepoService) private var documentRepoService
    @InjectStatic(\.timeSync) private var timeSync
    @InjectStatic(\.lucaIDService) private var lucaIDService

    private let revalidationKey = "revalidationKey"

    lazy var persons: Driver<[Person: [Document]]> = {
        Observable.deferred { [weak self] in
            guard let self = self else { return Observable.empty() }

            return self.updateSignal.flatMapLatest { _ in
                Observable.combineLatest(self.documentPersonAssociation
                                            .currentAndNewTestAssociations, self.lucaIDProcessChangesAsPersons()).map {
                    return $0.merging($1, uniquingKeysWith: +)
                }
                .subscribe(on: ConcurrentDispatchQueueScheduler(qos: .userInitiated))
            }
        }
        .share(replay: 1, scope: .whileConnected)
        .asDriver(onErrorJustReturn: [:])
    }()

    var isTimeInSync: Driver<Bool> {
        UIApplication.shared.rx
            .currentAndChangedAppState
            .filter { $0 == .active }
            .map { _ in Void() }
            .flatMapLatest { _ in self.timeSync.isInSync().catchAndReturn(true) }
            .asDriver(onErrorJustReturn: true)
    }

    private func lucaIDProcessChangesAsPersons() -> Observable<[Person: [Document]]> {
        lucaIDService.lucaIDProcessChanges
            .map({ (data: LucaIDParsedData?) -> [Person: [Document]] in
                guard let data = data, data.state.state == .success else { return [Person: [Document]]() }
                let lucaID = LucaID(payload: data)
                let p = Person(firstName: lucaID.firstName, lastName: lucaID.lastName, type: .placeholder)
                return [p: [lucaID]]
            })
    }

    func remove(document: Document) -> Completable {
        if document is LucaID {
            return lucaIDService.archiveData()
        } else {
            return documentProcessingService.remove(document: document)
        }
    }

    func authenticateDeviceUser(completion: @escaping (Bool) -> Void) {
        let authContext = LAContext()
        authContext.evaluatePolicy(LAPolicy.deviceOwnerAuthentication,
                                   localizedReason: L10n.IOSApp.Id.Authentication.reason,
                                   reply: { (success, _) in
            DispatchQueue.main.async {
                completion(success)
            }
        })
    }

    /// Emits a value everytime the display should be updated
    private var updateSignal: Observable<Void> {
        Observable.merge(
            UIApplication.shared.rx.currentAndChangedAppState.filter { $0 == .active }.map { _ in Void() },
            lucaPreferences.changes(\.userRegistrationData).map { _ in Void() },
            lucaPreferences.changes(\.uuid).map { _ in Void() },
            personRepo.onDataChanged,
            documentRepo.onDataChanged,
            documentRepoService.documentUpdateSignal
        )
    }

    var revalidation: Observable<Void> {
        return updateSignal.flatMap { _ -> Observable<Void> in
            self.revalidateIfNeeded().andThen(Observable.just(Void()))
        }
    }

    private func revalidateIfNeeded() -> Completable {
        return getLastRevalidationDate()
            .flatMapCompletable { date in
                if self.revalidationNeedsUpdate(for: date) {
                    return self.documentProcessingService.revalidateSavedTests()
                        .andThen(self.keyValueRepo.store(self.revalidationKey, value: self.timeProvider.now))
                }

                return Completable.empty()
            }
    }

    private func revalidationNeedsUpdate(for date: Date?) -> Bool {
        guard let date = date,
              let validUntil = Calendar.current.date(byAdding: .hour, value: 1, to: date) else { return true }
        return timeProvider.now > validUntil
    }

    private func getLastRevalidationDate() -> Single<Date?> {
        keyValueRepo.load(revalidationKey)
            .catch { _ in Single.just(nil) }
    }
}
