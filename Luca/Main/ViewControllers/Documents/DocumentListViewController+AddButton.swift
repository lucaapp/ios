import Foundation
import UIKit

extension DocumentListViewController {

    @IBAction func addTestPressed(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(cancelAlertAction())
        actionSheet.addAction(addDocumentAlertAction())
        actionSheet.addAction(appointmentAlertAction())
        present(actionSheet, animated: true)
    }

    @objc func calendarTapped() {
        let calendarURLString = "https://www.luca-app.de/coronatest/search"
        if let url = URL(string: calendarURLString) {
            UIApplication.shared.open(url)
        }
    }
}

private extension DocumentListViewController {

    func cancelAlertAction() -> UIAlertAction {
        let action = UIAlertAction(title: L10n.IOSApp.Navigation.Basic.cancel, style: .cancel)
        action.accessibilityLabel = L10n.IOSApp.Navigation.Basic.cancel
        return action
    }

    func addDocumentAlertAction() -> UIAlertAction {
        let action = UIAlertAction(title: L10n.IOSApp.My.Luca.Add.document, style: .default) { [weak self] _ in
            self?.presentQRCodeScanner()
        }
        action.accessibilityLabel = L10n.IOSApp.My.Luca.Add.document
        return action
    }

    func appointmentAlertAction() -> UIAlertAction {
        let action = UIAlertAction(title: L10n.IOSApp.My.Luca.calendar, style: .default) { [weak self] _ in
            self?.calendarTapped()
        }
        action.accessibilityLabel = L10n.IOSApp.My.Luca.calendar + L10n.IOSApp.External.Link.Button.accessibility
        return action
    }
}
