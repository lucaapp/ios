import UIKit
import RxSwift
import RxCocoa
import LucaUIComponents
import DependencyInjection
import JGProgressHUD

// swiftlint:disable file_length
class DocumentListViewController: UIViewController, HandlesLucaErrors {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.userService) private var userService
    @InjectStatic(\.lucaIDService) private var lucaIDService
    @InjectStatic(\.stagedRolloutService) private var stagedRolloutService
    @InjectStatic(\.lucaIDTermsOfUse) private var lucaIDToS

    @IBOutlet weak var addButton: LightStandardButton!
	@IBOutlet weak var notificationStackView: UIStackView!

    @IBOutlet weak var emptyStateView: UIStackView!
    @IBOutlet weak var welcomeImageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!

    weak var timeDifferenceView: TimeDifferenceView?
    var addChildrenButtonView: AddChildrenBarButtonView?

    var notificationCenter: NotificationCenter = NotificationCenter.default
    var application: Application = UIApplication.shared
    var mainScheduler: SchedulerType = MainScheduler.instance
    var mainSchedulerAsync: SchedulerType = MainScheduler.asyncInstance

    private var scanningDisposeBag: DisposeBag?
    private var lucaIDDisposeBag: DisposeBag?

    private var testScannerNavController: UINavigationController?
    private var loadingHUD = JGProgressHUD.lucaLoading()

    #if PRODUCTION || PREPROD
    let idDeeplink = "https://mls.idnow.de/"
    #else
    let idDeeplink = "https://mls.test.idnow.de/"
    #endif

    var viewModel: DocumentListViewModel!
    private var data: [Person: [Document]] = [:]
    private var disposeBag: DisposeBag?

    /// Current registration data. It will always be refreshed after every data load.
    private var currentRegistrationData: UserRegistrationData?

    var idInfoButtonView: UIView?

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        applyToViewAndChildrenControllers(theme: .dark)

		setup()

        setupNavigationBar()

        applyToViewAndChildrenControllers(theme: .dark)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: animated)

        _ = self.viewModel
            .isTimeInSync
            .do(onNext: { isValid in
                // only show the view, if there is a time difference > 5 min
                isValid ? self.hideTimeDifferenceView() : self.showTimeDifferenceView()
            })
            .asObservable()
            .take(until: rx.viewDidDisappear)
            .subscribe()

        _ = Observable.combineLatest(
                viewModel.persons.asObservable(),
                lucaIDService.lucaIDProcessChanges,
                lucaIDService.idProcessStartedCurrentAndChanges,
                stagedRolloutService.isFeatureEnabled(type: .lucaId),
                lucaIDToS.isAcceptedChanges.map { $0 ?? false }
            )
            .observe(on: mainSchedulerAsync)
            .do(onNext: { [weak self] in
                guard let self = self else { throw RxError.unknown }
                self.data = $0.0
                let termsAccepted = $0.4
                self.updateUI(with: $0.0, idData: $0.1, idProcessStarted: $0.2, isLucaIdEnabled: $0.3, lucaIdTermsAccepted: termsAccepted)
                self.loadingHUD.dismiss(animated: true)
            }, onSubscribe: { [weak self] in
                guard let self = self else { return }
                self.loadingHUD.show(in: self.view)
            })
            // It is crucial that this View Controller stays retain cycle free.
            // If not, this stream would run forever and all used resources would run even after user reset.
            .take(until: rx.deallocating)

            // Break this stream if deallocating wouldn't work and user has been deleted.
            .take(until: userService.onUserDeletedRx)
            .take(until: rx.viewDidDisappear)
            .subscribe()
        subscribeToRevalidation()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupAccessibility()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        lucaIDDisposeBag = nil
    }
}

// MARK: - Setup

extension DocumentListViewController {

    private func subscribeToRevalidation() {
        let newDisposeBag = DisposeBag()
        viewModel.revalidation
            .subscribe()
            .disposed(by: newDisposeBag)
        disposeBag = newDisposeBag
    }

    private func setup() {
        notificationStackView.removeAllArrangedSubviews()
        notificationStackView.isHidden = true

        emptyStateView.layoutMargins = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        emptyStateView.isLayoutMarginsRelativeArrangement = true

        setupApplicationStateObserver()
    }

    private func setupAddIdInfoButtonView() {
        self.removeIdInfoViews()
        let view = LucaIdButtonFactory.setupAddIdInfoButtonView(tapGestureRecognizer: UITapGestureRecognizer(target: self, action: #selector(didPressAddIdView(_:))))
        setupIdInfoView(view)
    }

    private func setupPendingIdInfoView(ident: String) {
        self.removeIdInfoViews()
        let view = LucaIdButtonFactory.setupPendingIdInfoView(
            ident: ident,
            tapGestureRecognizer: UITapGestureRecognizer(
                target: self,
                action: #selector(didTapPending(_:))),
            longPressGestureRecognizer: UILongPressGestureRecognizer(
                target: self,
                action: #selector(didLongPressPending(_:)))
        )
        setupIdInfoView(view)
    }

    @objc func didTapPending(_ gesture: UITapGestureRecognizer) {
        _ = lucaIDService.fetchState()
            .observe(on: mainScheduler)
            .subscribe(onSuccess: { data in
                if let data = data, let ident = data.receiptJWS?.claims.autoIdentId, let url = URL(string: self.idDeeplink + "\(ident)") {
                    if self.application.canOpenURL(url) {
                        self.application.open(url, options: [:], completionHandler: nil)
                    }
                }
            }, onFailure: { error in
                self.processErrorMessages(error: error, completion: nil)
            })
    }

    @objc func didLongPressPending(_ gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            let newDisposeBag = DisposeBag()
            showDeletionAlert()
                .filter { $0 }
                .asObservable()
                .flatMap { _ in self.lucaIDService.archiveData() }
                .subscribe(onError: { error in
                    self.processErrorMessages(error: error)
                })
                .disposed(by: newDisposeBag)

            lucaIDDisposeBag = newDisposeBag
        }
    }

    private func showDeletionAlert() -> Single<Bool> {
        UIAlertController.okAndCancelAlertRx(viewController: self, title: L10n.IOSApp.Id.deleteDialogId, message: "", okTitle: L10n.IOSApp.Navigation.Basic.yes)
    }

    private func setupQueuedIdInfoView() {
        self.removeIdInfoViews()
        setupIdInfoView(LucaIdButtonFactory.setupQueuedIdInfoView())
    }

    private func setupIdInfoView(_ view: UIView) {
        DispatchQueue.main.async {
            self.idInfoButtonView = view

            guard let idInfoButtonView = self.idInfoButtonView else { return }
            self.stackView.addArrangedSubview(idInfoButtonView)
        }
    }

    private func removeIdInfoViews() {
        DispatchQueue.main.async {
            if let idInfoView = self.idInfoButtonView {
                self.stackView.removeArrangedSubview(idInfoView)
                idInfoView.removeFromSuperview()
                self.idInfoButtonView = nil
            }
        }
    }

    private func showFailedAlert() {
        self.removeIdInfoViews()
        _ = UIAlertController.infoAlertRx(viewController: self, title: L10n.IOSApp.Id.idVerificationFailureHeadline, message: L10n.IOSApp.Id.idGenericStatusShortText)
            .ignoreElementsAsCompletable()
            .andThen(self.lucaIDService.archiveData())
            .subscribe(on: MainScheduler.instance)
            .subscribe()
    }

    @objc func didPressAddIdView(_ gesture: UITapGestureRecognizer) {
        let interactor = LucaIdInteractor(presenter: self)
        _ = interactor
            .interact()
            .subscribe()
    }

    private func setupNavigationBar() {
        set(title: L10n.IOSApp.My.Luca.title)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        addChildrenButtonView = AddChildrenBarButtonView(colorMode: .light)
        addChildrenButtonView?.delegate = self
        addChildrenButtonView?.button.accessibilityLabel = L10n.IOSApp.Children.List.title
        let addPersonButton = UIBarButtonItem(customView: addChildrenButtonView!)
        navigationItem.rightBarButtonItems = [addPersonButton]

        scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 43, right: 0)
    }

    func setupStackView(with data: [Person: [Document]], idData: LucaIDParsedData? = nil, idProcessStarted: Bool, isLucaIdEnabled: Bool, lucaIdTermsAccepted: Bool) {
        stackView.removeAllArrangedSubviews()

        var groupViews: [DocumentGroupView] = []
        data.forEach { (person, docs) in
            guard !docs.isEmpty else { return }
            let sortedDocs = sortDocuments(docs)
            let itemViews: [DocumentView] = sortedDocs.compactMap {
                DocumentViewFactory.createView(for: $0, owner: person, isExpanded: false, with: self)
            }
            for item in itemViews where item.warningPressed == nil {
                item.warningPressed = {
                    let alert = UIAlertController.infoAlert(title: L10n.IOSApp.Document.Verification.Failed.title, message: L10n.IOSApp.Document.Verification.Failed.description)
                    self.present(alert, animated: true, completion: nil)
                }
            }
            let groupView = DocumentGroupView(person: person, views: itemViews)
            groupViews.append(groupView)
        }

        groupViews.filter { ($0.person.type == .user) }.forEach { stackView.addArrangedSubview( $0 ) }
        groupViews.filter { ($0.person.type == .placeholder) }.forEach { stackView.addArrangedSubview( $0 ) }

        let childrenViews = groupViews.filter { ($0.person.type == .child) }
        if !childrenViews.isEmpty {
            stackView.addArrangedSubview(DocumentViewFactory.createSeparator())
            childrenViews.forEach { stackView.addArrangedSubview( $0 ) }
        }

        guard isLucaIdEnabled, lucaIdTermsAccepted else { return }

        if !idProcessStarted {
            setupAddIdInfoButtonView()
            return
        }

        guard let idData = idData else { return }

        switch idData.state.state {
        case .pending: setupPendingIdInfoView(ident: idData.receiptJWS?.claims.autoIdentId ?? L10n.IOSApp.Id.Ident.missingId)
        case .queued: setupQueuedIdInfoView()
        case .success: removeIdInfoViews()
        case .failed: showFailedAlert()
        }
    }

    private func updateUI(with data: [Person: [Document]], idData: LucaIDParsedData?, idProcessStarted: Bool, isLucaIdEnabled: Bool, lucaIdTermsAccepted: Bool) {
        let showLucaID = lucaIdTermsAccepted && (idData != nil || !idProcessStarted) && isLucaIdEnabled
        toggleEmptyState(data.values.flatMap { $0 }.isEmpty, showLucaID: showLucaID)
        setupStackView(with: data, idData: idData, idProcessStarted: idProcessStarted, isLucaIdEnabled: isLucaIdEnabled, lucaIdTermsAccepted: lucaIdTermsAccepted)
        let childrenCount = data.keys.filter { $0.type == .child }.count
        addChildrenButtonView?.setCount(childrenCount)
    }

    private func sortDocuments(_ documents: [Document]) -> [Document] {
        // sort by date
        var sortedDocs = documents.sorted(by: { $0.issuedAt.compare($1.issuedAt) == .orderedDescending })

        // if latest vaccine is not valid yet, present first valid vaccine instead
        if let index = sortedDocs.firstIndex(where: { doc in
            if let vaccination = doc as? Vaccination {
                return vaccination.isComplete()
            }
            return false
        }),
        index > 0 {
            let vaccination = sortedDocs.remove(at: index)
            sortedDocs.insert(vaccination, at: 0)
        }

        return sortedDocs
    }

    private func setupApplicationStateObserver() {
        notificationCenter.addObserver(
            self,
            selector: #selector(applicationDidEnterBackground(_:)),
            name: UIApplication.didEnterBackgroundNotification,
            object: nil)
    }

    @objc
    func applicationDidEnterBackground(_ notification: NSNotification) {
        self.testScannerNavController?.dismiss(animated: true, completion: {
            self.testScannerNavController = nil
        })
    }

    private func toggleEmptyState(_ isEmpty: Bool, showLucaID: Bool) {
        emptyStateView.isHidden = !isEmpty
        stackView.isHidden = isEmpty && !showLucaID
        welcomeImageView.isHidden = !isEmpty || showLucaID
    }
}

// MARK: - Actions

extension DocumentListViewController {

    func presentQRCodeScanner() {
        testScannerNavController = ViewControllerFactory.Document.createTestQRScannerViewController()
        if let scanner = testScannerNavController {
            scanner.modalPresentationStyle = .overFullScreen
            scanner.definesPresentationContext = true
            present(scanner, animated: true, completion: nil)
        }
        if let scannerVC = testScannerNavController?.viewControllers.compactMap({ $0 as? TestQRCodeScannerController }).first {

            let newDisposeBag = DisposeBag()

            scannerVC.scan
                .observe(on: mainSchedulerAsync)
                .take(1)
                .take(until: scannerVC.rx.viewDidDisappear)
                .do(onNext: { (qrType: QRType) in
                    self.testScannerNavController?.dismiss(animated: true, completion: nil)
                    if case .checkin = qrType {
                        (self.tabBarController as? MainTabBarViewController)?.changeTabBar(to: .checkin)
                    }
                })
                .subscribe()
                .disposed(by: newDisposeBag)

            scanningDisposeBag = newDisposeBag
        }
    }

    private func delete(document: Document, viewController: UIViewController) {
        let alert = UIAlertController.yesOrNo(title: L10n.IOSApp.Test.Delete.title, message: document.deletionConfirmationMessage, onYes: {
            _ = self.viewModel.remove(document: document)
                .observe(on: MainScheduler.instance)
                .do(onError: { error in
                    let alert = UIAlertController.infoAlert(title: L10n.IOSApp.Navigation.Basic.error, message: L10n.IOSApp.Test.Result.Delete.error)
                    viewController.present(alert, animated: true, completion: nil)
                }, onCompleted: { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                })
                .subscribe()
        })

        viewController.present(alert, animated: true, completion: nil)
    }
}

// MARK: - Timesync

extension DocumentListViewController {

    private func showTimeDifferenceView() {
        if timeDifferenceView == nil {
            timeDifferenceView = TimeDifferenceView.fromNib()
            notificationStackView.addArrangedSubview(timeDifferenceView!)
        }
        notificationStackView.isHidden = false

        timeDifferenceView?.isHidden = false
    }

    private func hideTimeDifferenceView() {
        timeDifferenceView?.isHidden = true
        notificationStackView.isHidden = true
    }
}

extension DocumentListViewController: UnsafeAddress, LogUtil {}

// MARK: - Delegates

extension DocumentListViewController: DocumentViewDelegate {
    func didTapDelete(_ view: UIView, for document: Document) {
        // Not used
    }

    func didSelect(_ view: UIView, document: Document) {

        guard let documentView = view as? DocumentView,
              let person = documentView.owner else { return }

        var documents: [Document] = []
        if document is Vaccination {
            documents = self.data[person]?.filter { $0 is Vaccination } ?? []
        } else {
            documents.append(document)
        }

        if document is LucaID {
            viewModel.authenticateDeviceUser { [weak self] success in
                guard let self = self else {return}
                if success {
                    self.showDetailView(documents: documents, person: person, document: document)
                } else {
                    let alert = UIAlertController.infoAlert(title: L10n.IOSApp.Id.Authentication.Failed.title, message: L10n.IOSApp.Id.Authentication.Failed.message)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        } else {
            self.showDetailView(documents: documents, person: person, document: document)
        }
    }

    private func showDetailView(documents: [Document], person: Person, document: Document) {
        let viewController = ViewControllerFactory.Document.createDocumentViewController(
            documents: self.sortDocuments(documents),
            owner: person,
            focusedDocument: document,
            delegate: self
        )

        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension DocumentListViewController: DocumentViewControllerDelegate {
    func didTapDelete(for document: Document, viewController: UIViewController) {
        delete(document: document, viewController: viewController)
    }
}

extension DocumentListViewController: AddChildrenBarButtonViewDelegate {
    func didTapButton() {
        let viewController = ViewControllerFactory.Children.createChildrenListViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

// MARK: - Accessibility

extension DocumentListViewController {

    private func setupAccessibility() {
        addButton.accessibilityLabel = L10n.IOSApp.Test.Add.title
        UIAccessibility.setFocusTo(navigationbarTitleLabel, notification: .layoutChanged, delay: 0.8)
    }
}
