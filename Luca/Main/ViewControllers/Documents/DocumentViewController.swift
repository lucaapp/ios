import UIKit
import RxSwift

protocol DocumentViewControllerDelegate: AnyObject {
    func didTapDelete(for document: Document, viewController: UIViewController)
}

class DocumentViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!

    weak var delegate: DocumentViewControllerDelegate?
    var owner: Person!
    var documents: [Document] = []
    var focusedDocument: Document?

    private var disposeBag: DisposeBag?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBar()
        setup()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        scrollToDocument()
    }
}

extension DocumentViewController {

    private func setup() {
        view.backgroundColor = .black
        setupStackView()
        setupAccessibility()
    }

    private func setupNavigationBar() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        set(title: L10n.IOSApp.My.Luca.title)
    }

    private func setupStackView() {
        stackView.removeAllArrangedSubviews()
        let itemViews: [DocumentView] = documents.compactMap { DocumentViewFactory.createView(for: $0, owner: owner, isExpanded: true, with: self) }
        for item in itemViews where item.warningPressed == nil {
            item.warningPressed = {
                let alert = UIAlertController.infoAlert(title: L10n.IOSApp.Document.Verification.Failed.title, message: L10n.IOSApp.Document.Verification.Failed.description)
                self.present(alert, animated: true, completion: nil)
            }
        }
        DocumentViewFactory.group(views: itemViews).forEach { stackView.addArrangedSubview($0) }
    }

    private func scrollToDocument() {
        guard let horizontalView = stackView.arrangedSubviews.first as? HorizontalDocumentListView, let focusedDocument = focusedDocument else { return }

        horizontalView.scroll(to: focusedDocument)
    }
}

extension DocumentViewController: DocumentViewDelegate {
    func didTapDelete(_ view: UIView, for document: Document) {
        delegate?.didTapDelete(for: document, viewController: self)
    }

    func didSelect(_ view: UIView, document: Document) {
        // Not used
    }
}

// MARK: - Accessibility

extension DocumentViewController {

    private func setupAccessibility() {
        UIAccessibility.setFocusTo(navigationbarTitleLabel, notification: .layoutChanged, delay: 0.8)
    }

}
