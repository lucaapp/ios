import UIKit
import RxSwift
import JGProgressHUD
import DependencyInjection

class DataAccessViewController: UIViewController {

    @InjectStatic(\.accessedTraceIdRepo) private var accessedTraceIdRepo
    @InjectStatic(\.accessTraceIdChecker) private var accessTraceIdChecker

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyStateView: UIView!

    private var progressHud = JGProgressHUD.lucaLoading()
    var dataAccesses: [SectionedAccessedTraceId] = []
    var disposeBag: DisposeBag?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        setupNavigationbar()
    }

    func loadEntries() {
        let newDisposeBag = DisposeBag()
        self.progressHud.show(in: self.view)

        accessedTraceIdRepo.restore()
            .flatMap { self.accessTraceIdChecker.sort(data: $0) }
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { [weak self] data in
                data.isEmpty ? self?.showEmptyState() : self?.hideEmptyState()
                self?.dataAccesses = data
                self?.tableView.reloadData()
            }, onDispose: { [weak self] in self?.progressHud.dismiss() })
            .subscribe()
            .disposed(by: newDisposeBag)
        disposeBag = newDisposeBag
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadEntries()
        setupAccessibility()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposeBag = nil
    }
}

extension DataAccessViewController {

    private func setupUI() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(DataAccessHeaderView.self, forHeaderFooterViewReuseIdentifier: "DataAccessHeaderView")
    }

    private func setupNavigationbar() {
        set(title: L10n.IOSApp.Navigation.DataAccess.title)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    func showEmptyState() {
        emptyStateView.isHidden = false
        tableView.isHidden = true
    }

    func hideEmptyState() {
        emptyStateView.isHidden = true
        tableView.isHidden = false
    }

}
extension DataAccessViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return dataAccesses.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataAccesses[section].accesses.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // swiftlint:disable:next force_cast
        let cell = tableView.dequeueReusableCell(withIdentifier: "DataAccessTableViewCell", for: indexPath) as! DataAccessTableViewCell
        let dataAccess = dataAccesses[indexPath.section].accesses[indexPath.row]

        cell.locationName.text = dataAccess.location.formattedName
        let checkin = dataAccess.traceInfo.checkInDate
        if let checkout = dataAccess.traceInfo.checkOutDate {
            cell.dateLabel.text = "\(checkin.formattedDateTime) - \(checkout.formattedDateTime)"
            cell.dateLabel.accessibilityLabel = L10n.IOSApp.History.Checkin.Checkout.time(checkin.accessibilityDate, checkout.accessibilityDate)
        } else {
            cell.dateLabel.text = "\(checkin.formattedDateTime)"
            cell.dateLabel.accessibilityLabel = checkin.accessibilityDate
        }

        return cell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // swiftlint:disable:next force_cast
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "DataAccessHeaderView") as! DataAccessHeaderView
        view.departmentLabel.text = dataAccesses[section].healthDepartment.name

        return view
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }

}

// MARK: - Accessibility
extension DataAccessViewController {
    private func setupAccessibility() {
        guard let navigationbarTitleLabel = navigationbarTitleLabel else { return }
        navigationbarTitleLabel.accessibilityTraits = .header
        UIAccessibility.setFocusTo(navigationbarTitleLabel, notification: .layoutChanged, delay: 0.8)
    }
}
