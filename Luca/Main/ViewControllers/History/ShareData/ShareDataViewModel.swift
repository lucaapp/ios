import Foundation
import RxSwift
import RxCocoa
import DependencyInjection

class ShareDataViewModel: LogUtil, UnsafeAddress {
    @InjectStatic(\.userService) private var userService

	private static let numberOfDays = 14
	private static let availableDays = Array(1...ShareDataViewModel.numberOfDays)

	let items: Observable<[Int]> = Observable.of(Array(1...14))

	let selectedNumberOfDays = BehaviorRelay<(row: Int, component: Int)>(value: (ShareDataViewModel.numberOfDays-1, 0))

    func generateTAN() -> Single<String> {
        userService.transferUserData(forNumberOfDays: selectedNumberOfDays.value.row)
            .map {
                String($0
                    .uppercased()
                    .split(every: 4)
                    .reduce("") { "\($0)-\($1)" }
                    .dropFirst()
                )
            }
    }
}
