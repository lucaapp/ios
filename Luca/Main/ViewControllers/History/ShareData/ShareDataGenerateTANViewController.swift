import UIKit
import LucaUIComponents
import RxCocoa
import RxSwift

class ShareDataGenerateTANViewController: UIViewController, BindableType, LucaModalAppearance, HandlesLucaErrors {
	@IBOutlet weak var descriptionLabel: Luca14PtLabel!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet weak var continueButton: LightStandardButton!
    @IBOutlet weak var titleLabel: Luca20PtBoldLabel!

	private let disposeBag = DisposeBag()
	weak var viewModel: ShareDataViewModel!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.titleLabel.text = L10n.IOSApp.History.Share.GenerateTAN.title.string
		self.descriptionLabel.text = L10n.IOSApp.History.Alert.description(viewModel.selectedNumberOfDays.value.row+1, L10n.IOSApp.History.Alert.link)
		for state in [UIControl.State.normal, UIControl.State.highlighted, UIControl.State.disabled] {
			self.continueButton.setTitle(L10n.IOSApp.History.Share.GenerateTAN.continueButtonTitle.uppercased(), for: state)
		}
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.stopAnimating()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		applyColors()

        bindViewModel()
	}

	func bindViewModel() {
        _ = continueButton.rx
            .tap
            .flatMapFirst { _ in
                self.viewModel.generateTAN()
                .subscribe(on: MainScheduler.instance)
                .observe(on: MainScheduler.instance)
                .do(onSubscribe: {
                    self.continueButton.isEnabled = false
                    self.activityIndicator.startAnimating()
                })
                .do(onDispose: {
                    self.continueButton.isEnabled = true
                    self.activityIndicator.stopAnimating()
                })
                .do(onSuccess: { self.performSegue(withIdentifier: "displayTAN", sender: $0) })
                .handleErrors(self)
            }
            .take(until: rx.viewWillDisappear)
            .subscribe()
	}

	// MARK: - Navigation

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destination.
	// Pass the selected object to the new view controller.
		if let TAN = sender as? String,
			 let vc = segue.destination as? ShareDataDisplayTANViewController,
			 segue.identifier == "displayTAN" {
			vc.TAN = TAN
		}
	}

}
