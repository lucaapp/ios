import UIKit
import LucaUIComponents
import RxSwift
import RxCocoa

class ShareDataSelectTimeframeViewController: UIViewController, BindableType, LucaModalAppearance {
	@IBOutlet weak var descriptionLabel: Luca14PtLabel!
	@IBOutlet weak var daysPickerview: UIPickerView!
	@IBOutlet weak var continueButton: LightStandardButton!
    @IBOutlet weak var titleLabel: Luca20PtBoldLabel!

	var viewModel: ShareDataViewModel!

	override func viewDidLoad() {
		super.viewDidLoad()
        self.titleLabel.text = L10n.IOSApp.History.Share.SelectTimeFrame.title.string
		self.descriptionLabel.text = L10n.IOSApp.History.Share.SelectTimeFrame.description.string
		for state in [UIControl.State.normal, UIControl.State.highlighted, UIControl.State.disabled] {
			self.continueButton.setTitle(L10n.IOSApp.History.Share.SelectTimeFrame.continueButtonTitle.uppercased(), for: state)
		}
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		applyColors()
        bindViewModel()
	}

	// MARK: - Navigation

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destination.
	// Pass the selected object to the new view controller.
		if let c = segue.destination as? ShareDataGenerateTANViewController {
			c.viewModel = viewModel
		}
	}

	func bindViewModel() {
		_ = viewModel.items
            .take(until: rx.viewWillDisappear)
			.bind(to: daysPickerview.rx.items) { _, value, _ in
				let l = Luca20PtBoldLabel(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
				l.textAlignment = .center
				l.text = "\(value)"
				return l
			}

		_ = daysPickerview.rx.itemSelected
            .take(until: rx.viewWillDisappear)
			.bind(to: viewModel.selectedNumberOfDays)

		self.daysPickerview.selectRow(viewModel.selectedNumberOfDays.value.row, inComponent: 0, animated: true)
	}

	@IBAction func dismissButtonPressed(_ sender: Any) {
		self.dismiss(animated: true, completion: nil)
	}

}
