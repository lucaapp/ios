import UIKit
import LucaUIComponents
import RxCocoa
import RxSwift

class ShareDataDisplayTANViewController: UIViewController, LucaModalAppearance {
	@IBOutlet weak var descriptionLabel: Luca14PtLabel!
	@IBOutlet weak var tanLabel: TANLabel!
	@IBOutlet weak var continueButton: LightStandardButton!
    @IBOutlet weak var titleLabel: Luca20PtBoldLabel!
    var TAN: String!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.titleLabel.text = L10n.IOSApp.History.Share.DisplayTAN.title.string
		self.descriptionLabel.text = L10n.IOSApp.History.Share.DisplayTAN.description.string
		for state in [UIControl.State.normal, UIControl.State.highlighted, UIControl.State.disabled] {
			self.continueButton.setTitle(L10n.IOSApp.History.Share.DisplayTAN.continueButtonTitle.uppercased(), for: state)
		}
		self.tanLabel.text = TAN
		self.tanLabel.accessibilityLabel = TAN.map { String($0) + " " }.joined()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		applyColors()
	}

	@IBAction func finishButtonPressed(_ sender: Any) {
		dismiss(animated: true, completion: nil)
	}

}
