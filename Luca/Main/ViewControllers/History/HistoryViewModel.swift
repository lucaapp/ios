import UIKit
import RxSwift
import RxCocoa
import DependencyInjection

struct HistoryEventViewModel {
    var event: HistoryEvent
    var persons: [Person] = []
    var accessedTraceIds: [AccessedTraceId] = []
}

final class HistoryViewModel: LogUtil, UnsafeAddress {

    // MARK: Bindings

    let isLoadingEvents = BehaviorRelay<Bool>(value: true)

    var shareButtonEnabled: Driver<Bool> {
        let isEmpty = data.map { $0.isEmpty }.asDriver(onErrorJustReturn: false)
        return Driver.combineLatest(isEmpty, dailyPublicKeyVerifier.isKeyValid) { isEmpty, isKeyValid in
            return !isEmpty && isKeyValid
        }
    }

    var data: Observable<[HistoryEventViewModel]> {
        updateSignal
            .flatMapLatest { _ in
                Observable.zip(
                    self.events,
                    self.children,
                    self.accessedTraceIdRepo.restore().asObservable()
                )
            }
            .map { events, children, accessesTraceIds -> [HistoryEventViewModel] in
                events.map { event in
                    if let userEvent = event as? UserEvent {

                        var filteredTraceIds = accessesTraceIds.filter { $0.traceId == userEvent.checkin.traceInfo?.traceId }

                        if let filter = self.filterForWarningLevel {
                            filteredTraceIds = filteredTraceIds.filter { $0.warningLevel == filter }
                        }

                        return HistoryEventViewModel(
                            event: userEvent,
                            persons: children[userEvent.checkin.traceInfo?.traceId ?? ""] ?? [],
                            accessedTraceIds: filteredTraceIds
                        )
                    }
                    return HistoryEventViewModel(event: event)
                }
            }
            .map { viewModels in
                if self.filterForWarningLevel != nil {
                    return viewModels.filter { !$0.accessedTraceIds.isEmpty }
                }
                return viewModels
            }
    }

    /// contains mapping of traceId to children in HistoryEvent
    private var children: Observable<[String: [Person]]> {
        events.flatMap { self.loadChildren(for: $0) }
    }

    private var events: Observable<[HistoryEvent]> {
        updateSignal.flatMap { _ in self.loadEvents() }
    }

    private var updateSignal: Observable<Void> {
        Observable.merge(
            Observable.just(Void()),
            historyService.onEventAddedRx.map { _ in Void() },
            historyRepo.onDataChanged,
            accessedTraceIdRepo.onDataChanged
        )
    }

    // MARK: Init

    @InjectStatic(\.personService) private var personService: PersonService
    @InjectStatic(\.history) private var historyService: HistoryService
    @InjectStatic(\.historyRepo) var historyRepo
    @InjectStatic(\.accessedTraceIdRepo) var accessedTraceIdRepo
    @InjectStatic(\.dailyPublicKeyVerifier) var dailyPublicKeyVerifier
    let filterForWarningLevel: UInt8?

    init(filterForWarningLevel: UInt8? = nil) {
        self.filterForWarningLevel = filterForWarningLevel
    }

    private func loadEvents() -> Single<[HistoryEvent]> {
        historyService
            .removeOldEntries()
            .andThen(historyService.historyEvents)
            .observe(on: MainScheduler.instance)
            .do(onSubscribe: {
                self.isLoadingEvents.accept(true)
            })
            .do(onDispose: {
                self.isLoadingEvents.accept(false)
            })
            .map { $0.reversed() }
    }

    private func loadChildren(for events: [HistoryEvent]) -> Single<[String: [Person]]> {
        Observable.from(events)
            .flatMap { event in
                self.loadChildren(for: event)
            }
            .toArray()
            .map { tupleArray in
                var children: [String: [Person]] = [:]
                _ = tupleArray
                    .filter { _, persons in
                        persons.count > 0
                    }
                    .map { traceId, persons in
                        children[traceId] = persons
                    }
                return children
            }
    }

    private func loadChildren(for event: HistoryEvent) -> Maybe<(String, [Person])> {
        if let userEvent = event as? UserEvent,
           let traceInfo = userEvent.checkout?.traceInfo ?? userEvent.checkin.traceInfo {
            return personService.retrieveAssociated(with: traceInfo)
                .map { (traceInfo.traceId, $0) }
                .asMaybe()
        }
        return Maybe.empty()
    }
}
