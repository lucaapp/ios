import UIKit
import RxSwift
import RxCocoa
import DependencyInjection

class HistoryViewController: UIViewController, BindableType, DisplaysProgress, HandlesLucaErrors {
    @InjectStatic(\.dataResetService) private var dataResetService

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var shareHistoryButton: UIButton?
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var noDataView: UIStackView?
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    var viewModel: HistoryViewModel!
    var deleteHistoryButton: UIBarButtonItem!

    var preserveBinding: Bool = false
    var disposeBag: DisposeBag! = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        title = L10n.IOSApp.Navigation.Tab.history.string
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        deleteHistoryButton = UIBarButtonItem(image: Asset.deleteBin.image, style: .plain, target: self, action: #selector(deleteHistoryPressed))
        deleteHistoryButton.accessibilityLabel = L10n.IOSApp.History.Delete.button
        shareHistoryButton?.setTitle(L10n.IOSApp.History.Data.share.uppercased(), for: .normal)

        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(eventLongPressed(sender:)))
        tableView.addGestureRecognizer(longPressRecognizer)
        tableView.tableFooterView = UIView()
        tableView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 100, right: 0)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if !preserveBinding {
            bindViewModel()
        }
        preserveBinding = false
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !preserveBinding {
            disposeBag = nil
        }
    }

    func bindViewModel() {
        disposeBag = DisposeBag()
        bindCells()
        viewModel.isLoadingEvents
            .observe(on: MainScheduler.instance)
            .bind { [weak self] loading in
                if loading {
                    self?.tableView.isHidden = true
                    self?.noDataView?.isHidden = true
                    self?.shareHistoryButton?.isHidden = true
                    self?.displayLoadingIndicator()
                } else {
                    self?.hideLoadingIndicator()
                }
            }
            .disposed(by: disposeBag)

        viewModel.shareButtonEnabled.drive(shareHistoryButton!.rx.isEnabled).disposed(by: disposeBag)
    }

    func bindCells() {
        var count = 0
        viewModel
            .data
            .observe(on: MainScheduler.instance)
            .do(onNext: { count = $0.count })
            .do(onNext: { [weak self] in self?.setupViews(for: $0)})
            .bind(to: tableView.rx
                    .items(cellIdentifier: "HistoryTableViewCell")) { index, event, cell in

                guard let cell = cell as? HistoryTableViewCell else {
                    return
                }

                cell.setup(
                    historyEvent: event.event,
                    children: event.persons,
                    hasAccessedTraceIds: !event.accessedTraceIds.isEmpty,
                    isRead: !event.accessedTraceIds.contains(where: { !$0.isRead })
                )

                // display lines along the cell
                if count == 1 {
                    cell.setupHistoryLineViews(position: .only)
                } else if index == count - 1 {
                    cell.setupHistoryLineViews(position: .last)
                } else if index == 0 {
                    cell.setupHistoryLineViews(position: .first)
                }
            }
            .disposed(by: disposeBag)

        tableView.rx.modelSelected(HistoryEventViewModel.self).do(onNext: { model in
            self.preserveBinding = true

            if let traceInfo = (model.event as? UserEvent)?.checkin.traceInfo {
                let accesses = model.accessedTraceIds

                if let access = accesses.first,
                   accesses.count == 1 {
                    AccessedTraceIdDetailCoordinator(presenter: self, accessedTraceId: access).start()
                    return
                } else if accesses.count > 1 {
                    WarningLevelsCoordinator(presenter: self, traceInfo: traceInfo).start()
                    return
                }
            }

            let event = model.event
            if let userEvent = event as? UserEvent, userEvent.checkin.role == .host {
                self.performSegue(withIdentifier: "showDetail", sender: event)
            }

            guard event is UserDataTransfer else {return}

            self.performSegue(withIdentifier: "showDetail", sender: event)
        })
        .subscribe()
        .disposed(by: disposeBag)
    }

    @IBAction func dataAccessPressed(_ sender: UIButton) {
        let vc = ViewControllerFactory.Main.createDataAccessViewController()
        navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func dataReleasePressed(_ sender: UIButton) {
        let navController = ViewControllerFactory.History.createShareDialogViewController()
        present(navController, animated: true, completion: nil)
    }

    private func setupViews(for items: [HistoryEventViewModel]) {
        let events = items
        tableView.isHidden = events.isEmpty
        shareHistoryButton?.isHidden = false
        noDataView?.isHidden = !events.isEmpty
        navigationItem.rightBarButtonItem = !events.isEmpty ? deleteHistoryButton : nil
        shareHistoryButton?.accessibilityLabel = events.isEmpty ? L10n.IOSApp.History.Data.Share.Deactivated.accessibility : L10n.IOSApp.History.Data.share
    }

    @objc private func deleteHistoryPressed() {
        resetLocallyAlert()
    }

    @objc private func eventLongPressed(sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            let touchPoint = sender.location(in: tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPoint),
               let event: HistoryEventViewModel = try? self.tableView.rx.model(at: indexPath),
               let userEvent = event.event as? UserEvent,
               let traceId = userEvent.checkin.traceInfo?.traceId {
                let traceIdDescription = L10n.IOSApp.History.TraceId.pasteboard(traceId)
                UIPasteboard.general.string = traceIdDescription
                let alert = UIAlertController.infoAlert(title: "", message: L10n.IOSApp.History.TraceId.Alert.description(traceIdDescription))
                present(alert, animated: true, completion: nil)
            }
        }
    }

    func resetLocallyAlert() {
        UIAlertController(
            title: L10n.IOSApp.Data.Clear.title,
            message: L10n.IOSApp.Data.Clear.description,
            preferredStyle: .alert
        )
        .actionAndCancelAlert(actionText: L10n.IOSApp.Data.Clear.title, action: {
            _ = self.dataResetService.resetHistory().subscribe()
        }, viewController: self)
    }
}

extension HistoryViewController {

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? HistoryDetailViewController,
           let event = sender as? HistoryEvent,
           segue.identifier == "showDetail" {
            destination.event = event
        }
    }

}
