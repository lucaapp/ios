import UIKit
import LucaUIComponents
import RxCocoa
import RxSwift

class HistoryDetailViewController: UIViewController {
	@IBOutlet weak var titleLabel: Luca14PtBoldLabel!
	@IBOutlet weak var locationCheckinStackView: UIStackView!
	@IBOutlet weak var locationCheckinTitle: Luca14PtBoldLabel!
	@IBOutlet weak var locationCheckinTimeLabel: Luca14PtLabel!
	@IBOutlet weak var informationLabel: Luca14PtLabel!
	@IBOutlet weak var privateMeetingStackView: UIStackView!
	@IBOutlet weak var privateMeetingTitleLabel: Luca14PtBoldLabel!
	@IBOutlet weak var privateMeetingGuestsStackView: UIStackView!

	var event: HistoryEvent?

	override func viewDidLoad() {
		super.viewDidLoad()
		locationCheckinStackView.isHidden = true
		informationLabel.isHidden = true
		privateMeetingStackView.isHidden = true

		if let userEvent = event as? UserEvent, userEvent.checkin.role == .host {
			informationLabel.isHidden = false

			self.title = L10n.IOSApp.Private.Meeting.Info.title.string
			titleLabel.text = "\(L10n.IOSApp.History.Detail.PrivateMeeting.title)"
			informationLabel.text = "\(L10n.IOSApp.History.Detail.PrivateMeeting.information)"

			if let checkout = userEvent.checkout {
				privateMeetingTitleLabel.text = "\(L10n.IOSApp.Private.Meeting.Participants.title): \(checkout.guestlist?.count ?? 0)"
				self.setupGuestlistStack(entry: checkout)
			} else {
				privateMeetingTitleLabel.text = "\(L10n.IOSApp.Private.Meeting.Participants.title): \(userEvent.checkin.guestlist?.count ?? 0)"
				self.setupGuestlistStack(entry: userEvent.checkin)
			}
		} else if let userDataTransfer = event as? UserDataTransfer {
			informationLabel.isHidden = false
			self.title = L10n.IOSApp.History.Data.shared.string
			titleLabel.text = "\(L10n.IOSApp.History.Detail.PrivateMeeting.title)"
			let numberOfDays = userDataTransfer.entry.numberOfDaysShared ?? 14
			informationLabel.text = L10n.IOSApp.Data.Shared.description(numberOfDays)
		}
	}

	private func setupGuestlistStack(entry: HistoryEntry) {
		self.privateMeetingStackView.isHidden = false
		let uniqueGuestList = Set(entry.guestlist ?? [])
		for (index, guest) in uniqueGuestList.enumerated() {
			let label = Luca14PtLabel()
            label.text = "\(index + 1)    \(guest)"
			label.numberOfLines = 1
			self.privateMeetingGuestsStackView.addArrangedSubview(label)
		}

		privateMeetingGuestsStackView.isHidden = uniqueGuestList.isEmpty

	}

	func setup(with event: HistoryEvent) {
		self.event = event
	}

}
