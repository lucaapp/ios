import UIKit
import DependencyInjection
import RxSwift
import LucaUIComponents

class OptionViewController: UIViewController, Themable {
    var theme: AppearanceTheme = .dark

    var options: [OptionGrouping] = [] {
        didSet {
            filteredOptions = [OptionGrouping]()
            for option in options {
                let accountOptions = option.options.filter { option in
                    guard let optionalOption = option as? OptionalOption else { return true }
                    return optionalOption.isVisible }

                if !accountOptions.isEmpty {
                    filteredOptions.append(OptionGrouping(header: option.header,
                                                          footer: option.footer,
                                                          options: accountOptions))
                }
            }
        }
    }

    private var filteredOptions: [OptionGrouping] = []

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.backgroundColor = .clear
        tableView.separatorColor = .black
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 16, right: 0)
        tableView.alwaysBounceVertical = false
        tableView.showsVerticalScrollIndicator = false
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))  // remove last separator
        tableView.dataSource = self
        tableView.delegate = self

        var uniqueCells: [AccountOption] = filteredOptions
            .flatMap { $0.options }
            .unique { $0.cellIdentifier }

        for objToRegister in uniqueCells {
            tableView.register(objToRegister.classForCoder, forCellReuseIdentifier: objToRegister.cellIdentifier)
        }

        tableView.register(OptionTableViewFooter.self, forHeaderFooterViewReuseIdentifier: "sectionFooter")
        tableView.register(OptionTableViewHeader.self, forHeaderFooterViewReuseIdentifier: "sectionHeader")
        view.addSubview(tableView)
        tableView.contentOffset = CGPoint(x: 0, y: -tableView.contentInset.top)

        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationbar()

        // Need to do it because those are needed by the tableView
        generateCoordinators()

        setupContraints()
        view.backgroundColor = .black
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        generateCoordinators()
        self.applyToViewAndChildrenControllers(theme: .dark)
        tableView.reloadData()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.options = []
    }

    func generateCoordinators() {
        fatalError("Needs to be overriden")
    }

    func updateOptions(options: [OptionGrouping]) {
        self.options = options
        self.tableView.reloadData()
    }

}

extension OptionViewController {
    private func setupNavigationbar() {
        set(title: L10n.IOSApp.Navigation.Tab.account)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    private func setupContraints() {
        tableView.setAnchor(top: view.topAnchor,
                            leading: view.leadingAnchor,
                            bottom: view.bottomAnchor,
                            trailing: view.trailingAnchor,
                            padding: UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16))
    }
}

extension OptionViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return filteredOptions.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredOptions[section].options.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let option = filteredOptions[indexPath.section].options[indexPath.row]
        // swiftlint:disable:next force_cast
        let cell = tableView.dequeueReusableCell(withIdentifier: option.cellIdentifier, for: indexPath) as! AccountTableViewCell

        cell.accessibilityTraits = .button
        cell.accessoryType = .none

        cell.option = option

        if let coordinatorAccountOption = option as? CoordinatorAccountOption, coordinatorAccountOption.coordinator is OpenLinkCoordinator {
            cell.configureTitle(option.title, accessibilityTitle: option.title + L10n.IOSApp.External.Link.Button.accessibility)
        } else {
            cell.configureTitle(option.title)
        }
        option.onDidCreate(cell: cell)

        return cell
    }

}

extension OptionViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            filteredOptions[indexPath.section].options[indexPath.row].onDidSelect(cell: cell)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "sectionFooter") as? OptionTableViewFooter
        view?.label.text = filteredOptions[section].footer
        return view
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "sectionHeader") as? OptionTableViewHeader
        view?.label.text = filteredOptions[section].header
        return view
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cornerRadius = 10
        var corners: UIRectCorner = []

        if indexPath.row == 0 {
            corners.update(with: .topLeft)
            corners.update(with: .topRight)
        }

        if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            corners.update(with: .bottomLeft)
            corners.update(with: .bottomRight)
        }

        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: cell.bounds,
                                      byRoundingCorners: corners,
                                      cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
        cell.layer.mask = maskLayer
        cell.applyToSubviews(theme: theme)
    }
}
