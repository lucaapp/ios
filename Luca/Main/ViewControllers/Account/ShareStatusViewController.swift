import UIKit

 class ShareStatusViewController: OptionViewController, HandlesLucaErrors {

    override func generateCoordinators() {
        self.options = [OptionGrouping(
            footer: L10n.IOSApp.Account.Option.HealthStatus.Share.description,
            options: [
                ToggleConsentAccountOption(
                    title: L10n.IOSApp.Account.Option.HealthStatus.Share.title,
                    keyPath: \.shareHealthStatus,
                    presenter: self,
                    interactor: ShareHealthStatusInteractor(presenter: self)
                )
            ]
        )]
    }

 }
