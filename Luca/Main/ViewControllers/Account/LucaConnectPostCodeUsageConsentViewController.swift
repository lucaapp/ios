import Foundation

class LucaConnectPostCodeUsageConsentViewController: OptionViewController, HandlesLucaErrors {

    override func generateCoordinators() {
        self.options = [OptionGrouping(footer: L10n.IOSApp.Postcode.Check.Toggle.footer,
                                       options: [ToggleConsentAccountOption(title: L10n.IOSApp.Postcode.Check.Toggle.title,
                                                                            keyPath: \.lucaConnectPostCodeUsageConsent,
                                                                            presenter: self,
                                                                            interactor: LucaConnectPostCodeUsageConsentInteractor(presenter: self))])]
    }

}
