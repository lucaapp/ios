import DependencyInjection

class ExplanationSelectionViewController: OptionViewController {

    @InjectStatic(\.explanationService) private var explanationService

    override func viewDidLoad() {
        super.viewDidLoad()
        set(title: L10n.IOSApp.General.news)
    }

    override func generateCoordinators() {
        let accountOptions = explanationService.topics.map { topic -> InteractorAccountOption in
            let explanationInteractor = ShowExplanationInteractor(topic: topic, presenter: self)
            return InteractorAccountOption(title: topic.title, interactor: explanationInteractor)
        }
        self.options = [OptionGrouping(options: accountOptions)]
    }

}
