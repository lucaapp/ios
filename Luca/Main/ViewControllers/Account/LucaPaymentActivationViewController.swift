import Foundation

class LucaPaymentActivationViewController: OptionViewController, HandlesLucaErrors {

    override func viewDidLoad() {
        super.viewDidLoad()
        set(title: L10n.UserApp.Pay.Account.title)
    }

    override func generateCoordinators() {
        let lucaPayToggleOption = LucaPaymentToggleAccountOption(title: L10n.UserApp.Pay.Account.title, presenter: self)
        self.options = [OptionGrouping(footer: L10n.UserApp.Pay.Account.text,
                                       options: [lucaPayToggleOption])]
    }
}
