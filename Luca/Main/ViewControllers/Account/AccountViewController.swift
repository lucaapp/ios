import UIKit
import RxSwift
import DependencyInjection

struct AccountViewControllerInteractables {
    let lucaConnect = "lucaConnect"
    let lucaConnectPostCodeUsageConsent = "lucaConnectPostCodeUsageConsent"
    let editContactData = "editContactData"
    let directCheckin = "directCheckin"
    let lucaPayActivation = "lucaPayActivation"
}

class AccountViewController: OptionViewController, HasInteractables {

    @InjectStatic(\.lucaConnectContactDataService) private var lucaConnectContactDataService

    var interactableKeys = AccountViewControllerInteractables()
    var interactables: [Interactable] {
        options.flatMap { $0.options }.compactMap { $0 as? Interactable }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        apply(theme: .dark)
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: animated)

        _ = lucaConnectContactDataService.hasResponsibleHealthDepartment()
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { [weak self] isEligible in

                guard let self = self else { return }
                let updatedOptions = self.options

                updatedOptions
                    .flatMap { $0.options }
                    .compactMap { $0 as? LucaConnectCoordinatorAccountOption }
                    .forEach { $0.isVisible = isEligible }
                self.updateOptions(options: updatedOptions)
            })
            .subscribe()
    }

    override func generateCoordinators() {
        self.options = [
            OptionGrouping(
                header: L10n.IOSApp.Account.Settings.heading,
                options: settingsAccountOptions()
            ),
            OptionGrouping(
                header: L10n.IOSApp.Account.Help.heading,
                options: helpAccountOptions()
            ),
            OptionGrouping(
                header: L10n.IOSApp.Account.Information.heading,
                options: informationAccountOptions()
            ),
            OptionGrouping(
                options: [CoordinatorAccountOption(
                    title: L10n.IOSApp.Data.ResetData.title,
                    coordinator: DeleteAccountCoordinator(presenter: self))
                ]
            )
        ]
    }

    private func settingsAccountOptions() -> [AccountOption] {
        [PostCodeCoordinatorAccountOption(title: L10n.IOSApp.Postcode.Check.title,
                                          coordinator: PushOptionCoordinator(presenter: self, toPresent: LucaConnectPostCodeUsageConsentViewController()),
                                          icon: Asset.postCodeCheck.image,
                                          tag: interactableKeys.lucaConnectPostCodeUsageConsent),

        SettingCoordinatorAccountOption(title: L10n.IOSApp.UserData.Navigation.edit,
                                         coordinator: EditAccountCoordinator(presenter: self),
                                         icon: Asset.contactData.image,
                                         tag: interactableKeys.editContactData),

         LucaConnectCoordinatorAccountOption(title: L10n.IOSApp.Account.Option.Luca.connect,
                                             coordinator: PushOptionCoordinator(presenter: self, toPresent: LucaConnectOptionViewController()),
                                             icon: Asset.notification.image,
                                             tag: interactableKeys.lucaConnect),
         lucaPaymentActivationOption,
         //         SettingCoordinatorAccountOption(title: L10n.IOSApp.Checkin.Qr.shareHealthStatusLabel,
         //                                         coordinator: PushOptionCoordinator(presenter: self, toPresent: ShareStatusViewController()),
         //                                         icon: "2g3gStatus"),
         //         SettingCoordinatorAccountOption(title: L10n.IOSApp.Account.Option.voluntaryCheckin,
         //                                         coordinator: PushOptionCoordinator(presenter: self, toPresent: VoluntaryCheckinViewController()),
         //                                         icon: "voluntaryCheckin"),

         SettingCoordinatorAccountOption(title: L10n.IOSApp.Account.Option.directCheckin,
                                         coordinator: PushOptionCoordinator(presenter: self, toPresent: DirectCheckinViewController()),
                                         icon: Asset.directCheckin.image,
                                         tag: interactableKeys.directCheckin)]
    }

    private var lucaPaymentActivationOption: SettingCoordinatorAccountOption {
        let lucaPaymentActivationViewController = LucaPaymentActivationViewController()
        let coordinator = PushOptionCoordinator(presenter: self, toPresent: lucaPaymentActivationViewController)
        return SettingCoordinatorAccountOption(title: L10n.UserApp.Pay.Account.title, coordinator: coordinator, icon: Asset.moneyWhite.image, tag: interactableKeys.lucaPayActivation)
    }

    private func helpAccountOptions() -> [AccountOption] {
        [CoordinatorAccountOption(title: L10n.IOSApp.General.news,
                                  coordinator: PushOptionCoordinator(presenter: self, toPresent: ExplanationSelectionViewController())),
         CoordinatorAccountOption(title: L10n.IOSApp.General.faq,
                                  coordinator: OpenLinkCoordinator(url: L10n.IOSApp.WelcomeViewController.linkFAQ)),
         CoordinatorAccountOption(title: L10n.IOSApp.General.support,
                                  coordinator: SendSupportEmailCoordinator(presenter: self))]
    }

    private func informationAccountOptions() -> [AccountOption] {
        [CoordinatorAccountOption(title: L10n.IOSApp.General.dataPrivacy,
                                  coordinator: OpenLinkCoordinator(url: L10n.IOSApp.WelcomeViewController.linkPrivacyPolicy)),
         InteractorAccountOption(title: L10n.IOSApp.DataReport.AccountSettings.title,
                                  interactor: AccountDataReportInteractor(presenter: self)),
         CoordinatorAccountOption(title: L10n.IOSApp.General.termsAndConditions,
                                  coordinator: OpenLinkCoordinator(url: L10n.IOSApp.WelcomeViewController.linkTC)),
         CoordinatorAccountOption(title: L10n.IOSApp.General.healthDepartmentKey,
                                  coordinator: HealthDepartmentCryptoInfoCoordinator(presenter: self)),
         CoordinatorAccountOption(title: L10n.UserApp.Account.Legal.Disclosure.title,
                                  coordinator: OpenLinkCoordinator(url: L10n.IOSApp.General.linkImprint)),
         CoordinatorAccountOption(title: L10n.IOSApp.acknowledgements,
                                  coordinator: LicensesCoordinator(presenter: self)),
         CoordinatorAccountOption(title: L10n.IOSApp.WelcomeViewController.gitLab,
                                  coordinator: GitlabLinkCoordinator(presenter: self, url: L10n.IOSApp.WelcomeViewController.linkGitLab)),
         CoordinatorAccountOption(title: L10n.IOSApp.AppVersion.button,
                                  coordinator: VersionDetailsCoordinator(presenter: self))]
    }
}
