import UIKit
import RxSwift
import DependencyInjection

class LucaConnectOptionViewController: OptionViewController {

    override func generateCoordinators() {
        self.options = [OptionGrouping(footer: L10n.IOSApp.Account.Option.Luca.Connect.Activate.description,
                                       options: [LucaConnectToggleAccountOption(title: L10n.IOSApp.Account.Option.Luca.Connect.Activate.title,
                                                                                presenter: self)])]
    }

}
