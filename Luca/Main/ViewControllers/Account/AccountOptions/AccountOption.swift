import UIKit
import RxSwift

protocol AccountOption {
    var title: String { get }
    var cellIdentifier: String { get }
    var classForCoder: AnyClass? { get }
    func onDidSelect(cell: UITableViewCell)
    func onDidCreate(cell: UITableViewCell)
}

extension AccountOption {
    var cellIdentifier: String { "AccountTableViewCell" }
    var classForCoder: AnyClass? { AccountTableViewCell.classForCoder() }
}

protocol ToggleOption {

    var toggleValue: Observable<Bool> { get }
    func toggleChanged(_ sender: UISwitch)

}

protocol OptionalOption {
    var isVisible: Bool { get set }
}
