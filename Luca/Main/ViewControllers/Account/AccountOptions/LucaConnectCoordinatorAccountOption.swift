import UIKit

class LucaConnectCoordinatorAccountOption: SettingCoordinatorAccountOption, OptionalOption {
    var isVisible: Bool

    override init(title: String, coordinator: Coordinator, icon: UIImage, tag: String) {
        self.isVisible = false
        super.init(title: title, coordinator: coordinator, icon: icon, tag: tag)
    }

}
