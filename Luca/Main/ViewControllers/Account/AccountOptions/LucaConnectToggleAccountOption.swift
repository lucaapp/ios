import UIKit
import RxSwift
import DependencyInjection

class LucaConnectToggleAccountOption: AccountOption, ToggleOption {
    @InjectStatic(\.lucaConnectContactDataService) private var lucaConnectContactDataService
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    var title: String
    weak var presenter: UIViewController?
    var coordinator: VoluntaryReachabilityCoordinator?

    var toggleValue: Observable<Bool> {
        lucaConnectContactDataService
            .isFeatureEnabled()
    }

    init(title: String, presenter: UIViewController) {
        self.title = title
        self.presenter = presenter
    }

    func onDidSelect(cell: UITableViewCell) {}
    func onDidCreate(cell: UITableViewCell) {}

    func toggleChanged(_ sender: UISwitch) {
        guard let presenter = presenter else { return }
        _ = lucaConnectContactDataService
            .isUserEligibleFeatureEnabled()
            .observe(on: MainScheduler.instance)
            .flatMapCompletable { [weak self] isEnabled in

                guard let self = self else { return .empty() }
                if isEnabled {
                    return self.lucaConnectContactDataService.disableFeature()
                } else {
                    return Completable.from {
                        DispatchQueue.main.asyncAfter(deadline: .now() + DispatchTimeInterval.milliseconds(500)) {
                            sender.setOn(false, animated: true)  // toggle value is set from observed toggleValue
                        }
                        if self.coordinator == nil {
                            self.coordinator = VoluntaryReachabilityCoordinator(presenter: presenter)
                        }
                        self.coordinator?.start()
                    }
                }
            }
            .subscribe()
    }

}
