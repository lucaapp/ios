import UIKit

class InteractorAccountOption: AccountOption {

    var interactor: Interactor
    var title: String

    init(title: String, interactor: Interactor) {
        self.title = title
        self.interactor = interactor
    }

    func onDidSelect(cell: UITableViewCell) {
        _ = interactor.interact()
            .subscribe()
    }

    func onDidCreate(cell: UITableViewCell) {}
}
