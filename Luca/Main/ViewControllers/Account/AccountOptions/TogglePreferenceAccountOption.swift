import UIKit
import RxSwift
import DependencyInjection

public class TogglePreferenceAccountOption: AccountOption, ToggleOption {
    @InjectStatic(\.lucaPreferences) internal var lucaPreferences

    var title: String
    var keyPath: KeyPath<LucaPreferencesTemplate, PreferencesEntry<Bool>>
    var invertValue: Bool

    init(title: String, keyPath: KeyPath<LucaPreferencesTemplate, PreferencesEntry<Bool>>, invertValue: Bool = false) {
        self.title = title
        self.keyPath = keyPath
        self.invertValue = invertValue
    }

    var toggleValue: Observable<Bool> {
        lucaPreferences
            .currentAndChanges(keyPath)
            .map { value in
                guard let value = value else {
                    return false
                }
                return self.invertValue ? !value : value
            }
    }

    func onDidSelect(cell: UITableViewCell) {}
    func onDidCreate(cell: UITableViewCell) {}

    func toggleChanged(_ sender: UISwitch) {
        let value = invertValue ? !sender.isOn : sender.isOn
        _ = lucaPreferences.set(keyPath, value: value)
            .andThen(Single<Bool>.just(value))
            .do(onError: { _ in
                // Switch back to original value if error was thrown
                sender.setOn(!sender.isOn, animated: true)
            })
            .subscribe(on: MainScheduler.instance)
            .subscribe()
    }

}
