import UIKit

class SettingCoordinatorAccountOption: CoordinatorAccountOption, Interactable {
    var interactableTag: String
    var icon: UIImage

    init(title: String, coordinator: Coordinator, icon: UIImage, tag: String) {
        self.icon = icon
        self.interactableTag = tag
        super.init(title: title, coordinator: coordinator)
    }

    func interact() {
        coordinator?.start()
    }
}
