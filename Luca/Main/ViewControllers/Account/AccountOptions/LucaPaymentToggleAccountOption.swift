import Foundation
import UIKit
import RxSwift
import DependencyInjection

class LucaPaymentToggleAccountOption: AccountOption, ToggleOption {

    typealias ViewController = UIViewController & HandlesLucaErrors

    var title: String
    weak var presenter: ViewController?
    @InjectStatic(\.lucaPayment) private var lucaPayment

    init(title: String, presenter: ViewController) {
        self.title = title
        self.presenter = presenter
    }

    var toggleValue: Observable<Bool> {
        lucaPayment.isUserActivated
            .asObservable()
    }

    func toggleChanged(_ uiSwitch: UISwitch) {
        guard let presenter = presenter else { return }
        let interactor: Interactor = uiSwitch.isOn ? PaymentEnableInteractor(presenter: presenter) : PaymentDisableInteractor(presenter: presenter)
        _ = interactor
            .interact()
            .handleErrors(presenter)
            .do(onError: { [weak self] _ in
                self?.toggleBack(uiSwitch)
            })
            .subscribe()
    }

    private func toggleBack(_ uiSwitch: UISwitch) {
        DispatchQueue.main.async {
            uiSwitch.setOn(!uiSwitch.isOn, animated: true)
        }
    }

    func onDidSelect(cell: UITableViewCell) {}
    func onDidCreate(cell: UITableViewCell) {}
}
