import UIKit

class CoordinatorAccountOption: AccountOption {
    var coordinator: Coordinator?
    var opensExternalLink: Bool
    var title: String

    init(title: String, coordinator: Coordinator? = nil, opensExternalLink: Bool = false) {
        self.title = title
        self.coordinator = coordinator
        self.opensExternalLink = opensExternalLink
    }

    func onDidSelect(cell: UITableViewCell) {
        coordinator?.start()
    }
    func onDidCreate(cell: UITableViewCell) {}
}
