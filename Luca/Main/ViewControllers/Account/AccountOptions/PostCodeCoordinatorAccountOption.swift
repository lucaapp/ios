import UIKit
import DependencyInjection

class PostCodeCoordinatorAccountOption: SettingCoordinatorAccountOption, OptionalOption {

    var isVisible: Bool = false
    @InjectStatic(\.lucaPreferences) private var lucaPreferences

    override init(title: String, coordinator: Coordinator, icon: UIImage, tag: String) {
        super.init(title: title, coordinator: coordinator, icon: icon, tag: tag)
        _ = lucaPreferences.get(\.userRegistrationData)
            .asObservable()
            .take(1)
            .subscribe(onNext: { [weak self] userRegistrationData in
                self?.isVisible = !String.isNilOrEmpty(userRegistrationData?.postCode)
            })
    }
}
