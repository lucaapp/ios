import UIKit
import RxSwift
import DependencyInjection

class ToggleConsentAccountOption: AccountOption, ToggleOption {
    typealias ViewController = UIViewController & HandlesLucaErrors

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    var title: String
    var keyPath: KeyPath<LucaPreferencesTemplate, PreferencesEntry<Bool>>
    weak var presenter: ViewController?
    private var interactor: ConsentInteractor

    var toggleValue: Observable<Bool> {
        lucaPreferences
            .currentAndChanges(keyPath)
            .map { $0 ?? false }
    }

    init(title: String, keyPath: KeyPath<LucaPreferencesTemplate, PreferencesEntry<Bool>>, presenter: ViewController, interactor: ConsentInteractor) {
        self.title = title
        self.keyPath = keyPath
        self.presenter = presenter
        self.interactor = interactor
    }

    func onDidSelect(cell: UITableViewCell) {}
    func onDidCreate(cell: UITableViewCell) {}

    func toggleChanged(_ sender: UISwitch) {
        guard let presenter = presenter else { return }
        _ = lucaPreferences.get(keyPath)
            .map { $0 ?? false }
            .flatMapCompletable { value in
                self.interactor
                    .interact(value: !value)
                    .andThen(Single<Bool?>.just(!value))
                    .handleErrors(presenter)
                    .catchAndReturn(nil)
                    .observe(on: MainScheduler.instance)
                    .do(onSuccess: { newValue in
                        // Turn switch off if error was thrown
                        if newValue == nil {
                            sender.setOn(false, animated: true)
                        }
                    })
                    .asCompletable()
            }
            .subscribe()
    }

}
