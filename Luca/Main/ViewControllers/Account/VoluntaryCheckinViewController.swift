import UIKit
import RxSwift
import DependencyInjection

// This view controller injects a second option when the user has turned on the switch for the first
class VoluntaryCheckinViewController: OptionViewController {
    @InjectStatic(\.lucaPreferences) private var lucaPreferences

    let initialOptions = [OptionGrouping(footer: L10n.IOSApp.Account.Option.VoluntaryCheckin.Activate.description,
                                         options: [TogglePreferenceAccountOption(title: L10n.IOSApp.Account.Option.VoluntaryCheckin.Activate.title,
                                                                                 keyPath: \.dontShowVoluntaryCheckinConsent)])]
    let fullOptions = [OptionGrouping(footer: L10n.IOSApp.Account.Option.VoluntaryCheckin.Activate.description,
                                      options: [TogglePreferenceAccountOption(title: L10n.IOSApp.Account.Option.VoluntaryCheckin.Activate.title,
                                                                              keyPath: \.dontShowVoluntaryCheckinConsent)]),
                       OptionGrouping(footer: L10n.IOSApp.Account.Option.VoluntaryCheckin.ShareData.description,
                                      options: [TogglePreferenceAccountOption(title: L10n.IOSApp.Account.Option.VoluntaryCheckin.ShareData.title,
                                                                              keyPath: \.checkInAnonymousWhenPossible,
                                                                              invertValue: true)])]

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        _ = lucaPreferences
            .currentAndChanges(\.dontShowVoluntaryCheckinConsent)
            .map { $0 ?? false }
            .distinctUntilChanged()
            .take(until: rx.viewDidDisappear)
            .subscribe(on: MainScheduler.instance)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { value in
                value ? self.updateOptions(options: self.fullOptions) : self.updateOptions(options: self.initialOptions)
            })
    }

    override func generateCoordinators() {
        self.options = initialOptions
    }

}
