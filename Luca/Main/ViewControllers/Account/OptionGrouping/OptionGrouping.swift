import Foundation

class OptionGrouping {
    var header: String?
    var footer: String?
    var options: [AccountOption]

    init(header: String? = nil, footer: String? = nil, options: [AccountOption]) {
        self.header = header
        self.footer = footer
        self.options = options
    }
}
