import UIKit

class DirectCheckinViewController: OptionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        set(title: L10n.IOSApp.Account.Option.directCheckin)
    }

    override func generateCoordinators() {
        self.options = [OptionGrouping(footer: L10n.IOSApp.Account.Option.DirectCheckin.Checkin.description,
                                       options: [TogglePreferenceAccountOption(title: L10n.IOSApp.Account.Option.DirectCheckin.Checkin.title,
                                                                               keyPath: \.dontShowCheckinConsent)])]
    }

}
