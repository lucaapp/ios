import UIKit

class OnboardingViewControllerFactory {

    private static var storyboard = UIStoryboard(name: "Onboarding", bundle: nil)

    static func instantiateViewController<T: UIViewController>(identifier: String) -> T {
        UIViewController.instantiate(storyboard: storyboard, identifier: identifier)
    }

    static func createFormViewController() -> UIViewController {
        return storyboard.instantiateViewController(withIdentifier: "FormViewController")
    }

    static func createWelcomeViewController() -> UIViewController {
        return storyboard.instantiateViewController(withIdentifier: "WelcomeViewController")
    }

    static func createDataPrivacyViewController() -> UIViewController {
        return storyboard.instantiateViewController(withIdentifier: "DataPrivacyViewController")
    }

    static func createDoneViewController() -> UIViewController {
        return storyboard.instantiateViewController(withIdentifier: "DoneViewController")
    }

    static func createCountrySelectionViewController(defaultCountryLocale: CountryLocale?, delegate: CountrySelectionViewControllerDelegate) -> UINavigationController {
        let countryViewController = CountrySelectionViewController(defaultLocale: defaultCountryLocale, delegate: delegate)
        return UINavigationController(rootViewController: countryViewController)
    }

    static func createCountryRejectionViewController() -> UIViewController {
        return CountryRejectionViewController.fromStoryboard()
    }
}
