import Foundation

class ViewControllerFactory {
    static let Onboarding = OnboardingViewControllerFactory.self
    static let Main = MainViewControllerFactory.self
    static let Checkin = CheckinViewControllerFactory.self
    static let Document = DocumentViewControllerFactory.self
    static let History = HistoryViewControllerFactory.self
    static let Account = AccountViewControllerFactory.self
    static let Terms = TermsViewControllerFactory.self
    static let Alert = AlertViewControllerFactory.self
    static let Children = ChildrenViewControllerFactory.self
    static let AccessedTraceIds = AccessedTraceIdsViewControllerFactory.self
    static let Notifications = NotificationViewControllerFactory.self
    static let Payment = PaymentViewControllerFactory.self
}
