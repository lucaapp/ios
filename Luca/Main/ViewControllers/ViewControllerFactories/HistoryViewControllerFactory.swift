import UIKit

class HistoryViewControllerFactory {
    static func createHistoryViewController(filterForWarningLevel: UInt8? = nil) -> UIViewController {
        let vc: HistoryViewController = HistoryViewController.fromStoryboard()

        vc.viewModel = HistoryViewModel(filterForWarningLevel: filterForWarningLevel)

		return vc
	}

	static func createShareDialogViewController() -> UIViewController {
		let timeFrameController = ShareDataSelectTimeframeViewController.instantiate(storyboard: UIStoryboard(name: "ShareDataDialog", bundle: nil), identifier: "ShareDataSelectTimeframeViewController")
		let navigationController = UINavigationController(rootViewController: timeFrameController)
		if let vc = timeFrameController as? ShareDataSelectTimeframeViewController {
			vc.viewModel = ShareDataViewModel()
		}
		return navigationController
	}

}
