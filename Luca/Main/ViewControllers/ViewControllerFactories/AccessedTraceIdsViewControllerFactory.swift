import UIKit

class AccessedTraceIdsViewControllerFactory {
    static func createWarningLevelsViewController(traceInfo: TraceInfo) -> WarningLevelsViewController {
        let vc: WarningLevelsViewController = WarningLevelsViewController.fromStoryboard()
        vc.viewModel = WarningLevelsViewModel(traceInfo: traceInfo)
        return vc
    }
}
