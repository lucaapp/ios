import UIKit

class CheckinViewControllerFactory {
	static func createContactQRViewControllerTab() -> UIViewController {
        var contactQRViewController: MainCheckinViewController = MainCheckinViewController.instantiate(storyboard: UIStoryboard(name: "CheckinRootViewController", bundle: nil), identifier: "MainCheckinViewController")
		contactQRViewController.tabBarItem.image = UIImage.init(named: "scanner")
		contactQRViewController.tabBarItem.title = L10n.IOSApp.Navigation.Tab.checkin
        let nav = UINavigationController(rootViewController: contactQRViewController)
		return nav
	}

	static func createMyQRViewController() -> UIViewController {
		let myQRCodeVC = MyQRCodeViewController.fromStoryboard()
        myQRCodeVC.viewModel = MyQRCodeViewModel()
		let navigationController = UINavigationController(rootViewController: myQRCodeVC)
		return navigationController
	}

	static func createMyQRFullScreenViewController() -> UIViewController {
		let myQRCodeVC = MyQRCodeFullScreenViewController.fromStoryboard()
		return myQRCodeVC
	}

	static func createLocationCheckinViewController(traceInfo: TraceInfo) -> ActiveCheckinViewController {
		let viewController: ActiveCheckinViewController = ActiveCheckinViewController.fromStoryboard()

        viewController.viewModel = DefaultLocationCheckInViewModel(
            traceInfo: traceInfo
        )

		return viewController
	}

	static func createPrivateMeetingViewController(meeting: PrivateMeetingData) -> NewPrivateMeetingViewController {
		let vc: NewPrivateMeetingViewController = NewPrivateMeetingViewController.fromStoryboard()
//		vc.meeting = meeting
		return vc
	}

	static func createQRScannerViewController() -> QRScannerViewController {
		return QRScannerViewController()
	}

    static func createOptionalDataCheckinViewController() -> UINavigationController {
        UINavigationController(rootViewController: OptionalCheckinDataViewController.fromStoryboard())
    }

    static func createConsentViewController(title: String, buttonTitle: String, description: [NantesLabelData], backgroundColor: UIColor? = nil) -> ConsentViewController {
        let vc: ConsentViewController = ConsentViewController(consentTitle: title, consentButtonTitle: buttonTitle, description: description, backgroundColor: backgroundColor)
        return vc
    }

    static func createExternalLinkConsentViewController(location: Location, url: LocationURL) -> UINavigationController {
        let nc = UINavigationController(rootViewController: ExternalLinkConsentViewController.fromStoryboard())
        if let vc = nc.viewControllers.first as? ExternalLinkConsentViewController {
            vc.setup(location: location, url: url)
        }
        return nc
    }
}
