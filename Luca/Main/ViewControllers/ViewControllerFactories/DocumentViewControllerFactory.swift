import UIKit

class DocumentViewControllerFactory {
    static func createDocumentListViewControllerTab() -> UIViewController {
        let documentViewController = DocumentListViewController.fromStoryboard()
        documentViewController.viewModel = DocumentListViewModel()
        let navigationController = UINavigationController(rootViewController: documentViewController)
        navigationController.tabBarItem.image = UIImage.init(named: "myLuca")
        navigationController.tabBarItem.title = L10n.IOSApp.Navigation.Tab.health

        return navigationController
    }

    static func createDocumentViewController(
        documents: [Document],
        owner: Person,
        focusedDocument: Document? = nil,
        delegate: DocumentViewControllerDelegate? = nil
    ) -> DocumentViewController {
        let vc = DocumentViewController.fromStoryboard()
        vc.delegate = delegate
        vc.documents = documents
        vc.focusedDocument = focusedDocument
        vc.owner = owner
        return vc
    }

    static func createTestQRScannerViewController() -> UINavigationController {
        let navigationController = UINavigationController(rootViewController: TestQRCodeScannerController.fromStoryboard())
        return navigationController
    }
}
