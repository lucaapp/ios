import UIKit

class NotificationViewControllerFactory {
    static func createNotificationViewControllerTab() -> UIViewController {
        let notificationListViewController: NotificationListViewController = NotificationListViewController.fromStoryboard()

        // Trigger view model initialisation
        notificationListViewController.loadViewIfNeeded()

        let navigationController = UINavigationController(rootViewController: notificationListViewController)
        navigationController.tabBarItem.image = UIImage.init(named: "messages")
        navigationController.tabBarItem.title = L10n.IOSApp.Navigation.Tab.notification

        notificationListViewController.initBindings()
        return navigationController
    }

    static func createNotificationDetailViewController(accessedTraceId: AccessedTraceId) -> UIViewController {
        let viewController: NotificationDetailViewController = NotificationDetailViewController.fromStoryboard()
        let viewModel = NotificationDetailViewModel(accessedTraceId: accessedTraceId)
        viewController.viewModel = viewModel
        return viewController
    }

    static func createHealthDepartmentDetailViewController(fetchedMessage: FetchedMessage) -> UIViewController {
        let vc = HealthDepartmentMessageDetailViewController.fromStoryboard()
        vc.viewModel = HealthDepartmentMessageDetailViewModel(message: fetchedMessage)
        return vc
    }

    static func createLucaIdIdentDetailViewController() -> UIViewController {
        let vc = LucaIdIdentDetailViewController.fromStoryboard()
        return vc
    }

    static func createLucaIdSuccessDetailViewController() -> UIViewController {
        let vc = LucaIdSuccessDetailViewController.fromStoryboard()
        return vc
    }

    static func createLucaIdFailureDetailViewController(localNewsMessage: LocalNewsMessage, navTitle: String) -> LocalNewsMessageDetailViewController {
        let vc = LocalNewsMessageDetailViewController.fromStoryboard()
        vc.localMessage = localNewsMessage
        vc.navTitle = navTitle
        return vc
    }

    static func createPaymentRevocationViewController(revocationCode: String) -> PaymentRevocationViewController {
        let vc = PaymentRevocationViewController(revocationCode: revocationCode)

        return vc
    }
}
