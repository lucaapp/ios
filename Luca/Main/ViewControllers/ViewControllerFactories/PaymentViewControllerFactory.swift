import UIKit

class PaymentViewControllerFactory {

    static func createMainViewControllerTab() -> UIViewController {
        let vc = PaymentViewController()
        vc.tabBarItem.image = Asset.payment.image
        vc.tabBarItem.title = L10n.UserApp.Pay.Nav.bar
        let nav = UINavigationController(rootViewController: vc)
        return nav
    }
}
