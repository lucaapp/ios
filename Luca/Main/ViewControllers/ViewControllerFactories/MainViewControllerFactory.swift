import UIKit

class MainViewControllerFactory {
    static func createTabBarController() -> MainTabBarViewController {
        let mainTabBarController = MainTabBarViewController()

        mainTabBarController.viewControllers = [
            ViewControllerFactory.Document.createDocumentListViewControllerTab(),
            ViewControllerFactory.Checkin.createContactQRViewControllerTab(),
            ViewControllerFactory.Payment.createMainViewControllerTab(),
            ViewControllerFactory.Notifications.createNotificationViewControllerTab(),
            ViewControllerFactory.Account.createAccountViewControllerTab()
        ]
        return mainTabBarController
    }

    static func createContactViewController() -> ContactViewController {
        return ContactViewController.fromStoryboard()
    }

    static func createDataAccessViewController() -> DataAccessViewController {
        return DataAccessViewController.fromStoryboard()
    }

    static func createHealthDepartmentCryptoInfoViewController() -> UIViewController {
        return HealthDepartmentCryptoInfoViewController.fromStoryboard()
    }

    static func createExplanationPageViewController(topic: ExplanationTopic) -> ExplanationPageViewController {
        let vc: ExplanationPageViewController = ExplanationPageViewController.fromStoryboard()
        vc.topic = topic
        return vc
    }

    static func createExplanationContainerViewController(topic: ExplanationTopic, showAllCloseButtons: Bool) -> ExplanationContainerViewController {
        let vc: ExplanationContainerViewController = ExplanationContainerViewController.fromStoryboard()
        vc.topic = topic
        vc.showAllCloseButtons = showAllCloseButtons
        return vc
    }

    static func createExplanationViewController(screen: ExplanationScreen) -> ExplanationViewController {
        let vc: ExplanationViewController = ExplanationViewController.fromStoryboard()
        vc.screen = screen
        return vc
    }

}
