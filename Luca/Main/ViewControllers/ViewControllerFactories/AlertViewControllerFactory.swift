import UIKit
import DeviceKit
import PhoneNumberKit
import RxSwift
import DependencyInjection

class AlertViewControllerFactory {

    private static var storyboardLarge = UIStoryboard(name: "Alerts", bundle: nil)
    private static var storyboardSE = UIStoryboard(name: "AlertsSE", bundle: nil)

    private static let storyboardSEModels = [Device.iPhone5, Device.iPhone5c, Device.iPhone5s, Device.iPhoneSE]
    private static let storyboardSESimulators = [Device.simulator(.iPhone5), Device.simulator(.iPhone5c), Device.simulator(.iPhone5s), Device.simulator(.iPhoneSE)]

    private static var storyboard: UIStoryboard = {
        return storyboardSEModels.contains(Device.current) || storyboardSESimulators.contains(Device.current) ? storyboardSE : storyboardLarge
    }()

    static func instantiateViewController<T: UIViewController>(identifier: String) -> T {
        UIViewController.instantiate(storyboard: storyboard, identifier: identifier)
    }

    static func createPhoneNumberConfirmationViewController(phoneNumber: PhoneNumber) -> PhoneNumberConfirmationViewController {
        let viewController: PhoneNumberConfirmationViewController = instantiateViewController(identifier: "PhoneNumberConfirmationViewController")
        viewController.phoneNumber = phoneNumber
        return viewController
    }

    static func createPhoneNumberVerificationViewController(challengeIDs: [String]) -> PhoneNumberVerificationViewController {
        let viewController: PhoneNumberVerificationViewController = instantiateViewController(identifier: "PhoneNumberVerificationViewController")
        viewController.challengeIds = challengeIDs
        return viewController
    }

    static func createAlertViewController(title: String, message: String, firstButtonTitle: String, firstButtonAction: (() -> Void)? = nil) -> LucaAlertViewController {
        let alert: LucaAlertViewController = instantiateViewController(identifier: "LucaAlertViewController")

        // Trigger loading
        _ = alert.view

        alert.titleLabel.text = title
        alert.messageLabel.text = message
        alert.firstButton.setTitle(firstButtonTitle, for: .normal)
        alert.onFirstButtonAction = firstButtonAction

        alert.modalTransitionStyle = .crossDissolve
        alert.modalPresentationStyle = .overCurrentContext

        return alert
    }

    static func createAppVersionAlertController(viewController: UIViewController) {

        let version = UIApplication.shared.applicationVersion ?? ""
        let buildNumber = UIApplication.shared.buildNumber ?? ""

        #if DEVELOPMENT
        let environment = "Dev"
        #elseif QA
        let environment = "QA"
        #elseif PENTEST
        let environment = "Pentest"
        #elseif RELEASE
        let environment = "Release"
        #elseif PREPROD
        let environment = "Preprod"
        #elseif HOTFIX
        let environment = "Hotfix"
        #elseif AQS
        let environment = "AQS"
        #elseif P1
        let environment = "P1"
        #elseif P2
        let environment = "P2"
        #elseif P3
        let environment = "P3"
        #elseif DEMO
        let environment = "Demo"
        #else
        let environment = "Production"
        #endif

        let message = L10n.IOSApp.AppVersion.Alert.message(version, buildNumber, environment, commitHash)

        let configuration = UserInputInteractorConfiguration(
            title: L10n.IOSApp.AppVersion.Alert.title,
            message: message,
            type: UserInputInteractorConfiguration.InteractorType.push
        )

        _ = viewController.showDialog(configuration: configuration)
            .subscribe(on: MainScheduler.instance)
            .subscribe()
    }
}

extension AlertViewControllerFactory {
    static func createAlertViewControllerRx(
        presentingViewController viewController: UIViewController,
        title: String,
        message: String,
        firstButtonTitle: String) -> Observable<LucaAlertViewController> {

        return Observable<LucaAlertViewController>.create { observer in
            let alert = AlertViewControllerFactory.createAlertViewController(
                title: title,
                message: message,
                firstButtonTitle: firstButtonTitle) {
                observer.onCompleted()
            }

            // This is in case someone dismisses the view controller outside of the stream. The stream should complete nevertheless.
            alert.__onDidDisappear = {
                observer.onCompleted()
            }

            viewController.present(alert, animated: true, completion: nil)

            observer.onNext(alert)
            return Disposables.create {
                // Disable safety notification, this stream is being disposed.
                alert.__onDidDisappear = nil
                alert.dismiss(animated: true, completion: nil)
            }
        }
        .subscribe(on: MainScheduler.instance)
    }
}
