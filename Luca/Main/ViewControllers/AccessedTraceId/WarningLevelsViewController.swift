import UIKit
import LucaUIComponents
import RxSwift
import RxCocoa

class WarningLevelsViewController: UIViewController, BindableType, DisplaysProgress, HandlesLucaErrors {

    @IBOutlet weak var tableView: UITableView!

    var progressIndicator: UIActivityIndicatorView!
    var viewModel: WarningLevelsViewModel!

    private var disposeBag: DisposeBag! = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        title = L10n.IOSApp.Navigation.Tab.history.string
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        tableView.separatorColor = .clear
        view.backgroundColor = .black
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        bindViewModel()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        disposeBag = nil
    }

    func bindViewModel() {
        disposeBag = DisposeBag()

        viewModel
            .warningLevels
            .drive(tableView
                    .rx
                    .items(cellIdentifier: "WarningLevelTableViewCell")) { _, warningLevel, cell in

                guard let cell = cell as? WarningLevelTableViewCell else {
                    return
                }

                cell.warningLevelCellModel = warningLevel
            }
        .disposed(by: disposeBag)

        tableView.rx.modelSelected(WarningLevelCellViewModel.self)
            .do(onNext: { model in
                let coordinator = AccessedTraceIdDetailCoordinator(presenter: self, accessedTraceId: model.accessedTraceId)
                coordinator.start()
            })
            .subscribe()
            .disposed(by: disposeBag)
    }

}
