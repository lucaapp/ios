import UIKit
import SwiftUI

class PaymentViewController: UIHostingController<AnyView> {
    init() {
        super.init(rootView: AnyView(EmptyView()))
        self.rootView = AnyView(LucaPaymentView().environment(\.presenter, Weak(self)))
    }

    @MainActor required dynamic init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        UIApplication.shared.setStatusBarStyle(.lightContent, animated: animated)
    }
}
