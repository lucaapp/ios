import UIKit
import SwiftUI

class PaymentRevocationViewController: UIHostingController<PaymentRevocationView> {

    init(revocationCode: String) {
        super.init(rootView: PaymentRevocationView(revocationCode: revocationCode))
    }

    @MainActor @objc required dynamic init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
