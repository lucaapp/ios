import UIKit
import LucaUIComponents
import RxSwift

class VoluntaryReachabilityConsentViewController: UIViewController, HandlesLucaErrors {
    var thankYouCoordinator: Coordinator?
    @IBOutlet weak var approveSwitch: UISwitch!
    @IBOutlet weak var continueButton: LightStandardButton!
    var data: ProvidesConsentApproval!
    private var disposeBag: DisposeBag?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    private func setupViews() {
        approveSwitch.isOn = data.dataApproved.value
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.installObservers()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposeBag = nil
    }

    private func installObservers() {
        let newDisposeBag = DisposeBag()
        approveSwitch.rx.isOn
            .bind(to: data.dataApproved)
            .disposed(by: newDisposeBag)

        self.data.dataApproved
            .bind(to: continueButton.rx.isEnabled)
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }
    @IBAction func continueButtonClicked(_ sender: Any) {
        guard let disposeBag = disposeBag else {return}
        NotificationPermissionHandler.shared.requestAuthorizationRx()
            .asObservable()
            .take(1)
            .catch { (error) -> Observable in
                return self.processErrorMessagesRx(error: error)
                    .andThen(Observable.error(error))
            }
            .flatMap({ authorized -> Completable in
                if authorized { return .empty() }
                return UIAlertController.goToApplicationSettingsRx(viewController: self,
                                                                   title: L10n.IOSApp.Notification.Permission.title,
                                                                   message: L10n.IOSApp.Notification.Permission.description).ignoreElementsAsCompletable()
            })
            .asCompletable()
            .andThen(
                self.data.enableFeature().do(onSubscribe: { [weak self] in
                    DispatchQueue.main.async {
                        self?.continueButton.isEnabled = false
                    }
                }, onDispose: { [weak self] in
                    DispatchQueue.main.async {
                        self?.continueButton.isEnabled = true
                    }
                })
            )
            .handleErrors(self)
            .subscribe(onCompleted: { [weak self] in
                DispatchQueue.main.async {
                    self?.thankYouCoordinator?.start()
                }
            })
            .disposed(by: disposeBag)

    }

}
