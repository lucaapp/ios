import UIKit
import AVFoundation
import RxSwift
import DependencyInjection
import JGProgressHUD

class VoluntaryReachabilityScanQRCodeController: UIViewController, HandlesLucaErrors, ContainsTorchButton {
    @IBOutlet weak var lucaQRScannerView: LucaQRScanner!
    @IBOutlet weak var lightButton: UIButton!
    private var disposeBag: DisposeBag!
    @InjectStatic(\.qrProcessingService) private var qrProcessingService
    private var loadingHUD = JGProgressHUD.lucaLoading()
    var addProofSuccessCoordinator: Coordinator?

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let device = AVCaptureDevice.default(for: .video) else {
            self.lightButton.isHidden = true
            return
        }
        self.lightButton.accessibilityLabel = L10n.IOSApp.Light.Button.activate
        self.lightButton.isHidden = !device.hasTorch || !device.isTorchAvailable
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.showScanner(display: false)
        self.disposeBag = nil
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.disposeBag = DisposeBag()
        bindLucaQRScanner()
        self.showScanner(display: true)
    }

    @IBAction func lightButtonTapped(_ sender: Any) {
        self.toggleTorch(sender: sender)
    }

    func startScanner() {
        let newDisposeBag = DisposeBag()

        UIApplication.shared.rx.currentAndChangedAppState
            .subscribe(on: MainScheduler.asyncInstance)
            .flatMapLatest { [weak self] appState -> Completable in
                guard let self = self, appState == .active else {
                    return .empty()
                }
                return self.lucaQRScannerView.scan
                // Delay the subscription to force the appearance to continue without waiting for the session
                    .delaySubscription(.milliseconds(1), scheduler: MainScheduler.asyncInstance)
                    .take(1)
                    .debug("scan")
                    .flatMap {
                        self.qrProcessingService.processQRCode(qr: $0, processingStrategy: .lucaConnectProof, presenter: self)
                            .observe(on: MainScheduler.asyncInstance)
                            .do(onSubscribe: { DispatchQueue.main.async { self.loadingHUD.show(in: self.view) } })
                            .do(onDispose: { self.loadingHUD.dismiss() })
                    }
                    .catch { (error) -> Observable in
                        if let localizedError = error as? LocalizedTitledError,
                           case MetadataScannerServiceLocalizedError.captureDeviceNotAuthorized = localizedError {
                            return Completable.from {
                                DispatchQueue.main.async {
                                    self.goToApplicationSettings()
                                }
                            }
                            .andThen(Observable.empty())
                        } else {
                            return self.processErrorMessagesRx(error: error)
                                .andThen(Observable.error(error))
                        }
                    }
                    .retry()
                    .asObservable()
                    .ignoreElementsAsCompletable()
                    .andThen(Completable.from { [weak self] in
                        self?.toggleTorch(sender: nil, forceOff: true)
                        self?.addProofSuccessCoordinator?.start()
                    })
            }
            .subscribe()
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

    func endScanner() {
        self.disposeBag = nil
    }

    private func bindLucaQRScanner() {
        lucaQRScannerView.onActivateButton
            .asDriver(onErrorDriveWith: .empty())
            .drive { [weak self] _ in
                self?.startScanner()
            }
            .disposed(by: disposeBag)
    }

    private func showScanner(display: Bool) {
        if display == false {
            self.endScanner()
        } else {
            lucaQRScannerView.showScanner(display: display)
            if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
                DispatchQueue.main.async { [weak self] in
                    self?.startScanner()
                }
            }
        }
    }

    private func goToApplicationSettings() {
        UIAlertController(title: L10n.IOSApp.Camera.Access.title, message: L10n.IOSApp.Camera.Access.description, preferredStyle: .alert).goToApplicationSettings(viewController: self, pop: true)
    }

}
