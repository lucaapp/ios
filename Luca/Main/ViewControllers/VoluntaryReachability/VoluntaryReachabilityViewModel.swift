import Foundation
import RxSwift
import RxCocoa
import DependencyInjection

protocol ProvidesContactInformation {
    var fullName: Driver<String> {get}
    var dateOfBirth: Driver<String> {get}
    var address: Driver<String> {get}
    var phoneNumber: Driver<String> {get}
    var email: BehaviorRelay<String> {get}
    func updateUserData() -> Completable
}
protocol ProvidesConsentApproval {
    var dataApproved: BehaviorRelay<Bool> {get}
    func enableFeature() -> Completable
}

typealias DocumentWithBirthdate = Document & ContainsDateOfBirth

class VoluntaryReachabilityViewModel: Any, ProvidesContactInformation, ProvidesConsentApproval {
    @InjectStatic(\.documentPersonAssociationService) private var documentPersonAssociationService
    @InjectStatic(\.lucaConnectContactDataService) private var lucaConnectContactDataService
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.userService) private var userService
    @InjectStatic(\.powService) private var powService

    lazy var email: BehaviorRelay<String> = BehaviorRelay<String>(value: "")
    var dataApproved: BehaviorRelay<Bool> = BehaviorRelay<Bool>(value: false)
    private var requestedAndScheduledChallenge = DispatchQueueSynced<PoWChallenge.Id?>(nil)

    init() {
        _ = lucaConnectContactDataService.getUsersContactData().subscribe { [weak self] data in
                self?.email.accept(data.email ?? "")
            }
        _ = powService.requestAndScheduleChallenge(type: .createConnectContact)
            .flatMapCompletable { challenge in self.requestedAndScheduledChallenge.writeRx { _ in challenge } }
            .subscribe()
    }

    var dateOfBirth: Driver<String> {
        certificates
            .compactMap({ (docs: [Document]) in
                return docs.filter {$0 is DocumentWithBirthdate}.compactMap {$0 as? DocumentWithBirthdate}
            })
            .map { (docs: [DocumentWithBirthdate]) -> String in
                return docs.first?.dateOfBirth.formattedDate ?? ""
            }
    }

    var address: Driver<String> {
        lucaConnectContactDataService.getUsersContactData().map { $0.fullAddress ?? "" }.asDriver(onErrorJustReturn: "")
    }

    var phoneNumber: Driver<String> {
        lucaConnectContactDataService.getUsersContactData().map { $0.phoneNumber }.asDriver(onErrorJustReturn: "")
    }

    var fullName: Driver<String> {
        lucaConnectContactDataService.getUsersContactData().map { $0.fullName }.asDriver(onErrorJustReturn: "")
    }

    var certificateLabelContent: Driver<String?> {
        certificates.map { (docs: [Document]) -> String? in
            if docs.contains(where: {$0 is Vaccination}) && docs.contains(where: {$0 is Recovery}) {
                return "\(L10n.IOSApp.VoluntaryReachability.VacStatus.recovery) & \(L10n.IOSApp.VoluntaryReachability.VacStatus.vaccination)"
            } else if docs.contains(where: {$0 is Recovery}) {
                return L10n.IOSApp.VoluntaryReachability.VacStatus.recovery
            } else if docs.contains(where: {$0 is Vaccination}) {
                return L10n.IOSApp.VoluntaryReachability.VacStatus.vaccination
            }
            return nil
        }
    }

    // can either be both or one of the following: vaccination certificate, recovery certificate
    var certificates: Driver<[Document]> {
        lucaConnectContactDataService.getEligibleDocuments()
            .asDriver(onErrorJustReturn: [])
    }

    func updateUserData() -> Completable {
        return lucaPreferences.get(\.userRegistrationData)
            .flatMapCompletable { [weak self] data -> Completable in
                guard let self = self, let user = data?.copy(with: nil) as? UserRegistrationData, user.email != self.email.value else {return .empty()}
                user.email = self.email.value
                return self.userService.update(data: user)
            }
    }

    func enableFeature() -> Completable {
        requestedAndScheduledChallenge.readRx()
            .flatMapCompletable { self.lucaConnectContactDataService.enableFeature(requestedPoWChallengeId: $0) }
    }

    private func sortDocuments(_ documents: [Document]) -> [Document] {
        // sort by date
        var sortedDocs = documents.sorted(by: { $0.issuedAt.compare($1.issuedAt) == .orderedDescending })

        // if latest vaccine is not valid yet, present first valid vaccine instead
        if let index = sortedDocs.firstIndex(where: { doc in
            if let vaccination = doc as? Vaccination {
                return vaccination.isComplete()
            }
            return false
        }),
           index > 0 {
            let vaccination = sortedDocs.remove(at: index)
            sortedDocs.insert(vaccination, at: 0)
        }

        return sortedDocs
    }
}
