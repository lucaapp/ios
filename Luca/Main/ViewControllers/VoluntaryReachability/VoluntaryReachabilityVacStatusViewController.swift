import UIKit
import LucaUIComponents
import RxCocoa
import RxSwift

class VoluntaryReachabilityVacStatusViewController: UIViewController {
    var addProofCoordinator: Coordinator?
    var contactDataCoordinator: Coordinator?
    @IBOutlet weak var continueButton: LightStandardButton!
    @IBOutlet weak var certMissingLabel: Luca14PtBoldAlertLabel!
    @IBOutlet weak var certPresentLabel: Luca14PtBoldLabel!
    private var disposeBag: DisposeBag?
    var viewModel: VoluntaryReachabilityViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        certMissingLabel.isHidden = true
        certPresentLabel.isHidden = true
        continueButton.isEnabled = false
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.installObservers()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposeBag = nil
    }

    private func installObservers() {
        let newDisposeBag = DisposeBag()
        viewModel.certificates.drive { [weak self] (documents: [Document]) in
            guard let self = self else {return}
            self.certMissingLabel.isHidden = !documents.isEmpty
            self.certPresentLabel.isHidden = documents.isEmpty
            let continueButtonTitle = (documents.isEmpty) ? L10n.IOSApp.VoluntaryReachability.VacStatus.addCert : L10n.IOSApp.VoluntaryReachability.VacStatus.provide
            let title = self.continueButton.uppercaseTitle ? continueButtonTitle.uppercased() : continueButtonTitle
            self.continueButton.setTitle(title, for: .normal)
            self.continueButton.setTitle(title, for: .disabled)
            self.continueButton.isEnabled = true
        }
        .disposed(by: newDisposeBag)

        viewModel.certificateLabelContent.drive { name in
            self.certPresentLabel.text = name
        }.disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

    @IBAction func continueButtonClicked(_ sender: Any) {
        guard let disposeBag = disposeBag else {return}
        self.viewModel.certificates
            .asObservable()
            .take(1)
            .observe(on: MainScheduler.asyncInstance)
            .subscribe { [weak self] (documents: [Document]) in
                if !documents.isEmpty {
                    self?.contactDataCoordinator?.start()
                } else {
                    self?.addProofCoordinator?.start()
                }
            }
            .disposed(by: disposeBag)
    }
}
