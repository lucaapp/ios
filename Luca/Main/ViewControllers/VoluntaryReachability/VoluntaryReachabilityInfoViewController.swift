import UIKit
import LucaUIComponents

class VoluntaryReachabilityInfoViewController: UIViewController {
    @IBOutlet weak var titleLabel: Luca20PtBoldLabel!
    @IBOutlet weak var descriptionLabel: Luca14PtLabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var continueButton: LightStandardButton!
    var coordinator: Coordinator?

    @IBAction func continueButtonClicked(_ sender: Any) {
        coordinator?.start()
    }
}
