import UIKit
import DependencyInjection

class VoluntaryReachabilityShareDataViewController: UIViewController {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences

    var continueCoordinator: Coordinator?

    override func viewDidLoad() {
        super.viewDidLoad()
        _ = lucaPreferences.set(\.lucaConnectInitiallyPresented, value: true).subscribe()
    }

    @IBAction func continueButtonClicked(_ sender: Any) {
        continueCoordinator?.start()
    }

}
