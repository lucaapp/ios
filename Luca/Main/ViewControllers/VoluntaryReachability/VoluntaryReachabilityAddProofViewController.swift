import UIKit
import RxSwift
import LucaUIComponents
import RxCocoa
import DependencyInjection
import JGProgressHUD

class VoluntaryReachabilityAddProofViewController: UIViewController, HandlesLucaErrors {
    @IBOutlet weak var cameraRollButton: DarkStandardButton!
    private var disposeBag: DisposeBag?
    private var loadingHUD = JGProgressHUD.lucaLoading()
    var addProofSuccessCoordinator: Coordinator?
    var scanQRCodeCoordinator: Coordinator?

    @InjectStatic(\.qrProcessingService) private var qrProcessingService

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.installObservers()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposeBag = nil
    }

    private func installObservers() {
        let newDisposeBag = DisposeBag()

        cameraRollButton.rx.tap
            .flatMapLatest { [weak self] _ in
                return UIImagePickerController.rx.createWithParent(self) { picker in
                    picker.sourceType = .photoLibrary
                    picker.allowsEditing = false
                }
                .flatMap {
                    $0.rx.didFinishPickingMediaWithInfo
                }
                .take(1)
            }
            .map { info in
                return info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage
            }
            .flatMap { MetadataScannerService.process(image: $0) }
            .flatMap { MetadataScannerService.process(features: $0) }
            .flatMap { [weak self] (qrCodeString: String?) -> Single<QRType> in
                guard let self = self, let qrCodeString = qrCodeString else {return .error(QRProcessingServiceSilentError.userNotAgreedOnPrivacyConsent)}
                return self.qrProcessingService.processQRCode(qr: qrCodeString, processingStrategy: .lucaConnectProof, presenter: self)
                    .observe(on: MainScheduler.asyncInstance)
                    .do(onSubscribe: { DispatchQueue.main.async { self.loadingHUD.show(in: self.view) } })
                    .do(onDispose: { DispatchQueue.main.async { self.loadingHUD.dismiss() } })
            }
            .catch { (error) -> Observable in
                if let authorizationError = error as? MetadataScannerServiceLocalizedError,
                   authorizationError == .captureDeviceNotAuthorized {
                    return self.handleAuthorizationError(title: L10n.IOSApp.Camera.Access.title, message: L10n.IOSApp.Camera.Access.description)
                        .andThen(Observable<QRType>.empty())
                }
                return self.processErrorMessagesRx(error: error)
                    .andThen(Observable.error(error))
            }

            // Don't break stream if import wrong image
            .retry()
            .subscribe(onNext: { [weak self] (_: QRType) in
                self?.addProofSuccessCoordinator?.start()
            })
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }
    @IBAction func cameraButtonTapped(_ sender: Any) {
        scanQRCodeCoordinator?.start()
    }
}
