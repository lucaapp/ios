import UIKit
import LucaUIComponents
import RxSwift
import Nantes
import DependencyInjection

class VoluntaryReachabilityKritisViewController: UIViewController {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences

    var consentCoordinator: Coordinator?

    @IBOutlet weak var industryTextField: LucaDefaultTextField!
    @IBOutlet weak var companyTextField: LucaDefaultTextField!
    @IBOutlet weak var descriptionLabel: NantesLabel!
    @IBOutlet weak var infrastructureSwitch: UISwitch!
    @IBOutlet weak var vulnerableGroupsSwitch: UISwitch!
    @IBOutlet weak var textFieldStackView: UIStackView!

    private var disposeBag: DisposeBag?

    override func viewDidLoad() {
        setupViews()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposeBag = nil
    }

    @IBAction func switchChanged(_ sender: UISwitch) {
        if sender.isOn {
            textFieldStackView.isHidden = false
        } else {
            if !infrastructureSwitch.isOn && !vulnerableGroupsSwitch.isOn {
                textFieldStackView.isHidden = true
            }
        }
    }

    @IBAction func continueButtonPressed(_ sender: RoundedLucaButton) {
        guard let coordinator = consentCoordinator else { return }

        let newDisposeBag = DisposeBag()

        let kritisData = KritisData()
        kritisData.criticalInfrastructure = infrastructureSwitch.isOn
        kritisData.vulnerableGroup = vulnerableGroupsSwitch.isOn

        kritisData.industry = industryTextField.text
        kritisData.company = companyTextField.text

        lucaPreferences.set(\.kritisData, value: kritisData)
            .subscribe(onCompleted: {
                DispatchQueue.main.async { coordinator.start() }
            })
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

    private func setupViews() {
        industryTextField.placeholder = L10n.IOSApp.VoluntaryReachability.Kritis.industry
        companyTextField.placeholder = L10n.IOSApp.VoluntaryReachability.Kritis.company

        buildTappableLabel(linkDescription: L10n.IOSApp.VoluntaryReachability.Kritis.message,
                           linkTerm: L10n.IOSApp.VoluntaryReachability.Kritis.Link.text,
                           linkURL: L10n.IOSApp.VoluntaryReachability.Kritis.Link.url,
                           tappableLabel: descriptionLabel)
    }

    private func buildTappableLabel(linkDescription: String, linkTerm: String, linkURL: String, tappableLabel: NantesLabel) {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: tappableLabel.font as Any,
            .foregroundColor: UIColor.white
        ]
        let attrText = NSMutableAttributedString(string: linkDescription, attributes: attributes)
        tappableLabel.attributedText = attrText

        tappableLabel.numberOfLines = 0
        tappableLabel.delegate = self

        let linkAttributes: [NSAttributedString.Key: Any] = [
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .font: tappableLabel.font.bold() as Any
        ]
        let clickedAttributes: [NSAttributedString.Key: Any] = [
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .foregroundColor: Asset.lucaGrey.color,
            .font: tappableLabel.font.bold() as Any
        ]
        tappableLabel.linkAttributes = linkAttributes
        tappableLabel.activeLinkAttributes = clickedAttributes

        if let linkRange = linkDescription.range(of: linkTerm),
           let url = URL(string: linkURL) {
            tappableLabel.addLink(to: url, withRange: NSRange(linkRange, in: linkDescription))
        }
    }

}
extension VoluntaryReachabilityKritisViewController: NantesLabelDelegate {

    func attributedLabel(_ label: NantesLabel, didSelectLink link: URL) {
        UIApplication.shared.open(link, options: [:], completionHandler: nil)
    }

}
