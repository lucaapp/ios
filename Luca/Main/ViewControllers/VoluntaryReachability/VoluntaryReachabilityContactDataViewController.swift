import UIKit
import LucaUIComponents
import RxSwift

class VoluntaryReachabilityContactDataViewController: UIViewController {
    var kritisCoordinator: Coordinator?

    @IBOutlet weak var fullNameLabel: Luca14PtBoldLabel!
    @IBOutlet weak var dateOfBirthLabel: Luca14PtBoldLabel!
    @IBOutlet weak var addressLabel: Luca14PtBoldLabel!
    @IBOutlet weak var phoneNumberLabel: Luca14PtBoldLabel!
    @IBOutlet weak var emailTextField: LucaDefaultTextField!
    @IBOutlet weak var continueButton: LightStandardButton!

    private var disposeBag: DisposeBag?
    var contactData: ProvidesContactInformation!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
   }

    private func setupViews() {
        emailTextField.delegate = self
        emailTextField.text = self.contactData.email.value
        emailTextField.setPlaceholder(text: L10n.IOSApp.UserData.Form.email, color: Asset.luca747480.color, font: FontFamily.Montserrat.medium.font(size: 14))
        emailTextField.set(.emailAddress)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.installObservers()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposeBag = nil
    }

    private func installObservers() {
        let newDisposeBag = DisposeBag()
        contactData.fullName.drive { name in
            self.fullNameLabel.text = name
        }.disposed(by: newDisposeBag)

        contactData.dateOfBirth.drive { dateOfBirth in
            self.dateOfBirthLabel.text = dateOfBirth
        }.disposed(by: newDisposeBag)

        contactData.address.drive { address in
            self.addressLabel.text = address
        }.disposed(by: newDisposeBag)

        contactData.phoneNumber.drive { phoneNumber in
            self.phoneNumberLabel.text = phoneNumber
        }.disposed(by: newDisposeBag)

        emailTextField.rx.text
            .orEmpty.bind(to: self.contactData.email)
            .disposed(by: newDisposeBag)

        self.contactData.email.map { value -> Bool in
            if value.sanitize().isEmpty || value.sanitize().isEmailAddress {
                return true
            }
            return false
        }.bind(to: continueButton.rx.isEnabled)
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

    @IBAction func continueButtonPressed(_ sender: Any) {
        _ = contactData.updateUserData()
            .observe(on: MainScheduler.instance)
            .do(onSubscribe: { [weak self] in
                self?.continueButton.isEnabled = false
            }, onDispose: { [weak self] in
                self?.continueButton.isEnabled = true
            })
            .andThen(Completable.from { [weak self] in self?.kritisCoordinator?.start()})
            .subscribe()
    }
}

extension VoluntaryReachabilityContactDataViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
