import UIKit
import RxSwift
import JGProgressHUD
import Alamofire
import DependencyInjection

enum TabBarIndex: Int {
    case myLuca = 0
    case checkin = 1
    case payment = 2
    case notifications = 3
    case account = 4
}

class MainTabBarViewController: UITabBarController, HandlesLucaErrors {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.documentProcessingService) private var documentProcessingService
    @InjectStatic(\.documentRepoService) private var documentRepoService
    @InjectStatic(\.personRepo) private var personRepo
    @InjectStatic(\.traceIdService) private var traceIdService
    @InjectStatic(\.userService) private var userService
    @InjectStatic(\.accessTraceIdChecker) private var accessTraceIdChecker
    @InjectStatic(\.privateMeetingService) private var privateMeetingService
    @InjectStatic(\.selfCheckin) private var selfCheckin
    @InjectStatic(\.versionChecker) private var versionChecker
    @InjectStatic(\.explanationService) private var explanationService
    @InjectStatic(\.lucaConnectContactDataService) private var lucaConnectContactDataService
    @InjectStatic(\.qrProcessingService) private var qrProcessingService

    private var disposeBag = DisposeBag()

    private var progressHud = JGProgressHUD.lucaLoading()

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self

        tabBar.barTintColor = .black
        tabBar.backgroundColor = .black
        tabBar.tintColor = .white
        tabBar.isTranslucent = false

        let borderView = UIView(frame: CGRect(x: 0, y: 0, width: tabBar.frame.size.width, height: 1))
        borderView.backgroundColor = Asset.lucaGrey.color
        tabBar.addSubview(borderView)

        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)

        // Subscribe in viewDidLoad so that terms of use doesn't pop up immediately again after screen is dismissed
        _ = ShowLatestExplanationInteractor(presenter: self).interact()
            .andThen(TermsOfUseInteractor(presenter: self).interact())
            .subscribe(on: MainScheduler.instance)
            .subscribe()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if let tabBarItems = tabBar.items {
            tabBar.selectionIndicatorImage = UIImage().createTabBarSelectionIndicator(tabSize: CGSize(width: tabBar.frame.width/CGFloat(tabBarItems.count), height: tabBar.frame.height))
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.changeTabBar(to: .checkin)

        versionChecker.errorPresenter = self

        _ = userService.registerIfNeeded()
            .flatMapCompletable { result in
                if result == .userRecreated {
                    return self
                        .traceIdService
                        .disposeData(clearTraceHistory: true)
                }
                return Completable.empty()
            }
            .do(onError: { (error) in
                DispatchQueue.main.async {
                    self.processErrorMessages(error: error)
                }
            })
            .do(onDispose: {
                DispatchQueue.main.async { self.progressHud.dismiss() }
            })
            .subscribe()

        subscribeToSelfCheckin()

        documentProcessingService
            .deeplinkStore
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { deepLink in
                if !deepLink.isEmpty {
                    self.parseQRCode(testString: deepLink)
                }
            })
            .disposed(by: disposeBag)

        // If app was terminated
        if privateMeetingService.currentMeeting != nil {
            self.changeTabBar(to: .checkin)
        }
        _ = self.traceIdService.isCurrentlyCheckedIn
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { checkedIn in
                if checkedIn { self.changeTabBar(to: .checkin) }
            })
            .subscribe()
            .disposed(by: disposeBag)

        handleAppEnteringFromBackground()
        handleNotificationForLucaConnectActivation()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposeBag = DisposeBag()
    }

    private func handleAppEnteringFromBackground() {
        UIApplication.shared.rx.applicationWillEnterForeground
            .flatMap { _ -> Single<(Bool)> in
                self.traceIdService.isCurrentlyCheckedIn.map { checkedIn -> (Bool) in
                    let privateMeeting = self.privateMeetingService.currentMeeting != nil
                    return (checkedIn || privateMeeting)
                }
            }.asObservable()
            .observe(on: MainScheduler.instance)
            .do(onNext: { checkedIn in
                if checkedIn { self.changeTabBar(to: .checkin) }
            }).subscribe()
            .disposed(by: disposeBag)
    }

    private func handleNotificationForLucaConnectActivation() {
        lucaConnectContactDataService.activationNotificationPublisher
                .asObservable()
                .observe(on: MainScheduler.instance)
                .do(onNext: { _ in
                    self.changeTabBar(to: .notifications)
                })
                .subscribe()
                .disposed(by: disposeBag)
    }

    private func parseQRCode(testString: String) {
        let link = L10n.IOSApp.History.Alert.link
        let interactorType = UserInputInteractorConfiguration.InteractorType.modal(
            linkText: link,
            url: try? DependencyContext[\.backendAddressV3].provide().privacyPolicyUrl)
        let interactorConfiguration = UserInputInteractorConfiguration(
            title: L10n.IOSApp.Tests.Uniqueness.Consent.title,
            message: L10n.IOSApp.Tests.Uniqueness.Consent.description(link),
            type: interactorType,
            error: QRProcessingServiceSilentError.userNotAgreedOnPrivacyConsent,
            displayCancelOption: true)

        _ = self.showDialog(configuration: interactorConfiguration)
            .andThen(
                initAdditionalValidators()
                    .flatMapCompletable { self.documentProcessingService.parseQRCode(qr: testString, additionalValidators: $0) }
            )
            .andThen(Completable.from { self.changeTabBar(to: .myLuca) }.subscribe(on: MainScheduler.instance))
            .subscribe(on: MainScheduler.instance)
            .subscribe(onError: { error in
                DispatchQueue.main.async {
                    self.processErrorMessages(error: error)
                }
            })
            .disposed(by: self.disposeBag)

    }

    private func initAdditionalValidators() -> Single<[DocumentValidator]> {
        lucaPreferences.get(\.userRegistrationData)
            .map {
                [VaccinationBirthDateValidator(
                    presenter: self,
                    userFirstName: $0?.firstName ?? "",
                    userLastName: $0?.lastName ?? "",
                    documentSource: self.documentRepoService.currentAndNewTests,
                    personsSource: self.personRepo.restore())
                ]
            }
    }

    private func subscribeToSelfCheckin() {
        // Continuously check if there is any pending self check in request and consume it if its the case
        selfCheckin
            .pendingSelfCheckinRx
            .flatMap({ pendingCheckin in
                return Completable.from { self.selfCheckin.consumeCurrent() }
                    .andThen(self.checkinOrPayment(checkin: pendingCheckin))
            })
            .asObservable()
            .ignoreElementsAsCompletable()
            .catch {
                self.rxErrorAlert(for: $0)
            }
            .logError(self, "Pending self checkin")
            .retry(delay: .seconds(1), scheduler: MainScheduler.instance)
            .subscribe()
            .disposed(by: disposeBag)

    }

    private func checkinOrPayment(checkin: SelfCheckin) -> Completable {
        qrProcessingService.determineQrType(qr: checkin.url.absoluteString)
            .flatMapCompletable { (qrType: QRType) -> Completable in
                if case .checkin = qrType {
                    return self.processCheckin(checkin: checkin)
                }
                throw RxError.unknown
            }.do(onSubscribe: { [weak self] in
                guard let self = self else { return }
                DispatchQueue.main.async { self.progressHud.show(in: self.view) }
            }, onDispose: { [weak self] in DispatchQueue.main.async { self?.progressHud.dismiss() } })
    }

    private func processCheckin(checkin: SelfCheckin) -> Completable {
        return Completable.from { self.selfCheckin.consumeCurrent() }
            .andThen(LocalCheckinInteractor(qrString: checkin.url.absoluteString).interact())
            .catch { error in
                if let err  = error as? LocalCheckinServiceError,
                    err == LocalCheckinServiceError.alreadyCheckedInDifferentLocation {
                        return LocalCheckoutInteractor().interact()
                            .andThen(LocalCheckinInteractor(qrString: checkin.url.absoluteString).interact())
                    }
                throw error
            }
            .do(onSubscribe: {
                DispatchQueue.main.async {
                    self.changeTabBar(to: .checkin)
                }
            })
    }

    private func rxErrorAlert(for error: Error) -> Completable {
        self.processErrorMessagesRx(error: error)
            .andThen(Completable.error(error)) // Push the error through to retry the stream
    }

    func changeTabBar(to index: TabBarIndex) {
        self.selectedIndex = index.rawValue
    }
}

extension MainTabBarViewController: UITabBarControllerDelegate {

    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let nav = viewController as? UINavigationController
        let isCheckinVC = nav?.visibleViewController is LocalCheckinViewController
        return viewController != tabBarController.selectedViewController || !(isCheckinVC)
    }

}

extension MainTabBarViewController: UnsafeAddress, LogUtil {}
