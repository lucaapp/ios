import Foundation
import RxSwift
import RxCocoa
import DependencyInjection

class HealthDepartmentMessageDetailViewModel {
    @InjectStatic(\.lucaConnectMessageFetcher) private var messageFetcher
    private let message: FetchedMessage
    init(message: FetchedMessage) {
        self.message = message
    }

    var date: Driver<String> {
        Single.from { self.message }
        .map { $0.createdAt.formattedDate }
        .asDriver(onErrorJustReturn: "")
    }

    var title: Driver<String> {
        Single.from { self.message }
        .map { $0.message.title }
        .asDriver(onErrorJustReturn: "")
    }

    var content: Driver<String> {
        Single.from { self.message }
        .map { $0.message.message }
        .asDriver(onErrorJustReturn: "")
    }

    func markAsRead() -> Completable {
        messageFetcher.markAsRead(message).asCompletable()
    }
}
