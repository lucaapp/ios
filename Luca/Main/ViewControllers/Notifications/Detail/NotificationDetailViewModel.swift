import Foundation
import RxSwift
import RxCocoa
import DependencyInjection

class NotificationDetailViewModel {
    @InjectStatic(\.traceInfoRepo) private var traceInfoRepo: DataRepo<TraceInfo>
    @InjectStatic(\.notificationConfigCachedDataSource) private var notificationConfigSource
    @InjectStatic(\.accessedTraceIdRepo) private var accessedRepo
    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.locationRepo) private var locationRepo

    var accessedTraceId: AccessedTraceId {
        didSet {
            date.onNext(accessedTraceId.createdAt.formattedDate)
        }
    }

    init(accessedTraceId: AccessedTraceId) {
        self.accessedTraceId = accessedTraceId
        self.date.onNext(accessedTraceId.createdAt.formattedDate)
    }

    var navigationBarTitle: String {
        accessedTraceId.warningLevel == 0 ? L10n.IOSApp.Notification.Details.Title.datarequest : L10n.IOSApp.Notification.Details.Title.infection
    }

    var title: Driver<String> {
        notificationMessage()
            .map { $0.title ?? "" }
            .asDriver(onErrorJustReturn: "Title")
    }

    var message: Driver<String> {
        notificationMessage()
            .map { $0.message ?? "" }
            .asDriver(onErrorJustReturn: "Message")
    }

    var locationName: Driver<String> {
        traceInfo()
            .map { $0?.locationId ?? "" }
            .asObservable()
            .flatMap { locationId in
                self.locationRepo.restore()
                    .compactMap { locations in locations.first(where: { $0.locationId == locationId }) }
                    .map { $0.groupName ?? $0.locationName ?? "" }
            }
            .asDriver(onErrorJustReturn: "locationName")
    }

    var duration: Driver<String> {
        traceInfo()
            .map { ($0?.checkout ?? 0) - ($0?.checkin ?? 0) }
            .map {
                let formatter = DateComponentsFormatter()
                formatter.allowedUnits = [.hour, .minute]
                formatter.unitsStyle = .full

                return formatter.string(from: TimeInterval($0)) ?? ""
            }
            .asDriver(onErrorJustReturn: "duration")
    }

    let date: BehaviorSubject<String> = BehaviorSubject(value: "")

    func markAsRead() -> Completable {
        if self.accessedTraceId.isRead {
            return Completable.empty()
        }
        return Single.from {
            var temp = self.accessedTraceId
            temp.readDate = self.timeProvider.now // self.accessedTraceId.isRead ? nil : self.timeProvider.now
            return temp
        }
        .flatMap { self.accessedRepo.store(object: $0) }
        .asCompletable()
    }
}

extension NotificationDetailViewModel {
    private func traceInfo() -> Single<TraceInfo?> {
        traceInfoRepo.restore()
            .map { traceInfos in traceInfos.first(where: { $0.traceId == self.accessedTraceId.traceId }) }
    }

    private func notificationMessage() -> Observable<Messages> {
        notificationConfigSource.retrieve()
            .compactMap { $0.first }
            .asObservable()
            .map { $0.retrieveMessages(for: self.accessedTraceId.warningLevel, healthDepartment: self.accessedTraceId.healthDepartmentId) }
    }
}
