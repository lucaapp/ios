import UIKit
import LucaUIComponents
import DependencyInjection
import RxAppState

class HealthDepartmentMessageDetailViewController: UIViewController {

    @IBOutlet weak var dateLabel: Luca16PtLabel!
    @IBOutlet weak var titleLabel: Luca16PtBoldLabel!
    @IBOutlet weak var contentLabel: Luca14PtLabel!

    var viewModel: HealthDepartmentMessageDetailViewModel!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        _ = viewModel.title.asObservable().map { $0 as String? }.take(until: self.rx.viewDidDisappear).subscribe(titleLabel.rx.text)
        _ = viewModel.date.asObservable().map { $0 as String? }.take(until: self.rx.viewDidDisappear).subscribe(dateLabel.rx.text)
        _ = viewModel.content.asObservable().map { $0 as String? }.take(until: self.rx.viewDidDisappear).subscribe(contentLabel.rx.text)

        _ = viewModel.markAsRead().subscribe()
    }
}
