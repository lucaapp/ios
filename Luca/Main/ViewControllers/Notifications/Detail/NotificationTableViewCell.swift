import UIKit
import LucaUIComponents
import DependencyInjection

protocol NotificationCellViewModel {
    var isRead: Bool { get }
    var date: Date { get }
    var title: String { get }
    var subtitle: String { get }
    func onSelect(_ presenter: UIViewController)
}

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var dotIndicatorView: UIView!
    @IBOutlet weak var titleLabel: Luca16PtBoldLabel!
    @IBOutlet weak var dateLabel: Luca14PtLabel!
    @IBOutlet weak var notificationLabel: Luca16PtLabel!
    @IBOutlet weak var disclosureImageView: UIImageView!

    func setup(viewModel: NotificationCellViewModel) {
        self.selectionStyle = .none
        self.accessoryType = .none

        dotIndicatorView.layer.cornerRadius = 5

        dotIndicatorView.backgroundColor = viewModel.isRead ? UIColor.white : Asset.lucaError.color
        titleLabel.textColor = viewModel.isRead ? UIColor.white : Asset.lucaError.color

        titleLabel.text = viewModel.title
        notificationLabel.text = viewModel.subtitle
        notificationLabel.numberOfLines = 2
        dateLabel.text = viewModel.date.formattedDate
    }
}
