import UIKit
import LucaUIComponents

class LocalNewsMessageDetailViewController: UIViewController, Themable {
    @IBOutlet weak var notificationTitleLabel: Luca16PtBoldLabel!
    @IBOutlet weak var notificationMessageLabel: Luca14PtLabel!

    var theme: AppearanceTheme = .dark
    var localMessage: LocalNewsMessage!
    var navTitle = L10n.IOSApp.AccessedTraceIdDetailViewController.title

    override func viewDidLoad() {
        title = navTitle
        self.notificationTitleLabel.text = localMessage.title
        self.notificationMessageLabel.text = localMessage.message
    }
}
