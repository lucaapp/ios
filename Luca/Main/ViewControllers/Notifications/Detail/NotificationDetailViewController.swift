import UIKit
import LucaUIComponents
import RxSwift
import RxCocoa

class NotificationDetailViewController: UIViewController, Themable {
    @IBOutlet weak var locationNameLabel: Luca16PtBoldLabel!
    @IBOutlet weak var dateLabel: Luca16PtLabel!
    @IBOutlet weak var childrenLabel: Luca16PtLabel!
    @IBOutlet weak var durationLabel: Luca16PtLabel!
    @IBOutlet weak var notificationTitleLabel: Luca16PtBoldLabel!
    @IBOutlet weak var notificationMessageLabel: Luca14PtLabel!

    var theme: AppearanceTheme = .dark

    var viewModel: NotificationDetailViewModel!
    var disposeBag: DisposeBag?

    override func viewDidLoad() {
        super.viewDidLoad()

        title = viewModel.navigationBarTitle
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        applyToViewAndChildrenControllers(theme: theme)
        bindViewModel()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        disposeBag = nil
    }

    func bindViewModel() {
        let newDisposeBag = DisposeBag()

        viewModel.title
            .do(onNext: { [weak self] _ in
                UIView.animate(withDuration: 0.3) {
                    self?.notificationTitleLabel.alpha = 1.0
                }
            })
            .drive(notificationTitleLabel.rx.text)
            .disposed(by: newDisposeBag)

        viewModel.message
            .do(onNext: { [weak self] _ in
                UIView.animate(withDuration: 0.3) {
                    self?.notificationMessageLabel.alpha = 1.0
                }
            })
            .drive(notificationMessageLabel.rx.text)
            .disposed(by: newDisposeBag)

        viewModel.locationName
            .do(onNext: { [weak self] _ in
                UIView.animate(withDuration: 0.3) {
                    self?.locationNameLabel.alpha = 1.0
                }
            })
            .drive(locationNameLabel.rx.text)
            .disposed(by: newDisposeBag)

        viewModel.duration
            .do(onNext: { [weak self] _ in
                UIView.animate(withDuration: 0.3) {
                    self?.durationLabel.alpha = 1.0
                }
            })
            .drive(durationLabel.rx.text)
            .disposed(by: newDisposeBag)

        viewModel.date
            .asObservable()
            .map { $0 }
            .bind(to: dateLabel.rx.text)
            .disposed(by: newDisposeBag)

        viewModel.markAsRead().subscribe().disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }
}
