import Foundation
import UIKit
import DependencyInjection
import RxSwift

struct AccessedTraceIdNotificationCellViewModel: NotificationCellViewModel {
    var isRead: Bool {
        accessedTraceId.isRead
    }
    var date: Date {
        accessedTraceId.createdAt
    }
    var title: String
    var subtitle: String
    var accessedTraceId: AccessedTraceId
    func onSelect(_ presenter: UIViewController) {
        let viewController = ViewControllerFactory.Notifications.createNotificationDetailViewController(accessedTraceId: accessedTraceId)
        presenter.navigationController?.pushViewController(viewController, animated: true)
    }
}

struct FetchedMessageNotificationCellViewModel: NotificationCellViewModel {

    var isRead: Bool {
        fetchedMessage.readDate != nil
    }
    var date: Date {
        fetchedMessage.createdAt
    }
    var title: String {
        fetchedMessage.message.title
    }
    var subtitle: String {
        fetchedMessage.message.message
    }
    var fetchedMessage: FetchedMessage
    func onSelect(_ presenter: UIViewController) {

        let viewController = ViewControllerFactory.Notifications.createHealthDepartmentDetailViewController(fetchedMessage: fetchedMessage)
        presenter.navigationController?.pushViewController(viewController, animated: true)
    }
}

struct LocalMessageNotificationCellViewModel: NotificationCellViewModel {
    @InjectStatic(\.lucaPayment) private var paymentService
    @InjectStatic(\.localNewsMessageRepo) private var localMessagesRepo
    @InjectStatic(\.timeProvider) private var timeProvider

    var isRead: Bool {
        localMessage.readDate != nil
    }

    var date: Date {
        localMessage.createDate
    }

    var title: String {
        localMessage.title
    }

    var subtitle: String {
        localMessage.message
    }

    var localMessage: LocalNewsMessage

    func onSelect(_ presenter: UIViewController) {
        if localMessage.readDate == nil {
            var newMessage = localMessage
            newMessage.readDate = timeProvider.now
            _ = localMessagesRepo.store(object: newMessage).subscribe()
        }
        switch localMessage.type {
        case .lucaConnectZipCodeUsageConsent: openAccountOption(from: presenter, to: \.lucaConnectPostCodeUsageConsent)
        case .lucaConnectActivate: openAccountOption(from: presenter, to: \.lucaConnect)
        case .idIdentPending: openLucaIdentDetails(from: presenter)
        case .newTermsOfUse: openTermsOfUse(from: presenter)
        case .idIdentFailure: openLucaIdentFailure(from: presenter, localMessage: localMessage)
        case .idIdentSuccess: openLucaIdentSuccess(from: presenter)
        case .paymentRevocation: openPaymentRevocation(from: presenter)
        default: return
        }
    }

    private func openAccountOption(from presenter: UIViewController, to interactable: KeyPath<AccountViewControllerInteractables, String>) {
        if let accountTab = presenter.tabBarController as? MainTabBarViewController {
            accountTab.changeTabBar(to: .account)

            // It's not a perfect solution because:
            // - it requires the knowledge about the construction of this view controller (UINavigationController + rootVC)
            // - it requires the knowledge that the interactables are constructed after appearance (DispatchQueue after some milliseconds)
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                if let accountVC = accountTab.viewControllers?
                    .compactMap({ $0 as? UINavigationController })
                    .flatMap({ $0.viewControllers })
                    .compactMap({ $0 as? AccountViewController })
                    .first,
                   let interactable = accountVC.retrieveInteractable(with: interactable) {
                    interactable.interact()
                }
            }
        }
    }

    private func openLucaIdentDetails(from presenter: UIViewController) {
        let vc = ViewControllerFactory.Notifications.createLucaIdIdentDetailViewController()
        presenter.navigationController?.pushViewController(vc, animated: true)
    }

    private func openTermsOfUse(from presenter: UIViewController) {
        let interactor = TermsOfUseInteractor(presenter: presenter)
        _ = interactor
            .interact()
            .subscribe()
    }

    private func openLucaIdentFailure(from presenter: UIViewController, localMessage: LocalNewsMessage) {
        let vc = ViewControllerFactory.Notifications.createLucaIdFailureDetailViewController(localNewsMessage: localMessage, navTitle: L10n.IOSApp.Navigation.Basic.error)
        presenter.navigationController?.pushViewController(vc, animated: true)
    }

    private func openLucaIdentSuccess(from presenter: UIViewController) {
        let vc = ViewControllerFactory.Notifications.createLucaIdSuccessDetailViewController()
        presenter.navigationController?.pushViewController(vc, animated: true)
    }

    private func openPaymentRevocation(from presenter: UIViewController) {
        _ = paymentService.revocationCode
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { revocationCode in
                let vc = ViewControllerFactory.Notifications.createPaymentRevocationViewController(revocationCode: revocationCode ?? L10n.IOSApp.General.unknown)
                presenter.navigationController?.pushViewController(vc, animated: true)
            })
            .subscribe()
    }
}
