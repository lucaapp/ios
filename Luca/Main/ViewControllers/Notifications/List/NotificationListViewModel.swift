import RxSwift
import RxCocoa
import DependencyInjection
import Foundation

class NotificationListViewModel {

    @InjectStatic(\.accessTraceIdChecker) private var accessTraceIdChecker
    @InjectStatic(\.accessedTraceIdRepo) private var accessedTraceIdRepo
    @InjectStatic(\.notificationConfigCachedDataSource) private var notificationConfigSource
    @InjectStatic(\.fetchedMessagesRepo) private var fetchedMessagesRepo
    @InjectStatic(\.lucaConnectContactDataService) private var lucaConnectContactDataService
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.localNewsMessageRepo) private var localMessagesRepo

    let isLoadingEvents = BehaviorRelay<Bool>(value: true)
    var fetchDisposeBag: DisposeBag! = nil

    private var accessedTraceIdEntries: Single<[NotificationCellViewModel]> {
        self.notificationConfigSource
            .retrieve()
            .asObservable()
            .compactMap { $0.first }
            .flatMap { config in
                self.accessedTraceIdRepo.restore()
                    .asObservable()
                    .flatMap { Observable.from($0) }
                    .map {
                        let msg = config.retrieveMessages(for: $0.warningLevel, healthDepartment: $0.healthDepartmentId)
                        return AccessedTraceIdNotificationCellViewModel(
                            title: msg.title ?? "Unknown title",
                            subtitle: msg.shortMessage ?? "Unknown subtitle",
                            accessedTraceId: $0
                        )
                    }
                    .toArray()
            }
            .ifEmpty(default: [])
            .asSingle()
    }

    private var fetchedMessagesEntries: Single<[NotificationCellViewModel]> {
        fetchedMessagesRepo
            .restore()
            .asObservable()
            .flatMap { Observable.from($0) }
            .map { FetchedMessageNotificationCellViewModel(fetchedMessage: $0) }
            .toArray()
    }

    private var localMessages: Single<[NotificationCellViewModel]> {
        localMessagesRepo
            .restore()
            .asObservable()
            .flatMap { Observable.from($0) }
            .deferredFilter({ self.shouldShowLocalNewsMessage($0) })
            .map { LocalMessageNotificationCellViewModel(localMessage: $0) }
            .toArray()
    }

    private func shouldShowLocalNewsMessage(_ localNewsMessage: LocalNewsMessage) -> Single<Bool> {
        switch localNewsMessage.type {
        case .lucaConnectZipCodeUsageConsent:
            return self.lucaPreferences.get(\.userRegistrationData).map { !String.isNilOrEmpty($0?.postCode) }
        default:
            return Single.just(true)
        }
    }

    private var messagesFromHD: Single<[FetchedMessage]> {
        fetchedMessagesRepo.restore()
            .observe(on: MainScheduler.instance)
    }

    var notifications: Driver<[NotificationCellViewModel]> {
        updateSignal
            .flatMapLatest { _ in
                Observable.merge([
                    self.fetchedMessagesEntries.asObservable(),
                    self.accessedTraceIdEntries.asObservable(),
                    self.localMessages.asObservable()
                ])
                .flatMap { Observable.from($0) }
                .toArray()
                .map { $0.sorted { $0.date > $1.date } }
            }
            .asDriver(onErrorJustReturn: [])
    }

    var hasUnreadNotifications: Driver<Bool> {
        let unreadNotifications = updateSignal
            .flatMapLatest { _ in
                self.notifications
                    .map { !$0.filter { !$0.isRead }.isEmpty }
                    .asObservable()
            }

        let lucaConnectNotSeenYet = lucaPreferences
            .currentAndChanges(\.lucaConnectInitiallyPresented)
            .flatMap { _ in self.presentVoluntaryReachabilityDialog }

        return Observable.combineLatest(unreadNotifications, lucaConnectNotSeenYet)
            .map { $0 || $1 }
            .asDriver(onErrorJustReturn: false)
    }

    var presentVoluntaryReachabilityDialog: Observable<Bool> {
        /*
         determine whether to show the voluntary reachability dialog; based on:
         - has the user already seen the dialog?
         - does the health department support the feature?
         - are the user's documents already ready to be processed?
         */

        Observable.combineLatest(lucaConnectContactDataService.hasResponsibleHealthDepartment().asObservable(),
                                 lucaConnectContactDataService.isFeatureEnabled(),
                                 lucaPreferences.get(\.lucaConnectInitiallyPresented).asObservable())
            .map { $0 && !$1 && !$2 }
    }

    var hideLucaConnectButton: Driver<Bool> {
        Observable.combineLatest(lucaConnectContactDataService.hasResponsibleHealthDepartment().asObservable(),
                                 lucaConnectContactDataService.isFeatureEnabled())
            .map { !$0 || $1 }
            .asDriver(onErrorJustReturn: true)
    }
}

// MARK: - preivate functions

extension NotificationListViewModel {
    private var updateSignal: Observable<Void> {
        Observable.merge(
            UIApplication.shared.rx.currentAndChangedAppState.filter { $0 == .active }.map { _ in Void() },
            accessedTraceIdRepo.onDataChanged,
            fetchedMessagesRepo.onDataChanged,
            localMessagesRepo.onDataChanged,
            lucaPreferences.changes(\.userRegistrationData).map({ _ in Void() })
        )
    }

}
