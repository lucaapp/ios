import UIKit
import RxSwift
import LucaUIComponents
import DependencyInjection

class NotificationListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessageStackView: UIStackView!
    @IBOutlet weak var emptyMessageTitleLabel: Luca16PtBoldLabel!
    @IBOutlet weak var emptyMessageDescriptionLabel: Luca14PtLabel!
    @IBOutlet weak var lucaConnectButton: LightStandardButton!

    var viewModel: NotificationListViewModel = NotificationListViewModel()
    var disposeBag: DisposeBag! = nil
    var coordinator: VoluntaryReachabilityCoordinator?

    @InjectStatic(\.userService) private var userService
    @InjectStatic(\.documentPersonAssociationService) private var documentPersonAssociationService

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        bindCells()
        applyToViewAndChildrenControllers(theme: .dark)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: animated)

        _ = viewModel.presentVoluntaryReachabilityDialog
            .take(1)
            .filter { $0 }
            .observe(on: MainScheduler.instance)
            .take(until: rx.viewDidDisappear)
            .subscribe(onNext: { [weak self] _ in
                self?.presentLucaConnectDialog()
            })

        _ = viewModel.hideLucaConnectButton
            .do(onNext: { [weak self] isHidden in
                self?.lucaConnectButton.superview?.isHidden = isHidden
            })
            .asObservable()
            .take(until: rx.viewDidDisappear)
            .subscribe()
    }

    private func presentLucaConnectDialog() {
        if self.coordinator == nil {
            self.coordinator = VoluntaryReachabilityCoordinator(presenter: self)
        }
        self.coordinator?.start()
    }

    @IBAction func lucaConnectButtonPressed(_ sender: UIButton) {
        presentLucaConnectDialog()
    }
}

extension NotificationListViewController {
    func initBindings() {
        bindViewModel()
    }
}

extension NotificationListViewController {
    private func setupUI() {
        title = L10n.IOSApp.Notification.List.title
        lucaConnectButton.superview?.isHidden = true
        lucaConnectButton.setTitle(L10n.IOSApp.Account.Option.Luca.connect.uppercased(), for: .normal)
        emptyMessageStackView.isHidden = true
    }

    private func bindViewModel() {
        viewModel.hasUnreadNotifications
            .do(onNext: { [weak self] hasUnread in
                self?.tabBarController?.tabBar.items![TabBarIndex.notifications.rawValue].image = hasUnread ? Asset.messagesNew.image : Asset.messages.image
            })
            .asObservable()
            .take(until: rx.deallocating)
            .take(until: userService.onUserDeletedRx)
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func bindCells() {
        disposeBag = DisposeBag()

        viewModel
            .notifications
            .do(onNext: { [weak self] in
                self?.tableView.isHidden = $0.count == 0
                self?.emptyMessageStackView.isHidden = $0.count != 0
            })
            .asObservable()
            .take(until: rx.deallocating)
            .take(until: userService.onUserDeletedRx)
            .bind(to: tableView.rx
                    .items(cellIdentifier: "NotificationTableViewCell")) { _, model, cell in

                guard let cell = cell as? NotificationTableViewCell else {
                    return
                }

                cell.setup(viewModel: model)
            }
        .disposed(by: disposeBag)

        tableView.rx.modelSelected(NotificationCellViewModel.self)
            .do(onNext: { [weak self] model in
                if let self = self { model.onSelect(self) }
            })
            .take(until: rx.deallocating)
            .take(until: userService.onUserDeletedRx)
            .subscribe()
            .disposed(by: disposeBag)
    }

}
