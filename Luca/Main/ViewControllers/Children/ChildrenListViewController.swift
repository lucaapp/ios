import UIKit
import LucaUIComponents
import RxSwift
import JGProgressHUD
import DependencyInjection

class ChildrenListViewController: UIViewController {

    @InjectStatic(\.personService) private var personService

    // MARK: - Outlets

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addChildButton: LightStandardButton!
    @IBOutlet weak var descriptionLabel: Luca14PtLabel!
    @IBOutlet weak var emptyStateImageView: UIImageView!

    private var progressHud = JGProgressHUD.lucaLoading()

    var persons: [Person] = []
    let disposeBag = DisposeBag()

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        reloadData()
    }

    func updateUI() {
        updateDescriptionLabel()
        updateEmptyState()

        addChildButton.isHidden = persons.count > 0
    }

    internal func updateDescriptionLabel() {
        descriptionLabel.text = persons.count == 0 ? L10n.IOSApp.Children.List.emptyDescription : L10n.IOSApp.Children.List.description
    }

    func reloadData() {
        _ = personService
            .retrieve { _ in
                return true
            }
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { entries in
                self.persons = entries
                self.tableView.reloadData()
                self.updateUI()
            })
            .subscribe()
    }

}

// MARK: - Private functions

extension ChildrenListViewController {
    private func setup() {
        tableView.backgroundColor = .clear
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "ChildrenListCell")
        tableView.delegate = self
        tableView.dataSource = self

        title = L10n.IOSApp.Children.List.title
        updateUI()

        addChildButton.setTitle(L10n.IOSApp.Children.List.Add.button.uppercased(), for: .normal)
        addChildButton.addTarget(self, action: #selector(didTapAdd), for: .touchUpInside)
    }

    private func updateEmptyState() {
        let isEmpty = persons.count == 0
        emptyStateImageView.isHidden = !isEmpty
        tableView.isHidden = isEmpty
    }

    private func delete(person: Person, completion: @escaping (Bool) -> Void) {
        _ = personService
            .remove(person: person)
            .observe(on: MainScheduler.instance)
            .do(onError: { _ in
                completion(false)
            }, onCompleted: {
                completion(true)
            })
            .subscribe()
    }
}

// MARK: - Actions

extension ChildrenListViewController {
    @objc
    func didTapAdd() {
        let viewController = ViewControllerFactory.Children.createChildrenCreateViewController(delegate: self)
        present(viewController, animated: true, completion: nil)
    }

    func handleDelete(for indexPath: IndexPath) {
        UIAlertController(
            title: L10n.IOSApp.Children.List.Delete.title,
            message: L10n.IOSApp.Children.List.Delete.message,
            preferredStyle: .alert
        )
        .actionAndCancelAlert(actionText: L10n.IOSApp.Navigation.Basic.confirm, action: {
            self.progressHud.show(in: self.view)
            self.delete(person: self.persons[indexPath.row]) { success in
                if success {
                    self.persons.remove(at: indexPath.row)
                    self.progressHud.dismiss(afterDelay: 0.0, animated: true) {
                        if self.persons.count > 0 {
                            self.tableView.deleteRows(at: [indexPath], with: .automatic)
                        } else {
                            self.tableView.reloadData()
                            self.updateUI()
                        }
                    }
                }
            }
        }, viewController: self)

    }
}

// MARK: - UITableViewDelegate / UITableViewDataSource

extension ChildrenListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        persons.count == 0 ? 1 : 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        section == 0 ? persons.count : 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if indexPath.section == 0 {
            cell = self.tableView.dequeueReusableCell(withIdentifier: "ChildrenListCell", for: indexPath)

            let person = persons[indexPath.row]
            cell.selectionStyle = .none
            cell.textLabel?.font = FontFamily.Montserrat.bold.font(size: 16)
            cell.backgroundColor = .clear
            cell.textLabel?.textColor = .white
            cell.textLabel?.backgroundColor = .clear
            cell.textLabel?.text = person.formattedName
        } else {
            cell = self.tableView.dequeueReusableCell(withIdentifier: "AddChildrenCell", for: indexPath)
            if let c = cell as? AddChildrenCell {
                c.setup()
            }

        }

        return cell
    }

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if indexPath.section == 1 {
            return nil
        }
        let trash = UIContextualAction(style: .destructive, title: "") { [weak self] (_, _, _) in
            self?.handleDelete(for: indexPath)
        }
        trash.image = Asset.deleteBin.image
        trash.backgroundColor = Asset.lucaError.color

        let configuration = UISwipeActionsConfiguration(actions: [trash])

        return configuration
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            self.didTapAdd()
        }
    }
}

// MARK: - ChildrenCreateViewControllerDelegate

extension ChildrenListViewController: ChildrenCreateViewControllerDelegate {
    func didAddPerson() {
        updateUI()
        reloadData()
    }
}
