import UIKit
import RxSwift
import DependencyInjection

class LaunchViewController: UIViewController, HandlesLucaErrors {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.backendMiscV3) private var backendMisc
    @InjectStatic(\.userService) private var userService
    @InjectStatic(\.traceIdService) private var traceIdService
    @InjectStatic(\.dailyPublicKeyVerifier) private var dailyPublicKeyVerifier
    @InjectStatic(\.versionChecker) private var versionChecker

    var keyAlreadyFetched = false

    // Holds last app version where TermsAcceptanceViewController needs to be presented
    let lastTermsUpdatedVersion = 53

    var versionCheckerDisposeBag = DisposeBag()

    // It's a safety check if data has been corrupted between updates. Or the initial state
    var dataComplete: Single<Bool> {
        Single.zip(lucaPreferences.get(\.uuid), userService.isRequiredDataComplete) { uuid, isRequiredDataComplete in
            return uuid != nil && isRequiredDataComplete
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        Task {
            do {
                try await launchStoryboard()
            } catch let error {
                processErrorMessages(error: error)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        _ = dataComplete.flatMapCompletable { dataComplete in
            if !dataComplete {
                return Completable.zip(
                    self.lucaPreferences.set(\.welcomePresented, value: false),
                    self.lucaPreferences.set(\.dataPrivacyPresented, value: false)
                )
            }
            return .empty()
        }
        .subscribe()

        versionChecker.errorPresenter = self
    }

    func launchStoryboard() async throws {

        var viewController: UIViewController!
        let dataComplete = try await dataComplete.task()
        let donePresented = try await lucaPreferences.get(\.donePresented).task()
        if !(try await lucaPreferences.get(\.welcomePresented).task()) {
            viewController = ViewControllerFactory.Onboarding.createWelcomeViewController()
        } else if !dataComplete {
            if !(try await lucaPreferences.get(\.dataPrivacyPresented).task()) {
                viewController = ViewControllerFactory.Onboarding.createDataPrivacyViewController()
            } else {
                try await lucaPreferences.set(\.userRegistrationData, value: UserRegistrationData()).task()
                try await lucaPreferences.set(\.currentOnboardingPage, value: 0).task()
                try await lucaPreferences.set(\.phoneNumberVerified, value: false).task()
                try await traceIdService.disposeData(clearTraceHistory: true).task()

                viewController = ViewControllerFactory.Onboarding.createFormViewController()
            }
        } else if dataComplete && !donePresented {
            viewController = ViewControllerFactory.Onboarding.createDoneViewController()
        } else if (try await lucaPreferences.get(\.termsAcceptedVersion).task()) < lastTermsUpdatedVersion {
            viewController = ViewControllerFactory.Terms.createTermsAcceptanceViewController()
        } else {
            viewController = ViewControllerFactory.Main.createTabBarController()
        }
        viewController.modalPresentationStyle = .fullScreen
        viewController.modalTransitionStyle = .crossDissolve
        self.present(viewController, animated: true, completion: nil)
    }

    private func showErrorAlert(for error: LocalizedTitledError) {
        DispatchQueue.main.async {
            let alert = UIAlertController.infoBox(
                title: error.localizedTitle,
                message: error.localizedDescription)

            UIViewController.visibleViewController?.present(alert, animated: true, completion: nil)
        }
    }
}

extension LaunchViewController: UnsafeAddress, LogUtil {}
