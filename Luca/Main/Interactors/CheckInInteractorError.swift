import Foundation

enum CheckInInteractorError: LocalizedTitledError {

    case dailyKeyOutdated
    case incompleteContactData
    case phoneNumberNotVerified
}

extension CheckInInteractorError {

    var localizedTitle: String {
        switch self {
        case .dailyKeyOutdated:
            return L10n.IOSApp.Checkin.Scanner.publicKeyOutdatedTitle
        case .incompleteContactData:
            return L10n.IOSApp.Checkin.Scanner.incompleteContactDataTitle
        case .phoneNumberNotVerified:
            return L10n.IOSApp.FormViewController.VerificationCancelled.message
        }
    }

    var errorDescription: String? {
        switch self {
        case .dailyKeyOutdated:
            return L10n.IOSApp.Checkin.Scanner.publicKeyOutdatedDescription
        case .incompleteContactData:
            return L10n.IOSApp.Checkin.Scanner.incompleteContactDataDescription
        case .phoneNumberNotVerified:
            return L10n.IOSApp.Navigation.Basic.error
        }
    }
}
