import Foundation
import RxSwift
import UIKit
import DependencyInjection
import JGProgressHUD

public class AccountDataReportInteractor: NSObject, Interactor {

    @InjectStatic(\.contactDataReporter) private var contactDataReporter
    @InjectStatic(\.documentsReporter) private var documentsReporter
    @InjectStatic(\.lucaIDReporter) private var lucaIDReporter
    @InjectStatic(\.paymentDataReporter) private var paymentDataReporter
    @InjectStatic(\.stagedRolloutService) private var stagedRolloutService

    private let loading = JGProgressHUD.lucaLoading()
    private weak var presenter: UIViewController?
    var disposeBag = DisposeBag()

    public init(presenter: UIViewController) {
        self.presenter = presenter
    }

    private func shareReport(report: Single<String>) {
        guard let presenter = presenter else { return }
        report
            .handleErrors(presenter)
            .observe(on: MainScheduler.instance)
            .map { [weak self] report in
                let activityViewController = UIActivityViewController(activityItems: [report], applicationActivities: nil)
                self?.presenter?.present(activityViewController, animated: true, completion: nil)
            }
            .handleLoading(loading, parent: presenter.view)
            .subscribe()
            .disposed(by: disposeBag)
    }

    public func interact() -> Completable {
        guard let presenter = presenter else { return .empty() }

        return stagedRolloutService.isFeatureEnabled(type: .lucaId)
            .take(1)
            .handleErrors(presenter)
            .observe(on: MainScheduler.instance)
            .do(onNext: { isLucaIDEnabled in
                let alert = UIAlertController(title: L10n.IOSApp.DataReport.AccountSettings.title, message: nil, preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: L10n.IOSApp.DataReport.AccountSettings.contactReport, style: .default, handler: { _ in
                    self.shareReport(report: self.contactDataReporter.createReport())
                }))
                alert.addAction(UIAlertAction(title: L10n.IOSApp.DataReport.AccountSettings.certificatesReport, style: .default, handler: { _ in
                    self.shareReport(report: self.documentsReporter.createReport())
                }))
                if isLucaIDEnabled {
                    alert.addAction(UIAlertAction(title: L10n.IOSApp.DataReport.AccountSettings.lucaIDReport, style: .default, handler: { _ in
                        self.shareReport(report: self.lucaIDReporter.createReport())
                    }))
                }
                alert.addAction(UIAlertAction(title: L10n.IOSApp.DataReport.AccountSettings.lucaPay, style: .default, handler: { _ in
                    self.shareReport(report: self.paymentDataReporter.createReport())
                }))
                alert.addAction(UIAlertAction(title: L10n.IOSApp.Navigation.Basic.cancel, style: .cancel))
                self.presenter?.present(alert, animated: true)
            })
            .ignoreElementsAsCompletable()
            .handleLoading(loading, parent: presenter.view)
    }
}
