import Foundation
import RxSwift

protocol ValueInteractor {

    func interact<T>(value: T) -> Completable

}
