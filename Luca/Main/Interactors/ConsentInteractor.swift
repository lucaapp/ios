import UIKit
import RxSwift
import DependencyInjection

class ConsentInteractor: ValueInteractor {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    private weak var presenter: UIViewController?
    private var keyPath: KeyPath<LucaPreferencesTemplate, PreferencesEntry<Bool>>

    private var consentTitle: String
    private var consentDescription: String

    init(presenter: UIViewController, keyPath: KeyPath<LucaPreferencesTemplate, PreferencesEntry<Bool>>, consentTitle: String, consentDescription: String) {
        self.presenter = presenter
        self.keyPath = keyPath
        self.consentTitle = consentTitle
        self.consentDescription = consentDescription
    }

    func interact<T>(value: T) -> Completable {
        guard let presenter = presenter, let value = value as? Bool else { return Completable.error(SilentErrorDefaults.resourceNotAvailable) }
        return set(toValue: value, presenter: presenter)
    }

    private func set(toValue: Bool, presenter: UIViewController) -> Completable {
        lucaPreferences.get(keyPath)
            .observe(on: MainScheduler.asyncInstance)
            .flatMapCompletable { value in
                if toValue && value != true {
                    return self.handleConsent(presenter: presenter)
                        .andThen(self.lucaPreferences.set(self.keyPath, value: toValue))
                }
                return self.lucaPreferences.set(self.keyPath, value: toValue)
            }
    }

    func handleConsent(presenter: UIViewController) -> Completable {
        showConsent(presenter: presenter)
            .subscribe(on: MainScheduler.asyncInstance)
    }

    func showConsent(presenter: UIViewController) -> Completable {
        Completable.deferred { [weak self] in
            guard let self = self else { return .empty() }

            let description = [NantesLabelData(text: self.consentDescription, linkText: nil, url: nil)]
            let vc = ViewControllerFactory.Checkin.createConsentViewController(
                title: self.consentTitle,
                buttonTitle: L10n.IOSApp.Navigation.Basic.accept,
                description: description
            )
            let nc = UINavigationController(rootViewController: vc)
            presenter.present(nc, animated: true, completion: nil)
            return vc.result.flatMapCompletable { consent in
                if !consent {
                    // Consent not given
                    throw SilentErrorDefaults.userDeclined
                }
                return .empty()
            }
            .do(onCompleted: { nc.dismiss(animated: true, completion: nil) })
        }
        .subscribe(on: MainScheduler.asyncInstance)
    }

}
