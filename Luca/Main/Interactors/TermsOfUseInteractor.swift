import UIKit
import RxSwift
import DependencyInjection

class TermsOfUseInteractor: Interactor {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.localNewsMessageRepo) private var localMessageRepo
    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.lucaIDTermsOfUse) private var lucaIDToS

    private weak var presenter: UIViewController?
    private weak var navigationControllerPresenter: UINavigationController?
    private var backButtonEnabled = true

    init(presenter: UIViewController) {
        self.presenter = presenter
    }

    init(navigationControllerPresenter: UINavigationController, backButtonEnabled: Bool = true) {
        self.navigationControllerPresenter = navigationControllerPresenter
        self.backButtonEnabled = backButtonEnabled
    }

    func interact() -> Completable {
        Single.zip(
            lucaPreferences.get(\.uuid).map { $0 != nil },
            lucaIDToS.isAccepted.map { $0 ?? false }
        )
        .flatMapCompletable { userIsRegistered, granted in
            guard userIsRegistered && !granted else { return .empty() }

            return self.showConsent()
                .andThen(self.lucaIDToS.setAsAccepted())
                .andThen(self.deleteTermsOfUseMessage())
                .subscribe(on: MainScheduler.asyncInstance)
        }
    }

    private func deleteTermsOfUseMessage() -> Completable {
        return localMessageRepo
            .restore()
            .asObservable()
            .map { messages in
                let termsMessages = messages.filter { $0.type == .newTermsOfUse }
                return termsMessages.filter { $0.identifier != nil }.map { $0.identifier! }
            }.flatMap { identifiers -> Completable in
                self.localMessageRepo.remove(identifiers: identifiers)
            }.ignoreElementsAsCompletable()
    }

    private func showConsent() -> Completable {
        if let navigationController = navigationControllerPresenter {
            return showConsentEmbedded(navigationController: navigationController)
        } else if let presenter = presenter {
            return showConsentStandalone(presenter: presenter)
        }
        return .error(SilentErrorDefaults.resourceNotAvailable)
    }

    private func showConsentEmbedded(navigationController: UINavigationController) -> Completable {
        Completable.deferred {
            let vc = self.consentViewController()

            if !self.backButtonEnabled {
                let backButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
                backButton.isEnabled = false
                vc.navigationItem.leftBarButtonItem = backButton
                navigationController.interactivePopGestureRecognizer?.isEnabled = false
            }

            navigationController.pushViewController(vc, animated: true)

            return vc.result.flatMapCompletable { consent in
                if !consent {
                    // Consent not given
                    throw SilentErrorDefaults.userDeclined
                }
                return .empty()
            }
            .do(onCompleted: { navigationController.dismiss(animated: true, completion: nil) })
        }
        .subscribe(on: MainScheduler.asyncInstance)
    }

    private func showConsentStandalone(presenter: UIViewController) -> Completable {
        Completable.deferred {

            let nc = UINavigationController(rootViewController: self.consentViewController())

            guard let vc = nc.viewControllers.first as? ConsentViewController else {
                throw SilentErrorDefaults.resourceNotAvailable
            }
            nc.modalPresentationStyle = .fullScreen

            presenter.present(nc, animated: true, completion: nil)
            return vc.result.flatMapCompletable { consent in
                if !consent {
                    // Consent not given
                    throw SilentErrorDefaults.userDeclined
                }
                return .empty()
            }
            .do(onCompleted: { nc.dismiss(animated: true, completion: nil) })
        }
        .subscribe(on: MainScheduler.asyncInstance)
    }

    private func consentViewController() -> ConsentViewController {
        let description = [
            NantesLabelData(
                text: L10n.IOSApp.TermsOfUse.Sheet.description1,
                linkText: L10n.IOSApp.TermsOfUse.Link.term1,
                url: L10n.IOSApp.TermsOfUse.Link.url
            ),
            NantesLabelData(
                text: L10n.IOSApp.TermsOfUse.Sheet.description2,
                linkText: L10n.IOSApp.TermsOfUse.Link.term2,
                url: L10n.IOSApp.WelcomeViewController.linkTC
            )
        ]

        return ViewControllerFactory.Checkin.createConsentViewController(
            title: L10n.IOSApp.TermsOfUse.Notification.title,
            buttonTitle: L10n.IOSApp.Navigation.Basic.accept,
            description: description
        )
    }

}
