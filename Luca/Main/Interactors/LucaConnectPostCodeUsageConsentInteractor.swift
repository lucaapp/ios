import Foundation
import RxSwift

class LucaConnectPostCodeUsageConsentInteractor: ConsentInteractor {

    init(presenter: UIViewController) {
        super.init(presenter: presenter,
                   keyPath: \.lucaConnectPostCodeUsageConsent,
                   consentTitle: L10n.IOSApp.Postcode.Check.Toggle.title,
                   consentDescription: L10n.IOSApp.Postcode.Check.Consent.description)
    }

}
