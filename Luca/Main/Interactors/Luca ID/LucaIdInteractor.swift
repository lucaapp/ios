import UIKit
import RxSwift
import DependencyInjection
import JGProgressHUD

enum LucaIdInteractorError: LocalizedTitledError {

    case invalidDeeplink

}

extension LucaIdInteractorError {

    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        "\(self)"
    }
}

class LucaIdInteractor: Interactor {

    @InjectStatic(\.attestationService) private var attestationService
    @InjectStatic(\.lucaIDService) private var lucaIDService

    private var presenter: UIViewController?
    private var navigationController: UINavigationController?
    private var loadingIndicatorViewController: UIViewController?

    static let idNowDeeplink = "https://apps.apple.com/de/app/idnow-autoident/id1437143249"

    public init(presenter: UIViewController) {
        self.presenter = presenter
    }

    func interact() -> Completable {
        guard let presenter = presenter else { return Completable.error(SilentErrorDefaults.resourceNotAvailable) }
        navigationController = presenter.navigationController

        return presentLoadingIndicator()
            .andThen(attestationService.checkPreconditionsIfNeeded())
            .andThen(removeLoadingIndicator())
            .andThen(showInfoScreen(presenter: presenter))
            .andThen(showConsent())
            .andThen(presentLoadingIndicator())
            .andThen(requestIdentProcess())
            .andThen(removeLoadingIndicator())
            .andThen(showSuccessScreen())
            .observe(on: MainScheduler.instance)
            .catch { [weak self] error in
                guard let self = self else { return Completable.error(SilentErrorDefaults.resourceNotAvailable) }
                return self.removeLoadingIndicator()
                    .andThen(Completable.from {
                        if let localizedError = error as? LocalizedTitledError {
                            let alert = UIAlertController.infoAlert(title: localizedError.localizedTitle, message: localizedError.localizedDescription)
                            self.navigationController?.present(alert, animated: true, completion: nil)
                        }
                    })
            }
    }

    private func showInfoScreen(presenter: UIViewController) -> Completable {
        Completable.deferred {
            let vc: DescriptionAndImageModalViewController = DescriptionAndImageModalViewController.fromStoryboard()

            vc.titleText = L10n.IOSApp.Id.idInfoscreenHeadline
            vc.descriptionText = L10n.IOSApp.Id.idInfoscreenText
            vc.buttonText = L10n.IOSApp.Id.idInfoscreenButton
            vc.image = Asset.addIDCard.image

            self.navigationController = UINavigationController(rootViewController: vc)
            presenter.present(self.navigationController!, animated: true)
            return vc.result.flatMapCompletable { next in
                if !next {
                    throw SilentErrorDefaults.userDeclined
                }
                return .empty()
            }
        }.subscribe(on: MainScheduler.asyncInstance)
    }

    private func showSuccessScreen() -> Completable {
        Completable.deferred {
            let vc: DescriptionAndImageModalViewController = DescriptionAndImageModalViewController.fromStoryboard()

            vc.titleText = L10n.IOSApp.Id.idRequestSentHeadline
            vc.descriptionText = L10n.IOSApp.Id.idRequestSentText
            vc.buttonText = L10n.IOSApp.Id.idRequestSentButton
            vc.image = Asset.lucaIdCheckmark.image
            vc.navigationItem.setHidesBackButton(true, animated: false)

            self.navigationController?.pushViewController(vc, animated: true)

            return vc.result.flatMapCompletable { next in
                if !next {
                    throw SilentErrorDefaults.userDeclined
                }
                return .empty()
            }
            .andThen(self.successButtonPressed(presenter: vc))
        }.subscribe(on: MainScheduler.asyncInstance)
    }

    private func successButtonPressed(presenter: UIViewController) -> Completable {
        Completable.from {
            if let url = URL(string: Self.idNowDeeplink), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, completionHandler: nil)
                presenter.dismiss(animated: true, completion: nil)
            } else {
                throw LucaIdInteractorError.invalidDeeplink
            }
        }
    }

    private func showConsent() -> Completable {
        Completable.deferred {
            let description = [NantesLabelData(text: L10n.IOSApp.Id.idConsentText, linkText: nil, url: nil)]
            let vc: ConsentViewController = ViewControllerFactory.Checkin.createConsentViewController(
                title: L10n.IOSApp.Id.idConsentHeadline,
                buttonTitle: L10n.IOSApp.Id.idConsentButton,
                description: description,
                backgroundColor: Asset.luca1d1d1d.color
            )

            vc.navigationItem.setHidesBackButton(true, animated: false)
            self.navigationController?.pushViewController(vc, animated: true)

            return vc.result.flatMapCompletable { consent in
                if !consent {
                    throw SilentErrorDefaults.userDeclined
                }
                return .empty()
            }
        }.subscribe(on: MainScheduler.asyncInstance)
    }

    private func requestIdentProcess() -> Completable {
        Completable.deferred { [weak self] in
            guard let self = self else { throw RxError.unknown }
            return self.lucaIDService.requestIdentProcess().asCompletable()
        }
    }

    // MARK: - Loading indicator handling

    private func presentLoadingIndicator() -> Completable {
        return Completable.create { observer -> Disposable in
            self.loadingIndicatorViewController = UIViewController()
            let vc = self.loadingIndicatorViewController!

            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext

            let loadingHUD = JGProgressHUD.lucaLoading()
            loadingHUD.show(in: vc.view, animated: true)

            self.navigationController?.present(vc, animated: false, completion: {
                observer(.completed)
            })
            return Disposables.create()
        }.subscribe(on: MainScheduler.asyncInstance)
    }

    private func removeLoadingIndicator() -> Completable {
        Completable.create { observer -> Disposable in
            if let loadingVC = self.loadingIndicatorViewController {
                loadingVC.dismiss(animated: false, completion: {
                    observer(.completed)
                })
            } else {
                observer(.completed)
            }
            return Disposables.create()
        }.subscribe(on: MainScheduler.asyncInstance)
    }
}
