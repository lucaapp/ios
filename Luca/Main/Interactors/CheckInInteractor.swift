import Foundation
import UIKit
import RxSwift
import DependencyInjection

class CheckInInteractor: Interactor {
    private weak var presenter: UIViewController?
    private let qr: String

    @InjectStatic(\.keyValueRepo) private var keyValueRepo: KeyValueRepoProtocol
    @InjectStatic(\.traceIdService) private var traceIdService: TraceIdService
    @InjectStatic(\.dailyPublicKeyVerifier) private var dailyKeyVerifier: DailyPublicKeyVerifier
    @InjectStatic(\.lucaPreferences) private var lucaPreferences: LucaPreferences
    @InjectStatic(\.userService) private var userService: UserService

    init(presenter: UIViewController, qrString: String) {
        self.presenter = presenter
        self.qr = qrString
    }

    func interact() -> Completable {
        guard let presenter = presenter else { return Completable.empty() }
        return checkIfDailyKeyIsValid()
        .andThen(checkIfUserContactDataIsComplete())
        .andThen(checkIfPhoneNumberIsVerified())
        .andThen(Single<SelfCheckin>.from {
            guard let url = URL(string: self.qr), let checkin = CheckInURLParser.parse(url: url) else {
                throw QRProcessingError.parsingFailed
            }
            return checkin
        })
        .flatMap {
            if $0 is PrivateMeetingSelfCheckin {
                return self.showPrivateMeetingConsent(viewController: presenter).andThen(Single.just($0))
            }
            return Single.just($0)
        }
        .flatMap { checkin in
            self.traceIdService.fetchScanner(for: checkin)
                .map { (checkin, $0) }
        }
        .flatMap { (checkin: SelfCheckin, scannerInfo: ScannerInfo) in
            // don't show consent for private meetings
            if checkin is PrivateMeetingSelfCheckin {
                return Single.just((checkin, scannerInfo, true))
            }
            return self.traceIdService.downloadLocationInfo(for: scannerInfo.locationId)
                .flatMap { location in
                    self.checkCheckInConsent(location: location, presenter: presenter)
                        .andThen(self.shouldShowVoluntaryCheckinConsent(for: location, on: presenter))
                        .map { shouldCheckInAnonymous in
                            (checkin, scannerInfo, shouldCheckInAnonymous)
                        }
                }
        }
        .flatMapCompletable { (checkin: SelfCheckin, scannerInfo: ScannerInfo, shouldCheckInAnonymous: Bool) in
            self.traceIdService.checkIn(
                selfCheckin: checkin,
                scannerInfo: scannerInfo,
                anonymous: shouldCheckInAnonymous
            )
        }
        .andThen(self.traceIdService.fetchTraceStatusRx())
        .asCompletable()
    }

    private func checkIfDailyKeyIsValid() -> Completable {
        dailyKeyVerifier.isKeyValid
            .asObservable()
            .take(1)
            .asSingle()
            .do(onSuccess: { (valid: Bool) in
                guard valid else { throw CheckInInteractorError.dailyKeyOutdated }
            })
            .asCompletable()
    }

    private func checkIfUserContactDataIsComplete() -> Completable {
        userService.isUserDataComplete
            .do(onSuccess: { isComplete in
                guard isComplete else { throw CheckInInteractorError.incompleteContactData }
            })
            .asCompletable()
    }

    private func checkIfPhoneNumberIsVerified() -> Completable {
        lucaPreferences.get(\.phoneNumberVerified)
            .do(onSuccess: {
                if !$0 {
                    throw CheckInInteractorError.phoneNumberNotVerified
                }
            })
            .asCompletable()
    }

    private func showPrivateMeetingConsent(viewController: UIViewController) -> Completable {
        Completable.create { observer -> Disposable in
            let alert = UIAlertController
                .actionAndCancelAlert(
                    title: L10n.IOSApp.Navigation.Basic.hint,
                    message: L10n.IOSApp.Private.Meeting.Alert.description,
                    actionTitle: L10n.IOSApp.Navigation.Basic.continue, action: {
                        observer(.completed)
                    }, cancelAction: {
                        observer(.error(SilentErrorDefaults.userDeclined))
                    })

            viewController.present(alert, animated: true, completion: nil)

            return Disposables.create { alert.dismiss(animated: true, completion: nil) }
        }
        .subscribe(on: MainScheduler.instance)
    }

    private func checkCheckInConsent(location: Location, presenter: UIViewController) -> Completable {
        lucaPreferences.get(\.dontShowCheckinConsent)
            .catch { _ in Single.just(false) }
            .flatMapCompletable { value in
                let directCheckin = value ?? false
                if !directCheckin {
                    return self.showCheckInConsent(location: location, presenter: presenter)
                }

                return Completable.empty()
            }
    }

    private func showCheckInConsent(location: Location, presenter: UIViewController) -> Completable {
        return Completable.create { observer -> Disposable in
            let message = L10n.IOSApp.Checkin.Consent.description(location.groupName ?? "-")
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: L10n.IOSApp.Navigation.Basic.cancel, style: .cancel, handler: { _ in
                observer(.error(QRProcessingServiceSilentError.userNotAgreedOnCheckIn)) }))
            alert.addAction(UIAlertAction(title: L10n.IOSApp.Navigation.Basic.confirm, style: .default, handler: { _ in observer(.completed) }))
            alert.addAction(UIAlertAction(title: L10n.IOSApp.Checkin.Consent.dontAskAgain, style: .default, handler: { [weak self] _ in
                if let self = self {
                    _ = self.lucaPreferences.set(\.dontShowCheckinConsent, value: true).subscribe()
                }
                observer(.completed)
            }))

            presenter.present(alert, animated: true, completion: nil)
            return Disposables.create { alert.dismiss(animated: true, completion: nil) }
        }
        .subscribe(on: MainScheduler.instance)
    }

    private func shouldShowVoluntaryCheckinConsent(for location: Location, on presenter: UIViewController) -> Single<Bool> {
        .just(false)

        // Disable this logic for the time being. It will be enabled soon
//        lucaPreferences.get(\.dontShowVoluntaryCheckinConsent)
//            .flatMap { dontShowConsent in
//                if let dontShowConsent = dontShowConsent, dontShowConsent {
//                    return self.lucaPreferences.get(\.checkInAnonymousWhenPossible).map { $0 ?? true }
//                } else {
//                    // Show consent and check if should check in anonymous
//                    return self.shouldCheckInAnonymous(for: location, on: presenter)
//                }
//            }
    }

    /// Emits the answer user has selected or just `false` if `isContactDataMandatory` is nil or true.
    /// It fails with `SilentErrorDefaults.userDeclined` if user cancels or swipes down the view controller
    private func shouldCheckInAnonymous(for location: Location, on presenter: UIViewController) -> Single<Bool> {
//        .just(false)
        // Disable this logic for the time being. It will be enabled soon
        if location.isContactDataMandatory == nil || location.isContactDataMandatory == true {
            return Single.just(false)
        }
        return Single.deferred {
            let nc = ViewControllerFactory.Checkin.createOptionalDataCheckinViewController()
            guard let vc = nc.viewControllers.first as? OptionalCheckinDataViewController else {
                return Single.just(false)
            }
            presenter.present(nc, animated: true, completion: nil)
            return vc.result
                .ifEmpty(switchTo: Single.error(SilentErrorDefaults.userDeclined)) // If no answer (user has canceled the view), just break the stream
        }
        .subscribe(on: MainScheduler.asyncInstance)
    }
}
