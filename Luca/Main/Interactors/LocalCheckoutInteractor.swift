import RxSwift
import DependencyInjection
import UIKit

class LocalCheckoutInteractor: Interactor {

    @InjectStatic(\.localCheckinService) private var localCheckinService
    @InjectStatic(\.traceIdService) private var traceIdService

    func interact() -> Completable {
        traceIdService.isCurrentlyCheckedIn
            .flatMapCompletable { isCheckedIn in
                if isCheckedIn {
                    return self.traceIdService.checkOut()
                }
                return .empty()
            }
            .andThen(localCheckinService.checkout())
    }

}
