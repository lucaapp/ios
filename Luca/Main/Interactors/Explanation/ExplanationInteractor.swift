import UIKit
import RxSwift
import DependencyInjection

class ShowExplanationInteractor: Interactor {

    var showAllCloseButtons: Bool = false

    private var topic: ExplanationTopic
    private weak var presenter: UIViewController?
    @InjectStatic(\.explanationService) private var explanationService

    init(topic: ExplanationTopic, presenter: UIViewController) {
        self.topic = topic
        self.presenter = presenter
    }

    func interact() -> Completable {
        return Completable.create { observer -> Disposable in
            let explanationContainerViewController = ViewControllerFactory.Main.createExplanationContainerViewController(topic: self.topic, showAllCloseButtons: self.showAllCloseButtons)

            explanationContainerViewController.onCompleted = { [weak self] in
                guard let unwrappedSelf = self else { return }
                _ = unwrappedSelf.explanationService.setDisplayed(unwrappedSelf.topic)
                    .subscribe()
            }

            explanationContainerViewController.onDisappear = {
                observer(.completed)
            }

            let explanationPresenter = self.presenter is UITabBarController ? self.presenter : self.presenter?.tabBarController
            explanationPresenter?.present(explanationContainerViewController, animated: true)
            return Disposables.create()
        }.subscribe(on: MainScheduler.instance)
    }
}

class ShowLatestExplanationInteractor: Interactor {

    private weak var presenter: UIViewController?
    @InjectStatic(\.explanationService) private var explanationService

    init(presenter: UIViewController) {
        self.presenter = presenter
    }

    func interact() -> Completable {
        explanationService.lastSeenAppVersion
            .flatMapCompletable { lastSeenAppVersion in
                guard let presenter = self.presenter,
                      let latestTopic = self.explanationService.topics.filter({ $0.appVersion > lastSeenAppVersion }).sorted(by: { $0.appVersion < $1.appVersion }).last else {
                    return .empty()
                }

                let showExplanationInteractor = ShowExplanationInteractor(topic: latestTopic, presenter: presenter)
                showExplanationInteractor.showAllCloseButtons = true
                return showExplanationInteractor.interact()
            }
    }
}
