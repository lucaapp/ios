import Foundation
import RxSwift
import DependencyInjection

enum ShareHealthStatusInteractorError: LocalizedTitledError {
    case noDocuments
    case invalidDocuments
}

extension ShareHealthStatusInteractorError {
    var localizedTitle: String {
        switch self {
        case .noDocuments: return L10n.IOSApp.HealthStatusConsent.NoDocumentsAlert.title
        case .invalidDocuments: return L10n.IOSApp.HealthStatusConsent.InvalidDocumentsAlert.title
        }
    }

    var errorDescription: String? {
        switch self {
        case .noDocuments: return L10n.IOSApp.HealthStatusConsent.NoDocumentsAlert.message
        case .invalidDocuments: return L10n.IOSApp.HealthStatusConsent.InvalidDocumentsAlert.message
        }
    }
}

class ShareHealthStatusInteractor: ConsentInteractor {

    @InjectStatic(\.documentPersonAssociationService) private var documentPersonAssociationService

    init(presenter: UIViewController) {
        super.init(presenter: presenter, keyPath: \.shareHealthStatus, consentTitle: L10n.IOSApp.HealthStatusConsent.title, consentDescription: L10n.IOSApp.HealthStatusConsent.description)
    }

    /// Emits an error of consent wasn't given
    override func handleConsent(presenter: UIViewController) -> Completable {
        checkIfNoDocuments()
            .andThen(showConsent(presenter: presenter))
            .subscribe(on: MainScheduler.asyncInstance)
    }

    private func checkIfNoDocuments() -> Completable {
        documentPersonAssociationService
            .createUsersPerson()
            .flatMap(documentPersonAssociationService.healthStatus(for:))
            .flatMapCompletable { [weak self] status in
                guard let self = self else {
                    throw NSError(domain: "Missing weak self", code: 0, userInfo: nil)
                }
                if status == HealthStatus.notShared.rawValue {
                    return self.documentPersonAssociationService
                                .createUsersPerson()
                                .flatMap(self.documentPersonAssociationService.invalidDocuments(for:))
                                .flatMapCompletable { invalidDocs in
                                    if invalidDocs.count > 0 {
                                        throw ShareHealthStatusInteractorError.invalidDocuments
                                    }
                                    throw ShareHealthStatusInteractorError.noDocuments
                                }
                }
                return .empty()
            }
    }

}
