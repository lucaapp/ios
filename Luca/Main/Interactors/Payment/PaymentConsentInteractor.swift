import RxSwift
import UIKit
import DependencyInjection

class PaymentConsentInteractor: Interactor {
    @InjectStatic(\.lucaPaymentConsent) private var paymentConsent
    @InjectStatic(\.backendAddressV4) private var backendAddress

    private weak var presenter: UIViewController?

    init(presenter: UIViewController) {
        self.presenter = presenter
    }

    func interact() -> Completable {
        paymentConsent.isAccepted
            .map { $0 ?? false }
            .flatMapCompletable { isAccepted in
                if isAccepted { return .empty() }
                return self.showConsent().andThen(self.paymentConsent.setAsAccepted())
            }
    }

    private func showConsent() -> Completable {
        Completable.deferred {
            guard let presenter = self.presenter else {
                throw SilentErrorDefaults.resourceNotAvailable
            }

            let vc = ViewControllerFactory.Checkin.createConsentViewController(
                title: L10n.IOSApp.Payment.Consent.title,
                buttonTitle: L10n.IOSApp.Payment.Consent.accept,
                description: [
                    NantesLabelData(
                        text: L10n.IOSApp.Payment.Consent.content,
                        linkText: L10n.IOSApp.Payment.Consent.linkPlaceholder,
                        url: self.backendAddress.privacyPolicyUrl?.absoluteString ?? ""
                    )
                ]
            )
            let nc = UINavigationController(rootViewController: vc)

            presenter.present(nc, animated: true)
            return vc.result.flatMapCompletable { consent in
                if !consent {
                    throw SilentErrorDefaults.userDeclined
                }
                return .empty()
            }
            .observe(on: MainScheduler.asyncInstance)
            .do(onDispose: { nc.dismiss(animated: true) })
        }
        .subscribe(on: MainScheduler.asyncInstance)
    }
}
