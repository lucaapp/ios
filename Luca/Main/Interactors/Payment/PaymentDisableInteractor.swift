import RxSwift
import DependencyInjection

class PaymentDisableInteractor: Interactor {
    @InjectStatic(\.lucaPayment) private var payment
    @InjectStatic(\.lucaPaymentConsent) private var consent

    private weak var presenter: UIViewController?

    init(presenter: UIViewController) {
        self.presenter = presenter
    }

    func interact() -> Completable {
        showQuestion()
            .andThen(consent.setAsDenied())
            .andThen(payment.remove())
    }

    private func showQuestion() -> Completable {
        Completable.create { observer in
            guard let presenter = self.presenter else {
                observer(.error(RxError.unknown))
                return Disposables.create()
            }

            let alert = UIAlertController.actionAndCancelAlert(
                title: L10n.UserApp.Pay.Delete.Alert.title,
                message: L10n.UserApp.Pay.Delete.Alert.text,
                actionTitle: L10n.IOSApp.Navigation.Basic.delete,
                actionStyle: .destructive) {
                    observer(.completed)
                } cancelAction: {
                    observer(.error(SilentErrorDefaults.userDeclined))
                }
            presenter.present(alert, animated: true)
            return Disposables.create {
                alert.dismiss(animated: true)
            }
        }
        .subscribe(on: MainScheduler.instance)
    }
}
