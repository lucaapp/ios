import RxSwift
import DependencyInjection

class PaymentEnableInteractor: Interactor {
    @InjectStatic(\.lucaPayment) private var payment

    weak private var presenter: UIViewController?

    init(presenter: UIViewController) {
        self.presenter = presenter
    }

    func interact() -> Completable {
        guard let presenter = presenter else {
            return .error(RxError.unknown)
        }

        return PaymentConsentInteractor(presenter: presenter)
            .interact()
            .andThen(payment.activate())
    }
}
