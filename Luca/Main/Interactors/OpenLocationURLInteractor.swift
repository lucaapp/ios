import Foundation
import RxSwift
import DependencyInjection

class OpenLocationURLInteractor: Interactor {
    @InjectStatic(\.lucaPreferences) private var preferences

    private let location: Location
    private let locationURL: LocationURL
    private weak var presenter: UIViewController?

    init(presenter: UIViewController, location: Location, locationURL: LocationURL) {
        self.location = location
        self.locationURL = locationURL
        self.presenter = presenter
    }

    func interact() -> Completable {
        guard let presenter = presenter else {
            return .empty()
        }

        if locationURL.type == .report {
            return displayReportContentMailDialog(presenter: presenter)
                .observe(on: MainScheduler.asyncInstance)
        }

        let userConsent = Single<Bool>.deferred {
            let nc = ViewControllerFactory.Checkin.createExternalLinkConsentViewController(
                location: self.location,
                url: self.locationURL
            )
            if let vc = nc.viewControllers[0] as? ExternalLinkConsentViewController {
                presenter.present(nc, animated: true)
                return vc.result
            }
            return .just(false)
        }
        .subscribe(on: MainScheduler.asyncInstance)

        return preferences.get(\.allowOpenExternalLocationLinks)
            .flatMap { allow in
                if allow == true {
                    return .just(true)
                }
                return userConsent
            }
            .observe(on: MainScheduler.asyncInstance)
            .do(onSuccess: { (allow: Bool) in
                if allow {
                    OpenLinkCoordinator(url: self.locationURL.url).start()
                }
            })
            .asCompletable()
    }

    private func displayReportContentMailDialog(presenter: UIViewController) -> Completable {
        SendMailInteractor(
            presenter: presenter,
            recipients: L10n.IOSApp.ReportContent.email,
            subject: L10n.IOSApp.ReportContent.title,
            messageBody: L10n.IOSApp.ReportContent.Email.body(
                self.locationEmailString(location: location),
                self.locationLinksForMail(location: location)
            )
        )
        .interact()
    }

    private func locationEmailString(location: Location) -> String {
        if let groupName = location.groupName {
            return "\(location.locationName ?? "") (\(groupName))"
        } else {
            return location.locationName ?? ""
        }
    }

    private func locationLinksForMail(location: Location) -> String {
        guard let urls = location.urls, !urls.isEmpty else {return ""}
        return urls.map {$0.url}.joined(separator: "<br>")
    }
}
