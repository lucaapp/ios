import Foundation
import RxSwift
import DependencyInjection
import SwiftUI
import UIKit

class LocalCheckinInteractor: Interactor {

    @InjectStatic(\.localCheckinService) private var localCheckinService
    private let qr: String

    init(qrString: String) {
        self.qr = qrString
    }

    func interact() -> Completable {
        localCheckinService.checkin(with: qr).asCompletable()
    }
}
