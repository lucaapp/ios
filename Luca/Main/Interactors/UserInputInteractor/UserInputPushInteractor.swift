import UIKit
import RxSwift
import RxAppState

/// Interactor that handles ModalView-based interactions

/// Copied from UserInputModalInteractor
class UserInputPushInteractor {

    func showDialog(configuration: UserInputInteractorConfiguration, navigationController: UINavigationController) -> Completable {
        Single.from {
            UserInputModalInteractorViewController.fromStoryboard()
        }
        .flatMapCompletable { (vc: UserInputModalInteractorViewController) in

            let callbacks = Completable.create { observer in
                vc.setup(with: configuration) {
                    observer(.completed)
                } cancelCallback: {
                    if configuration.displayCancelOption {
                        observer(.error(configuration.error ?? SilentErrorDefaults.userDeclined))
                    } else {
                        observer(.completed)
                    }
                }
                return Disposables.create {
                    vc.actionCallback = nil
                    vc.cancelCallback = nil
                    vc.dismiss(animated: true, completion: nil)
                }
            }

            navigationController.pushViewController(vc, animated: true)
            return Observable.merge(
                callbacks.asObservable().materialize(),
                vc.rx.viewDidDisappear
                    .do(onNext: { _ in
                        if configuration.displayCancelOption {
                            throw configuration.error ?? SilentErrorDefaults.userDeclined
                        }
                    })
                    .take(1)
                    .ignoreElements()
                    .materialize()
            )
                .take(1)
                .dematerialize()
                .ignoreElementsAsCompletable()
        }
    }
}
