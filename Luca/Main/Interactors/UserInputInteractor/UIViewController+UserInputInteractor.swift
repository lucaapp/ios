import Foundation
import UIKit
import RxSwift
import DependencyInjection

extension UIViewController {

    /// extends view controller with a dialog-agnostic way to show user-consent messages, either through an alert or through a bottom-up modal sheet

    func showDialog(configuration: UserInputInteractorConfiguration) -> Completable {
        var completable = Completable.empty()
        switch configuration.type {
        case .alert:
            completable = UserInputAlertInteractor().showDialog(configuration: configuration, viewController: self)
        case .modal:
            completable = UserInputModalInteractor().showDialog(configuration: configuration, viewController: self)
        case .push:
            guard let navigationController = self.navigationController else {
                return .error(RxError.unknown)
            }
            completable = UserInputPushInteractor().showDialog(configuration: configuration, navigationController: navigationController)
        }
        return completable.subscribe(on: MainScheduler.instance)
    }
}

protocol UserInputInteractor {
    func showDialog(configuration: UserInputInteractorConfiguration, viewController: UIViewController) -> Completable
}
