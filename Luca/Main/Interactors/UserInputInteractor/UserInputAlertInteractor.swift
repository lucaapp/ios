import UIKit
import RxSwift

/// Interactor that handles Alert-based interactions

class UserInputAlertInteractor: UserInputInteractor {
    func showDialog(configuration: UserInputInteractorConfiguration, viewController: UIViewController) -> Completable {
        if configuration.displayCancelOption == false {
            return UIAlertController.infoAlertRx(viewController: viewController, title: configuration.title, message: configuration.message).ignoreElementsAsCompletable()
        }
        return Completable.create { observer -> Disposable in
            let alert = UIAlertController
                .actionAndCancelAlert(
                    title: configuration.title,
                    message: configuration.message,
                    actionTitle: configuration.actionTitle,
                    action: {
                        observer(.completed)
                    }, cancelAction: {
                        observer(.error(configuration.error ?? SilentErrorDefaults.userDeclined))
                    })

            viewController.present(alert, animated: true, completion: nil)

            return Disposables.create { alert.dismiss(animated: true, completion: nil) }
        }
    }
}
