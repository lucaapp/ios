import UIKit

struct UserInputInteractorConfiguration {
    enum InteractorType {
        case alert
        case modal(linkText: String?, url: URL?)
        case push
    }

    let title: String
    let message: String
    let type: InteractorType
    let actionTitle: String
    let cancelTitle: String?
    let error: Swift.Error?
    let displayCancelOption: Bool
    let accessibilityText: String?
    let accessibilityTitle: String?
    let image: UIImage?

    init(title: String,
         message: String,
         type: InteractorType,
         actionTitle: String = L10n.IOSApp.Navigation.Basic.ok,
         cancelTitle: String? = nil,
         error: Swift.Error? = nil,
         displayCancelOption: Bool = false,
         accessibilityText: String? = nil,
         accessibilityTitle: String? = nil, image: UIImage? = nil) {
        self.title = title
        self.message = message
        self.type = type
        self.actionTitle = actionTitle
        self.cancelTitle = cancelTitle
        self.error = error
        self.displayCancelOption = displayCancelOption
        self.accessibilityText = accessibilityText
        self.accessibilityTitle = accessibilityTitle
        self.image = image
    }
}
