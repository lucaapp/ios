import UIKit
import RxSwift
import RxAppState

/// Interactor that handles ModalView-based interactions

class UserInputModalInteractor: UserInputInteractor {

    func showDialog(configuration: UserInputInteractorConfiguration, viewController: UIViewController) -> Completable {
        Single.from {
            UserInputModalInteractorViewController.fromStoryboard()
        }
        .flatMapCompletable { (vc: UserInputModalInteractorViewController) in

            let callbacks = Completable.create { observer in
                vc.setup(with: configuration) {
                    observer(.completed)
                } cancelCallback: {
                    if configuration.displayCancelOption {
                        observer(.error(configuration.error ?? SilentErrorDefaults.userDeclined))
                    } else {
                        observer(.completed)
                    }
                }
                return Disposables.create {
                    vc.actionCallback = nil
                    vc.cancelCallback = nil
                    vc.dismiss(animated: true, completion: nil)
                }
            }

            let nav = UINavigationController(rootViewController: vc)
            viewController.present(nav, animated: true, completion: nil)
            return Observable.merge(
                callbacks.asObservable().materialize(),
                nav.rx.viewDidDisappear
                    .do(onNext: { _ in
                        if configuration.displayCancelOption {
                            throw configuration.error ?? SilentErrorDefaults.userDeclined
                        }
                    })
                    .take(1)
                    .ignoreElements()
                    .materialize()
            )
                .take(1)
                .dematerialize()
                .ignoreElementsAsCompletable()
        }
    }
}
