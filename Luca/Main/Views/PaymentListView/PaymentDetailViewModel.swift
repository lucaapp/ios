import Foundation
import DeviceKit
import MessageUI
import SwiftUI
import RxSwift
import DependencyInjection

class PaymentDetailViewModel: BaseViewModel {
    @InjectStatic(\.backendTableInfoV4) private var tableInfoService
    @InjectStatic(\.lucaPayment) private var paymentService
    @InjectStatic(\.campaignRepo) private var campaignRepo

    let payment: Payment

    @Published var table: String?
    @Published var showRaffleDescription: Bool = false

    init(payment: Payment) {
        self.payment = payment
        super.init()

        if let tableId = payment.table {
            _ = tableInfoService.fetchTableInfo(tableId: tableId)
                .asSingle()
                .observe(on: MainScheduler.instance)
                .do { [weak self] tableInfo in
                    self?.table = tableInfo.description
                }
                .subscribe()
        } else {
            table = nil
        }

        checkRaffleTipTopupCampaign()
        _ = self.objectWillChange.send()
    }

    private func checkRaffleTipTopupCampaign() {
        _ = paymentService.fetchTipTopUpCampaignAmountFormatted(forPayment: payment)
            .observe(on: MainScheduler.instance)
            .flatMapCompletable { [weak self] _ in
                guard let self = self else { return .empty() }
                // check if user approved raffle participation
                return self.campaignRepo.restore()
                    .map { submissions -> CampaignSubmissionData? in
                        submissions.filter { $0.submission.campaign == CampaignType.tipTopUpAndRaffle &&
                            $0.paymentId == self.payment.paymentId }.first
                    }
                    .observe(on: MainScheduler.instance)
                    .do(onSuccess: { self.showRaffleDescription = $0 != nil })
                    .asCompletable()
            }
            .subscribe()
    }

    var paymentTitle: String {
        return payment.locationName
    }

    var date: String {
        return payment.dateTimeParsed.formattedDate
    }

    var time: String {
        return payment.dateTimeParsed.formattedTime(.short)
    }

    var sum: String {
        return formatted(payment.amount)
    }

    var amount: String {
        return formatted(payment.invoiceAmount ?? 0.0)
    }

    var tip: String {
        return "(\(formattedTipPercentage)) \(formatted(payment.tipAmount ?? 0.0))"
    }

    var transactionId: String {
        payment.paymentVerifier ?? ""
    }

    private func formatted(_ amount: Double) -> String {
        let money = Money(double: amount, currency: EUR())
        return money.formattedAmountWithCode
    }

    private var formattedTipPercentage: String {
        guard let tipAmount = payment.tipAmount, (payment.invoiceAmount ?? 0.0) > 0 else { return "0 %" }

        let tipPercent = tipAmount / (payment.originalInvoiceAmount * 0.01)
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        return formatter.string(for: tipPercent / 100) ?? ""
    }

    // MARK: Support Button

    func presentSupportMail(from presenter: UIViewController) {
        _ = SendMailInteractor(
            presenter: presenter,
            recipients: supportMailRecipient,
            subject: supportMailSubject,
            messageBody: supportMailBody
        )
        .interact()
        .copyError(to: alertsSubject)
        .subscribe()
    }

    var supportMailRecipient: String {
        return L10n.IOSApp.Payment.Support.Email.recipient
    }

    var supportMailSubject: String {
        return L10n.UserApp.Pay.Support.Mail.subject
    }

    var supportMailBody: String {
        let appVersion = UIApplication.shared.applicationVersion ?? ""
        return L10n.IOSApp.Payment.Support.Email.body(
            Device.current.description,
            UIDevice.current.systemVersion,
            appVersion,
            payment.paymentVerifier ?? L10n.IOSApp.General.unknown,
            payment.dateTimeParsed.formattedDateTime,
            payment.locationName
        )
    }
}
