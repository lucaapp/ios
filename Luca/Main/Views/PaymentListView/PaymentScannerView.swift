import SwiftUI

struct PaymentScannerView: View, ContainsTorchButton {
    @Environment(\.scenePhase) var scenePhase
    @StateObject var viewModel: PaymentScannerViewModel = PaymentScannerViewModel()
    @State var showNextScreen = false
    @EnvironmentObject var paymentFlowVM: PaymentFlowViewModel
    @Binding var openPaymentFlow: Bool

    var body: some View {
        VStack(alignment: .center, spacing: 16) {
            LucaPaymentQRScanner(
                displayScanner: $viewModel.displayScanner,
                paymentLocationData: $paymentFlowVM.paymentLocationData,
                alertsSubject: paymentFlowVM.alertsSubject,
                expectedQRType: .checkin(supportsPayment: true)
            )
            .aspectRatio(contentMode: .fit)

            Text(L10n.IOSApp.Contact.Qr.scan).lucaFont(.luca14Medium)
                .frame(minWidth: 0, maxWidth: .infinity).padding(.horizontal, 52)
                .overlay(
                    VStack(alignment: .trailing) {
                        Button {
                            self.toggleTorch(sender: nil, forceOff: false)
                        } label: {
                            HStack {
                                Spacer()
                                Image(uiImage: Asset.light.image)
                            }
                        }.frame(width: 44, height: 44)
                    }, alignment: .trailing
                )
            Spacer()

        }
        .padding(.top, 40)
        .padding(.horizontal, 16)
        .navBarCloseButton($openPaymentFlow)
        .navigationBarTitleDisplayMode(.inline)
        .overlay(
            NavigationLink(isActive: $showNextScreen) {
                PaymentEnterAmountView(openPaymentFlow: $openPaymentFlow)
            } label: {
                EmptyView()
            }
        )
        .appearance(.dark)
        .onAppear(perform: {
            viewModel.viewVisible = true
        })
        .onDisappear(perform: {
            viewModel.viewVisible = false
        })
        .onChange(of: paymentFlowVM.paymentLocationData, perform: { newValue in
            // navigate to next screen when qr code has been scanned
            showNextScreen = newValue != nil
        })
        .onChange(of: showNextScreen) { newValue in
            // restart scanner when coming back to this screen
            viewModel.viewVisible = !newValue
            if !newValue {
                paymentFlowVM.paymentLocationData = nil
            }
        }

    }
}

struct PaymentScannerView_Previews: PreviewProvider {
    static var previews: some View {
        PaymentScannerView(openPaymentFlow: .constant(false)).environmentObject(PaymentFlowViewModel(paymentLocationData: nil))
    }
}
