import SwiftUI
import LucaUIComponents

struct PaymentDetailView: View {

    @StateObject var viewModel: PaymentDetailViewModel
    @Environment(\.presenter) var presenter

    var body: some View {
        scrollView
            .handleAlerts($viewModel.alerts)
            .navigationBarTitle(L10n.UserApp.Pay.Details.title, displayMode: .inline)
            .padding(.horizontal, 16)
            .padding(.vertical, 24)
            .appearance(.dark)
    }

    private var scrollView: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 16) {
                paymentTitle
                list(viewModel.date, for: L10n.UserApp.Pay.Details.date)
                list(viewModel.time, for: L10n.UserApp.Pay.Details.time)
                list(viewModel.table ?? "", for: L10n.UserApp.Pay.Details.table)
                    .show(isVisible: $viewModel.table.isPresent, remove: false)
                Spacer().frame(height: 8)
                Group {
                    listSum()
                    amountSection
                    list(viewModel.tip, for: L10n.UserApp.Pay.Details.Tip.amount)
                }
                list(viewModel.transactionId, for: L10n.UserApp.Pay.Details.Payment.code)
                Spacer().frame(height: 16)
                if viewModel.showRaffleDescription {
                    raffleDescription
                }
            }
            Spacer().frame(height: 40)
            supportButton
            Spacer()
        }
    }

    private var paymentTitle: some View {
        Text(viewModel.paymentTitle)
            .lucaFont(.luca16Bold)
    }

    private func list(_ value: String, for caption: String) -> some View {
        HStack {
            Text(caption).lucaFont(.luca14Medium)
            Spacer()
            Text(value).lucaFont(.luca14Bold)
        }
    }

    private func listSum() -> some View {
        HStack {
            Text(L10n.UserApp.Pay.Details.amount)
                .lucaFont(.luca14Medium)
            Spacer()
            Text(viewModel.sum)
                .lucaFont(.luca24Regular)
        }
    }

    @ViewBuilder
    private var amountSection: some View {
        if let discount = viewModel.payment.lucaDiscount {
            LucaDiscountCampaignAmountView(
                labelText: L10n.UserApp.Pay.Details.Invoice.amount,
                amount: discount.originalInvoiceAmount.money(with: EUR()),
                discountedAmount: (discount.originalInvoiceAmount - discount.discountAmount).money(with: EUR())
            )
        } else {
            list(viewModel.payment.originalInvoiceAmount.money(with: EUR()).formattedAmountWithCode, for: L10n.UserApp.Pay.Details.Invoice.amount)
        }
    }

    private var supportButton: some View {
        Button(L10n.UserApp.Pay.Details.Report.problem.uppercased()) {
            guard let presenter = presenter.value else { return }
            viewModel.presentSupportMail(from: presenter)
        }
        .buttonStyle(LucaButtonWithWhiteBorder())
    }

    private var raffleDescription: some View {
        HStack(alignment: .top, spacing: 16) {
            Image(uiImage: Asset.cupYellow.image)
                .padding(.top, 4)
            Text(L10n.UserApp.Pay.Details.Raffle.hint)
                .lucaFont(.luca14Medium)
        }
    }
}

struct PaymentDetailView_Previews: PreviewProvider {

    static var previews: some View {
        let payment = Payment(locationName: "Restaurant Name", locationId: "1", amount: 1499, invoiceAmount: 123, tipAmount: 100, dateTime: 42637282, status: .closed)
        let viewModel = PaymentDetailViewModel(payment: payment)
        UIElementPreview(
            NavigationView {
                PaymentDetailView(viewModel: viewModel)
            }
        )
    }
}
