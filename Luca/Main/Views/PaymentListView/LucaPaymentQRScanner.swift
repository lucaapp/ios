import Foundation
import SwiftUI
import AVFoundation
import RxSwift
import DependencyInjection
import Combine

struct LucaPaymentQRScanner: UIViewRepresentable {
    @Binding var displayScanner: Bool
    @Binding var paymentLocationData: PaymentLocationData?
    var alertsSubject: CurrentValueSubject<AlertContent?, Never>
    var expectedQRType: QRType
    @Environment(\.presenter) var presenter

    func makeUIView(context: Context) -> LucaQRScanner {
        let v = LucaQRScanner()
        context.coordinator.qrScannerView = v
        return v
    }

    func updateUIView(_ qrScanner: LucaQRScanner, context: Context) {
        if context.coordinator.displayScanner != displayScanner {
            context.coordinator.displayScanner = displayScanner
        }
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self, expectedQRType: expectedQRType, paymentLocationData: $paymentLocationData, alertsSubject: self.alertsSubject)
    }

    final class Coordinator: NSObject {
        @InjectStatic(\.qrProcessingService) private var qrProcessingService
        @InjectStatic(\.traceIdService) private var traceIdService
        @InjectStatic(\.localCheckinService) private var localCheckinService

        @Binding var paymentLocationData: PaymentLocationData?
        var alertsSubject: AlertsSubject

        var expectedQRType: QRType
        var parent: LucaPaymentQRScanner
        var disposeBag: DisposeBag?
        var qrScannerView: LucaQRScanner? {
            didSet {
                bindActivationButton()
            }
        }
        var displayScanner = false {
            didSet {
                if displayScanner == false {
                    endScanner()
                } else {
                    startScanner()
                }
            }
        }

        init(_ scanner: LucaPaymentQRScanner, expectedQRType: QRType, paymentLocationData: Binding<PaymentLocationData?>, alertsSubject: AlertsSubject) {
            self.parent = scanner
            self.expectedQRType = expectedQRType
            self._paymentLocationData = paymentLocationData
            self.alertsSubject = alertsSubject
            super.init()

            if parent.displayScanner == false {
                self.endScanner()
            } else {
                if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
                    DispatchQueue.main.async { [weak self] in
                        self?.startScanner()
                    }
                }
            }
        }

        func bindActivationButton() {
            _ = qrScannerView?.onActivateButton
                .take(until: rx.deallocating)
                .subscribe { [weak self] _ in
                    self?.startScanner()
                }
        }

        private func endScanner() {
            // Delay this call to prevent hiccup on disposal
            DispatchQueue.main.asyncAfter(deadline: .now() + DispatchTimeInterval.milliseconds(1)) { [weak self] in
                self?.disposeBag = nil
            }
        }

        private func startScanner() {
            guard let lucaQRScannerView = qrScannerView else {return}
            let scanningDisposeBag = DisposeBag()
            lucaQRScannerView.scan
                // Delay the subscription to force the appearance to continue without waiting for the session
                .delaySubscription(.milliseconds(1), scheduler: MainScheduler.asyncInstance)
                .take(1)
                .flatMap { [weak self] (qr: String) -> Single<LocalCheckin> in
                    guard let self = self else {throw RxError.unknown}
                    return self.processQRCode(qr: qr)
                        .do { paymentLocationData in
                            self.paymentLocationData = paymentLocationData
                        }
                }
                .catch { [weak self] (error) -> Observable in
                    if self?.isAuthorizationError(error) == true {
                        return self?.goToApplicationSettingsObservable() ?? .error(error)
                    } else {
                        return .error(error)
                    }
                }
                .copyError(to: alertsSubject)
                .retry(delay: RxTimeInterval.seconds(1), scheduler: MainScheduler.asyncInstance)
                .subscribe()
                .disposed(by: scanningDisposeBag)
            disposeBag = scanningDisposeBag
        }

        private func isAuthorizationError(_ error: Error) -> Bool {
            if let error = error as? MetadataScannerServiceLocalizedError {
                return error == .captureDeviceNotAuthorized
            }
            return false
        }

        private func goToApplicationSettingsObservable() -> Observable<LocalCheckin> {
            return Completable.from {
                DispatchQueue.main.async {
                    self.alertsSubject.value = self.missingAuthenticationAlertContent()
                }
            }
            .andThen(Observable.empty())
        }

        private func missingAuthenticationAlertContent() -> AlertContent {
            let cancelButton = Alert.Button.cancel()
            let settingsButton = Alert.Button.default(Text(L10n.IOSApp.Navigation.Basic.ok)) {
                UIApplication.shared.openApplicationSettings()
            }
            let alertContent = AlertContent(title: L10n.IOSApp.Camera.Access.title,
                                            message: L10n.IOSApp.Camera.Access.description,
                                            type: .yesNo(primaryButton: cancelButton, secondaryButton: settingsButton))
            return alertContent
        }

        private func processQRCode(qr: String) -> Single<LocalCheckin> {
            guard let presenter = parent.presenter.value else {return Single.error(RxError.unknown)}
            return self.qrProcessingService.processQRCode(
                qr: qr,
                processingStrategy: .paymentScanner(presenter),
                presenter: presenter
            )
            .flatMap { _ in self.localCheckinService.generateLocalCheckinPayload(with: qr) }
            .debug("payment => processQRCode")
            .observe(on: MainScheduler.asyncInstance)
        }
    }
}
