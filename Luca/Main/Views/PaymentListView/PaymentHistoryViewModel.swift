import Foundation
import SwiftUI
import DeviceKit
import DependencyInjection
import RxSwift

class PaymentHistoryViewModel: BaseViewModel {
    private var generalSupportMailBody: String {
         let appVersion = UIApplication.shared.applicationVersion ?? ""
        return L10n.IOSApp.Payment.Support.General.Email.body(
             Device.current.description,
             UIDevice.current.systemVersion,
             appVersion
         )
     }

    func presentSupportMail(from presenter: UIViewController?) {
        guard let presenter = presenter else {return}
        _ = SendMailInteractor(
            presenter: presenter,
            recipients: L10n.IOSApp.Payment.Support.Email.recipient,
            subject: L10n.UserApp.Pay.Support.Mail.subject,
            messageBody: generalSupportMailBody
        )
        .interact()
        .copyError(to: alertsSubject)
        .asObservable()
        .take(1)
        .subscribe()
    }
}

extension Payment {
    var amountAsString: String {
        if refunded != nil && refunded! {return L10n.UserApp.Pay.refunded}

        switch status {
        case .open:
            return L10n.IOSApp.General.unknown
        case .started:
            return L10n.IOSApp.General.unknown
        case .closed:
            return amount.money(with: EUR()).formattedAmountWithCode
        case .error:
            return L10n.IOSApp.Navigation.Basic.error
        case .unknown:
            return L10n.IOSApp.General.unknown
        }
    }
}
