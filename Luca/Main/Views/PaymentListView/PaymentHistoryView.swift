import SwiftUI
import Combine

struct PaymentHistoryView: View {
    @StateObject var viewModel = PaymentHistoryViewModel()
    @Environment(\.presenter) var presenter
    @Binding var openScanner: Bool
    var hasMorePaymentsAvailable: Bool
    @Binding var hasMorePaymentsLoadingFailed: Bool
    @Binding var payments: [Payment]
    var reloadTrigger: PassthroughSubject<Void, Never>

    var body: some View {
        VStack {
            VStack(spacing: 16) {
                ScrollView {
                    LazyVStack(alignment: .leading, spacing: 24) {
                        ForEach(payments, id: \.paymentId) { payment in
                            NavigationLink(destination: EmptyView()) {
                                PaymentHistoryViewCell(payment: payment)
                            }
                        }
                        loadingCell
                    }
                }
                Spacer()
                Button {
                    openScanner.toggle()
                } label: {
                    Text(L10n.UserApp.Pay.Open.scanner.uppercased())
                }.buttonStyle(LucaButton())
            }
            .padding(16)
        }
        .toolbar {
            topBar
        }
        .appearance(.dark)
    }

    @ToolbarContentBuilder
    private var topBar: some ToolbarContent {
        ToolbarItem(placement: .navigationBarTrailing) {
            Button {
                viewModel.presentSupportMail(from: presenter.value)
            } label: {
                Image(uiImage: Asset.questionmarkBlue.image)
            }
        }
    }

    private var loadingCell: some View {
        HStack(alignment: .center, spacing: 16) {
            Spacer()
            if !hasMorePaymentsLoadingFailed {
                ProgressView()
                    .onAppear {
                        reloadTrigger.send()
                    }
                    .show(isVisible: hasMorePaymentsAvailable, remove: true)
            } else {
                Text(L10n.UserApp.Pay.Retry.button)
                    .lucaFont(.luca14Medium)
                    .foregroundColor(Color(Asset.lucaError.color))
                Button {
                    hasMorePaymentsLoadingFailed = false
                } label: {
                    Image(uiImage: Asset.reloadBlue.image)
                }
                .onDisappear {
                    hasMorePaymentsLoadingFailed = false
                }
            }
            Spacer()
        }
    }
}

struct PaymentHistoryViewCell: View {
    @Environment(\.presenter) var presenter
    var payment: Payment

    var body: some View {
        NavigationLink {
            LazyView(PaymentDetailView(viewModel: PaymentDetailViewModel(payment: payment))
                .environment(\.presenter, presenter))
        } label: {
            Label {
                VStack(alignment: .leading, spacing: 8) {
                    HStack {
                        Text(payment.locationName).lucaFont(.luca16Bold)
                        Spacer(minLength: 0)
                        Image(uiImage: Asset.disclosureIndicator.image)
                    }
                    HStack {
                        Text("\(payment.dateTimeParsed.formattedDateTime)").lucaFont(.luca16Medium)
                        Spacer()
                        Text(payment.amountAsString)
                            .lucaFont(.luca16Bold)
                            .if(payment.refunded == true) {
                                $0.foregroundColor(Color(Asset.lucaPaymentWarning.color))
                            }
                    }
                }
            } icon: {
                Circle().frame(width: 8, height: 8).padding(.trailing, 20)
            }
        }
    }
}

struct PaymentHistoryView_Previews: PreviewProvider {
    static var previews: some View {
        UIElementPreview(
            PaymentHistoryView.init(
                openScanner: .constant(false),
                hasMorePaymentsAvailable: true,
                hasMorePaymentsLoadingFailed: .constant(false),
                payments: .constant([
                    Payment(paymentId: "1", locationName: "Restaurant Fillipus", locationId: "1", amount: 1, invoiceAmount: 123.0, tipAmount: 1, dateTime: 123, status: .closed),
                    Payment(paymentId: "2", locationName: "Restaurant Fillipus", locationId: "2", amount: 0.123, invoiceAmount: 123.0, tipAmount: 1, dateTime: 123, status: .error),
                    Payment(paymentId: "3", locationName: "Restaurant Fillipus", locationId: "3", amount: 0.012, invoiceAmount: 123.0, tipAmount: 1, dateTime: 123, status: .closed),
                    Payment(paymentId: "4", locationName: "Restaurant Fillipus", locationId: "4", amount: 123, invoiceAmount: 123.0, tipAmount: 1, dateTime: 123, status: .unknown),
                    Payment(paymentId: "5", locationName: "Restaurant Fillipus", locationId: "5", amount: 123, invoiceAmount: 123.0, tipAmount: 1, dateTime: 123, status: .unknown, refunded: true),
                    Payment(paymentId: "6", locationName: "Restaurant Fillipus very long name very long name very long name very long name very long name", locationId: "6", amount: 123, invoiceAmount: 123.0, tipAmount: 1, dateTime: 123, status: .unknown)
                ]),
                reloadTrigger: PassthroughSubject<Void, Never>()
            )
            .environment(\.presenter, Weak(UIViewController()))
        )
    }
}
