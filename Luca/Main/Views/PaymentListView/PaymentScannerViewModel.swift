import SwiftUI
import Combine
import AVFoundation
import RxSwift

class PaymentScannerViewModel: BaseViewModel {
    @Published var appActive: Bool = false
    @Published var viewVisible: Bool = false
    private var bag = Set<AnyCancellable>()
    private var disposeBag = DisposeBag()
    @Published var displayScanner: Bool = false

    override init() {
        super.init()
        self.showScanner.sink(receiveCompletion: { _ in
            print("completion")
        }, receiveValue: { [weak self] showScanner in
            self?.displayScanner = showScanner
        })
        .store(in: &bag)

        UIApplication.shared.rx.currentAndChangedAppState
            .subscribe(on: MainScheduler.asyncInstance)
            .subscribe { [weak self] appState in
                self?.appActive = appState == .active
            }.disposed(by: disposeBag)

    }

    var showScanner: AnyPublisher<Bool, Never> {
        Publishers.CombineLatest($appActive, $viewVisible).map { (appPhase, viewVisible) -> Bool in
            return appPhase && viewVisible
        }
        .removeDuplicates()
        .eraseToAnyPublisher()
    }
}
