import UIKit

class AppointmentView: DocumentView {

    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var labLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var qrCodeImageView: UIImageView!

    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var expandView: UIView!

    @IBOutlet weak var dateStackView: UIStackView!
    @IBOutlet weak var addressStackView: UIStackView!

    override var viewsToCheck: [ViewWidthDefining] {
        isExpanded ? [dateLabel, typeLabel, labLabel, addressLabel] : [dateLabel]
    }

    override var stackViews: [UIStackView] {
        isExpanded ? [dateStackView, addressStackView] : [dateStackView]
    }

    override internal func setup() {
        guard let appointment = document as? Appointment else { return }

        wrapperView.layer.cornerRadius = 8
        wrapperView.backgroundColor = Asset.lucaDarkBlue.color

        typeLabel.text = "\(L10n.IOSApp.Appointment.Result.prefix): \(appointment.type)"
        dateLabel.text = appointment.issuedAt.formattedDateTime
        dateLabel.accessibilityLabel = appointment.issuedAt.accessibilityDate
        labLabel.text = appointment.lab.replacingOccurrences(of: "\\s[\\s]+", with: "\n", options: .regularExpression, range: nil)
        addressLabel.text = appointment.address

        qrCodeImageView.layer.cornerRadius = 8
        setupQRCodeImage(for: appointment)

        qrCodeImageView.isAccessibilityElement = true
        qrCodeImageView.accessibilityLabel = L10n.IOSApp.Contact.Qr.Accessibility.qrCode

        deleteButton.addTarget(self, action: #selector(didPressDelete(sender:)), for: .touchUpInside)
        deleteButton.layer.borderWidth = 1
        deleteButton.layer.borderColor = UIColor.black.cgColor
        deleteButton.layer.cornerRadius = 16
        deleteButton.accessibilityLabel = L10n.IOSApp.Test.Delete.title

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.isEnabled = true
        tapGestureRecognizer.cancelsTouchesInView = true
        addGestureRecognizer(tapGestureRecognizer)

        expandView.isHidden = !self.isExpanded
        position = .single

        initObserver()
        updateStackViews()
    }

    private func setupQRCodeImage(for appointment: Appointment) {
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let image = QRCodeGenerator.generateQRCode(string: appointment.qrCode)
        if let scaledQr = image?.transformed(by: transform) {
            qrCodeImageView.image = UIImage(ciImage: scaledQr)
        }
    }
}

// MARK: - DocumentViewProtocol

extension AppointmentView: DocumentViewProtocol {

    public static func createView(document: Document, owner: Person, isExpanded: Bool, delegate: DocumentViewDelegate?) -> DocumentView? {
        guard let document = document as? Appointment else { return nil }

        let itemView: AppointmentView = AppointmentView.fromNib()
        itemView.delegate = delegate
        itemView.isExpanded = isExpanded
        itemView.document = document
		itemView.isAccessibilityElement = !isExpanded
        itemView.accessibilityTraits = .button
        itemView.accessibilityLabel = L10n.IOSApp.Test.Show.title
        itemView.owner = owner

        return itemView
    }
}

// MARK: - Actions

extension AppointmentView {
    @objc
    private func viewTapped(_ sender: UITapGestureRecognizer) {
        guard let document = document else { return }
        delegate?.didSelect(self, document: document)
    }

    @objc
    private func didPressDelete(sender: UIButton) {
        if let appointment = self.document {
            delegate?.didTapDelete(self, for: appointment)
        }
    }
}
