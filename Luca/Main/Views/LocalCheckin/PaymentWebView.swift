import SwiftUI

struct PaymentWebView: View {
    @StateObject private var viewModel = PaymentWebViewModel()
    @Binding var openPaymentFlow: Bool
    @Binding var openTipView: Bool

    var paymentCheckout: PaymentCreateCheckoutResponse

    #if !PRODUCTION
    private let credential = URLCredential.init(user: secrets.backendLogin, password: secrets.backendPassword, persistence: .forSession)
    #else
    private let credential: URLCredential? = nil
    #endif

    var body: some View {
        VStack {
            WebView(request: URLRequest(url: URL(string: paymentCheckout.checkoutUrl)!), alerts: $viewModel.alerts, credential: credential, onEnterURL: {
                if $0.revealsPaymentCompletion {
                    viewModel.fetchPaymentStatus(paymentCheckout: paymentCheckout)
                    return .cancel
                }
                return .allow
            })
        }
        .handleAlerts($viewModel.alerts)
        .loadingIndicator(isLoading: $viewModel.isProcessing)
        .navBarCloseButton($openPaymentFlow)
        .overlay(
            NavigationLink(
                destination: LazyView {
                    if let payment = viewModel.fetchedPayment {
                        LucaPaymentCompletionView(openPaymentFlow: $openPaymentFlow, openTipView: $openTipView, payment: payment)
                    }
                },
                isActive: $viewModel.showCompletionView,
                label: {}
            )
        )
        .onAppear {
            UIApplication.shared.setStatusBarStyle(.lightContent, animated: true)
        }
    }
}

private extension URL {

    var revealsPaymentCompletion: Bool {
        absoluteString.contains("complete_checkout") ||
        absoluteString.contains("error_payment") ||
        absoluteString.contains("cancel_checkout") ||
        absoluteString.contains("complete_payment") // This case is when the amount is so high so the 3D Security is triggered. This is due to incosistency in backend but has to be handled nevertheless.
    }
}
