import SwiftUI
import DependencyInjection
import RxSwift

struct LocalCheckInView: View {
    @StateObject private var viewModel = LocalCheckInViewModel()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    @State private var sheetPresented = false
    @State private var showPaymentWebView = false
    @State private var openScanner = false

    private let onLinkClick: (LocationURL) -> Void
    private let padding = 16.0

    weak var presenter: UIViewController?
    private var localCheckin: LocalCheckin

    init(localCheckin: LocalCheckin, onLinkClick: @escaping (LocationURL) -> Void) {
        self.localCheckin = localCheckin
        self.onLinkClick = onLinkClick
    }

    var body: some View {
        VStack(spacing: 0) {
            separator
            ScrollView(showsIndicators: false) {
                VStack(spacing: 24) {
                    welcomeArea
                    separator
                    traceInfoButton
                        .show(isVisible: $viewModel.isDailyKeyAvailable, remove: true)
                    LucaDiscountCampaignInfoView(discountCampaign: $viewModel.discountCampaign, style: .greenTint)
                    locationLinks
                        .show(isVisible: !viewModel.locationURLs.isEmpty, remove: true)
                    emptyScreen
                }
            }
            .padding(.horizontal, padding)
            Spacer()
            paymentButton
                .show(isVisible: viewModel.showPaymentButton == true)
        }
        .navigationBarTitle(L10n.IOSApp.Checkin.noun, displayMode: .inline)
        .navigationBarBackButtonHidden(true)
        .toolbar {
            topBar
        }
        .appearance(.dark)
        .handleAlerts($viewModel.alerts)
        .loadingIndicator(isLoading: $viewModel.isProcessing)
        .fullScreenCover(isPresented: $showPaymentWebView, content: {
            LazyView {
                PaymentFlowRootView(viewModel: PaymentFlowViewModel(paymentLocationData: viewModel.localCheckin), openPaymentFlow: $showPaymentWebView)
            }
        })
        .background(
            Group {
                NavigationLink(
                    isActive: $viewModel.showTraceInfo,
                    destination: { LazyView(checkInNavigationDestination) },
                    label: {}
                )

                /// fixes iOS 14 bug that would cause views to immediatly pop after push if the total of navigationLinks equals 2; thus we need to add a few more invisible ones
                /// https://forums.swift.org/t/14-5-beta3-navigationlink-unexpected-pop/45279/42
                NavigationLink(destination: EmptyView(), label: {})
                NavigationLink(destination: EmptyView(), label: {})

            }
        )
        .onAppear {
            viewModel.presenter = presenter
            viewModel.localCheckin = localCheckin

            viewModel.checkDailyKey()
            viewModel.checkPayment()
            viewModel.checkContactTraceInfoStatus()
        }
        .onChange(of: showPaymentWebView) { newValue in
            // This ugly solution has to be done this way instead of onAppear, because it wont be called after payment dismissal
            if !newValue {
                UIApplication.shared.setStatusBarStyle(.darkContent, animated: true)
            }
        }
    }

    @ViewBuilder
    private var separator: some View {
        Divider()
            .foregroundColor(Color(Asset.luca747480.color))
    }

    @ViewBuilder
    private var emptyScreen: some View {
        Image(uiImage: Asset.localCheckinEmptyImage.image)
            .show(isVisible: viewModel.locationURLs.isEmpty, remove: true)
            .padding(.top, 48)
    }

    @ViewBuilder
    private var welcomeArea: some View {
        VStack(spacing: 8) {
            Text(viewModel.welcomeAreaTitle)
                .lucaFont(.luca24Bold)
                .frame(maxWidth: .infinity, alignment: .topLeading)
            if let subtitle = viewModel.welcomeAreaSubtitle {
                Text(subtitle)
                    .lucaFont(.luca14Medium)
                    .frame(maxWidth: .infinity, alignment: .topLeading)
            }
            if let table = viewModel.table {
                HStack {
                    Text(L10n.IOSApp.LocationCheckinViewController.AdditionalData.table("")).lucaFont(.luca16Medium)
                    Spacer()
                    Text(table).lucaFont(.luca16Bold)
                }
            }
        }
        .padding(.top, 24)
    }

    @ViewBuilder
    private var traceInfoButton: some View {
        Button(viewModel.traceInfoButtonLabel) { viewModel.createOrGetTraceInfo() }
            .frame(maxWidth: .infinity)
            .buttonStyle(LucaButtonWithWhiteBorder())
    }

    @ViewBuilder
    private var paymentButton: some View {
        Button(L10n.IOSApp.Checkin.Local.Payment.button.uppercased()) {
            viewModel.consentOnPayment {
                showPaymentWebView.toggle()
            }
        }
        .buttonStyle(LucaButton())
        .padding(.horizontal, padding)
        .padding(.bottom, 32)
    }

    @ViewBuilder
    private var locationLinks: some View {
        VStack(alignment: .leading, spacing: padding) {
            ForEach(viewModel.locationURLs, id: \.urlId) { locationURL in
                Button(locationURL.type.localized) { onLinkClick(locationURL) }
                    .buttonStyle(LucaIconButton(icon: locationURL.type.icon))
            }
            Text(L10n.IOSApp.Checkin.Local.report)
                .font(SwiftUI.Font(FontFamily.Montserrat.medium.font(size: 12.0)))
                .underline()
                .foregroundColor(.white)
                .onTapGesture {
                    onLinkClick(LocationURL(urlId: UUID().uuidString, type: .report, url: ""))
                }
        }
    }

    @ToolbarContentBuilder
    private var topBar: some ToolbarContent {
        ToolbarItem(placement: .navigationBarTrailing) {
            Button(action: {
                if viewModel.currentTraceInfo != nil {
                    viewModel.alerts = AlertContent(
                        title: L10n.IOSApp.Checkout.noun,
                        message: L10n.UserApp.Confirm.End.Contact.tracing,
                        type: .yesNo(
                            primaryButton: .default(Text(L10n.IOSApp.Navigation.Basic.yes), action: {
                                viewModel.checkout(completion: { presentationMode.wrappedValue.dismiss() })
                            }),
                            secondaryButton: .cancel(Text(L10n.IOSApp.Navigation.Basic.no))
                        )
                    )
                } else {
                    viewModel.checkout(completion: { presentationMode.wrappedValue.dismiss() })
                }
            }) {
                Image(uiImage: Asset.checkinExit.image).frame(width: 16.0)
            }
        }
    }

    @ViewBuilder
    private var checkInNavigationDestination: some View {
        if let traceInfo = viewModel.currentTraceInfo {
            MainCheckinView(traceInfo: traceInfo)
        } else {
            EmptyView()
        }
    }
}

struct MainCheckinView: UIViewControllerRepresentable {

    let traceInfo: TraceInfo

    func makeUIViewController(context: Context) -> ActiveCheckinViewController {
        let vc = ViewControllerFactory.Checkin.createLocationCheckinViewController(traceInfo: traceInfo)
        return vc
    }

    func updateUIViewController(_ uiViewController: ActiveCheckinViewController, context: Context) {
    }
}

struct LocalCheckinView_Previews: PreviewProvider {
    static var previews: some View {
        UIElementPreview(
            NavigationView {
                // swiftlint:disable:next line_length
                LocalCheckInView(localCheckin: LocalCheckin(qrCode: "", location: Location(locationId: "", publicKey: "", radius: 0), selfCheckin: TableSelfCheckin(urlToParse: URL(string: "https://app-p1.luca-app.de/webapp/b638b6df-e84e-482a-8dbe-d94648e8e66b#eyJ0YWJsZSI6MX0/CWA1/CAESKQgBEghBQ01FIEluYxobU29tZSBTdHJlZXQgMiwgMTAyMDAgQmVybGluGnYIARJggwLMzE153tQwAOf2MZoUXXfzWTdlSpfS99iZffmcmxOG9njSK4RTimFOFwDh6t0Tyw8XR01ugDYjtuKwjjuK49Oh83FWct6XpefPi9Skjxvvz53i9gaMmUEc96pbtoaAGhCZHI628tkgNVeAuH69O13VIgYIARAEGHg")!)!)) { _ in

                }
            }
        )
    }
}
