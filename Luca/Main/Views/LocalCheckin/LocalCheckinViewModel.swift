import SwiftUI
import DependencyInjection
import RxSwift

class LocalCheckInViewModel: BaseViewModel {
    var localCheckin: LocalCheckin! = nil {
        didSet {
            setWelcomeAreaTitles()
            fetchTableInfo()
            checkDiscountCampaign()
            locationURLs = (localCheckin.location.urls ?? []).filter { $0.type != .report }
        }
    }
    weak var presenter: UIViewController?

    @InjectStatic(\.localCheckinService) private var localCheckinService
    @InjectStatic(\.traceIdService) private var traceIdService
    @InjectStatic(\.dailyPublicKeyVerifier) private var dailyKey
    @InjectStatic(\.backendPayment) private var backend
    @InjectStatic(\.backendAddressV4) private var backendAddress
    @InjectStatic(\.lucaPayment) private var payment
    @InjectStatic(\.backendTableInfoV4) private var tableInfoService
    @InjectStatic(\.campaignService) private var campaignService
    @InjectStatic(\.timeProvider) private var timeProvider

    @Published var isDailyKeyAvailable: Bool = false

    @Published var showPaymentButton: Bool?
    @Published var paymentLink: URL?
    @Published var paymentCompletedPartialLink: String?
    @Published var discountCampaign: PaymentCampaign?

    @Published var welcomeAreaTitle: String = ""
    @Published var welcomeAreaSubtitle: String?
    @Published var table: String?
    @Published var locationURLs: [LocationURL] = []

    @Published var showTraceInfo: Bool = false
    @Published var currentTraceInfo: TraceInfo?
    @Published var traceInfoButtonLabel: String = L10n.IOSApp.Checkin.Local.ContactTracingButton.activate.uppercased()

    var checkInStatusDisposeBag: DisposeBag?

    func checkout(completion: @escaping () -> Void) {
        _ = LocalCheckoutInteractor().interact()
            .observe(on: MainScheduler.asyncInstance)
            .copyError(to: alertsSubject)
            .copyRunningState(to: processCountBinding)
            .do(onCompleted: { completion() })
            .subscribe()
    }

    func checkDailyKey() {
        _ = dailyKey
            .isKeyValid
            .asObservable()
            .do(onNext: { [weak self] in self?.isDailyKeyAvailable = $0 })
            .subscribe()
    }

    func checkContactTraceInfoStatus() {
        if checkInStatusDisposeBag == nil {
            let db = DisposeBag()

            traceIdService.isCurrentlyCheckedInChanges
                .flatMap { _ in self.traceIdService.currentTraceInfo.map { $0 as TraceInfo? }.ifEmpty(default: nil) }
                .observe(on: MainScheduler.asyncInstance)
                .do(onNext: { [weak self] traceInfo in
                    if traceInfo == nil { self?.showTraceInfo = false }
                    self?.currentTraceInfo = traceInfo
                    self?.traceInfoButtonLabel = traceInfo != nil ? L10n.IOSApp.Checkin.Local.ContactTracingButton.show.uppercased() : L10n.IOSApp.Checkin.Local.ContactTracingButton.activate.uppercased()
                })
                .subscribe()
                .disposed(by: db)

            checkInStatusDisposeBag = db
        }
    }

    func createOrGetTraceInfo() {
        _ = traceIdService.currentTraceInfo
            .asObservable()
            .ifEmpty(switchTo: checkIn().asObservable())
            .take(1)
            .observe(on: MainScheduler.asyncInstance)
            .do(onNext: {
                self.traceInfoButtonLabel = L10n.IOSApp.Checkin.Local.ContactTracingButton.show.uppercased()
                self.currentTraceInfo = $0
                self.showTraceInfo = true
            })
            .subscribe()
    }

    func checkPayment() {
        _ = backend.checkPaymentActive(locationId: localCheckin.location.locationId)
            .asSingle()
            .observe(on: MainScheduler.asyncInstance)
            .do(onSuccess: {
                self.showPaymentButton = $0.paymentActive

                var urlComponents = URLComponents(string: self.backendAddress.host.absoluteString)
                urlComponents?.path = "/pay/\(self.localCheckin.location.locationId)"
                urlComponents?.queryItems = [
                    URLQueryItem(name: "referer", value: "luca/ios")
                ]
                if let tableCheckin = self.localCheckin.selfCheckin as? TableSelfCheckin,
                   let tableId = tableCheckin.additionalData?.tableIdentifier {
                    urlComponents?.queryItems?.append(URLQueryItem(name: "table", value: tableId))
                }

                self.paymentLink = urlComponents?.url

                self.paymentCompletedPartialLink = self.backendAddress
                    .host
                    .appendingPathComponent("webapp")
                    .absoluteString
                    .remove(prefix: self.backendAddress.host.scheme ?? "")
                    .remove(prefix: "://")
                    .remove(prefix: "www.")
            })
            .subscribe()
    }

    private func fetchTableInfo() {
        guard let tableCheckin = localCheckin.selfCheckin as? TableSelfCheckin,
              let tableId = tableCheckin.additionalData?.tableIdentifier else { return }

        _ = tableInfoService.fetchTableInfo(tableId: tableId)
            .asSingle()
            .observe(on: MainScheduler.instance)
            .do { [weak self] tableInfo in
                print("table: \(tableInfo.description)")
                self?.table = tableInfo.description
            }
            .subscribe()
    }

    func consentOnPayment(_ callback: @escaping () -> Void) {
        guard let presenter = presenter else {
            return
        }
        _ = payment.isUserActivated
            .flatMapCompletable { userActivated in
                if !userActivated {
                    return PaymentEnableInteractor(presenter: presenter).interact()
                }
                return .empty()
            }
            .copyError(to: alertsSubject)
            .copyRunningState(to: processCountBinding)
            .do(onCompleted: { callback() })
            .subscribe()
    }

    private func checkIn() -> Single<TraceInfo> {
        guard let presenter = presenter else {
            return .error(TraceIdServiceError.unknown)
        }
        return CheckInInteractor(presenter: presenter, qrString: localCheckin.qrCode)
            .interact()
            .observe(on: MainScheduler.asyncInstance)
            .copyError(to: alertsSubject)
            .copyRunningState(to: processCountBinding)
            .andThen(traceIdService.currentTraceInfo.asObservable().asSingle())
    }

    private func setWelcomeAreaTitles() {
        if let locationName = localCheckin?.location.locationName {
            welcomeAreaTitle = locationName
            welcomeAreaSubtitle = localCheckin?.location.groupName
        } else if let groupName = localCheckin?.location.groupName {
            welcomeAreaTitle = groupName
            welcomeAreaSubtitle = nil
        }
    }

    private func checkDiscountCampaign() {
        _ = campaignService.checkCampaign(for: localCheckin.location.locationId, type: .lucaDiscount, date: timeProvider.now)
            .observe(on: MainScheduler.instance)
            .subscribe(onSuccess: { [weak self] campaign in
                self?.discountCampaign = campaign
            })
    }
}
