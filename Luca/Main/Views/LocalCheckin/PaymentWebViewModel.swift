import SwiftUI
import Combine
import DependencyInjection
import RxSwift

class PaymentWebViewModel: BaseViewModel {
    @InjectStatic(\.lucaPayment) private var payment

    @Published var fetchedPayment: PaymentFetchCheckoutResponse?
    @Published var showCompletionView: Bool = false

    func fetchPaymentStatus(paymentCheckout: PaymentCreateCheckoutResponse) {
        _ = payment.fetchCheckout(id: paymentCheckout.uuid)
            .observe(on: MainScheduler.instance)
            .do(onSuccess: {
                self.fetchedPayment = $0
                self.showCompletionView = true
            })
            .copyRunningState(to: processCountBinding)
            .copyError(to: alertsSubject)
            .debug("WebView.checkout")
            .subscribe()
    }
}
