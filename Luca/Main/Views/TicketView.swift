import UIKit

class TicketView: DocumentView {

    @IBOutlet weak var eventLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var issuerLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var ticketIdLabel: UILabel!
    @IBOutlet weak var verificationIdLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var eventIconImageView: UIImageView!
    @IBOutlet weak var qrCodeImageView: UIImageView!

    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var expandView: UIView!

    @IBOutlet weak var dateStackView: UIStackView!
    @IBOutlet weak var locationStackView: UIStackView!
    @IBOutlet weak var nameStackView: UIStackView!
    @IBOutlet weak var issuerStackView: UIStackView!
    @IBOutlet weak var categoryStackView: UIStackView!
    @IBOutlet weak var ticketIdStackView: UIStackView!
    @IBOutlet weak var verificationIdStackView: UIStackView!

    override var viewsToCheck: [ViewWidthDefining] {
        isExpanded ?
        [dateLabel, locationLabel, nameLabel, issuerLabel, ticketIdLabel, verificationIdLabel] :
        [dateLabel, locationLabel, nameLabel]
    }

    override var stackViews: [UIStackView] {
        isExpanded ?
        [dateStackView, locationStackView, nameStackView, issuerStackView, categoryStackView, ticketIdStackView, verificationIdStackView] :
        [dateStackView, locationStackView, nameStackView]
    }

    override internal func setup() {
        guard let ticket = document as? ConnfairTicket else { return }

        wrapperView.layer.cornerRadius = 8

        eventLabel.text = ticket.event
        dateLabel.text = ticket.startTime.formattedDateTime
        locationLabel.text = ticket.location ?? "-"
        nameLabel.text = ticket.formattedName
        issuerLabel.text = ticket.issuer
        ticketIdLabel.text = ticket.ticketId
        verificationIdLabel.text = ticket.verificationId ?? "-"

        setupEventCategory(ticket.category)

        qrCodeImageView.layer.cornerRadius = 8
        setupQRCodeImage(for: ticket)

        qrCodeImageView.isAccessibilityElement = true
        qrCodeImageView.accessibilityLabel = L10n.IOSApp.Contact.Qr.Accessibility.qrCode

        deleteButton.addTarget(self, action: #selector(didPressDelete(sender:)), for: .touchUpInside)
        deleteButton.layer.borderWidth = 1
        deleteButton.layer.borderColor = UIColor.black.cgColor
        deleteButton.layer.cornerRadius = 16
        deleteButton.accessibilityLabel = L10n.IOSApp.Test.Delete.title

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.isEnabled = true
        tapGestureRecognizer.cancelsTouchesInView = true
        addGestureRecognizer(tapGestureRecognizer)

        expandView.isHidden = !self.isExpanded
        disclosureIndicator.isHidden = isExpanded
        position = .single

        initObserver()
        updateStackViews()
    }

    private func setupEventCategory(_ category: TicketCategory?) {
        switch category {
        case .music: wrapperView.backgroundColor = Asset.lucaEventMusic.color
            categoryLabel.text = L10n.UserApp.Ticket.Category.music
            eventIconImageView.image = Asset.musicEventIcon.image
        case .party: wrapperView.backgroundColor = Asset.lucaEventParty.color
            categoryLabel.text = L10n.UserApp.Ticket.Category.party
            eventIconImageView.image = Asset.partyEventIcon.image
        case .classic: wrapperView.backgroundColor = Asset.lucaEventClassic.color
            categoryLabel.text = L10n.UserApp.Ticket.Category.classic
            eventIconImageView.image = Asset.classicEventIcon.image
        case .culture: wrapperView.backgroundColor = Asset.lucaEventCulture.color
            categoryLabel.text = L10n.UserApp.Ticket.Category.culture
            eventIconImageView.image = Asset.cultureEventIcon.image
        case .exhibition: wrapperView.backgroundColor = Asset.lucaEventExhibition.color
            categoryLabel.text = L10n.UserApp.Ticket.Category.exhibition
            eventIconImageView.image = Asset.exhibitionEventIcon.image
        case .conference: wrapperView.backgroundColor = Asset.lucaEventConference.color
            categoryLabel.text = L10n.UserApp.Ticket.Category.conference
            eventIconImageView.image = Asset.conferenceEventIcon.image
        case .movie: wrapperView.backgroundColor = Asset.lucaEventMovie.color
            categoryLabel.text = L10n.UserApp.Ticket.Category.movie
            eventIconImageView.image = Asset.movieEventIcon.image
        case .family: wrapperView.backgroundColor = Asset.lucaEventFamily.color
            categoryLabel.text = L10n.UserApp.Ticket.Category.family
            eventIconImageView.image = Asset.familieEventIcon.image
        case .sport: wrapperView.backgroundColor = Asset.lucaEventSport.color
            categoryLabel.text = L10n.UserApp.Ticket.Category.sport
            eventIconImageView.image = Asset.sportEventIcon.image
        case .other, .none: wrapperView.backgroundColor = Asset.lucaEventOther.color
            categoryLabel.text = L10n.UserApp.Ticket.Category.others
            eventIconImageView.image = Asset.otherEventIcon.image
        }
    }

    private func setupQRCodeImage(for ticket: ConnfairTicket) {
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let image = QRCodeGenerator.generateQRCode(string: ticket.originalCode)
        if let scaledQr = image?.transformed(by: transform) {
            qrCodeImageView.image = UIImage(ciImage: scaledQr)
        }
    }
}

// MARK: - DocumentViewProtocol

extension TicketView: DocumentViewProtocol {

    public static func createView(document: Document, owner: Person, isExpanded: Bool, delegate: DocumentViewDelegate?) -> DocumentView? {
        guard let document = document as? ConnfairTicket else { return nil }

        let itemView: TicketView = TicketView.fromNib()
        itemView.delegate = delegate
        itemView.isExpanded = isExpanded
        itemView.document = document
        itemView.isAccessibilityElement = !isExpanded
        itemView.accessibilityTraits = .button
        itemView.accessibilityLabel = L10n.IOSApp.Test.Show.title
        itemView.owner = owner

        return itemView
    }
}

// MARK: - Actions

extension TicketView {
    @objc
    private func viewTapped(_ sender: UITapGestureRecognizer) {
        guard let document = document else { return }
        delegate?.didSelect(self, document: document)
    }

    @objc
    private func didPressDelete(sender: UIButton) {
        if let appointment = self.document {
            delegate?.didTapDelete(self, for: appointment)
        }
    }
}
