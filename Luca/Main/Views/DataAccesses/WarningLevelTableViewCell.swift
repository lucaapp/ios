import UIKit

class WarningLevelTableViewCell: UITableViewCell {
    @IBOutlet weak var readIndicator: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var shortMessageLabel: UILabel!

    var warningLevelCellModel: WarningLevelCellViewModel! {
        didSet {
            guard let warningLevel = warningLevelCellModel else { return }
            isRead = warningLevel.isRead
            titleLabel.text = warningLevel.title
            shortMessageLabel.text = warningLevel.subtitle
        }
    }

    var isRead: Bool = true {
        didSet {
            readIndicator.layer.cornerRadius = readIndicator.frame.width * 0.5
            readIndicator.backgroundColor = isRead ? .white : Asset.lucaError.color
            titleLabel.textColor = isRead ? .white : Asset.lucaError.color
        }
    }
}
