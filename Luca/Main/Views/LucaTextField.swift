import UIKit

class LucaTextField: FormTextField {

    override func setText(_ text: String?) {
        if let value = text, value != "" {
            setDefaultMode()
            textField.text = value
            return
        }
    }

    func setupGreyField() {
        textField.keyboardType = .numberPad
        textField.borderColor = .black
        textField.tintColor = .black
        textField.textColor = .black
    }

}
