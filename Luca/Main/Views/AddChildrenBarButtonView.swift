import LucaUIComponents
import UIKit

protocol AddChildrenBarButtonViewDelegate: AnyObject {
    func didTapButton()
}

enum AddChildrenBarButtonViewColorMode {
    case light
    case dark
}

class AddChildrenBarButtonView: UIView {

    // MARK: - UI elements

    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 5

        return stackView
    }()

    lazy var countLabel: Luca16PtBoldLabel = {
        let label = Luca16PtBoldLabel()
        label.text = ""
        label.textColor = colorMode == .light ? Asset.lucaBlue.color : UIColor.black

        return label
    }()

    lazy var button: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)

        return button
    }()

    lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.image = Asset.addPersonWhite.image.withRenderingMode(.alwaysTemplate)
        view.contentMode = .center
        view.tintColor = colorMode == .light ? Asset.lucaBlue.color : UIColor.black

        return view
    }()

    // MARK: - Properties

    weak var delegate: AddChildrenBarButtonViewDelegate?
    var colorMode: AddChildrenBarButtonViewColorMode = .light

    // MARK: - Init

    init(colorMode: AddChildrenBarButtonViewColorMode) {
        super.init(frame: .zero)

        self.colorMode = colorMode

        setupUI()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
        setupConstraints()
    }
}

// MARK: - public functions

extension AddChildrenBarButtonView {
    func setCount(_ count: Int) {
        countLabel.text = count == 0 ? "" : "\(count)"
        countLabel.accessibilityLabel = (count == 0 ? L10n.IOSApp.Children.Number.zero : (count == 1 ? L10n.IOSApp.Children.Number.one : L10n.IOSApp.Children.Number.other(count)))
    }
}

// MARK: - Actions

extension AddChildrenBarButtonView {
    @objc
    func didTapButton() {
        delegate?.didTapButton()
    }
}

// MARK: - UI setup

extension AddChildrenBarButtonView {
    private func setupUI() {
        clipsToBounds = true
        backgroundColor = .clear

        stackView.addArrangedSubview(countLabel)
        stackView.addArrangedSubview(imageView)
        addSubview(stackView)
        addSubview(button)
    }

    private func setupConstraints() {
        stackView.pinToSuperView()
        button.pinToSuperView()
    }
}
