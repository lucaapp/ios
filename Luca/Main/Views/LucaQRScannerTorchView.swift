import UIKit
import AVFoundation

class LucaQRScannerTorchView: UIView, ContainsTorchButton {

    var label: UILabel = {
        let label = UILabel()
        label.text = L10n.IOSApp.Contact.Qr.scan
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.montserratTableViewDescription
        return label
    }()

    let torchButton: UIButton = {
        let button = UIButton()
        button.accessibilityLabel = L10n.IOSApp.Light.Button.activate
        button.setImage(Asset.light.image, for: .normal)
        button.contentHorizontalAlignment = .right
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }

    private func setup() {
        addSubview(label)
        addSubview(torchButton)

        label.setAnchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor)
        label.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true

        guard let device = AVCaptureDevice.default(for: .video) else {
            self.torchButton.isHidden = true
            return
        }

        torchButton.isHidden = !device.hasTorch || !device.isTorchAvailable
        torchButton.setAnchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor)
        torchButton.aspectRatio(1).isActive = true
        torchButton.addTarget(self, action: #selector(didPressTorch(sender:)), for: .touchUpInside)
    }

    @objc func didPressTorch(sender: UIButton) {
        toggleTorch(sender: sender)
    }

}
