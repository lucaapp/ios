import SwiftUI
import RxSwift
import DependencyInjection

struct CampaignView: View {

    @ObservedObject var viewModel: CampaignViewModel
    @Binding var openPaymentFlow: Bool
    @State var isChecked: Bool = false
    @State private var email: String = ""

    var payment: Payment

    var body: some View {
        VStack(alignment: .leading, spacing: 24) {
            titleView
            Text(L10n.UserApp.Pay.Raffle.Consent.text).lucaFont(.luca14Medium)
            TextField(L10n.UserApp.Pay.Raffle.Mail.field, text: self.$email).textFieldStyle(LucaTextFieldStyle())

            Spacer()
            checkboxView
            Button(L10n.UserApp.Pay.Raffle.Participate.button) {
                viewModel.submitCampaign(payment: payment, email: email.trimmingCharacters(in: .whitespaces))
            }
            .disabled(email.isEmpty || !isChecked)
            .buttonStyle(LucaButton(color: .blue))
        }
        .handleAlerts($viewModel.alerts)
        .onChange(of: viewModel.campaignSubmitted) { submitted in
            if submitted {
                openPaymentFlow.toggle()
            }
        }
        .loadingIndicator(isLoading: $viewModel.isProcessing)
        .padding(16)
        .appearance(.dark)
    }

    private var titleView: some View {
        HStack(alignment: .center) {
            Spacer()
            Text(L10n.UserApp.Pay.Raffle.Consent.title).lucaFont(.luca20Bold)
            Spacer()
        }
    }

    private var checkboxView: some View {
        HStack {
            Toggle(L10n.UserApp.Pay.Raffle.Consent.Terms.accept, isOn: $isChecked)
                .toggleStyle(CheckboxToggleStyle())
            Spacer().frame(width: 16)
            Text.supportMarkdownLinks(L10n.UserApp.Pay.Raffle.Consent.Terms.accept)
            .accentColor(.white)
            .lucaFont(.luca14Medium)
        }
    }

}

struct CampaignView_Previews: PreviewProvider {

    static var previews: some View {
        CampaignView(viewModel: CampaignViewModel(),
                   openPaymentFlow: .constant(true),
                   payment: Payment(paymentId: "",
                                    locationName: "Test",
                                    locationId: "",
                                    amount: 1,
                                    invoiceAmount: 123,
                                    tipAmount: 1,
                                    dateTime: 123,
                                    status: .closed))
    }

}
