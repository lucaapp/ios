import SwiftUI

enum LucaDiscountCampaignInfoViewStyle {

    case plain
    case greenTint
}

struct LucaDiscountCampaignInfoView: View {

    @Binding var discountCampaign: PaymentCampaign?
    let style: LucaDiscountCampaignInfoViewStyle

    var body: some View {
        if let discountCampaign = discountCampaign,
           let discountPercentage = discountCampaign.paymentCampaign.discountPercentage,
           let discountMaxAmount = discountCampaign.paymentCampaign.discountMaxAmount {
            content(for: discountPercentage, and: discountMaxAmount)
                .padding(.horizontal, style == .greenTint ? 16 : 0)
                .frame(maxWidth: .infinity)
                .background(
                    border
                )
                .appearance(.dark)
        } else {
            EmptyView()
        }
    }

    private var greenTint: Color {
        let green = Asset.lucaDiscountGreen.color
        return style == .greenTint ? Color(green) : .white
    }

    private func content(for percentage: Int, and maxAmount: Double) -> some View {
        HStack(alignment: .top, spacing: 16, content: {
            Image(uiImage: Asset.starGreen.image)
                .padding(.top, 16)
            Text(L10n.UserApp.Pay.Details.Discount.hint(percentage, Int(maxAmount)))
                .foregroundColor(greenTint)
                .frame(maxWidth: .infinity)
                .padding(.vertical, 12)
                .multilineTextAlignment(.leading)
                .lucaFont(.luca14Medium)
        })
    }

    @ViewBuilder
    private var border: some View {
        if style == .greenTint {
            RoundedRectangle(cornerRadius: 6)
                .stroke(greenTint, lineWidth: 1)
                .padding(1)
        } else {
            EmptyView()
        }
    }
}

struct LucaDiscountCampaignInfoView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            LucaDiscountCampaignInfoView(discountCampaign: .constant(nil), style: .plain)
                .previewLayout(.fixed(width: 400, height: 120))
                .previewDisplayName("Plain Style")

            LucaDiscountCampaignInfoView(discountCampaign: .constant(nil), style: .greenTint)
                .previewLayout(.fixed(width: 400, height: 100))
                .previewDisplayName("Plain Style")
        }
    }
}
