import Foundation
import RxSwift
import SwiftUI
import DependencyInjection

class CampaignViewModel: BaseViewModel {

    @InjectStatic(\.campaignService) private var campaignService

    @Published var campaignExists: Bool = false
    @Published var campaign: PaymentCampaign? {
        didSet {
            self.campaignExists = campaign != nil
        }
    }

    @Published var dismissView: Bool = false
    @Published var campaignSubmitted: Bool = false

    func checkCampaign(locationId: String) {
        _ = campaignService.checkCampaign(for: locationId, type: .tipTopUpAndRaffle)
            .observe(on: MainScheduler.instance)
            .subscribe(onSuccess: { campaign in
                self.campaign = campaign
            })
    }

    func submitCampaign(payment: Payment, email: String) {
        guard let lang = Locale.current.languageCode, let id = payment.paymentId else { return }

        _ = campaignService.submitParticipation(
            paymentId: id,
            campaign: CampaignType.tipTopUpAndRaffle.rawValue,
            email: email,
            language: lang
        )
            .copyError(to: alertsSubject)
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { _ in
                self.campaignSubmitted = true
            })
            .copyRunningState(to: processCountBinding)
            .subscribe()
    }

}
