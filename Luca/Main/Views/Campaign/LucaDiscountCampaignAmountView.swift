import SwiftUI

struct LucaDiscountCampaignAmountView: View {

    let labelText: String
    let amount: Money<EUR>
    let discountedAmount: Money<EUR>

    var body: some View {
        HStack(alignment: .top, content: {
            Text(labelText).lucaFont(.luca14Medium)
            Spacer()
            VStack(spacing: 4) {
                Text(discountedAmount.formattedAmountWithCode)
                    .foregroundColor(Color(Asset.lucaDiscountGreen.color))
                    .lucaFont(.luca16Bold)
                Text(amount.formattedAmountWithCode)
                    .strikethrough()
                    .lucaFont(.luca14Medium)
            }
        })
    }
}
