import UIKit
import LucaUIComponents
import RxSwift

class AccountTableViewCell: UITableViewCell, Identifiable {

    private var disposeBag: DisposeBag!

    var option: AccountOption? {
        didSet { setupOption() }
    }

    private lazy var `switch`: UISwitch = {
        let view = UISwitch()
        view.onTintColor = Asset.lucaBlue.color
        view.setContentHuggingPriority(.required, for: .horizontal)
        view.setContentCompressionResistancePriority(.required, for: .horizontal)
        return view
    }()

    private lazy var iconView: UIImageView = {
        let icon = UIImageView()
        icon.contentMode = .scaleAspectFit
        icon.setContentHuggingPriority(.required, for: .horizontal)
        icon.setContentCompressionResistancePriority(.required, for: .horizontal)
        return icon
    }()

    private lazy var switchView = UIView()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 5

        return stackView
    }()

    private lazy var titleLabel: UILabel = {
        let titleLabel = LucaAccountTitleLabel()
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping

        return titleLabel
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupUI() {
        backgroundColor = Asset.lucaWhiteLowAlpha.color
        contentView.addSubview(stackView)
        setupStackView()
    }

    private func setupStackView() {
        stackView.addArrangedSubview(iconView)

        stackView.addArrangedSubview(titleLabel)

        switchView.addSubview(self.switch)

        self.switch.translatesAutoresizingMaskIntoConstraints = false
        self.switch.trailingAnchor.constraint(equalTo: switchView.trailingAnchor).isActive = true
        self.switch.centerYAnchor.constraint(equalTo: switchView.centerYAnchor).isActive = true
        self.switch.leadingAnchor.constraint(equalTo: switchView.leadingAnchor).isActive = true

        stackView.addArrangedSubview(switchView)
        switchView.isHidden = true

        stackView.setAnchor(top: contentView.topAnchor,
                            leading: contentView.leadingAnchor,
                            bottom: contentView.bottomAnchor,
                            trailing: contentView.trailingAnchor,
                            padding: .init(top: 16, left: 16, bottom: 16, right: 16))
    }

    func configureTitle(_ title: String, accessibilityTitle: String? = nil) {
        titleLabel.text = title
        titleLabel.accessibilityLabel = accessibilityTitle ?? title
        self.switch.accessibilityLabel = title
    }

    func setupOption() {
        setupIconViews()
        setupAccessoryViews()
    }

    func setupIconViews() {
        iconView.image = nil
        iconView.isHidden = true

        if let option = option as? SettingCoordinatorAccountOption {
            let icon = option.icon.withAlignmentRectInsets(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -10))
            iconView.isHidden = false
            iconView.image = icon
        }
    }

    func setupAccessoryViews() {
        accessoryView = .none
        disposeBag = DisposeBag()

        // Add switch to toggle options or icons to buttons and external links
        if let option  = option as? ToggleOption {
            switchView.isHidden = false
            accessibilityTraits = .none
            selectionStyle = .none

            option
                .toggleValue
                .observe(on: MainScheduler.instance)
                .do(onNext: { [weak self] value in
                    self?.switch.setOn(value, animated: false)
                })
                .subscribe()
                .disposed(by: disposeBag)

            self.switch.addTarget(self, action: #selector(switchChanged(_:)), for: .valueChanged)
        } else {
            var icon = Asset.rightArrow.image
            if let option = option as? CoordinatorAccountOption, option.coordinator is OpenLinkCoordinator {
                icon = Asset.opensLink.image
            }
            let imageView = UIImageView(image: icon)
            imageView.contentMode = .scaleAspectFit
            accessoryView = imageView
        }
    }

    @objc func switchChanged(_ sender: UISwitch) {
        if let option = option as? ToggleOption {
            option.toggleChanged(sender)
        }
    }

}
