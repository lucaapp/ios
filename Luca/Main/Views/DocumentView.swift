import UIKit
import RxSwift
import DependencyInjection

protocol DocumentViewDelegate: AnyObject {
    func didSelect(_ view: UIView, document: Document)
    func didTapDelete(_ view: UIView, for document: Document)
}

protocol DocumentViewProtocol: AnyObject {
    static func createView(document: Document, owner: Person, isExpanded: Bool, delegate: DocumentViewDelegate?) -> DocumentView?
}

class DocumentView: UIView {
    @InjectStatic(\.dgcVerificationService) private var dgcVerificationService

    @IBOutlet weak var verificationButton: UIButton!
    @IBOutlet weak var recentDocumentIndicator: UIImageView!
    @IBOutlet weak var disclosureIndicator: UIImageView!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint?
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint?

    /// Presented document
    var document: Document? {
        didSet {
            updateVerificationImageView()
            setup()
        }
    }

    /// Person this document belongs to.
    var owner: Person!

    var isExpanded: Bool = false

    /// should the view use horizontal layout
    var useHorizontalLayout: Bool = true

    /// define the views to be checkt for length
    var viewsToCheck: [ViewWidthDefining] {
        []
    }

    /// defice the stackview, which needs to be switched, when the length of the labels did not fit
    var stackViews: [UIStackView] {
        []
    }

    /// closure for pressed verification warning button
    var warningPressed: (() -> Void)?

    weak var delegate: DocumentViewDelegate?
    var position: HorizontalDocumentListViewItemPosition = .middle {
        didSet {
            updatePosition()
        }
    }

    internal func setup() {
        fatalError("Not implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        checkLabels()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func updatePosition() {
        if position == .leading {
            leadingConstraint?.constant = 16
            trailingConstraint?.constant = 0
        } else if position == .trailing {
            leadingConstraint?.constant = 0
            trailingConstraint?.constant = -16
        } else if position == .single {
            leadingConstraint?.constant = 16
            trailingConstraint?.constant = -16
        } else if position == .middle {
            leadingConstraint?.constant = 0
            trailingConstraint?.constant = 0
        }
    }

    func setupVerificationButton(isVerified: Bool) {
        isVerified ? verificationButton.setImage(Asset.checkmarkGreen.image, for: .normal) : verificationButton.setImage(Asset.warningOrange.image, for: .normal)
        // Only enable button press when unverified (to display information modal)
        verificationButton.isEnabled = !isVerified
        verificationButton.setTitle("", for: .normal)
        verificationButton.isHidden = false

        // Accessibility
        verificationButton.accessibilityTraits = isVerified ? .none : .button
        verificationButton.accessibilityLabel = isVerified ? L10n.IOSApp.Document.Verification.Success.accessibility : L10n.IOSApp.Document.Verification.Failed.title
    }
}

// MARK: - DynamicType handling

extension DocumentView {
    func initObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(preferredContentSizeChanged(_:)), name: UIContentSizeCategory.didChangeNotification, object: nil)
    }

    func updateStackViews() {
        stackViews.forEach {
            $0.spacing = useHorizontalLayout ? 8 : 0
            $0.axis = useHorizontalLayout ? .horizontal : .vertical
        }
    }

    @objc func preferredContentSizeChanged(_ notification: Notification) {
        useHorizontalLayout = true
        updateStackViews()
    }

    func checkLabels() {
        if viewsToCheck.compactMap({ return $0.viewFitsInOneLine() == true ? true : nil }).count > 0 {
            useHorizontalLayout = false
            updateStackViews()
        }
    }
}

// MARK: - DGC verification presentation

extension DocumentView {
    private func updateVerificationImageView() {
        if let dgcVaccination = document as? ConformsDGC {
            verificationButton.isHidden = true
            _ = dgcVerificationService.verifyCode(dgcVaccination.cert.hCert)
                .observe(on: MainScheduler.asyncInstance)
                .do(onSuccess: { [weak self] isVerified in
                    self?.setupVerificationButton(isVerified: isVerified) },
                    onError: { [weak self] _ in
                    self?.setupVerificationButton(isVerified: false) })
                .subscribe()
        }
    }

    @IBAction func verificationButtonPressed(_ sender: UIButton) {
        if let warningPressed = warningPressed {
            warningPressed()
        }
    }
}
