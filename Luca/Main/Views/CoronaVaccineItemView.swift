import UIKit
import RxSwift
import DependencyInjection
import LucaUIComponents

class CoronaVaccineItemView: DocumentView, HorizontalGroupable {

    @InjectStatic(\.dgcVerificationService) private var dgcVerificationService

    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var validDurationDescriptionLabel: UILabel!
    @IBOutlet weak var validDurationLabel: UILabel!
    @IBOutlet weak var labLabel: UILabel!
    @IBOutlet weak var vaccinationDateLabel: UILabel!
    @IBOutlet weak var vaccineLabel: UILabel!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var qrCodeImageView: UIImageView!

    @IBOutlet weak var dateStackView: UIStackView!
    @IBOutlet weak var durationStackView: UIStackView!
    @IBOutlet weak var labStackView: UIStackView!
    @IBOutlet weak var dateOfBirthStackView: UIStackView!
    @IBOutlet weak var vaccineDataStackView: UIStackView!

    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var expandView: UIView!

    let groupedKey = "CoronaVaccineItemView"

    override var viewsToCheck: [ViewWidthDefining] {
        isExpanded ? [nameLabel, durationStackView, dateLabel, labLabel, dateOfBirthLabel, vaccinationDateLabel, vaccineLabel] : [nameLabel, dateLabel, durationStackView]
    }

    override var stackViews: [UIStackView] {
        isExpanded ? [dateStackView, durationStackView, labStackView, dateOfBirthStackView, vaccineDataStackView] : [dateStackView, durationStackView]
    }

    override func updatePosition() {
        super.updatePosition()

        updateResultLabel()
    }

    override internal func setup() {
        verificationButton?.setTitle("", for: .normal)
        verificationButton?.isHidden = true
        guard let vaccination = document as? Vaccination else { return }

        updateResultLabel()

        backgroundColor = .clear

        nameLabel.text = owner.formattedName
        dateLabel.text = vaccination.fullVaccinationDate?.formattedDate
        dateLabel.accessibilityLabel = vaccination.issuedAt.accessibilityDate
        validDurationLabel.text = vaccination.issuedAt.durationSinceDate
        labLabel.text = vaccination.laboratory
        dateOfBirthLabel.text = vaccination.dateOfBirth.formattedDate
        vaccinationDateLabel.text = vaccination.issuedAt.formattedDate
        vaccineLabel.text = vaccination.vaccineType.readableName

        qrCodeImageView.layer.cornerRadius = 8
        setupQRCodeImage(for: vaccination)

        qrCodeImageView.isAccessibilityElement = true
        qrCodeImageView.accessibilityLabel = L10n.IOSApp.Contact.Qr.Accessibility.qrCode

        deleteButton.addTarget(self, action: #selector(didPressDelete(sender:)), for: .touchUpInside)
        deleteButton.layer.borderWidth = 1
        deleteButton.layer.borderColor = Asset.lucaBlack.color.cgColor
        deleteButton.layer.cornerRadius = 16
        deleteButton.accessibilityLabel = L10n.IOSApp.Test.Delete.title

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.isEnabled = true
        tapGestureRecognizer.cancelsTouchesInView = true
        addGestureRecognizer(tapGestureRecognizer)

        recentDocumentIndicator.isHidden = !vaccination.isRecent
        disclosureIndicator.isHidden = isExpanded
        expandView.isHidden = !isExpanded

        initObserver()
        updateStackViews()
    }

    private func updateResultLabel() {
        guard let vaccination = document as? Vaccination else { return }

        let doseNumber = vaccination.doseNumber
        let dosesTotal = vaccination.totalDosesNumber
        wrapperView.layer.cornerRadius = 8
        if vaccination.isComplete() {
            wrapperView.backgroundColor = Asset.lucaCheckinGradientTop.color
            resultLabel.text = L10n.IOSApp.Vaccine.Result.title(doseNumber, dosesTotal)
        } else {
            wrapperView.backgroundColor = Asset.lucaBeige.color
            if vaccination.isCompleteDose {
                resultLabel.text = L10n.IOSApp.Vaccine.Result.completeInDays(vaccination.fullyVaccinatedInDays)
            } else {
                resultLabel.text = L10n.IOSApp.Vaccine.Result.title(doseNumber, dosesTotal)
            }
        }
        if !vaccination.isValid {
            wrapperView.backgroundColor = Asset.lucaGrey.color
        }
    }

    private func setupQRCodeImage(for vaccine: Document) {
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let image = QRCodeGenerator.generateQRCode(string: vaccine.originalCode)
        if let scaledQr = image?.transformed(by: transform) {
            qrCodeImageView.image = UIImage(ciImage: scaledQr)
        }
    }
}

// MARK: - DocumentViewProtocol

extension CoronaVaccineItemView: DocumentViewProtocol {
    public static func createView(document: Document, owner: Person, isExpanded: Bool, delegate: DocumentViewDelegate?) -> DocumentView? {
        guard let document = document as? Vaccination else { return nil }

        let itemView: CoronaVaccineItemView = CoronaVaccineItemView.fromNib()
        itemView.delegate = delegate
        itemView.owner = owner
        itemView.isExpanded = isExpanded
        itemView.document = document
	    itemView.isAccessibilityElement = !isExpanded
        itemView.accessibilityTraits = .button
        itemView.accessibilityLabel = L10n.IOSApp.Test.Show.title

        return itemView
    }
}

// MARK: - Actions

extension CoronaVaccineItemView {
    @objc
    private func viewTapped(_ sender: UITapGestureRecognizer) {
        guard let document = document else { return }
        delegate?.didSelect(self, document: document)
    }
    @objc

    private func didPressDelete(sender: UIButton) {
        if let vaccination = document {
            delegate?.didTapDelete(self, for: vaccination)
        }
    }
}
