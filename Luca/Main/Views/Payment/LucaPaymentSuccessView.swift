import SwiftUI

struct LucaPaymentSuccessView: View {

    @StateObject var campaignViewModel = CampaignViewModel()
    @StateObject var viewModel: LucaPaymentSuccessViewModel
    @Binding var openPaymentFlow: Bool
    @State var openCampaignView: Bool = false

    var body: some View {
        stackView
            .handleAlerts($viewModel.alerts)
            .padding(.horizontal, 16)
            .padding(.vertical, 24)
            .appearance(.dark)
    }

    private var stackView: some View {
        VStack {
            VStack(spacing: 24) {
                titleView
                subtitleSection
                infoSection
            }
            .padding(.bottom, 44)

            checkmarkImage
            Spacer().frame(maxHeight: .infinity)

            VStack(spacing: 24) {
                campaignButton.show(isVisible: campaignViewModel.campaignExists)
                okButton
            }
        }
        .onAppear {
            campaignViewModel.checkCampaign(locationId: viewModel.payment.locationId)
        }
        .overlay(
            NavigationLink(isActive: $openCampaignView, destination: {
                CampaignView(viewModel: campaignViewModel, openPaymentFlow: $openPaymentFlow, payment: viewModel.payment)
            }, label: {})
        )
    }

    private var titleView: some View {
        Text(L10n.UserApp.Pay.Success.title)
            .lucaFont(.luca20Bold)
    }

    private var subtitleSection: some View {
        VStack(spacing: 10) {
            Text(viewModel.formattedAmount)
                .lucaFont(.luca24Regular)
            Text(viewModel.payment.locationName)
                .lucaFont(.luca16Medium)
        }
    }

    private var infoSection: some View {
        VStack(spacing: 10) {
            amountSection
            if let tipAmount = viewModel.payment.tipAmount {
                list(tipAmount.money(with: EUR()).formattedAmountWithCode, for: L10n.UserApp.Pay.Success.tip)
            }
            let paymentId = viewModel.payment.paymentVerifier ?? L10n.IOSApp.General.unknown
            list(paymentId, for: L10n.UserApp.Pay.Success.transactionID)
        }
    }

    @ViewBuilder
    private var amountSection: some View {
        if let discount = viewModel.payment.lucaDiscount {
            LucaDiscountCampaignAmountView(
                labelText: L10n.UserApp.Pay.Success.amount,
                amount: discount.originalInvoiceAmount.money(with: EUR()),
                discountedAmount: (discount.originalInvoiceAmount - discount.discountAmount).money(with: EUR())
            )
        } else {
            list(viewModel.payment.originalInvoiceAmount.money(with: EUR()).formattedAmountWithCode, for: L10n.UserApp.Pay.Success.amount)
        }
    }

    private func list(_ value: String, for caption: String) -> some View {
        HStack {
            Text(caption).lucaFont(.luca14Medium)
            Spacer()
            Text(value).lucaFont(.luca14Bold)
        }
    }

    private var checkmarkImage: some View {
        GeometryReader { geometry in
            VStack {
                Image(uiImage: Asset.checkmarkPayment.image)
                    .resizable()
                    .aspectRatio(1, contentMode: .fit)
                    .frame(width: geometry.size.width / 3.0)
            }
            .frame(maxWidth: .infinity)
        }
    }

    private var okButton: some View {
        Button(L10n.IOSApp.Navigation.Basic.ok.uppercased()) {
            viewModel.checkout(completion: {
                openPaymentFlow.toggle()
            })

        }
        .buttonStyle(LucaButton(color: .blue))
    }

    private var campaignButton: some View {
        Button(L10n.UserApp.Pay.Success.Raffle.button) {
            openCampaignView = true
        }.buttonStyle(LucaButtonWithWhiteBorder())
    }

}

struct LucaPaymentSuccessView_Previews: PreviewProvider {
    static var previews: some View {
        let payment = Payment(
            paymentId: "OLEHB",
            locationName: "Restaurant XYZ",
            locationId: "1",
            amount: 70.32,
            invoiceAmount: 123,
            tipAmount: 11.72,
            dateTime: 0.0,
            status: .closed
        )
        LucaPaymentSuccessView(viewModel: LucaPaymentSuccessViewModel(payment: payment), openPaymentFlow: .constant(true))
            .environmentObject(PaymentFlowViewModel(paymentLocationData: nil))
    }
}
