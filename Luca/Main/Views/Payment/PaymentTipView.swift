import SwiftUI

struct PaymentTipView: View {
    @EnvironmentObject var viewModel: PaymentFlowViewModel
    @Binding var openPaymentFlow: Bool

    private let padding = 16.0

    init(openPaymentFlow: Binding<Bool>) {
        self._openPaymentFlow = openPaymentFlow
    }

    var body: some View {
        VStack(spacing: 24) {
            Text(L10n.UserApp.Pay.Tip.headline)
                .lucaFont(.luca20Bold)
            if let discountedAmount = viewModel.discountedAmount {
                LucaDiscountCampaignAmountView(
                    labelText: L10n.UserApp.Pay.Success.amount,
                    amount: viewModel.enteredAmount,
                    discountedAmount: discountedAmount
                )
            }
            LucaDiscountCampaignInfoView(discountCampaign: $viewModel.discountCampaign, style: .plain)
                .frame(maxHeight: 120)
            Text(L10n.UserApp.Pay.Tip.text)
                .lucaFont(.luca14Medium)
            tipAmountViews
            Spacer()
            HStack {
                Text(L10n.UserApp.Pay.Success.amount)
                    .lucaFont(.luca14Medium)
                Spacer()
                Text(viewModel.formattedAmountWithTip)
                    .lucaFont(24.0, weight: .medium)
            }

            VStack(spacing: 8) {
                Text(L10n.UserApp.Rapyd.disclaimer)
                    .lucaFont(.luca14Medium)
                    .foregroundColor(Color(Asset.lucaGrey.color))

                    Button(L10n.IOSApp.Pay.Amount.button(viewModel.formattedAmountWithTip)) {
                        Task {
                            await viewModel.saveTipPreference()
                            await viewModel.makePayment()
                        }
                    }
                    .buttonStyle(LucaButton())
            }
        }
        .padding(padding)
        .navigationBarTitle("", displayMode: .inline)
        .navBarCloseButton($openPaymentFlow)
        .loadingIndicator(isLoading: $viewModel.isProcessing)
        .handleAlerts($viewModel.alerts)
        .appearance(.dark)
        .onAppear {
            viewModel.pullTipPreference()
        }
        .overlay(
            NavigationLink(isActive: $viewModel.openRedirectURL, destination: {
                if let paymentCheckout = viewModel.currentPayment {
                    LazyView(PaymentWebView(openPaymentFlow: $openPaymentFlow, openTipView: $viewModel.openRedirectURL, paymentCheckout: paymentCheckout))
                }
            }, label: {})
        )

    }

    @ViewBuilder
    private var tipAmountViews: some View {
        HStack(spacing: 13) {
            ForEach(0..<4) { index in
                let tipPercentage = viewModel.tipPercentages[index]
                TipCell(tipPercent: tipPercentage)
                    .onTapGesture(perform: {
                        self.viewModel.tipSelected = tipPercentage
                    })
            }
        }
    }

}

struct TipCell: View {
    @EnvironmentObject var viewModel: PaymentFlowViewModel
    var tipPercent: Double
    var tipSelected: Bool {
        tipPercent == viewModel.tipSelected
    }

    init(tipPercent: Double) {
        self.tipPercent = tipPercent
    }

    var body: some View {
        RoundedRectangle(cornerRadius: 4)
            .stroke(tipSelected ? Color(Asset.lucaBlue.color) : .white,
                    lineWidth: tipSelected ? 3.0 : 1.0)
            .foregroundColor(Color.black)
            .frame(height: 56)
            .overlay(
                VStack(spacing: 8) {
                    Text(viewModel.formatTip(tipPercent: tipPercent))
                        .lucaFont(.luca14Medium)
                    Text(tipPercent.formattedPercentage)
                        .lucaFont(12.0, weight: .medium)
                })
    }

}

struct PaymentTipView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            PaymentTipView(openPaymentFlow: .constant(false))
        }
    }
}
