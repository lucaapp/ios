import SwiftUI

struct LucaPaymentFailureView: View {

    @Binding var openPaymentFlow: Bool
    @Binding var openTipView: Bool
    @EnvironmentObject private var paymentFlowViewModel: PaymentFlowViewModel

    var body: some View {
        stackView
            .padding(.horizontal, 16)
            .padding(.vertical, 24)
            .navBarCancelButton($openPaymentFlow)
            .appearance(.dark)
    }

    private var stackView: some View {
        VStack {
            VStack(spacing: 24) {
                titleView
                textView
            }
            .padding(.bottom, 40)

            image
            Spacer().frame(maxHeight: .infinity)
            retryButton
        }
    }

    private var titleView: some View {
        Text(L10n.UserApp.Pay.Error.title)
            .lucaFont(.luca20Bold)
    }

    private var textView: some View {
        Text(L10n.UserApp.Pay.Error.text)
            .multilineTextAlignment(.center)
            .lucaFont(.luca14Medium)
    }

    private var image: some View {
        Image(uiImage: Asset.rejectCross.image)
            .resizable()
            .frame(width: 60, height: 60)
    }

    private var retryButton: some View {
        Button(L10n.UserApp.Pay.Retry.button.uppercased()) {
            openTipView.toggle()
            paymentFlowViewModel.currentPayment = nil
        }
        .buttonStyle(LucaButton(color: .blue))
    }
}

struct LucaPaymentFailureView_Previews: PreviewProvider {
    static var previews: some View {
        LucaPaymentFailureView(openPaymentFlow: .constant(true), openTipView: .constant(false))
    }
}
