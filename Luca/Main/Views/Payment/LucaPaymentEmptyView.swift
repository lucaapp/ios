import SwiftUI

struct LucaPaymentEmptyView: View {
    @Binding var openScanner: Bool

    var body: some View {
        VStack {
            VStack(alignment: .leading, spacing: 16) {
                Text(L10n.UserApp.Pay.Empty.Screen.title)
                    .lucaFont(.luca16Bold)
                Text(L10n.UserApp.Pay.Empty.Screen.text)
                    .lucaFont(.luca14Medium)
            }
            .padding(.top, 8)
            .padding(.bottom, 40)

            Image(uiImage: Asset.money.image)
                .scaledToFill()
                .frame(width: 120.0, height: 120.0)
            Spacer()
            Button {
                openScanner.toggle()
            } label: {
                Text(L10n.UserApp.Pay.Open.scanner.uppercased())
            }.buttonStyle(LucaButton())

        }.padding(16)
    }
}

struct LucaPaymentEmptyView_Previews: PreviewProvider {
    static var previews: some View {
        UIElementPreview(
            LucaPaymentEmptyView(openScanner: .constant(false)).environment(\.presenter, Weak(UIViewController()))
        )
    }
}
