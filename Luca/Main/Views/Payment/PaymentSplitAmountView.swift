import SwiftUI
import Combine

struct PaymentSplitAmountView: View {
    @EnvironmentObject var viewModel: PaymentFlowViewModel
    @Binding var openPaymentFlow: Bool
    @State private var showTipView = false

    private let padding = 16.0

    var body: some View {
        VStack {
            ScrollView {
                VStack {
                    Text(L10n.UserApp.Pay.Split.Initiate.button).lucaFont(.luca20Bold)
                        .padding(.bottom, 24)
                    Group {
                        HStack {
                            Text(L10n.UserApp.Pay.Split.Open.amount)
                                .lucaFont(.luca16Medium)
                                .frame(maxWidth: .infinity, alignment: .leading)
                            Text(viewModel.invoiceAmount?.formattedAmountWithCode ?? "")
                                .lucaFont(.luca16Bold)
                        }
                        .padding(.bottom, 8)
                        if viewModel.discountCampaign != nil {
                        Text(L10n.UserApp.Pay.Split.Discount.hint)
                            .lucaFont(.luca14Medium)
                            .foregroundColor(Color(Asset.lucaDiscountGreen.color))
                            .padding(.bottom, 8)
                        }
                        Text(L10n.UserApp.Pay.Split.amount)
                            .lucaFont(.luca14Medium)
                    }
                    .frame(maxWidth: .infinity, alignment: .leading)
                    amountView
                }
            }
            Spacer()
            if let notice = viewModel.notice {
                Text(notice)
                    .foregroundColor(Color(Asset.lucaError.color))
                    .lucaFont(.luca14Medium)
            }
            Button(L10n.IOSApp.Pay.Amount.button(viewModel.enteredAmount.formattedAmountWithCode)) {
                viewModel.showKeyboard = false
                showTipView = true
            }
            .buttonStyle(LucaButton())
            .disabled(viewModel.isContinueButtonDisabled)
        }
        .handleAlerts($viewModel.alerts)
        .loadingIndicator(isLoading: $viewModel.isProcessing)
        .padding(padding)
        .appearance(.dark)
        .navigationBarTitle("", displayMode: .inline)
        .navBarCloseButton($openPaymentFlow)
        .overlay(
            ZStack {
                NavigationLink(
                    isActive: $showTipView,
                    destination: { LazyView { PaymentTipView(openPaymentFlow: $openPaymentFlow) } },
                    label: {}
                )

                // fixes iOS 14 bug that would cause views to immediatly pop after push if the total of navigationLinks equals 2; thus we need to add a few more invisible ones
                // https://forums.swift.org/t/14-5-beta3-navigationlink-unexpected-pop/45279/42
                NavigationLink(destination: EmptyView(), label: {})
                NavigationLink(destination: EmptyView(), label: {})
            }
        )
        .onChange(of: openPaymentFlow, perform: { openPaymentFlow in
            if !openPaymentFlow {
                viewModel.showKeyboard = false
            }
        })
        .onDisappear(perform: {
            print("iOS * APPEARANCE TEST AMOUNT FALSE")
            viewModel.showKeyboard = false
        })
        .onTapGesture {
            viewModel.showKeyboard = false
        }
    }

    @ViewBuilder
    private var amountView: some View {
        ZStack {
            AmountTextField("", text: $viewModel.splitAmountInCentsString, isFirstResponder: viewModel.showKeyboard)
                .frame(width: 1, height: 1, alignment: .center)
                .accentColor(.clear)
                amountTextView(for: viewModel.enteredSplitAmount.formattedAmountWithoutCode)
        }
    }

    private func amountTextView(for formattedAmount: String) -> some View {
        Text(formattedAmount)
            .font(SwiftUI.Font(FontFamily.Montserrat.medium.font(size: 60.0)))
            .scaledToFit()
            .minimumScaleFactor(0.5)
            .lineLimit(1)
            .onTapGesture {
                viewModel.showKeyboard = true
            }
    }
}

struct PaymentSplitAmountView_Previews: PreviewProvider {
    static var previews: some View {
        UIElementPreview(
            NavigationView {
                // swiftlint:disable:next line_length
                PaymentSplitAmountView(/*localCheckin: LocalCheckin(qrCode: "", location: Location(locationId: "", publicKey: "", radius: 0), selfCheckin: TableSelfCheckin(urlToParse: URL(string: "https://app-p1.luca-app.de/webapp/b638b6df-e84e-482a-8dbe-d94648e8e66b#eyJ0YWJsZSI6MX0/CWA1/CAESKQgBEghBQ01FIEluYxobU29tZSBTdHJlZXQgMiwgMTAyMDAgQmVybGluGnYIARJggwLMzE153tQwAOf2MZoUXXfzWTdlSpfS99iZffmcmxOG9njSK4RTimFOFwDh6t0Tyw8XR01ugDYjtuKwjjuK49Oh83FWct6XpefPi9Skjxvvz53i9gaMmUEc96pbtoaAGhCZHI628tkgNVeAuH69O13VIgYIARAEGHg")!)!),*/ openPaymentFlow: .constant(false))
            }
        )
    }
}
