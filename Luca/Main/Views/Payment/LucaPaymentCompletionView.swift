import SwiftUI

struct LucaPaymentCompletionView: View {
    @Binding var openPaymentFlow: Bool
    @Binding var openTipView: Bool

    var payment: PaymentFetchCheckoutResponse
    var body: some View {
        VStack {
            if payment.payment.isSuccessful {
                LucaPaymentSuccessView(viewModel: LucaPaymentSuccessViewModel(payment: payment.payment), openPaymentFlow: $openPaymentFlow)
            } else {
                LucaPaymentFailureView(openPaymentFlow: $openPaymentFlow, openTipView: $openTipView)
            }
        }
        .navigationBarBackButtonHidden(true)
        .appearance(.dark)
    }
}

struct LucaPaymentCompletionView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            LucaPaymentCompletionView(
                openPaymentFlow: .constant(true),
                openTipView: .constant(false),
                payment: PaymentFetchCheckoutResponse(
                    checkoutId: "",
                    locationId: "",
                    payment: Payment(
                        paymentId: "OLEHB",
                        locationName: "Restaurant XYZ",
                        locationId: "1",
                        amount: 70.32,
                        invoiceAmount: 123,
                        tipAmount: 11.72,
                        dateTime: 0.0,
                        status: .closed
                    )
                )
            )
            LucaPaymentCompletionView(
                openPaymentFlow: .constant(true),
                openTipView: .constant(false),
                payment: PaymentFetchCheckoutResponse(
                    checkoutId: "",
                    locationId: "",
                    payment: Payment(
                        locationName: "",
                        locationId: "2",
                        amount: 0.0,
                        invoiceAmount: 0.0,
                        tipAmount: 0.0,
                        dateTime: 0.0,
                        status: .unknown
                    )
                )
            )
        }
    }
}
