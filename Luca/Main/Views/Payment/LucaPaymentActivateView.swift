import SwiftUI

struct LucaPaymentActivateView: View {
    @StateObject private var viewModel = LucaPaymentActivateViewModel()
    @Environment(\.presenter) private var presenter

    var body: some View {
        VStack(spacing: 8.0) {
            ScrollView {
                VStack(spacing: 8.0) {
                    Group {
                        Text(L10n.UserApp.Pay.Info.Screen.headline)
                            .lucaFont(.luca16Bold)
                        Text(L10n.UserApp.Pay.Info.Screen.text)
                            .lucaFont(.luca14Medium)
                    }
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .layoutPriority(1.0)
                    Spacer(minLength: 40.0 - 16.0)
                    Image(uiImage: Asset.paymentActivateImage.image)
                }
            }
            .layoutPriority(1.0)
            Spacer()
                .frame(maxHeight: .infinity)
            Button(L10n.UserApp.Pay.Activate.button.uppercased()) {
                if let presenter = presenter.value {
                    viewModel.activate(on: presenter)
                }
            }
            .layoutPriority(1.0)
            .disabled(viewModel.isProcessing)
            .buttonStyle(LucaButton(color: .green))
        }
        .foregroundColor(.white)
        .padding(16.0)
        .loadingIndicator(isLoading: $viewModel.isProcessing)
        .handleAlerts($viewModel.alerts)
    }
}

struct LucaPaymentActivateView_Previews: PreviewProvider {
    static var previews: some View {
        UIElementPreview(LucaPaymentActivateView().background(SwiftUI.Color.black))
    }
}
