import SwiftUI

struct PaymentFlowRootView: View {
    @StateObject var viewModel: PaymentFlowViewModel
    @Binding var openPaymentFlow: Bool

    var body: some View {
        NavigationView {
            if viewModel.paymentLocationData == nil {
                PaymentScannerView(openPaymentFlow: $openPaymentFlow)
            } else {
                PaymentEnterAmountView(openPaymentFlow: $openPaymentFlow)
            }
        }
        .handleAlerts($viewModel.alerts)
        .environmentObject(viewModel)
    }
}

struct PaymentFlowRootView_Previews: PreviewProvider {
    static var previews: some View {
        PaymentFlowRootView(viewModel: PaymentFlowViewModel(paymentLocationData: nil), openPaymentFlow: .constant(false))
    }
}
