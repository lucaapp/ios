import SwiftUI
import Combine

struct PaymentEnterAmountView: View {
    @EnvironmentObject var viewModel: PaymentFlowViewModel
    @Binding var openPaymentFlow: Bool
    @State private var showTipView = false
    private var buttonText: String {
        if viewModel.isOpenPayment {
            return viewModel.amountToBePaid.formattedAmountWithCode
        }
        return viewModel.enteredAmount.formattedAmountWithCode
    }

    private let padding = 16.0

    var body: some View {
        VStack {
            ScrollView {
                VStack {
                    Text(L10n.UserApp.Pay.Amount.headline).lucaFont(.luca20Bold)
                        .padding(.bottom, 8)
                    Group {
                        Text(L10n.IOSApp.Pay.Amount.text1).font(SwiftUI.Font(FontFamily.Montserrat.medium.font(size: 14.0))) +
                        Text(viewModel.locationName).font(SwiftUI.Font(FontFamily.Montserrat.bold.font(size: 14.0))) +
                        Text(L10n.IOSApp.Pay.Amount.text2).font(SwiftUI.Font(FontFamily.Montserrat.medium.font(size: 14.0)))
                    }
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.bottom, 24)
                    amountView
                    LucaDiscountCampaignInfoView(discountCampaign: $viewModel.discountCampaign, style: .plain)
                        .show(isVisible: viewModel.isOpenPayment, remove: true)
                }
            }
            Spacer()
            if let notice = viewModel.notice {
                Text(notice)
                    .foregroundColor(Color(Asset.lucaError.color))
                    .lucaFont(.luca14Medium)
            }
            if viewModel.openRequest?.allowSplitPayment == true {
                Button(L10n.UserApp.Pay.Split.Initiate.button.uppercased()) {
                    viewModel.showKeyboard = false
                    viewModel.useSplitPayment = true
                }
                .buttonStyle(LucaButtonWithWhiteBorder())
                .padding(.bottom, 24)
            }
            Button(L10n.IOSApp.Pay.Amount.button(buttonText).uppercased()) {
                viewModel.showKeyboard = false
                showTipView = true
            }
            .buttonStyle(LucaButton())
            .disabled(viewModel.isContinueButtonDisabled)
        }
        .handleAlerts($viewModel.alerts)
        .loadingIndicator(isLoading: $viewModel.isProcessing)
        .padding(padding)
        .appearance(.dark)
        .navigationBarTitle("", displayMode: .inline)
        .navBarCloseButton($openPaymentFlow)
        .overlay(
            ZStack {
                NavigationLink(
                    isActive: $showTipView,
                    destination: { LazyView { PaymentTipView(openPaymentFlow: $openPaymentFlow) } },
                    label: {}
                )
                NavigationLink(
                    isActive: $viewModel.useSplitPayment,
                    destination: { LazyView { PaymentSplitAmountView(openPaymentFlow: $openPaymentFlow) } },
                    label: {}
                )

                // fixes iOS 14 bug that would cause views to immediatly pop after push if the total of navigationLinks equals 2; thus we need to add a few more invisible ones
                // https://forums.swift.org/t/14-5-beta3-navigationlink-unexpected-pop/45279/42
                NavigationLink(destination: EmptyView(), label: {})
                NavigationLink(destination: EmptyView(), label: {})
            }
        )
        .onChange(of: viewModel.useSplitPayment, perform: { useSplitPayment in
            if !useSplitPayment {
                viewModel.applyCurrentPaymentRequest()
            }
        })
        .onChange(of: openPaymentFlow, perform: { openPaymentFlow in
            if !openPaymentFlow {
                viewModel.showKeyboard = false
            }
        })
        .onFirstAppear(perform: {
            if #unavailable(iOS 15.0) {
                print("iOS 14 APPEARANCE TEST AMOUNT TRUE FIRST")
                viewModel.enterAmountViewVisible = true
            }
        })
        .onAppear {
            if #available(iOS 15.0, *) {
                print("iOS 15 APPEARANCE TEST AMOUNT TRUE")
                viewModel.enterAmountViewVisible = true
            }
        }
        .onChange(of: showTipView, perform: { _ in
            if #unavailable(iOS 15.0) {
                print("iOS 14 APPEARANCE TEST AMOUNT \(!showTipView)")
                viewModel.enterAmountViewVisible = !showTipView
            }
        })
        .onDisappear(perform: {
            print("iOS * APPEARANCE TEST AMOUNT FALSE")
            viewModel.enterAmountViewVisible = false
            viewModel.showKeyboard = false
        })
    }

    @ViewBuilder
    private var amountView: some View {
        ZStack {
            AmountTextField("", text: $viewModel.amountInCentsString, isFirstResponder: viewModel.showKeyboard)
                .frame(width: 1, height: 1, alignment: .center)
                .accentColor(.clear)
                .onFirstAppear {
                    print("APPEARANCE TEST AMOUNT KEYBOARD FIRST \(!viewModel.isOpenPayment)")
                    viewModel.showKeyboard = !viewModel.isOpenPayment
                }
                .onChange(of: viewModel.isOpenPayment) { _ in
                    print("APPEARANCE TEST AMOUNT KEYBOARD \(!viewModel.isOpenPayment)")
                    viewModel.showKeyboard = !viewModel.isOpenPayment
                }
            if let discountedAmount = viewModel.discountedUnsplitAmount, viewModel.isOpenPayment {
                VStack(spacing: 8) {
                    amountTextView(for: discountedAmount.formattedAmountWithoutCode)
                        .foregroundColor(Color(Asset.lucaDiscountGreen.color))
                    Text(L10n.IOSApp.Pay.Amount.currency).lucaFont(.luca14Medium)
                    Text(viewModel.enteredUnsplitAmount.formattedAmountWithCode)
                        .strikethrough()
                        .padding(.top, 8)
                        .lucaFont(.luca24Regular)
                }
            } else {
                amountTextView(for: viewModel.enteredUnsplitAmount.formattedAmountWithCode)
            }
        }
    }

    private func amountTextView(for formattedAmount: String) -> some View {
        Text(formattedAmount)
            .font(SwiftUI.Font(FontFamily.Montserrat.medium.font(size: 60.0)))
            .scaledToFit()
            .minimumScaleFactor(0.5)
            .lineLimit(1)
    }
}

struct AmountTextField: UIViewRepresentable {
    private let placeholder: String
    private let isFirstResponder: Bool
    @Binding public var text: String

    public init(_ placeholder: String, text: Binding<String>, isFirstResponder: Bool) {
        self.placeholder = placeholder
        self.isFirstResponder = isFirstResponder
        self._text = text
    }

    public func makeUIView(context: Context) -> UITextField {
        let textField = UITextField()
        textField.addTarget(context.coordinator, action: #selector(Coordinator.textViewDidChange), for: .editingChanged)
        textField.delegate = context.coordinator
        textField.textColor = .clear
        textField.backgroundColor = .clear
        textField.placeholder = placeholder
        textField.keyboardType = .numberPad
        textField.inputAccessoryView = UIView()
        textField.becomeFirstResponder()
        return textField
    }

    public func updateUIView(_ uiView: UITextField, context: Context) {
        uiView.text = text
        uiView.frame = CGRect(x: 0, y: 0, width: 1, height: 1)
        DispatchQueue.main.async {
            switch isFirstResponder {
            case true: uiView.becomeFirstResponder()
            case false: uiView.resignFirstResponder()
            }
        }
    }

    public func makeCoordinator() -> Coordinator {
        Coordinator($text)
    }

    public class Coordinator: NSObject, UITextFieldDelegate {
        var text: Binding<String>

        init(_ text: Binding<String>) {
            self.text = text
        }

        @objc public func textViewDidChange(_ textField: UITextField) {
            self.text.wrappedValue = textField.text ?? ""
        }
    }
}

struct PaymentEnterAmountView_Previews: PreviewProvider {
    static var previews: some View {
        UIElementPreview(
            NavigationView {
                // swiftlint:disable:next line_length
                PaymentEnterAmountView(/*localCheckin: LocalCheckin(qrCode: "", location: Location(locationId: "", publicKey: "", radius: 0), selfCheckin: TableSelfCheckin(urlToParse: URL(string: "https://app-p1.luca-app.de/webapp/b638b6df-e84e-482a-8dbe-d94648e8e66b#eyJ0YWJsZSI6MX0/CWA1/CAESKQgBEghBQ01FIEluYxobU29tZSBTdHJlZXQgMiwgMTAyMDAgQmVybGluGnYIARJggwLMzE153tQwAOf2MZoUXXfzWTdlSpfS99iZffmcmxOG9njSK4RTimFOFwDh6t0Tyw8XR01ugDYjtuKwjjuK49Oh83FWct6XpefPi9Skjxvvz53i9gaMmUEc96pbtoaAGhCZHI628tkgNVeAuH69O13VIgYIARAEGHg")!)!),*/ openPaymentFlow: .constant(false))
            }
        )
    }
}
