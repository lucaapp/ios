import SwiftUI

struct LucaPaymentView: View {
    @StateObject private var viewModel = LucaPaymentViewModel()
    @Environment(\.presenter) private var presenter

    @ViewBuilder
    var body: some View {
        VStack {
            if viewModel.isPaymentActivated == false {
                LucaPaymentActivateView()
            } else if viewModel.isPaymentActivated == true && viewModel.hasPaymentHistory {
                PaymentHistoryView(
                    openScanner: $viewModel.openPaymentFlow,
                    hasMorePaymentsAvailable: viewModel.hasMorePaymentsAvailable,
                    hasMorePaymentsLoadingFailed: $viewModel.hasMorePaymentsLoadingFailed,
                    payments: $viewModel.payments,
                    reloadTrigger: viewModel.triggerHistoryReload
                )
            } else if viewModel.isPaymentActivated == true && !viewModel.hasPaymentHistory {
                LucaPaymentEmptyView(openScanner: $viewModel.openPaymentFlow)
            }
        }
        .padding(.bottom, 16)
        .foregroundColor(SwiftUI.Color.white)
        .navigationBarTitle("luca Pay", displayMode: .inline)
        .handleAlerts($viewModel.alerts)
        .loadingIndicator(isLoading: $viewModel.isProcessing)
        .onAppear {
            viewModel.evaluateState()
        }
        .fullScreenCover(isPresented: $viewModel.openPaymentFlow) {
            LazyView {
                PaymentFlowRootView(
                    viewModel: PaymentFlowViewModel(paymentLocationData: nil),
                    openPaymentFlow: $viewModel.openPaymentFlow
                )
            }
        }
    }
}

struct LucaPaymentView_Previews: PreviewProvider {
    static var previews: some View {
        UIElementPreview(NavigationView {LucaPaymentView().background(SwiftUI.Color.black)})
    }
}
