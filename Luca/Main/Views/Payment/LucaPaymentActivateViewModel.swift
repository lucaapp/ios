import SwiftUI
import DependencyInjection
import RxSwift

class LucaPaymentActivateViewModel: BaseViewModel {
    func activate(on presenter: UIViewController) {
        _ = PaymentEnableInteractor(presenter: presenter)
            .interact()
            .copyError(to: alertsSubject)
            .copyRunningState(to: processCountBinding)
            .subscribe()
    }
}
