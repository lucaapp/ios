import SwiftUI
import RxSwift
import DependencyInjection
import Combine

typealias PaymentLocationData = LocalCheckin

class PaymentFlowViewModel: BaseViewModel {
    @InjectStatic(\.lucaPayment) private var lucaPayment
    @InjectStatic(\.backendAddressV4) private var backendAddress
    @InjectStatic(\.campaignService) private var campaignService
    @InjectStatic(\.timeProvider) private var timeProvider

    let tipPercentages: [Double] = [0, 0.1, 0.15, 0.2]

    // MARK: - Stored properties
    private var disposeBag: DisposeBag?

    @Published var maximumAmount: Money<EUR>?
    @Published var minimumAmount: Money<EUR>?

    /// true if user wants to pay a part of the full amount.
    @Published var useSplitPayment = false

    @Published var showKeyboard = true
    @Published var currentPayment: PaymentCreateCheckoutResponse?
    @Published var discountCampaign: PaymentCampaign?
    @Published var openRedirectURL: Bool = false
    @Published var tipSelected: Double

    /// Interpreted amount of entered string. Nil if user selected split payment.
    var enteredUnsplitAmount: Money<EUR> {
        amount(from: amountInCentsString) ?? Money(double: 0, currency: EUR())
    }

    /// Returns split amount entered by user, if split payment is selected.
    var enteredSplitAmount: Money<EUR> {
        amount(from: splitAmountInCentsString) ?? Money(double: 0, currency: EUR())
    }

    /// Contains amount that user wants to pay. It contains already split/unsplit logic
    var enteredAmount: Money<EUR> {
        if useSplitPayment {
            return enteredSplitAmount
        }
        return enteredUnsplitAmount
    }

    @Published var openRequest: PaymentFetchOpenRequestResponse? {
        didSet {
            showKeyboard = openRequest == nil
        }
    }

    var amountInCentsString: String = "0" {
        didSet {
            currentPayment = nil
        }
    }

    var splitAmountInCentsString: String = "0" {
        didSet {
            currentPayment = nil
        }
    }

    var enterAmountViewVisible: Bool = false {
        didSet {
            if enterAmountViewVisible {
                checkOpenPayments()
            } else {
                disposeBag = nil
            }
        }
    }

    // MARK: - Computed properties
    var notice: String? {
        if useSplitPayment {
            return notice(for: enteredSplitAmount)
        }
        return notice(for: enteredUnsplitAmount)
    }
    var isContinueButtonDisabled: Bool {
        notice != nil
    }
    var isOpenPayment: Bool {
        openRequest != nil
    }

    var locationName: String {
        paymentLocationData?.location.formattedName ?? ""
    }

    var paymentLocationData: PaymentLocationData? {
        didSet {
            checkDiscountCampaign()
        }
    }

    /// Contains discounted amount if a discount campaign present.
    ///
    /// It will automatically select between split and unsplit amounts
    var discountedAmount: Money<EUR>? {
        discountCampaign?.discount(amount: enteredAmount)
    }

    /// Contains discounted amount for the unsplit amount if a discount campaign present.
    var discountedUnsplitAmount: Money<EUR>? {
        discountCampaign?.discount(amount: enteredUnsplitAmount)
    }

    /// Initial amount on the open payment request. Nil if there is no open payment request.
    var invoiceAmount: Money<EUR>? {
        if let openRequest = openRequest {
            if let openAmount = openRequest.openAmount?.money(with: EUR()),
               useSplitPayment {
                return openAmount
            }
            return openRequest.invoiceAmount.money(with: EUR())
        }
        return nil
    }

    /// Contains discounted amount if a discount campaign present or the invoice amount.
    var amountToBePaid: Money<EUR> {
        return discountedAmount ?? enteredAmount
    }
    var tipAmount: Money<EUR> {
        // It has to be rounded because we pass this amount separately from the actual invoice amount.
        // Without the rounding here we would loose a cent on the display
        // We explicitly don't use the amountToBePaid property because it contains discount which shouldn't
        (enteredAmount * tipSelected).rounded
    }
    var amountToBePaidWithTip: Money<EUR> {
        tipAmount + amountToBePaid
    }
    var formattedAmountWithTip: String {
        amountToBePaidWithTip.formattedAmountWithCode
    }

    init(paymentLocationData: PaymentLocationData?) {
        self.paymentLocationData = paymentLocationData
        tipSelected = 0.1
        super.init()
        fetchPaymentLimits()
        checkOpenPayments()
        checkDiscountCampaign()
        _ = self.objectWillChange
    }

    func amount(from amountString: String) -> Money<EUR>? {
        let filteredAmount = !amountString.isEmpty ? amountString : "0"
        guard let amountInCents = Money(stringWithLocale: filteredAmount, currency: EUR()) else { return nil }
        let amountInEuros = amountInCents / 100
        return amountInEuros
    }

    private func checkOpenPayments() {
        guard let paymentLocationData = paymentLocationData,
              let tableCheckin = paymentLocationData.selfCheckin as? TableSelfCheckin else { return }

        let tableId = tableCheckin.additionalData?.tableIdentifier
        let newDisposeBag = DisposeBag()

        Observable<Int>.timer(.seconds(0), period: .seconds(5), scheduler: LucaScheduling.backgroundScheduler)
            .flatMapFirst { [weak self] _ -> Observable<PaymentFetchOpenRequestResponse?> in
                guard let self = self else { throw RxError.unknown }
                return self.lucaPayment.fetchOpenRequest(forLocationId: paymentLocationData.location.locationId, atTable: tableId)
                    .asObservable()
                    .copyError(to: self.alertsSubject)
            }
            .observe(on: MainScheduler.instance)
            .do(onNext: { [weak self] in
                self?.apply(openPaymentRequest: $0)
            })
            .retry(delay: .seconds(5), scheduler: MainScheduler.instance)
            .subscribe()
            .disposed(by: newDisposeBag)
        disposeBag = newDisposeBag
    }

    private func apply(openPaymentRequest: PaymentFetchOpenRequestResponse?) {
        guard let response = openPaymentRequest else {
            self.openRequest = nil
            return
        }
        let invoice = response.openAmount ?? response.invoiceAmount
        let money100 = Money(double: invoice, currency: EUR()) * 100.0
        let roundedMoney = money100.rounded

        self.amountInCentsString = String(roundedMoney.toInt32)
        self.openRequest = response
        self.objectWillChange.send()
    }

    func applyCurrentPaymentRequest() {
        apply(openPaymentRequest: openRequest)
    }

    func pullTipPreference() {
        _ = lucaPayment.pullTipPreference()
            .observe(on: MainScheduler.instance)
            .subscribe(onSuccess: { percentage in
                self.tipSelected = percentage == 0.0 ? 0.1 : percentage
        })
    }

    func saveTipPreference() async {
        try? await lucaPayment
            .pullTipPreference()
            .observe(on: MainScheduler.instance)
            .flatMapCompletable {
                if $0 != self.tipSelected {
                    self.currentPayment = nil
                    return self.lucaPayment.setTipPreference(value: self.tipSelected)
                }
                return .empty()
            }.task()
        }

    func makePayment() async {
        if let currentPayment = currentPayment {
            handle(payment: currentPayment)
            return
        }
        guard let paymentLocationData = paymentLocationData else { return }
        var table = ""
        if let tableCheckin = paymentLocationData.selfCheckin as? TableSelfCheckin,
           let tableNumber = tableCheckin.additionalData?.tableIdentifier {
            table = tableNumber
        }

        let createPayment: Single<PaymentCreateCheckoutResponse>
        if let openRequest = openRequest {
            if useSplitPayment {
                createPayment = lucaPayment.makePayment(for: openRequest, partialAmount: enteredSplitAmount, tip: tipAmount, table: table)
            } else {
                createPayment = lucaPayment.makePayment(for: openRequest, tip: tipAmount, table: table)
            }
        } else {
            createPayment = lucaPayment.makePayment(
                locationId: paymentLocationData.location.locationId,
                invoice: enteredUnsplitAmount,
                tip: tipAmount,
                table: table
            )
        }

        _ = try? await createPayment
        .observe(on: MainScheduler.instance)
        .do(onSuccess: {
            self.handle(payment: $0)
        })
        .copyRunningState(to: processCountBinding)
        .copyError(to: alertsSubject)
        .debug("PAYMENT.make")
        .task()
    }

    func formatTip(tipPercent: Double) -> String {
        let amountWithTip = tipPercent * enteredAmount
        return amountWithTip.formattedAmountWithCode
    }

    private func handle(payment: PaymentCreateCheckoutResponse) {
        self.currentPayment = payment
        self.openRedirectURL = true
    }

    private func fetchPaymentLimits() {
        _ = lucaPayment.fetchLimits()
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { [weak self] limits in
                self?.minimumAmount = Money(double: limits.minInvoiceAmount, currency: EUR())
                self?.maximumAmount = Money(double: limits.maxInvoiceAmount, currency: EUR())
            })
            .copyRunningState(to: processCountBinding)
            .copyError(to: alertsSubject)
            .subscribe()
    }

    private func notice(for amount: Money<EUR>) -> String? {
        guard let minimumAmount = minimumAmount,
              let maximumAmount = maximumAmount else {
            return nil
        }

        if let openAmount = openRequest?.openAmount?.money(with: EUR()),
           openRequest?.allowSplitPayment == true {
            if amount > openAmount {
                return L10n.UserApp.Error.Split.Exceeds.amount
            }
            let newMinimumAmount = min(minimumAmount, openAmount)
            if amount < newMinimumAmount {
                return L10n.IOSApp.Pay.Amount.minimum(newMinimumAmount.formattedAmountWithCode)
            }
        } else {
            if amount < minimumAmount {
                return L10n.IOSApp.Pay.Amount.minimum(minimumAmount.formattedAmountWithCode)
            } else if amount > maximumAmount {
                return L10n.IOSApp.Pay.Amount.maximum(maximumAmount.formattedAmountWithCode)
            }
        }
        return nil
    }

    private func checkDiscountCampaign() {
        guard let paymentLocationData = paymentLocationData else { return }
        _ = campaignService.checkCampaign(for: paymentLocationData.location.locationId, type: .lucaDiscount, date: timeProvider.now)
            .observe(on: MainScheduler.instance)
            .subscribe(onSuccess: { [weak self] in
                self?.discountCampaign = $0
            })
    }
}
