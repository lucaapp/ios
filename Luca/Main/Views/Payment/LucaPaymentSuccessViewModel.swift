import Foundation
import RxSwift
import DependencyInjection

class LucaPaymentSuccessViewModel: BaseViewModel {
    @InjectStatic(\.lucaPayment) private var paymentService

    let payment: Payment

    var formattedAmount: String {
        payment.amount.money(with: EUR()).formattedAmountWithCode
    }

    init(payment: Payment) {
        self.payment = payment
        super.init()
    }

    func checkout(completion: @escaping () -> Void) {
           _ = LocalCheckoutInteractor().interact()
               .observe(on: MainScheduler.asyncInstance)
               .copyError(to: alertsSubject)
               .copyRunningState(to: processCountBinding)
               .do(onCompleted: { completion() })
               .subscribe()
    }
}
