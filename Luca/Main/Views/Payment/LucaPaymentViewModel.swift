import SwiftUI
import DependencyInjection
import RxSwift
import Combine

class LucaPaymentViewModel: BaseViewModel {
    @InjectStatic(\.lucaPayment) private var payment

    @Published var openPaymentFlow: Bool = false
    @Published var isPaymentActivated: Bool?
    @Published var hasPaymentHistory: Bool = false
    @Published var hasMorePaymentsAvailable: Bool = false
    @Published var hasMorePaymentsLoadingFailed: Bool = false
    @Published var payments: [Payment] = [] {
        didSet {
            self.hasPaymentHistory = !payments.isEmpty
        }
    }
    private var paymentHistoryCursor: String? {
        didSet {
            self.hasMorePaymentsAvailable = paymentHistoryCursor != nil
        }
    }
    var triggerHistoryReload = PassthroughSubject<Void, Never>()
    private var bag = Set<AnyCancellable>()

    override init() {
        super.init()
        triggerHistoryReload.sink { [weak self] _ in
            self?.loadMorePayments()
        }
        .store(in: &bag)
    }

    private var disposeBag: DisposeBag?

    func evaluateState() {
        let newDisposeBag = DisposeBag()
        processCount += 1
        Observable.combineLatest(payment.isUserActivatedChanges, $openPaymentFlow.eraseToAnyPublisher().asObservable())
            .map { $0.0 }
            .observe(on: MainScheduler.asyncInstance)
            .flatMapFirst { [weak self] isActivated -> Completable in
                guard let self = self else {
                    throw RxError.unknown
                }
                self.processCount -= 1

                if isActivated {
                    return self.payment.fetchPaymentHistory()
                        .map { $0.first }
                        .observe(on: MainScheduler.asyncInstance)
                        .copyRunningState(to: self.processCountBinding)
                        .do(onSuccess: { historyPage in
                            self.payments = historyPage?.payments ?? []
                            self.paymentHistoryCursor = historyPage?.cursor
                            self.isPaymentActivated = true
                        })
                        .do(onError: { _ in
                            self.isPaymentActivated = nil
                        })
                        .asCompletable()
                } else {
                    self.isPaymentActivated = false
                    self.hasPaymentHistory = false
                }
                return .empty()
            }
            .copyError(to: alertsSubject)
            .subscribe()
            .disposed(by: newDisposeBag)
        disposeBag = newDisposeBag
    }

    func loadMorePayments() {
        guard let cursor = paymentHistoryCursor else { return }
        _ = payment.fetchPaymentHistory(cursor: cursor)
            .observe(on: MainScheduler.asyncInstance)
            .do(onSuccess: { pages in
                guard let page = pages.first else { return }
                self.payments.append(contentsOf: page.payments)
                self.paymentHistoryCursor = page.cursor
                self.hasMorePaymentsLoadingFailed = false
            })
            .do(onError: { _ in
                self.hasMorePaymentsLoadingFailed = true
            })
            .copyError(to: alertsSubject)
            .subscribe()
    }
}
