import SwiftUI

struct PaymentRevocationView: View {
    private let revocationCode: String
    @State private var showShareSheet = false

    init(revocationCode: String) {
        self.revocationCode = revocationCode
    }

    var body: some View {
        VStack {
            VStack(alignment: .leading, spacing: 24) {
                Text(L10n.UserApp.Pay.Revocation.Notification.title)
                    .lucaFont(.luca16Bold)
                Text(L10n.UserApp.Pay.Revocation.Notification.text)
                    .lucaFont(.luca14Medium)
            }
            .padding(.horizontal, 16)
            .padding(.top, 24)
            .padding(.bottom, 24)

            VStack(alignment: .center, spacing: 8) {
                Text(L10n.UserApp.Pay.Revocation.Code.label)
                    .lucaFont(.luca14Medium)
                HStack(spacing: 16) {
                    Text(revocationCode)
                        .lucaFont(.luca24Bold)
                    Button(action: { self.showShareSheet = true }) {
                        Image(uiImage: Asset.share.image)
                    }
                }
            }
            Spacer()
        }
        .navigationBarTitle(L10n.IOSApp.Payment.Revocation.Navbar.title, displayMode: .inline)
        .sheet(isPresented: $showShareSheet) {
            ShareSheet(activityItems: [revocationCode])
        }
    }
}

struct PaymentRevocationView_Previews: PreviewProvider {
    static var previews: some View {
        UIElementPreview(
            PaymentRevocationView(revocationCode: "TestRevocation").environment(\.presenter, Weak(UIViewController()))
        )
    }
}
