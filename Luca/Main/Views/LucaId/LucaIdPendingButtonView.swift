import UIKit
import LucaUIComponents

class LucaIdPendingButtonView: UIView {

    @IBOutlet weak var wrapper: UIView!
    @IBOutlet weak var identLabel: Luca24PtBoldLabel!

    func setup(ident: String, tapGestureRecognizer: UITapGestureRecognizer, longPressGestureRecognizer: UILongPressGestureRecognizer) {
        setupUI()
        addGestureRecognizer(tapGestureRecognizer)
        addGestureRecognizer(longPressGestureRecognizer)
        identLabel.text = ident
    }

    private func setupUI() {
        wrapper.layer.cornerRadius = 10
        wrapper.borderColor = UIColor.white
        wrapper.layer.borderWidth = 2
    }

}
