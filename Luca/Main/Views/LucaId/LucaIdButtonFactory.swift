import UIKit

class LucaIdButtonFactory {

    static func setupAddIdInfoButtonView(tapGestureRecognizer: UITapGestureRecognizer?) -> UIView {
        let idButtonView: LucaIdInfoButtonView = LucaIdInfoButtonView.fromNib()

        idButtonView.setup(image: Asset.addIDCard.image,
                           title: L10n.IOSApp.Id.addIdPromptHeadline,
                           description: L10n.IOSApp.Id.addIdPromptText,
                           tapGestureRecognizer: tapGestureRecognizer)

        return idButtonView
    }

    static func setupQueuedIdInfoView() -> UIView {
        let idButtonView: LucaIdInfoButtonView = LucaIdInfoButtonView.fromNib()

        idButtonView.setup(image: Asset.spinningWheel.image,
                           title: L10n.IOSApp.Id.queuedIdPromptHeadline,
                           description: L10n.IOSApp.Id.queuedIdPromptText,
                           tapGestureRecognizer: nil)

        return idButtonView
    }

    static func setupPendingIdInfoView(ident: String, tapGestureRecognizer: UITapGestureRecognizer, longPressGestureRecognizer: UILongPressGestureRecognizer) -> UIView {
        let idButtonView: LucaIdPendingButtonView = LucaIdPendingButtonView.fromNib()
        idButtonView.setup(ident: ident,
                           tapGestureRecognizer: tapGestureRecognizer,
                           longPressGestureRecognizer: longPressGestureRecognizer)
        return idButtonView
    }

}
