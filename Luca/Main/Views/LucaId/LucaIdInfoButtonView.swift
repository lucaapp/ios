import UIKit

class LucaIdInfoButtonView: UIView {

    @IBOutlet weak var wrapper: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    func setup(image: UIImage,
               title: String,
               description: String,
               tapGestureRecognizer: UITapGestureRecognizer?) {
        imageView.image = image
        titleLabel.text = title
        descriptionLabel.text = description

        if let tapGestureRecognizer = tapGestureRecognizer {
            addGestureRecognizer(tapGestureRecognizer)
        }

        setupUI()
    }

    private func setupUI() {
        wrapper.layer.cornerRadius = 10
        wrapper.borderColor = Asset.lucaBlue.color
        wrapper.layer.borderWidth = 2
    }

}
