import UIKit
import RxSwift
import DependencyInjection
import LucaUIComponents

class LucaIDView: DocumentView, HorizontalGroupable {

    @InjectStatic(\.dgcVerificationService) private var dgcVerificationService

    @IBOutlet weak var idCardNameTitleLabel: Luca14PtBoldBlackLabel!
    @IBOutlet weak var idCardNameLabel: Luca20PtMediumLabel!
    @IBOutlet weak var dateOfBirthLabel: Luca20PtMediumLabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var faceImageView: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var blurredLabel: Luca14PtBoldBlackLabel!
    @IBOutlet weak var blurredLabelImageView: UIImageView!

    @IBOutlet weak var deleteButtonStackView: UIStackView!
    @IBOutlet weak var nameStackView: UIStackView!
    @IBOutlet weak var dateOfBirthStackView: UIStackView!

    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var expandView: UIView!
    @IBOutlet weak var collapsedView: UIView!

    let groupedKey = "LucaIDView"

    override var viewsToCheck: [ViewWidthDefining] { [] }

    override var stackViews: [UIStackView] {
        isExpanded ? [deleteButtonStackView, nameStackView, dateOfBirthStackView] : []
    }

    override internal func setup() {
        self.useHorizontalLayout = false
        verificationButton.setTitle("", for: .normal)
        verificationButton.accessibilityLabel = L10n.IOSApp.Id.Card.Tooltip.accessibility
        verificationButton.isHidden = true
        guard let lucaID = document as? LucaID else { return }

        backgroundColor = .clear
        dateOfBirthLabel.text = lucaID.dateOfBirth
        idCardNameLabel.text = lucaID.fullName

        wrapperView.layer.cornerRadius = 8
        if let image = lucaID.image {
            faceImageView.image = image
        }

        faceImageView.isAccessibilityElement = false

        deleteButton.addTarget(self, action: #selector(didPressDelete(sender:)), for: .touchUpInside)
        deleteButton.layer.borderWidth = 1
        deleteButton.layer.borderColor = Asset.lucaBlack.color.cgColor
        deleteButton.layer.cornerRadius = 16
        deleteButton.accessibilityLabel = L10n.IOSApp.Test.Delete.title

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.isEnabled = true
        tapGestureRecognizer.cancelsTouchesInView = true
        addGestureRecognizer(tapGestureRecognizer)

        expandView.isHidden = !isExpanded
        collapsedView.isHidden = isExpanded

        backgroundImageView.image = isExpanded ? Asset.idBackgroundBigVertical.image : Asset.idBackgroundSmall.image
        verificationButton.isEnabled = true
        self.warningPressed = {
            let alert = UIAlertController.infoAlert(
                title: "",
                message: L10n.IOSApp.Id.Card.Verification.tooltip)

            UIViewController.visibleViewController?.present(alert, animated: true, completion: nil)
        }

        initObserver()
        updateStackViews()
        blurredLabel.isHidden = false
        let blur = blurredLabel.blurredTextAsImage()
        blurredLabelImageView.image = blur
        blurredLabel.isHidden = true
        setupVerificationButton(isVerified: true)
    }

    override func setupVerificationButton(isVerified: Bool) {
        super.setupVerificationButton(isVerified: isVerified)
        verificationButton.isEnabled = true
    }

}

// MARK: - DocumentViewProtocol

extension LucaIDView: DocumentViewProtocol {
    public static func createView(document: Document, owner: Person, isExpanded: Bool, delegate: DocumentViewDelegate?) -> DocumentView? {
        guard let document = document as? LucaID else { return nil }

        let itemView: LucaIDView = LucaIDView.fromNib()
        itemView.delegate = delegate
        itemView.isExpanded = isExpanded
        itemView.document = document
        itemView.isAccessibilityElement = !isExpanded
        itemView.accessibilityTraits = .button
        itemView.accessibilityLabel = L10n.IOSApp.Test.Show.title
        itemView.owner = owner

        return itemView
    }
}

// MARK: - Actions

extension LucaIDView {
    @objc
    private func viewTapped(_ sender: UITapGestureRecognizer) {
        guard let document = document else { return }
        delegate?.didSelect(self, document: document)
    }
    @objc

    private func didPressDelete(sender: UIButton) {
        if let lucaID = document {
            delegate?.didTapDelete(self, for: lucaID)
        }
    }
}
