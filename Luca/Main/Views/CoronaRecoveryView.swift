import UIKit

class CoronaRecoveryView: DocumentView {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var validUntilLabel: UILabel!
    @IBOutlet weak var validDurationLabel: UILabel!
    @IBOutlet weak var validFromLabel: UILabel!
    @IBOutlet weak var labLabel: UILabel!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var qrCodeImageView: UIImageView!

    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var expandView: UIView!

    @IBOutlet weak var validUntilStackView: UIStackView!
    @IBOutlet weak var durationStackView: UIStackView!
    @IBOutlet weak var validFromStackView: UIStackView!
    @IBOutlet weak var issuerStackView: UIStackView!
    @IBOutlet weak var dateOfBirthStackView: UIStackView!

    override var viewsToCheck: [ViewWidthDefining] {
        isExpanded ? [nameLabel, validUntilLabel, durationStackView, labLabel, dateOfBirthLabel, validFromLabel] : [nameLabel, validUntilLabel, durationStackView]
    }

    override var stackViews: [UIStackView] {
        isExpanded ? [validUntilStackView, durationStackView, validFromStackView, issuerStackView, dateOfBirthStackView] : [validUntilStackView, durationStackView]
    }

    override internal func setup() {
        guard let recovery = document as? Recovery else { return }

        wrapperView.layer.cornerRadius = 8
        wrapperView.backgroundColor = recovery.isValid ? Asset.lucaEMGreen.color : Asset.lucaGrey.color

        nameLabel.text = owner.formattedName
        validUntilLabel.text = recovery.validUntilDate.formattedDate
        validUntilLabel.accessibilityLabel = recovery.validUntilDate.accessibilityDate
        validDurationLabel.text = recovery.validFromDate.durationSinceDate
        validFromLabel.text = recovery.validFromDate.formattedDateTime
        validFromLabel.accessibilityLabel = recovery.validFromDate.accessibilityDate
        labLabel.text = recovery.laboratory.replacingOccurrences(of: "\\s[\\s]+", with: "\n", options: .regularExpression, range: nil)
        dateOfBirthLabel.text = recovery.dateOfBirth.formattedDate
        dateOfBirthLabel.accessibilityLabel = recovery.dateOfBirth.accessibilityDate

        qrCodeImageView.layer.cornerRadius = 8
        setupQRCodeImage(for: recovery)

        qrCodeImageView.isAccessibilityElement = true
        qrCodeImageView.accessibilityLabel = L10n.IOSApp.Contact.Qr.Accessibility.qrCode

        deleteButton.addTarget(self, action: #selector(didPressDelete(sender:)), for: .touchUpInside)
        deleteButton.layer.borderWidth = 1
        deleteButton.layer.borderColor = UIColor.black.cgColor
        deleteButton.layer.cornerRadius = 16
        deleteButton.accessibilityLabel = L10n.IOSApp.Test.Delete.title

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.isEnabled = true
        tapGestureRecognizer.cancelsTouchesInView = true
        addGestureRecognizer(tapGestureRecognizer)

        recentDocumentIndicator.isHidden = !recovery.isRecent
        disclosureIndicator.isHidden = isExpanded
        expandView.isHidden = !self.isExpanded

        position = .single

        initObserver()
        updateStackViews()
    }

    private func setupQRCodeImage(for recovery: Recovery) {
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let image = QRCodeGenerator.generateQRCode(string: recovery.originalCode)
        if let scaledQr = image?.transformed(by: transform) {
            qrCodeImageView.image = UIImage(ciImage: scaledQr)
        }
    }
}

// MARK: - DocumentViewProtocol

extension CoronaRecoveryView: DocumentViewProtocol {
    public static func createView(document: Document, owner: Person, isExpanded: Bool, delegate: DocumentViewDelegate?) -> DocumentView? {
        guard let document = document as? Recovery else { return nil }

        let itemView: CoronaRecoveryView = CoronaRecoveryView.fromNib()
        itemView.delegate = delegate
        itemView.owner = owner
        itemView.isExpanded = isExpanded
        itemView.document = document
  		itemView.isAccessibilityElement = !isExpanded
        itemView.accessibilityTraits = .button
        itemView.accessibilityLabel = L10n.IOSApp.Test.Show.title

        return itemView
    }
}

// MARK: - Actions

extension CoronaRecoveryView {
    @objc
    private func viewTapped(_ sender: UITapGestureRecognizer) {
        guard let document = document else { return }
        delegate?.didSelect(self, document: document)
    }

    @objc
    private func didPressDelete(sender: UIButton) {
        if let recovery = self.document {
            delegate?.didTapDelete(self, for: recovery)
        }
    }
}
