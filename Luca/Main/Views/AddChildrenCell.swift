import UIKit
import LucaUIComponents

class AddChildrenCell: UITableViewCell {
    @IBOutlet weak var theTextLabel: Luca14PtBoldLabel!

    func setup() {
        self.theTextLabel.text = L10n.IOSApp.Checkin.Kids.addAnother.uppercased()
    }
}
