import UIKit
import LucaUIComponents
import RxSwift
import RxCocoa
import AVFoundation
import DependencyInjection

class LucaQRScanner: DesignableView {

    // MARK: - dependencies

    @InjectStatic(\.qrProcessingService) private var qrProcessingService

    // MARK: - UI elements

    lazy var qrScannerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Asset.qrCodeScanner.image

        return imageView
    }()

    lazy var cameraImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Asset.camera.image

        return imageView
    }()

    lazy var qrScannerView: DesignableView = {
        let view = DesignableView()

        return view
    }()

    lazy var activateScannerView: DesignableView = {
        let view = DesignableView()

        return view
    }()

    lazy var activateScannerButton: UIButton! = {
        let button = UIButton()
        button.setTitle("", for: .normal)

        return button
    }()

    lazy var activateScannerLabel: Luca14PtLabel = {
        let label = Luca14PtLabel()
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 0

        return label
    }()

    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.spacing = 24

        return stackView
    }()

    // MARK: - properties

    lazy var qrScannerViewController: QRScannerViewController = {
        let view = QRScannerViewController()

        return view
    }()

    // MARK: - init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.clear.cgColor
        layer.cornerRadius = 10
    }
}

// MARK: - setup

extension LucaQRScanner {
    private func setup() {
        setupUI()
        setupConstraints()
        setupAccessibility()
    }

    private func setupUI() {
        clipsToBounds = true
        backgroundColor = .clear

        addSubview(qrScannerView)
        addSubview(activateScannerView)
        addSubview(qrScannerImageView)
        activateScannerView.addSubview(stackView)
        activateScannerView.addSubview(activateScannerButton)

        stackView.addArrangedSubview(cameraImageView)
        stackView.addArrangedSubview(activateScannerLabel)

        activateScannerView.isHidden = true
        qrScannerView.isHidden = true

        activateScannerLabel.text = L10n.IOSApp.Camera.Access.Activate.label.string
    }

    private func setupConstraints() {
        aspectRatio(1).isActive = true

        qrScannerView.pinToSuperView()
        activateScannerView.pinToSuperView()
        qrScannerImageView.pinToSuperView()

        activateScannerLabel.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        cameraImageView.translatesAutoresizingMaskIntoConstraints = false
        cameraImageView.widthAnchor.constraint(equalToConstant: 80.0).isActive = true
        cameraImageView.heightAnchor.constraint(equalToConstant: 80.0).isActive = true

        activateScannerButton.pinToSuperView()

        stackView.centerXAnchor.constraint(equalTo: activateScannerView.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: activateScannerView.centerYAnchor).isActive = true
    }

    private func setupQRScannerViewController() {
        guard !qrScannerViewController.view.isDescendant(of: qrScannerView) else { return }

        qrScannerView.addSubview(qrScannerViewController.view)
        qrScannerViewController.view.pinToSuperView()
    }

    private func setupAccessibility() {
        qrScannerView.isAccessibilityElement = true
        qrScannerView.accessibilityLabel = L10n.IOSApp.Contact.Qr.Accessibility.qrCodeScanner
        activateScannerLabel.accessibilityTraits = .button
        activateScannerButton.isAccessibilityElement = false
    }
}

// MARK: - public obserables

extension LucaQRScanner {
    var scan: Observable<String> {
        qrScannerViewController.scan.do(onSubscribed: { [weak self] in
            self?.showScanner(display: true)
        }, onDispose: { [weak self] in
            self?.showScanner(display: false)
        })
        .asObservable()
    }

    var onActivateButton: Observable<Void> {
        activateScannerButton.rx.tap.asObservable()
    }
}

// MARK: - public functions

extension LucaQRScanner {
    func showScanner(display: Bool) {
        if display {
            if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
                setupQRScannerViewController()
                self.qrScannerView.isHidden = false
                self.activateScannerView.isHidden = true
            } else {
                self.qrScannerView.isHidden = true
                self.activateScannerView.isHidden = false
            }
        }
    }
}
