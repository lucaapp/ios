import UIKit

protocol HorizontalGroupable {
    var groupedKey: String { get }
}

enum HorizontalDocumentListViewItemPosition {
    case leading
    case middle
    case trailing
    case single
}

class HorizontalDocumentListView: UIView {
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isPagingEnabled = true
        scrollView.bounces = false
        scrollView.delegate = self

        return scrollView
    }()

    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.spacing = 8

        return stackView
    }()

    lazy var pageControl: UIPageControl? = {
        guard documentViewList.count > 1 else { return nil}

        let pageControl = UIPageControl()
        pageControl.isUserInteractionEnabled = false
        pageControl.numberOfPages = documentViewList.count

        return pageControl
    }()

    var heightConstraint: NSLayoutConstraint?
    var isExpanded: Bool = false

    var documentViewList: [DocumentView]

    init(views: [DocumentView]) {
        documentViewList = views

        super.init(frame: CGRect.zero)

        setupStackView()
        setupUI()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        updateScrolViewHeight()
    }
}

extension HorizontalDocumentListView {
    func scroll(to document: Document) {
        let position = getPosition(of: document)
        guard position > 0 else { return }

        let itemOffset = scrollView.contentSize.width / CGFloat(documentViewList.count)
        let positionOffset = position == documentViewList.count ? 24 : 8
        let offset = CGPoint(x: itemOffset * CGFloat(position) - CGFloat(positionOffset), y: 0)

        scrollView.setContentOffset(offset, animated: true)
        pageControl?.currentPage = Int(position)
    }

    private func getPosition(of document: Document) -> Int {
        return stackView.arrangedSubviews.firstIndex(where: { view in
            guard let view = view as? CoronaVaccineItemView else { return false }
            return view.document?.identifier == document.identifier
        }) ?? 0
    }
}

extension HorizontalDocumentListView {
    private func setupUI() {
        clipsToBounds = true
        backgroundColor = .clear

        heightConstraint = self.scrollView.heightAnchor.constraint(equalToConstant: 0)
        heightConstraint?.priority = UILayoutPriority(999)
        heightConstraint?.isActive = true

        addSubview(scrollView)
        if let pageControl = pageControl {
            addSubview(pageControl)
        }
        scrollView.addSubview(stackView)
    }

    private func updateScrolViewHeight() {
        DispatchQueue.main.async {
            let height = self.stackView.arrangedSubviews.map {$0.frame.size.height}.max() ?? 0.0
            self.heightConstraint?.constant = height
        }
    }

    private func setupStackView() {
        stackView.removeAllArrangedSubviews()

        for (index, item) in documentViewList.enumerated() {
            let view = item
            let position: HorizontalDocumentListViewItemPosition = documentViewList.count == 1 ? .single : index == 0 ? .leading : index == documentViewList.count - 1 ? .trailing : .middle
            view.position = position

            var width = UIScreen.main.bounds.size.width
            if position != .single {
                width -= 16
            }
            let anchor = view.widthAnchor.constraint(equalToConstant: width)
            anchor.priority = UILayoutPriority(1000)
            anchor.isActive = true
            stackView.addArrangedSubview(view)
        }

        updateScrolViewHeight()
    }

    private func setupConstraints() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true

        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0).isActive = true
        stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0).isActive = true
        stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0).isActive = true
        stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0).isActive = true
        stackView.heightAnchor.isEqual(scrollView.heightAnchor)

        if let pageControl = pageControl {
            pageControl.translatesAutoresizingMaskIntoConstraints = false
            pageControl.topAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0).isActive = true
            pageControl.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
            pageControl.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        } else {
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        }
    }
}

extension HorizontalDocumentListView: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / (scrollView.frame.size.width - 32)
        pageControl?.currentPage = Int(pageNumber)
    }
}
