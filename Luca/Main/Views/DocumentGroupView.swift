import UIKit
import LucaUIComponents

class DocumentGroupView: UIView {

    // MARK: - UI elements

    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 0

        return stackView
    }()

    // MARK: - Properties

    var documentViewList: [DocumentView]? {
        didSet {
            setupStackView()
        }
    }

    var person: Person

    init(person: Person, views: [DocumentView]) {
        self.person = person

        super.init(frame: CGRect.zero)

        setupUI()
        setupConstraints()

        self.documentViewList = views

        setupStackView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Setup

extension DocumentGroupView {
    private func setupUI() {
        clipsToBounds = true
        backgroundColor = .clear

        addSubview(stackView)
    }

    private func setupStackView() {
        stackView.removeAllArrangedSubviews()

        guard let documentViewList = documentViewList else { return }
        DocumentViewFactory.group(views: documentViewList).forEach { stackView.addArrangedSubview($0) }
    }

    private func setupConstraints() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true
    }
}
