import UIKit

class CoronaTestView: DocumentView {

    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var durationSinceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var labLabel: UILabel!
    @IBOutlet weak var doctorLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var qrCodeImageView: UIImageView!
    @IBOutlet weak var providerLabel: UILabel!

    @IBOutlet weak var durationStackView: UIStackView!
    @IBOutlet weak var dateStackView: UIStackView!
    @IBOutlet weak var issuerStackView: UIStackView!
    @IBOutlet weak var doctorStackView: UIStackView!

    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var expandView: UIView!

    weak var timer: Timer?

    var indexPath: IndexPath?

    override var viewsToCheck: [ViewWidthDefining] {
        isExpanded ? [nameLabel, dateLabel, durationStackView, doctorLabel, labLabel] : [nameLabel, dateLabel, durationStackView]
    }

    override var stackViews: [UIStackView] {
        isExpanded ? [durationStackView, dateStackView, issuerStackView, doctorStackView] : [dateStackView, durationStackView]
    }

    override internal func setup() {
        guard let test = document as? CoronaTest else { return }

        let testResult = test.isNegative ? L10n.IOSApp.Test.Result.negative : L10n.IOSApp.Test.Result.positive
        resultLabel.text = "\(test.testType.localized): \(testResult)"

        wrapperView.layer.cornerRadius = 8
        wrapperView.backgroundColor = test.isNegative ? Asset.lucaCheckinGradientBottom.color : UIColor.white

        nameLabel.text = owner.formattedName
        durationSinceLabel.text = test.issuedAt.durationSinceDate
        dateLabel.text = test.issuedAt.formattedDateTime
        dateLabel.accessibilityLabel = test.issuedAt.accessibilityDate
        labLabel.text = test.laboratory.replacingOccurrences(of: "\\s[\\s]+", with: "\n", options: .regularExpression, range: nil)
        doctorLabel.text = test.doctor

        qrCodeImageView.isHidden = !test.showQRCode

        qrCodeImageView.layer.cornerRadius = 8
        setupQRCodeImage(for: test)

        qrCodeImageView.isAccessibilityElement = true
        qrCodeImageView.accessibilityLabel = L10n.IOSApp.Contact.Qr.Accessibility.qrCode

        deleteButton.addTarget(self, action: #selector(didPressDelete(sender:)), for: .touchUpInside)
        deleteButton.layer.borderWidth = 1
        deleteButton.layer.borderColor = UIColor.black.cgColor
        deleteButton.layer.cornerRadius = 16
        deleteButton.accessibilityLabel = L10n.IOSApp.Test.Delete.title

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.isEnabled = true
        tapGestureRecognizer.cancelsTouchesInView = true
        addGestureRecognizer(tapGestureRecognizer)
        providerLabel.text = test.provider

        recentDocumentIndicator.isHidden = !test.isRecent
        disclosureIndicator.isHidden = isExpanded
        expandView.isHidden = !self.isExpanded

        startDateUpdateTimer()
        updateStackViews()
        initObserver()

        position = .single
    }

    private func setupQRCodeImage(for test: CoronaTest) {
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let image = QRCodeGenerator.generateQRCode(string: test.originalCode)
        if let scaledQr = image?.transformed(by: transform) {
            qrCodeImageView.image = UIImage(ciImage: scaledQr)
        }
    }

    func startDateUpdateTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 60.0, repeats: true) { [weak self] _ in
            self?.durationSinceLabel.text = self?.document?.issuedAt.durationSinceDate
        }
    }

    func stopDateUpdateTimer() {
        timer?.invalidate()
    }
}

// MARK: - DocumentViewProtocol

extension CoronaTestView: DocumentViewProtocol {
    public static func createView(document: Document, owner: Person, isExpanded: Bool, delegate: DocumentViewDelegate?) -> DocumentView? {
        guard let document = document as? CoronaTest else { return nil }

        let itemView: CoronaTestView = CoronaTestView.fromNib()
        itemView.delegate = delegate
        itemView.owner = owner
        itemView.isExpanded = isExpanded
        itemView.document = document
		itemView.isAccessibilityElement = !isExpanded
        itemView.accessibilityTraits = .button
        itemView.accessibilityLabel = L10n.IOSApp.Test.Show.title
        return itemView
    }
}

// MARK: - Actions

extension CoronaTestView {
    @objc
    private func viewTapped(_ sender: UITapGestureRecognizer) {
        guard let document = document else { return }
        delegate?.didSelect(self, document: document)
    }

    @objc
    private func didPressDelete(sender: UIButton) {
        if let test = self.document {
            delegate?.didTapDelete(self, for: test)
        }
    }
}
