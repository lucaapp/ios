import WebKit
import SwiftUI

struct WebView: UIViewRepresentable {
    typealias OnURLCallback = ((URL) -> WKNavigationActionPolicy)
    private var request: URLRequest
    @Binding var alerts: AlertContent?
    private var onEnterURL: OnURLCallback?
    var credential: URLCredential?

    init(
        request: URLRequest,
        alerts: Binding<AlertContent?>? = nil,
        credential: URLCredential? = nil,
        onEnterURL: OnURLCallback? = nil
    ) {
        self.request = request
        self.onEnterURL = onEnterURL
        self._alerts = alerts ?? .constant(nil)
        self.credential = credential
    }

    func makeUIView(context: Context) -> WKWebView {
        let webView = WKWebView()
        webView.navigationDelegate = context.coordinator
        webView.load(self.request)
        return webView
    }

    func updateUIView(_ uiView: WKWebView, context: Context) {}

    func makeCoordinator() -> Coordinator {
        return Coordinator(alerts: $alerts, onEnterURL: onEnterURL, credential: credential)
    }

    final class Coordinator: NSObject, WKNavigationDelegate {
        @Binding var alerts: AlertContent?
        var onEnterURL: OnURLCallback?
        var credential: URLCredential?

        init(alerts: Binding<AlertContent?>, onEnterURL: OnURLCallback?, credential: URLCredential?) {
            self._alerts = alerts
            self.credential = credential
            self.onEnterURL = onEnterURL
        }

        func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
            print("WebView.didStartProvisionalNavigation: \(navigation)")

        }
        func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
            print("WebView.navigationAction: \(navigationAction.request.url)")
            if let url = navigationAction.request.url,
               let onEnterURL = onEnterURL {
                decisionHandler(onEnterURL(url))
                return
            }
            decisionHandler(.allow)
        }
        func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
            print("WebView.redirect: \(navigation)")
        }

        func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
            print("WebView.WERROR: \(error)")
            alerts = error.alertContent
        }

        func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            print("WebView.Did finish")
        }

        func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
            if let error = error as? NSError,
               error.code == -1009 {
                alerts = NetworkError.noInternet.alertContent
            } else {
                alerts = error.alertContent
            }
        }

        func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge,
                     completionHandler: @escaping(URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
            print("WebView.didReceive challenge")
            if let credential = credential {
                completionHandler(.useCredential, credential)
            } else {
                completionHandler(.performDefaultHandling, nil)
            }
        }
    }

}
