import UIKit
import RxSwift
import RxCocoa
import LucaUIComponents

@IBDesignable
class DontAskAnymoreView: UIView, Themable {
    var theme: AppearanceTheme = .dark
    var supportedThemes: [AppearanceTheme] = [.light, .dark]

    private weak var `switch`: UISwitch!

    var isOn: Bool {
        self.switch.isOn
    }

    /// Emits value changes
    var onToggle: Driver<Bool> {
        self.switch.rx.isOn.skip(1).distinctUntilChanged().asDriver(onErrorJustReturn: false)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }

    func apply(theme: AppearanceTheme) {
        applyToSubviews(theme: theme)
    }

    /// Connects source to the switch
    func bind(source: Driver<Bool>) -> Disposable {
        source.distinctUntilChanged().drive(self.switch.rx.isOn)
    }

    private func setup() {

        let titleLabel = Luca14PtLabel()
        titleLabel.text = L10n.IOSApp.General.dontAskAnymore
        titleLabel.numberOfLines = 0

        let titleLabelContainer = UIView()
        titleLabelContainer.addSubview(titleLabel)
        titleLabel.setAnchorConstraintsFullSizeTo(view: titleLabelContainer, padding: 0)

        let toggle = UISwitch()
        toggle.onTintColor = Asset.lucaBlue.color

        self.switch = toggle
        let stackView = UIStackView(arrangedSubviews: [titleLabelContainer, toggle])
        addSubview(stackView)

        stackView.setAnchorConstraintsFullSizeTo(view: self, padding: 0)

        stackView.alignment = .center

        backgroundColor = .clear
    }
}
