import UIKit

public class PushOptionCoordinator: Coordinator {

    private weak var presenter: UIViewController?
    private var toPresent: UIViewController

    public init(presenter: UIViewController, toPresent: UIViewController) {
        self.presenter = presenter
        self.toPresent = toPresent
    }

    public func start() {
        guard let presenter = presenter else {
            return
        }
        presenter.navigationController?.pushViewController(toPresent, animated: true)
    }

}
