import UIKit

public class GitlabLinkCoordinator: Coordinator {

    private let url: String
    private weak var presenter: UIViewController?

    public init(presenter: UIViewController, url: String) {
        self.presenter = presenter
        self.url = url
    }

    public func start() {
        guard let presenter = presenter else { return }

        let alert = UIAlertController.actionAndCancelAlert(
            title: L10n.IOSApp.General.Gitlab.Alert.title,
            message: L10n.IOSApp.General.Gitlab.Alert.description,
            actionTitle: L10n.IOSApp.General.Gitlab.Alert.actionButton,
            action: {
                guard let url = URL(string: self.url) else { return }
                UIApplication.shared.open(url, options: [:])
            })

        presenter.present(alert, animated: true, completion: nil)
    }
}
