import UIKit

class VoluntaryReachabilityCoordinator: Coordinator {

    private weak var presenter: UIViewController? // UITabViewController
    static let storyboard = UIStoryboard(name: "VoluntaryReachability", bundle: nil)
    private lazy var closeButton: UIBarButtonItem = {
        UIBarButtonItem(title: L10n.IOSApp.Navigation.Basic.Cancel.uppercased, style: .plain, target: self, action: #selector(closeModal))
    }()
    private lazy var backButton: UIBarButtonItem = {
        UIBarButtonItem(title: L10n.IOSApp.Navigation.Basic.back, style: .plain, target: self, action: nil)
    }()

    public init(presenter: UIViewController) {
        self.presenter = presenter
    }

    func start() {
        let vc: VoluntaryReachabilityShareDataViewController = VoluntaryReachabilityShareDataViewController.instantiate(
            storyboard: VoluntaryReachabilityCoordinator.storyboard,
            identifier: "VoluntaryReachabilityShareDataViewController")
        vc.continueCoordinator = VoluntaryReachabilityVacStateCoordinator(presenter: vc, viewModel: VoluntaryReachabilityViewModel(), closeButton: closeButton, backButton: backButton)
        vc.navigationItem.rightBarButtonItem = closeButton
        vc.navigationItem.backBarButtonItem = backButton
        let navigationController = UINavigationController(rootViewController: vc)
        guard let nav = presenter?.presentedViewController as? UINavigationController, nav.viewControllers.first is VoluntaryReachabilityShareDataViewController else {
            presenter?.present(navigationController, animated: true)
            return
        }
    }

    @objc private func closeModal() {
        guard let presenter = presenter else {return}
        presenter.dismiss(animated: true, completion: nil)
    }
}
