import UIKit

class VoluntaryReachabilityScanCodeCoordinator: VoluntaryReachabilityBaseCoordinator {
    override func start() {
        // display Scan Code Coordinator
        let vc: VoluntaryReachabilityScanQRCodeController = VoluntaryReachabilityScanQRCodeController.instantiate(
            storyboard: VoluntaryReachabilityCoordinator.storyboard,
            identifier: "VoluntaryReachabilityScanQRCodeController")
        vc.addProofSuccessCoordinator = VoluntaryReachabilityAddProofSuccessCoordinator(presenter: presenter, viewModel: viewModel, closeButton: closeButton, backButton: backButton)
        vc.navigationItem.rightBarButtonItem = closeButton
        vc.navigationItem.backBarButtonItem = backButton
        presenter?.navigationController?.pushViewController(vc, animated: true)

    }
}
