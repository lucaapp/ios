import UIKit

class VoluntaryReachabilityAddProofSuccessCoordinator: VoluntaryReachabilityBaseCoordinator {

    override func start() {
        let vc: VoluntaryReachabilityInfoViewController = VoluntaryReachabilityInfoViewController.instantiate(
            storyboard: VoluntaryReachabilityCoordinator.storyboard,
            identifier: "VoluntaryReachabilityInfoViewController")
        _ = vc.view
        vc.titleLabel.text = L10n.IOSApp.VoluntaryReachability.AddProofSuccess.title
        vc.descriptionLabel.text = L10n.IOSApp.VoluntaryReachability.AddProofSuccess.message
        vc.imageView.image = UIImage(named: L10n.IOSApp.VoluntaryReachability.AddProofSuccess.imageName)
        vc.continueButton.setTitle(L10n.IOSApp.VoluntaryReachability.AddProofSuccess.continue.uppercased(), for: .normal)
        vc.coordinator = VoluntaryReachabilityContactDataCoordinator(presenter: presenter, viewModel: viewModel, closeButton: closeButton, backButton: backButton)
        vc.navigationItem.rightBarButtonItem = closeButton
        vc.navigationItem.backBarButtonItem = backButton
      presenter?.navigationController?.pushViewController(vc, animated: true)
    }
}
