import UIKit

class VoluntaryReachabilityKritisCoordinator: VoluntaryReachabilityBaseCoordinator {

    override func start() {
        let vc: VoluntaryReachabilityKritisViewController = VoluntaryReachabilityKritisViewController.instantiate(
            storyboard: VoluntaryReachabilityCoordinator.storyboard,
            identifier: "VoluntaryReachabilityKritisViewController")
        vc.navigationItem.rightBarButtonItem = closeButton
        vc.navigationItem.backBarButtonItem = backButton

        vc.consentCoordinator = VoluntaryReachabilityConsentCoordinator(presenter: presenter, viewModel: viewModel, closeButton: closeButton, backButton: backButton)
        presenter?.navigationController?.pushViewController(vc, animated: true)
    }

}
