import UIKit

class VoluntaryReachabilityBaseCoordinator: Coordinator {
    weak var presenter: UIViewController?
    var viewModel: VoluntaryReachabilityViewModel
    weak var closeButton: UIBarButtonItem!
    weak var backButton: UIBarButtonItem!

    init(presenter: UIViewController?, viewModel: VoluntaryReachabilityViewModel, closeButton: UIBarButtonItem, backButton: UIBarButtonItem) {
        self.presenter = presenter
        self.viewModel = viewModel
        self.closeButton = closeButton
        self.backButton = backButton
    }

    func start() {
        fatalError("Needs to be implemented")
    }
}
