import UIKit

class VoluntaryReachabilityContactDataCoordinator: VoluntaryReachabilityBaseCoordinator {

    override func start() {
        let vc: VoluntaryReachabilityContactDataViewController = VoluntaryReachabilityContactDataViewController.instantiate(
            storyboard: VoluntaryReachabilityCoordinator.storyboard,
            identifier: "VoluntaryReachabilityContactDataViewController")
        vc.navigationItem.rightBarButtonItem = closeButton
        vc.navigationItem.backBarButtonItem = backButton
       vc.contactData = viewModel

        vc.kritisCoordinator = VoluntaryReachabilityKritisCoordinator(presenter: presenter, viewModel: viewModel, closeButton: closeButton, backButton: backButton)
        presenter?.navigationController?.pushViewController(vc, animated: true)
    }
}
