import UIKit
import RxSwift

class VoluntaryReachabilityConsentCoordinator: VoluntaryReachabilityBaseCoordinator {

    override func start() {
        let vc: VoluntaryReachabilityConsentViewController = VoluntaryReachabilityConsentViewController.instantiate(
            storyboard: VoluntaryReachabilityCoordinator.storyboard,
            identifier: "VoluntaryReachabilityConsentViewController")
        vc.navigationItem.rightBarButtonItem = closeButton
        vc.navigationItem.backBarButtonItem = backButton
        vc.data = viewModel
        vc.thankYouCoordinator = VoluntaryReachabilityThankYouCoordinator(presenter: presenter, viewModel: viewModel, closeButton: closeButton, backButton: backButton)
        presenter?.navigationController?.pushViewController(vc, animated: true)
    }
}
