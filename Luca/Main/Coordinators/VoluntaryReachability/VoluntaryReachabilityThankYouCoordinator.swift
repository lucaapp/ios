import UIKit

class VoluntaryReachabilityThankYouCoordinator: VoluntaryReachabilityBaseCoordinator {

    override func start() {
        let vc: VoluntaryReachabilityInfoViewController = VoluntaryReachabilityInfoViewController.instantiate(
            storyboard: VoluntaryReachabilityCoordinator.storyboard,
            identifier: "VoluntaryReachabilityInfoViewController")
        _ = vc.view
        vc.titleLabel.text = L10n.IOSApp.VoluntaryReachability.Thankyou.title
        vc.descriptionLabel.text = L10n.IOSApp.VoluntaryReachability.Thankyou.message
        vc.imageView.image = UIImage(named: L10n.IOSApp.VoluntaryReachability.Thankyou.imageName)
        vc.continueButton.setTitle(L10n.IOSApp.VoluntaryReachability.Thankyou.close.uppercased(), for: .normal)
        vc.coordinator = VoluntaryReachabilityFinalCoordinator(presenter: presenter, viewModel: viewModel, closeButton: closeButton, backButton: backButton)
        vc.navigationItem.rightBarButtonItem = closeButton
        vc.navigationItem.backBarButtonItem = backButton
        presenter?.navigationController?.pushViewController(vc, animated: true)
    }
}

class VoluntaryReachabilityFinalCoordinator: VoluntaryReachabilityBaseCoordinator {
    override func start() {
        presenter?.dismiss(animated: true, completion: nil)
    }
}
