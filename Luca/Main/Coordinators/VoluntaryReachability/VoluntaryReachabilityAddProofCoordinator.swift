import UIKit

class VoluntaryReachabilityAddProofCoordinator: VoluntaryReachabilityBaseCoordinator {

    override func start() {
        let vc: VoluntaryReachabilityAddProofViewController = VoluntaryReachabilityAddProofViewController.instantiate(
            storyboard: VoluntaryReachabilityCoordinator.storyboard,
            identifier: "VoluntaryReachabilityAddProofViewController")
        vc.addProofSuccessCoordinator = VoluntaryReachabilityAddProofSuccessCoordinator(presenter: presenter, viewModel: viewModel, closeButton: closeButton, backButton: backButton)
        vc.scanQRCodeCoordinator = VoluntaryReachabilityScanCodeCoordinator(presenter: presenter, viewModel: viewModel, closeButton: closeButton, backButton: backButton)
        vc.navigationItem.rightBarButtonItem = closeButton
        vc.navigationItem.backBarButtonItem = backButton
        presenter?.navigationController?.pushViewController(vc, animated: true)
    }
}
