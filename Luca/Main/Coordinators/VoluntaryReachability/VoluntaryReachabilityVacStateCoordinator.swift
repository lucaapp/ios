import UIKit

class VoluntaryReachabilityVacStateCoordinator: VoluntaryReachabilityBaseCoordinator {

    override func start() {
        let vc: VoluntaryReachabilityVacStatusViewController = VoluntaryReachabilityVacStatusViewController.instantiate(
            storyboard: VoluntaryReachabilityCoordinator.storyboard,
            identifier: "VoluntaryReachabilityVacStatusViewController")
        vc.viewModel = viewModel
        vc.addProofCoordinator = VoluntaryReachabilityAddProofCoordinator(presenter: presenter, viewModel: viewModel, closeButton: closeButton, backButton: backButton)
        vc.contactDataCoordinator = VoluntaryReachabilityAddProofSuccessCoordinator(presenter: presenter, viewModel: viewModel, closeButton: closeButton, backButton: backButton)
        vc.navigationItem.rightBarButtonItem = closeButton
        vc.navigationItem.backBarButtonItem = backButton
        presenter?.navigationController?.pushViewController(vc, animated: true)
    }
}
