import UIKit

public class HealthDepartmentCryptoInfoCoordinator: Coordinator {

    private weak var presenter: UIViewController?

    public init(presenter: UIViewController) {
        self.presenter = presenter
    }

    public func start() {
        guard let presenter = presenter else { return }
        let viewController = ViewControllerFactory.Main.createHealthDepartmentCryptoInfoViewController()
        presenter.navigationController?.pushViewController(viewController, animated: true)
    }
}
