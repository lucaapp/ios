import UIKit

class AccessedTraceIdDetailCoordinator: Coordinator {

    private let accessedTraceId: AccessedTraceId
    private weak var presenter: UIViewController?

    public init(presenter: UIViewController, accessedTraceId: AccessedTraceId) {
        self.presenter = presenter
        self.accessedTraceId = accessedTraceId
    }

    public func start() {
        guard let presenter = presenter else { return }

        let vc = NotificationViewControllerFactory.createNotificationDetailViewController(accessedTraceId: accessedTraceId)
        presenter.navigationController?.pushViewController(vc, animated: true)
    }
}
