import UIKit
import DeviceKit
import MessageUI

public class SendSupportEmailCoordinator: NSObject, Coordinator {

    private weak var presenter: UIViewController?

    public init(presenter: UIViewController) {
        self.presenter = presenter
    }

    public func start() {
        guard let presenter = presenter else { return }
        if MFMailComposeViewController.canSendMail() {
            let version = UIApplication.shared.applicationVersion ?? ""
            let messageBody = L10n.IOSApp.General.Support.Email.body(Device.current.description, UIDevice.current.systemVersion, version)

            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([L10n.IOSApp.General.Support.email])
            mail.setSubject(L10n.UserApp.Support.Account.Email.subject)
            mail.setMessageBody(messageBody, isHTML: true)
            presenter.present(mail, animated: true)
        } else {
            let alert = UIAlertController.infoAlert(title: L10n.IOSApp.Navigation.Basic.error, message: L10n.IOSApp.General.Support.error)
            presenter.present(alert, animated: true, completion: nil)
        }
    }
}

 extension SendSupportEmailCoordinator: MFMailComposeViewControllerDelegate {

    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

 }
