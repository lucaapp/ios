import UIKit
import LicensesViewController

public class LicensesCoordinator: NSObject, Coordinator {

    private weak var presenter: UIViewController?

    public init(presenter: UIViewController) {
        self.presenter = presenter
    }

    public func start() {
        guard let presenter = presenter else { return }
        let licensesViewController = LicensesViewController()
        licensesViewController.loadViewIfNeeded()
        licensesViewController.title = L10n.IOSApp.acknowledgements
        licensesViewController.loadPlist(Bundle.main, resourceName: "Credits")
        presenter.navigationController?.pushViewController(licensesViewController, animated: true)
    }
}
