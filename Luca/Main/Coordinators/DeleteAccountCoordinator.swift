import UIKit
import RxSwift
import JGProgressHUD
import DependencyInjection

public class DeleteAccountCoordinator: NSObject, Coordinator {

    @InjectStatic(\.dataResetService) private var dataResetService

    private weak var presenter: UIViewController?

    private let progressHUD = JGProgressHUD.lucaLoading()

    public init(presenter: UIViewController) {
        self.presenter = presenter
    }

    public func start() {
        guard let presenter = self.presenter else { return }
        UIAlertController(title: L10n.IOSApp.Data.ResetData.title,
                          message: L10n.IOSApp.Data.ResetData.description,
                          preferredStyle: .alert)
            .actionAndCancelAlert(actionText: L10n.IOSApp.Data.ResetData.title, actionStyle: .destructive, action: {
                self.deleteUser()
            }, viewController: presenter)
    }

    private func deleteUser() {
        guard let presenter = presenter else {
            return
        }
        if let view = presenter.view {
            progressHUD.show(in: view)
        }
        _ = deleteUserData()
            .observe(on: MainScheduler.instance)
            .handleErrors(presenter)
            .do(onDispose: { [weak self] in
                self?.progressHUD.dismiss()
            })
            .subscribe()
    }

    private func deleteUserData() -> Completable {
        dataResetService.resetAll()
            .andThen(
                Completable.from { self.presenter?.dismiss(animated: true, completion: nil) }
                    .subscribe(on: MainScheduler.asyncInstance)
            )
    }
}
