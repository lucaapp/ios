import UIKit

public class VersionDetailsCoordinator: Coordinator {

    private weak var presenter: UIViewController?

    public init(presenter: UIViewController) {
        self.presenter = presenter
    }

    public func start() {
        guard let presenter = presenter else { return }
        AlertViewControllerFactory.createAppVersionAlertController(viewController: presenter)
    }
}
