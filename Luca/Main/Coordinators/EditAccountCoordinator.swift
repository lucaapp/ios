import UIKit
import LicensesViewController

public class EditAccountCoordinator: NSObject, Coordinator {

    private weak var presenter: UIViewController?

    public init(presenter: UIViewController) {
        self.presenter = presenter
    }

    public func start() {
        guard let presenter = presenter else { return }
        let contactViewController = ViewControllerFactory.Main.createContactViewController()
        presenter.navigationController?.pushViewController(contactViewController, animated: true)
    }
}
