import Foundation
import SwiftUI

struct LucaTextFieldStyle: TextFieldStyle {

    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
            .frame(height: 56.0)
            .padding(EdgeInsets(top: 0,
                                leading: 16,
                                bottom: 0,
                                trailing: 16))
            .cornerRadius(8)
            .foregroundColor(.white)
            .lucaFont(.luca14Medium)
            .overlay(RoundedRectangle(cornerRadius: 8)
                .stroke(Color(Asset.luca747480.color)))

    }

}
