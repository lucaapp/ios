import SwiftUI

struct LucaButton: ButtonStyle {
    @Environment(\.isEnabled) var isEnabled: Bool

    enum Color {
        case blue
        case green
    }

    var color: Color = .blue

    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .lucaFont(.luca14Bold)
            .foregroundColor(.black)
            .frame(height: 48.0)
            .frame(maxWidth: .infinity)
            .opacity(isEnabled ? 1 : 0.5)
            .background(color.color)
            .clipShape(Capsule())
    }
}

extension LucaButton.Color {
    var color: Color {
        switch self {
        case .blue: return Color(Asset.lucaBlue.color)
        case .green: return Color(Asset.lucaCheckinGradientTop.color)
        }
    }
}

struct LucaWhiteButtonWithBlackBorder: ButtonStyle {

    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .lucaFont(.luca14Bold)
            .foregroundColor(.black)
            .frame(maxWidth: .infinity)
            .frame(height: 48.0)
            .contentShape(Rectangle())
            .background(
                RoundedRectangle(cornerRadius: .infinity)
                    .stroke(Color.black, lineWidth: 1.0)
            )
    }
}

struct LucaButtonWithWhiteBorder: ButtonStyle {

    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding(EdgeInsets(top: 0, leading: 32, bottom: 0, trailing: 32))
            .lucaFont(.luca14Bold)
            .frame(maxWidth: .infinity)
            .foregroundColor(.white)
            .frame(minHeight: 48.0)
            .contentShape(Rectangle())
            .background(
                RoundedRectangle(cornerRadius: .infinity)
                    .stroke(Color.white, lineWidth: 2.0)
            )
    }
}

struct LucaIconButton: ButtonStyle {

    let icon: UIImage

    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .lucaFont(.luca14Bold)
            .foregroundColor(.white)
            .padding(.horizontal, 64)
            .frame(maxWidth: .infinity, minHeight: 64, alignment: .leading)
            .contentShape(Rectangle())
            .background(
                RoundedRectangle(cornerRadius: 6)
                    .fill(.white)
                    .opacity(0.15)
            )
            .overlay(
                Image(uiImage: icon)
                    .frame(width: 64, height: 64, alignment: .center)
                    .aspectRatio(contentMode: .fit),
                alignment: .leading
            )
    }
}
