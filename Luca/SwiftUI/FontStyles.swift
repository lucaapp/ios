import SwiftUI
import LucaUIComponents

// MARK: - Fonts

enum LucaFont {
    case luca14Medium
    case luca14Bold
    case luca16Medium
    case luca16Bold
    case luca20Bold
    case luca24Bold
    case luca24Regular
    case navigationItem
}

extension LucaFont {
    var font: SwiftUI.Font {
        switch self {
        case .luca14Medium: return SwiftUI.Font.custom(FontFamily.Montserrat.medium.name, size: 14.0, relativeTo: SwiftUI.Font.TextStyle.body)
        case .luca14Bold: return SwiftUI.Font.custom(FontFamily.Montserrat.bold.name, size: 14.0, relativeTo: SwiftUI.Font.TextStyle.body)
        case .luca16Medium: return SwiftUI.Font.custom(FontFamily.Montserrat.medium.name, size: 16.0, relativeTo: SwiftUI.Font.TextStyle.body)
        case .luca16Bold: return SwiftUI.Font.custom(FontFamily.Montserrat.bold.name, size: 16.0, relativeTo: SwiftUI.Font.TextStyle.body)
        case .luca20Bold: return SwiftUI.Font.custom(FontFamily.Montserrat.bold.name, size: 20.0, relativeTo: SwiftUI.Font.TextStyle.title)
        case .luca24Bold: return SwiftUI.Font.custom(FontFamily.Montserrat.bold.name, size: 24.0, relativeTo: SwiftUI.Font.TextStyle.title)
        case .luca24Regular: return SwiftUI.Font(FontFamily.Montserrat.regular.font(size: 24.0))
        case .navigationItem: return SwiftUI.Font.custom(FontFamily.Montserrat.bold.name, size: 14.0, relativeTo: SwiftUI.Font.TextStyle.body)
        }
    }
}

struct LucaFontModifier: ViewModifier {
    var font: LucaFont

    func body(content: Content) -> some View {
        content
            .font(font.font)
    }
}

struct LucaCustomFontModifier: ViewModifier {
    var size: CGFloat
    var weight: SwiftUI.Font.Weight
    var relativeTo: SwiftUI.Font.TextStyle

    func body(content: Content) -> some View {
        content
            .font(dynamicFont(size, weight: weight, relativeTo: relativeTo))
    }

    private func dynamicFont(_ size: CGFloat, weight: SwiftUI.Font.Weight, relativeTo: SwiftUI.Font.TextStyle) -> SwiftUI.Font {
        switch weight {
        case .semibold:
            return SwiftUI.Font.custom(FontFamily.Montserrat.semiBold.name, size: size, relativeTo: SwiftUI.Font.TextStyle.body)
        case .light:
            return SwiftUI.Font.custom(FontFamily.Montserrat.light.name, size: size, relativeTo: SwiftUI.Font.TextStyle.body)
        case .regular:
            return SwiftUI.Font.custom(FontFamily.Montserrat.regular.name, size: size, relativeTo: SwiftUI.Font.TextStyle.body)
        case .medium:
            return SwiftUI.Font.custom(FontFamily.Montserrat.medium.name, size: size, relativeTo: SwiftUI.Font.TextStyle.body)
        case .bold:
            return SwiftUI.Font.custom(FontFamily.Montserrat.bold.name, size: size, relativeTo: SwiftUI.Font.TextStyle.body)
        default:
            return SwiftUI.Font.custom(FontFamily.Montserrat.medium.name, size: size, relativeTo: SwiftUI.Font.TextStyle.body)
        }
    }
}

extension View {

    /// Use to set a specific font to a text element:
    ///
    ///     var body: some View {
    ///         Text("My String")
    ///             .lucaFont(.luca16Bold)
    ///     }
    func lucaFont(_ font: LucaFont) -> some View {
        modifier(LucaFontModifier(font: font))
    }

    func lucaFont(_ size: CGFloat, weight: SwiftUI.Font.Weight, relativeTo: SwiftUI.Font.TextStyle = SwiftUI.Font.TextStyle.body) -> some View {
        modifier(LucaCustomFontModifier(size: size, weight: weight, relativeTo: relativeTo))
    }

    func navigationItemFont() -> some View {
        modifier(LucaFontModifier(font: .navigationItem))
            .foregroundColor(Color(Asset.lucaBlue.color))
    }
}
