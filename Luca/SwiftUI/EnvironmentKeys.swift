import UIKit
import SwiftUI

/// Value wrapper to prevent strong references
class Weak<T: AnyObject> {
    weak var value: T?

    init(_ value: T? = nil) {
        self.value = value
    }
}

private struct PresenterEnvironmentKey: EnvironmentKey {
    static let defaultValue: Weak<UIViewController> = Weak()
}

extension EnvironmentValues {
    var presenter: Weak<UIViewController> {
        get { self[PresenterEnvironmentKey.self] }
        set { self[PresenterEnvironmentKey.self] = newValue }
    }
}
