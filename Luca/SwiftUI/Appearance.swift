import SwiftUI
import LucaUIComponents

// MARK: - Appearance

struct LucaBaseColorApplier: ViewModifier {
    var theme: AppearanceTheme

    private var foregroundColor: Color {
        switch theme {
        case .light: return Color.black
        case .dark: return Color.white
        }
    }

    private var backgroundColor: Color {
        switch theme {
        case .light: return Color.white
        case .dark: return Color.black
        }
    }

    func body(content: Content) -> some View {
        content
            .foregroundColor(foregroundColor)
            .frame(maxWidth: .infinity, maxHeight: .infinity) // 1
            .background(backgroundColor.edgesIgnoringSafeArea(.bottom))
    }
}

extension View {

    private func appearanceBaseColorApplier(theme: AppearanceTheme) -> some View {
        modifier(LucaBaseColorApplier(theme: theme))
    }

    /// Sets appearance of a view (foreground and background color). Needs to be set on the outermost view
    /// Make sure you don't override .background on the outermost view
    ///
    ///     var body: some View {
    ///         MyView()
    ///             .appearance(.light)
    ///     }
    func appearance(_ appearance: AppearanceTheme) -> some View {
        appearanceBaseColorApplier(theme: appearance)
    }
}
