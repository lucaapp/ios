// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  internal enum IOSApp {
    /// Licenses
    internal static let acknowledgements = L10n.tr("iOSApp", "Acknowledgements")
    internal enum AccessedTraceIdDetailViewController {
      /// Notification
      internal static let title = L10n.tr("iOSApp", "accessedTraceIdDetailViewController.title")
    }
    internal enum Account {
      internal enum Help {
        /// Help
        internal static let heading = L10n.tr("iOSApp", "account.help.heading")
      }
      internal enum Information {
        /// Information
        internal static let heading = L10n.tr("iOSApp", "account.information.heading")
      }
      internal enum Option {
        /// Automatic check-out
        internal static let automaticCheckout = L10n.tr("iOSApp", "account.option.automaticCheckout")
        /// Direct check-in
        internal static let directCheckin = L10n.tr("iOSApp", "account.option.directCheckin")
        /// 2G/3G status
        internal static let healthStatus = L10n.tr("iOSApp", "account.option.healthStatus")
        /// Voluntary check-in
        internal static let voluntaryCheckin = L10n.tr("iOSApp", "account.option.voluntaryCheckin")
        internal enum DirectCheckin {
          internal enum Checkin {
            /// Check in directly without having to confirm it every time? Activate the direct check-in here. You can cancel this setting at any time.
            internal static let description = L10n.tr("iOSApp", "account.option.directCheckin.checkin.description")
            /// Check in directly
            internal static let title = L10n.tr("iOSApp", "account.option.directCheckin.checkin.title")
          }
        }
        internal enum HealthStatus {
          internal enum Share {
            /// luca locations may have 2G/3G entry requirements. If you choose to share your 2G/§G status, locations will only be informed about whether you fulfill their requirement or not. They will not see your 2G/3G documents. Here in the account tab, you can set whether you want to share your 2G/3G status when checking in.
            internal static let description = L10n.tr("iOSApp", "account.option.healthStatus.share.description")
            /// Share 2G/3G status
            internal static let title = L10n.tr("iOSApp", "account.option.healthStatus.share.title")
          }
        }
        internal enum Luca {
          /// luca Connect
          internal static let connect = L10n.tr("iOSApp", "account.option.luca.connect")
          internal enum Connect {
            internal enum Activate {
              /// Activate personal notifications to help your health department and receive important information from it directly within the luca app. \n\nYour information will be encrypted and only displayed if a health department employee's search leads to an exact match. Providing your information helps health departments work faster and more efficiently, and improves the notification process.
              internal static let description = L10n.tr("iOSApp", "account.option.luca.connect.activate.description")
              /// Activate luca Connect 
              internal static let title = L10n.tr("iOSApp", "account.option.luca.connect.activate.title")
            }
          }
        }
        internal enum VoluntaryCheckin {
          internal enum Activate {
            /// Enable this setting if you want to save and apply it to future check-ins. If you enable it, you will automatically be checked into voluntary check-ins. This is based on your consent according to Art. 6 (1) 1 a) DSGVO. Further information on revocation and deletion can be found in our privacy policy.\n\nHere in the account tab you can change your check-in settings at any time.
            internal static let description = L10n.tr("iOSApp", "account.option.voluntaryCheckin.activate.description")
            /// Activate voluntary check-in
            internal static let title = L10n.tr("iOSApp", "account.option.voluntaryCheckin.activate.title")
          }
          internal enum ShareData {
            /// Additionally, if you would like to voluntarily submit your contact information during future check-ins, enable here.
            internal static let description = L10n.tr("iOSApp", "account.option.voluntaryCheckin.shareData.description")
            /// Share contact data
            internal static let title = L10n.tr("iOSApp", "account.option.voluntaryCheckin.shareData.title")
          }
        }
      }
      internal enum Settings {
        /// Settings
        internal static let heading = L10n.tr("iOSApp", "account.settings.heading")
      }
    }
    internal enum AppVersion {
      /// Version Details
      internal static let button = L10n.tr("iOSApp", "appVersion.button")
      internal enum Alert {
        /// You are currently using the iOS app version %@ (%@) %@\n\nThe source code can be found on GitLab using the commit hash %@
        internal static func message(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any) -> String {
          return L10n.tr("iOSApp", "appVersion.alert.message", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4))
        }
        /// App Version
        internal static let title = L10n.tr("iOSApp", "appVersion.alert.title")
      }
    }
    internal enum Appointment {
      internal enum Result {
        /// APPOINTMENT
        internal static let `prefix` = L10n.tr("iOSApp", "appointment.result.prefix")
      }
      internal enum View {
        internal enum Label {
          /// Address:
          internal static let address = L10n.tr("iOSApp", "appointment.view.label.address")
          /// Date:
          internal static let date = L10n.tr("iOSApp", "appointment.view.label.date")
        }
      }
    }
    internal enum Button {
      internal enum Private {
        /// Create private meeting
        internal static let meeting = L10n.tr("iOSApp", "button.private.meeting")
      }
      internal enum Scanner {
        /// Self check-in
        internal static let checkin = L10n.tr("iOSApp", "button.scanner.checkin")
      }
      internal enum Show {
        /// Show my QR code
        internal static let qr = L10n.tr("iOSApp", "button.show.qr")
      }
    }
    internal enum Camera {
      internal enum Access {
        /// Once you've granted access, you can scan QR codes within your luca app.
        internal static let description = L10n.tr("iOSApp", "camera.access.description")
        /// luca needs to access your camera
        internal static let title = L10n.tr("iOSApp", "camera.access.title")
        internal enum Activate {
          /// Press to\nactivate camera
          internal static let label = L10n.tr("iOSApp", "camera.access.activate.label")
        }
      }
      internal enum Error {
        /// Your device cannot be used to scan.
        internal static let scanningFailed = L10n.tr("iOSApp", "camera.error.scanningFailed")
      }
      internal enum Warning {
        internal enum Checkin {
          /// Hey, you're scanning the QR code of a document and not of a luca location. If you want to add a document, click "CONTINUE".
          internal static let description = L10n.tr("iOSApp", "camera.warning.checkin.description")
          /// Continue with import?
          internal static let title = L10n.tr("iOSApp", "camera.warning.checkin.title")
        }
        internal enum Document {
          /// Hey, you're scanning the QR code of a luca location and not of a document. Click "CONTINUE" to check in.
          internal static let description = L10n.tr("iOSApp", "camera.warning.document.description")
          /// Proceed with check-in?
          internal static let title = L10n.tr("iOSApp", "camera.warning.document.title")
        }
        internal enum Incompatible {
          /// The QR code could not be scanned and the data could not be processed. This could be because this QR code is not compatible with luca.
          internal static let description = L10n.tr("iOSApp", "camera.warning.incompatible.description")
          /// Operation failed
          internal static let title = L10n.tr("iOSApp", "camera.warning.incompatible.title")
        }
        internal enum NoQRCodeContained {
          /// The image you provided does not contain a processible QR code. Please try again with a different image.
          internal static let description = L10n.tr("iOSApp", "camera.warning.noQRCodeContained.description")
          /// Operation failed
          internal static let title = L10n.tr("iOSApp", "camera.warning.noQRCodeContained.title")
        }
        internal enum Processing {
          /// Wrong format
          internal static let title = L10n.tr("iOSApp", "camera.warning.processing.title")
          internal enum NotCheckin {
            /// The detected QR code cannot be used to check in.
            internal static let description = L10n.tr("iOSApp", "camera.warning.processing.notCheckin.description")
          }
          internal enum NotDocument {
            /// The detected QR code is not a vaccination or recovery certificate.
            internal static let description = L10n.tr("iOSApp", "camera.warning.processing.notDocument.description")
          }
          internal enum NotPayment {
            /// The detected QR code is not a valid Payment.
            internal static let description = L10n.tr("iOSApp", "camera.warning.processing.notPayment.description")
          }
          internal enum NotURL {
            /// The detected QR code is not a weblink.
            internal static let description = L10n.tr("iOSApp", "camera.warning.processing.notURL.description")
          }
        }
      }
    }
    internal enum Checkin {
      /// Check-in
      internal static let noun = L10n.tr("iOSApp", "checkin.noun")
      internal enum Buttons {
        internal enum Report {
          /// Report
          internal static let title = L10n.tr("iOSApp", "checkin.buttons.report.title")
        }
      }
      internal enum Consent {
        /// You want to check in at %@?
        internal static func description(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "checkin.consent.description", String(describing: p1))
        }
        /// Confirm & Don't ask again
        internal static let dontAskAgain = L10n.tr("iOSApp", "checkin.consent.dontAskAgain")
      }
      internal enum Failure {
        internal enum AlreadyCheckedIn {
          /// You are already checked in.
          internal static let message = L10n.tr("iOSApp", "checkin.failure.alreadyCheckedIn.message")
        }
        internal enum MissingIsPrivateFlag {
          /// Check-in not possible. Apparently, this QR-Code is not valid.
          internal static let message = L10n.tr("iOSApp", "checkin.failure.missingIsPrivateFlag.message")
        }
        internal enum NotAvailableAnymore {
          /// It is not possible to check in here anymore.
          internal static let message = L10n.tr("iOSApp", "checkin.failure.notAvailableAnymore.message")
        }
        internal enum PrivateMeetingRunning {
          /// You can't check in while you are hosting a private meeting. End the meeting and try again.
          internal static let message = L10n.tr("iOSApp", "checkin.failure.privateMeetingRunning.message")
        }
      }
      internal enum Kids {
        /// Add kid
        internal static let add = L10n.tr("iOSApp", "checkin.kids.add")
        /// Add another child
        internal static let addAnother = L10n.tr("iOSApp", "checkin.kids.addAnother")
        /// Checkin kids
        internal static let checkin = L10n.tr("iOSApp", "checkin.kids.checkin")
        /// Select the kids that shall be checked in with you. Your selection will not be saved und needs to be re-submitted on every checkin.
        internal static let description = L10n.tr("iOSApp", "checkin.kids.description")
      }
      internal enum Local {
        /// Report
        internal static let report = L10n.tr("iOSApp", "checkin.local.report")
        /// Welcome!
        internal static let welcome = L10n.tr("iOSApp", "checkin.local.welcome")
        internal enum ContactTracingButton {
          /// Activate contact tracing
          internal static let activate = L10n.tr("iOSApp", "checkin.local.contactTracingButton.activate")
          /// Show contact tracing
          internal static let show = L10n.tr("iOSApp", "checkin.local.contactTracingButton.show")
        }
        internal enum Payment {
          /// Start payment process
          internal static let button = L10n.tr("iOSApp", "checkin.local.payment.button")
          /// Payment powered by Rapyd.
          internal static let rapydInfo = L10n.tr("iOSApp", "checkin.local.payment.rapydInfo")
        }
      }
      internal enum Qr {
        /// This is your QR code. To check in, have it scanned at a luca location.
        internal static let description = L10n.tr("iOSApp", "checkin.qr.description")
        /// Share 2G/3G status
        internal static let shareHealthStatusLabel = L10n.tr("iOSApp", "checkin.qr.shareHealthStatusLabel")
        /// Check-in
        internal static let title = L10n.tr("iOSApp", "checkin.qr.title")
        internal enum Enlarge {
          /// Enlarge QR code
          internal static let accessibilty = L10n.tr("iOSApp", "checkin.qr.enlarge.accessibilty")
        }
        internal enum Title {
          /// My QR-Code
          internal static let myQRCode = L10n.tr("iOSApp", "checkin.qr.title.myQRCode")
          internal enum MyQRCode {
            /// Enlarge my QR-Code
            internal static let expand = L10n.tr("iOSApp", "checkin.qr.title.myQRCode.expand")
          }
        }
      }
      internal enum Scanner {
        /// Scan the QR code of a place that uses luca or a private meeting to check yourself in.
        internal static let description = L10n.tr("iOSApp", "checkin.scanner.description")
        /// The check-in can't be performed, because the contact data you entered aren't complete. Please go to the account tab, complete your contact data and then try again.
        internal static let incompleteContactDataDescription = L10n.tr("iOSApp", "checkin.scanner.incompleteContactDataDescription")
        /// Check-in failed
        internal static let incompleteContactDataTitle = L10n.tr("iOSApp", "checkin.scanner.incompleteContactDataTitle")
        /// You are currently unable to check in. Check your connection and try again. Please contact us if your check-in still does not work.
        internal static let publicKeyOutdatedDescription = L10n.tr("iOSApp", "checkin.scanner.publicKeyOutdatedDescription")
        /// Check-in not possible
        internal static let publicKeyOutdatedTitle = L10n.tr("iOSApp", "checkin.scanner.publicKeyOutdatedTitle")
        /// Self check-in
        internal static let title = L10n.tr("iOSApp", "checkin.scanner.title")
      }
      internal enum Slider {
        /// %@
        internal static func date(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "checkin.slider.date", String(describing: p1))
        }
        /// dd.MM.yyyy HH:mm
        internal static let dateFormat = L10n.tr("iOSApp", "checkin.slider.dateFormat")
        internal enum Date {
          /// Check-in: %@
          internal static func accessibility(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "checkin.slider.date.accessibility", String(describing: p1))
          }
        }
      }
      internal enum Url {
        /// Link: %@
        internal static func pasteboard(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "checkin.url.pasteboard", String(describing: p1))
        }
        internal enum Alert {
          /// %@\n\nLink was copied to your clipboard.
          internal static func description(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "checkin.url.alert.description", String(describing: p1))
          }
        }
      }
      internal enum Urls {
        /// General info
        internal static let general = L10n.tr("iOSApp", "checkin.urls.general")
        /// Location Map
        internal static let map = L10n.tr("iOSApp", "checkin.urls.map")
        /// Menu
        internal static let menu = L10n.tr("iOSApp", "checkin.urls.menu")
        /// Report
        internal static let report = L10n.tr("iOSApp", "checkin.urls.report")
        /// Program
        internal static let schedule = L10n.tr("iOSApp", "checkin.urls.schedule")
        /// Website
        internal static let website = L10n.tr("iOSApp", "checkin.urls.website")
      }
    }
    internal enum Checkout {
      /// Checkout
      internal static let noun = L10n.tr("iOSApp", "checkout.noun")
      /// Checkout now
      internal static let now = L10n.tr("iOSApp", "checkout.now")
    }
    internal enum Children {
      internal enum Add {
        /// Add
        internal static let button = L10n.tr("iOSApp", "children.add.button")
        /// Please provide your child's full name. Documents can only be added if the names match.
        internal static let description = L10n.tr("iOSApp", "children.add.description")
        /// Add Child
        internal static let title = L10n.tr("iOSApp", "children.add.title")
        internal enum Button {
          internal enum Deactivated {
            /// Add deactivated. Please fill out all text fields first.
            internal static let accessibility = L10n.tr("iOSApp", "children.add.button.deactivated.accessibility")
          }
        }
        internal enum Placeholder {
          /// Firstname
          internal static let firstname = L10n.tr("iOSApp", "children.add.placeholder.firstname")
          /// Lastname
          internal static let lastname = L10n.tr("iOSApp", "children.add.placeholder.lastname")
        }
      }
      internal enum List {
        /// You can add children (up to 14 years) to your app to import their documents and check them in with you. The information will not be shared with the health department, it is for your convenience only.
        internal static let description = L10n.tr("iOSApp", "children.list.description")
        /// You can add children (up to 14 years) to your app to import their documents and check them in with you.\n\nPlease make sure that the provided data is correct. The information will not be shared with the health department, it is for your convenience only.
        internal static let emptyDescription = L10n.tr("iOSApp", "children.list.emptyDescription")
        /// Add Children
        internal static let title = L10n.tr("iOSApp", "children.list.title")
        internal enum Add {
          /// Add Child
          internal static let button = L10n.tr("iOSApp", "children.list.add.button")
        }
        internal enum Delete {
          /// When you delete a child, all the related documents are also deleted.
          internal static let message = L10n.tr("iOSApp", "children.list.delete.message")
          /// Save changes?
          internal static let title = L10n.tr("iOSApp", "children.list.delete.title")
        }
      }
      internal enum Number {
        /// 1 child added
        internal static let one = L10n.tr("iOSApp", "children.number.one")
        /// %@ children added
        internal static func other(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "children.number.other", String(describing: p1))
        }
        /// No children added
        internal static let zero = L10n.tr("iOSApp", "children.number.zero")
      }
    }
    internal enum Contact {
      internal enum Qr {
        /// Scan QR code
        internal static let scan = L10n.tr("iOSApp", "contact.qr.scan")
        internal enum Accessibility {
          /// QR Code
          internal static let qrCode = L10n.tr("iOSApp", "contact.qr.accessibility.qrCode")
          /// QR Code Scanner
          internal static let qrCodeScanner = L10n.tr("iOSApp", "contact.qr.accessibility.qrCodeScanner")
        }
        internal enum Button {
          /// CLOSE QR CODE SCANNER
          internal static let closeScanner = L10n.tr("iOSApp", "contact.qr.button.closeScanner")
          /// SELF CHECK-IN
          internal static let selfCheckin = L10n.tr("iOSApp", "contact.qr.button.selfCheckin")
        }
      }
    }
    internal enum ContactViewController {
      /// Save
      internal static let save = L10n.tr("iOSApp", "contactViewController.save")
      internal enum ChangedName {
        /// Continue
        internal static let action = L10n.tr("iOSApp", "contactViewController.changedName.action")
        /// You are about to change your name. Changing your name will delete the documents related to that name in the app. This change will disable luca Connect and personal notifications from your current health department. You can re-enable it in the "Notifications" tab or in your "Account" tab. Do you want to continue?
        internal static let message = L10n.tr("iOSApp", "contactViewController.changedName.message")
        /// Change name?
        internal static let title = L10n.tr("iOSApp", "contactViewController.changedName.title")
      }
      internal enum ChangedPostCode {
        /// Change anyway
        internal static let action = L10n.tr("iOSApp", "contactViewController.changedPostCode.action")
        /// You are in the process of changing your zip code. This change will disable luca Connect and personal notifications from your current health department. You can re-enable notifications in the "Notifications" tab or in your "Account" tab if your new health department uses luca and this feature.
        internal static let message = L10n.tr("iOSApp", "contactViewController.changedPostCode.message")
        /// Change zip code?
        internal static let title = L10n.tr("iOSApp", "contactViewController.changedPostCode.title")
      }
      internal enum EmptyAddress {
        /// You must enter a complete address!
        internal static let message = L10n.tr("iOSApp", "contactViewController.emptyAddress.message")
      }
      internal enum EmptyRest {
        /// You must fill out all required fields.
        internal static let message = L10n.tr("iOSApp", "contactViewController.emptyRest.message")
      }
      internal enum SaveFailed {
        /// Save failed: %@
        internal static func message(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "contactViewController.saveFailed.message", String(describing: p1))
        }
      }
      internal enum ShouldSave {
        /// Apply all changes? If you've changed your name and it doesn't match the name in your imported tests, these might be deleted.
        internal static let message = L10n.tr("iOSApp", "contactViewController.shouldSave.message")
        /// Save changes
        internal static let title = L10n.tr("iOSApp", "contactViewController.shouldSave.title")
      }
    }
    internal enum CountryRejectionViewController {
      /// Unfortunately, luca is currently not available in this country. We hope, that we can offer luca to locations and guests in your country. If you want to be informed about news around Luca? Please register for our newsletter!
      internal static let description = L10n.tr("iOSApp", "countryRejectionViewController.description")
      /// https://www.luca-app.de/newsletter-en/
      internal static let link = L10n.tr("iOSApp", "countryRejectionViewController.link")
      /// newsletter
      internal static let term = L10n.tr("iOSApp", "countryRejectionViewController.term")
      /// Oh no!
      internal static let title = L10n.tr("iOSApp", "countryRejectionViewController.title")
    }
    internal enum Crypto {
      internal enum Info {
        /// Date:
        internal static let date = L10n.tr("iOSApp", "crypto.info.date")
        /// Every day a new daily key is created by one of the health departments that use luca. The health departments need this key to decrypt check-in data. Here you can see which health department created the key that's currently in use.
        internal static let description = L10n.tr("iOSApp", "crypto.info.description")
        /// Issuer:
        internal static let issuer = L10n.tr("iOSApp", "crypto.info.issuer")
        /// Signed:
        internal static let signed = L10n.tr("iOSApp", "crypto.info.signed")
        internal enum Download {
          /// DOWNLOAD CERTIFICATE CHAIN
          internal static let uppercased = L10n.tr("iOSApp", "crypto.info.download.uppercased")
        }
      }
    }
    internal enum DailyKey {
      internal enum Fetch {
        internal enum FailedToDownload {
          /// The luca Service is currently not available, please try again later.
          internal static let message = L10n.tr("iOSApp", "dailyKey.fetch.failedToDownload.message")
        }
        internal enum FailedToSave {
          /// The daily key could not be saved! %@
          internal static func message(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "dailyKey.fetch.failedToSave.message", String(describing: p1))
          }
        }
      }
    }
    internal enum Data {
      internal enum Access {
        /// SHOW
        internal static let showButton = L10n.tr("iOSApp", "data.access.showButton")
        /// New data request
        internal static let title = L10n.tr("iOSApp", "data.access.title")
        internal enum Empty {
          internal enum State {
            /// No data requests from health departments in the last 14 days.
            internal static let description = L10n.tr("iOSApp", "data.access.empty.state.description")
          }
        }
        internal enum Notification {
          /// A health department has requested your contact data.
          internal static let description = L10n.tr("iOSApp", "data.access.notification.description")
        }
        internal enum Title {
          /// Show new data request
          internal static let accessibility = L10n.tr("iOSApp", "data.access.title.accessibility")
        }
        internal enum WarningLevel1 {
          /// Du hast eine neue Datenanfrage.
          internal static let title = L10n.tr("iOSApp", "data.access.warningLevel1.title")
        }
        internal enum WarningLevel2 {
          /// Du hast dich an einer besonders kritischen Stelle aufgehalten und wirst gewarnt.
          internal static let title = L10n.tr("iOSApp", "data.access.warningLevel2.title")
        }
      }
      internal enum Clear {
        /// Do you really want to clear your history?\n\nPast entries won't be displayed in your app anymore, but will stay in the system for up to 28 days and will be shared with the health authorities if you share your data.
        internal static let description = L10n.tr("iOSApp", "data.clear.description")
        /// Clear history
        internal static let title = L10n.tr("iOSApp", "data.clear.title")
      }
      internal enum ResetData {
        /// Do you really want to delete your account?\n\nIf you delete your account, you will not be able to check in or access your history.\n\nIn accordance with the Corona/COVID-19 infection control regulations, all encrypted check-in data will be deleted after 4 weeks.\n\nOnce your account is deleted, you can still be notified by a health department up to 4 weeks after your last check-in.
        internal static let description = L10n.tr("iOSApp", "data.resetData.description")
        /// Delete account
        internal static let title = L10n.tr("iOSApp", "data.resetData.title")
      }
      internal enum Shared {
        /// You have shared your check-ins from the last %@ days with the health department.
        internal static func description(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "data.shared.description", String(describing: p1))
        }
        /// Data shared
        internal static let title = L10n.tr("iOSApp", "data.shared.title")
      }
    }
    internal enum DataPrivacy {
      internal enum Info {
        /// In the next step, you can enter your contact data if you want to.
        internal static let description = L10n.tr("iOSApp", "dataPrivacy.info.description")
        /// GOT IT!
        internal static let okButton = L10n.tr("iOSApp", "dataPrivacy.info.okButton")
        /// Your contact data
        internal static let title = L10n.tr("iOSApp", "dataPrivacy.info.title")
      }
    }
    internal enum DataRelease {
      internal enum Tan {
        /// TAN
        internal static let title = L10n.tr("iOSApp", "dataRelease.tan.title")
        internal enum Failure {
          /// The TAN could not be loaded: %@
          internal static func message(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "dataRelease.tan.failure.message", String(describing: p1))
          }
        }
      }
    }
    internal enum DataReport {
      internal enum AccountSettings {
        /// Covid certificates
        internal static let certificatesReport = L10n.tr("iOSApp", "dataReport.accountSettings.certificatesReport")
        /// Contact tracing
        internal static let contactReport = L10n.tr("iOSApp", "dataReport.accountSettings.contactReport")
        /// luca ID
        internal static let lucaIDReport = L10n.tr("iOSApp", "dataReport.accountSettings.lucaIDReport")
        /// luca Pay
        internal static let lucaPay = L10n.tr("iOSApp", "dataReport.accountSettings.lucaPay")
        /// Request data
        internal static let title = L10n.tr("iOSApp", "dataReport.accountSettings.title")
      }
      internal enum ContactData {
        /// Check in time: %@\nDuration: %@\nLocation: %@
        internal static func checkIn(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
          return L10n.tr("iOSApp", "dataReport.contactData.checkIn", String(describing: p1), String(describing: p2), String(describing: p3))
        }
        /// Contact tracing:\nYour data collected and processed for contact tracing is stored locally in your app and encrypted on the luca server. The luca server is hosted by Deutsche Telekom AG. Certification of this can be found at: https://open-telekom-cloud.com/de/sicherheit/datenschutz-compliance. Furthermore, Bundesdruckerei Gruppe GmbH provides other IT infrastructure services that enable secure transmission of your data to health authorities. Our subcontractor neXenio provides software development, software maintenance and software operation services. Neither we nor neXenio GmbH can decrypt or clearly view your data at any time. During registration we verify your phone number by sending an automated SMS. For this purpose, your phone number will be sent to our SMS service provider. The current recipients can be found in our current privacy policy: https://www.luca-app.de/app-privacy-policy/\n\nOnly you can view, edit and delete your data within your app. If you have already checked in at a luca location and a health department has requested it from the operator, the dual-encrypted data will be decrypted by the operator and then by the health department. Only the health department can access the decrypted data while contact tracing and view your data. With the exception of the health authorities (legal basis Art. 6 para. 1 lit. c DSGVO in conjunction with the respective state ordinance to combat COVID-19 infections, Infection Protection Act), we do not pass on your data to third parties.
        internal static let header = L10n.tr("iOSApp", "dataReport.contactData.header")
        /// Private meeting\nHost name: %@\nCheck in time: %@\nDuration: %@
        internal static func privateMeetingCheckIn(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
          return L10n.tr("iOSApp", "dataReport.contactData.privateMeetingCheckIn", String(describing: p1), String(describing: p2), String(describing: p3))
        }
        /// Private meeting\nGuests: %@\nCheck in time: %@\nDuration: %@
        internal static func privateMeetingHost(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
          return L10n.tr("iOSApp", "dataReport.contactData.privateMeetingHost", String(describing: p1), String(describing: p2), String(describing: p3))
        }
        /// Trace Info\nCheck in Zeit: %@\nCheck out Zeit: %@\nLocation Id: %@
        internal static func traceInfo(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
          return L10n.tr("iOSApp", "dataReport.contactData.traceInfo", String(describing: p1), String(describing: p2), String(describing: p3))
        }
        /// Contact data:\nName: %@\nAddress: %@\nPhone number: %@\nMail: %@\nStorage location: luca-Server, local\nStorage period: Deletion takes place 28 days after the deletion of the account (delete button) or annually at the end of each year
        internal static func userData(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any) -> String {
          return L10n.tr("iOSApp", "dataReport.contactData.userData", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4))
        }
        /// Date Transfer\nTime: %@\nNumber of shared days: %i
        internal static func userDataTransfer(_ p1: Any, _ p2: Int) -> String {
          return L10n.tr("iOSApp", "dataReport.contactData.userDataTransfer", String(describing: p1), p2)
        }
        internal enum CheckIn {
          /// Currently checked in
          internal static let currentlyCheckedIn = L10n.tr("iOSApp", "dataReport.contactData.checkIn.currentlyCheckedIn")
        }
      }
      internal enum Document {
        /// Covid certificates:\nWithin your luca app, you have the option to store a test result, vaccination or recovery certificate. Your data stored and transferred to the luca app will not leave your smartphone. Your data is only stored locally in your app on your smartphone. A transmission to our servers, to the operator or to the health authorities does not take place. Your data will not be passed on to third parties.\nOnly information necessary for identity matching (first and last name, date of birth, vaccination date, issuer and vaccine) is displayed in the app. Other data may be included in the test or ID document, but will not be displayed to you. All data contained in the QR code is listed here. To prevent misuse, so that the same document cannot be stored by different people in the luca app, a pseudonymized identifier is created by your app and transmitted to the luca system. Only the identifier is stored in the luca system. We cannot assign it to you.
        internal static let header = L10n.tr("iOSApp", "dataReport.document.header")
        internal enum Appointment {
          /// %@\nCertificate information:\n%@\nQR-Code: %@\nStorage location: local\n%@
          internal static func description(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any) -> String {
            return L10n.tr("iOSApp", "dataReport.document.appointment.description", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4))
          }
        }
        internal enum Certificate {
          /// %@\nCertificate information:\n%@\nQR-Code: %@\nStorage location: local\n%@
          internal static func description(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any) -> String {
            return L10n.tr("iOSApp", "dataReport.document.certificate.description", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4))
          }
          /// Storage period: Deletion takes place via deletion in the app or as soon as validity is exceeded
          internal static let storagePeriodAppointment = L10n.tr("iOSApp", "dataReport.document.certificate.storagePeriodAppointment")
          /// Storage period: Deletion takes place via deletion in the app or after %@
          internal static func storagePeriodTest(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "dataReport.document.certificate.storagePeriodTest", String(describing: p1))
          }
          /// Storage period: Deletion takes place via deletion in the app
          internal static let storagePeriodVaccine = L10n.tr("iOSApp", "dataReport.document.certificate.storagePeriodVaccine")
        }
        internal enum CertificateType {
          /// Certificate type: Appointment
          internal static let appointment = L10n.tr("iOSApp", "dataReport.document.certificateType.appointment")
          /// Certificate type: Test(BärCode Format)
          internal static let baercodeTest = L10n.tr("iOSApp", "dataReport.document.certificateType.baercodeTest")
          /// Certificate type: Vaccination (BärCode)
          internal static let baercodeVaccination = L10n.tr("iOSApp", "dataReport.document.certificateType.baercodeVaccination")
          /// Certificate type: Recovery (DGC)
          internal static let dgcRecovery = L10n.tr("iOSApp", "dataReport.document.certificateType.dgcRecovery")
          /// Certificate type: Test (DGC)
          internal static let dgcTest = L10n.tr("iOSApp", "dataReport.document.certificateType.dgcTest")
          /// Certificate type: Vaccination (DGC)
          internal static let dgcVaccination = L10n.tr("iOSApp", "dataReport.document.certificateType.dgcVaccination")
          /// Certificate type: Test(ticket.io Format)
          internal static let ticketIOTest = L10n.tr("iOSApp", "dataReport.document.certificateType.ticketIOTest")
        }
        internal enum ContactData {
          /// Date of birth: %@
          internal static func dateOfBirth(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "dataReport.document.contactData.dateOfBirth", String(describing: p1))
          }
          /// First name: %@\nLast name: %@
          internal static func general(_ p1: Any, _ p2: Any) -> String {
            return L10n.tr("iOSApp", "dataReport.document.contactData.general", String(describing: p1), String(describing: p2))
          }
          /// Name Hash: %@
          internal static func unknownUser(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "dataReport.document.contactData.unknownUser", String(describing: p1))
          }
        }
        internal enum DescriptionDetails {
          /// Test type: %@\nTest centre: %@\nDate: %@\nAddress of test centre: %@
          internal static func appointment(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any) -> String {
            return L10n.tr("iOSApp", "dataReport.document.descriptionDetails.appointment", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4))
          }
          /// Result: %@\nIssuer: %@
          internal static func baercodeTest(_ p1: Any, _ p2: Any) -> String {
            return L10n.tr("iOSApp", "dataReport.document.descriptionDetails.baercodeTest", String(describing: p1), String(describing: p2))
          }
          /// Targeted disease: %i\nProcedure operator: %@
          internal static func baercodeVaccination(_ p1: Int, _ p2: Any) -> String {
            return L10n.tr("iOSApp", "dataReport.document.descriptionDetails.baercodeVaccination", p1, String(describing: p2))
          }
          /// Disease or agent targeted "tg": %@\nISO 8601 complete date of first positive NAA test result "fr": %@\nCountry of Test "co": %@\nCertificate Issuer "is": %@\nISO 8601 complete date: Certificate Valid From "df": %@\nISO 8601 complete date: Certificate Valid Unti "du": %@\nUnique Certificate Identifier: UVCI "ci": %@
          internal static func dgcRecovery(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any, _ p5: Any, _ p6: Any, _ p7: Any) -> String {
            return L10n.tr("iOSApp", "dataReport.document.descriptionDetails.dgcRecovery", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4), String(describing: p5), String(describing: p6), String(describing: p7))
          }
          /// Targeted disease "tg": %@\nType of test "tt": %@\nDate/Time of Sample Collection "sc": %@\nTest Result "tr": %@\nTesting Centre "tc": %@\nCountry of Test "co": %@\nCertificate Issuer "is": %@\nUnique Certificate Identifier, UVCI "ci": %@
          internal static func dgcTest(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any, _ p5: Any, _ p6: Any, _ p7: Any, _ p8: Any) -> String {
            return L10n.tr("iOSApp", "dataReport.document.descriptionDetails.dgcTest", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4), String(describing: p5), String(describing: p6), String(describing: p7), String(describing: p8))
          }
          /// Disease or agent targeted "tg": %@\nVaccine or prophylaxis "vp": %@\nVaccine medicinal product "mp": %@\nMarketing Authorization Holder - if no MAH present, then manufacturer "ma": %@\nDose Number "dn": %i\nTotal Series of Doses "sd": %i\nISO8601 complete date: Date of Vaccination "dt": %@\nCountry of Vaccination "co": %@\nCertificate Issuer "is":  %@\nUnique Certificate Identifier: UVCI "ci": %@
          internal static func dgcVaccination(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any, _ p5: Int, _ p6: Int, _ p7: Any, _ p8: Any, _ p9: Any, _ p10: Any) -> String {
            return L10n.tr("iOSApp", "dataReport.document.descriptionDetails.dgcVaccination", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4), p5, p6, String(describing: p7), String(describing: p8), String(describing: p9), String(describing: p10))
          }
          /// Result: %@\nTest type: %@\nDate: %@\nIssuer: %@\nDoc: %@
          internal static func ticketIOTest(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any, _ p5: Any) -> String {
            return L10n.tr("iOSApp", "dataReport.document.descriptionDetails.ticketIOTest", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4), String(describing: p5))
          }
          internal enum Baercode {
            /// Procedure %i: %@ %@
            internal static func procedure(_ p1: Int, _ p2: Any, _ p3: Any) -> String {
              return L10n.tr("iOSApp", "dataReport.document.descriptionDetails.baercode.procedure", p1, String(describing: p2), String(describing: p3))
            }
          }
        }
      }
      internal enum LucaID {
        /// Luca ID\nIf you have explicitly given your consent in accordance with Article Art. 9 (2) a) in combination with Article 6 (1) 1 a) DSGVO and have stored your identity document in your Luca App, your personal data has been processed.\nAs part of the identification process, photos and images of you and the ID documents used, as well as all personal data contained on the ID document, are transmitted to our subcontractor and processed by them. The server is located in Germany.\nAfter successful identification, your data signed by IDnow is encrypted and transferred to your own luca app via the luca system. The luca server is hosted by Deutsche Telekom AG. Certification for this can be found at: https://open-telekom-cloud.com/de/sicherheit/datenschutz-compliance. Our subcontractor neXenio provides software development, software maintenance and software operation services. Neither we nor neXenio GmbH can decrypt your data at any time and view it in the clear form.
        internal static let header = L10n.tr("iOSApp", "dataReport.lucaID.header")
        internal enum IDnow {
          /// Decrypted Data (stored on IDNow-Server max. 7 days, stored in the luca app until manual deletion):
          internal static let decryptedData = L10n.tr("iOSApp", "dataReport.lucaID.IDnow.decryptedData")
          /// Encrypted Data (stored on IDNow-Server max. 7 days, stored in the luca app until manual deletion):
          internal static let encryptedData = L10n.tr("iOSApp", "dataReport.lucaID.IDnow.encryptedData")
          /// Signed Data (stored on the luca Server and in the luca App until manual deletion):
          internal static let signedData = L10n.tr("iOSApp", "dataReport.lucaID.IDnow.signedData")
          internal enum DecryptedData {
            /// Date of birth
            internal static let birthdayTimestamp = L10n.tr("iOSApp", "dataReport.lucaID.IDnow.decryptedData.birthdayTimestamp")
            /// First name
            internal static let firstName = L10n.tr("iOSApp", "dataReport.lucaID.IDnow.decryptedData.firstName")
            /// Last name
            internal static let lastName = L10n.tr("iOSApp", "dataReport.lucaID.IDnow.decryptedData.lastName")
          }
          internal enum EncryptedData {
            /// faceJWE
            internal static let faceJWE = L10n.tr("iOSApp", "dataReport.lucaID.IDnow.encryptedData.faceJWE")
            /// identityJWE
            internal static let identityJWE = L10n.tr("iOSApp", "dataReport.lucaID.IDnow.encryptedData.identityJWE")
            /// minimalIdentityJwe
            internal static let minimalIdentityJwe = L10n.tr("iOSApp", "dataReport.lucaID.IDnow.encryptedData.minimalIdentityJwe")
          }
          internal enum SignedData {
            /// enrollmentToken
            internal static let enrollmentToken = L10n.tr("iOSApp", "dataReport.lucaID.IDnow.signedData.enrollmentToken")
            /// revocationCode
            internal static let revocationCode = L10n.tr("iOSApp", "dataReport.lucaID.IDnow.signedData.revocationCode")
            /// verificationStatus
            internal static let verificationStatus = L10n.tr("iOSApp", "dataReport.lucaID.IDnow.signedData.verificationStatus")
          }
        }
      }
      internal enum Payment {
        /// \nName: %@\nType: %@\nLast 4 digits: %@\nExpiration year: %@\nExpiration month: %@
        internal static func card(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any, _ p5: Any) -> String {
          return L10n.tr("iOSApp", "dataReport.payment.card", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4), String(describing: p5))
        }
        /// User ID (local in your luca app, on the luca server and at Rapyd, deletion by app functionalities):\nUser ID: %@\nPayment information: %@
        internal static func data(_ p1: Any, _ p2: Any) -> String {
          return L10n.tr("iOSApp", "dataReport.payment.data", String(describing: p1), String(describing: p2))
        }
        /// Location: %@, Amount: %@, Tip: %@, Transaction ID: %@
        internal static func list(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any) -> String {
          return L10n.tr("iOSApp", "dataReport.payment.list", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4))
        }
        /// Not applicable
        internal static let notApplicable = L10n.tr("iOSApp", "dataReport.payment.notApplicable")
        /// If you have explicitly given your consent to the use of luca Pay in accordance with Article 6 (1) 1 a) DSGVO in combination with Article 49 (1) 1. a) and have stored your payment method information in the luca system or with our subcontractor Rapyd Europe, your personal data has been processed.\n\nIn addition to storing your means of payment, your luca app also provides you with a list of your payment transactions.\n\nYour payment method information is only stored with our subcontractor Rapyd Europe. Your personal payment history is stored on your own device within your luca app. However, information about your payments as well as your user ID are also stored at Rapyd Europe as well as in the luca system. You can view Rapyd Europe's privacy policy here: https://www.rapyd.net/privacypolicy/. The luca server is hosted by Deutsche Telekom AG. Our subcontractor neXenio provides software development, software maintenance and software operation services.
        internal static let `prefix` = L10n.tr("iOSApp", "dataReport.payment.prefix")
      }
      internal enum PrivacyPolicy {
        /// If you would like to know whether your data is stored by our customer IDnow, please provide us with your IDnow identity or revocation code. With the help of these codes, we can determine for you whether your personal data is held by our order taker Idnow.
        internal static let lucaID = L10n.tr("iOSApp", "dataReport.privacyPolicy.lucaID")
        /// All information about the processing, purposes, legal bases and deletion periods can be found in our privacy policy: https://www.luca-app.de/app-privacy-policy/. Your data will not be transferred to third countries. Automated decision-making and profiling according to Art. 22 DSGVO are not carried out by us or the contractors of the luca system.\nIf you would like to receive information about your data, which we have received as clear data through your own contact (e.g. support requests), please contact our Privacy Team at privacy@culture4life.de. Please share any other information that would help us identify you and research your related data with us. If you have been in contact with us via social media, information such as your e-mail address or the pseudonym you use in those social media channels are important in order to provide you with your requested information.
        internal static let `prefix` = L10n.tr("iOSApp", "dataReport.privacyPolicy.prefix")
        /// In accordance with Art. 77 DSGVO, you have the right to complain to the supervisory authority if you believe that your personal data is not processed lawfully. The address of the supervisory authority responsible for our company is: Berliner Beauftragte für Datenschutz und Informationsfreiheit, Friedrichstr. 219, 10969 Berlin, Tel.: 030/13889-0, e-mail: mailbox@datenschutz-berlin.de.
        internal static let suffix = L10n.tr("iOSApp", "dataReport.privacyPolicy.suffix")
        internal enum LucaID {
          /// IDnow identity code
          internal static let iDnowIdentityCode = L10n.tr("iOSApp", "dataReport.privacyPolicy.lucaID.IDnowIdentityCode")
          /// Revocation code
          internal static let iDnowRevocationCode = L10n.tr("iOSApp", "dataReport.privacyPolicy.lucaID.IDnowRevocationCode")
        }
      }
    }
    internal enum Document {
      internal enum Verification {
        internal enum Failed {
          /// Please check if your certificate has been issued by an authorized institution and get a new certificate if you are not sure. You cannot share the status of this certificate at check-in.
          internal static let description = L10n.tr("iOSApp", "document.verification.failed.description")
          /// We could not verify your certificate
          internal static let title = L10n.tr("iOSApp", "document.verification.failed.title")
        }
        internal enum Success {
          /// Certificate verified
          internal static let accessibility = L10n.tr("iOSApp", "document.verification.success.accessibility")
        }
      }
      internal enum View {
        internal enum Button {
          /// DELETE
          internal static let delete = L10n.tr("iOSApp", "document.view.button.delete")
        }
        internal enum Label {
          /// Birth date:
          internal static let birthDate = L10n.tr("iOSApp", "document.view.label.birthDate")
          /// Issued:
          internal static let issued = L10n.tr("iOSApp", "document.view.label.issued")
        }
      }
    }
    internal enum Done {
      /// You can now share your encrypted contact data by presenting your QR code or by scanning a code from a place that uses luca.\n\nChecking in is possible at places that use luca or private meetings with other luca App users.
      internal static let description = L10n.tr("iOSApp", "done.description")
      /// OKAY!
      internal static let okButton = L10n.tr("iOSApp", "done.okButton")
      /// That's it!
      internal static let title = L10n.tr("iOSApp", "done.title")
    }
    internal enum Error {
      internal enum DeleteUser {
        /// Your account has already been deleted.\n\nYou can now register again.
        internal static let alreadyDeleted = L10n.tr("iOSApp", "error.deleteUser.alreadyDeleted")
        /// Your account could not be deleted. (400 - Bad Request Error)\n\nPlease contact our support.
        internal static let badInput = L10n.tr("iOSApp", "error.deleteUser.badInput")
        /// Your account could not be deleted. (403 - Forbidden Error)\n\nPlease contact our support.
        internal static let invalidSignature = L10n.tr("iOSApp", "error.deleteUser.invalidSignature")
        /// Your account could not be deleted.\n\nPlease try again.
        internal static let rateLimit = L10n.tr("iOSApp", "error.deleteUser.rateLimit")
        /// Your account could not be deleted.\n\nPlease contact our support.
        internal static let unableToBuildPayload = L10n.tr("iOSApp", "error.deleteUser.unableToBuildPayload")
        /// Your account does not exist.\n\nYou can now register again.
        internal static let userNotFound = L10n.tr("iOSApp", "error.deleteUser.userNotFound")
      }
      internal enum Network {
        /// The luca Service is currently unavailable, please try again later.
        internal static let badGateway = L10n.tr("iOSApp", "error.network.badGateway")
      }
    }
    internal enum Explanation {
      internal enum Checkin {
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">We have news again!</p>\n                           <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">We worked hard to improve the check-in process for you.</p>
        internal static let _1 = L10n.tr("iOSApp", "explanation.checkin.1")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Share 2G/3G status at check-in</p>\n                           <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">Share your 2G/3G status directly through your luca QR code at check-in. Ease the scanning process for yourself and for operators. </p>
        internal static let _2 = L10n.tr("iOSApp", "explanation.checkin.2")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Menu and more</p>\n                           <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">Operators now have the possibility to add their website, menus, maps, programs and more. You can find any added information behind the info button after you've checked in.</p>
        internal static let _3 = L10n.tr("iOSApp", "explanation.checkin.3")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">That's it! </p>\n                           <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">You can find all information and even more explanation screens in the news section of the "Account" tab.</p>\n                           <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">PS: Don't forget to check out!</p>
        internal static let _4 = L10n.tr("iOSApp", "explanation.checkin.4")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">luca has a new look!</p>\n<p style="font-family: Montserrat-medium; font-size: bodySize; color: white">We redesigned the screen after check-in completely. Now you can find there all important information about the location where you have checked in at a glance. Simply scan the luca QR code to find the new view. We hope you like it!</p>
        internal static let redesign = L10n.tr("iOSApp", "explanation.checkin.redesign")
        /// Check-in
        internal static let title = L10n.tr("iOSApp", "explanation.checkin.title")
      }
      internal enum Contactdata {
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">No contact tracing</p>\n                            <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">Currently, contact tracing via luca is on hold. This means for you: Your contact information will no longer be recorded when you scan a QR code.</p>
        internal static let _1 = L10n.tr("iOSApp", "explanation.contactdata.1")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Optional contact data</p>\n                            <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">You no longer need to enter contact information to use luca. If you have imported documents, you must enter your first name and last name. Make sure that the names match the information on your certificates. All other information is optional.</p>
        internal static let _2 = L10n.tr("iOSApp", "explanation.contactdata.2")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">New Check-in</p>\n                            <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">luca is still available for you and offers you many advantages. You can check in as before and use the functions provided by the luca location.</p>
        internal static let _3 = L10n.tr("iOSApp", "explanation.contactdata.3")
        /// New Check-in
        internal static let title = L10n.tr("iOSApp", "explanation.contactdata.title")
      }
      internal enum Luca20 {
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Hooray! A lot has happened at luca.</p>\n                         <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">Swipe right to see what's new.</p>\n                         <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">No time right now? You can also find all the information in the "Account" tab.</p>
        internal static let _1 = L10n.tr("iOSApp", "explanation.luca20.1")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Extension of "My luca"</p>\n                         <p style="font-family: Montserrat-bold; font-size: bodySize; color: white">Add children</p>\n                         <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">You can now use luca for your children as well.</p>\n                         <p style="font-family: Montserrat-bold; font-size: bodySize; color: white">Important notifications</p>\n                         <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">You will receive any notifications or warnings here.</p>
        internal static let _2 = L10n.tr("iOSApp", "explanation.luca20.2")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Adding Children</p>\n                         <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">Add your children to check them in with you. You can even import documents for them, but make sure to input full and correct information.</p>
        internal static let _3 = L10n.tr("iOSApp", "explanation.luca20.3")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Optimization of warnings</p>\n                         <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">Our different warning levels can inform you in more detail, when you were checked in somewhere at the same time as an infected person. Affected events are marked in your history.</p>
        internal static let _4 = L10n.tr("iOSApp", "explanation.luca20.4")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Faster check-in</p>\n                         <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">When you press on "Check-in", luca now directly starts on the camera. Just activate the camera once and you're good to go. Access your personal QR code by clicking on the button below it.</p>
        internal static let _5 = L10n.tr("iOSApp", "explanation.luca20.5")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Relocation of your history</p>\n                         <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">You can now access your history via the clock icon in the top right corner of the "Check-in" tab.</p>\n                         <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">As always, your data cannot be viewed until you generate your TAN and share it with the health department.</p>
        internal static let _6 = L10n.tr("iOSApp", "explanation.luca20.6")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">That's it!</p>\n                         <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">You can find all info and even more explanation screens in the news section of the "Account" tab.</p>\n                         <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">PS: Don't forget to check out!</p>
        internal static let _7 = L10n.tr("iOSApp", "explanation.luca20.7")
        /// luca 2.0
        internal static let title = L10n.tr("iOSApp", "explanation.luca20.title")
      }
      internal enum LucaID {
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Create your luca ID now!</p>\n                           <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">Goodbye to tedious searching and full hands!\nYou will soon be able to verify your identity with your ID card in the luca app and create your luca ID.</p>\n                           <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">The luca ID is stored locally on your phone and can be used to prove your identity.</p>
        internal static let _1 = L10n.tr("iOSApp", "explanation.lucaID.1")
        /// luca ID
        internal static let title = L10n.tr("iOSApp", "explanation.lucaID.title")
      }
      internal enum Messages {
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">News in the luca app</p>\n                           <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">There is something new in the app again: the notifications tab!</p>
        internal static let _1 = L10n.tr("iOSApp", "explanation.messages.1")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">The new notifications tab</p>\n                           <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">The new notification tab displays notifications from health departments and the luca system.</p>
        internal static let _2 = L10n.tr("iOSApp", "explanation.messages.2")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">That's it! </p>\n                           <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">You can find all information and even more explanation screens in the news section of the "Account" tab.</p>\n                           <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">PS: Don't forget to check out!</p>
        internal static let _3 = L10n.tr("iOSApp", "explanation.messages.3")
        /// Notifications tab
        internal static let title = L10n.tr("iOSApp", "explanation.messages.title")
      }
      internal enum Payment {
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Already heard of luca Pay?</p>\n<p style="font-family: Montserrat-medium; font-size: bodySize; color: white">You can now pay in luca locations with luca Pay. Scan the table QR code or QR code on the operator's device, add a tip and you're done!</p>
        internal static let _1 = L10n.tr("iOSApp", "explanation.payment.1")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">luca Pay - fast and secure</p>\n<p style="font-family: Montserrat-medium; font-size: bodySize; color: white">You can find luca Pay in a new tab in your app. There you can activate luca Pay, enter your payment data and view an overview of your payments. The payment history is created for you and can be accessed via this smartphone.</p>
        internal static let _2 = L10n.tr("iOSApp", "explanation.payment.2")
      }
      internal enum Raffle {
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Win 500€ coupon now</p>\n<p style="font-family: Montserrat-medium; font-size: bodySize; color: white">In the period from June 7 to August 31, 2022, you have the chance to win a €500 meal voucher for the restaurant where you made the payment by paying with luca Pay.\nAt the same time, every win supports the restaurant: if you win, the staff will also receive €500.</p>
        internal static let _1 = L10n.tr("iOSApp", "explanation.raffle.1")
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Here's how it works</p>\n<p style="font-family: Montserrat-medium; font-size: bodySize; color: white">Participation is easy: You get a raffle ticket when you pay with luca Pay at participating locations. Then all you have to do is enter your e-mail address and confirm your participation - and you could win a €500 food voucher.</p>
        internal static let _2 = L10n.tr("iOSApp", "explanation.raffle.2")
      }
      internal enum Tiptopup {
        /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">We increase the tip</p>\n<p style="font-family: Montserrat-medium; font-size: bodySize; color: white">Paying with luca Pay doesn't only benefit you. Every time you tip with luca Pay during the campaign period, luca adds 10&#37; to your tip.</p>
        internal static let _1 = L10n.tr("iOSApp", "explanation.tiptopup.1")
      }
    }
    internal enum External {
      internal enum Link {
        internal enum Button {
          /// , opens external website
          internal static let accessibility = L10n.tr("iOSApp", "external.link.button.accessibility")
        }
      }
    }
    internal enum ExternalLinkConsent {
      /// Open
      internal static let button = L10n.tr("iOSApp", "externalLinkConsent.button")
      /// You're about to open %@'s %@. This link is provided by the luca Location you are checked into, luca has no control over the content. The following link will be opened:\n(%@)
      internal static func message(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
        return L10n.tr("iOSApp", "externalLinkConsent.message", String(describing: p1), String(describing: p2), String(describing: p3))
      }
      /// Open external link
      internal static let title = L10n.tr("iOSApp", "externalLinkConsent.title")
    }
    internal enum FetchTraceId {
      internal enum Failure {
        /// Error while checking the user status: %@
        internal static func message(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "fetchTraceId.failure.message", String(describing: p1))
        }
      }
    }
    internal enum FormViewController {
      internal enum PhoneVerificationFailure {
        /// Please enter a new phone number.
        internal static let message = L10n.tr("iOSApp", "formViewController.phoneVerificationFailure.message")
        /// Phone number verification failed
        internal static let title = L10n.tr("iOSApp", "formViewController.phoneVerificationFailure.title")
      }
      internal enum UserDataFailure {
        internal enum Failed {
          /// User could not be registered: %@
          internal static func message(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "formViewController.userDataFailure.failed.message", String(describing: p1))
          }
        }
        internal enum Unavailable {
          /// User data is not available
          internal static let message = L10n.tr("iOSApp", "formViewController.userDataFailure.unavailable.message")
        }
      }
      internal enum VerificationCancelled {
        /// Phone number unverified.
        internal static let message = L10n.tr("iOSApp", "formViewController.verificationCancelled.message")
      }
      internal enum VerificationUnconfirmed {
        /// Phone number unconfirmed.
        internal static let message = L10n.tr("iOSApp", "formViewController.verificationUnconfirmed.message")
      }
    }
    internal enum General {
      /// Privacy policy
      internal static let dataPrivacy = L10n.tr("iOSApp", "general.dataPrivacy")
      /// Don't ask again and remember setting
      internal static let dontAskAnymore = L10n.tr("iOSApp", "general.dontAskAnymore")
      /// FAQ
      internal static let faq = L10n.tr("iOSApp", "general.faq")
      /// Daily Key
      internal static let healthDepartmentKey = L10n.tr("iOSApp", "general.healthDepartmentKey")
      /// Imprint
      internal static let imprint = L10n.tr("iOSApp", "general.imprint")
      /// https://www.luca-app.de/impressum/
      internal static let linkImprint = L10n.tr("iOSApp", "general.linkImprint")
      /// News
      internal static let news = L10n.tr("iOSApp", "general.news")
      /// Support
      internal static let support = L10n.tr("iOSApp", "general.support")
      /// Terms of use
      internal static let termsAndConditions = L10n.tr("iOSApp", "general.termsAndConditions")
      /// luca
      internal static let title = L10n.tr("iOSApp", "general.title")
      /// Unknown
      internal static let unknown = L10n.tr("iOSApp", "general.unknown")
      internal enum Failure {
        internal enum InvalidCertificate {
          /// When attempting to establish a secure connection to the luca servers, we found untrusted certificates.\n\nPlease try switching your network and make sure that you're using the latest app version.
          internal static let message = L10n.tr("iOSApp", "general.failure.invalidCertificate.message")
          /// Unexpected server certificates
          internal static let title = L10n.tr("iOSApp", "general.failure.invalidCertificate.title")
        }
        internal enum NoInternet {
          /// The process could not be executed. Please connect to the internet and try again.
          internal static let message = L10n.tr("iOSApp", "general.failure.noInternet.message")
          /// No internet connection
          internal static let title = L10n.tr("iOSApp", "general.failure.noInternet.title")
        }
        internal enum Unknown {
          /// Unknown error: %@
          internal static func message(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "general.failure.unknown.message", String(describing: p1))
          }
        }
      }
      internal enum Gitlab {
        internal enum Alert {
          /// Open GitLab
          internal static let actionButton = L10n.tr("iOSApp", "general.gitlab.alert.actionButton")
          /// luca is open source. It's important to us that everyone can comprehend how the luca system works and that it's possible to give independent feedback. If you want to take a closer look, you'll find the code repository on GitLab.
          internal static let description = L10n.tr("iOSApp", "general.gitlab.alert.description")
          /// Take me to the code repository 
          internal static let title = L10n.tr("iOSApp", "general.gitlab.alert.title")
        }
      }
      internal enum Greeting {
        /// Hello!
        internal static let title = L10n.tr("iOSApp", "general.greeting.title")
      }
      internal enum OperationFailure {
        internal enum Error {
          /// Operation failed
          internal static let title = L10n.tr("iOSApp", "general.operationFailure.error.title")
        }
      }
      internal enum Support {
        /// hello@luca-app.de
        internal static let email = L10n.tr("iOSApp", "general.support.email")
        /// Emails cannot be sent from this device because no email account is linked.
        internal static let error = L10n.tr("iOSApp", "general.support.error")
        internal enum Email {
          /// Dear luca support,<br><br>The following problem occurred while using the app:<br><br><i>Please describe your problem and where it occurs, as precisely as possible. It would also be very helpful if you could attach a screenshot or screen capture of your problem or publicKeyOutdatedTitle message.</i><br><br><br>Device: %@<br>Operating system: iOS %@<br>App version: %@
          internal static func body(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
            return L10n.tr("iOSApp", "general.support.email.body", String(describing: p1), String(describing: p2), String(describing: p3))
          }
        }
      }
    }
    internal enum HealthStatusConsent {
      /// I hereby give my consent in accordance with Art. 9 (2) a) in combination with Art. 6 (1) 1 a) DSGVO to process the status of my stored COVID test, recovery or vaccination document (tested negative, recovered or vaccinated). At check-in, my luca app checks whether my imported document matches the location's entry requirement. My luca app displays whether I fulfill the entry requirement of the location I am checked into. My document or the data and information it contains will not be stored or transmitted outside of my luca app.
      internal static let description = L10n.tr("iOSApp", "healthStatusConsent.description")
      /// Consent
      internal static let title = L10n.tr("iOSApp", "healthStatusConsent.title")
      internal enum InvalidDocumentsAlert {
        /// We could not verify your certificate. Please check if your certificate has been issued by an authorized institution and get a new certificate if you are not sure. You cannot share the status of this certificate at check-in. 
        internal static let message = L10n.tr("iOSApp", "healthStatusConsent.invalidDocumentsAlert.message")
        /// No valid documents
        internal static let title = L10n.tr("iOSApp", "healthStatusConsent.invalidDocumentsAlert.title")
      }
      internal enum NoDocumentsAlert {
        /// You currently have no documents in your "My luca" tab and therefore cannot share your 2G/3G status. Manually show your 2G/3G document as soon as you are checked in. If you want to automatically share your 2G/3G status for the next check-in, add your document in the "My luca" tab.
        internal static let message = L10n.tr("iOSApp", "healthStatusConsent.noDocumentsAlert.message")
        /// No documents
        internal static let title = L10n.tr("iOSApp", "healthStatusConsent.noDocumentsAlert.title")
      }
    }
    internal enum History {
      internal enum Accessibility {
        /// View data accesses
        internal static let dataAccessButton = L10n.tr("iOSApp", "history.accessibility.dataAccessButton")
      }
      internal enum Alert {
        /// Would you like to share your contact data and the last %@ days of your history with the health department? This is only done with your voluntary consent in accordance with art. 9 (2) a) GDPR (more: %@).
        internal static func description(_ p1: Any, _ p2: Any) -> String {
          return L10n.tr("iOSApp", "history.alert.description", String(describing: p1), String(describing: p2))
        }
        /// data privacy - app
        internal static let link = L10n.tr("iOSApp", "history.alert.link")
        /// Confirm share
        internal static let title = L10n.tr("iOSApp", "history.alert.title")
        internal enum Description {
          /// Would you like to share your contact data and the last 14 days of your history with the health department? This is only done with your voluntary consent under article 9 (2) a) GDPR (more: %@).
          internal static func accessibility(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "history.alert.description.accessibility", String(describing: p1))
          }
        }
      }
      internal enum Checkin {
        internal enum Checkout {
          /// %@ to %@
          internal static func time(_ p1: Any, _ p2: Any) -> String {
            return L10n.tr("iOSApp", "history.checkin.checkout.time", String(describing: p1), String(describing: p2))
          }
        }
      }
      internal enum Data {
        /// Your history collects all your activities from the last 28 days. You don't have any entries during this period at the moment.
        internal static let empty = L10n.tr("iOSApp", "history.data.empty")
        /// Share history
        internal static let share = L10n.tr("iOSApp", "history.data.share")
        /// Shared data
        internal static let shared = L10n.tr("iOSApp", "history.data.shared")
        /// Updated data
        internal static let updated = L10n.tr("iOSApp", "history.data.updated")
        internal enum Share {
          internal enum Deactivated {
            /// Share history deactivated. To activate, you need entries in your history.
            internal static let accessibility = L10n.tr("iOSApp", "history.data.share.deactivated.accessibility")
          }
        }
      }
      internal enum DataAccess {
        /// %@ data requests
        internal static func count(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "history.dataAccess.count", String(describing: p1))
        }
      }
      internal enum Delete {
        /// Delete history
        internal static let button = L10n.tr("iOSApp", "history.delete.button")
      }
      internal enum Detail {
        internal enum PrivateMeeting {
          /// Private meetings are meant for small gatherings with friends and family. Private meetings are used as reminders and won't be shared with health authorities - that's why they display the full names of you and your guests.
          internal static let information = L10n.tr("iOSApp", "history.detail.privateMeeting.information")
          /// Information
          internal static let title = L10n.tr("iOSApp", "history.detail.privateMeeting.title")
        }
      }
      internal enum Kids {
        /// Kids: %@
        internal static func title(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "history.kids.title", String(describing: p1))
        }
      }
      internal enum Share {
        internal enum DisplayTAN {
          /// Okay
          internal static let continueButtonTitle = L10n.tr("iOSApp", "history.share.displayTAN.continueButtonTitle")
          /// If you give this TAN to a health department, they can decrypt your history and inform luca places.
          internal static let description = L10n.tr("iOSApp", "history.share.displayTAN.description")
          /// Share data
          internal static let title = L10n.tr("iOSApp", "history.share.displayTAN.title")
        }
        internal enum GenerateTAN {
          /// Generate TAN
          internal static let continueButtonTitle = L10n.tr("iOSApp", "history.share.generateTAN.continueButtonTitle")
          /// Share data
          internal static let title = L10n.tr("iOSApp", "history.share.generateTAN.title")
        }
        internal enum SelectTimeFrame {
          /// Select
          internal static let continueButtonTitle = L10n.tr("iOSApp", "history.share.selectTimeFrame.continueButtonTitle")
          /// Your history gathers all of your check-ins from the last 28 days. You can decide how many days you want to share with the health department. Define the number of days:
          internal static let description = L10n.tr("iOSApp", "history.share.selectTimeFrame.description")
          /// Define time period
          internal static let title = L10n.tr("iOSApp", "history.share.selectTimeFrame.title")
        }
      }
      internal enum TraceId {
        /// Trace ID: %@
        internal static func pasteboard(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "history.traceId.pasteboard", String(describing: p1))
        }
        internal enum Alert {
          /// %@\n\nThe trace ID was copied to your clipboard.
          internal static func description(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "history.traceId.alert.description", String(describing: p1))
          }
        }
      }
    }
    internal enum Id {
      /// Create your luca ID now
      internal static let addIdPromptHeadline = L10n.tr("iOSApp", "id.addIdPromptHeadline")
      /// Click to begin
      internal static let addIdPromptText = L10n.tr("iOSApp", "id.addIdPromptText")
      /// Your Ident ID was copied to your clipboard.
      internal static let clipboard = L10n.tr("iOSApp", "id.clipboard")
      /// Do you really want to delete your luca ID?
      internal static let deleteDialogId = L10n.tr("iOSApp", "id.deleteDialogId")
      /// Agree
      internal static let idConsentButton = L10n.tr("iOSApp", "id.idConsentButton")
      /// Consent
      internal static let idConsentHeadline = L10n.tr("iOSApp", "id.idConsentHeadline")
      /// I declare my consent according to Art. 9 (2) a) in combination with Art. 6 (1) 1 a) DSGVO to the processing of my data in the process of locally depositing a verified digital identity ("luca ID") in the luca app. The identity check enables the user to provide necessary proof to the operator by using the luca app.\n\nIn the course of the identity verifications, I agree that photos and recordings of my identification document will be taken and that I will be verified by means of the Autoident procedure and that this data will be transmitted to IDnow via the IDnow app. In the process, all my data contained in the document (ID card number, first name, last name, address, date of birth, place of birth, nationality, expiration date, eye color, height) will be processed. More information about the processing can be found in the privacy policy.\n\nI can revoke my consent at any time in my luca App. This is done for the future by deleting the stored ID card or canceling the identification attempt.
      internal static let idConsentText = L10n.tr("iOSApp", "id.idConsentText")
      /// Add luca ID
      internal static let identAvailableCardHeadline = L10n.tr("iOSApp", "id.identAvailableCardHeadline")
      /// Your IDnow Ident-ID
      internal static let identAvailableCardSubline = L10n.tr("iOSApp", "id.identAvailableCardSubline")
      /// You can find more information in the notifications tab in your luca app.
      internal static let idGenericStatusShortText = L10n.tr("iOSApp", "id.idGenericStatusShortText")
      /// Start now
      internal static let idInfoscreenButton = L10n.tr("iOSApp", "id.idInfoscreenButton")
      /// Create luca ID
      internal static let idInfoscreenHeadline = L10n.tr("iOSApp", "id.idInfoscreenHeadline")
      /// In the luca app, you can now create your luca ID by verifying your identity using your ID card or a different identity proof. This will allow us to offer you cool additional features in the future. For example, it will become easier to enter luca locations.\n\nYou need to verify your identity with the free IDnow app to save your luca ID locally on your phone.
      internal static let idInfoscreenText = L10n.tr("iOSApp", "id.idInfoscreenText")
      /// IDNOW APP
      internal static let idNowAppButton = L10n.tr("iOSApp", "id.idNowAppButton")
      /// Download IDnow app
      internal static let idRequestSentButton = L10n.tr("iOSApp", "id.idRequestSentButton")
      /// Ident-ID requested
      internal static let idRequestSentHeadline = L10n.tr("iOSApp", "id.idRequestSentHeadline")
      /// To verify your identity with the IDnow app, you need an IDnow Ident-ID. luca will now request one for you. You will find your IDnow Ident-ID in your notifications tab once we have received it.\n\nYou may now download the IDnow app.
      internal static let idRequestSentText = L10n.tr("iOSApp", "id.idRequestSentText")
      /// Here comes your Ident-ID
      internal static let idRequestSuccessHeadline = L10n.tr("iOSApp", "id.idRequestSuccessHeadline")
      /// Your luca ID could not be created.
      internal static let idVerificationFailureHeadline = L10n.tr("iOSApp", "id.idVerificationFailureHeadline")
      /// Unfortunately, something went wrong with the verification of your identity or when we tried to create your luca ID. You can retry later.\n\n\nIf you think there's an issue, please contact our support.
      internal static let idVerificationFailureText = L10n.tr("iOSApp", "id.idVerificationFailureText")
      /// luca ID successfully created!
      internal static let idVerificationSuccessHeadline = L10n.tr("iOSApp", "id.idVerificationSuccessHeadline")
      /// Your luca ID
      internal static let idVerificationSuccessTitle = L10n.tr("iOSApp", "id.idVerificationSuccessTitle")
      /// An IDnow Ident-ID was requested
      internal static let queuedIdPromptHeadline = L10n.tr("iOSApp", "id.queuedIdPromptHeadline")
      /// You will receive a notification once we have received it.
      internal static let queuedIdPromptText = L10n.tr("iOSApp", "id.queuedIdPromptText")
      /// Your lock password was copied to your clipboard.
      internal static let revocationCodeClipboard = L10n.tr("iOSApp", "id.revocationCodeClipboard")
      internal enum Authentication {
        /// To ensure that only you can view your luca ID, we require authentication. Please authenticate yourself.
        internal static let reason = L10n.tr("iOSApp", "id.authentication.reason")
        internal enum Failed {
          /// To ensure that only you can create a luca ID, we require authentication. Please authenticate yourself.
          internal static let message = L10n.tr("iOSApp", "id.authentication.failed.message")
          /// Authentication failed
          internal static let title = L10n.tr("iOSApp", "id.authentication.failed.title")
        }
      }
      internal enum Card {
        /// Delete
        internal static let button = L10n.tr("iOSApp", "id.card.button")
        /// luca ID
        internal static let headline = L10n.tr("iOSApp", "id.card.headline")
        internal enum Blurred {
          /// First name last name
          internal static let label = L10n.tr("iOSApp", "id.card.blurred.label")
        }
        internal enum DateOfBirth {
          /// Date of birth
          internal static let label = L10n.tr("iOSApp", "id.card.dateOfBirth.label")
        }
        internal enum Name {
          /// Full name
          internal static let label = L10n.tr("iOSApp", "id.card.name.label")
        }
        internal enum Tooltip {
          /// Verified digital identity
          internal static let accessibility = L10n.tr("iOSApp", "id.card.tooltip.accessibility")
        }
        internal enum Validity {
          /// Valid since:
          internal static let short = L10n.tr("iOSApp", "id.card.validity.short")
        }
        internal enum Verification {
          /// This identity has been verified by IDnow
          internal static let tooltip = L10n.tr("iOSApp", "id.card.verification.tooltip")
        }
        internal enum Verified {
          /// Your verified digital identity
          internal static let identity = L10n.tr("iOSApp", "id.card.verified.identity")
        }
      }
      internal enum Delete {
        /// Delete
        internal static let action = L10n.tr("iOSApp", "id.delete.action")
        /// Do you really want to delete your luca ID?
        internal static let description = L10n.tr("iOSApp", "id.delete.description")
      }
      internal enum Detail {
        /// IDnow Ident-ID
        internal static let title = L10n.tr("iOSApp", "id.detail.title")
      }
      internal enum Device {
        internal enum Check {
          /// Unfortunately, your device does not fulfil necessary requirements. This might be because you don't have a passcode or because your device is too old.
          internal static let error = L10n.tr("iOSApp", "id.device.check.error")
        }
      }
      internal enum IdRequestSuccessText {
        /// This is your personal IDnow Ident-ID:
        internal static let _1 = L10n.tr("iOSApp", "id.idRequestSuccessText.1")
        /// It is valid until %@.
        internal static func _2(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "id.idRequestSuccessText.2", String(describing: p1))
        }
        /// You can use this ID to verify your identity in the IDnow app and create your luca ID.
        internal static let _3 = L10n.tr("iOSApp", "id.idRequestSuccessText.3")
      }
      internal enum IdVerificationSuccessText {
        /// Your luca ID has been successfully created in your luca app. You will find it in your my luca tab from now on.\n\n\nPlease note down the following lock password in a safe place. You should be able to access it when you lose your phone.
        internal static let _1 = L10n.tr("iOSApp", "id.idVerificationSuccessText.1")
        /// Your lock password:
        internal static let _2 = L10n.tr("iOSApp", "id.idVerificationSuccessText.2")
        /// You will need your lock password to enable us to lock your luca ID so it can't be used by someone else. Without your lock password, we cannot lock your luca ID because it is only saved locally on your device.
        internal static let _3 = L10n.tr("iOSApp", "id.idVerificationSuccessText.3")
        /// Your luca ID has been successfully created in your luca app. You will find it in your my luca tab from now on.
        internal static let short = L10n.tr("iOSApp", "id.idVerificationSuccessText.short")
      }
      internal enum Ident {
        /// Missing Ident-ID
        internal static let missingId = L10n.tr("iOSApp", "id.ident.missingId")
        internal enum Copy {
          /// Copy Ident-ID
          internal static let button = L10n.tr("iOSApp", "id.ident.copy.button")
        }
      }
    }
    internal enum Light {
      internal enum Button {
        /// Activate light
        internal static let activate = L10n.tr("iOSApp", "light.button.activate")
        /// Deactivate light
        internal static let deactivate = L10n.tr("iOSApp", "light.button.deactivate")
      }
    }
    internal enum List {
      ///  and 
      internal static let and = L10n.tr("iOSApp", "list.and")
      /// , and 
      internal static let lastElement = L10n.tr("iOSApp", "list.lastElement")
    }
    internal enum LocationCheckinViewController {
      /// Automatic checkout
      internal static let autoCheckout = L10n.tr("iOSApp", "locationCheckinViewController.autoCheckout")
      internal enum Accessibility {
        /// Check out
        internal static let checkoutSlider = L10n.tr("iOSApp", "locationCheckinViewController.accessibility.checkoutSlider")
        /// Checkout of location
        internal static let directCheckout = L10n.tr("iOSApp", "locationCheckinViewController.accessibility.directCheckout")
      }
      internal enum AdditionalData {
        /// Table: %@
        internal static func table(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "locationCheckinViewController.additionalData.table", String(describing: p1))
        }
      }
      internal enum AutoCheckout {
        /// Off
        internal static let off = L10n.tr("iOSApp", "locationCheckinViewController.autoCheckout.off")
        /// On
        internal static let on = L10n.tr("iOSApp", "locationCheckinViewController.autoCheckout.on")
        internal enum Permission {
          internal enum BeforePrompt {
            /// data privacy - app
            internal static let link = L10n.tr("iOSApp", "locationCheckinViewController.autoCheckout.permission.beforePrompt.link")
            /// This automatic checkout feature uses geofencing. If a luca-location uses it as well, you can check out automatically using your location through the GPS function of your phone, even when the app is closed. This is only done with your consent under art. 6 (1) 1 a) GDPR (more: %@). However, you can still check out manually at any time.
            internal static func message(_ p1: Any) -> String {
              return L10n.tr("iOSApp", "locationCheckinViewController.autoCheckout.permission.beforePrompt.message", String(describing: p1))
            }
            /// OKAY
            internal static let okButton = L10n.tr("iOSApp", "locationCheckinViewController.autoCheckout.permission.beforePrompt.okButton")
            /// Automatic checkout
            internal static let title = L10n.tr("iOSApp", "locationCheckinViewController.autoCheckout.permission.beforePrompt.title")
          }
        }
      }
      internal enum AutoCheckoutPermissionDisabled {
        /// Change your luca location permission to "Always".
        internal static let message = L10n.tr("iOSApp", "locationCheckinViewController.autoCheckoutPermissionDisabled.message")
        /// Auto-checkout is not possible
        internal static let title = L10n.tr("iOSApp", "locationCheckinViewController.autoCheckoutPermissionDisabled.title")
      }
      internal enum CheckOutFailed {
        internal enum LocationNotAvailable {
          /// There is no information available about the current location.
          internal static let message = L10n.tr("iOSApp", "locationCheckinViewController.checkOutFailed.locationNotAvailable.message")
        }
        internal enum LowDuration {
          /// You just checked in! You can first check out when you've stayed a bit longer.
          internal static let message = L10n.tr("iOSApp", "locationCheckinViewController.checkOutFailed.lowDuration.message")
        }
        internal enum StillInLocation {
          /// You are still at %@. Move farther away to check out.
          internal static func message(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "locationCheckinViewController.checkOutFailed.stillInLocation.message", String(describing: p1))
          }
          /// You are still at the location. Move farther away to check out.
          internal static let messageWithoutName = L10n.tr("iOSApp", "locationCheckinViewController.checkOutFailed.stillInLocation.messageWithoutName")
          /// Info
          internal static let title = L10n.tr("iOSApp", "locationCheckinViewController.checkOutFailed.stillInLocation.title")
        }
      }
      internal enum LocationInfoFetchFailure {
        /// Error while loading the location data: %@
        internal static func message(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "locationCheckinViewController.locationInfoFetchFailure.message", String(describing: p1))
        }
      }
      internal enum Permission {
        internal enum Change {
          /// To use auto-checkout you must choose the option "Always" in your luca location settings.
          internal static let message = L10n.tr("iOSApp", "locationCheckinViewController.permission.change.message")
          /// Location Settings
          internal static let title = L10n.tr("iOSApp", "locationCheckinViewController.permission.change.title")
        }
        internal enum Denied {
          /// Your location setting has still not been enabled. Without your position we cannot check if you have left %@.
          internal static func message(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "locationCheckinViewController.permission.denied.message", String(describing: p1))
          }
          /// Your location setting has still not been enabled. Without your position we cannot check if you have left the location.
          internal static let messageWithoutName = L10n.tr("iOSApp", "locationCheckinViewController.permission.denied.messageWithoutName")
        }
      }
    }
    internal enum Luca {
      internal enum Connect {
        internal enum HealthDepartment {
          internal enum Active {
            /// Your local health department now provides personal notifications! Activate personal notifications to help your health department and receive important information from it directly within your luca app.\n\nActivate now!
            internal static let description = L10n.tr("iOSApp", "luca.connect.healthDepartment.active.description")
            /// Activate luca Connect - personal notifications
            internal static let title = L10n.tr("iOSApp", "luca.connect.healthDepartment.active.title")
          }
        }
      }
    }
    internal enum MainCheckinViewController {
      internal enum NoDailyKey {
        /// 
        internal static let message = L10n.tr("iOSApp", "mainCheckinViewController.noDailyKey.message")
      }
    }
    internal enum MainTabBarViewController {
      internal enum ScannerFailure {
        /// Error: %@
        internal static func message(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "mainTabBarViewController.scannerFailure.message", String(describing: p1))
        }
        /// Scanner error
        internal static let title = L10n.tr("iOSApp", "mainTabBarViewController.scannerFailure.title")
      }
      internal enum UserRegistrationFailure {
        internal enum General {
          /// User could not be registered: %@
          internal static func message(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "mainTabBarViewController.userRegistrationFailure.general.message", String(describing: p1))
          }
        }
        internal enum NoInternet {
          /// User could not be registered! No internet connection could be made.
          internal static let message = L10n.tr("iOSApp", "mainTabBarViewController.userRegistrationFailure.noInternet.message")
        }
      }
    }
    internal enum My {
      internal enum Luca {
        /// ADD
        internal static let add = L10n.tr("iOSApp", "my.luca.add")
        /// Book test appointment
        internal static let calendar = L10n.tr("iOSApp", "my.luca.calendar")
        /// My luca
        internal static let title = L10n.tr("iOSApp", "my.luca.title")
        internal enum Add {
          /// Add document
          internal static let document = L10n.tr("iOSApp", "my.luca.add.document")
        }
        internal enum Empty {
          internal enum State {
            /// This is your personal space, where you can add documents to make your check-in easier.
            internal static let description = L10n.tr("iOSApp", "my.luca.empty.state.description")
            /// Welcome to "My luca"
            internal static let title = L10n.tr("iOSApp", "my.luca.empty.state.title")
          }
        }
        internal enum Separator {
          /// Child
          internal static let kid = L10n.tr("iOSApp", "my.luca.separator.kid")
        }
      }
    }
    internal enum Navigation {
      /// More
      internal static let menu = L10n.tr("iOSApp", "navigation.menu")
      internal enum Basic {
        /// ACCEPT
        internal static let accept = L10n.tr("iOSApp", "navigation.basic.accept")
        /// AGREE
        internal static let agree = L10n.tr("iOSApp", "navigation.basic.agree")
        /// Attention!
        internal static let attention = L10n.tr("iOSApp", "navigation.basic.attention")
        /// BACK
        internal static let back = L10n.tr("iOSApp", "navigation.basic.back")
        /// Cancel
        internal static let cancel = L10n.tr("iOSApp", "navigation.basic.cancel")
        /// CLOSE
        internal static let close = L10n.tr("iOSApp", "navigation.basic.close")
        /// Confirm
        internal static let confirm = L10n.tr("iOSApp", "navigation.basic.confirm")
        /// Continue
        internal static let `continue` = L10n.tr("iOSApp", "navigation.basic.continue")
        /// Delete
        internal static let delete = L10n.tr("iOSApp", "navigation.basic.delete")
        /// DONE
        internal static let done = L10n.tr("iOSApp", "navigation.basic.done")
        /// Error
        internal static let error = L10n.tr("iOSApp", "navigation.basic.error")
        /// Attention
        internal static let hint = L10n.tr("iOSApp", "navigation.basic.hint")
        /// Loading
        internal static let loading = L10n.tr("iOSApp", "navigation.basic.loading")
        /// LET'S GO
        internal static let look = L10n.tr("iOSApp", "navigation.basic.look")
        /// NEXT
        internal static let next = L10n.tr("iOSApp", "navigation.basic.next")
        /// No
        internal static let no = L10n.tr("iOSApp", "navigation.basic.no")
        /// Ok
        internal static let ok = L10n.tr("iOSApp", "navigation.basic.ok")
        /// SKIP
        internal static let skip = L10n.tr("iOSApp", "navigation.basic.skip")
        /// Start
        internal static let start = L10n.tr("iOSApp", "navigation.basic.start")
        /// Yes
        internal static let yes = L10n.tr("iOSApp", "navigation.basic.yes")
        internal enum Cancel {
          /// CANCEL
          internal static let uppercased = L10n.tr("iOSApp", "navigation.basic.cancel.uppercased")
        }
        internal enum Error {
          /// Error: 
          internal static let accessibility = L10n.tr("iOSApp", "navigation.basic.error.accessibility")
        }
      }
      internal enum DataAccess {
        /// Data requests
        internal static let title = L10n.tr("iOSApp", "navigation.dataAccess.title")
      }
      internal enum Tab {
        /// Account
        internal static let account = L10n.tr("iOSApp", "navigation.tab.account")
        /// Check-in
        internal static let checkin = L10n.tr("iOSApp", "navigation.tab.checkin")
        /// My luca
        internal static let health = L10n.tr("iOSApp", "navigation.tab.health")
        /// History
        internal static let history = L10n.tr("iOSApp", "navigation.tab.history")
        /// Notifications
        internal static let notification = L10n.tr("iOSApp", "navigation.tab.notification")
      }
    }
    internal enum NoCT {
      /// CHECK-IN
      internal static let button = L10n.tr("iOSApp", "noCT.button")
      /// Currently, contact tracing via luca is on hold as health departments have stopped collecting contact information due to the current pandemic situation.\n\nWhat this means for you: Your contact information will no longer be captured. luca will continue to be available to you and will continue to provide many benefits to you in the future. So you can check in as before and use the functions provided by the luca locations in your luca app.
      internal static let message = L10n.tr("iOSApp", "noCT.message")
      /// No contact tracing
      internal static let title = L10n.tr("iOSApp", "noCT.title")
    }
    internal enum Notification {
      internal enum Checkout {
        /// Don't forget to checkout in the luca app when you've left your location.
        internal static let description = L10n.tr("iOSApp", "notification.checkout.description")
        /// Important!
        internal static let title = L10n.tr("iOSApp", "notification.checkout.title")
      }
      internal enum Details {
        internal enum Title {
          /// Data request
          internal static let datarequest = L10n.tr("iOSApp", "notification.details.title.datarequest")
          /// Risk of infection
          internal static let infection = L10n.tr("iOSApp", "notification.details.title.infection")
        }
      }
      internal enum List {
        /// Notifications
        internal static let title = L10n.tr("iOSApp", "notification.list.title")
        internal enum Empty {
          /// Notifications will appear here whenever your data has been accessed or your health department wants to provide you with important information. Possible notifications: System information, information about possible/increased risk of infection & data accesses.
          internal static let message = L10n.tr("iOSApp", "notification.list.empty.message")
          /// You haven't received any notifications yet
          internal static let title = L10n.tr("iOSApp", "notification.list.empty.title")
        }
      }
      internal enum Permission {
        /// You didn't allow notifications. Please enable luca notifications in your settings to be reminded to checkout.
        internal static let description = L10n.tr("iOSApp", "notification.permission.description")
        /// Enable notifications
        internal static let title = L10n.tr("iOSApp", "notification.permission.title")
      }
    }
    internal enum OptionalCheckin {
      /// CHECK IN VOLUNTARILY
      internal static let button = L10n.tr("iOSApp", "optionalCheckin.button")
      /// Optionally share contact information with the health department
      internal static let checkBox = L10n.tr("iOSApp", "optionalCheckin.checkBox")
      /// Checking into this location is optional.  If you still want to help with contact tracing and be alerted in case of risk, you can check in voluntarily and even anonymously. Your contact information will not be shared with the health department if you check in anonymously.
      internal static let message = L10n.tr("iOSApp", "optionalCheckin.message")
      /// Voluntary Check-in
      internal static let title = L10n.tr("iOSApp", "optionalCheckin.title")
    }
    internal enum Pay {
      internal enum Amount {
        /// Continue with %@
        internal static func button(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "pay.amount.button", String(describing: p1))
        }
        /// Euro
        internal static let currency = L10n.tr("iOSApp", "pay.amount.currency")
        /// Maximum amount %@
        internal static func maximum(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "pay.amount.maximum", String(describing: p1))
        }
        /// Minimum amount %@
        internal static func minimum(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "pay.amount.minimum", String(describing: p1))
        }
        /// Please enter the amount, that you want to pay to 
        internal static let text1 = L10n.tr("iOSApp", "pay.amount.text1")
        /// 
        internal static let text2 = L10n.tr("iOSApp", "pay.amount.text2")
      }
    }
    internal enum Payment {
      /// Unfortunately, it is not possible to pay here with luca Pay yet.
      internal static let notAvailable = L10n.tr("iOSApp", "payment.notAvailable")
      internal enum Consent {
        /// Agree
        internal static let accept = L10n.tr("iOSApp", "payment.consent.accept")
        /// Hereby I declare my consent to the processing of my payment information, information about my payment transactions, for example with a restaurant, and my individual user ID by luca for the purpose of making payments by the payment service provider Rapyd Europe according to Art. 6 (1) 1 a) in combination with Art. 49 (1) 1 a) GDPR. This allows me to repeatedly initiate payments without re-collecting the above-mentioned data as well as viewing and checking my payment history. I have taken note of further information in the luca Privacy Policy and consent to Rapyd Europe’s disclosure of my aforementioned data to third parties who may process such data outside the European Economic Area (EEA) and Switzerland for the stated purpose. I am aware that these countries do not always have the same level of data protection as the EU. However, Rapyd Europe has taken concrete measures to ensure the protection of my data in accordance with EEA data protection law. I can revoke my consent at any time for the future.
        internal static let content = L10n.tr("iOSApp", "payment.consent.content")
        /// Privacy Policy
        internal static let linkPlaceholder = L10n.tr("iOSApp", "payment.consent.linkPlaceholder")
        /// DSGVO
        internal static let title = L10n.tr("iOSApp", "payment.consent.title")
      }
      internal enum NotAvailable {
        /// luca Pay not available yet
        internal static let title = L10n.tr("iOSApp", "payment.notAvailable.title")
      }
      internal enum PreConsentInfo {
        /// In order to use the new features like luca Pay, you need to agree to the updated terms of use. In the next step, you can view the updated terms and conditions as well as the changes.
        internal static let text = L10n.tr("iOSApp", "payment.preConsentInfo.text")
      }
      internal enum Revocation {
        internal enum Navbar {
          /// Revocation code
          internal static let title = L10n.tr("iOSApp", "payment.revocation.navbar.title")
        }
      }
      internal enum Support {
        internal enum Email {
          /// Dear luca-support,<br><br>the following problem occurs when using the luca Pay App:<br><br>Additional Information:<br>Device: %@<br>Operating System: %@<br>App Version: %@<br>Payment Code: %@<br>Payment Date: %@<br>Location: %@
          internal static func body(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any, _ p5: Any, _ p6: Any) -> String {
            return L10n.tr("iOSApp", "payment.support.email.body", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4), String(describing: p5), String(describing: p6))
          }
          /// lucapay@luca-app.de
          internal static let recipient = L10n.tr("iOSApp", "payment.support.email.recipient")
        }
        internal enum General {
          internal enum Email {
            /// Dear luca-support,<br><br>the following problem occurs when using the luca Pay App:<br><br>Additional Information:<br>Device: %@<br>Operating System: %@<br>App Version: %@
            internal static func body(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
              return L10n.tr("iOSApp", "payment.support.general.email.body", String(describing: p1), String(describing: p2), String(describing: p3))
            }
          }
        }
      }
    }
    internal enum PhoneNumber {
      internal enum Confirmation {
        internal enum Description {
          /// You will receive a call with a message containing the TAN that is needed to verify your phone number in the next step. Please make sure the phone is nearby and the number is correct:
          internal static let fixed = L10n.tr("iOSApp", "phoneNumber.confirmation.description.fixed")
          /// To verify your phone number we will send a TAN to the number you provided in the next step. Please make sure this number is correct:
          internal static let mobile = L10n.tr("iOSApp", "phoneNumber.confirmation.description.mobile")
        }
      }
    }
    internal enum Postcode {
      internal enum Check {
        /// Zip code matching
        internal static let title = L10n.tr("iOSApp", "postcode.check.title")
        internal enum Activation {
          /// Zip code matching has not been activated.
          internal static let error = L10n.tr("iOSApp", "postcode.check.activation.error")
        }
        internal enum Consent {
          /// I hereby declare my consent according to Art. 6 (1) 1 a) DSGVO for the processing of my zip code stored locally in the app for the purpose of enabling regional functionalities. My zip code was initially collected for the purpose of contact tracking and stored in encrypted form in the luca System. I agree that the zip code is also processed locally on my end device by the luca app. This allows luca to show me and unlock new and regionally limited functionalities. The processing is only performed locally within my own app. No further storage takes place. I can execute my revocation at any time within my preferences (under "Account").
          internal static let description = L10n.tr("iOSApp", "postcode.check.consent.description")
        }
        internal enum Toggle {
          /// Some of our (new) features and functions are location dependent. Enable zip code matching to get information about location dependent features.
          internal static let footer = L10n.tr("iOSApp", "postcode.check.toggle.footer")
          /// Activate zip code matching
          internal static let title = L10n.tr("iOSApp", "postcode.check.toggle.title")
        }
      }
    }
    internal enum Private {
      internal enum Meeting {
        /// Let your guests scan the QR code to check into your meeting.
        internal static let description = L10n.tr("iOSApp", "private.meeting.description")
        /// Length
        internal static let length = L10n.tr("iOSApp", "private.meeting.length")
        /// Private Meeting
        internal static let title = L10n.tr("iOSApp", "private.meeting.title")
        internal enum Accessibility {
          /// End meeting
          internal static let endMeeting = L10n.tr("iOSApp", "private.meeting.accessibility.endMeeting")
          /// Guests: %d out of %d
          internal static func guests(_ p1: Int, _ p2: Int) -> String {
            return L10n.tr("iOSApp", "private.meeting.accessibility.guests", p1, p2)
          }
          /// Length: %@
          internal static func length(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "private.meeting.accessibility.length", String(describing: p1))
          }
        }
        internal enum Alert {
          /// When you check yourself in at a private meeting, your host can see your first and last name.\n\nPrivate meetings are used as reminders and won't be shared with health authorities.
          internal static let description = L10n.tr("iOSApp", "private.meeting.alert.description")
        }
        internal enum Create {
          /// There was an error while creating a new meeting: %@
          internal static func failure(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "private.meeting.create.failure", String(describing: p1))
          }
        }
        internal enum Details {
          /// Duration
          internal static let duration = L10n.tr("iOSApp", "private.meeting.details.duration")
          /// Participants: %d
          internal static func guests(_ p1: Int) -> String {
            return L10n.tr("iOSApp", "private.meeting.details.guests", p1)
          }
          /// Begin
          internal static let start = L10n.tr("iOSApp", "private.meeting.details.start")
          /// Details
          internal static let title = L10n.tr("iOSApp", "private.meeting.details.title")
        }
        internal enum End {
          /// Do you really want to end this meeting?
          internal static let description = L10n.tr("iOSApp", "private.meeting.end.description")
          /// End meeting
          internal static let title = L10n.tr("iOSApp", "private.meeting.end.title")
        }
        internal enum Info {
          /// When friends and family check into your private meeting, their first and last names will be shown in your app.\n\nThey will also see your first and last name in their app.\n\nYou can already use this feature at locations that are not part of the luca system to keep track of private meetings yourself.
          internal static let description = L10n.tr("iOSApp", "private.meeting.info.description")
          /// Private Meeting
          internal static let title = L10n.tr("iOSApp", "private.meeting.info.title")
        }
        internal enum Participants {
          /// No participants.
          internal static let `none` = L10n.tr("iOSApp", "private.meeting.participants.none")
          /// Participants
          internal static let title = L10n.tr("iOSApp", "private.meeting.participants.title")
        }
        internal enum Start {
          /// Do you want to start a private meeting now?\n\nWhen friends and family check into your private meeting, their first and last names will be shown in your app. They will also see your first and last name in their app.\n\nPrivate meetings are used as reminders and won't be shared with health authorities.
          internal static let description = L10n.tr("iOSApp", "private.meeting.start.description")
          /// Set up a private meeting
          internal static let title = L10n.tr("iOSApp", "private.meeting.start.title")
        }
      }
    }
    internal enum QrCodeGeneration {
      internal enum Failure {
        /// QR Code could not be generated: %@
        internal static func message(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "qrCodeGeneration.failure.message", String(describing: p1))
        }
      }
    }
    internal enum Recovery {
      internal enum View {
        internal enum Label {
          /// RECOVERY CERTIFICATE
          internal static let title = L10n.tr("iOSApp", "recovery.view.label.title")
          /// Recovered before:
          internal static let validDuration = L10n.tr("iOSApp", "recovery.view.label.validDuration")
          /// Valid until:
          internal static let validUntil = L10n.tr("iOSApp", "recovery.view.label.validUntil")
        }
      }
    }
    internal enum ReportContent {
      /// abuse@luca-app.de
      internal static let email = L10n.tr("iOSApp", "reportContent.email")
      /// Report inappropriate content
      internal static let title = L10n.tr("iOSApp", "reportContent.title")
      internal enum Email {
        /// With this mail I would like to report the contents of the following location: %@.<br><br>\nThe location has provided inappropriate content through the following links:<br><br>\n%@\n<br><br>\nThe content is inappropriate for the following reason: Please add why you are reporting the links.
        internal static func body(_ p1: Any, _ p2: Any) -> String {
          return L10n.tr("iOSApp", "reportContent.email.body", String(describing: p1), String(describing: p2))
        }
        internal enum Error {
          /// Mail Error
          internal static let title = L10n.tr("iOSApp", "reportContent.email.error.title")
          internal enum MailNotConfigured {
            /// No email accounts configured.
            internal static let description = L10n.tr("iOSApp", "reportContent.email.error.mailNotConfigured.description")
          }
          internal enum SaveFailed {
            /// Saving failed.
            internal static let description = L10n.tr("iOSApp", "reportContent.email.error.saveFailed.description")
          }
          internal enum SendFailed {
            /// Sending failed.
            internal static let description = L10n.tr("iOSApp", "reportContent.email.error.sendFailed.description")
          }
          internal enum Unknown {
            /// An unknown error occured.
            internal static let description = L10n.tr("iOSApp", "reportContent.email.error.unknown.description")
          }
        }
      }
    }
    internal enum Terms {
      internal enum Acceptance {
        /// We are updating our terms of use and privacy policy.
        internal static let description = L10n.tr("iOSApp", "terms.acceptance.description")
        /// https://www.luca-app.de/changes-terms-of-use-app/
        internal static let linkChanges = L10n.tr("iOSApp", "terms.acceptance.linkChanges")
        /// Tap "AGREE" to accept the updated terms of use and privacy policy.
        internal static let linkDescription = L10n.tr("iOSApp", "terms.acceptance.linkDescription")
        /// Changes
        internal static let termsAndConditionsChanges = L10n.tr("iOSApp", "terms.acceptance.termsAndConditionsChanges")
      }
    }
    internal enum TermsOfUse {
      internal enum Link {
        /// changes in the terms of use
        internal static let term1 = L10n.tr("iOSApp", "termsOfUse.link.term1")
        /// updated terms of use
        internal static let term2 = L10n.tr("iOSApp", "termsOfUse.link.term2")
        /// https://www.luca-app.de/changes-terms-of-use-app/
        internal static let url = L10n.tr("iOSApp", "termsOfUse.link.url")
      }
      internal enum Notification {
        /// We have updated our terms of use. Here you can review and agree to the changes.
        internal static let description = L10n.tr("iOSApp", "termsOfUse.notification.description")
        /// New Terms Of Use
        internal static let title = L10n.tr("iOSApp", "termsOfUse.notification.title")
      }
      internal enum Sheet {
        /// We have updated our terms of use. In order to use the new features of luca, you must agree to the changes in the terms of use.
        internal static let description1 = L10n.tr("iOSApp", "termsOfUse.sheet.description1")
        /// Click accept to accept the updated terms of use.
        internal static let description2 = L10n.tr("iOSApp", "termsOfUse.sheet.description2")
      }
    }
    internal enum Test {
      internal enum Add {
        /// Add
        internal static let title = L10n.tr("iOSApp", "test.add.title")
      }
      internal enum BirthdateMismatch {
        internal enum Error {
          /// The document's date of birth does not match your other documents.
          internal static let description = L10n.tr("iOSApp", "test.birthdateMismatch.error.description")
          /// Import
          internal static let ok = L10n.tr("iOSApp", "test.birthdateMismatch.error.ok")
        }
      }
      internal enum Delete {
        /// Do you really want to delete this document?
        internal static let description = L10n.tr("iOSApp", "test.delete.description")
        /// Delete document
        internal static let title = L10n.tr("iOSApp", "test.delete.title")
      }
      internal enum Expiry {
        /// h
        internal static let hours = L10n.tr("iOSApp", "test.expiry.hours")
        /// m
        internal static let minutes = L10n.tr("iOSApp", "test.expiry.minutes")
      }
      internal enum Key {
        internal enum With {
          /// Some text with %@ and [links](https://www.google.com)
          internal static func placeholdersAndMarkdownLinks(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "test.key.with.placeholdersAndMarkdownLinks", String(describing: p1))
          }
        }
      }
      internal enum Result {
        /// dd.MM.yyyy
        internal static let dateFormat = L10n.tr("iOSApp", "test.result.dateFormat")
        /// Expiry: %@
        internal static func expiry(_ p1: Any) -> String {
          return L10n.tr("iOSApp", "test.result.expiry", String(describing: p1))
        }
        /// Rapid test
        internal static let fast = L10n.tr("iOSApp", "test.result.fast")
        /// Negative
        internal static let negative = L10n.tr("iOSApp", "test.result.negative")
        /// Other
        internal static let other = L10n.tr("iOSApp", "test.result.other")
        /// PCR test
        internal static let pcr = L10n.tr("iOSApp", "test.result.pcr")
        /// Positive
        internal static let positive = L10n.tr("iOSApp", "test.result.positive")
        /// Successful
        internal static let success = L10n.tr("iOSApp", "test.result.success")
        internal enum Child {
          internal enum Age {
            /// Documents can only be added for your children if they are under the age of 14.
            internal static let error = L10n.tr("iOSApp", "test.result.child.age.error")
          }
        }
        internal enum Delete {
          /// It was not possible to delete this test.
          internal static let error = L10n.tr("iOSApp", "test.result.delete.error")
        }
        internal enum Duration {
          /// one hour
          internal static let hour = L10n.tr("iOSApp", "test.result.duration.hour")
          /// %i hours
          internal static func hours(_ p1: Int) -> String {
            return L10n.tr("iOSApp", "test.result.duration.hours", p1)
          }
          /// %i minutes
          internal static func minutes(_ p1: Int) -> String {
            return L10n.tr("iOSApp", "test.result.duration.minutes", p1)
          }
          /// %i months
          internal static func months(_ p1: Int) -> String {
            return L10n.tr("iOSApp", "test.result.duration.months", p1)
          }
        }
        internal enum Error {
          /// The Document could not be imported
          internal static let title = L10n.tr("iOSApp", "test.result.error.title")
        }
        internal enum Expiration {
          /// Unfortunately, the document cannot be imported because it already expired.
          internal static let error = L10n.tr("iOSApp", "test.result.expiration.error")
        }
        internal enum Future {
          /// This document's issue date is set in the future. You can only add documents that are valid at the moment.
          internal static let error = L10n.tr("iOSApp", "test.result.future.error")
        }
        internal enum Name {
          internal enum Validation {
            /// It was not possible to validate your name. Please check if your name is entered correctly in the app.
            internal static let error = L10n.tr("iOSApp", "test.result.name.validation.error")
          }
        }
        internal enum Parsing {
          /// Data couldn't be processed.
          internal static let error = L10n.tr("iOSApp", "test.result.parsing.error")
          /// Your document was added successfully.
          internal static let success = L10n.tr("iOSApp", "test.result.parsing.success")
        }
        internal enum Positive {
          /// luca helps you to check in easily. Since only negative test results are relevant for checking in, you can only add negative results at the moment. You can only add a positive result if it is a PCR test older than 14 days. 
          internal static let error = L10n.tr("iOSApp", "test.result.positive.error")
          internal enum Error {
            /// Adding not possible
            internal static let title = L10n.tr("iOSApp", "test.result.positive.error.title")
          }
        }
        internal enum Validation {
          /// It was not possible to validate that this document belongs to you or your children. Please check if the names are entered correctly in the app.
          internal static let error = L10n.tr("iOSApp", "test.result.validation.error")
        }
        internal enum Verification {
          /// Unfortunately, the document cannot be imported because the signature is not valid.
          internal static let error = L10n.tr("iOSApp", "test.result.verification.error")
        }
      }
      internal enum Scan {
        /// Scan the QR code of your document to add it to the app.
        internal static let description = L10n.tr("iOSApp", "test.scan.description")
      }
      internal enum Scanner {
        /// QR code scanner
        internal static let camera = L10n.tr("iOSApp", "test.scanner.camera")
        /// Close
        internal static let close = L10n.tr("iOSApp", "test.scanner.close")
        /// Scan QR code
        internal static let title = L10n.tr("iOSApp", "test.scanner.title")
      }
      internal enum Show {
        /// Show document
        internal static let title = L10n.tr("iOSApp", "test.show.title")
      }
      internal enum Uniqueness {
        internal enum Create {
          internal enum RandomTag {
            /// Failed to create a random tag.
            internal static let error = L10n.tr("iOSApp", "test.uniqueness.create.randomTag.error")
          }
        }
        internal enum Encoding {
          /// Failed to encode test data.
          internal static let error = L10n.tr("iOSApp", "test.uniqueness.encoding.error")
        }
        internal enum Rate {
          internal enum Limit {
            /// The rate limit has been reached.
            internal static let error = L10n.tr("iOSApp", "test.uniqueness.rate.limit.error")
          }
        }
        internal enum Redeemed {
          /// This document was already imported.
          internal static let error = L10n.tr("iOSApp", "test.uniqueness.redeemed.error")
        }
        internal enum Release {
          /// Document could not be deleted.
          internal static let error = L10n.tr("iOSApp", "test.uniqueness.release.error")
        }
      }
      internal enum View {
        internal enum Label {
          /// Issuer:
          internal static let issuer = L10n.tr("iOSApp", "test.view.label.issuer")
          /// Tested before:
          internal static let tested = L10n.tr("iOSApp", "test.view.label.tested")
          /// Tester:
          internal static let tester = L10n.tr("iOSApp", "test.view.label.tester")
        }
      }
    }
    internal enum Tests {
      internal enum Uniqueness {
        internal enum Consent {
          /// I hereby declare my consent in accordance with Art. 9 (2) a) in conjunction with Art. 6 (1) 1 a) DSGVO (more: %@) to the processing of my data during the import of my COVID test, recovery or vaccination certificate in the luca app. The data included in the document can be matched to my first and last name in the luca app. I also allow the storage of a pseudonymized identifier on the luca server. This is solely to prevent misuse, so that a document cannot be used by different people. The document’s individual identifier is automatically deleted from the luca system after 72 hours of storage.
          internal static func description(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "tests.uniqueness.consent.description", String(describing: p1))
          }
          /// DSGVO
          internal static let title = L10n.tr("iOSApp", "tests.uniqueness.consent.title")
          internal enum Title {
            /// D.S.G.V.O
            internal static let accessibility = L10n.tr("iOSApp", "tests.uniqueness.consent.title.accessibility")
          }
        }
      }
    }
    internal enum TimeBanner {
      /// GO TO SETTINGS
      internal static let button = L10n.tr("iOSApp", "timeBanner.button")
      /// The time you set in your phone doesn't match the current time.
      internal static let message = L10n.tr("iOSApp", "timeBanner.message")
    }
    internal enum UserData {
      /// You have given no address in your data yet. Please fill it before further usage.
      internal static let addressNotFilledMessage = L10n.tr("iOSApp", "userData.addressNotFilledMessage")
      internal enum Address {
        /// Fields marked with * are mandatory.
        internal static let mandatory = L10n.tr("iOSApp", "userData.address.mandatory")
      }
      internal enum Form {
        /// Please fill out all mandatory fields to continue
        internal static let accessibilityError = L10n.tr("iOSApp", "userData.form.accessibilityError")
        /// City
        internal static let city = L10n.tr("iOSApp", "userData.form.city")
        /// Email
        internal static let email = L10n.tr("iOSApp", "userData.form.email")
        /// * First name
        internal static let firstName = L10n.tr("iOSApp", "userData.form.firstName")
        /// Number
        internal static let houseNumber = L10n.tr("iOSApp", "userData.form.houseNumber")
        /// * Last name
        internal static let lastName = L10n.tr("iOSApp", "userData.form.lastName")
        /// Phone number
        internal static let phoneNumber = L10n.tr("iOSApp", "userData.form.phoneNumber")
        /// Post code
        internal static let postCode = L10n.tr("iOSApp", "userData.form.postCode")
        /// Street
        internal static let street = L10n.tr("iOSApp", "userData.form.street")
        internal enum Address {
          /// Where do you live?
          internal static let formTitle = L10n.tr("iOSApp", "userData.form.address.formTitle")
        }
        internal enum City {
          /// Please enter your city to continue.
          internal static let accessibilityError = L10n.tr("iOSApp", "userData.form.city.accessibilityError")
        }
        internal enum Field {
          /// Fields marked with * are mandatory.
          internal static let error = L10n.tr("iOSApp", "userData.form.field.error")
        }
        internal enum FirstName {
          /// Please enter your first name to continue.
          internal static let accessibilityError = L10n.tr("iOSApp", "userData.form.firstName.accessibilityError")
        }
        internal enum HouseNumber {
          /// Please enter your house number to continue.
          internal static let accessibilityError = L10n.tr("iOSApp", "userData.form.houseNumber.accessibilityError")
        }
        internal enum LastName {
          /// Please enter your last name to continue.
          internal static let accessibilityError = L10n.tr("iOSApp", "userData.form.lastName.accessibilityError")
        }
        internal enum Name {
          /// What's your name?
          internal static let formTitle = L10n.tr("iOSApp", "userData.form.name.formTitle")
        }
        internal enum Phone {
          /// How can you be contacted?
          internal static let formTitle = L10n.tr("iOSApp", "userData.form.phone.formTitle")
        }
        internal enum PhoneNumber {
          /// Please enter your phone number to continue.
          internal static let accessibilityError = L10n.tr("iOSApp", "userData.form.phoneNumber.accessibilityError")
        }
        internal enum PostCode {
          /// Please enter your zip code to continue.
          internal static let accessibilityError = L10n.tr("iOSApp", "userData.form.postCode.accessibilityError")
        }
        internal enum Street {
          /// Please enter your street to continue.
          internal static let accessibilityError = L10n.tr("iOSApp", "userData.form.street.accessibilityError")
        }
      }
      internal enum Name {
        /// Fields marked with * are mandatory.
        internal static let mandatory = L10n.tr("iOSApp", "userData.name.mandatory")
      }
      internal enum Navigation {
        /// Edit contact data
        internal static let edit = L10n.tr("iOSApp", "userData.navigation.edit")
        /// Contact Data
        internal static let title = L10n.tr("iOSApp", "userData.navigation.title")
      }
      internal enum Phone {
        /// Please provide a correct mobile or landline number. This number will be used for verification. You can choose if you want to provide an email address.\n\nFields marked with * are mandatory.
        internal static let mandatory = L10n.tr("iOSApp", "userData.phone.mandatory")
      }
    }
    internal enum Vaccine {
      internal enum Result {
        /// BioNTech/Pfizer
        internal static let comirnaty = L10n.tr("iOSApp", "vaccine.result.comirnaty")
        /// Complete vaccination in %i days
        internal static func completeInDays(_ p1: Int) -> String {
          return L10n.tr("iOSApp", "vaccine.result.complete_in_days", p1)
        }
        /// Vaccination
        internal static let `default` = L10n.tr("iOSApp", "vaccine.result.default")
        /// Vaccination:
        internal static let description = L10n.tr("iOSApp", "vaccine.result.description")
        /// Johnson & Johnson
        internal static let janssen = L10n.tr("iOSApp", "vaccine.result.janssen")
        /// Moderna
        internal static let moderna = L10n.tr("iOSApp", "vaccine.result.moderna")
        /// No­va­vax
        internal static let nuvaxovid = L10n.tr("iOSApp", "vaccine.result.nuvaxovid")
        /// Sputnik V
        internal static let sputnikV = L10n.tr("iOSApp", "vaccine.result.sputnikV")
        /// VACCINATION CERTIFICATE (%i/%i)
        internal static func title(_ p1: Int, _ p2: Int) -> String {
          return L10n.tr("iOSApp", "vaccine.result.title", p1, p2)
        }
        /// AstraZeneca
        internal static let vaxzevria = L10n.tr("iOSApp", "vaccine.result.vaxzevria")
      }
      internal enum View {
        internal enum Label {
          /// VACCINATION CERTIFICATE
          internal static let title = L10n.tr("iOSApp", "vaccine.view.label.title")
          /// Vaccinated before:
          internal static let validDuration = L10n.tr("iOSApp", "vaccine.view.label.validDuration")
          /// Valid from:
          internal static let validFrom = L10n.tr("iOSApp", "vaccine.view.label.validFrom")
        }
      }
    }
    internal enum Verification {
      internal enum PhoneNumber {
        /// TAN
        internal static let code = L10n.tr("iOSApp", "verification.phoneNumber.code")
        /// The TAN you entered was incorrect.
        internal static let failureMessage = L10n.tr("iOSApp", "verification.phoneNumber.failureMessage")
        /// You have not verified your phone number yet. Please resave your contact data and verify your phone number.
        internal static let notYetVerified = L10n.tr("iOSApp", "verification.phoneNumber.notYetVerified")
        /// No SMS could be sent out.
        internal static let requestFailure = L10n.tr("iOSApp", "verification.phoneNumber.requestFailure")
        /// The verification was succesful.
        internal static let successMessage = L10n.tr("iOSApp", "verification.phoneNumber.successMessage")
        /// Successful verification
        internal static let successTitle = L10n.tr("iOSApp", "verification.phoneNumber.successTitle")
        /// Your data could not be saved.
        internal static let updateFailure = L10n.tr("iOSApp", "verification.phoneNumber.updateFailure")
        /// verified
        internal static let verified = L10n.tr("iOSApp", "verification.phoneNumber.verified")
        /// The given phone number is not in the right format. Please enter a valid phone number and its country code (e.g. +49xxxxxxxxxxx).
        internal static let wrongFormat = L10n.tr("iOSApp", "verification.phoneNumber.wrongFormat")
        internal enum Info {
          /// It can take a few minutes for the TAN to arrive. Please wait a moment before you request another TAN. If nothing happens after a few minutes, please check if you've provided the correct number and try again.
          internal static let message = L10n.tr("iOSApp", "verification.phoneNumber.info.message")
          /// Info
          internal static let title = L10n.tr("iOSApp", "verification.phoneNumber.info.title")
        }
        internal enum IpBlock {
          /// The operation could not be performed. Try to get off the wifi or disable VPN if you are connected through one. Please contact us if the problem persists.
          internal static let message = L10n.tr("iOSApp", "verification.phoneNumber.ipBlock.message")
        }
        internal enum LimitReached {
          /// Your daily SMS limit has been reached, please try again later.
          internal static let message = L10n.tr("iOSApp", "verification.phoneNumber.limitReached.message")
          /// Rate limit exceeded
          internal static let title = L10n.tr("iOSApp", "verification.phoneNumber.limitReached.title")
        }
        internal enum TimerDelay {
          /// You have recently requested a TAN. Please wait a moment. If nothing happens, you can try again in:\n\n%@
          internal static func message(_ p1: Any) -> String {
            return L10n.tr("iOSApp", "verification.phoneNumber.timerDelay.message", String(describing: p1))
          }
          /// TAN already requested
          internal static let title = L10n.tr("iOSApp", "verification.phoneNumber.timerDelay.title")
          internal enum Message {
            /// You have recently requested a TAN. Please wait a moment. If nothing happens, you can try again in:\n\n%@ minutes, %@ seconds
            internal static func accessibility(_ p1: Any, _ p2: Any) -> String {
              return L10n.tr("iOSApp", "verification.phoneNumber.timerDelay.message.accessibility", String(describing: p1), String(describing: p2))
            }
          }
        }
      }
    }
    internal enum VersionSupportChecker {
      /// Your luca version is not supported anymore! Please update your luca app in the AppStore.
      internal static let failureMessage = L10n.tr("iOSApp", "versionSupportChecker.failureMessage")
    }
    internal enum VoluntaryReachability {
      internal enum AddProof {
        /// Import image
        internal static let `import` = L10n.tr("iOSApp", "voluntaryReachability.addProof.import")
        /// Select an image of your document's QR code to add your document to luca.\nScan the QR code of your document (digital EU certificate) to add it to luca.\n\nIf you have already added your document to another app, you can use the QR code to add the document into luca as well.
        internal static let message = L10n.tr("iOSApp", "voluntaryReachability.addProof.message")
        /// Scan QR code
        internal static let scan = L10n.tr("iOSApp", "voluntaryReachability.addProof.scan")
        /// Deposit document: Import image or scan
        internal static let title = L10n.tr("iOSApp", "voluntaryReachability.addProof.title")
      }
      internal enum AddProofSuccess {
        /// Continue
        internal static let `continue` = L10n.tr("iOSApp", "voluntaryReachability.addProofSuccess.continue")
        /// Checkmark
        internal static let imageName = L10n.tr("iOSApp", "voluntaryReachability.addProofSuccess.imageName")
        /// You can now share your proof with your local health department. Additionally, you can find it in your "My luca" tab, where you can also show it whenever you are asked for it.
        internal static let message = L10n.tr("iOSApp", "voluntaryReachability.addProofSuccess.message")
        /// Document successfully added!
        internal static let title = L10n.tr("iOSApp", "voluntaryReachability.addProofSuccess.title")
      }
      internal enum Consent {
        /// Agree
        internal static let accept = L10n.tr("iOSApp", "voluntaryReachability.consent.accept")
        /// I hereby confirm the truthfulness of my contact data. The vaccination/recovery document belongs to me.
        internal static let approve = L10n.tr("iOSApp", "voluntaryReachability.consent.approve")
        /// I declare my consent in accordance with Art. 9 (2) a) in combination with Art. 6 (1) 1 a) DSGVO to make my vaccination or recovery document and my contact data that is already stored in the luca app available for my local health department via the luca system. The health department can search for me in the luca system if necessary and send me messages via the luca app. I can revoke my consent at any time via the account tab in my luca app. Deletion is automatic, at the latest, after 7 days of inactivity.
        internal static let description = L10n.tr("iOSApp", "voluntaryReachability.consent.description")
        /// Consent
        internal static let title = L10n.tr("iOSApp", "voluntaryReachability.consent.title")
      }
      internal enum ContactData {
        /// Address
        internal static let address = L10n.tr("iOSApp", "voluntaryReachability.contactData.address")
        /// Date of birth
        internal static let dateOfBirth = L10n.tr("iOSApp", "voluntaryReachability.contactData.dateOfBirth")
        /// First and last name
        internal static let firstLastName = L10n.tr("iOSApp", "voluntaryReachability.contactData.firstLastName")
        /// Email
        internal static let mail = L10n.tr("iOSApp", "voluntaryReachability.contactData.mail")
        /// In addition to your document, your contact information is also encrypted and shared. You can edit your information in your "Account".
        internal static let message = L10n.tr("iOSApp", "voluntaryReachability.contactData.message")
        /// Phone number
        internal static let phone = L10n.tr("iOSApp", "voluntaryReachability.contactData.phone")
        /// Share
        internal static let share = L10n.tr("iOSApp", "voluntaryReachability.contactData.share")
        /// Data
        internal static let title = L10n.tr("iOSApp", "voluntaryReachability.contactData.title")
      }
      internal enum Kritis {
        /// Company name (optional)
        internal static let company = L10n.tr("iOSApp", "voluntaryReachability.kritis.company")
        /// Industry (optional)
        internal static let industry = L10n.tr("iOSApp", "voluntaryReachability.kritis.industry")
        /// I am part of a critical infrastructure
        internal static let infrastructure = L10n.tr("iOSApp", "voluntaryReachability.kritis.infrastructure")
        /// Are you part of critical infrastructure (KRITIS) or do you work with vulnerable groups? If you're not sure if you're part of the critical infrastructure, you can find more information here.\n\nNote: Only fill in these boxes if you are part of a critical infrastructure (KRITIS) or work with vulnerable groups.
        internal static let message = L10n.tr("iOSApp", "voluntaryReachability.kritis.message")
        /// Optional information
        internal static let `optional` = L10n.tr("iOSApp", "voluntaryReachability.kritis.optional")
        /// Optional: Additional information
        internal static let title = L10n.tr("iOSApp", "voluntaryReachability.kritis.title")
        /// I work with vulnerable groups
        internal static let vulnerableGroups = L10n.tr("iOSApp", "voluntaryReachability.kritis.vulnerableGroups")
        internal enum Link {
          /// here
          internal static let text = L10n.tr("iOSApp", "voluntaryReachability.kritis.link.text")
          /// https://www.luca-app.de/weitere-informationen-fuer-gesundheitsaemter-wer-gehoert-zur-kritischen-infrastruktur/
          internal static let url = L10n.tr("iOSApp", "voluntaryReachability.kritis.link.url")
        }
      }
      internal enum ScanQRCode {
        /// Scan the QR code of your document (e.g. EU vaccination certificate / proof of recovery).
        internal static let message = L10n.tr("iOSApp", "voluntaryReachability.scanQRCode.message")
        /// Add document
        internal static let title = L10n.tr("iOSApp", "voluntaryReachability.scanQRCode.title")
      }
      internal enum Share {
        /// Activate
        internal static let action = L10n.tr("iOSApp", "voluntaryReachability.share.action")
        /// Support your health department and receive important information (e.g. recommendations for action, information about the vaccination) and notifications directly in the luca app.\n\nYour data is sent encrypted and is only shown when there is an exact match from a responsible health department employee's search.\nProviding your information helps health departments work faster and more efficiently, and improves the notification process.
        internal static let message = L10n.tr("iOSApp", "voluntaryReachability.share.message")
        /// Receive personal notifications from your local health department with luca connect!
        internal static let title = L10n.tr("iOSApp", "voluntaryReachability.share.title")
      }
      internal enum Thankyou {
        /// Close
        internal static let close = L10n.tr("iOSApp", "voluntaryReachability.thankyou.close")
        /// Thankyou
        internal static let imageName = L10n.tr("iOSApp", "voluntaryReachability.thankyou.imageName")
        /// By providing your proof of recovery/vaccination document and your contact details, you are helping your local health department by stopping infection chains early.\n\nYour local health department can provide you with important information and notifications quickly and directly via the luca app.
        internal static let message = L10n.tr("iOSApp", "voluntaryReachability.thankyou.message")
        /// Thank you for your help!
        internal static let title = L10n.tr("iOSApp", "voluntaryReachability.thankyou.title")
      }
      internal enum VacStatus {
        /// Add certificate
        internal static let addCert = L10n.tr("iOSApp", "voluntaryReachability.vacStatus.addCert")
        /// You haven't added a certificate yet.
        internal static let certMissing = L10n.tr("iOSApp", "voluntaryReachability.vacStatus.certMissing")
        /// To activate personal notifications, you must have a proof of recovery or vaccination stored within the luca app.\n\nYour data will be encrypted and sent to your local health department and can only be used by them. It will only be shown in a name-based search by a health office employee.\n\nShared documents:
        internal static let message = L10n.tr("iOSApp", "voluntaryReachability.vacStatus.message")
        /// Provide
        internal static let provide = L10n.tr("iOSApp", "voluntaryReachability.vacStatus.provide")
        /// Recovery certificate
        internal static let recovery = L10n.tr("iOSApp", "voluntaryReachability.vacStatus.recovery")
        /// Enable personal notifications: Provide a document
        internal static let title = L10n.tr("iOSApp", "voluntaryReachability.vacStatus.title")
        /// Vaccination
        internal static let vaccination = L10n.tr("iOSApp", "voluntaryReachability.vacStatus.vaccination")
      }
    }
    internal enum Welcome {
      internal enum Checkboxes {
        /// Please accept the terms of use to continue.
        internal static let accessibilityError = L10n.tr("iOSApp", "welcome.checkboxes.accessibilityError")
      }
      internal enum Country {
        /// Please select the country you are currently in.
        internal static let description = L10n.tr("iOSApp", "welcome.country.description")
        /// Select country
        internal static let title = L10n.tr("iOSApp", "welcome.country.title")
      }
      internal enum Info {
        /// luca helps you encrypt and securely submit your contact data. With luca, you don't have to worry about your data when visiting events, restaurants, cafés or bars anymore.
        internal static let description = L10n.tr("iOSApp", "welcome.info.description")
        /// Hello
        internal static let hello = L10n.tr("iOSApp", "welcome.info.hello")
        /// GET STARTED!
        internal static let okButton = L10n.tr("iOSApp", "welcome.info.okButton")
      }
    }
    internal enum WelcomeViewController {
      /// GitLab
      internal static let gitLab = L10n.tr("iOSApp", "welcomeViewController.gitLab")
      /// https://www.luca-app.de/faq/
      internal static let linkFAQ = L10n.tr("iOSApp", "welcomeViewController.linkFAQ")
      /// https://gitlab.com/lucaapp/ios
      internal static let linkGitLab = L10n.tr("iOSApp", "welcomeViewController.linkGitLab")
      /// https://luca-app.de/app-privacy-policy/
      internal static let linkPrivacyPolicy = L10n.tr("iOSApp", "welcomeViewController.linkPrivacyPolicy")
      /// https://www.luca-app.de/app-terms-and-conditions
      internal static let linkTC = L10n.tr("iOSApp", "welcomeViewController.linkT_C")
      /// privacy policy
      internal static let termPrivacyPolicy = L10n.tr("iOSApp", "welcomeViewController.termPrivacyPolicy")
      /// terms of use
      internal static let termTC = L10n.tr("iOSApp", "welcomeViewController.termT_C")
      internal enum PrivacyPolicy {
        /// Privacy Policy Checkbox
        internal static let checkboxAccessibility = L10n.tr("iOSApp", "welcomeViewController.privacyPolicy.checkboxAccessibility")
        /// You can view our privacy policy under this link.
        internal static let message = L10n.tr("iOSApp", "welcomeViewController.privacyPolicy.message")
        internal enum Checkbox {
          /// Confirmed: I have read and agree to the privacy policy. Double tap to unconfirm.
          internal static let confirmed = L10n.tr("iOSApp", "welcomeViewController.privacyPolicy.checkbox.confirmed")
          /// Not confirmed: I have read and agree to the privacy policy. Double tap to confirm.
          internal static let notConfirmed = L10n.tr("iOSApp", "welcomeViewController.privacyPolicy.checkbox.notConfirmed")
        }
      }
      internal enum TermsAndConditions {
        /// Terms of Use Checkbox
        internal static let checkboxAccessibility = L10n.tr("iOSApp", "welcomeViewController.termsAndConditions.checkboxAccessibility")
        /// Not confirmed
        internal static let checkboxAccessibilityOff = L10n.tr("iOSApp", "welcomeViewController.termsAndConditions.checkboxAccessibilityOff")
        /// Confirmed
        internal static let checkboxAccessibilityOn = L10n.tr("iOSApp", "welcomeViewController.termsAndConditions.checkboxAccessibilityOn")
        /// I accept the terms of use.
        internal static let checkboxMessage = L10n.tr("iOSApp", "welcomeViewController.termsAndConditions.checkboxMessage")
        internal enum Checkbox {
          /// Confirmed: I accept the terms of use. Double tap to unconfirm.
          internal static let confirmed = L10n.tr("iOSApp", "welcomeViewController.termsAndConditions.checkbox.confirmed")
          /// Not confirmed: I accept the terms of use. Double tap to confirm.
          internal static let notConfirmed = L10n.tr("iOSApp", "welcomeViewController.termsAndConditions.checkbox.notConfirmed")
        }
      }
    }
  }
  internal enum UserApp {
    internal enum Account {
      internal enum Legal {
        internal enum Disclosure {
          /// Legal disclosure
          internal static let title = L10n.tr("UserApp", "account.legal.disclosure.title")
        }
      }
    }
    internal enum Checked {
      internal enum In {
        /// This is where you will soon be able to use all the functions provided by your luca location.
        internal static let text = L10n.tr("UserApp", "checked.in.text")
      }
    }
    internal enum Checkin {
      internal enum Activate {
        internal enum Contact {
          /// Contact tracing
          internal static let tracing = L10n.tr("UserApp", "checkin.activate.contact.tracing")
        }
      }
      internal enum Redesign {
        internal enum Account {
          /// New view after check-in
          internal static let title = L10n.tr("UserApp", "checkin.redesign.account.title")
        }
      }
    }
    internal enum Confirm {
      internal enum End {
        internal enum Contact {
          /// You currently have contact tracing enabled. When you check out, it will be automatically ended. Do you want to check out and automatically stop contact tracing?
          internal static let tracing = L10n.tr("UserApp", "confirm.end.contact.tracing")
        }
      }
    }
    internal enum Contact {
      internal enum Tracing {
        internal enum Account {
          /// Contact tracing
          internal static let title = L10n.tr("UserApp", "contact.tracing.account.title")
        }
      }
    }
    internal enum Daily {
      internal enum Key {
        internal enum Account {
          /// Daily keys are an important element of contact tracing via luca, as they’re necessary for decrypting the contact data. If contact tracing is active, health departments create new daily keys on a regular basis. When there is an active daily key, you can find it here.
          internal static let text = L10n.tr("UserApp", "daily.key.account.text")
          /// Daily key
          internal static let title = L10n.tr("UserApp", "daily.key.account.title")
        }
      }
    }
    internal enum Discovery {
      /// With luca Discovery, you can explore luca Locations near you.\n\nWho knows, you may find your new favorite restaurant?
      internal static let description = L10n.tr("UserApp", "discovery.description")
      /// Discover
      internal static let discoverButton = L10n.tr("UserApp", "discovery.discoverButton")
      /// Discover luca Locations
      internal static let title = L10n.tr("UserApp", "discovery.title")
      internal enum Navbar {
        /// luca Discovery
        internal static let title = L10n.tr("UserApp", "discovery.navbar.title")
      }
    }
    internal enum Error {
      internal enum Document {
        internal enum Invalid {
          /// The document could not be imported. The certificate may be invalid.
          internal static let text = L10n.tr("UserApp", "error.document.invalid.text")
          /// Something went wrong
          internal static let title = L10n.tr("UserApp", "error.document.invalid.title")
        }
        internal enum Unknown {
          /// The document could not be imported.
          internal static let text = L10n.tr("UserApp", "error.document.unknown.text")
          /// Something went wrong
          internal static let title = L10n.tr("UserApp", "error.document.unknown.title")
        }
      }
      internal enum Incomplete {
        internal enum Contact {
          /// Check-in failed\n\nThe check-in can't be performed, because the contact data you entered aren't complete. Please go to the account tab, complete your contact data and then try again.
          internal static let data = L10n.tr("UserApp", "error.incomplete.contact.data")
        }
      }
      internal enum Invalid {
        internal enum Deeplink {
          /// The link below is not valid: \n<deeplink>
          internal static let description = L10n.tr("UserApp", "error.invalid.deeplink.description")
          /// Something went wrong
          internal static let title = L10n.tr("UserApp", "error.invalid.deeplink.title")
        }
      }
      internal enum No {
        internal enum Daily {
          /// No Daily Key\n\nUnfortunately, the action cannot be carried out because no health department is currently handling contact tracing via luca.
          internal static let key = L10n.tr("UserApp", "error.no.daily.key")
        }
      }
      internal enum Pay {
        internal enum Split {
          /// The specified amount does not match the open invoice amount. Please check the payment amount and try again.
          internal static let amount = L10n.tr("UserApp", "error.pay.split.amount")
        }
      }
      internal enum Request {
        /// The operation could not be performed because the server is not responding. Please try again.
        internal static let timeout = L10n.tr("UserApp", "error.request.timeout")
        internal enum Failed {
          /// Please try again later. If the error occurs again, please contact our support team.
          internal static let text = L10n.tr("UserApp", "error.request.failed.text")
          /// Something went wrong
          internal static let title = L10n.tr("UserApp", "error.request.failed.title")
        }
      }
      internal enum Split {
        internal enum Exceeds {
          /// The amount exceeds the open bill.
          internal static let amount = L10n.tr("UserApp", "error.split.exceeds.amount")
        }
      }
    }
    internal enum Expl {
      internal enum Account {
        /// luca Promotion
        internal static let promotion = L10n.tr("UserApp", "expl.account.promotion")
      }
      internal enum Checkin {
        internal enum Redesign {
          /// We redesigned the screen after check-in completely. Now you can find there all important information about the location where you have checked in at a glance. Simply scan the luca QR code to find the new view. We hope you like it!
          internal static let text = L10n.tr("UserApp", "expl.checkin.redesign.text")
          /// luca has a new look!
          internal static let title = L10n.tr("UserApp", "expl.checkin.redesign.title")
        }
      }
      internal enum New {
        internal enum Check {
          internal enum In {
            internal enum _1 {
              /// Currently, contact tracking via luca is on hold. This means for you: Your contact information will no longer be recorded when you scan a QR code.
              internal static let text = L10n.tr("UserApp", "expl.new.check.in.1.text")
              /// No contact tracing
              internal static let title = L10n.tr("UserApp", "expl.new.check.in.1.title")
            }
            internal enum _2 {
              /// You no longer need to enter contact information to use luca. If you have imported documents, you must enter your first name and last name. Make sure that the names match the information on your certificates. All other information is optional.
              internal static let text = L10n.tr("UserApp", "expl.new.check.in.2.text")
              /// Optional contact data
              internal static let title = L10n.tr("UserApp", "expl.new.check.in.2.title")
            }
            internal enum _3 {
              /// luca is still available for you and offers you many advantages. You can check in as before and use the functions provided by the luca location.
              internal static let text = L10n.tr("UserApp", "expl.new.check.in.3.text")
              /// New check-in
              internal static let title = L10n.tr("UserApp", "expl.new.check.in.3.title")
            }
          }
        }
      }
      internal enum Pay {
        internal enum _1 {
          /// You can now pay in luca locations with luca Pay. Scan the table QR code or QR code on the operator's device, add a tip and you're done!
          internal static let text = L10n.tr("UserApp", "expl.pay.1.text")
          /// Already heard of luca Pay?
          internal static let title = L10n.tr("UserApp", "expl.pay.1.title")
        }
        internal enum _2 {
          /// You can find luca Pay in a new tab in your app. There you can activate luca Pay, enter your payment data and view an overview of your payments. The payment history is created for you and can be accessed via this smartphone.
          internal static let text = L10n.tr("UserApp", "expl.pay.2.text")
          /// luca Pay - fast and secure
          internal static let title = L10n.tr("UserApp", "expl.pay.2.title")
        }
      }
      internal enum Raffle {
        internal enum _1 {
          /// Until August 31, 2022, you have the chance to win a €500 meal voucher for the restaurant where you made the payment by paying with luca Pay.\nAt the same time, every win supports the restaurant: if you win, the staff will also receive €500.
          internal static let text = L10n.tr("UserApp", "expl.raffle.1.text")
          /// Win €500 coupon now
          internal static let title = L10n.tr("UserApp", "expl.raffle.1.title")
        }
        internal enum _2 {
          /// Participation is easy: You get a raffle ticket when you pay with luca Pay at participating locations. Then all you have to do is enter your e-mail address and confirm your participation - and you could win a €500 food voucher.
          internal static let text = L10n.tr("UserApp", "expl.raffle.2.text")
          /// Here's how it works
          internal static let title = L10n.tr("UserApp", "expl.raffle.2.title")
        }
        internal enum Account {
          /// luca raffle
          internal static let title = L10n.tr("UserApp", "expl.raffle.account.title")
        }
      }
      internal enum Tiptopup {
        internal enum _1 {
          /// Paying with luca Pay doesn't only benefit you. Every time you tip with luca Pay during the campaign period, luca adds 10%% to your tip.
          internal static let text = L10n.tr("UserApp", "expl.tiptopup.1.text")
          /// We increase the tip
          internal static let title = L10n.tr("UserApp", "expl.tiptopup.1.title")
        }
      }
    }
    internal enum Navigation {
      internal enum Tab {
        /// Discovery
        internal static let discovery = L10n.tr("UserApp", "navigation.tab.discovery")
      }
    }
    internal enum Pay {
      /// Refunded
      internal static let refunded = L10n.tr("UserApp", "pay.refunded")
      internal enum Account {
        /// Activate luca Pay
        internal static let `switch` = L10n.tr("UserApp", "pay.account.switch")
        /// Activate luca Pay and benefit from more service and less waiting times.
        internal static let text = L10n.tr("UserApp", "pay.account.text")
        /// luca Pay
        internal static let title = L10n.tr("UserApp", "pay.account.title")
      }
      internal enum Activate {
        /// Activate
        internal static let button = L10n.tr("UserApp", "pay.activate.button")
      }
      internal enum Activation {
        /// Hereby I declare my consent to the processing of my payment information, information about my payment transactions, for example with a restaurant, and my individual user ID by luca for the purpose of making payments by the payment service provider Rapyd Europe according to Art. 6 (1) 1 a) in combination with Art. 49 (1) 1 a) GDPR. This allows me to repeatedly initiate payments without re-collecting the above-mentioned data as well as viewing and checking my payment history. I have taken note of further information in the luca <!%@]> and consent to Rapyd Europe’s disclosure of my aforementioned data to third parties who may process such data outside the European Economic Area (EEA) and Switzerland for the stated purpose. I am aware that these countries do not always have the same level of data protection as the EU. However, Rapyd Europe has taken concrete measures to ensure the protection of my data in accordance with EEA data protection law. I can revoke my consent at any time for the future.
        internal static func consent(_ p1: Any) -> String {
          return L10n.tr("UserApp", "pay.activation.consent", String(describing: p1))
        }
      }
      internal enum Amount {
        /// Enter amount
        internal static let headline = L10n.tr("UserApp", "pay.amount.headline")
        /// Please ask the staff for your invoice amount for <locationName> and enter it here.
        internal static let text = L10n.tr("UserApp", "pay.amount.text")
      }
      internal enum Cancel {
        /// The payment was cancelled, no money was withdrawn from you.
        internal static let text = L10n.tr("UserApp", "pay.cancel.text")
        /// Payment cancelled
        internal static let title = L10n.tr("UserApp", "pay.cancel.title")
      }
      internal enum Continue {
        /// Continue with <amount>
        internal static let button = L10n.tr("UserApp", "pay.continue.button")
      }
      internal enum Data {
        internal enum Request {
          internal enum Content {
            /// If you have explicitly given your consent to the use of luca Pay in accordance with Article 6 (1) 1 a) DSGVO in combination with Article 49 (1) 1. a) and have stored your payment method information in the luca system or with our subcontractor Rapyd Europe, your personal data has been processed.\n\nIn addition to storing your means of payment, your luca app also provides you with a list of your payment transactions.\n\nYour payment method information is only stored with our subcontractor Rapyd Europe. Your personal payment history is stored on your own device within your luca app. However, information about your payments as well as your user ID are also stored at Rapyd Europe as well as in the luca system. You can view Rapyd Europe's privacy policy here: https://www.rapyd.net/privacypolicy/. The luca server is hosted by Deutsche Telekom AG. Our subcontractor neXenio provides software development, software maintenance and software operation services.\n
            internal static let `prefix` = L10n.tr("UserApp", "pay.data.request.content.prefix")
            /// If you would like to know whether further data is stored by our contractor Rapyd, please provide us with your user ID. This identifier will allow us to determine for you whether your personal data is held by our contractor Rapyd.
            internal static let suffix = L10n.tr("UserApp", "pay.data.request.content.suffix")
            internal enum Payment {
              /// Payment information:
              internal static let info = L10n.tr("UserApp", "pay.data.request.content.payment.info")
              internal enum History {
                /// Amount
                internal static let amount = L10n.tr("UserApp", "pay.data.request.content.payment.history.amount")
                /// Payment history (locally in your luca app, on the luca server, and at Rapyd):
                internal static let info = L10n.tr("UserApp", "pay.data.request.content.payment.history.info")
                /// Location
                internal static let location = L10n.tr("UserApp", "pay.data.request.content.payment.history.location")
                /// Table
                internal static let table = L10n.tr("UserApp", "pay.data.request.content.payment.history.table")
                internal enum Payment {
                  /// TransactionID
                  internal static let code = L10n.tr("UserApp", "pay.data.request.content.payment.history.payment.code")
                }
                internal enum Tip {
                  /// Tip
                  internal static let amount = L10n.tr("UserApp", "pay.data.request.content.payment.history.tip.amount")
                }
              }
            }
            internal enum User {
              internal enum Id {
                /// User object (local in your luca App, on the luca server and at Rapyd, deletion by app functionalities):
                internal static let info = L10n.tr("UserApp", "pay.data.request.content.user.id.info")
                /// User ID
                internal static let label = L10n.tr("UserApp", "pay.data.request.content.user.id.label")
              }
              internal enum Revocation {
                /// Revocation code:
                internal static let code = L10n.tr("UserApp", "pay.data.request.content.user.revocation.code")
              }
            }
          }
        }
      }
      internal enum Delete {
        internal enum Alert {
          /// If you deactivate luca Pay, your luca Pay account including the payment history on this device will be deleted.
          internal static let text = L10n.tr("UserApp", "pay.delete.alert.text")
          /// Deactivate luca Pay account
          internal static let title = L10n.tr("UserApp", "pay.delete.alert.title")
        }
      }
      internal enum Details {
        /// Total:
        internal static let amount = L10n.tr("UserApp", "pay.details.amount")
        /// Date:
        internal static let date = L10n.tr("UserApp", "pay.details.date")
        /// Table:
        internal static let table = L10n.tr("UserApp", "pay.details.table")
        /// Time:
        internal static let time = L10n.tr("UserApp", "pay.details.time")
        /// Payment
        internal static let title = L10n.tr("UserApp", "pay.details.title")
        internal enum Discount {
          /// Currently luca takes over %@%% of every payment made via luca Pay in this venue. Maximum discount €%@.
          internal static func hint(_ p1: Any, _ p2: Any) -> String {
            return L10n.tr("UserApp", "pay.details.discount.hint", String(describing: p1), String(describing: p2))
          }
        }
        internal enum Invoice {
          /// Amount:
          internal static let amount = L10n.tr("UserApp", "pay.details.invoice.amount")
        }
        internal enum Payment {
          /// Transaction ID:
          internal static let code = L10n.tr("UserApp", "pay.details.payment.code")
        }
        internal enum Raffle {
          /// With this payment you take part in the €500 + €500 raffle. For more information, please check your confirmation email.
          internal static let hint = L10n.tr("UserApp", "pay.details.raffle.hint")
        }
        internal enum Report {
          /// Report a problem
          internal static let problem = L10n.tr("UserApp", "pay.details.report.problem")
        }
        internal enum Tip {
          /// Tip:
          internal static let amount = L10n.tr("UserApp", "pay.details.tip.amount")
        }
      }
      internal enum Empty {
        internal enum Screen {
          /// You can now pay with luca Pay in participating luca locations. Scan the table QR code or the QR code shown and pay. Once you have made your first payments, you can view them here in your luca Pay tab. Additionally, you can save your payment method data and reuse it next time.
          internal static let text = L10n.tr("UserApp", "pay.empty.screen.text")
          /// Pay fast and easy with luca Pay!
          internal static let title = L10n.tr("UserApp", "pay.empty.screen.title")
        }
      }
      internal enum Error {
        /// The payment failed, no money was withdrawn.
        internal static let text = L10n.tr("UserApp", "pay.error.text")
        /// Payment failed
        internal static let title = L10n.tr("UserApp", "pay.error.title")
        internal enum Expired {
          /// Your request has expired or was canceled. Please try again.
          internal static let text = L10n.tr("UserApp", "pay.error.expired.text")
          /// Payment failed
          internal static let title = L10n.tr("UserApp", "pay.error.expired.title")
        }
      }
      internal enum Info {
        internal enum Screen {
          /// Activate luca Pay
          internal static let headline = L10n.tr("UserApp", "pay.info.screen.headline")
          /// Activate luca Pay and benefit from more service and less waiting time in luca locations. By activating luca Pay for this device, you can pay easily and securely with luca Pay at participating luca locations, save your payment method data for further payments, and view your payment history.
          internal static let text = L10n.tr("UserApp", "pay.info.screen.text")
        }
      }
      internal enum Initiate {
        /// Start payment
        internal static let button = L10n.tr("UserApp", "pay.initiate.button")
      }
      internal enum Invoice {
        /// Amount:
        internal static let amount = L10n.tr("UserApp", "pay.invoice.amount")
      }
      internal enum Max {
        /// Maximum amount €<maxAmount>
        internal static let amount = L10n.tr("UserApp", "pay.max.amount")
      }
      internal enum Min {
        /// Minimum amount €<minAmount>
        internal static let amount = L10n.tr("UserApp", "pay.min.amount")
      }
      internal enum Nav {
        /// Pay
        internal static let bar = L10n.tr("UserApp", "pay.nav.bar")
      }
      internal enum No {
        /// No tip
        internal static let tip = L10n.tr("UserApp", "pay.no.tip")
      }
      internal enum Not {
        internal enum Available {
          /// QR-Code can't be read
          internal static let headline = L10n.tr("UserApp", "pay.not.available.headline")
          /// This location is not using luca for payment yet.
          internal static let text = L10n.tr("UserApp", "pay.not.available.text")
        }
      }
      internal enum Open {
        /// New payment
        internal static let scanner = L10n.tr("UserApp", "pay.open.scanner")
      }
      internal enum Raffle {
        internal enum Consent {
          /// With every payment you make with luca Pay during the campaign period, you have the chance to win a €500 meal voucher for the restaurant (available in participating restaurants). At the same time, you also support the restaurant with every win: if you win, the restaurant staff will also receive €500.\nEnter your e-mail address here and confirm your participation if you want to win a raffle ticket.
          internal static let text = L10n.tr("UserApp", "pay.raffle.consent.text")
          /// €500 + €500 Raffle
          internal static let title = L10n.tr("UserApp", "pay.raffle.consent.title")
          internal enum Terms {
            /// I confirm that I'm over 18 years old and accept the [terms of participation](https://www.luca-app.de/luca-pay-gewinnspiel-locations-und-nutzerinnen-tc-nutzerinnen)
            internal static let accept = L10n.tr("UserApp", "pay.raffle.consent.terms.accept")
          }
        }
        internal enum Mail {
          /// e-mail
          internal static let field = L10n.tr("UserApp", "pay.raffle.mail.field")
        }
        internal enum Participate {
          /// Participate
          internal static let button = L10n.tr("UserApp", "pay.raffle.participate.button")
        }
      }
      internal enum Retry {
        /// Try again
        internal static let button = L10n.tr("UserApp", "pay.retry.button")
      }
      internal enum Revocation {
        internal enum Code {
          /// luca Pay revocation code
          internal static let button = L10n.tr("UserApp", "pay.revocation.code.button")
          /// Your revocation code:
          internal static let label = L10n.tr("UserApp", "pay.revocation.code.label")
          /// Note: The revocation code is needed to lock luca Pay on this device in case you lose your phone. Therefore, make sure you save it in a way that you can access it even if you don't have this device.
          internal static let text = L10n.tr("UserApp", "pay.revocation.code.text")
        }
        internal enum Notification {
          /// Save code
          internal static let button = L10n.tr("UserApp", "pay.revocation.notification.button")
          /// Here you get the revocation code with which you can lock your luca Pay account in case you lose your phone. If you lock your account, your former payments can no longer be viewed on the device and no new payments can be made. So save this code well and make sure you don't have it only on this device.
          internal static let text = L10n.tr("UserApp", "pay.revocation.notification.text")
          /// Your luca Pay revocation code
          internal static let title = L10n.tr("UserApp", "pay.revocation.notification.title")
        }
      }
      internal enum Split {
        /// You pay:
        internal static let amount = L10n.tr("UserApp", "pay.split.amount")
        internal enum Discount {
          /// luca Promotion is calculated in the next step.
          internal static let hint = L10n.tr("UserApp", "pay.split.discount.hint")
        }
        internal enum Initiate {
          /// Split amount
          internal static let button = L10n.tr("UserApp", "pay.split.initiate.button")
        }
        internal enum Open {
          /// Open amount:
          internal static let amount = L10n.tr("UserApp", "pay.split.open.amount")
        }
      }
      internal enum Success {
        /// Amount:
        internal static let amount = L10n.tr("UserApp", "pay.success.amount")
        /// Tip:
        internal static let tip = L10n.tr("UserApp", "pay.success.tip")
        /// luca adds:
        internal static let tiptopup = L10n.tr("UserApp", "pay.success.tiptopup")
        /// Payment done
        internal static let title = L10n.tr("UserApp", "pay.success.title")
        /// Transaction ID:
        internal static let transactionID = L10n.tr("UserApp", "pay.success.transactionID")
        internal enum Raffle {
          /// Win €500
          internal static let button = L10n.tr("UserApp", "pay.success.raffle.button")
        }
      }
      internal enum Support {
        internal enum Mail {
          /// Dear luca support,\n\nthe following problem occurs when using the luca Pay App:\n\nAdditional Information:\n<deviceInfo>\n<paymentInfo>
          internal static let body = L10n.tr("UserApp", "pay.support.mail.body")
          /// Problems with the use of luca Pay
          internal static let subject = L10n.tr("UserApp", "pay.support.mail.subject")
          internal enum Body {
            internal enum Payment {
              /// Transaction ID: <paymentCode>\nPayment Date: <paymentDate>\nLocation: <locationName>
              internal static let info = L10n.tr("UserApp", "pay.support.mail.body.payment.info")
            }
          }
        }
      }
      internal enum Tab {
        /// luca Pay
        internal static let title = L10n.tr("UserApp", "pay.tab.title")
      }
      internal enum Tip {
        /// Add tip
        internal static let headline = L10n.tr("UserApp", "pay.tip.headline")
        /// How much tip do you want to add?
        internal static let text = L10n.tr("UserApp", "pay.tip.text")
      }
      internal enum Tiptopup {
        internal enum Additional {
          internal enum Info {
            /// More information
            internal static let accessibility = L10n.tr("UserApp", "pay.tiptopup.additional.info.accessibility")
          }
        }
        internal enum Info {
          /// In the campaign period luca adds 10%% to the tip for every payment made via luca Pay in participating restaurants.
          internal static let text = L10n.tr("UserApp", "pay.tiptopup.info.text")
          /// 10%% extra tip
          internal static let title = L10n.tr("UserApp", "pay.tiptopup.info.title")
        }
      }
    }
    internal enum Raffle {
      internal enum Error {
        internal enum Mail {
          internal enum Missing {
            /// Please enter your e-mail address to participate in the raffle.
            internal static let text = L10n.tr("UserApp", "raffle.error.mail.missing.text")
            /// No email address
            internal static let title = L10n.tr("UserApp", "raffle.error.mail.missing.title")
          }
        }
      }
    }
    internal enum Rapyd {
      /// Performed by Rapyd
      internal static let disclaimer = L10n.tr("UserApp", "rapyd.disclaimer")
    }
    internal enum Registration {
      internal enum Contact {
        internal enum Tracing {
          /// Encrypt data
          internal static let button = L10n.tr("UserApp", "registration.contact.tracing.button")
          /// This location asks for your contact data for contact tracing. It may be that further data is required for this.
          internal static let text = L10n.tr("UserApp", "registration.contact.tracing.text")
          /// Enable contact tracing
          internal static let title = L10n.tr("UserApp", "registration.contact.tracing.title")
        }
      }
      internal enum Name {
        /// Continue
        internal static let button = L10n.tr("UserApp", "registration.name.button")
        /// Please add your name. All other data is optional.
        internal static let text = L10n.tr("UserApp", "registration.name.text")
        /// What's your name?
        internal static let title = L10n.tr("UserApp", "registration.name.title")
      }
      internal enum Success {
        /// Done
        internal static let button = L10n.tr("UserApp", "registration.success.button")
        /// You can now use luca to check in to luca Locations. Depending on the Location, you can now access further information or pay via luca Pay.
        internal static let text = L10n.tr("UserApp", "registration.success.text")
        /// That's it!
        internal static let title = L10n.tr("UserApp", "registration.success.title")
      }
      internal enum Welcome {
        /// Let's go!
        internal static let button = L10n.tr("UserApp", "registration.welcome.button")
        /// I accept the [terms and conditions](https://www.luca-app.de/app-terms-and-conditions-2/).
        internal static let terms = L10n.tr("UserApp", "registration.welcome.terms")
        /// luca helps you digitally shape your social life, while giving you full control over what data is used, when, and for what purpose.\n\nLet's go!
        internal static let text = L10n.tr("UserApp", "registration.welcome.text")
        /// Hello
        internal static let title = L10n.tr("UserApp", "registration.welcome.title")
        internal enum Privacy {
          /// I accept the [privacy policy](https://www.luca-app.de/app-privacy-policy/).
          internal static let policy = L10n.tr("UserApp", "registration.welcome.privacy.policy")
        }
      }
    }
    internal enum Support {
      internal enum Account {
        internal enum Email {
          /// luca App: Support request
          internal static let subject = L10n.tr("UserApp", "support.account.email.subject")
        }
      }
    }
    internal enum Ticket {
      /// Category:
      internal static let category = L10n.tr("UserApp", "ticket.category")
      /// Date:
      internal static let date = L10n.tr("UserApp", "ticket.date")
      /// Ticket ID:
      internal static let id = L10n.tr("UserApp", "ticket.ID")
      /// Issuer:
      internal static let issuer = L10n.tr("UserApp", "ticket.issuer")
      /// Location:
      internal static let location = L10n.tr("UserApp", "ticket.location")
      /// Verification ID:
      internal static let verificationID = L10n.tr("UserApp", "ticket.verificationID")
      internal enum Category {
        /// Classic
        internal static let classic = L10n.tr("UserApp", "ticket.category.classic")
        /// Conference
        internal static let conference = L10n.tr("UserApp", "ticket.category.conference")
        /// Culture
        internal static let culture = L10n.tr("UserApp", "ticket.category.culture")
        /// Exhibition
        internal static let exhibition = L10n.tr("UserApp", "ticket.category.exhibition")
        /// Family
        internal static let family = L10n.tr("UserApp", "ticket.category.family")
        /// Movie
        internal static let movie = L10n.tr("UserApp", "ticket.category.movie")
        /// Music
        internal static let music = L10n.tr("UserApp", "ticket.category.music")
        /// Other
        internal static let others = L10n.tr("UserApp", "ticket.category.others")
        /// Party
        internal static let party = L10n.tr("UserApp", "ticket.category.party")
        /// Sport
        internal static let sport = L10n.tr("UserApp", "ticket.category.sport")
      }
      internal enum Import {
        /// I hereby declare my consent in accordance with Art. 9 (2) a) in conjunction with Art. 6 (1) 1 a) GDPR to the processing of my data during the import of my event ticket in the luca app. The data included in the document can be matched to my first and last name in the luca app.
        internal static let consent = L10n.tr("UserApp", "ticket.import.consent")
      }
    }
    internal enum Updated {
      internal enum Terms {
        internal enum Info {
          /// Continue
          internal static let button = L10n.tr("UserApp", "updated.terms.info.button")
          /// You want to use the new features of luca? In the next step, agree to the updated terms of use.
          internal static let text = L10n.tr("UserApp", "updated.terms.info.text")
          /// New terms of use
          internal static let title = L10n.tr("UserApp", "updated.terms.info.title")
        }
      }
    }
    internal enum Wifi {
      internal enum Connection {
        /// Unfortunately, the connection could not be established.
        internal static let failed = L10n.tr("UserApp", "wifi.connection.failed")
      }
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
