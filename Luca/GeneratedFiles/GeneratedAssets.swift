// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let _2g3gStatus = ImageAsset(name: "2g3gStatus")
  internal static let anonymous = ImageAsset(name: "Anonymous")
  internal static let anonymousEnabled = ImageAsset(name: "Anonymous_enabled")
  internal static let luca1d1d1d = ColorAsset(name: "luca1d1d1d")
  internal static let luca2a2a2a = ColorAsset(name: "luca2a2a2a")
  internal static let luca747480 = ColorAsset(name: "luca747480")
  internal static let lucaB4D296 = ColorAsset(name: "lucaB4D296")
  internal static let lucaBlack87Percent = ColorAsset(name: "lucaBlack87Percent")
  internal static let lucaCheckinGradientBottom = ColorAsset(name: "lucaCheckinGradientBottom")
  internal static let lucaCheckinGradientTop = ColorAsset(name: "lucaCheckinGradientTop")
  internal static let lucaDiscountGreen = ColorAsset(name: "lucaDiscountGreen")
  internal static let lucaEventClassic = ColorAsset(name: "lucaEventClassic")
  internal static let lucaEventConference = ColorAsset(name: "lucaEventConference")
  internal static let lucaEventCulture = ColorAsset(name: "lucaEventCulture")
  internal static let lucaEventExhibition = ColorAsset(name: "lucaEventExhibition")
  internal static let lucaEventFamily = ColorAsset(name: "lucaEventFamily")
  internal static let lucaEventMovie = ColorAsset(name: "lucaEventMovie")
  internal static let lucaEventMusic = ColorAsset(name: "lucaEventMusic")
  internal static let lucaEventOther = ColorAsset(name: "lucaEventOther")
  internal static let lucaEventParty = ColorAsset(name: "lucaEventParty")
  internal static let lucaEventSport = ColorAsset(name: "lucaEventSport")
  internal static let lucaPaymentWarning = ColorAsset(name: "lucaPaymentWarning")
  internal static let disclosureIndicator = ImageAsset(name: "DisclosureIndicator")
  internal static let explanationCheckin1 = ImageAsset(name: "explanationCheckin-1")
  internal static let explanationCheckin2 = ImageAsset(name: "explanationCheckin-2")
  internal static let explanationCheckin3 = ImageAsset(name: "explanationCheckin-3")
  internal static let explanationCheckin4 = ImageAsset(name: "explanationCheckin-4")
  internal static let explanationCheckinRedesign = ImageAsset(name: "explanationCheckinRedesign")
  internal static let explanationContactdata1 = ImageAsset(name: "explanationContactdata-1")
  internal static let explanationContactdata2 = ImageAsset(name: "explanationContactdata-2")
  internal static let explanationContactdata3 = ImageAsset(name: "explanationContactdata-3")
  internal static let explanationLuca201 = ImageAsset(name: "explanationLuca20-1")
  internal static let explanationLuca202 = ImageAsset(name: "explanationLuca20-2")
  internal static let explanationLuca203 = ImageAsset(name: "explanationLuca20-3")
  internal static let explanationLuca204 = ImageAsset(name: "explanationLuca20-4")
  internal static let explanationLuca205 = ImageAsset(name: "explanationLuca20-5")
  internal static let explanationLuca206 = ImageAsset(name: "explanationLuca20-6")
  internal static let explanationLuca207 = ImageAsset(name: "explanationLuca20-7")
  internal static let explanationLucaID1 = ImageAsset(name: "explanationLucaID-1")
  internal static let explanationMessages1 = ImageAsset(name: "explanationMessages-1")
  internal static let explanationMessages2 = ImageAsset(name: "explanationMessages-2")
  internal static let explanationMessages3 = ImageAsset(name: "explanationMessages-3")
  internal static let explanationPayment1 = ImageAsset(name: "explanationPayment-1")
  internal static let explanationPayment2 = ImageAsset(name: "explanationPayment-2")
  internal static let explanationRaffle1 = ImageAsset(name: "explanationRaffle-1")
  internal static let explanationRaffle2 = ImageAsset(name: "explanationRaffle-2")
  internal static let explanationTipTopup1 = ImageAsset(name: "explanationTipTopup-1")
  internal static let blackArrow = ImageAsset(name: "BlackArrow")
  internal static let camera = ImageAsset(name: "Camera")
  internal static let checkmark = ImageAsset(name: "Checkmark")
  internal static let child = ImageAsset(name: "Child")
  internal static let circle = ImageAsset(name: "Circle")
  internal static let cupYellow = ImageAsset(name: "CupYellow")
  internal static let hourglass = ImageAsset(name: "Hourglass")
  internal static let idBackgroundBigVertical = ImageAsset(name: "ID_Background_Big_vertical")
  internal static let idBackgroundSmall = ImageAsset(name: "ID_Background_Small")
  internal static let light = ImageAsset(name: "Light")
  internal static let person = ImageAsset(name: "Person")
  internal static let questionmarkBlue = ImageAsset(name: "Questionmark_Blue")
  internal static let `right` = ImageAsset(name: "Right")
  internal static let search = ImageAsset(name: "Search")
  internal static let thankyou = ImageAsset(name: "Thankyou")
  internal static let checkmarkSelected = ImageAsset(name: "checkmarkSelected")
  internal static let checkmarkUnselected = ImageAsset(name: "checkmarkUnselected")
  internal static let checkoutSpinnerBG = ImageAsset(name: "checkoutSpinnerBG")
  internal static let infoWhite = ImageAsset(name: "infoWhite")
  internal static let rocket = ImageAsset(name: "rocket")
  internal static let locationURLMenu = ImageAsset(name: "LocationURLMenu")
  internal static let locationURLContact = ImageAsset(name: "locationURLContact")
  internal static let locationURLMore = ImageAsset(name: "locationURLMore")
  internal static let locationURLPin = ImageAsset(name: "locationURLPin")
  internal static let locationURLTimetable = ImageAsset(name: "locationURLTimetable")
  internal static let locationURLWebsite = ImageAsset(name: "locationURLWebsite")
  internal static let money = ImageAsset(name: "Money")
  internal static let payment = ImageAsset(name: "payment")
  internal static let paymentActivateImage = ImageAsset(name: "paymentActivateImage")
  internal static let starGreen = ImageAsset(name: "starGreen")
  internal static let qrCodeScanner = ImageAsset(name: "QRCodeScanner ")
  internal static let classicEventIcon = ImageAsset(name: "classicEventIcon")
  internal static let conferenceEventIcon = ImageAsset(name: "conferenceEventIcon")
  internal static let cultureEventIcon = ImageAsset(name: "cultureEventIcon")
  internal static let exhibitionEventIcon = ImageAsset(name: "exhibitionEventIcon")
  internal static let familieEventIcon = ImageAsset(name: "familieEventIcon")
  internal static let movieEventIcon = ImageAsset(name: "movieEventIcon")
  internal static let musicEventIcon = ImageAsset(name: "musicEventIcon")
  internal static let otherEventIcon = ImageAsset(name: "otherEventIcon")
  internal static let partyEventIcon = ImageAsset(name: "partyEventIcon")
  internal static let sportEventIcon = ImageAsset(name: "sportEventIcon")
  internal static let accountActive = ImageAsset(name: "accountActive")
  internal static let addCircleWhite = ImageAsset(name: "addCircleWhite")
  internal static let addIDCard = ImageAsset(name: "addIDCard")
  internal static let addPersonBlack = ImageAsset(name: "addPersonBlack")
  internal static let addPersonWhite = ImageAsset(name: "addPersonWhite")
  internal static let arrowCircle = ImageAsset(name: "arrowCircle")
  internal static let automaticCheckout = ImageAsset(name: "automaticCheckout")
  internal static let cameraIcon = ImageAsset(name: "cameraIcon")
  internal static let checkinActive = ImageAsset(name: "checkinActive")
  internal static let checkinExit = ImageAsset(name: "checkinExit")
  internal static let checkmarkBlack = ImageAsset(name: "checkmarkBlack")
  internal static let checkmarkBlue = ImageAsset(name: "checkmarkBlue")
  internal static let checkmarkGreen = ImageAsset(name: "checkmarkGreen")
  internal static let checkmarkPayment = ImageAsset(name: "checkmarkPayment")
  internal static let childLight = ImageAsset(name: "child-light")
  internal static let closeButton = ImageAsset(name: "closeButton")
  internal static let contactData = ImageAsset(name: "contactData")
  internal static let copyItem = ImageAsset(name: "copyItem")
  internal static let deleteBin = ImageAsset(name: "deleteBin")
  internal static let directCheckin = ImageAsset(name: "directCheckin")
  internal static let editPencil = ImageAsset(name: "editPencil")
  internal static let epmtyMessages = ImageAsset(name: "epmty_messages")
  internal static let externalLink = ImageAsset(name: "externalLink")
  internal static let eye = ImageAsset(name: "eye")
  internal static let healthAuthority = ImageAsset(name: "healthAuthority")
  internal static let history = ImageAsset(name: "history")
  internal static let historyActive = ImageAsset(name: "historyActive")
  internal static let infoIcon = ImageAsset(name: "infoIcon")
  internal static let infoIconBlack = ImageAsset(name: "infoIconBlack")
  internal static let localCheckinEmptyImage = ImageAsset(name: "localCheckinEmptyImage")
  internal static let lucaAlertTint = ColorAsset(name: "lucaAlertTint")
  internal static let lucaBG = ImageAsset(name: "lucaBG")
  internal static let lucaBackgroundBlue = ColorAsset(name: "lucaBackgroundBlue")
  internal static let lucaBeige = ColorAsset(name: "lucaBeige")
  internal static let lucaBlack = ColorAsset(name: "lucaBlack")
  internal static let lucaBlackLogo = ImageAsset(name: "lucaBlackLogo")
  internal static let lucaBlackLowAlpha = ColorAsset(name: "lucaBlackLowAlpha")
  internal static let lucaBlue = ColorAsset(name: "lucaBlue")
  internal static let lucaButtonBlack = ColorAsset(name: "lucaButtonBlack")
  internal static let lucaDarkBlue = ColorAsset(name: "lucaDarkBlue")
  internal static let lucaDarkGrey = ColorAsset(name: "lucaDarkGrey")
  internal static let lucaDividerGray = ColorAsset(name: "lucaDividerGray")
  internal static let lucaEMGreen = ColorAsset(name: "lucaEMGreen")
  internal static let lucaError = ColorAsset(name: "lucaError")
  internal static let lucaGradientWelcomeBegin = ColorAsset(name: "lucaGradientWelcomeBegin")
  internal static let lucaGradientWelcomeEnd = ColorAsset(name: "lucaGradientWelcomeEnd")
  internal static let lucaGrey = ColorAsset(name: "lucaGrey")
  internal static let lucaIdCheckmark = ImageAsset(name: "lucaIdCheckmark")
  internal static let lucaLightBlue = ColorAsset(name: "lucaLightBlue")
  internal static let lucaLightGreen = ColorAsset(name: "lucaLightGreen")
  internal static let lucaLightGrey = ColorAsset(name: "lucaLightGrey")
  internal static let lucaLogo = ImageAsset(name: "lucaLogo")
  internal static let lucaLogoBlack = ImageAsset(name: "lucaLogoBlack")
  internal static let lucaWhiteHalfAlpha = ColorAsset(name: "lucaWhiteHalfAlpha")
  internal static let lucaWhiteLowAlpha = ColorAsset(name: "lucaWhiteLowAlpha")
  internal static let lucaWhiteLowAlphaText = ColorAsset(name: "lucaWhiteLowAlphaText")
  internal static let lucaWhiteTextFieldBorder = ColorAsset(name: "lucaWhiteTextFieldBorder")
  internal static let lucaWhiteTextFieldFont = ColorAsset(name: "lucaWhiteTextFieldFont")
  internal static let messages = ImageAsset(name: "messages")
  internal static let messagesEmpty = ImageAsset(name: "messages_empty")
  internal static let messagesNew = ImageAsset(name: "messages_new")
  internal static let moneyWhite = ImageAsset(name: "moneyWhite")
  internal static let myLuca = ImageAsset(name: "myLuca")
  internal static let noEntries = ImageAsset(name: "noEntries")
  internal static let notification = ImageAsset(name: "notification")
  internal static let opensLink = ImageAsset(name: "opensLink")
  internal static let personBlack = ImageAsset(name: "personBlack")
  internal static let personWhite = ImageAsset(name: "personWhite")
  internal static let plusSign = ImageAsset(name: "plusSign")
  internal static let postCodeCheck = ImageAsset(name: "postCodeCheck")
  internal static let rejectCross = ImageAsset(name: "rejectCross")
  internal static let reloadBlue = ImageAsset(name: "reloadBlue")
  internal static let rightArrow = ImageAsset(name: "rightArrow")
  internal static let scanner = ImageAsset(name: "scanner")
  internal static let share = ImageAsset(name: "share")
  internal static let sicherSein = ImageAsset(name: "sicherSein")
  internal static let sicherSeinBlack = ImageAsset(name: "sicherSeinBlack")
  internal static let spinningWheel = ImageAsset(name: "spinningWheel")
  internal static let viewMore = ImageAsset(name: "viewMore")
  internal static let viewMoreBlack = ImageAsset(name: "viewMoreBlack")
  internal static let voluntaryCheckin = ImageAsset(name: "voluntaryCheckin")
  internal static let warningCircleWhite = ImageAsset(name: "warningCircleWhite")
  internal static let warningOrange = ImageAsset(name: "warningOrange")
  internal static let warningTriangle = ImageAsset(name: "warningTriangle")
  internal static let welcome = ImageAsset(name: "welcome")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = {
    guard let color = Color(asset: self) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }()

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init?(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
}

internal extension ImageAsset.Image {
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init?(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
