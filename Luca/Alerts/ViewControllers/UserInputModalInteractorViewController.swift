import UIKit
import LucaUIComponents
import Nantes

class UserInputModalInteractorViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: NantesLabel!
    @IBOutlet weak var continueButton: LightStandardButton!
    @IBOutlet weak var titleLabel: Luca20PtBoldLabel!
    @IBOutlet weak var gradient: GradientView!
    var configuration: UserInputInteractorConfiguration?
    var actionCallback:(() -> Void)?
    var cancelCallback:(() -> Void)?

    func setup(with config: UserInputInteractorConfiguration, actionCallback:(() -> Void)? = nil, cancelCallback:(() -> Void)? = nil) {
        self.configuration = config
        self.actionCallback = actionCallback
        self.cancelCallback = cancelCallback
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let configuration = configuration {
            self.descriptionLabel.text = configuration.message
            self.titleLabel.text = configuration.title
            switch configuration.type {
            case .modal(let linkText, let url):
                descriptionLabel.delegate = self
                setupDescriptionLabel(with: configuration.message, linkText: linkText, url: url)
            case .push:
                self.title = configuration.title
                self.continueButton.removeFromSuperview()
                self.gradient.removeFromSuperview()
                self.titleLabel.removeFromSuperview()
            default:
                break
            }

            self.continueButton.setTitle(configuration.actionTitle, for: .normal)
            if let image = configuration.image {
                self.imageView.isHidden = false
                self.imageView.image = image
            }
            if configuration.displayCancelOption {
                if let cancelTitle = configuration.cancelTitle {
                    self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: cancelTitle, style: .plain, target: self, action: #selector(cancelButtonPressed))
                } else {
                    self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonPressed))
                }
            }
            setupAccessibility()
        }
    }

    private func setupDescriptionLabel(with description: String, linkText: String?, url: URL?) {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: FontFamily.Montserrat.medium.font(size: 14),
            .foregroundColor: UIColor.white
        ]
        let attrText = NSMutableAttributedString(string: description, attributes: attributes)
        descriptionLabel.attributedText = attrText
        descriptionLabel.numberOfLines = 0

        if let linkText = linkText, let linkRange = description.range(of: linkText),
           let url = url {
            let linkAttributes: [NSAttributedString.Key: Any] = [
                .foregroundColor: UIColor.white,
                .font: FontFamily.Montserrat.bold.font(size: 14)
            ]
            let clickedAttributes: [NSAttributedString.Key: Any] = [
                .foregroundColor: Asset.lucaGrey.color,
                .font: FontFamily.Montserrat.bold.font(size: 14)
            ]

            descriptionLabel.linkAttributes = linkAttributes
            descriptionLabel.activeLinkAttributes = clickedAttributes
            descriptionLabel.addLink(to: url, withRange: NSRange(linkRange, in: description))
        }

        descriptionLabel.accessibilityTraits = .link
    }

    @IBAction func actionButtonPressed(_ sender: Any) {
        actionCallback?()
    }
    @objc func cancelButtonPressed(_ sender: Any) {
        cancelCallback?()
    }
}

extension UserInputModalInteractorViewController: NantesLabelDelegate {

    func attributedLabel(_ label: NantesLabel, didSelectLink link: URL) {
        UIApplication.shared.open(link, options: [:], completionHandler: nil)
    }

}

// MARK: - Accessibility
extension UserInputModalInteractorViewController {

    private func setupAccessibility() {
        titleLabel.accessibilityTraits = .header
        if let accessibility = configuration?.accessibilityText {
            descriptionLabel.accessibilityLabel = accessibility
        }

        if let title = configuration?.accessibilityTitle {
            titleLabel.accessibilityLabel = title
        }
    }

}
