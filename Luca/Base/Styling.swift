import Foundation
import UIKit
import LucaUIComponents

internal struct Styling {
    static func applyStyling() {
        // Alerts
        UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self]).tintColor = Asset.lucaBlue.color

        configureDefaultNavigationBar()
        configureEmbeddedNavigationBar()
        configureLabels()
        configureTextFields()
        configureButtons()
        configureSwitches()

        self.styleEmbeddedChildrenViewControllers()
    }

    private static func configureLabels() {
        Luca14PtLabel.appearance().setDefaultFont(Styling.font(font: FontFamily.Montserrat.medium.font(size: 14)))
        Luca14PtLabel.appearance().textColor = UIColor.white

        Luca14PtBlackLabel.appearance().setDefaultFont(Styling.font(font: FontFamily.Montserrat.medium.font(size: 14)))
        Luca14PtBlackLabel.appearance().textColor = UIColor.black

        Luca14PtBoldBlackLabel.appearance().setDefaultFont(Styling.font(font: FontFamily.Montserrat.bold.font(size: 14)))
        Luca14PtBoldBlackLabel.appearance().textColor = UIColor.black

        Luca14PtBoldLabel.appearance().setDefaultFont(Styling.font(font: FontFamily.Montserrat.bold.font(size: 14)))
        Luca14PtBoldLabel.appearance().textColor = UIColor.white

        Luca14PtBoldAlertLabel.appearance().setDefaultFont(Styling.font(font: FontFamily.Montserrat.bold.font(size: 14)))
        Luca14PtBoldAlertLabel.appearance().textColor = Asset.lucaError.color

        Luca20PtBoldLabel.appearance().setDefaultFont(Styling.font(font: FontFamily.Montserrat.bold.font(size: 20)))
        Luca20PtBoldLabel.appearance().textColor = UIColor.white

        Luca20PtMediumLabel.appearance().setDefaultFont(Styling.font(font: FontFamily.Montserrat.medium.font(size: 20)))
        Luca20PtMediumLabel.appearance().textColor = UIColor.black

        TANLabel.appearance().setDefaultFont(Styling.font(font: UIFont.monospacedDigitSystemFont(ofSize: 20, weight: .bold)))
        TANLabel.appearance().textColor = UIColor.white

        Luca16PtLabel.appearance().setDefaultFont(Styling.font(font: FontFamily.Montserrat.medium.font(size: 16)))
        Luca16PtLabel.appearance().textColor = UIColor.white

        Luca16PtBoldLabel.appearance().setDefaultFont(Styling.font(font: FontFamily.Montserrat.bold.font(size: 16)))
        Luca16PtBoldLabel.appearance().textColor = UIColor.white

        Luca16PtBoldBlackLabel.appearance().setDefaultFont(Styling.font(font: FontFamily.Montserrat.bold.font(size: 16)))
        Luca16PtBoldBlackLabel.appearance().textColor = UIColor.black

        Luca24PtBoldLabel.appearance().setDefaultFont(Styling.font(font: FontFamily.Montserrat.bold.font(size: 24)))
        Luca24PtBoldLabel.appearance().textColor = UIColor.white

        Luca60PtLabel.appearance().setDefaultFont(Styling.font(font: UIFont.monospacedDigitSystemFont(ofSize: 60, weight: .semibold)))
        Luca60PtLabel.appearance().textColor = Asset.lucaBlack.color

        Luca12PtLabel.appearance().setDefaultFont(Styling.font(font: FontFamily.Montserrat.medium.font(size: 12)))
        Luca12PtLabel.appearance().textColor = Asset.lucaBlack.color

        LucaAccountTitleLabel.appearance().setDefaultFont(Styling.font(font: FontFamily.Montserrat.medium.font(size: 16.0)))
        LucaAccountTitleLabel.appearance().textColor = .white

        // NewPrivateMeetingViewController
        PrivateMeetingTimerLabel.appearance(whenContainedInInstancesOf: [NewPrivateMeetingViewController.self]).font = Styling.font(font: FontFamily.Montserrat.bold.font(size: 16))

        PrivateMeetingTimerLabel.appearance(whenContainedInInstancesOf: [NewPrivateMeetingDetailsViewController.self]).font = Styling.font(font: FontFamily.Montserrat.regular.font(size: 16))
        // DocumentListViewController

        Luca16PtLabel.appearance(whenContainedInInstancesOf: [DocumentListViewController.self]).textColor = UIColor.white
    }

    private static func configureTextFields() {
        LucaDefaultTextField.appearance().backgroundColor = .clear
        LucaDefaultTextField.appearance().font = FontFamily.Montserrat.medium.font(size: 14)
        LucaDefaultTextField.appearance().textColor = UIColor.white
        LucaDefaultTextField.appearance().borderColor = Asset.luca747480.color
    }

    private static func configureButtons() {
        DarkStandardButton.appearance().titleLabelFont = Styling.font(font: FontFamily.Montserrat.bold.font(size: 14))
        DarkStandardButton.appearance().titleLabelColor = UIColor.white
        DarkStandardButton.appearance().backgroundColor = UIColor.clear
        DarkStandardButton.appearance().borderColor = UIColor.white

        BlackTransparentButton.appearance().titleLabelFont = Styling.font(font: FontFamily.Montserrat.bold.font(size: 14))
        BlackTransparentButton.appearance().titleLabelColor = UIColor.black
        BlackTransparentButton.appearance().backgroundColor = UIColor.clear
        BlackTransparentButton.appearance().borderColor = UIColor.black

        LightStandardButton.appearance().titleLabelFont = Styling.font(font: FontFamily.Montserrat.bold.font(size: 14))
        LightStandardButton.appearance().titleLabelColor = UIColor.black
        LightStandardButton.appearance().backgroundColor = Asset.lucaBlue.color

        WhiteButton.appearance().titleLabelColor = UIColor.black
        WhiteButton.appearance().backgroundColor = .white

        BlueButton.appearance().titleLabelFont = Styling.font(font: FontFamily.Montserrat.bold.font(size: 14))
        BlueButton.appearance().titleLabelColor = Asset.lucaBlue.color
    }

    private static func configureSwitches() {
        let switchAppearence = UISwitch.appearance()
        switchAppearence.tintColor = Asset.lucaBlue.color
        switchAppearence.onTintColor = Asset.lucaBlue.color
    }

    private static func configureDefaultNavigationBar() {
        let attrsTitle = [
            NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 20),
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]

        let attrsButton = [
            NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 14),
            NSAttributedString.Key.foregroundColor: Asset.lucaBlue.color
        ]

        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor.black
        appearance.titleTextAttributes = attrsTitle
        appearance.shadowColor = .white
        appearance.buttonAppearance.normal.titleTextAttributes = attrsButton
        appearance.buttonAppearance.highlighted.titleTextAttributes = attrsButton

        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        UINavigationBar.appearance().tintColor = Asset.lucaBlue.color
    }

    private static func configureEmbeddedNavigationBar() {
        let attrsTitleEmbedded = [
            NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 20),
            NSAttributedString.Key.foregroundColor: UIColor.black]

        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = Asset.lucaCheckinGradientBottom.color
        appearance.titleTextAttributes = attrsTitleEmbedded
        appearance.shadowImage = UIImage()
        appearance.shadowColor = .white
    }

    private static func font(font: UIFont, textStyle: UIFont.TextStyle = UIFont.TextStyle.body, maximumFontSize: CGFloat = 60) -> UIFont {
        let fontMetrics = UIFontMetrics(forTextStyle: textStyle)
        return fontMetrics.scaledFont(for: font, maximumPointSize: maximumFontSize)
    }

    private static func styleEmbeddedChildrenViewControllers() {
        UITableView.appearance(whenContainedInInstancesOf: [EmbeddedChildrenListViewController.self]).separatorColor = .yellow
    }
}

protocol LucaModalAppearance {
    func applyColors()
}

extension LucaModalAppearance where Self: UIViewController {
    func applyColors() {
        self.view.backgroundColor = Asset.luca1d1d1d.color

        let attrsTitle = [
            NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 20),
            NSAttributedString.Key.foregroundColor: UIColor.white]

        let attrsButton = [
            NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 14),
            NSAttributedString.Key.foregroundColor: Asset.lucaBlue.color
        ]

        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = Asset.luca1d1d1d.color
        appearance.titleTextAttributes = attrsTitle
        appearance.shadowImage = UIImage()
        appearance.shadowColor = .clear
        appearance.buttonAppearance.normal.titleTextAttributes = attrsButton
        appearance.buttonAppearance.highlighted.titleTextAttributes = attrsButton

        self.navigationController?.navigationBar.standardAppearance = appearance
        self.navigationController?.navigationBar.scrollEdgeAppearance = appearance
    }
}

public enum NavigationBarAppearanceTheme {
    case light
    case dark
    case white
}

extension UIViewController {
    func configureNavigationBar(theme: NavigationBarAppearanceTheme) {
        let appearance = navigationBarAppearence(theme: theme)
        self.navigationController?.navigationBar.standardAppearance = appearance
        self.navigationController?.navigationBar.scrollEdgeAppearance = appearance
        self.navigationController?.navigationBar.tintColor = theme == .dark ? Asset.lucaBlue.color : UIColor.black

        Luca12PtLabel.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).textColor = theme == .dark ? .white : .black
        Luca20PtBoldLabel.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).textColor = theme == .dark ? .white : .black
        Luca16PtBoldLabel.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).textColor = theme == .dark ? .white : .black
    }

    private func navigationBarAppearence(theme: NavigationBarAppearanceTheme) -> UINavigationBarAppearance {
        let attrsTitle = [
            NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 20),
            NSAttributedString.Key.foregroundColor: theme == .dark ? UIColor.white : UIColor.black]
        let attrsButton = [
            NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 14),
            NSAttributedString.Key.foregroundColor: theme == .dark ? Asset.lucaBlue.color : UIColor.black
        ]
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        switch theme {
        case .light:
            appearance.backgroundColor = Asset.lucaCheckinGradientBottom.color
        case .dark:
            appearance.backgroundColor = UIColor.black
        case .white:
            appearance.backgroundColor = UIColor.white
        }
        appearance.titleTextAttributes = attrsTitle
        appearance.shadowImage = nil
        appearance.shadowColor = .black
        appearance.buttonAppearance.normal.titleTextAttributes = attrsButton
        appearance.buttonAppearance.highlighted.titleTextAttributes = attrsButton
        return appearance
    }
}
