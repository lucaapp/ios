import Foundation
import MessageUI
import RxSwift
import RxCocoa

enum RxMFMailComposeError: LocalizedTitledError {
    case mailNotConfigured
    case sendFailed
    case saveFailed
    case unknown
}

extension RxMFMailComposeError {
    var localizedTitle: String {
        L10n.IOSApp.ReportContent.Email.Error.title
    }

    var errorDescription: String? {
        switch self {
        case .mailNotConfigured:
            return L10n.IOSApp.ReportContent.Email.Error.MailNotConfigured.description
        case .sendFailed:
            return L10n.IOSApp.ReportContent.Email.Error.SendFailed.description
        case .saveFailed:
            return L10n.IOSApp.ReportContent.Email.Error.SaveFailed.description
        case .unknown:
            return L10n.IOSApp.ReportContent.Email.Error.Unknown.description
        }
    }
}

extension MFMailComposeViewController: HasDelegate {
    public typealias Delegate = MFMailComposeViewControllerDelegate
}

class RxMFMailComposeViewControllerDelegateProxy: DelegateProxy<MFMailComposeViewController, MFMailComposeViewControllerDelegate>,
                                                  DelegateProxyType,
                                                  MFMailComposeViewControllerDelegate {

    public weak private(set) var mailComposerView: MFMailComposeViewController?

    static func currentDelegate(for object: MFMailComposeViewController) -> MFMailComposeViewControllerDelegate? {
        return object.mailComposeDelegate
    }

    static func setCurrentDelegate(_ delegate: MFMailComposeViewControllerDelegate?, to object: MFMailComposeViewController) {
        object.mailComposeDelegate = delegate
    }

    public init(mailComposerView: ParentObject) {
        self.mailComposerView = mailComposerView
        super.init(parentObject: mailComposerView, delegateProxy: RxMFMailComposeViewControllerDelegateProxy.self)
    }

    static func registerKnownImplementations() {
        self.register { RxMFMailComposeViewControllerDelegateProxy(mailComposerView: $0) }
    }
}

extension Reactive where Base: MFMailComposeViewController {

    public var mailComposeDelegate: DelegateProxy<MFMailComposeViewController, MFMailComposeViewControllerDelegate> {
        return RxMFMailComposeViewControllerDelegateProxy.proxy(for: base)
    }

    var sendMail: Completable {
        if !MFMailComposeViewController.canSendMail() {
            return .error(RxMFMailComposeError.mailNotConfigured)
        }
        return mailComposeDelegate
            .methodInvoked(#selector(MFMailComposeViewControllerDelegate.mailComposeController(_:didFinishWith:error:)))
            .take(1)
            .flatMap({ parameters -> Completable in
                guard let result = parameters[1] as? Int else {
                    throw RxMFMailComposeError.unknown
                }
                let error = parameters[2] as? MFMailComposeError
                switch result {
                case 0, 1, 2:
                    return .empty()
                case 3:
                    switch error!.code {
                    case .saveFailed:
                        throw RxMFMailComposeError.saveFailed
                    case .sendFailed:
                        throw RxMFMailComposeError.sendFailed
                    @unknown default:
                        throw RxMFMailComposeError.unknown
                    }
                default:
                    throw RxMFMailComposeError.unknown
                }

            })
            .do(onDispose: { [weak base] in
                base?.dismiss(animated: true, completion: nil)
            })
            .asCompletable()
        }
}
