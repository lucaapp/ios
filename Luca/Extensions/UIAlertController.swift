import UIKit
import RxSwift

extension UIAlertController {

    static func presentErrorAlert(presenter: UIViewController, for error: Error, onOk: (() -> Void)? = nil) {
        switch error {
        case let localizedError as LocalizedTitledError:
            let alert = UIAlertController.infoAlert(title: localizedError.localizedTitle, message: localizedError.localizedDescription, onOk: onOk)
            presenter.present(alert, animated: true, completion: nil)
        case _ as SilentError:
            onOk?()
        default:
            let alert = UIAlertController.infoAlert(title: L10n.IOSApp.Navigation.Basic.error, message: error.localizedDescription, onOk: onOk)
            presenter.present(alert, animated: true, completion: nil)
        }
    }

    static func infoAlert(title: String, message: String, onOk: (() -> Void)? = nil) -> UIAlertController {
        let alert = CustomAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: L10n.IOSApp.Navigation.Basic.ok, style: .default, handler: { _ in onOk?() }))
        return alert
    }
    /// Alert controller without actions. It has to be dismissed in code. Used for blocking UI for some critical reasons
    static func infoBox(title: String, message: String) -> UIAlertController {
        let alert = CustomAlertController(title: title, message: message, preferredStyle: .alert)
        return alert
    }

    /// It presents an alert on subscribe and completes on "ok" button. Dismissed when disposed
    static func infoAlertRx(viewController: UIViewController, title: String, message: String) -> Observable<UIAlertController> {
        return Observable<UIAlertController>.create { observer in
            let alert = UIAlertController.infoAlert(title: title, message: message) {
                observer.onCompleted()
                // swiftlint:disable:next force_cast
            } as! CustomAlertController

            // This is in case someone dismisses the view controller outside of the stream. The stream should complete nevertheless.
            alert.onDismissBegin = {
                observer.onCompleted()
            }

            viewController.present(alert, animated: true, completion: nil)

            observer.onNext(alert)
            return Disposables.create {
                // Disable safety notification, this stream is being disposed.
                alert.onDismissBegin = nil
                alert.dismiss(animated: true, completion: nil)
            }
        }
        .subscribe(on: MainScheduler.instance)
    }

    /// It presents an alert on subscribe and emits the instance.
    /// - complete: when alert is dismissed
    /// - error: None
    /// - onDispose: dismisses the alert
    static func infoBoxRx(viewController: UIViewController, title: String, message: String) -> Observable<UIAlertController> {
        return Observable.create { observer in
            // swiftlint:disable:next force_cast
            let alert = CustomAlertController.infoBox(title: title, message: message) as! CustomAlertController

            // This is in case someone dismisses the view controller outside of the stream. The stream should complete nevertheless.
            alert.onDismissBegin = {
                observer.onCompleted()
            }

            viewController.present(alert, animated: true, completion: nil)
            observer.onNext(alert)
            return Disposables.create {
                // Disable safety notification, this stream is being disposed.
                alert.onDismissBegin = nil
                alert.dismiss(animated: true, completion: nil)
            }
        }
        .subscribe(on: MainScheduler.instance)
    }

    static func yesOrNo(title: String, message: String, onYes: (() -> Void)? = nil, onNo: (() -> Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: L10n.IOSApp.Navigation.Basic.yes, style: .default, handler: { _ in onYes?() }))
        alert.addAction(UIAlertAction(title: L10n.IOSApp.Navigation.Basic.no, style: .cancel, handler: { _ in onNo?() }))
        return alert
    }

    static func okAndCancelAlertRx(viewController: UIViewController,
                                   title: String,
                                   message: String,
                                   okTitle: String? = nil) -> Single<Bool> {
        return Single.create { observer in
            let alert = UIAlertController.okAndCancelAlert(title: title, message: message, okTitle: okTitle) { success in
                observer(.success(success))
            }
            viewController.present(alert, animated: true, completion: nil)
            return Disposables.create {
                alert.dismiss(animated: true, completion: nil)
            }
        }
        .subscribe(on: MainScheduler.instance)
    }

    static func okAndCancelAlert(title: String,
                                 message: String,
                                 okTitle: String? = nil,
                                 completed: @escaping(Bool) -> Void) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: okTitle ?? L10n.IOSApp.Navigation.Basic.ok, style: .default, handler: { _ in completed(true) }))
        alert.addAction(UIAlertAction(title: L10n.IOSApp.Navigation.Basic.cancel, style: .cancel, handler: { _ in completed(false) }))
        return alert
    }

    func yesActionAndNoAlert(action: @escaping() -> Void, viewController: UIViewController) {
        let action = UIAlertAction(title: L10n.IOSApp.Navigation.Basic.yes, style: .default) { _ in
            action()
        }

        let cancelAction = UIAlertAction(title: L10n.IOSApp.Navigation.Basic.no, style: .cancel)

        self.addAction(action)
        self.addAction(cancelAction)

        viewController.present(self, animated: true, completion: nil)
    }

    static func actionAndCancelAlert(title: String,
                                     message: String,
                                     actionTitle: String,
                                     actionStyle: UIAlertAction.Style = .default,
                                     action: (() -> Void)? = nil,
                                     cancelAction: (() -> Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: actionStyle) { _ in
            action?()
        })
        alert.addAction(UIAlertAction(title: L10n.IOSApp.Navigation.Basic.cancel, style: .cancel) { _ in
            cancelAction?()
        })
        return alert
    }

    func actionAndCancelAlert(actionText: String, actionStyle: UIAlertAction.Style = .default, action: @escaping() -> Void, viewController: UIViewController) {
        let action = UIAlertAction(title: actionText, style: actionStyle) { _ in
            action()
        }

        let cancelAction = UIAlertAction(title: L10n.IOSApp.Navigation.Basic.cancel, style: .cancel)

        self.addAction(action)
        self.addAction(cancelAction)

        viewController.present(self, animated: true, completion: nil)
    }

    func goToApplicationSettings(viewController: UIViewController, pop: Bool = false) {
        let okAction = UIAlertAction(title: L10n.IOSApp.Navigation.Basic.ok, style: .default) { _ in
            UIApplication.shared.openApplicationSettings()
            self.dismiss(animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: L10n.IOSApp.Navigation.Basic.cancel, style: .cancel) { _ in
            if pop {
                viewController.navigationController?.popViewController(animated: true)
            }
            self.dismiss(animated: true, completion: nil)
        }

        addAction(okAction)
        addAction(cancelAction)

        viewController.present(self, animated: true, completion: nil)
    }

    static func goToApplicationSettingsRx(
        viewController: UIViewController,
        title: String,
        message: String,
        pop: Bool = false,
        preferredStyle: UIAlertController.Style = .alert
    ) -> Observable<UIAlertController> {
        return Observable.create { observer in
            // swiftlint:disable:next force_cast
            let alert = CustomAlertController.infoBox(title: title, message: message) as! CustomAlertController

            // This is in case someone dismisses the view controller outside of the stream. The stream should complete nevertheless.
            alert.onDismissBegin = {
                observer.onCompleted()
            }

            let okAction = UIAlertAction(title: L10n.IOSApp.Navigation.Basic.ok, style: .default) { _ in
                UIApplication.shared.openApplicationSettings()
                observer.onCompleted()
            }
            let cancelAction = UIAlertAction(title: L10n.IOSApp.Navigation.Basic.cancel, style: .cancel) { _ in
                if pop {
                    viewController.navigationController?.popViewController(animated: true)
                }
                observer.onCompleted()
            }

            alert.addAction(okAction)
            alert.addAction(cancelAction)

            viewController.present(alert, animated: true, completion: nil)
            observer.onNext(alert)
            return Disposables.create {
                // Disable safety notification, this stream is being disposed.
                alert.onDismissBegin = nil
                alert.dismiss(animated: true, completion: nil)
            }
        }
        .subscribe(on: MainScheduler.instance)
    }

    func menuActionSheet(viewController: UIViewController, additionalActions: [UIAlertAction] = []) {
        self.addAction(UIAlertAction(title: L10n.IOSApp.Navigation.Basic.cancel, style: .cancel, handler: nil))
        for action in additionalActions { self.addAction(action) }
        viewController.present(self, animated: true)
    }

    func termsAndConditionsActionSheet(viewController: UIViewController, additionalActions: [UIAlertAction] = []) {
        self.addAction(UIAlertAction(title: L10n.IOSApp.Terms.Acceptance.termsAndConditionsChanges, style: .default) { _ in
            guard let url = URL(string: L10n.IOSApp.Terms.Acceptance.linkChanges) else {
                return
            }
            UIApplication.shared.open(url, options: [:])
        })

        self.addAction(UIAlertAction(title: L10n.IOSApp.Navigation.Basic.cancel, style: .cancel, handler: nil))
        for action in additionalActions { self.addAction(action) }
        viewController.present(self, animated: true)
    }
}

private class CustomAlertController: UIAlertController {
    public var onDismissBegin: (() -> Void)?
    public var onDismissEnd: (() -> Void)?

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        self.onDismissBegin?()
        super.dismiss(animated: true) {
            self.onDismissEnd?()
            completion?()
        }
    }
}
