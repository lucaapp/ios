import UIKit

protocol ViewWidthDefining {
    func viewFitsInOneLine() -> Bool
}

extension UILabel: ViewWidthDefining {
    func viewFitsInOneLine() -> Bool {
        layoutIfNeeded()

        guard let labelText = text as NSString? else {
            return false
        }

        let size = labelText.size(withAttributes: [NSAttributedString.Key.font: font ?? 0])
        return size.width > bounds.width
    }
}

extension UIStackView: ViewWidthDefining {
    func viewFitsInOneLine() -> Bool {
        layoutIfNeeded()

        let size = systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        return size.width > bounds.width
    }
}
