import UIKit

protocol Localizable {

    /// Looks up a key with the content of `self` and returns, If not found, an empty string would be returned.
    var localized: String { get }
}
extension String: Localizable {

    private static let localizableFiles: [String] = ["iOSApp", "UserApp"]

    var localized: String {
        for localizableFile in String.localizableFiles {
            let value = NSLocalizedString(self, tableName: localizableFile, bundle: .main, comment: "")
            if value != self {
                return value
            }
        }
        return ""
    }
}

protocol UILocalizable {
    var localizationKey: String? { get set }
}

extension UILabel: UILocalizable {
    @IBInspectable var localizationKey: String? {
        get { return nil }
        set(key) {
            text = key?.localized
        }
    }
}

extension UIButton: UILocalizable {
    @IBInspectable var localizationKey: String? {
        get { return nil }
        set(key) {
            setTitle(key?.localized, for: .normal)
        }
   }
}
