import Foundation

extension Array where Element: Hashable {

    func difference(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }

}
extension Array {
    func difference(from other: [Element], equalityChecker: (Element, Element) -> Bool) -> [Element] {
        let thisExclusiveOther = self.filter { firstElement in
            !other.contains(where: { secondElement in
                equalityChecker(firstElement, secondElement)
            })
        }
        let otherExclusiveThis = other.filter { firstElement in
            !self.contains(where: { secondElement in
                equalityChecker(firstElement, secondElement)
            })
        }
        return thisExclusiveOther + otherExclusiveThis
    }

    func subtract(_ other: [Element], equalityChecker: (Element, Element) -> Bool) -> [Element] {
        filter { firstElement in
            !other.contains(where: { secondElement in
                equalityChecker(firstElement, secondElement)
            })
        }
    }

    /// Returns array with unique elements by given property
    func unique<T: Hashable>(by: ((Element) -> (T))) -> [Element] {
        var set = Set<T>()
        var arrayOrdered = [Element]()
        for value in self {
            if !set.contains(by(value)) {
                set.insert(by(value))
                arrayOrdered.append(value)
            }
        }
        return arrayOrdered
    }

    /// Move element from one index to another within Array
    mutating func move(at index: Index, to newIndex: Index) {
        insert(remove(at: index), at: newIndex)
    }
}

extension Array where Element: Equatable {
    /// Move element to a new index within Array
    mutating func move(_ item: Element, to newIndex: Index) {
        if let index = firstIndex(of: item) {
            move(at: index, to: newIndex)
        }
    }
}

public extension Array where Element: OptionalType {

    func unwrapOptional() -> [Element.Wrapped] {
        filter { (value) in
            if value.value == nil {
                return false
            }
            return true
        }
        .map { $0.value! }
    }

}

extension Array where Element == Double {

    func sum() -> Element {
        return self.reduce(0, +)
    }

    func avg() -> Element {
        return self.sum() / Element(self.count)
    }

    func std() -> Element {
        let mean = self.avg()
        let v = self.reduce(0, { $0 + ($1-mean)*($1-mean) })
        return sqrt(v / (Element(self.count) - 1))
    }

}
