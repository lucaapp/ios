extension BidirectionalCollection where Element: StringProtocol {
		var asSentence: String {
				count <= 2 ?
					joined(separator: L10n.IOSApp.List.and.string) :
					dropLast().joined(separator: ", ") + L10n.IOSApp.List.lastElement.string + last!
		}
}
