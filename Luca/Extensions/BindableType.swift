import UIKit
import RxSwift

protocol BindableType {
	associatedtype ViewModelType
	var viewModel: ViewModelType! { get set }
	func bindViewModel()
}

extension BindableType where Self: UIViewController {
	mutating func bindViewModel(to model: Self.ViewModelType) {
		viewModel = model
		loadViewIfNeeded()
		bindViewModel()
	}
}

protocol ViewControllerOwnedVM {
	var owner: UIViewController? {get}
	func rxErrorAlert(for error: Error) -> Completable
}

extension ViewControllerOwnedVM {
	func rxErrorAlert(for error: Error) -> Completable {
		guard let owner = owner else {return Completable.empty()}
		return UIAlertController.infoAlertRx(
					viewController: owner,
					title: L10n.IOSApp.MainTabBarViewController.ScannerFailure.title,
					message: error.localizedDescription)
					.ignoreElementsAsCompletable()
					.andThen(Completable.error(error)) // Push the error through to retry the stream
	}

}

protocol HandlesLucaErrors {
	func processErrorMessages(error: Error, completion:(() -> Void)?)
}

private func _processErrorMessages(error: Error, viewController: UIViewController, completion:(() -> Void)? = nil) {
    if let printableError = error as? PrintableError {
        let alert = UIAlertController.infoAlert(
            title: printableError.title,
            message: printableError.message, onOk: completion)
        viewController.present(alert, animated: true, completion: nil)
    } else if let blockingError = error as? BlockingError {
        _ = UIAlertController.infoBoxRx(
            viewController: viewController,
            title: blockingError.localizedTitle,
            message: blockingError.localizedDescription
        )
        .ignoreElementsAsCompletable()
        .subscribe(on: MainScheduler.instance)
        .subscribe(onDisposed: {
            completion?()
        })
    } else if let localizedError = error as? LocalizedTitledError {
        let alert = UIAlertController.infoAlert(
            title: localizedError.localizedTitle,
            message: localizedError.localizedDescription, onOk: completion)
        viewController.present(alert, animated: true, completion: nil)
    } else if error as? SilentError != nil {
        // Silent errors will be ignored as they are only used when user actively cancels something
        completion?()
        return
    } else {
        let alert = UIAlertController.infoAlert(
            title: L10n.IOSApp.Navigation.Basic.error,
            message: L10n.IOSApp.General.Failure.Unknown.message(error.localizedDescription), onOk: completion)
        viewController.present(alert, animated: true, completion: nil)
    }
}

private func _processErrorMessagesRx(error: Error, on vc: UIViewController) -> Completable {
    Completable.create { observer in
        _processErrorMessages(error: error, viewController: vc) {
            observer(.completed)
        }
        return Disposables.create()
    }
    .subscribe(on: MainScheduler.asyncInstance)
}

extension HandlesLucaErrors where Self: UIViewController {
	func processErrorMessages(error: Error, completion:(() -> Void)? = nil) {
        _processErrorMessages(error: error, viewController: self, completion: completion)
	}

    func processErrorMessagesRx(error: Error) -> Completable {
        _processErrorMessagesRx(error: error, on: self)
    }

    func handleAuthorizationError(title: String, message: String) -> Completable {
        UIAlertController.okAndCancelAlertRx(
            viewController: self,
            title: title,
            message: message
        )
        .do(onSuccess: {
            if $0 {
                UIApplication.shared.openApplicationSettings()
            }
        })
        .do(onDispose: {
            self.dismiss(animated: true, completion: nil)
        })
        .asCompletable()
    }

}

extension Completable {
    func handleErrors(_ handler: UIViewController) -> Completable {
        self.catch {
            _processErrorMessagesRx(error: $0, on: handler).andThen(.error($0))
        }
    }
}

extension Observable {
    func handleErrors(_ handler: UIViewController) -> Observable<Element> {
        self.catch {
            _processErrorMessagesRx(error: $0, on: handler).andThen(.error($0))
        }
    }
}

extension PrimitiveSequence where Trait == SingleTrait {
    func handleErrors(_ handler: UIViewController) -> Self {
        self.catch {
            _processErrorMessagesRx(error: $0, on: handler).andThen(.error($0))
        }
    }
}

extension PrimitiveSequence where Trait == MaybeTrait {
    func handleErrors(_ handler: UIViewController) -> Self {
        self.catch {
            _processErrorMessagesRx(error: $0, on: handler).andThen(.error($0))
        }
    }
}
