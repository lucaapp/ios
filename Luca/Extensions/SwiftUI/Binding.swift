import SwiftUI

extension Binding where Value: OptionalType {
    var isPresent: Binding<Bool> {
        Binding<Bool>(
            get: {
                self.wrappedValue.value != nil
            },
            set: { _ in
            }
        )
    }
 }

extension Binding where Value == Bool {
    var invert: Binding<Bool> {
        Binding<Bool>(
            get: { !self.wrappedValue },
            set: { self.wrappedValue = !$0 }
        )
    }
}
