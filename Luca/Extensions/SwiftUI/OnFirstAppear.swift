import SwiftUI

extension View {

    func onFirstAppear(perform: @escaping () -> Void) -> some View {
        ModifiedContent(content: self, modifier: OnFirstAppear(callback: perform))
    }
}

struct OnFirstAppear: ViewModifier {
    @State private var alreadyShown: Bool = false
    let callback: () -> Void

    @ViewBuilder
    func body(content: Content) -> some View {
        content
            .overlay(
                EmptyView()
                    .onAppear {
                        if !alreadyShown {
                            alreadyShown = true
                            callback()
                        }
                    }
            )
    }
}
