import SwiftUI

enum AlertType {
    case dismiss(button: Alert.Button)
    case yesNo(primaryButton: Alert.Button, secondaryButton: Alert.Button)
}

struct AlertContent: Identifiable, Equatable {
    static func == (lhs: AlertContent, rhs: AlertContent) -> Bool {
        lhs.id == rhs.id
    }

    var id = "\(Int.random(in: Int.min..<Int.max))"
    var title: String
    var message: String
    var type: AlertType
}

struct AlertHandler: ViewModifier {
    @Binding var alertContent: AlertContent?

    private var shouldPresent: Binding<Bool> {
        Binding(
            get: { alertContent != nil },
            set: {
                if $0 == false {
                    alertContent = nil
                }
            }
        )
    }

    @ViewBuilder
    func body(content: Content) -> some View {
        content
            .alert(isPresented: shouldPresent, content: {
                switch alertContent!.type {
                case .dismiss(let button):
                    return Alert(
                        title: Text(alertContent!.title),
                        message: Text(alertContent!.message),
                        dismissButton: button
                    )
                case .yesNo(let primaryButton, let secondaryButton):
                    return Alert(
                        title: Text(alertContent!.title),
                        message: Text(alertContent!.message),
                        primaryButton: primaryButton,
                        secondaryButton: secondaryButton
                    )
                }
            })
    }
}

extension View {
    func handleAlerts(_ content: Binding<AlertContent?>) -> some View {
        self.overlay(ModifiedContent(content: EmptyView(), modifier: AlertHandler(alertContent: content)))
    }
}

extension Error {
    var alertContent: AlertContent? {
        if self is SilentError {
            return nil
        }
        if let printable = self as? PrintableError {
            return AlertContent(
                title: printable.title,
                message: printable.message,
                type: .dismiss(button: .default(Text(L10n.IOSApp.Navigation.Basic.ok)))
            )
        }
        if let localized = self as? LocalizedTitledError {
            return AlertContent(
                title: localized.localizedTitle,
                message: localized.localizedDescription,
                type: .dismiss(button: .default(Text(L10n.IOSApp.Navigation.Basic.ok)))
            )
        }
        return AlertContent(
            title: L10n.IOSApp.Navigation.Basic.error,
            message: "\(self)",
            type: .dismiss(button: .default(Text(L10n.IOSApp.Navigation.Basic.ok)))
        )
    }
}
