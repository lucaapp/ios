import SwiftUI

extension View {
    /// Shows a loading indicator when `isLoading` turns to `true`. It will wait `threshold` seconds to prevent quick actions that may result in quickly changing loading indicators
    func loadingIndicator(isLoading: Binding<Bool>, threshold: Double = 0.2) -> some View {
        ModifiedContent(content: self, modifier: LoadingIndicator(isLoading: isLoading, threshold: threshold))
    }
}

struct LoadingIndicator: ViewModifier {
    @Binding var isLoading: Bool
    var threshold: Double

    @State private var isVisible: Bool = false
    @State private var opacity: Double = 0.0

    @ViewBuilder
    func body(content: Content) -> some View {
        content
            .overlay(
                Rectangle()
                    .frame(minWidth: 300)
                    .foregroundColor(Color.black)
                    .opacity(opacity)
                    .ignoresSafeArea()
                    .overlay(
                        VStack(spacing: 16.0) {
                            ProgressView()
                            Text("Loading")
                                .font(SwiftUI.Font(FontFamily.Montserrat.bold.font(size: 16.0)))
                                .foregroundColor(Color.white)
                        }
                        .padding(16.0)
                        .background(Color.black)
                        .opacity(0.7)
                        .cornerRadius(16.0)
                    )
                    .allowsHitTesting(true)
                    .animation(.easeInOut(duration: 0.3), value: opacity)
                    .onChange(of: isVisible, perform: {
                        opacity = $0 ? 0.3 : 0.0
                    })
                    .onChange(of: isLoading, perform: { newIsLoading in
                        if newIsLoading {
                            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Int(threshold * 1000.0))) {
                                // If this value is still true (operation is still running) after threshold, make it visible
                                if isLoading {
                                    isVisible = true
                                }
                            }
                        } else {
                            isVisible = false
                        }
                    })
                    .show(isVisible: $isVisible)
            )
            .progressViewStyle(CircularProgressViewStyle(tint: .white))
    }
}

struct LoadingIndicator_Previews: PreviewProvider {
    static var previews: some View {
        UIElementPreview(
            VStack {
                Text("hello")
                    .fixedSize(horizontal: true, vertical: false)
            }
            .frame(width: 300, height: 300)
            .background(Color.red)
            .loadingIndicator(isLoading: .constant(true))
        )
    }
}
