import SwiftUI
import RxSwift
import Combine

extension Completable {
    func copyError(to: AlertsSubject) -> Completable {
        self.asObservable()
            .copyError(to: to)
            .ignoreElementsAsCompletable()
    }

    func copyRunningState(to processCount: Binding<Int>) -> Completable {
        self.asObservable()
            .copyRunningState(to: processCount)
            .ignoreElementsAsCompletable()
    }
}

extension PrimitiveSequence where Trait == MaybeTrait {
    func copyError(to: AlertsSubject) -> Self {
        self.asObservable()
            .copyError(to: to)
            .asMaybe()
    }

    func copyRunningState(to processCount: Binding<Int>) -> Self {
        self.asObservable()
            .copyRunningState(to: processCount)
            .asMaybe()
    }
}

extension PrimitiveSequence where Trait == SingleTrait {
    func copyError(to: AlertsSubject) -> Single<Element> {
        self.asObservable()
            .copyError(to: to)
            .asSingle()
    }

    func copyRunningState(to processCount: Binding<Int>) -> Single<Element> {
        self.asObservable()
            .copyRunningState(to: processCount)
            .asSingle()
    }
}

private class AlertScheduler {
    enum State {
        case idle
        case waitForNextNil
        case presenting(alertContent: AlertContent)
    }
    let subject: AlertsSubject
    let onAlertCompleted = PublishSubject<AlertContent>()

    private var cancellable: AnyCancellable?
    private var queue = [AlertContent]()
    private var currentAlert: AlertContent?
    private var state = State.idle {
        willSet {
            switch state {
            case .presenting(let alertContent): onAlertCompleted.onNext(alertContent) // Call the callback when the state is about to switch from `presenting` to something else
            default: break
            }
        }
    }

    init(subject: AlertsSubject) {
        self.subject = subject
        cancellable = subject
            .receive(on: DispatchQueue.main)
            .dropFirst()
            .sink { [weak self] newAlert in
                guard let self = self else { return }
                switch self.state {
                case .idle: self.executeNextInQueue()
                case .waitForNextNil:
                    if newAlert == nil {
                        self.state = .idle
                    }
                case .presenting(let alert):
                    if newAlert == nil && !self.executeNextInQueue() {
                        self.state = .idle
                    }
                }
            }
    }

    func schedule(_ alertContent: AlertContent) {
        queue.append(alertContent)
        executeNextInQueue()
    }

    /// Executes new item from the queue and returns `true` if there were some items, `false` if empty
    @discardableResult
    private func executeNextInQueue() -> Bool {
        guard let first = queue.first else { return false }
        switch state {
        case .idle:
            state = .presenting(alertContent: first)
            queue.removeFirst()
            subject.value = first
        case .presenting:
            state = .waitForNextNil
            subject.value = nil
        case .waitForNextNil: break
        }
        return true
    }
}

private var schedulers = [AlertScheduler]()

extension Observable {

    /// Set the error to given `CurrentValueSubject` and waits until it changes (other error comes in or it's niled out)
    func copyError(to: AlertsSubject) -> Observable<Element> {
        self.catch { error in
            guard let content = error.alertContent else {
                throw RxError.unknown
            }
            return Single<AlertScheduler>.from {
                if let scheduler = schedulers.first(where: { $0.subject === to }) {
                    return scheduler
                } else {
                    let scheduler = AlertScheduler(subject: to)
                    schedulers.append(scheduler)
                    return scheduler
                }
            }
            .subscribe(on: MainScheduler.instance)
            .asObservable()
            .observe(on: MainScheduler.instance)
            .flatMap { scheduler -> Observable<Element> in
                scheduler
                    .onAlertCompleted
                    .subscribe(on: MainScheduler.instance)
                    .do(onSubscribe: { scheduler.schedule(content) })
                    .filter { $0 == content }
                    .flatMap { _ -> Observable<Element> in
                        throw error
                    }
            }
        }
    }

    func copyRunningState(to processCount: Binding<Int>) -> Observable<Element> {
        self
            .do(onSubscribe: { DispatchQueue.main.async { processCount.wrappedValue += 1 } })
            .do(onCompleted: { DispatchQueue.main.async { processCount.wrappedValue -= 1 } })
            .do(onError: { _ in DispatchQueue.main.async { processCount.wrappedValue -= 1 } })
    }
}

extension Binding {

    /// Creates a binding from a property in a class.
    ///
    /// - parameter target: owner of the property
    /// - parameter keyPath: keyPath of the property
    /// - parameter defaultValue: value to be returned when `target` is not available as this binding creates a weak reference.
    static func from<T, V>(_ target: T, keyPath: WritableKeyPath<T, V>, defaultValue: V) -> Binding<V> where T: AnyObject {
        Binding<V>(
            get: { [weak target] in
                target?[keyPath: keyPath] ?? defaultValue
            },
            set: { [weak target] in
                var t = target
                t?[keyPath: keyPath] = $0
            }
        )
    }
}

extension CurrentValueSubject {
    func binding(defaultValue: Output) -> Binding<Output> {
        Binding<Output>(
            get: { [weak self] in
                if let self = self {
                    return self.value
                }
                return defaultValue
            },
            set: { [weak self] in
                self?.value = $0
            }
        )
    }
}
