import SwiftUI

struct Show: ViewModifier {
    @Binding var isVisible: Bool
    var remove: Bool

    @ViewBuilder
    func body(content: Content) -> some View {
        if isVisible {
            content
        } else {
            if remove {
                EmptyView()
            } else {
                content.hidden()
            }
        }
    }
}

extension View {
    func show(isVisible: Binding<Bool>, remove: Bool = false) -> some View {
        ModifiedContent(content: self, modifier: Show(isVisible: isVisible, remove: remove))
    }
    func show(isVisible: Bool, remove: Bool = false) -> some View {
        ModifiedContent(content: self, modifier: Show(isVisible: Binding.constant(isVisible), remove: remove))
    }

    @ViewBuilder
    func ifLet<V, Transform: View>(
        _ value: V?,
        transform: (Self, V) -> Transform
    ) -> some View {
        if let value = value {
            transform(self, value)
        } else {
            self
        }
    }

    @ViewBuilder
        func `if`<TrueContent: View, FalseContent: View>(
            _ condition: Bool,
            if ifTransform: (Self) -> TrueContent,
            else elseTransform: (Self) -> FalseContent
        ) -> some View {
            if condition {
                ifTransform(self)
            } else {
                elseTransform(self)
            }
        }

    @ViewBuilder
        func `if`<Transform: View>(
            _ condition: Bool,
            transform: (Self) -> Transform
        ) -> some View {
            if condition {
                transform(self)
            } else {
                self
            }
        }
}
