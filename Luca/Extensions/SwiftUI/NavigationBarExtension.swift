import SwiftUI

struct HiddenNavigationBar: ViewModifier {
    func body(content: Content) -> some View {
        content
        .navigationBarTitle("", displayMode: .inline)
        .navigationBarHidden(true)
    }
}

extension View {
    func hiddenNavigationBarStyle() -> some View {
        modifier( HiddenNavigationBar() )
    }
}

struct LucaNavBarCloseButton: ViewModifier {
    @Binding var showModal: Bool
    var buttonTitle: String = L10n.IOSApp.Navigation.Basic.close

    @ViewBuilder
    func body(content: Content) -> some View {
        content
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button {
                        self.showModal = false
                    } label: {
                        Text(buttonTitle).navigationItemFont()
                    }
                }
            }

    }
}

extension View {
    func navBarCloseButton(_ showModal: Binding<Bool>) -> some View {
        ModifiedContent(content: self, modifier: LucaNavBarCloseButton(showModal: showModal))
    }

    func navBarCancelButton(_ showModal: Binding<Bool>) -> some View {
        var cancelButton = LucaNavBarCloseButton(showModal: showModal)
        cancelButton.buttonTitle = L10n.IOSApp.Navigation.Basic.cancel.uppercased()
        return ModifiedContent(content: self, modifier: cancelButton)
    }
}
