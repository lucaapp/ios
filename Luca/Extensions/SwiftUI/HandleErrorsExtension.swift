import SwiftUI

struct HandleErrors: ViewModifier {
    let error: Binding<Error?>

    private var shouldPresent: Binding<Bool> {
        Binding(get: { error.wrappedValue != nil }, set: { _ in })
    }
    private var title: String {
        error.wrappedValue?.alertContent?.title ?? L10n.IOSApp.Navigation.Basic.error
    }
    private var message: String {
        error.wrappedValue?.alertContent?.message ?? ""
    }

    @ViewBuilder
    func body(content: Content) -> some View {
        content
            .alert(isPresented: shouldPresent) {
                Alert(
                    title: Text(title),
                    message: Text(message),
                    dismissButton: .default(Text(L10n.IOSApp.Navigation.Basic.ok), action: { error.wrappedValue = nil })
                )
            }
    }
}

extension View {
    func handleError(error: Binding<Error?>) -> some View {
        ModifiedContent(content: self, modifier: HandleErrors(error: error))
    }
}
