import Foundation

extension KeyedDecodingContainer {
    func decodeFromIntOrString(forKey key: KeyedDecodingContainer<K>.Key) throws -> String {
        if let string = try? decode(String.self, forKey: key) {
            return string
        } else if let number = try? decode(Int.self, forKey: key) {
            return "\(number)"
        }
        throw DecodingError.keyNotFound(
            key,
            DecodingError.Context(
                codingPath: [key],
                debugDescription: "Given key is neither a string nor an int or doesn't exist"
            )
        )
    }
    func decodeFromIntOrStringIfPresent(forKey key: KeyedDecodingContainer<K>.Key) throws -> String? {
        if let string = try? decode(String.self, forKey: key) {
            return string
        } else if let number = try? decode(Int.self, forKey: key) {
            return "\(number)"
        }
        return nil
    }
}
