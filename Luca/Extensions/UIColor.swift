import UIKit
import CoreGraphics

extension UIColor {

    func as1pxImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let ctx = UIGraphicsGetCurrentContext() {
            self.setFill()
            ctx.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        }
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image ?? UIImage()
    }

}
