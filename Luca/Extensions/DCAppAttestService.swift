import RxSwift
import DeviceCheck
import CryptoKit

extension DCAppAttestService {

    /// Generate public/private key. These are stored in secure enclave. Should be generated on every new device migration or app installation
    /// - Returns: keyIdentifier required to find keys within secure enclave
    func generateAttestKeyIdentifier() -> Single<String> {
        return Single.create { observer -> Disposable in

            DCAppAttestService.shared.generateKey { (keyIdentifier, error) in
                if let error = error {
                    observer(.failure(error))
                    return
                }
                guard let keyIdentifier = keyIdentifier else {
                    observer(.failure(DeviceCheckError.noAttestKey))
                    return
                }
                observer(.success(keyIdentifier))
            }
            return Disposables.create()
        }
    }

    /// Get attestation to receive public key from apple within `attestation` data. Should happen once to transfer public key to server.
    /// - Parameters:
    ///   - keyIdentifier: identifier generated for each new installation. Not the actual key
    ///   - challenge: One-time server use token to avoid replay attacks
    /// - Returns: Attestation data containing public key and verification data
    func getAttestation(keyIdentifier: String, nonce: String) -> Single<RegisterAttestationRequest> {

        guard let challenge = nonce.data(using: .utf8) else {
            return .error(DeviceCheckError.missingData)
        }
        let hash = Data(SHA256.hash(data: challenge))

        return Single.create { observer -> Disposable in

            DCAppAttestService.shared.attestKey(keyIdentifier, clientDataHash: hash) { (attestation, error) in
                if let error = error {
                    observer(.failure(error))
                    return
                }
                guard let attestationString = attestation?.base64EncodedString() else {
                    observer(.failure(DeviceCheckError.noAttestKey))
                    return
                }

                let payload = RegisterAttestationRequest(nonce: nonce, attestation: attestationString, keyIdentifier: keyIdentifier)
                observer(.success(payload))
            }
            return Disposables.create()
        }
    }

    /// Encrypts server requests with private key
    /// - Parameters:
    ///   - keyIdentifier: identifier generated for each new installation. Not the actual key
    ///   - clientData: Server request containing server challenge
    /// - Returns: Encrypted client data required to be sent to server
    func getAssertion(keyIdentifier: String, clientData: Data) -> Single<Data> {
        return Single.create { observer -> Disposable in

            let hash = Data(SHA256.hash(data: clientData))
            DCAppAttestService.shared.generateAssertion(keyIdentifier, clientDataHash: hash) { (assertion, error) in
                if let error = error {
                    observer(.failure(error))
                    return
                }
                guard let assertion = assertion else {
                    observer(.failure(DeviceCheckError.noAttestKey))
                    return
                }
                observer(.success(assertion))
            }
            return Disposables.create()
        }
    }
}
