import Foundation
import RxSwift
import RxCocoa

/// It enables debug prints in `toggleableDebug`
let debugPrintsEnabled = false

public enum CommonRxError: Error {
    case selfDoesNotExistAnymore
    case unexpectedNil
}

public extension Completable {
    static func zip(_ stream: Observable<Completable>) -> Completable {
        return stream
            .toArray()
            .asObservable()
            .flatMap { Completable.zip($0) }
            .asCompletable()
    }

    static func from(_ closure: @escaping () throws -> Void) -> Completable {
        return Completable.deferred {
            do {
                try closure()
                return Completable.empty()
            } catch {
                return Completable.error(error)
            }
        }
    }

    func onErrorComplete() -> Completable {
        return self
            .asObservable()
            .onErrorComplete()
            .asCompletable()
    }

    func debug(logUtil: LogUtil, _ identifier: String) -> Completable {
        return self.do(onError: {error in
            logUtil.log("[\(identifier)] error: \(error)", entryType: .info)
        }, onCompleted: {
            logUtil.log("[\(identifier)] completed", entryType: .info)
        }, onSubscribe: {
            logUtil.log("[\(identifier)] subscribe", entryType: .info)
        }, onSubscribed: {
            logUtil.log("[\(identifier)] after subscribe", entryType: .info)
        }, onDispose: {
            logUtil.log("[\(identifier)] disposed", entryType: .info)
        })
    }

    func debugNotification(_ identifier: String, groupNotificationByIdentifier: Bool = false) -> Completable {
        return self.do(onError: {error in
            NotificationScheduler.shared
                .scheduleNotification(
                    title: "\(identifier)",
                    message: "Error: \(error)",
                    threadIdentifier: groupNotificationByIdentifier ? identifier : nil
                )
        }, onCompleted: {
            NotificationScheduler.shared
                .scheduleNotification(
                    title: "\(identifier)",
                    message: "Completed",
                    threadIdentifier: groupNotificationByIdentifier ? identifier : nil
                )
        }, onSubscribe: {
            NotificationScheduler.shared
                .scheduleNotification(
                    title: "\(identifier)",
                    message: "Subscribe",
                    threadIdentifier: groupNotificationByIdentifier ? identifier : nil
                )
        }, onSubscribed: {
            NotificationScheduler.shared
                .scheduleNotification(
                    title: "\(identifier)",
                    message: "After Subscribe",
                    threadIdentifier: groupNotificationByIdentifier ? identifier : nil
                )
        }, onDispose: {
            NotificationScheduler.shared
                .scheduleNotification(
                    title: "\(identifier)",
                    message: "Dispose",
                    threadIdentifier: groupNotificationByIdentifier ? identifier : nil
                )
        })
    }

    func logError(_ logUtil: LogUtil, _ identifier: String = "") -> Completable {
        self.do(onError: { error in
            if identifier != "" {
                logUtil.log("[\(identifier)] error: \(error)", entryType: .error)
            } else {
                logUtil.log("\(error)", entryType: .error)
            }
        })
    }

    func `catch`<ErrorType: Error & Equatable>(specific: ErrorType, _ action: @escaping () -> Completable) -> Completable {
        self.catch { error in
            if let cast = error as? ErrorType,
               cast == specific {
                return action()
            }
            throw error
        }
    }

    func continueInBackground(_ syncronousTask: @escaping () throws -> Void) -> Completable {
        Completable.create { observer in
            var taskID: UIBackgroundTaskIdentifier = .invalid
            taskID = UIApplication.shared.beginBackgroundTask {
                observer(.error(NSError(domain: "Task cancelled", code: 0, userInfo: nil)))
                UIApplication.shared.endBackgroundTask(taskID)
            }
            do {
                try syncronousTask()
                observer(.completed)
            } catch {
                observer(.error(error))
            }
            return Disposables.create {
                UIApplication.shared.endBackgroundTask(taskID)
                taskID = .invalid
            }
        }
    }
}

public enum ObservableError: Error {
    /// Occures when the value in the .cast() operator couldn't be casted
    case casting(from: String, to: String)
}

public protocol OptionalType {
    associatedtype Wrapped
    var value: Wrapped? { get }
}

extension Optional: OptionalType {
    /// Cast `Optional<Wrapped>` to `Wrapped?`
    public var value: Wrapped? {
        return self
    }
}

public extension PrimitiveSequence where Trait == SingleTrait, Element: OptionalType {
    func unwrapOptional(errorOnNil: Bool = false) -> Single<Element.Wrapped> {
        self.filter { (value) in
            if value.value == nil {
                if errorOnNil {
                    throw NSError(domain: "Nil force unwrapped!", code: 404, userInfo: nil)
                }
                return false
            }
            return true
        }
        .map { $0.value! }
        .asObservable()
        .asSingle()
    }
}

public extension PrimitiveSequence where Trait == MaybeTrait, Element: OptionalType {
    func unwrapOptional(errorOnNil: Bool = false) -> Maybe<Element.Wrapped> {
        self.filter { (value) in
            if value.value == nil {
                if errorOnNil {
                    throw NSError(domain: "Nil force unwrapped!", code: 404, userInfo: nil)
                }
                return false
            }
            return true
        }
        .map { $0.value! }
    }
}

public extension PrimitiveSequenceType where Trait == SingleTrait {

    static func from(_ closure: @escaping () throws -> Element) -> Single<Element> {
        return Single<Element>.deferred {
            do {
                let retVal = try closure()
                return Single.just(retVal)
            } catch {
                return Single.error(error)
            }
        }
    }

    func cast<T>() -> Single<T> {
        return self.map { (value) in
            if let retVal = value as? T {
                return retVal
            }
            throw ObservableError.casting(from: String(reflecting: Element.self), to: String(reflecting: T.self))
        }
    }

    func logError(_ logUtil: LogUtil, _ identifier: String = "") -> Single<Element> {
        self.do(onError: { error in
            if identifier != "" {
                logUtil.log("[\(identifier)] error: \(error)", entryType: .error)
            } else {
                logUtil.log("\(error)", entryType: .error)
            }
        })
    }

    func debug(logUtil: LogUtil, _ identifier: String) -> Single<Element> {
        return self.do(onSuccess: {
            logUtil.log("[\(identifier)] success: \($0)", entryType: .info)
        }, onError: {error in
            logUtil.log("[\(identifier)] error: \(error)", entryType: .info)
        }, onSubscribe: {
            logUtil.log("[\(identifier)] subscribe", entryType: .info)
        }, onSubscribed: {
            logUtil.log("[\(identifier)] after subscribe", entryType: .info)
        }, onDispose: {
            logUtil.log("[\(identifier)] disposed", entryType: .info)
        })
    }

    func debugNotification(_ identifier: String, groupNotificationByIdentifier: Bool = false) -> Single<Element> {
        return self.do(onSuccess: { element in
            NotificationScheduler.shared
                .scheduleNotification(
                    title: "\(identifier)",
                    message: "Success: \(element)",
                    threadIdentifier: groupNotificationByIdentifier ? identifier : nil
                )
        }, onError: {error in
            NotificationScheduler.shared
                .scheduleNotification(
                    title: "\(identifier)",
                    message: "Error: \(error)",
                    threadIdentifier: groupNotificationByIdentifier ? identifier : nil
                )
        }, onSubscribe: {
            NotificationScheduler.shared
                .scheduleNotification(
                    title: "\(identifier)",
                    message: "Subscribe",
                    threadIdentifier: groupNotificationByIdentifier ? identifier : nil
                )
        }, onSubscribed: {
            NotificationScheduler.shared
                .scheduleNotification(
                    title: "\(identifier)",
                    message: "After Subscribe",
                    threadIdentifier: groupNotificationByIdentifier ? identifier : nil
                )
        }, onDispose: {
            NotificationScheduler.shared
                .scheduleNotification(
                    title: "\(identifier)",
                    message: "Dispose",
                    threadIdentifier: groupNotificationByIdentifier ? identifier : nil
                )
        })
    }

    func continueInBackground<T>(_ syncronousTask: @escaping () throws -> T) -> Single<T> {
        Single.create { observer in
            var taskID: UIBackgroundTaskIdentifier = .invalid
            taskID = UIApplication.shared.beginBackgroundTask {
                observer(.failure(NSError(domain: "Task cancelled", code: 0, userInfo: nil)))
                UIApplication.shared.endBackgroundTask(taskID)
            }
            do {
                let t: T = try syncronousTask()
                observer(.success(t))
            } catch {
                observer(.failure(error))
            }
            return Disposables.create {
                UIApplication.shared.endBackgroundTask(taskID)
                taskID = .invalid
            }
        }
    }

}

public extension PrimitiveSequenceType where Trait == MaybeTrait {
    static func from(_ closure: @escaping () throws -> Element?) -> Maybe<Element> {
        return Maybe<Element>.deferred {
            do {
                let retVal = try closure()
                if let retValUnwrapped = retVal {
                    return Maybe.just(retValUnwrapped)
                }
                return Maybe.empty()
            } catch {
                return Maybe.error(error)
            }
        }
    }

    func cast<T>() -> Maybe<T> {
        return self.map { (value) in
            if let retVal = value as? T {
                return retVal
            }
            throw ObservableError.casting(from: String(reflecting: Element.self), to: String(reflecting: T.self))
        }
    }

    func logError(_ logUtil: LogUtil, _ identifier: String = "") -> Maybe<Element> {
        self.do(onError: { error in
            if identifier != "" {
                logUtil.log("[\(identifier)] error: \(error)", entryType: .error)
            } else {
                logUtil.log("\(error)", entryType: .error)
            }
        })
    }

}

public extension PrimitiveSequence {
    func retry(maxAttempts: Int, delay: RxTimeInterval, scheduler: SchedulerType) -> PrimitiveSequence<Trait, Element> {
        return self.retry { errors in
            return errors.enumerated().flatMap { (index, error) -> Observable<Int64> in
                if index <= maxAttempts {
                    return Observable<Int64>.timer(delay, scheduler: scheduler)
                } else {
                    return Observable.error(error)
                }
            }
        }
    }

    func retry(delay: RxTimeInterval, scheduler: SchedulerType) -> PrimitiveSequence<Trait, Element> {
        return self.retry { errors in
            return errors.enumerated().flatMap { (_, _) -> Observable<Int64> in
                return Observable<Int64>.timer(delay, scheduler: scheduler)
            }
        }
    }

    func toggleableDebug(_ identifier: String? = nil, trimOutput: Bool = false) -> Self {
        if debugPrintsEnabled {
            return self.debug(identifier, trimOutput: trimOutput)
        }
        return self
    }
}

extension Observable {
    func delayed(by delay: RxTimeInterval, scheduler: SchedulerType) -> Observable {
        Observable.zip(
            self,
            Observable<Int>.interval(delay, scheduler: scheduler)) { element, _ in
            return element
        }
    }
}

// Two way binding operator between control property and relay, that's all it takes.
infix operator <-> : DefaultPrecedence

func <-> <T> (property: ControlProperty<T>, relay: BehaviorRelay<T>) -> Disposable {
    let bindToUIDisposable = relay.bind(to: property)
    let bindToRelay = property
        .subscribe(onNext: { n in
            relay.accept(n)
        }, onCompleted: {
            bindToUIDisposable.dispose()
        })

    return Disposables.create(bindToUIDisposable, bindToRelay)
}

func <-> <T> (property: ControlProperty<T>, relay: PublishSubject<T>) -> Disposable {
    let bindToUIDisposable = relay.bind(to: property)
    let bindToRelay = property
        .subscribe(onNext: { n in
            relay.onNext(n)
        }, onCompleted: {
            bindToUIDisposable.dispose()
        })

    return Disposables.create(bindToUIDisposable, bindToRelay)
}

func + <T>(l: Observable<T>, r: Observable<T>) -> Observable<T> {
    return Observable<T>.merge(l, r)
}
