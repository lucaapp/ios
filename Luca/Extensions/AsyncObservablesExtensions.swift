import Foundation
import RxSwift
import RxCocoa

public extension PrimitiveSequence where Trait == SingleTrait {
    func task() async throws -> Self.Element {
        try await withCheckedThrowingContinuation { continuation in
            _ = self
                .do(onSuccess: { continuation.resume(returning: $0) })
                .do(onError: { continuation.resume(throwing: $0) })
                .subscribe()
        }
    }
}

public extension PrimitiveSequence where Trait == MaybeTrait {
    func task() async throws -> Self.Element? {
        try await self.asObservable().toArray().map { $0.first }.task()
    }
}

public extension Observable {
    func task() async throws -> [Element] {
        try await self.toArray().task()
    }
}

public extension Completable {
    func task() async throws {
        try await withCheckedThrowingContinuation { continuation in
            _ = self
                .do(onCompleted: { continuation.resume() })
                .do(onError: { continuation.resume(throwing: $0) })
                .subscribe()
        }
    }
}

public extension SharedSequence where SharingStrategy == DriverSharingStrategy {
    func task() async -> [Self.Element] {
        await withCheckedContinuation { continuation in
            _ = self
                .asObservable()
                .toArray()
                .do(onSuccess: { continuation.resume(returning: $0) }) // It doesn't need to handle errors as `Drivers` are not failable
                .subscribe()
        }
    }
}

public extension Infallible {
    func task() async -> [Self.Element] {
        await withCheckedContinuation { continuation in
            _ = self
                .asObservable()
                .toArray()
                .do(onSuccess: { continuation.resume(returning: $0) }) // It doesn't need to handle errors as `Infallibles` guarantee to have no errors
                .subscribe()
        }
    }

    func taskFirst() async -> Self.Element {
        await withCheckedContinuation { continuation in
            _ = self
                .asObservable()
                .take(1)
                .do(onNext: { continuation.resume(returning: $0) }) // It doesn't need to handle errors as `Infallibles` guarantee to have no errors
                .subscribe()
        }
    }
}
