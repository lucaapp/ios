import UIKit

public extension Double {

    private var hoursTimeUnit: Int {
        return Int(self) / 3600
    }

    private var minutesTimeUnit: Int {
        return Int(self) / 60 % 60
    }

    private var secondsTimeUnit: Int {
        return Int(self) % 60
    }

    /// Returns a string containing time with the format of HH:mm:ss
    var formattedTimeString: String {
        return String(format: "%02i:%02i:%02i", self.hoursTimeUnit, self.minutesTimeUnit, self.secondsTimeUnit)
    }

    var formattedExpiryTimeMinutes: String {
        return String(format: "%i %@", self.minutesTimeUnit, L10n.IOSApp.Test.Expiry.minutes)
    }

    var formattedExpiryTimeHours: String {
        return String(format: "%i %@", self.hoursTimeUnit, L10n.IOSApp.Test.Expiry.hours)
    }

    var formattedPercentage: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        return formatter.string(for: self) ?? ""
    }

}

extension Double {
    func money<C: Currency>(with currency: C) -> Money<C> {
        Money(double: self, currency: currency)
    }
}
