import Foundation

extension Date {

    /// Format "dd.MM.yyyy HH.mm"
    var formattedDateTime: String {
        let formatter = DateFormatter()
        formatter.dateFormat = L10n.IOSApp.Checkin.Slider.dateFormat
        let date = formatter.string(from: self)
        return date
    }

    /// Format "dd.MM.yyyy"
    var formattedDate: String {
        let formatter = DateFormatter()
        formatter.dateFormat = L10n.IOSApp.Test.Result.dateFormat
        let date = formatter.string(from: self)
        return date
    }

    func formattedTime(_ timeStyle: DateFormatter.Style) -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = timeStyle

        let date = formatter.string(from: self)
        return date
    }

    var accessibilityDate: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .short

        let date = formatter.string(from: self)
        return date
    }

    var durationSinceDate: String {
        let formatter = DateComponentsFormatter()
        formatter.maximumUnitCount = 1
        formatter.unitsStyle = .full
        formatter.zeroFormattingBehavior = .dropAll
        formatter.allowedUnits = [.year, .month, .day, .hour, .minute]
        return formatter.string(from: self, to: Date.now) ?? "Time error"
    }

    func durationUntil(date: Date) -> String {
        let time = NSInteger(date.timeIntervalSince1970 - timeIntervalSince1970)

        let minutes = (time / 60) % 60
        let hours = (time / 3600)

        return String(format: "%0.2d:%0.2dh", hours, minutes)

    }

    /// Format string with DGC date format (e.g. 19640812 = 12.Aug 1964) or RFC3339 date time if time was added to the date string
    /// - Parameter dateString: date formated string. Either `yyyy-MM-dd` or RFC3339 date time format
    /// - Returns: Date
    static func formatDGCDateString(dateString: String) -> Date? {
        return Date.formatDateString(dateString: dateString, format: "yyyy-MM-dd") ??
        Date.formatRFC3339DateTimeString(dateString: dateString)
    }

    static func formatDateString(dateString: String, format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: dateString)
    }

    /// Returns true if `self` is within the defined scope,
    func isWithinLast(_ value: Int, _ component: Calendar.Component) -> Bool {
        guard let threshold = Calendar.current.date(byAdding: component, value: -value, to: Date.now) else {
            return false
        }
        return self.timeIntervalSince1970 > threshold.timeIntervalSince1970
    }

    /// Format RFC3339 date time string
    /// based on SwiftDGC implementation
    /// - Parameter dateString: string in RFC3339 format
    static func formatRFC3339DateTimeString(dateString: String) -> Date? {
      var str = dateString
      let rfc3339DateTimeFormatter = DateFormatter()

      if (try? NSRegularExpression(
        pattern: "\\.[0-9]{6}Z?$", options: []
      ).matches(
        in: str, options: [], range: NSRange(
          str.startIndex..<str.endIndex,
          in: str
        )
      ))?.isEmpty == false {
        str = str.trimmingCharacters(in: ["Z"])
        str = String(str.prefix(str.count - 3)) + "Z"
      }

      for fmt in [
        "yyyy-MM-dd'T'HH:mm:ssZZZZZ", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ",
        "yyyy-MM-dd't'HH:mm:ss'z'", "yyyy-MM-dd't'HH:mm:ss.SSS'z'"
      ] {
        rfc3339DateTimeFormatter.dateFormat = fmt
        if let date = rfc3339DateTimeFormatter.date(from: str) {
          return date
        }
      }
      return nil
    }
}
