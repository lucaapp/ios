import Foundation

extension URL {

    func getParam(of parameter: String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == parameter })?.value
    }

    mutating func excludeFromCloudBackups() {
        var values = URLResourceValues()
        values.isExcludedFromBackup = true
        try? setResourceValues(values)
    }
}
