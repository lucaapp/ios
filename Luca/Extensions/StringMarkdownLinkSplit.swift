import Foundation

extension Range {
    static func ~= (lhs: Self, rhs: Self) -> Bool {
        rhs.clamped(to: lhs) == rhs
    }
}
extension ClosedRange {
    static func ~= (lhs: Self, rhs: Self) -> Bool {
        rhs.clamped(to: lhs) == rhs
    }
}

extension String {
    var splitByMarkdownLinks: [String] {
        var elements = [String]()
        var currentComponent = self[self.startIndex..<self.endIndex]
        while !currentComponent.isEmpty {
            if let potentialNewLinkRangeLowerbound = currentComponent.firstIndex(of: "["),
               let midRange = currentComponent.range(of: "]("),
               let potentialNewLinkRangeUpperbound = currentComponent.firstIndex(of: ")") {

                if potentialNewLinkRangeUpperbound < potentialNewLinkRangeLowerbound {
                    let highestIndex = currentComponent.index(after: min(potentialNewLinkRangeUpperbound, midRange.lowerBound))
                    elements.append(String(currentComponent.prefix(upTo: highestIndex)))
                    currentComponent = currentComponent.suffix(from: highestIndex)
                    continue
                }

                let wholeRange = potentialNewLinkRangeLowerbound..<currentComponent.index(after: potentialNewLinkRangeUpperbound)

                if wholeRange ~= midRange,
                   String(currentComponent[wholeRange]).isMarkdownLink {
                    elements.append(String(currentComponent[currentComponent.startIndex..<potentialNewLinkRangeLowerbound]))
                    elements.append(String(currentComponent[wholeRange]))
                    currentComponent = currentComponent.suffix(from: wholeRange.upperBound)
                } else {
                    let lowestIndex = currentComponent.index(after: min(wholeRange.lowerBound, midRange.lowerBound))
                    elements.append(String(currentComponent.prefix(upTo: lowestIndex)))
                    currentComponent = currentComponent.suffix(from: lowestIndex)
                }
                continue
            }
            elements.append(String(currentComponent))
            currentComponent = ""
        }
        return elements
            .filter { !$0.isEmpty }
            .merge({ !$0.isMarkdownLink && !$1.isMarkdownLink }, merger: +)
    }

    var isMarkdownLink: Bool {
        self[self.startIndex] == "[" &&
        self[self.index(before: self.endIndex)] == ")" &&
        self.contains("](") &&
        count(occurrences: "[") == 1 &&
        count(occurrences: "]") == 1 &&
        count(occurrences: "](") == 1 &&
        count(occurrences: ")") == 1 &&
        count(occurrences: "(") == 1
    }

    func count(occurrences: String) -> Int {
        components(separatedBy: occurrences).count - 1
    }

    var markdownLink: MarkdownLink? {
        guard isMarkdownLink else {
            return nil
        }
        let split = self.components(separatedBy: "](")
        var textString = split[0]
        var linkString = split[1]
        textString.removeFirst()
        linkString.removeLast()
        return MarkdownLink(text: textString, link: linkString)
    }

}

extension Array {
    func merge(_ condition: (Element, Element) -> Bool, merger: (Element, Element) -> Element) -> [Element] {
        var copy = self
        var i = 0
        while i != copy.endIndex - 1 {
            if condition(copy[i], copy[i + 1]) {
                copy[i] = merger(copy[i], copy[i + 1])
                copy.remove(at: i + 1)
            } else {
                i += 1
            }
        }
        return copy
    }
}

struct MarkdownLink {
    var text: String
    var link: String
}
