import RxSwift
import JGProgressHUD

extension Observable {
    func handleLoading(_ indicator: JGProgressHUD, parent: UIView) -> Observable<Element> {
        self
            .do(onSubscribe: {
                DispatchQueue.main.async {
                    indicator.show(in: parent, animated: true)
                }
            })
            .do(onDispose: {
                DispatchQueue.main.async {
                    indicator.dismiss(animated: true)
                }
            })
    }
}

extension PrimitiveSequence where Trait == SingleTrait {
    func handleLoading(_ indicator: JGProgressHUD, parent: UIView) -> Self {
        self.asObservable()
            .handleLoading(indicator, parent: parent)
            .asSingle()
    }
}

extension PrimitiveSequence where Trait == MaybeTrait {
    func handleLoading(_ indicator: JGProgressHUD, parent: UIView) -> Self {
        self.asObservable()
            .handleLoading(indicator, parent: parent)
            .asMaybe()
    }
}

extension Completable {
    func handleLoading(_ indicator: JGProgressHUD, parent: UIView) -> Self {
        self.asObservable()
            .handleLoading(indicator, parent: parent)
            .ignoreElementsAsCompletable()
    }
}
