import SwiftUI
import Foundation

extension Text {
    @ViewBuilder
    static func supportMarkdownLinks(_ string: String) -> some View {
        if #available(iOS 15, *) {
            string.splitByMarkdownLinks.reduce(Text("")) { partialResult, nextString in
                if let att = try? AttributedString(markdown: nextString),
                   nextString.isMarkdownLink {
                    return partialResult + Text(att).foregroundColor(Color.red).underline()
                } else {
                    return partialResult + Text(nextString)
                }
            }
        } else {
            VStack {
                ForEach(string.splitByMarkdownLinks, id: \.self) { line in
                    if let markdownLink = line.markdownLink {
                        Text(markdownLink.text).onTapGesture {
                            print("Tapped on: \(markdownLink.link)")
                        }
                        .foregroundColor(Color.red)
                    } else {
                        Text(line)
                    }
                }
            }
        }
    }
}
