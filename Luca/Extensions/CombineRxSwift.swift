import Combine
import RxSwift

extension AnyPublisher {
    func asObservable() -> Observable<Output> {
        Observable<Output>.create { observer in
            var cancellable: AnyCancellable? = self.sink { completion in
                switch completion {
                case .failure(let error): observer.onError(error)
                case .finished: observer.onCompleted()
                }
            } receiveValue: { value in
                observer.onNext(value)
            }
            return Disposables.create {
                cancellable = nil
            }
        }
    }
}
