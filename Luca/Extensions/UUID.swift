import Foundation

/// Represents settings about UUID components
protocol UUIDComponent {

    /// Affected byte index
    var byteIndex: Int { get }

    /// Bitmask to extract the value
    var bitmask: UInt8 { get }

    /// Value that represents this specific component
    var componentValue: UInt8 { get }
}

/// Version of the UUID
enum UUIDVersion: CaseIterable {
    case v1
    case v2
    case v3
    case v4
}

/// Variant of the UUID
enum UUIDVariant: CaseIterable {
    case variant0
    case variant1
    case variant2
}

extension UUIDVersion: UUIDComponent {
    var byteIndex: Int { 6 }
    var bitmask: UInt8 { 0b11110000 }
    var componentValue: UInt8 {
        switch self {
        case .v1: return 0x10
        case .v2: return 0x20
        case .v3: return 0x30
        case .v4: return 0x40
        }
    }
}

extension UUIDVariant: UUIDComponent {
    var byteIndex: Int { 8 }
    var bitmask: UInt8 {
        switch self {
        case .variant0: return 0b10000000
        case .variant1: return 0b11000000
        case .variant2: return 0b11100000
        }
    }
    var componentValue: UInt8 {
        switch self {
        case .variant0: return 0
        case .variant1: return 0b10000000
        case .variant2: return 0b11000000
        }
    }
}

extension UInt8 {
    /// Returns the inverted byte.
    ///
    /// Example: `0b00000001.inverted` returns `0b11111110`
    var inverted: UInt8 {
        self ^ 0xff
    }
}

extension UUID {
    var bytes: [UInt8] {
        var tuple = self.uuid
        return [UInt8](UnsafeBufferPointer(start: &tuple.0, count: MemoryLayout.size(ofValue: tuple)))
    }

    var data: Data {
        Data(bytes)
    }

    /// Returns the version of the UUID. Returns nil if unable to specify the value
    var version: UUIDVersion? {
        let b = bytes
        for version in UUIDVersion.allCases where b[version.byteIndex] & version.bitmask == version.componentValue {
            return version
        }
        return nil
    }

    /// Returns the variant of the UUID. Returns nil if unable to specify the value
    var variant: UUIDVariant? {
        let b = bytes
        for variant in UUIDVariant.allCases where b[variant.byteIndex] & variant.bitmask == variant.componentValue {
            return variant
        }
        return nil
    }

    /// Creates a valid UUID with given parameters.
    ///
    /// - Parameter data: nil for random data; otherwise 16 byte is required
    /// - Parameter version: it will set the 7th byte according to selected version
    /// - Parameter variant: it will set the 9th byte according to selected version
    ///
    /// - Returns: nil if given data buffer is not 16 bytes long
    init?(data: Data? = nil, version: UUIDVersion, variant: UUIDVariant) {
        let data = data ?? KeyFactory.randomBytes(size: 16) ?? Data()

        if data.count != 16 {
            return nil
        }

        self.init()

        var modifiedBytes = data.bytes

        modifiedBytes[version.byteIndex] = (modifiedBytes[version.byteIndex] & version.bitmask.inverted) | version.componentValue
        modifiedBytes[variant.byteIndex] = (modifiedBytes[variant.byteIndex] & variant.bitmask.inverted) | variant.componentValue

        self = modifiedBytes.withUnsafeBytes { $0.load(as: UUID.self) }
    }
}
