import UIKit
import Nantes

extension NantesLabel {
    func buildTappableLabel(linkDescription: String, linkTerm: String, linkURL: String, delegate: NantesLabelDelegate) {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font as Any,
            .foregroundColor: UIColor.white
        ]
        let attrText = NSMutableAttributedString(string: linkDescription, attributes: attributes)
        attributedText = attrText

        numberOfLines = 0
        self.delegate = delegate

        let linkAttributes: [NSAttributedString.Key: Any] = [
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .font: font.bold() as Any
        ]
        let clickedAttributes: [NSAttributedString.Key: Any] = [
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .foregroundColor: Asset.lucaGrey.color,
            .font: font.bold() as Any
        ]
        self.linkAttributes = linkAttributes
        activeLinkAttributes = clickedAttributes
        if let linkRange = linkDescription.range(of: linkTerm),
           let url = URL(string: linkURL) {
            addLink(to: url, withRange: NSRange(linkRange, in: linkDescription))
        }
    }
}
