import Foundation
import SwiftJWT

extension JWT {
    /// Validate the time based standard JWT claims.
    /// This function checks that the "exp" (expiration time) is in the future
    /// and the "iat" (issued at) and "nbf" (not before) headers are in the past,
    ///
    /// - Parameter leeway: The time in seconds that the JWT can be invalid but still accepted to account for clock differences.
    /// - Parameter now: Current date
    /// - Parameter validityTime: In case where `exp` is not set, the validity duration could be set.
    /// - Returns: A value of `ValidateClaimsResult`.
    public func validateClaims(leeway: TimeInterval = 0, now: Date, validityTime: TimeInterval? = nil) -> ValidateClaimsResult {

        if let issuedAtDate = claims.iat {
            if issuedAtDate > now + leeway {
                return .issuedAt
            }
        }

        if let expirationDate = claims.exp {
            if expirationDate + leeway < now {
                return .expired
            }
        }

        if let validityTime = validityTime,
           let iat = claims.iat,
           claims.exp == nil {
            if validityTime < 0 {
                return .invalidExpiration
            }
            if now > iat + validityTime {
                return .expired
            }
        }

        if let notBeforeDate = claims.nbf {
            if notBeforeDate > now + leeway {
                return .notBefore
            }
        }

        return .success
    }
}
