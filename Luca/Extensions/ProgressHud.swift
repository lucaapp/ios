import Foundation
import JGProgressHUD

extension JGProgressHUD {
    static func lucaLoading() -> JGProgressHUD {
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = L10n.IOSApp.Navigation.Basic.loading
        hud.tintColor = .black
        return hud
    }
}
