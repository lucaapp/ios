import UIKit
import AVFoundation

protocol ContainsTorchButton {
    func toggleTorch(sender: Any?, forceOff: Bool)
}

extension ContainsTorchButton {
    func toggleTorch(sender: Any?, forceOff: Bool = false) {
        guard let device = AVCaptureDevice.default(for: .video) else { return }
        defer { device.unlockForConfiguration() }
        try? device.lockForConfiguration()
        if device.torchMode == .off && forceOff == false {
            try? device.setTorchModeOn(level: 1.0)
            if let lightButton = sender as? UIButton {
                lightButton.accessibilityLabel = L10n.IOSApp.Light.Button.deactivate
            }
        } else {
            device.torchMode = .off
            if let lightButton = sender as? UIButton {
                lightButton.accessibilityLabel = L10n.IOSApp.Light.Button.activate
            }
        }
    }
}
