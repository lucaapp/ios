import Foundation

extension String {
    static func isNilOrEmpty(_ string: String?) -> Bool {
        return string == nil || string == ""
    }
    func split(every length: Int) -> [Substring] {
        guard length > 0 && length < count else { return [suffix(from: startIndex)] }

        return (0 ... (count - 1) / length).map { dropFirst($0 * length).prefix(length) }
    }

    func split(backwardsEvery length: Int) -> [Substring] {
        guard length > 0 && length < count else { return [suffix(from: startIndex)] }

        return (0 ... (count - 1) / length).map { dropLast($0 * length).suffix(length) }.reversed()
    }

    static func hex(from value: Int) -> String {
        String(format: "%02X", value)
    }

    func base64ToHex() -> String? {
        let data = Data(base64Encoded: self)
        return data?.toHexString()
    }

    /// Removes some set of extra characters.
    ///
    /// This set is compatible with Android and Backend implementations.
    func sanitize() -> String {
        self.replacingOccurrences(of: "[^\\w +.:@£À-ÿāăąćĉċčđēėęěĝğģĥħĩīįİıĵķĸĺļłńņōőœŗřśŝšţŦũūŭůűųŵŷźżžơưếệ-]", with: " ", options: .regularExpression).trimmingCharacters(in: .whitespacesAndNewlines)
    }

    /// Removes occurences of `PROF` and `DR` uppercased.
    func removeAcademicTitles() -> String {
        var current = self
            .replacingOccurrences(of: ".", with: " ")
            .replacingOccurrences(of: ",", with: " ")
        var previous = ""
        while current != previous {
            previous = current
            current = current
                .remove(suffix: " PROF")
                .remove(suffix: " DR")
                .remove(prefix: "PROF ")
                .remove(prefix: "DR ")
                .trimmingCharacters(in: .whitespacesAndNewlines)
        }
        return current
    }

    /// Removes all characters from string except capital letters A...Z.
    /// - Returns: Capital letters from english alphabet only
    func removeNonASCIIUppercased() -> String {
        replacingOccurrences(of: "[^\\x41-\\x5A]", with: "", options: .regularExpression)
    }

    /// Removes occurences of `strings`. Case sensitive.
    func removeOccurences(of strings: [String]) -> String {
        var result = self
        for string in strings {
            result = result.replacingOccurrences(of: string, with: "")
        }
        return result
    }

    /// Removes prefix if contained. Case sensitive.
    func remove(prefix: String) -> String {
        if hasPrefix(prefix) {
            return String(self.dropFirst(prefix.count))
        }
        return self
    }

    /// Removes suffix if contained. Case sensitive.
    func remove(suffix: String) -> String {
        if hasSuffix(suffix) {
            return String(self.dropLast(suffix.count))
        }
        return self
    }

    func removeWhitespaces() -> String {
        return self.filter { !$0.isWhitespace }
    }

    var htmlString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }

    func debug(_ prefix: String = "") -> String {
        print(prefix + " \(self)")
        return self
    }

    public var isEmailAddress: Bool {
        let dataDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let firstMatch = dataDetector?.firstMatch(in: self,
                                                  options: .reportCompletion,
                                                  range: NSRange(location: 0, length: count))
        return (firstMatch?.range.location != NSNotFound && firstMatch?.url?.scheme == "mailto")
    }

}

extension StringProtocol {
    func distance(of element: Element) -> Int? { firstIndex(of: element)?.distance(in: self) }
    func distance<S: StringProtocol>(of string: S) -> Int? { range(of: string)?.lowerBound.distance(in: self) }
}

extension Collection {
    func distance(to index: Index) -> Int { distance(from: startIndex, to: index) }
}

extension String.Index {
    func distance<S: StringProtocol>(in string: S) -> Int { string.distance(to: self) }
}

extension Optional where Wrapped == String {

    func prefix(chars: Int) -> String? {
        if let self = self {
            return String(self.prefix(chars))
        }
        return nil
    }

}
