import UIKit
import LucaUIComponents

extension UIViewController {
    func set(title: String, subtitle: String? = nil) {

        if navigationbarTitleLabel == nil {
            let stackView = UIStackView()
            stackView.axis = .vertical

            let titleLabel = (subtitle == nil ? Luca20PtBoldLabel() : Luca16PtBoldLabel())
            titleLabel.textAlignment = .center
            titleLabel.tag = 1
            titleLabel.accessibilityTraits = .header
            stackView.addArrangedSubview(titleLabel)

            let subtitleLabel = Luca12PtLabel()
            subtitleLabel.textAlignment = .center
            subtitleLabel.tag = 2
            stackView.addArrangedSubview(subtitleLabel)

            navigationItem.title = title
            navigationItem.titleView = stackView
        }

        navigationbarTitleLabel?.text = title
        navigationbarSubtitleLabel?.text = subtitle
        navigationbarSubtitleLabel?.isHidden = subtitle == nil
    }

    var navigationbarTitleLabel: UILabel? {
        return navigationItem.titleView?.viewWithTag(1) as? UILabel
    }

    var navigationbarSubtitleLabel: UILabel? {
        return navigationItem.titleView?.viewWithTag(2) as? UILabel
    }
}

extension UIViewController {

    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }

}

extension UIViewController {
    static var visibleViewController: UIViewController? {
        var currentVc = UIApplication.shared.keyWindow?.rootViewController
        while let presentedVc = currentVc?.presentedViewController {
            if let navVc = (presentedVc as? UINavigationController)?.viewControllers.last {
                currentVc = navVc
            } else if let tabVc = (presentedVc as? UITabBarController)?.selectedViewController {
                currentVc = tabVc
            } else {
                currentVc = presentedVc
            }
        }
        return currentVc
    }
}

extension UIViewController {
    static func instantiate<T: UIViewController>(storyboard: UIStoryboard, identifier: String) -> T {
        let viewController = storyboard.instantiateViewController(withIdentifier: identifier) as? T
        if viewController == nil {
            print("Error instantiating UIViewController. Storyboard is setup incorrectly.")
        }
        // Is purposefully force unwrapped to crash the system if the storyboard is setup incorrectly.
        return viewController!
    }

    static func fromStoryboard() -> Self {
        let identifier = String(describing: self)
        let viewController = UIStoryboard(name: identifier, bundle: nil).instantiateViewController(withIdentifier: identifier) as? Self
        if viewController == nil {
            print("Error instantiating UIViewController. Storyboard is setup incorrectly.")
        }
        // Is purposefully force unwrapped to crash the system if the storyboard is setup incorrectly.
        return viewController!
    }
}
