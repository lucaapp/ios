import Foundation

protocol AssociableToIdentity {
    func belongs(to identity: HasFirstAndLastName) -> Bool
}

protocol HasFirstAndLastName {
    var firstName: String { get }
    var lastName: String { get }
}

extension HasFirstAndLastName {

    var simplifiedFirstName: String {
        (firstName
            .uppercased()
            .removeAcademicTitles()
            .components(separatedBy: " ")
            .first ?? "")
            .removeNonASCIIUppercased()
            .removeWhitespaces()
    }

    var simplifiedLastName: String {
        lastName
            .uppercased()
            .removeAcademicTitles()
            .removeNonASCIIUppercased()
            .removeWhitespaces()
    }

    var simplifiedFullName: String {
        "\(simplifiedFirstName) \(simplifiedLastName)"
    }

    func equalsSimplified(to other: HasFirstAndLastName) -> Bool {
        simplifiedFullName == other.simplifiedFullName
    }

    var fullName: String {
        [firstName, lastName].compactMap {$0}.joined(separator: " ")
    }
}

extension AssociableToIdentity where Self: HasFirstAndLastName {
    func belongs(to identity: HasFirstAndLastName) -> Bool {
        equalsSimplified(to: identity)
    }
}

protocol HasAddress {
    var street: String {get}
    var houseNumber: String {get}
    var postCode: String {get}
    var city: String {get}
}

extension HasAddress {
    var fullAddress: String? {
        guard addressComplete else {return nil}
        return "\(street) \(houseNumber), \(postCode) \(city)"
    }

    var addressComplete: Bool {
        return !String.isNilOrEmpty(street) &&
            !String.isNilOrEmpty(houseNumber) &&
            !String.isNilOrEmpty(postCode) &&
            !String.isNilOrEmpty(city)
    }
}
