import Foundation
import UserNotifications
import DependencyInjection

public class NotificationScheduler {
    @InjectStatic(\.timeProvider) private var timeProvider

    public static let shared = NotificationScheduler()
    public static let runtimeId = Int.random(in: 0...100)

    public func scheduleNotification(title: String, message: String, date: Date? = nil, requestIdentifier: String? = nil, threadIdentifier: String? = nil) {

        let content = UNMutableNotificationContent()

        content.title = title
        content.body = message
        if let thread = threadIdentifier {
            content.threadIdentifier = String(describing: thread)
        }

        #if DEBUG
        content.body = "\(message)\nRuntimeId: \(NotificationScheduler.runtimeId)"
        content.threadIdentifier = "Debug Notifications.\(String(describing: threadIdentifier))"
        #endif

        var trigger: UNNotificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        if let dateTrigger = date {
            trigger = UNTimeIntervalNotificationTrigger(timeInterval: dateTrigger.timeIntervalSince1970 - timeProvider.now.timeIntervalSince1970, repeats: false)
        }
        let identifier = requestIdentifier ?? UUID().uuidString
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)

        UNUserNotificationCenter.current().add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    }

}
