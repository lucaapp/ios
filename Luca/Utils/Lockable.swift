import Foundation
import RxSwift

// Object used solely for locking and releasing synchronizations
public class LockSynchronizer: NSObject {}

// Watch out for the _swiftEmptyDictionaryStorage! Empty dictionaries are actually NOT allocated! The new dictionaries without content are references of this mysterious _swiftEmptyDictionaryStorage. So, when entering a lock with this reference and exitting after filling the dictionary with data, you are locking and releasing two different objects.
public func synchronized<T>(_ lock: LockSynchronizer, closure: () throws -> T) rethrows -> T {
    defer { objc_sync_exit(lock) }
    objc_sync_enter(lock)
    return try closure()
}

public func synchronized<T>(_ lock: LockSynchronizer, closure: () -> T) -> T {
    defer { objc_sync_exit(lock) }
    objc_sync_enter(lock)
    return closure()
}

public class Lockable<U> {
    var target: U
    private lazy var logger: LogUtil = { StandardLog(subsystem: "Lockable", category: String(describing: self), subDomains: []) }()
    private var lock = LockSynchronizer()
    public init(target: U) {
        self.target = target
    }

    public func synchronized<T>(_ closure: () throws -> T) rethrows -> T {
        defer {
            objc_sync_exit(lock)
        }
        objc_sync_enter(lock)
        return try closure()
    }

    public func synchronized<T>(_ closure: () -> T) -> T {
        defer {
            objc_sync_exit(lock)
        }
        objc_sync_enter(lock)
        return closure()
    }
}

extension Lockable {
    func synchronized<T>(_ closure: (U) -> T) -> T {
        self.synchronized { closure(self.target) }
    }

    func synchronized<T>(_ closure: (U) throws -> T) rethrows -> T {
        try self.synchronized { try closure(self.target) }
    }
}

public class DispatchQueueSynced<U> {
    private var target: U
    private var dispatchQueue: DispatchQueue
    private var callbackQueue: DispatchQueue
    private let changePublisher = PublishSubject<Void>()
    var onChange: Observable<Void> {
        changePublisher
    }
    init(_ target: U) {
        dispatchQueue = DispatchQueue(label: "\(String(describing: Self.self))", qos: .default)
        callbackQueue = DispatchQueue(label: "\(String(describing: Self.self)).Callback", qos: .default)
        self.target = target
    }
    public func write(_ closure: @escaping (U) -> U) {
        dispatchQueue.async(flags: .barrier) {
            self.target = closure(self.target)
            self.callbackQueue.async { self.changePublisher.onNext(Void()) }
        }
    }

    public func read(_ closure: @escaping (U) -> Void) {
        dispatchQueue.async {
            closure(self.target)
        }
    }
}

extension DispatchQueueSynced {
    func writeRx(_ closure: @escaping (U) -> U) -> Completable {
        Completable.create { observer in
            self.write { currentValue in
                defer {
                    self.callbackQueue.async { observer(.completed) }
                }
                return closure(currentValue)
            }
            return Disposables.create()
        }
    }

    func readRx() -> Single<U> {
        Single.create { observer in
            self.read { value in
                self.callbackQueue.async { observer(.success(value)) }
            }
            return Disposables.create()
        }
    }

    var currentAndChanges: Observable<U> {
        readRx().asObservable() + onChange.flatMap { _ in self.readRx() }
    }
}
