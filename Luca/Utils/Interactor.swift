import Foundation
import RxSwift
/// Interactors consist of logic that interacts with UI
protocol Interactor {
    func interact() -> Completable
}
