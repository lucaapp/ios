import Foundation

protocol Currency {
    var code: String { get }
}

struct EUR: Currency {
    var code = "EUR"
}
struct USD: Currency {
    var code = "USD"
}

struct Money<C> where C: Currency {
    private(set) var amount: Decimal
    let currency: C

    /// Returns rounded `Money` instance to the resolution supported by the currency, eg. cents in EUR
    var rounded: Money<C> {
        let numberHandler = NSDecimalNumberHandler(roundingMode: .plain, scale: 2, raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: false)
        let roundedAmount =  NSDecimalNumber(decimal: amount).rounding(accordingToBehavior: numberHandler)
        return Money<C>(decimal: roundedAmount as Decimal, currency: self.currency)
    }

    private init(decimal: Decimal, currency: C) {
        self.amount = decimal
        self.currency = currency
    }

    init(double: Double, currency: C) {
        self.amount = NSNumber(value: double).decimalValue
        self.currency = currency
    }
    /// Creates new instance from string like "10,00". It is locale aware, so "10.00" wouldn't be accepted
    init?(stringWithLocale: String, currency: C) {
        let formatter = NumberFormatter()
        formatter.generatesDecimalNumbers = true
        formatter.numberStyle = NumberFormatter.Style.decimal
        guard let decimalAmount = formatter.number(from: stringWithLocale)?.decimalValue else { return nil }
        self.amount = decimalAmount
        self.currency = currency
    }

    /// Creates new instance from double string like "10.00".
    init?(string: String, currency: C) {
        guard let decimal = Decimal(string: string) else { return nil}
        amount = decimal
        self.currency = currency
    }

    /// Formatted string with specified currency code, e.g. "$181,45" for USD() Currency
    var formattedAmountWithCode: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencyCode = currency.code
        return formatter.string(from: rounded.amount as NSNumber) ?? ""
    }

    /// Formatted string without currency code, e.g. "181,45"
    var formattedAmountWithoutCode: String {
        Formatters.currencyAmountWithoutCodeFormatter.string(from: rounded.amount as NSNumber) ?? ""
    }

    func convert<T: Currency>(to: T, ratio: Double) -> Money<T> {
        let amount = self.amount * NSNumber(value: ratio).decimalValue
        return Money<T>(decimal: amount, currency: to)
    }
}

extension Money {

    /// Creates an Int32 value. It does round somehow.
    ///
    /// DO NOT USE `.intValue`. It contains a bug. See `MoneyTests` for further information
    var toInt32: Int32 {
        NSDecimalNumber(decimal: amount).int32Value
    }
}

extension Money: Comparable {

    // MARK: - +

    static func + (lhs: Self, rhs: Self) -> Money<C> {
        let amount = lhs.amount + rhs.amount
        return Money<C>(decimal: amount, currency: lhs.currency)
    }

    static func + (lhs: Self, rhs: Double) -> Money<C> {
        let amount = lhs.amount + NSNumber(value: rhs).decimalValue
        return Money<C>(decimal: amount, currency: lhs.currency)
    }

    static func + (lhs: Double, rhs: Self) -> Money<C> {
        let amount = NSNumber(value: lhs).decimalValue + rhs.amount
        return Money<C>(decimal: amount, currency: rhs.currency)
    }

    // MARK: - -

    static func - (lhs: Self, rhs: Self) -> Money<C> {
        let amount = lhs.amount - rhs.amount
        return Money<C>(decimal: amount, currency: lhs.currency)
    }

    static func - (lhs: Self, rhs: Double) -> Money<C> {
        let amount = lhs.amount - NSNumber(value: rhs).decimalValue
        return Money<C>(decimal: amount, currency: lhs.currency)
    }

    static func - (lhs: Double, rhs: Self) -> Money<C> {
        let amount = NSNumber(value: lhs).decimalValue - rhs.amount
        return Money<C>(decimal: amount, currency: rhs.currency)
    }

    // MARK: - *

    static func * (lhs: Self, rhs: Self) -> Money<C> {
        let amount = lhs.amount * rhs.amount
        return Money<C>(decimal: amount, currency: lhs.currency)
    }

    static func * (lhs: Self, rhs: Double) -> Money<C> {
        let amount = lhs.amount * NSNumber(value: rhs).decimalValue
        return Money<C>(decimal: amount, currency: lhs.currency)
    }

    static func * (lhs: Double, rhs: Self) -> Money<C> {
        let amount = NSNumber(value: lhs).decimalValue * rhs.amount
        return Money<C>(decimal: amount, currency: rhs.currency)
    }

    // MARK: - /

    static func / (lhs: Self, rhs: Self) -> Money<C> {
        let amount = lhs.amount / rhs.amount
        return Money<C>(decimal: amount, currency: lhs.currency)
    }

    static func / (lhs: Self, rhs: Double) -> Money<C> {
        let amount = lhs.amount / NSNumber(value: rhs).decimalValue
        return Money<C>(decimal: amount, currency: lhs.currency)
    }

    static func / (lhs: Double, rhs: Self) -> Money<C> {
        let amount = NSNumber(value: lhs).decimalValue / rhs.amount
        return Money<C>(decimal: amount, currency: rhs.currency)
    }

    // MARK: - ==

    static func == (lhs: Self, rhs: Self) -> Bool {
      lhs.amount == rhs.amount
    }

    static func == (lhs: Self, rhs: Double) -> Bool {
      lhs.amount == NSNumber(value: rhs).decimalValue
    }

    static func == (lhs: Double, rhs: Self) -> Bool {
        NSNumber(value: lhs).decimalValue == rhs.amount
    }

    // MARK: - >

    static func > (lhs: Self, rhs: Self) -> Bool {
      lhs.amount > rhs.amount
    }

    static func > (lhs: Self, rhs: Double) -> Bool {
        lhs.amount > NSNumber(value: rhs).decimalValue
    }

    static func > (lhs: Double, rhs: Self) -> Bool {
        NSNumber(value: lhs).decimalValue > rhs.amount
    }

    // MARK: - <

    static func < (lhs: Self, rhs: Self) -> Bool {
        lhs.amount < rhs.amount
    }

    static func < (lhs: Self, rhs: Double) -> Bool {
        lhs.amount < NSNumber(value: rhs).decimalValue
    }

    static func < (lhs: Double, rhs: Self) -> Bool {
        NSNumber(value: lhs).decimalValue < rhs.amount
    }
}
