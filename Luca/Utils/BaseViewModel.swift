import SwiftUI
import DependencyInjection
import RxSwift
import Combine

typealias AlertsSubject = CurrentValueSubject<AlertContent?, Never>

class BaseViewModel: ObservableObject {

    @Published var isProcessing: Bool = false
    @Published var processCount: Int = 0 {
        didSet {
            isProcessing = processCount > 0
        }
    }

    var alerts: AlertContent? {
        get { alertsSubjectUnderlying.value }
        set { alertsSubject.value = newValue }
    }

    /// Both Subjects are needed to handle the SwiftUI's way of refreshing views. It happens on `willSet`. Those subjects simulate this behaviour
    let alertsSubject = AlertsSubject(nil)
    private var alertsSubjectUnderlying = AlertsSubject(nil)
    private var cancellable: AnyCancellable?

    init() {
        cancellable = alertsSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] in
                self?.objectWillChange.send()
                self?.alertsSubjectUnderlying.value = $0
            }
    }
}

extension BaseViewModel {

    /// Creates binding from `alerts` property.
    var alertsBinding: Binding<AlertContent?> {
        Binding<AlertContent?>.from(self, keyPath: \.alerts, defaultValue: nil)
    }

    /// Creates binding from `isProcessing` property.
    var isProcessingBinding: Binding<Bool> {
        Binding<Bool>.init { [weak self] in
            guard let processCount = self?.processCount else { return false }
            return processCount > 0
        } set: { _ in }
    }

    var processCountBinding: Binding<Int> {
        Binding<Int>.from(self, keyPath: \.processCount, defaultValue: 0)
    }
}
