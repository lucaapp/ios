import Foundation
import RxSwift

/// Tool used to create events that will be invoked and awaited until completed, contrary to fire & forget events.
class DeferredEvent<T> {

    typealias Receiver = (T) -> Completable

    private var receivers: [AnyHashable: Receiver] = [:]
    private let timeout: Double?

    private init(timeout: Double? = nil) {
        self.timeout = timeout
    }

    func addListener(for key: AnyHashable, receiver: @escaping Receiver) {
        receivers[key] = receiver
    }

    func removeListener(for key: AnyHashable) {
        receivers.removeValue(forKey: key)
    }

    static func create(timeout: Double? = nil) -> Invoker {
        let event = DeferredEvent<T>(timeout: timeout)
        return Invoker(event: event)
    }

    class Invoker {
        let event: DeferredEvent<T>

        fileprivate init(event: DeferredEvent<T>) {
            self.event = event
        }

        /// Invokes the listeners and awaits until completed.
        ///
        /// It will respect the timout if any given. It will circumvent any error thrown by the listeners.
        ///
        /// - parameter value: value to call the event with.
        func invoke(value: T) -> Completable {
            var completables = event.receivers.map { $0.value(value).onErrorComplete() }
            if let timeout = event.timeout {
                completables = completables.map {
                    $0.timeout(.milliseconds(Int(timeout * 1000.0)), scheduler: SerialDispatchQueueScheduler(qos: .userInteractive))
                        .onErrorComplete()
                }
            }

            return Completable.zip(completables)
        }
    }
}
