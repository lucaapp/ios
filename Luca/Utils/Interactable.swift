import Foundation
import UIKit

/// It represents elements that can be interacted with. Not necessarily restricted to UI.
protocol Interactable {
    var interactableTag: String { get }
    func interact()
}

protocol HasInteractables {
    associatedtype InteractableKeys
    var interactableKeys: InteractableKeys { get }
    var interactables: [Interactable] { get }
}

extension HasInteractables {

    func retrieveInteractable(with key: KeyPath<InteractableKeys, String>) -> Interactable? {
        let key = self.interactableKeys[keyPath: key]
        return interactables.filter { $0.interactableTag == key }.first
    }
}
