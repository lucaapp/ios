import Foundation

class TransactionOperation: Operation {
    private let lockQueue = DispatchQueue(label: "de.luca.transactionOperationLock", attributes: .concurrent)
    private let block: (@escaping () -> Void) -> Void

    init(_ block: @escaping (@escaping () -> Void) -> Void) {
        self.block = block
    }

    override var isAsynchronous: Bool {
        return true
    }

    private var _isExecuting: Bool = false
    override private(set) var isExecuting: Bool {
        get {
            return lockQueue.sync { () -> Bool in
                return _isExecuting
            }
        }
        set {
            willChangeValue(forKey: "isExecuting")
            lockQueue.sync(flags: [.barrier]) {
                _isExecuting = newValue
            }
            didChangeValue(forKey: "isExecuting")
        }
    }

    private var _isFinished: Bool = false
    override private(set) var isFinished: Bool {
        get {
            return lockQueue.sync { () -> Bool in
                return _isFinished
            }
        }
        set {
            willChangeValue(forKey: "isFinished")
            lockQueue.sync(flags: [.barrier]) {
                _isFinished = newValue
            }
            didChangeValue(forKey: "isFinished")
        }
    }

    override func start() {
        isFinished = false
        isExecuting = true
        main()
    }

    override func main() {
        block {
            self.finish()
        }
    }

    func finish() {
        isExecuting = false
        isFinished = true
    }
}
