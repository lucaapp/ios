import Foundation
import RxSwift

public class AsyncDataOperation<ErrorType: Error, Result> {

    /// - returns: cancel token. Operation will be canceled if executed
    @discardableResult
    func execute(completion: @escaping (Result) -> Void, failure: @escaping (ErrorType) -> Void) -> (() -> Void) {
        fatalError("Not implemented")
    }

}

extension AsyncDataOperation {
    func asSingle() -> Single<Result> {
        Single<Result>.create { (observer) -> Disposable in
            let execution = self.execute { result in
                observer(.success(result))
            } failure: { (error) in
                observer(.failure(error))
            }

            return Disposables.create {
                execution()
            }
        }
    }

    /// Generates an infallible with `Swift.Result` which contains the result of the operation or typed error. It emits always only one element.
    func asInfallibleResult() -> Infallible<Swift.Result<Result, ErrorType>> {
        Infallible<Swift.Result<Result, ErrorType>>.create { observer in
            let execution = self.execute { result in
                observer(.next(Swift.Result.success(result)))
                observer(.completed)
            } failure: { error in
                observer(.next(Swift.Result.failure(error)))
                observer(.completed)
            }
            return Disposables.create {
                execution()
            }
        }
    }
}
