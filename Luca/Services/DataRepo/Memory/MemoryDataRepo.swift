import Foundation
import RxSwift

class MemoryDataRepo<Model>: DataRepo<Model> where Model: DataRepoModel & Codable {
    private let dispatchQueue: DispatchQueue
    private let callbacksDispatchQueue: DispatchQueue

    private var data: [Int: Model] = [:]

    var debug: Bool = false

    override init() {
        dispatchQueue = DispatchQueue(label: "Memory.\(String(describing: Self.self))", qos: .background)
        callbacksDispatchQueue = DispatchQueue(label: "Memory.\(String(describing: Self.self)).callbacks", qos: .background)
        super.init()
    }

    deinit {
        if debug { self.log("deinit") }
    }

    override func restore(completion: @escaping([Model]) -> Void, failure: @escaping ErrorCompletion) {
        dispatchQueue.async {
            if self.debug { self.log("restore", entryType: .debug) }
            let retVal = Array(self.data.values)
            self.callbacksDispatchQueue.async { completion(retVal) }
        }
    }

    override func remove(identifiers: [Int], completion: @escaping() -> Void, failure: @escaping ErrorCompletion) {
        if debug { self.log("remove \(identifiers)", entryType: .debug) }
        dispatchQueue.async(flags: .barrier) {
            if identifiers.isEmpty {
                self.callbacksDispatchQueue.async { completion() }
                return
            }
            for id in identifiers {
                self.data.removeValue(forKey: id)
            }
            self.callbacksDispatchQueue.async {
                completion()
                NotificationCenter.default.post(name: NSNotification.Name(self.onDataChanged), object: self)
            }
        }
    }

    override func removeAll(completion: @escaping() -> Void, failure: @escaping ErrorCompletion) {
        dispatchQueue.async(flags: .barrier) {
            if self.debug { self.log("removeAll", entryType: .debug) }
            self.data = [:]
            self.callbacksDispatchQueue.async {
                completion()
                NotificationCenter.default.post(name: NSNotification.Name(self.onDataChanged), object: self)
            }
        }
    }

    override func store(object: Model, completion: @escaping (Model) -> Void, failure: @escaping ErrorCompletion) {
        dispatchQueue.async(flags: .barrier) {
            if self.debug { self.log("store \(object)", entryType: .debug) }
            let model = self.store(object: object)
            self.callbacksDispatchQueue.async {
                completion(model)
                NotificationCenter.default.post(name: NSNotification.Name(self.onDataChanged), object: self)
            }
        }
    }

    override func store(objects: [Model], completion: @escaping ([Model]) -> Void, failure: @escaping ErrorCompletion) {
        if debug { self.log("store \(objects)", entryType: .debug) }
        if objects.isEmpty {
            callbacksDispatchQueue.async { completion(objects) }
            return
        }
        dispatchQueue.async(flags: .barrier) {
            var retVals: [Model] = []
            for object in objects {
                retVals.append(self.store(object: object))
            }
            self.callbacksDispatchQueue.async {
                completion(retVals)
                NotificationCenter.default.post(name: NSNotification.Name(self.onDataChanged), object: self)
            }
        }
    }

    private func store(object: Model) -> Model {
        if let identifier = object.identifier {
            data[identifier] = object
            return object
        } else {
            var identifier = object.identifier ?? 0
            while identifier == 0 || data[identifier] != nil {
                identifier = Int.random(in: Int.min..<Int.max)
            }
            var mutableObject = object
            mutableObject.identifier = identifier
            data[identifier] = mutableObject
            return mutableObject
        }
    }

    func serialize() throws -> Data {
        try JSONEncoder().encode(data)
    }

    func deserialize(data: Data) throws {
        self.data = try JSONDecoder().decode([Int: Model].self, from: data)
    }
}

extension MemoryDataRepo: UnsafeAddress, LogUtil {}
