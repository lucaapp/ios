import Foundation
import RealmSwift
import RxSwift

class RealmSaveModel<Model>: Object where Model: DataRepoModel {

    let identifier = RealmOptional<Int>()

    override static func primaryKey() -> String? {
        return "identifier"
    }

    /// Populates this instance from the model
    func populate(from: Model) {
    }

    /// Creates empty result instance
    func create() -> Model {
        fatalError()
    }

    /// Populates the result model from this instance
    var model: Model {
        var m = create()
        m.identifier = identifier.value
        return m
    }
}

class RealmDataRepo<SaveModel, Model>: DataRepo<Model>, MemoryWarningHandler where SaveModel: RealmSaveModel<Model> {

    private let dispatchQueue: DispatchQueue
    private let callbacksDispatchQueue: DispatchQueue
    private let configuration: Realm.Configuration
    private var cachedData: [Model]?

    var debug: Bool = false

    ///
    /// - Parameters:
    ///   - filenameSalt: The hash of this string will be the database filename.
    ///   - schemaVersion: Version of the schema. Migration callback will be triggered if the schema version won't match.
    ///   - migrationBlock: Callback that handles the migration.
    ///   - encryptionKey: If nil, data won't be encrypted. The key should be 64 bytes long.
    init(filenameSalt: String, schemaVersion: UInt64, migrationBlock: MigrationBlock? = nil, encryptionKey: Data? = nil) {

        dispatchQueue = DispatchQueue(label: "Realm.\(String(describing: Self.self))", qos: .background)
        callbacksDispatchQueue = DispatchQueue(label: "Realm.\(String(describing: Self.self)).callbacks", qos: .background)

        let hashedName = filenameSalt.data(using: .utf8)!.crc32().toHexString()

        // All various repos must be saved in separate files to be able to handle schema versions independently
        let defaultRealmDirectory = Realm.Configuration.defaultConfiguration.fileURL!.deletingLastPathComponent()
        let fileURL = defaultRealmDirectory.appendingPathComponent("\(hashedName).realm")
        configuration = Realm.Configuration(
            fileURL: fileURL,
            encryptionKey: encryptionKey,
            schemaVersion: schemaVersion,
            migrationBlock: migrationBlock,
            objectTypes: [SaveModel.self])

        super.init()
    }

    deinit {
        if debug { self.log("deinit") }
    }

    func createSaveModel() -> SaveModel {
        fatalError()
    }

    private func createRealm() throws -> Realm {
        if debug { self.log("createRealm", entryType: .debug) }
        let realm = try Realm(configuration: configuration)
        var realmURL = realm.configuration.fileURL
        realmURL?.excludeFromCloudBackups()
        return realm
    }

    override func restore(completion: @escaping([Model]) -> Void, failure: @escaping ErrorCompletion) {
        if debug { self.log("restore", entryType: .debug) }
        dispatchQueue.async {
            if let cachedData = self.cachedData {
                if self.debug { self.log("Use cache", entryType: .debug) }
                self.callbacksDispatchQueue.async { completion(cachedData) }
                return
            }
            var models: [Model] = []
            do {
                try autoreleasepool {
                    let realm = try self.createRealm()
                    models = Array(realm.objects(SaveModel.self).map { $0.model })
                }
                self.dispatchQueue.async(flags: .barrier) { self.cachedData = models }
                self.callbacksDispatchQueue.async { completion(models) }

            } catch let error {
                self.callbacksDispatchQueue.async { failure(error) }
            }
        }
    }

    override func remove(identifiers: [Int], completion: @escaping() -> Void, failure: @escaping ErrorCompletion) {
        if debug { self.log("remove \(identifiers)", entryType: .debug) }
        dispatchQueue.async(flags: .barrier) {
            if identifiers.isEmpty {
                self.callbacksDispatchQueue.async { completion() }
                return
            }
            self.invalidateCache()
            do {
                try autoreleasepool {
                    let realm = try self.createRealm()
                    let models = realm.objects(SaveModel.self).filter { identifiers.contains($0.identifier.value ?? -1) }
                    try realm.write { realm.delete(models) }
                }

                self.callbacksDispatchQueue.async {
                    completion()
                    NotificationCenter.default.post(name: NSNotification.Name(self.onDataChanged), object: self)
                }
            } catch let error {
                self.callbacksDispatchQueue.async { failure(error) }
            }
        }
    }

    override func removeAll(completion: @escaping() -> Void, failure: @escaping ErrorCompletion) {
        if debug { self.log("removeAll", entryType: .debug) }
        dispatchQueue.async(flags: .barrier) {
            self.invalidateCache()
            do {
                try autoreleasepool {
                    let realm = try self.createRealm()
                    try realm.write { realm.deleteAll() }
                }

                self.callbacksDispatchQueue.async {
                    completion()
                    NotificationCenter.default.post(name: NSNotification.Name(self.onDataChanged), object: self)
                }
            } catch let error {
                self.callbacksDispatchQueue.async { failure(error) }
            }
        }
    }

    override func store(object: Model, completion: @escaping (Model) -> Void, failure: @escaping ErrorCompletion) {
        if debug { self.log("store \(object)", entryType: .debug) }
        dispatchQueue.async(flags: .barrier) {
            self.invalidateCache()
            var model: Model?
            do {
                try autoreleasepool {
                    let realm = try self.createRealm()
                    model = try self.store(object: object, realm: realm)
                }

                self.callbacksDispatchQueue.async {
                    if let model = model {
                        completion(model)
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(self.onDataChanged), object: self)
                }
            } catch let error {
                self.callbacksDispatchQueue.async { failure(error) }
            }
        }
    }

    override func store(objects: [Model], completion: @escaping ([Model]) -> Void, failure: @escaping ErrorCompletion) {
        if debug { self.log("store \(objects)", entryType: .debug) }
        if objects.isEmpty {
            callbacksDispatchQueue.async { completion(objects) }
            return
        }
        dispatchQueue.async(flags: .barrier) {
            self.invalidateCache()
            var retVals: [Model] = []
            do {
                try autoreleasepool {
                    let realm = try self.createRealm()

                    for object in objects {
                        retVals.append(try self.store(object: object, realm: realm))
                    }
                }

                self.callbacksDispatchQueue.async {
                    completion(retVals)
                    NotificationCenter.default.post(name: NSNotification.Name(self.onDataChanged), object: self)
                }
            } catch let error {
                self.callbacksDispatchQueue.async { failure(error) }
            }
        }
    }

    private func store(object: Model, realm: Realm) throws -> Model {
        if let identifier = object.identifier,
           let foundObject = realm.object(ofType: SaveModel.self, forPrimaryKey: identifier) {
            try realm.write {
                foundObject.populate(from: object)
                realm.add(foundObject, update: .modified)
            }
            return foundObject.model
        } else {
            let model = self.createSaveModel()
            if object.identifier != nil {
                model.identifier.value = object.identifier
            } else {
                let nowInMS = UInt64(1000.0 * Date.now.timeIntervalSince1970) // precision to ms
                let hashSeed = nowInMS.data +
                    realm.objects(SaveModel.self).count.data +
                    Int.random(in: 0..<10000000).data
                model.identifier.value = Int(hashSeed.crc32)
            }
            try realm.write {
                model.populate(from: object)
                realm.add(model, update: .modified)
            }
            return model.model
        }
    }

    private func invalidateCache() {
        if debug { log("Invalidate cache", entryType: .debug) }
        cachedData = nil
    }

    func onMemoryWarning() {
        dispatchQueue.async(flags: .barrier) {
            self.invalidateCache()
        }
    }
}

extension RealmDataRepo: RealmDatabaseUtils {

    func removeFile(completion: @escaping() -> Void, failure: @escaping ErrorCompletion) {
        if debug { self.log("removeFile", entryType: .debug) }
        dispatchQueue.async(flags: .barrier) {
            self.invalidateCache()
            do {
                if FileManager.default.fileExists(atPath: self.configuration.fileURL!.path) {
                    try FileManager.default.removeItem(at: self.configuration.fileURL!)
                }
                self.callbacksDispatchQueue.async { completion() }
            } catch let error {
                self.callbacksDispatchQueue.async { failure(error) }
            }
        }
    }

    func changeEncryptionSettings(oldKey: Data?, newKey: Data?, completion: @escaping () -> Void, failure: @escaping ErrorCompletion) {
        dispatchQueue.async(flags: .barrier) {
            if self.debug { self.log("changeEncryptionSettings", entryType: .debug) }
            if let oldKey = oldKey, oldKey.count != 64 {
                self.callbacksDispatchQueue.async { failure(NSError(domain: "Old key has invalid length", code: 0, userInfo: nil)) }
                return
            }
            if let newKey = newKey, newKey.count != 64 {
                self.callbacksDispatchQueue.async { failure(NSError(domain: "New key has invalid length", code: 0, userInfo: nil)) }
                return
            }

            // If the database has not been created yet then just return. New settings will be applied with the creation
            if !FileManager.default.fileExists(atPath: self.configuration.fileURL!.path) {
                print("DATABASE RESET: Path does not exist: \(self.configuration.fileURL!.path)")
                self.callbacksDispatchQueue.async { completion() }
                return
            } else {
                print("DATABASE RESET: Path DOES exist: \(self.configuration.fileURL!.path)")
            }

            // Rename current database to a backup one
            let backupURL = self.backupURL()
            do {
                try FileManager.default.moveItem(at: self.configuration.fileURL!, to: backupURL)
            } catch let error {
                self.callbacksDispatchQueue.async { failure(error) }
                return
            }

            // Create configuration with backup file URL and old key
            var oldConfiguration = self.realmConfiguration(with: oldKey)
            oldConfiguration.fileURL = backupURL

            // Create configuration with the same URL as the initial DB and a new key
            let newConfiguration = self.realmConfiguration(with: newKey)

            self.invalidateCache()
            do {
                let realm = try Realm(configuration: oldConfiguration)
                try self.deleteFile(at: self.configuration.fileURL!)
                try realm.writeCopy(toFile: self.configuration.fileURL!, encryptionKey: newKey)
                try self.deleteFile(at: backupURL)
                self.callbacksDispatchQueue.async { completion() }
            } catch let error {
                self.log("Failed to change encryption settings: \(error). Restoring backup file.", entryType: .error)
                do {
                    try FileManager.default.removeItem(at: newConfiguration.fileURL!)
                    try FileManager.default.moveItem(at: backupURL, to: self.configuration.fileURL!)
                } catch let e {
                    self.callbacksDispatchQueue.async { failure(e) }
                    return
                }
                self.callbacksDispatchQueue.async { failure(error) }
            }
        }
    }

    private func backupURL() -> URL {
        let backupFilename = self.configuration.fileURL!.lastPathComponent.replacingOccurrences(of: ".realm", with: "_backup.realm")
        return self.configuration.fileURL!.deletingLastPathComponent().appendingPathComponent(backupFilename)
    }

    private func realmConfiguration(with encryptionKey: Data?) -> Realm.Configuration {
        var configuration = self.configuration
        configuration.encryptionKey = encryptionKey
        return configuration
    }

    private func deleteFile(at url: URL) throws {
        if FileManager.default.fileExists(atPath: url.path) {
            try FileManager.default.removeItem(at: url)
        }
    }
}

extension RealmDataRepo: UnsafeAddress, LogUtil {

}
