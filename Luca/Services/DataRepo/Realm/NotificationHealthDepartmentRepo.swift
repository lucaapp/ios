import Foundation
import RealmSwift

struct NotificationHealthDepartment: Codable, Hashable {
    var uuid: String
    var name: String?
    var email: String?
    var phone: String?
    var config: MessagesConfig?
}

extension NotificationHealthDepartment: DataRepoModel {
    var identifier: Int? {
        get {
            let checksum = uuid.data(using: .utf8)!.crc32

            return Int(checksum)
        }
        set { }
    }
}

class NotificationHealthDepartmentRealmModel: RealmSaveModel<NotificationHealthDepartment> {

    @objc dynamic var uuid = ""
    @objc dynamic var name: String?
    @objc dynamic var email: String?
    @objc dynamic var phone: String?

    override func create() -> NotificationHealthDepartment {
        return NotificationHealthDepartment(uuid: "")
    }

    override func populate(from: NotificationHealthDepartment) {
        super.populate(from: from)
        uuid = from.uuid
        name = from.name
        email = from.email
        phone = from.phone
    }

    override var model: NotificationHealthDepartment {
        var m = super.model
        m.uuid = uuid
        m.name = name
        m.phone = phone
        m.email = email
        return m
    }
}

class NotificationHealthDepartmentRepo: RealmDataRepo<NotificationHealthDepartmentRealmModel, NotificationHealthDepartment> {
    override func createSaveModel() -> NotificationHealthDepartmentRealmModel {
        return NotificationHealthDepartmentRealmModel()
    }

    init(key: Data) {
        super.init(filenameSalt: "NotificationHealthDepartmentRepo", schemaVersion: 0, encryptionKey: key)
    }
}
