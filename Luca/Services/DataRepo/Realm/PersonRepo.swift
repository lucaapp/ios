import Foundation
import RealmSwift

class PersonRealmModel: RealmSaveModel<Person> {

    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var type: String = ""

    let traceInfos = List<Int>()

    override func create() -> Person {
        return Person(firstName: "", lastName: "", type: .unknown)
    }

    override func populate(from: Person) {
        super.populate(from: from)

        firstName = from.firstName
        lastName = from.lastName
        type = from.type.rawValue
        traceInfos.removeAll()
        traceInfos.append(objectsIn: from.traceInfoIDs)
    }

    override var model: Person {
        var m = super.model

        m.firstName = firstName
        m.lastName = lastName
        m.type = PersonType(rawValue: type) ?? .child
        m.traceInfoIDs = Array(traceInfos)
        return m
    }
}

class PersonRepo: RealmDataRepo<PersonRealmModel, Person> {
    override func createSaveModel() -> PersonRealmModel {
        return PersonRealmModel()
    }

    init(key: Data) {
        super.init(filenameSalt: "PersonRepo", schemaVersion: 1, migrationBlock: { (migration, schemaVersion) in
            if schemaVersion < 1 {
                migration.enumerateObjects(ofType: HistoryEntryRealmModel.className()) { _, newObject in
                    newObject?["type"] = PersonType.child.rawValue
                }
            }
        }, encryptionKey: key)
    }
}
