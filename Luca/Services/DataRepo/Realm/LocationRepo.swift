import Foundation
import RealmSwift

class LocationRealmModel: RealmSaveModel<Location> {

    @objc dynamic var locationId: String = ""
    @objc dynamic var publicKey: String?
    @objc dynamic var radius: Double = 0.0
    @objc dynamic var locationName: String?
    @objc dynamic var groupName: String?
    @objc dynamic var firstName: String?
    @objc dynamic var lastName: String?
    @objc dynamic var phone: String?
    @objc dynamic var streetName: String?
    @objc dynamic var streetNr: String?
    @objc dynamic var zipCode: String?
    @objc dynamic var city: String?
    @objc dynamic var state: String?
    @objc dynamic var entryPolicy: String?
    @objc dynamic var urls: Data?

    var lat = RealmOptional<Double>()
    var lng = RealmOptional<Double>()
    var startsAt = RealmOptional<Int>()
    var endsAt = RealmOptional<Int>()
    var isPrivate = RealmOptional<Bool>()
    var isContactDataMandatory = RealmOptional<Bool>()
    var averageCheckinTime = RealmOptional<Int>()

    override func create() -> Location {
        return Location(locationId: "", radius: 0)
    }

    override func populate(from: Location) {
        super.populate(from: from)

        locationId = from.locationId
        publicKey = from.publicKey
        radius = from.radius
        locationName = from.locationName
        groupName = from.groupName
        firstName = from.firstName
        lastName = from.lastName
        phone = from.phone
        streetName = from.streetName
        streetNr = from.streetNr
        zipCode = from.zipCode
        city = from.city
        state = from.state
        entryPolicy = from.entryPolicy
        urls = try? JSONEncoder().encode(from.urls)

        lat.value = from.lat
        lng.value = from.lng
        startsAt.value = from.startsAt
        endsAt.value = from.endsAt
        isPrivate.value = from.isPrivate
        isContactDataMandatory.value = from.isContactDataMandatory
        averageCheckinTime.value = from.averageCheckinTime
    }

    override var model: Location {
        var m = super.model

        m.locationId = locationId
        m.publicKey = publicKey
        m.radius = radius
        m.groupName = groupName
        m.locationName = locationName
        m.firstName = firstName
        m.lastName = lastName
        m.phone = phone
        m.streetName = streetName
        m.streetNr = streetNr
        m.zipCode = zipCode
        m.city = city
        m.state = state
        m.entryPolicy = entryPolicy

        m.lat = lat.value
        m.lng = lng.value
        m.startsAt = startsAt.value
        m.endsAt = endsAt.value
        m.isPrivate = isPrivate.value
        m.isContactDataMandatory = isContactDataMandatory.value
        m.urls = try? JSONDecoder().decode([LocationURL].self, from: urls ?? Data())
        m.averageCheckinTime = averageCheckinTime.value
        return m
    }
}

class LocationRepo: RealmDataRepo<LocationRealmModel, Location> {
    override func createSaveModel() -> LocationRealmModel {
        return LocationRealmModel()
    }

    init(key: Data) {
        super.init(filenameSalt: "LocationRepo", schemaVersion: 6, migrationBlock: { (migration, schemaVersion) in
            if schemaVersion < 1 {
                migration.enumerateObjects(ofType: LocationRealmModel.className()) { _, newObject in
                    newObject?["isPrivate"] = nil
                }
            }
            if schemaVersion < 2 {
                migration.enumerateObjects(ofType: LocationRealmModel.className()) { _, newObject in
                    newObject?["isContactDataMandatory"] = nil
                }
            }
            if schemaVersion < 3 {
                migration.enumerateObjects(ofType: LocationRealmModel.className()) { _, newObject in
                    newObject?["entryPolicy"] = nil
                    newObject?["averageCheckinTime"] = nil
                }
            }
            if schemaVersion < 4 {
                migration.enumerateObjects(ofType: LocationRealmModel.className()) { _, newObject in
                    newObject?["urls"] = nil
                }
            }
            if schemaVersion < 5 {
                // name property will be automatically removed
            }
            if schemaVersion < 6 {
                migration.enumerateObjects(ofType: LocationRealmModel.className()) { oldObject, newObject in
                    newObject?["publicKey"] = oldObject?["publicKey"]
                }
            }
        }, encryptionKey: key)
    }
}
