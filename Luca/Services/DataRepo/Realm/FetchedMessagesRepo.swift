import Foundation
import RealmSwift

struct FetchedMessage {
    var healthDepartmentId: String

    var messageId: String

    var message: FetchedMessageContent

    /// Date when this data set has been created on the backend.
    var createdAt: Date

    /// Date when this message has been seen by the user.
    var readDate: Date?
}

extension FetchedMessage: DataRepoModel {
    var identifier: Int? {
        get {
            Int((messageId.data(using: .utf8) ?? Data()).crc32)
        }
        set {}
    }
}

class FetchedMessageRealmModel: RealmSaveModel<FetchedMessage> {
    @objc dynamic var healthDepartmentId: String = ""
    @objc dynamic var messageId: String = ""
    @objc dynamic var message: Data?
    @objc dynamic var createdAt: Date = Date()
    @objc dynamic var readDate: Date?

    override func create() -> FetchedMessage {
        return FetchedMessage(healthDepartmentId: "", messageId: "", message: FetchedMessageContent(title: "", message: ""), createdAt: Date())
    }

    override func populate(from: FetchedMessage) {
        super.populate(from: from)

        healthDepartmentId = from.healthDepartmentId
        messageId = from.messageId
        message = try? JSONEncoder().encode(from.message)
        createdAt = from.createdAt
        readDate = from.readDate
    }

    override var model: FetchedMessage {
        var m = super.model
        m.healthDepartmentId = healthDepartmentId
        m.messageId = messageId
        m.createdAt = createdAt
        m.readDate = readDate
        if let message = message,
           let content = try? JSONDecoder().decode(FetchedMessageContent.self, from: message) {
            m.message = content
        }
        return m
    }
}

class FetchedMessageRepo: RealmDataRepo<FetchedMessageRealmModel, FetchedMessage> {
    override func createSaveModel() -> FetchedMessageRealmModel {
        return FetchedMessageRealmModel()
    }

    init(key: Data) {
        super.init(filenameSalt: "FetchedMessagesRepo", schemaVersion: 0, encryptionKey: key)
    }
}
