import Foundation
import RealmSwift

enum LocalNewsType: Int {
    case lucaConnectZipCodeUsageConsent = 1
    case lucaConnectActivate = 2
    case newTermsOfUse = 3
    case idIdentPending = 4
    case idIdentSuccess = 5
    case idIdentFailure = 6
    case paymentRevocation = 7
    case custom
}

extension LocalNewsType {
    var title: String? {
        switch self {
        case .custom: return nil
        case .lucaConnectZipCodeUsageConsent: return L10n.IOSApp.Postcode.Check.Toggle.title
        case .lucaConnectActivate: return L10n.IOSApp.Luca.Connect.HealthDepartment.Active.title
        case .idIdentPending: return L10n.IOSApp.Id.idRequestSuccessHeadline
        case .newTermsOfUse: return L10n.IOSApp.TermsOfUse.Notification.title
        case .idIdentSuccess: return L10n.IOSApp.Id.idVerificationSuccessHeadline
        case .idIdentFailure: return L10n.IOSApp.Id.idVerificationFailureHeadline
        case .paymentRevocation: return L10n.UserApp.Pay.Revocation.Notification.title
        }
    }
    var message: String? {
        switch self {
        case .custom: return nil
        case .lucaConnectZipCodeUsageConsent: return L10n.IOSApp.Postcode.Check.Toggle.footer
        case .lucaConnectActivate: return L10n.IOSApp.Luca.Connect.HealthDepartment.Active.description
        case .idIdentPending: return L10n.IOSApp.Id.idGenericStatusShortText
        case .newTermsOfUse: return L10n.IOSApp.TermsOfUse.Notification.description
        case .idIdentSuccess: return L10n.IOSApp.Id.IdVerificationSuccessText.short
        case .idIdentFailure: return L10n.IOSApp.Id.idVerificationFailureText
        case .paymentRevocation: return L10n.UserApp.Pay.Revocation.Notification.text
        }
    }

}

struct LocalNewsMessage: DataRepoModel {
    var identifier: Int?
    var type: LocalNewsType
    var customTitle: String?
    var customMessage: String?
    var createDate: Date
    var readDate: Date?
}

extension LocalNewsMessage {
    var title: String {
        type.title ?? customTitle ?? ""
    }
    var message: String {
        type.message ?? customMessage ?? ""
    }
}

class LocalNewsMessageRealmModel: RealmSaveModel<LocalNewsMessage> {
    @objc dynamic var type: Int = 0
    @objc dynamic var customTitle: String?
    @objc dynamic var customMessage: String?
    @objc dynamic var createDate: Date = Date()
    @objc dynamic var readDate: Date?

    override func create() -> LocalNewsMessage {
        return LocalNewsMessage(type: .custom, createDate: Date())
    }

    override func populate(from: LocalNewsMessage) {
        super.populate(from: from)
        type = from.type.rawValue
        customTitle = from.customTitle
        customMessage = from.customMessage
        createDate = from.createDate
        readDate = from.readDate
    }

    override var model: LocalNewsMessage {
        var m = super.model
        m.type = LocalNewsType(rawValue: type) ?? .custom
        m.customTitle = customTitle
        m.customMessage = customMessage
        m.createDate = createDate
        m.readDate = readDate
        return m
    }

}

class LocalNewsMessageRepo: RealmDataRepo<LocalNewsMessageRealmModel, LocalNewsMessage> {

    override func createSaveModel() -> LocalNewsMessageRealmModel {
        return LocalNewsMessageRealmModel()
    }

    init(key: Data) {
        super.init(filenameSalt: "LocalNewsMessageRepo", schemaVersion: 0, encryptionKey: key)
    }

}
