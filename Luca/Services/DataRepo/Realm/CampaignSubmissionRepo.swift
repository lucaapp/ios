import Foundation
import RealmSwift

struct CampaignSubmissionData: Codable {
    var paymentId: String
    var submission: CampaignSubmission
}

extension CampaignSubmissionData: DataRepoModel {

    var identifier: Int? {
        get {
            Int((submission.uuid.data(using: .utf8) ?? Data()).crc32)
        }
        set {}
    }

}

class CampaignSubmissionRealmModel: RealmSaveModel<CampaignSubmissionData> {

    @objc dynamic var uuid: String = ""
    @objc dynamic var consumerId: String = ""
    @objc dynamic var paymentId: String = ""
    @objc dynamic var internalPaymentId: String = ""
    @objc dynamic var campaign: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var language: String = ""
    @objc dynamic var revocationSecret: String = ""
    @objc dynamic var createdAt: Double = 0.0

    override func create() -> CampaignSubmissionData {
        return CampaignSubmissionData(paymentId: "", submission: CampaignSubmission(uuid: "", consumerId: "", paymentId: "", campaign: .unknown, email: "", language: "", revocationSecret: "", createdAt: 0.0))
    }

    override func populate(from: CampaignSubmissionData) {
        super.populate(from: from)
        paymentId = from.paymentId
        uuid = from.submission.uuid
        consumerId = from.submission.consumerId
        internalPaymentId = from.submission.paymentId
        campaign = from.submission.campaign.rawValue
        email = from.submission.email
        language = from.submission.language
        revocationSecret = from.submission.revocationSecret
        createdAt = from.submission.createdAt
    }

    override var model: CampaignSubmissionData {
        var m = super.model
        m.paymentId = paymentId
        m.submission = CampaignSubmission(
            uuid: uuid,
            consumerId: consumerId,
            paymentId: internalPaymentId,
            campaign: CampaignType(rawValue: campaign) ?? .unknown,
            email: email,
            language: language,
            revocationSecret: revocationSecret,
            createdAt: createdAt
        )
        return m
    }

}
class CampaignSubmissionRepo: RealmDataRepo<CampaignSubmissionRealmModel, CampaignSubmissionData> {

    override func createSaveModel() -> CampaignSubmissionRealmModel {
        return CampaignSubmissionRealmModel()
    }

    init(key: Data) {
        super.init(filenameSalt: "CampaignSubmissionDataRepo", schemaVersion: 0, encryptionKey: key)
    }

}
