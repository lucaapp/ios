import Foundation

extension Numeric {

    /// Returns raw data bytes
    var data: Data {
        var retVal = Data()
        var selfCopy = self
        Swift.withUnsafeMutableBytes(of: &selfCopy) { retVal.append(contentsOf: $0) }
        return retVal
    }
}
