import Foundation
import RxSwift
import DependencyInjection

class LucaIdProcessFetcherService: Toggleable {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.lucaIDService) private var lucaIDService
    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.localNewsMessageRepo) private var localMessagesRepo

    private var disposeBag: DisposeBag?

    var isEnabled: Bool {
        disposeBag != nil
    }

    func enable() {
        if isEnabled {
            return
        }

        let newDisposeBag = DisposeBag()

        let updateSignal = Observable.merge(
            lucaPreferences.currentAndChanges(\.idProcessStarted).map { _ in Void() },
            UIApplication.shared.rx
            .currentAndChangedAppState
                .filter { $0 == .active || $0 == .background }
            .map { _ in Void() })

        #if PRODUCTION
        let timerInterval = 300
        #else
        let timerInterval = 15
        #endif

        updateSignal
            .flatMapLatest {
                self.lucaPreferences.get(\.uuid)
                    .flatMapCompletable { uuid in
                        if uuid == nil { return Completable.empty() }
                        return Observable<Int>.timer(.seconds(0), period: .seconds(timerInterval), scheduler: LucaScheduling.backgroundScheduler)
                            .flatMap { _ in self.lucaIDService.pollAfterDate }
                            .filter { self.timeProvider.now >= $0 }
                            .flatMap { _ in self.lucaIDService.fetchState() }
                            .onErrorComplete()
                            .ignoreElementsAsCompletable()
                    }
            }
            .subscribe()
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

    func disable() {
        disposeBag = nil
    }

}

extension LucaIdProcessFetcherService: UnsafeAddress, LogUtil {}
