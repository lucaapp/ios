import Foundation
import DependencyInjection
import RxSwift
import JOSESwift
import SwiftJWT
import LocalAuthentication
import CryptorECC
import Base58Swift

struct LucaIDParsedData: Equatable {
    var state: LucaIDProcess
    var minimalIdentityJWT: JWT<LucaIDMinimalIdentityClaims>?
    var identityJWT: JWT<LucaIDIdentityClaims>?
    var faceJWT: JWT<LucaIDImageClaims>?
    var receiptJWS: JWT<LucaIDReceiptClaims>?
}
extension LucaIDParsedData {

    static func == (lhs: LucaIDParsedData, rhs: LucaIDParsedData) -> Bool {
        return lhs.state == rhs.state
    }

}

/// Set it to true if you want to override the ident process.
/// False if you want to use/test the complete ident process.
#if PRODUCTION
private let mockIdentProcess = false
#else
private let mockIdentProcess = false
#endif

class LucaIDService: SharedProviderConnectionEventHandler {
    @InjectStatic(\.lucaIDKeysBundle) private var keysBundle
    @InjectStatic(\.backendLucaID) private var backend
    @InjectStatic(\.attestationService) private var attestationService
    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.keyValueRepo) private var keyValueRepo

    private static let requestResultDelayKey = "requestResultDelay"
    private static let idProcessStartedKey = "idProcessStarted"

    private let lucaIDProcessPublisher = PublishSubject<LucaIDParsedData?>()

    /// Fetches state on subscribe and emit all subsequent changes
    var lucaIDProcessChanges: Observable<LucaIDParsedData?> {
        Observable.merge(fetchState()
                            .asObservable(),
                         lucaIDProcessPublisher.asObservable())
            .distinctUntilChanged()
    }

    private let lucaIDDeletionPublisher = PublishSubject<Void>()

    var lucaIDDeletionChanges: Observable<Void> {
        lucaIDDeletionPublisher
    }

    var idProcessStarted: Single<Bool> {
        self.lucaPreferences.get(\.idProcessStarted)
    }

    var idProcessStartedCurrentAndChanges: Observable<Bool> {
        self.lucaPreferences.currentAndChanges(\.idProcessStarted)
    }

    private var cachedIdentState: CachedDataSource<LucaIDProcess>! // `!` is needed because `self` is used in initialiser. This value is never nil after initialisation.

    var pollAfterDate: Single<Date> {
        self.keyValueRepo.load(Self.requestResultDelayKey)
    }

    init() {
        let dataSource = DataSourceWrapper<LucaIDProcess>(source: Single.deferred {
            return self.attestationService.fetchJWT()
                .flatMap { self.backend.fetchIDState(attestationJWT: $0.jwtString).asSingle() }
                .map { [$0] }
        })
        let cacheValidity = CacheValidity.onceIn(unit: .year)
        cachedIdentState = BaseCachedDataSource(
            dataSource: dataSource,
            cacheDataRepo: KeyValueRepoCacheWrapper(uniqueCacheKey: "IDProcessCache"),
            cacheValidity: cacheValidity,
            uniqueCacheIdentifier: "IDProcessCacheKey"
        )
    }

    func didDisconnectFromSharedProvider() {

        // Remove this instance to break the retain cycle made in init
        cachedIdentState = nil
    }

    func requestIdentProcess() -> Single<LucaIDRequestProcessResult> {
        Completable.from { try self.keysBundle.generateKeysIfNeeded() }
        .andThen(fetchAssertionPayload())
        .flatMap { (att: JWTAssertion, payload: LucaIDRequestProcessRequest) in
            self.backend.requestIdentProcess(
                assertionJWT: att.jwtString,
                payload: payload,
                mock: mockIdentProcess)
            .asSingle()
        }.flatMap { payload in
            let delay = Int(ceil((payload.waitForMs / 1000.0)))
            let pollAfter = Calendar.current.date(byAdding: .second, value: delay, to: self.timeProvider.now)
            return self.keyValueRepo.store(Self.requestResultDelayKey, value: pollAfter)
                .andThen(self.lucaPreferences.set(\.idProcessStarted, value: true))
                .andThen(Single.just(payload))
        }
    }

    private func fetchAssertionPayload() -> Single<(JWTAssertion, LucaIDRequestProcessRequest)> {
        Single.deferred {
            let laContext = LAContext()

            return Single.zip(self.attestationService.fetchJWT(),
                              self.attestationService.getAttestationNonce())
            .flatMap { (jwtAssertion, nonce) -> Single<(JWTAssertion, LucaIDRequestProcessRequest)> in
                let idPubKey = try self.keysBundle.deriveJWEIdPubKey(context: laContext)
                let idEncKey = try self.keysBundle.deriveJWEEncPubKey(context: laContext)

                guard let nonceData = nonce.data(using: .utf8),
                      let idPubKeyData = Data(base64Encoded: idPubKey, options: .ignoreUnknownCharacters) else {
                    throw DeviceCheckError.missingData
                }
                let clientData = nonceData + idPubKeyData

                return self.attestationService.getAssertion(clientData: clientData)
                    .map { assertion in
                        let payload = LucaIDRequestProcessRequest(encPublicKey: idEncKey,
                                                                  idPublicKey: idPubKey,
                                                                  assertion: assertion.base64EncodedString(),
                                                                  nonce: nonce)
                        return (jwtAssertion, payload)
                    }
            }
        }
    }

    func deletePollAfterDate() -> Completable {
        keyValueRepo.remove(Self.requestResultDelayKey)
    }

    /// Deletes the data from LUCA after successful Id fetch
    func delete() -> Completable {
        attestationService.fetchJWT()
            .flatMapCompletable { self.backend.deleteData(attestationJWT: $0.jwtString).asCompletable() }
            .do(onCompleted: { [weak self] in
                            self?.lucaIDDeletionPublisher.onNext(Void()) })
    }

    /// Deletes and revokes the data from LUCA and from Ident system.
    func archiveData() -> Completable {
        idProcessStarted.flatMapCompletable { active in
            guard active else { return .empty() }
            return self.processDeletionAndRevokation()
        }
    }

    private func processDeletionAndRevokation() -> Completable {
        return self.attestationService.fetchJWT()
            .flatMapCompletable { self.backend.archiveData(attestationJWT: $0.jwtString).asCompletable() }
            .andThen(keysBundle.purgeKeysRx())
            .andThen(cachedIdentState.invalidateCache())
            .andThen(self.lucaPreferences.remove(\.idProcessStarted))
            .andThen(self.deletePollAfterDate())
            .do(onCompleted: { [weak self] in
                self?.lucaIDDeletionPublisher.onNext(Void()) })
    }

    func fetchState() -> Single<LucaIDParsedData?> {
        idProcessStarted
        .flatMap { [weak self] (generated: Bool) -> Single<LucaIDProcess> in
            guard let self = self, generated else { throw RxError.unknown }
                return self.fetchIdentState()
        }
        .flatMap { [weak self] (process: LucaIDProcess) -> Single<(LucaIDProcess, LucaIDParsedData)> in
            guard let self = self else { throw RxError.unknown }
            var parsedData = LucaIDParsedData(state: process)
            guard let receiptJWS = process.receiptJWS else {
                if mockIdentProcess {
                    return Single.just((process, parsedData))
                } else {
                    throw NSError(domain: "JWS recept missing", code: 0, userInfo: nil)
                }
            }

            // if there is a receiptJWS -> verify it
            let receipt = try JWT<LucaIDReceiptClaims>(jwtString: receiptJWS)
            return self.verifyReceipt(receipt, rawJWS: receiptJWS).andThen(
                Single.from {
                    parsedData.receiptJWS = receipt
                    return (process, parsedData)
                }
            )
        }
        .map { (process, parsedData) in
            var parsedData = parsedData
            if let data = process.data {
                let decryptedIdentityJWTString = try self.decryptVC(data.valueIdentity)
                let decryptedMinimalIdentityJWTString = try self.decryptVC(data.valueMinimalIdentity)
                let decryptedFaceJWTString = try self.decryptVC(data.valueFace)

                let jwt = try JWT<LucaIDIdentityClaims>(jwtString: decryptedIdentityJWTString)
                let jwtMinimal = try JWT<LucaIDMinimalIdentityClaims>(jwtString: decryptedMinimalIdentityJWTString)
                let jwtFace = try JWT<LucaIDImageClaims>(jwtString: decryptedFaceJWTString)
                guard jwt.validateClaims(now: self.timeProvider.now) == .success else {
                    throw NSError(domain: "identity jwt not valid", code: 0, userInfo: nil)
                }
                try self.verifyVC(jwt.claims.sub)

                guard jwtMinimal.validateClaims(now: self.timeProvider.now) == .success else {
                    throw NSError(domain: "minimal identity jwt not valid", code: 0, userInfo: nil)
                }
                try self.verifyVC(jwtMinimal.claims.sub)

                guard jwtFace.validateClaims(now: self.timeProvider.now) == .success else {
                    throw NSError(domain: "face jwt not valid", code: 0, userInfo: nil)
                }
                try self.verifyVC(jwtFace.claims.sub)

                parsedData.minimalIdentityJWT = jwtMinimal
                parsedData.faceJWT = jwtFace
                parsedData.identityJWT = jwt
            }
            return parsedData
        }
        .catchAndReturn(nil)
        .do(onSuccess: {
            self.lucaIDProcessPublisher.onNext($0)
        })
    }

    /// It fetches the state and manages the cache accordingly.
    private func fetchIdentState() -> Single<LucaIDProcess> {
        cachedIdentState.retrieve()
            .compactMap { $0.first }
            .asObservable()
            .asSingle() // it will throw an error if the array is empty but it's intended.
            .flatMap { state -> Single<LucaIDProcess> in
                if state.state != .success {
                    // Invalidate cache if the state is not success! There should be always a new value if it's pending/failed.
                    return self.cachedIdentState.invalidateCache().andThen(.just(state))
                }
                return self.pollAfterDate
                    .flatMapCompletable { _ in self.delete() }
                    .andThen(self.deletePollAfterDate())
                    .andThen(.just(state))
                    .catchAndReturn(state)
            }
    }

    private func verifyVC(_ sub: String) throws {
        if mockIdentProcess {
            return
        }

        let laContext = LAContext()
        var didKeyString = sub.remove(prefix: "did:key:")

        // reference: https://github.com/multiformats/multibase/blob/master/multibase.csv
        let didKeyEncoding = didKeyString.removeFirst()
        guard didKeyEncoding == "z",
              let vcPubKey = Base58.base58Decode(didKeyString) else {
            throw NSError(domain: "vc not base58 encoded", code: 0, userInfo: nil)
        }

        let vcPubKeyHex = Data(vcPubKey).hexString
        // reference: https://github.com/multiformats/multicodec/blob/master/table.csv
        let p256PubPrefix = "8024"
        guard vcPubKeyHex.hasPrefix(p256PubPrefix) else {
            throw NSError(domain: "not p256-pub key", code: 0, userInfo: nil)
        }

        let jweIdPubKey = try keysBundle.deriveCompressedJWEIdPubKey(context: laContext)
        guard jweIdPubKey.hexString == vcPubKeyHex.remove(prefix: p256PubPrefix) else {
            throw NSError(domain: "could not verify vc", code: 0, userInfo: nil)
        }
    }

    private func verifyReceipt(_ jws: JWT<LucaIDReceiptClaims>, rawJWS: String) -> Completable {
        let laContext = LAContext()

        do {
            let jweEncPubKey = try keysBundle.deriveJWEEncPubKey(context: laContext)
            guard jweEncPubKey == jws.claims.input_data.encryptionKey else {
                throw NSError(domain: "wrong encryption key", code: 0, userInfo: nil)
            }

            let jweIdPubKey = try keysBundle.deriveJWEIdPubKey(context: laContext)
            guard jweIdPubKey == jws.claims.input_data.bindingKey else {
                throw NSError(domain: "wrong binding key", code: 0, userInfo: nil)
            }
        } catch {
            return .error(error)
        }

        if !mockIdentProcess {
            // Mock JWT don't have a proper x5c header
            return verifyCertificates(for: jws, rawJWS: rawJWS)
        }

        return .empty()
    }

    private var idNowRootCertificate: Single<String> {
        Single<URL?>.from {
#if PRODUCTION || PREPROD
    let filenamePrefix = "prod"
#else
    let filenamePrefix = "dev"
#endif
            return Bundle.main.url(forResource: filenamePrefix + "_idnow_ca_root", withExtension: "pem")
        }
        .unwrapOptional()
        .map { try String(contentsOf: $0) }
    }

    private func verifyCertificates(for jws: JWT<LucaIDReceiptClaims>, rawJWS: String) -> Completable {
        if var jwtCertificates = jws.header.x5c {
            let root = idNowRootCertificate

            let leaf = jwtCertificates.remove(at: 0)

            let intermediate: Single<[String]> = Single.just(jwtCertificates)

            let verifier = LucaIdJWTVerifier(rootCertificateSource: root, intermediateCertificatesSource: intermediate)

            return verifier.verifyCertificates(with: leaf)
                .andThen(verifier.verifyJWTSignature(for: rawJWS, withCertificate: leaf))
        }
        return .error(NSError(domain: "jws header not present", code: 0, userInfo: nil))
    }

    private func decryptVC(_ encryptedJWE: String) throws -> String {
        let privateKey = try self.keysBundle.encPrivateKey(context: nil)
        let ecPrivateKey = try ECPrivateKey(privateKey: privateKey)

        let jwe = try JWE(compactSerialization: encryptedJWE)
        guard let decrypter = Decrypter(keyManagementAlgorithm: .ECDH_ES_A256KW, contentEncryptionAlgorithm: .A256CBCHS512, decryptionKey: ecPrivateKey) else {
            throw NSError(domain: "No decrypter", code: 0, userInfo: nil)
        }
        let payload = try jwe.decrypt(using: decrypter)
        if let message = String(data: payload.data(), encoding: .utf8) {
            return message
        } else {
            throw NSError(domain: "Decrypted data not string representable", code: 0, userInfo: nil)
        }
    }
}
