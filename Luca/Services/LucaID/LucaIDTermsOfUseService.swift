import Foundation
import RxSwift
import DependencyInjection

class LucaIDTermsOfUseService: TermsOfUseService<String> {
    init() {
        super.init(serviceKey: "LucaID", newestVersion: "98")
    }
}
