import Foundation
import RxSwift
import SwiftJWT
import LocalAuthentication
import DeviceCheck
import DependencyInjection
import CryptoKit

struct AttestationClaims: Claims {
    var iss: String
    var sub: String
    var os: String
    var iat: Date?
    var nbf: Date?
    var exp: Date
}

enum DeviceCheckError: LocalizedTitledError {
    case lockScreenDisabled
    case wrongIOSVersion
    case noAttestKey
    case missingData
    case expiredJWT
    case attestationFailed
    case serverUnavailable
}

extension DeviceCheckError {
    var localizedTitle: String {
        L10n.IOSApp.Test.Result.Error.title
    }

    var errorDescription: String? {
        switch self {
        default: return L10n.IOSApp.Id.Device.Check.error
        }
    }
}

struct JWTAssertion: Codable {
    var jwtString: String
    var jwt: JWT<AttestationClaims>
    var nonce: String
}

class LucaIDVerificationPreconditionsCheckService {

    @InjectStatic(\.keyValueRepo) private var keyValueRepo
    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.backendLucaID) private var backend

    private let attestKeyIdentifierKey = "attestKeyIdentifier"
    private let attestDeviceIdentifierKey = "attestDeviceIdentifier"

    private let jwtValidityInSeconds = 600

    private lazy var jwtCache: CachedDataSource<JWTAssertion> = {
        let source = getJWTAssertion().map { [$0] }
        return BaseCachedDataSource(
            dataSource: DataSourceWrapper(source: source),
            cacheDataRepo: KeyValueRepoCacheWrapper<JWTAssertion>(uniqueCacheKey: "jwtAssertion"),
            cacheValidity: .until(unit: .second, count: jwtValidityInSeconds),
            uniqueCacheIdentifier: "AttestationCacheId"
        )
    }()

    private func isLockScreenEnabled() -> Completable {
        if LAContext().canEvaluatePolicy(.deviceOwnerAuthentication, error: nil) {
            return .empty()
        }
        return .error(DeviceCheckError.lockScreenDisabled)
    }

    private func getAttestKeyIdentifier() -> Single<String> {
        keyValueRepo.load(attestKeyIdentifierKey)
            .catch { _ in
                return DCAppAttestService.shared.generateAttestKeyIdentifier()
            }
    }

    func resetIdentifiers() -> Completable {
        keyValueRepo.remove(attestKeyIdentifierKey)
            .andThen(keyValueRepo.remove(attestDeviceIdentifierKey))
    }

    func getAttestationNonce() -> Single<String> {
        backend.requestAttestationNonce()
            .asSingle()
            .map { $0.nonce }
    }

    private func getAttestationRequest(keyIdentifier: String) -> Single<(String, RegisterAttestationRequest)> {
        getAttestationNonce()
            .flatMap { [weak self] nonce in
                guard let self = self else {
                    return .error(DeviceCheckError.missingData)
                }
                return DCAppAttestService.shared.getAttestation(keyIdentifier: keyIdentifier, nonce: nonce)
                    .map { (keyIdentifier, $0) }
                    .catch { error in
                        if let dcError = error as? DCError,
                           dcError.code == .serverUnavailable {
                            return .error(DeviceCheckError.serverUnavailable)
                        }
                        return self.resetIdentifiers().andThen(.error(DeviceCheckError.attestationFailed))
                    }
            }
    }

    private func registerAttestation(keyIdentifier: String, request: RegisterAttestationRequest) -> Completable {
        backend.registerAttestation(payload: request)
            .asSingle()
            .flatMapCompletable { [weak self] attestationResult in
                guard let self = self else {
                    return .error(DeviceCheckError.missingData)
                }
                return self.keyValueRepo.store(self.attestKeyIdentifierKey, value: keyIdentifier)
                    .andThen(self.keyValueRepo.store(self.attestDeviceIdentifierKey, value: attestationResult.deviceId))
            }
    }

    func getAssertion(clientData: Data) -> Single<Data> {
        return getAttestKeyIdentifier()
            .flatMap { keyIdentifier in
                return DCAppAttestService.shared.getAssertion(keyIdentifier: keyIdentifier, clientData: clientData)
            }
    }

    /// Pull JWT from server based on hashed nonce. This JWT is later attached to requests to prove valid app assertion
    /// - Parameter nonce: One-time token from `requestAttestationNonce()` method
    /// - Returns: JWT containing attestation claims
    private func getJWTAssertion(nonce: String) -> Single<JWTAssertion> {
        guard let clientData = nonce.data(using: .utf8) else {
            return .error(DeviceCheckError.missingData)
        }

        return Single.zip(keyValueRepo.loadOptional(attestDeviceIdentifierKey),
                          getAssertion(clientData: clientData))
            .flatMap { [weak self] (deviceId: String?, assertion: Data) in
                guard let self = self, let deviceId = deviceId else { return .error(DeviceCheckError.noAttestKey) }
                let payload = RequestAssertionJWTRequest(deviceId: deviceId, nonce: nonce, assertion: assertion.base64EncodedString())
                return self.backend.requestAssertionJWT(payload: payload)
                    .asSingle()
                    .flatMap { response in
                        let jwt = try JWT<AttestationClaims>(jwtString: response.jwt)
                        return .just(JWTAssertion(jwtString: response.jwt, jwt: jwt, nonce: nonce))
                    }
            }
    }

    private func getJWTAssertion() -> Single<JWTAssertion> {
        getAttestationNonce()
            .flatMap(getJWTAssertion(nonce:))
    }

    private func getCachedJWT() -> Single<JWTAssertion> {
        jwtCache.retrieve()
            .asObservable()
            .compactMap { $0.first }
            .asSingle()
            .flatMap { [weak self] jwt in
                guard let self = self else {
                    throw DeviceCheckError.missingData
                }
                guard jwt.jwt.validateClaims(now: self.timeProvider.now, validityTime: TimeInterval(self.jwtValidityInSeconds)) == .success else {
                    return self.jwtCache.invalidateCache().andThen(.error(DeviceCheckError.expiredJWT))
                }
                return .just(jwt)
            }
    }

    func fetchJWT() -> Single<JWTAssertion> {
        checkPreconditionsIfNeeded()
            .andThen(self.getCachedJWT().catch { error in
                if let error = error as? DeviceCheckError,
                   error == DeviceCheckError.expiredJWT {
                    return self.getCachedJWT()
                }
                throw error
            })
    }

    func checkPreconditionsIfNeeded() -> Completable {
        keyValueRepo.loadOptional(attestKeyIdentifierKey)
            .flatMapCompletable { (keyIdentifier: String?) in
                if keyIdentifier == nil {
                    return self.isLockScreenEnabled()
                        .andThen(self.getAttestKeyIdentifier())
                        .flatMap(self.getAttestationRequest(keyIdentifier:))
                        .flatMapCompletable(self.registerAttestation(keyIdentifier: request:))
                } else {
                    return .empty()
                }
            }
    }
}
