import Foundation
import RxSwift
import DependencyInjection

class LucaIDMessageService: Toggleable {

    @InjectStatic(\.localNewsMessageRepo) private var localMessagesRepo
    @InjectStatic(\.lucaIDService) private var lucaIDService
    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.lucaIDTermsOfUse) private var lucaIDToS

    private var disposeBag: DisposeBag?

    var isEnabled: Bool {
        disposeBag != nil
    }

    func enable() {
        if isEnabled { return }

        let newDisposeBag = DisposeBag()

        lucaIDService.lucaIDProcessChanges
            .flatMap { data -> Completable in
                if data?.state.state == .pending, data?.receiptJWS?.claims.autoIdentId != nil {
                    return self.emitNotification(for: .idIdentPending)
                } else if data?.state.state == .failed {
                    return self.emitNotification(for: .idIdentFailure)
                } else if data?.state.state == .success, data?.receiptJWS?.claims.autoIdentId != nil {
                    return self.emitNotification(for: .idIdentSuccess)
                }
                return Completable.empty()
            }
            .subscribe()
            .disposed(by: newDisposeBag)

        lucaIDService.lucaIDDeletionChanges
            .flatMap { _ in
                self.lucaPreferences.get(\.idProcessStarted)
            }
            .flatMap { started -> Completable in
                // If started, ID exists and has only been deleted at successful import, not revoked.
                if started {
                    return self.deleteNotifications(for: .idIdentPending)
                        .andThen(self.deleteNotifications(for: .idIdentFailure))

                } else {
                    // In this case user has revoked and deleted ID manually.
                    return self.deleteNotifications(for: .idIdentPending)
                        .andThen(self.deleteNotifications(for: .idIdentFailure))
                        .andThen(self.deleteNotifications(for: .idIdentSuccess))
                }

            }.subscribe()
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

    func disable() {
        disposeBag = nil
    }

    func deleteNotifications(for type: LocalNewsType) -> Completable {
        return localMessagesRepo
            .restore()
            .asObservable()
            .map { messages in
                let successMessages = messages.filter { $0.type == type }
                return successMessages.filter { $0.identifier != nil }.map { $0.identifier! }
            }.flatMap { identifiers -> Completable in
                self.localMessagesRepo.remove(identifiers: identifiers)
            }.ignoreElementsAsCompletable()
    }

    private func emitNotification(for type: LocalNewsType) -> Completable {
        notificationSent(for: type)
            .filter { !$0 }
            .asObservable()
            .flatMap { _ -> Single<LocalNewsMessage> in
                let message = LocalNewsMessage(type: type, createDate: self.timeProvider.now)
                return self.localMessagesRepo.store(object: message)
            }
            .ignoreElementsAsCompletable()
    }

    private func notificationSent(for type: LocalNewsType) -> Single<Bool> {
        localMessagesRepo
            .restore()
            .map { messages -> Bool in
                let messages = messages.filter { $0.type == type }
                return messages.isEmpty ? false : true
            }
    }

}
