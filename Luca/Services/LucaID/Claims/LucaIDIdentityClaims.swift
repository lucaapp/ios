import Foundation
import SwiftJWT

struct LucaIDCredentialSubject: Codable {
    struct Nationality: Codable {
        var identifier: String
    }
    var familyName: String
    var givenName: String
    var birthDate: String
    var nationality: Nationality
}

struct LucaIDIdentityClaimsPayload: Codable {
    var type: [String]
    var credentialSubject: LucaIDCredentialSubject
}

struct LucaIDIdentityClaims: Codable, Claims {
    var sub: String
    var nbf: Date
    var iss: String
    var exp: Date
    var vc: LucaIDIdentityClaimsPayload
    var jti: String
}
