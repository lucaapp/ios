import Foundation
import SwiftJWT

struct LucaIDMinimalIdentityClaimsPayload: Codable {
    struct CredentialsSubject: Codable {
        var familyName: String
        var givenName: String
        var birthDate: String
    }
    var type: [String]
    var credentialSubject: CredentialsSubject
}

struct LucaIDMinimalIdentityClaims: Codable, Claims {
      var sub: String
      var nbf: Date
      var iss: String
      var exp: Date
      var vc: LucaIDMinimalIdentityClaimsPayload
      var jti: String
}
