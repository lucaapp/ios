import Foundation
import SwiftJWT

struct LucaIDReceiptInputDataClaimsPayload: Codable {
    var bindingKey: String
    var encryptionKey: String
}

struct LucaIDReceiptClaims: Codable, Claims {
    var input_data: LucaIDReceiptInputDataClaimsPayload
    var autoIdentId: String
    var transactionNumber: String
}
