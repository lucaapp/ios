import Foundation
import SwiftJWT

struct LucaIDImageClaimsPayload: Codable {
    struct CredentialsSubject: Codable {
        var image: String // Base64 with image data. Probably PNG/JPG which can be directly imported to UIImage
    }
    var type: [String]
    var credentialSubject: CredentialsSubject
}

struct LucaIDImageClaims: Codable, Claims {
    var sub: String
    var nbf: Date
    var iss: String
    var exp: Date
    var jti: String
    var vc: LucaIDImageClaimsPayload
}
