import Foundation
import LocalAuthentication
import JOSESwift
import RxSwift

class LucaIDKeysBundle {
    private var _encPrivateKeyRepo = SecKeyRepository(tag: "LucaID.encPrivateKey")
    private var _idPrivateKeyRepo = SecKeyRepository(tag: "LucaID.idPrivateKey")

    func encPrivateKey(context: LAContext? = nil) throws -> SecKey {
        try _encPrivateKeyRepo.restore(context: context)
    }
    func idPrivateKey(context: LAContext? = nil) throws -> SecKey {
        try _idPrivateKeyRepo.restore(context: context)
    }

    var keysAlreadyGenerated: Bool {
        let encKey = try? encPrivateKey()
        let idKey = try? idPrivateKey()
        return encKey != nil && idKey != nil
    }

    func generateKeysIfNeeded() throws {
        if keysAlreadyGenerated { return }
        // Supports only .eciesEncryptionCofactorX963SHA256AESGCM but can be used to exchange with DH and extract the material
        _ = try KeyFactory.createECPrivateKeySecureEnclave(tag: _idPrivateKeyRepo.tag, accessibility: .whenPasscodeSetThisDeviceOnly, accessControlCreateFlags: .userPresence)
        _ = try KeyFactory.createPrivate(tag: _encPrivateKeyRepo.tag, type: .ecsecPrimeRandom, sizeInBits: 256)
    }

    func purgeKeys() throws {
        try KeyStorage.purge(tag: _encPrivateKeyRepo.tag)
        try KeyStorage.purge(tag: _idPrivateKeyRepo.tag)
    }

    func purgeKeysRx() -> Completable {
        Completable.from {
            try self.purgeKeys()
        }
    }

    func deriveJWEIdPubKey(context: LAContext) throws -> String {
        let idPrivKey = try idPrivateKey(context: context)

        guard let idPubKey = KeyFactory.derivePublic(from: idPrivKey) else {
            throw NSError(domain: "Couldn't derive public from private id key", code: 0, userInfo: nil)
        }
        let idPubKeyData = try idPubKey.toData()
        return idPubKeyData.base64EncodedString()
    }

    func deriveCompressedJWEIdPubKey(context: LAContext) throws -> Data {
        let idPrivKey = try idPrivateKey(context: context)

        guard let idPubKey = KeyFactory.derivePublic(from: idPrivKey) else {
            throw NSError(domain: "Couldn't derive public from private id key", code: 0, userInfo: nil)
        }

        return try KeyFactory.compressPublicEC(key: idPubKey)
    }

    func deriveJWEEncPubKey(context: LAContext) throws -> String {

        let encPrivKey = try encPrivateKey(context: context)

        guard let encPubKey = KeyFactory.derivePublic(from: encPrivKey) else {
            throw NSError(domain: "Couldn't derive public from private encryption key", code: 0, userInfo: nil)
        }
        let encPubKeyData = try encPubKey.toData()
        return encPubKeyData.base64EncodedString()
    }
}
