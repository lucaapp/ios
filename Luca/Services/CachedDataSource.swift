import Foundation
import RxSwift
import DependencyInjection

enum CacheValidity {
    case none
    case until(unit: Calendar.Component, count: Int)
    case onceIn(unit: Calendar.Component)
    case forever

    #if !PRODUCTION
    /// This is limited to non-productive code as the sessions in the real life can last for very long time.
    case onceInRuntime
    #endif
}

protocol DataSource {
    associatedtype T
    func retrieve() -> Single<[T]>
}

protocol CacheDataRepo {
    associatedtype T
    func store(data: [T]) -> Completable
    func retrieve() -> Single<[T]>
    func invalidate() -> Completable
}

class CachedDataSource<T> {

    /// Emits `true` if the cache is valid, `false` otherwise
    var isCacheValid: Single<Bool> {
        fatalError("Not implemented")
    }

    /// Emits data either from the source or from the cache if still valid
    func retrieve(loadOnlyFromCache: Bool = false) -> Single<[T]> {
        fatalError("Not implemented")
    }

    /// Deletes the cache.
    func invalidateCache() -> Completable {
        fatalError("Not implemented")
    }
}

class BaseCachedDataSource<T, DataSourceType, CacheDataRepoType>: CachedDataSource<T>
            where DataSourceType: DataSource, DataSourceType.T == T,
                  CacheDataRepoType: CacheDataRepo, CacheDataRepoType.T == T {

    @InjectStatic(\.keyValueRepo) private var keyValueRepo: KeyValueRepoProtocol
    @InjectStatic(\.timeProvider) private var timeProvider: TimeProvider

    private let dispatchQueue: SchedulerType
    private let dataSource: DataSourceType
    private let cache: CacheDataRepoType
    private let cacheValidity: CacheValidity

    #if !PRODUCTION
    // It says if some value has been received. It is used only in case where cacheValidity is set to `onceInRuntime`
    private var hasBeenReceived = false
    #endif

    private var cacheTimestampKey: String = ""

    /// Shared source should be initialized once.
    /// It guarantees that there will be no duplicated download processes if someone calls `retrieve` from two threads at the same time
    ///
    /// Since lazy is not thread safe, it has to be  synchronised with `dispatchQueue`
    private lazy var sharedSource: Observable<[T]> = {
        dataSource.retrieve()
            .asObservable()
            .share(replay: 1, scope: .whileConnected)
    }()

    init(
        dataSource: DataSourceType,
        cacheDataRepo: CacheDataRepoType,
        cacheValidity: CacheValidity,
        uniqueCacheIdentifier: String
    ) {
        self.dispatchQueue = SerialDispatchQueueScheduler(qos: .default, internalSerialQueueName: "\(String(describing: Self.self)).CachedDataSource")
        self.dataSource = dataSource
        self.cache = cacheDataRepo
        self.cacheValidity = cacheValidity

        super.init()

        cacheTimestampKey = "\(uniqueCacheIdentifier).cacheTimestamp"
    }

    override var isCacheValid: Single<Bool> {
        cacheTimestamp()
            .map { self.isCacheTimestampValid(date: $0) }
            .ifEmpty(default: false)
    }

    override func retrieve(loadOnlyFromCache: Bool = false) -> Single<[T]> {
        if loadOnlyFromCache {
            return cache.retrieve()
        }
        return isCacheValid
            .flatMap { isValid in
                if isValid {
                    return self.cache.retrieve().catch { _ in self.retrieveFromSource() }
                }
                return self.retrieveFromSource()
            }
    }

    override func invalidateCache() -> Completable {
        cache.invalidate()
            .andThen(keyValueRepo.remove(cacheTimestampKey))
            #if !PRODUCTION
            .andThen(Completable.from { self.hasBeenReceived = false })
            #endif
    }

    /// Retrieves last cache timestamp, if there is any. Completes empty if there isn't any.
    private func cacheTimestamp() -> Maybe<Date> {
        keyValueRepo.load(cacheTimestampKey).asObservable().onErrorComplete().asMaybe()
    }

    /// Retrieves data from source, saves to the cache and saves the current timestamp.
    private func retrieveFromSource() -> Single<[T]> {
        Single.deferred { self.sharedSource.take(1).asSingle() }
        .subscribe(on: self.dispatchQueue)
        .flatMap { data in
            self.cache.store(data: data)
                .andThen(self.keyValueRepo.store(self.cacheTimestampKey, value: self.timeProvider.now))
                #if !PRODUCTION
                .andThen(Completable.from { self.hasBeenReceived = true })
                #endif
                .andThen(Single.just(data))
        }
    }

    /// Checks if given date is valid within current settings
    private func isCacheTimestampValid(date: Date) -> Bool {
        switch cacheValidity {
        case .none:
            return false
        case .forever:
            return true
        #if !PRODUCTION
        case .onceInRuntime:
            return hasBeenReceived
        #endif
        case .until(let unit, let count):
            guard let threshold = Calendar.current.date(byAdding: unit, value: -count, to: timeProvider.now) else { return false }

            // Expiry date should be larger or equal threshold and current date should be larger or equal expiry date
            return date >= threshold && timeProvider.now >= date
        case .onceIn(let unit):
            return Calendar.current.isDate(date, equalTo: timeProvider.now, toGranularity: unit)
        }
    }
}

extension AsyncDataOperation: DataSource {
    typealias T = Result

    func retrieve() -> Single<[T]> {
        self.asSingle().map { [$0] }
    }
}

class KeyValueRepoCacheWrapper<T>: CacheDataRepo where T: Codable {

    @InjectStatic(\.keyValueRepo) private var keyValueRepo: KeyValueRepoProtocol
    private var key: String = ""

    init(uniqueCacheKey: String) {
        key = uniqueCacheKey + ".cachedData"
    }

    func store(data: [T]) -> Completable {
        keyValueRepo.store(key, value: data)
    }

    func retrieve() -> Single<[T]> {
        keyValueRepo.load(key)
    }

    func invalidate() -> Completable {
        keyValueRepo.remove(key)
    }
}

class DataSourceWrapper<T>: DataSource {
    private let source: Single<[T]>
    init(source: Single<[T]>) {
        self.source = source
    }
    func retrieve() -> Single<[T]> {
        source
    }
}
