import Foundation
import SwiftJWT
import DependencyInjection
import RxSwift

struct PrivateMeetingQRCodeV3AdditionalData: Codable {
    var fn: String
    var ln: String
}

struct PrivateMeetingQRCodeV3 {
    var additionalData: PrivateMeetingQRCodeV3AdditionalData
    var scannerId: String
    var host: URL
}

extension PrivateMeetingQRCodeV3 {
    var generatedUrl: String? {
        guard let encodedData = try? JSONEncoderUnescaped().encode(additionalData) else {
            return nil
        }
        let payload = JWTEncoder.base64urlEncodedString(data: encodedData)
        let url = host.appendingPathComponent("webapp")
            .appendingPathComponent("meeting")
            .appendingPathComponent("\(scannerId)")
        return "\(url.absoluteString)#\(payload)"
    }
}

class PrivateMeetingQRCodeBuilderV3 {
    @InjectStatic(\.backendAddressV3) private var backendAddress
    @InjectStatic(\.lucaPreferences) private var lucaPreferences

    func build(scannerId: String) -> Single<PrivateMeetingQRCodeV3> {
        lucaPreferences.get(\.userRegistrationData)
            .map { userData in
                guard let userData = userData,
                      let firstName = userData.firstName,
                      let lastName = userData.lastName else {
                    throw NSError(domain: "Couldn't obtain user name from data", code: 0, userInfo: nil)
                }
                let additionalData = PrivateMeetingQRCodeV3AdditionalData(fn: firstName, ln: lastName)
                return PrivateMeetingQRCodeV3(additionalData: additionalData, scannerId: scannerId, host: self.backendAddress.host)
            }
    }
}
