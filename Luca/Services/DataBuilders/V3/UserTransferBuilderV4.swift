import Foundation
import RxSwift
import DependencyInjection

struct UserTransferPayload: Codable {
    var data: String
    var iv: String
    var mac: String
    var publicKey: String
    var keyId: Int
}

// swiftlint:disable nesting
struct UserTransferDataV4: Codable {
    struct Entry: Codable {
        struct TimestampSecretPair: Codable {
            var timestamp: Int
            var secret: String

            enum CodingKeys: String, CodingKey {
                case timestamp = "ts"
                case secret = "s"
            }
        }

        var registrationTimestamp: Int
        var id: String
        var dataSecret: String
        var tracingSecrets: [TimestampSecretPair]

        enum CodingKeys: String, CodingKey {
            case registrationTimestamp = "rts"
            case id = "id"
            case dataSecret = "ds"
            case tracingSecrets = "ts"
        }
    }
    let version: Int = 4
    var data: [Entry]

    enum CodingKeys: String, CodingKey {
        case version = "v"
        case data = "u"
    }
}

class UserTransferBuilderV4 {
    @InjectStatic(\.dailyKeyRepository) private var dailyKey
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.userKeysBundle) private var userKeysBundle

    func build(numberOfDays: Int, customIV: Data? = nil) -> Single<UserTransferPayload> {
        collectData(forNumberOfDays: numberOfDays)
            .map { payload in
                guard let newestId = self.dailyKey.newestId else {
                    throw NSError(domain: "No public key available", code: 0, userInfo: nil)
                }
                let receiverPublicKeySource = try self.dailyKey.keySource(index: newestId)
                let ephemeralKeyPair = try KeyFactory.createECKeyPair(tag: "")

                let dlies = DLIES(ephemeralKeyPair: ephemeralKeyPair, receiverPublicKeySource: receiverPublicKeySource, iv: customIV)
                let encrypted = try dlies.encrypt(data: payload)

                return UserTransferPayload(
                    data: encrypted.data,
                    iv: encrypted.iv,
                    mac: encrypted.mac,
                    publicKey: encrypted.publicKey ?? "",
                    keyId: newestId.keyId
                )
            }
    }

    private func collectData(forNumberOfDays days: Int) -> Single<Data> {
        Single.zip(
            lucaPreferences.get(\.oldUUIDs),
            lucaPreferences.get(\.uuid).compactMap { $0 }.asObservable().asSingle() // It will generate an error, if compactMap filters the nil out but this is required anyway.
        )
            .map { uuids, currentUUID in
                guard let thresholdDate = Calendar.current.date(byAdding: .day, value: -days, to: self.timeProvider.now) else {
                    throw NSError(domain: "Couldn't generate threshold date", code: 0, userInfo: nil)
                }
                var filteredUUIDs = uuids.filter { $0.timestamp > thresholdDate || Calendar.current.isDate($0.timestamp, inSameDayAs: thresholdDate) }

                // If current UUID is not there, add it with the top most date or the threshold date.
                if !filteredUUIDs.contains(where: { $0.id == currentUUID }) {
                    if let newestDate = filteredUUIDs.map({$0.timestamp}).sorted().last {
                        filteredUUIDs.append(UserIDWithTimestamp(id: currentUUID, timestamp: newestDate))
                    } else {
                        filteredUUIDs.append(UserIDWithTimestamp(id: currentUUID, timestamp: thresholdDate))
                    }
                }
                let sortedUUIDs = filteredUUIDs.sorted(by: { $0.timestamp < $1.timestamp })
                let dataSecret: Data = try self.userKeysBundle.dataSecret.retrieveKey()
                let dataSecretB64 = dataSecret.base64EncodedString()

                return try sortedUUIDs.enumerated().map {
                    let nextTimestamp = $0.offset == uuids.count - 1 ? uuids.last : uuids[$0.offset + 1]
                    return UserTransferDataV4.Entry(
                        registrationTimestamp: Int($0.element.timestamp.timeIntervalSince1970),
                        id: $0.element.id.uuidString.lowercased(),
                        dataSecret: dataSecretB64,
                        tracingSecrets: try self.retrieveSecrets(
                            from: $0.element.timestamp,
                            until: nextTimestamp?.timestamp ?? self.timeProvider.now)
                        )
                    }
            }
            .map { UserTransferDataV4(data: $0) }
            .map { try JSONEncoder().encode($0) }
    }

    private func retrieveSecrets(from: Date, until: Date) throws -> [UserTransferDataV4.Entry.TimestampSecretPair] {
        guard from <= until else {
            throw NSError(domain: "Invalid date range", code: 0, userInfo: nil)
        }
        var dates = [from]
        while !Calendar.current.isDate(dates.last!, inSameDayAs: until) {
            guard let date = Calendar.current.date(byAdding: .day, value: 1, to: dates.last!) else {
                throw NSError(domain: "Couldn't generate day", code: 0, userInfo: nil)
            }
            dates.append(date)
        }
        return try dates
            .map {
                let ts = Calendar.current.startOfDay(for: $0)
                return UserTransferDataV4.Entry.TimestampSecretPair(
                    timestamp: Int(ts.timeIntervalSince1970),
                    secret: try self.userKeysBundle.traceSecrets.restore(index: $0).base64EncodedString()
                )
            }
    }
}
