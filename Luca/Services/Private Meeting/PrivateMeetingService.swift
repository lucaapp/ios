import Foundation
import RxSwift
import DependencyInjection

struct PrivateMeetingData: Codable {
    var keyIndex: Int
    var createdAt: Date
    var deletedAt: Date?
    var guests: [PrivateMeetingGuest] = []
    var ids: PrivateMeetingIds
}

extension PrivateMeetingData {
    var isOpen: Bool {
        return deletedAt == nil || deletedAt!.timeIntervalSince1970 > Date.now.timeIntervalSince1970
    }
}

// Service that checks the host of a private meeting in and out.
public class PrivateMeetingService {

    let onHostCheckIn = "onHostCheckIn"
    let onHostCheckOut = "onHostCheckOut"

    let onMeetingCreated = "onMeetingCreated"
    let onMeetingClosed = "onMeetingClosed"

    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.locationPrivateKeyHistoryRepository) private var privateKeysHistoryRepo
    @InjectStatic(\.backendLocationV3) private var backend
    @InjectStatic(\.traceIdAdditionalBuilderV3) private var traceIdAdditionalDataBuilder
    private let preferences: Preferences

    private var meetings: [PrivateMeetingData] {
        get {
            preferences.retrieve(key: "meetingsData", type: [PrivateMeetingData].self) ?? []
        }
        set {
            preferences.store(newValue, key: "meetingsData")
        }
    }

    var currentMeeting: PrivateMeetingData? {
        meetings.filter { $0.isOpen }.first
    }

    init(preferences: Preferences) {
        self.preferences = preferences
    }

    func createMeeting(completion: @escaping (PrivateMeetingData) -> Void, failure: @escaping (Error) -> Void) {
        guard let key = try? KeyFactory.createPrivate(tag: "", type: .ecsecPrimeRandom, sizeInBits: 256) else {
            failure(CryptoError.privateKeyNotRetrieved)
            return
        }
        let keyIndex = privateKeysHistoryRepo.indices.count
        do {
            try privateKeysHistoryRepo.store(key: key, index: keyIndex)
        } catch let error {
            failure(error)
            return
        }

        guard let publicKey = KeyFactory.derivePublic(from: key) else {
            failure(CryptoError.publicKeyNotRetrieved)
            return
        }

        backend.createPrivateMeeting(publicKey: publicKey)
            .execute { (ids) in
                let privateMeeting = PrivateMeetingData(keyIndex: keyIndex, createdAt: self.timeProvider.now, ids: ids)

                self.refreshInstance(meeting: privateMeeting)

                NotificationCenter.default.post(Notification(
                                                    name: Notification.Name(self.onMeetingCreated),
                                                    object: self,
                                                    userInfo: ["meeting": privateMeeting]))
                completion(privateMeeting)

            } failure: { (error) in
                self.privateKeysHistoryRepo.remove(index: keyIndex)
                failure(error)
            }
    }

    func close(meeting: PrivateMeetingData, completion: @escaping () -> Void, failure: @escaping (Error) -> Void) {
        backend.deletePrivateMeeting(accessId: meeting.ids.accessId).execute {

            self.closeLocally(meeting: meeting)

            completion()
        } failure: { (error) in
            if let backendError = error.backendError,
               backendError == .notFound {
                self.closeLocally(meeting: meeting)
                completion()
                return
            }
            failure(error)
        }
    }

    func refresh(meeting: PrivateMeetingData, completion: @escaping (PrivateMeetingData) -> Void, failure: @escaping (Error) -> Void) {
        backend.fetchLocationGuests(accessId: meeting.ids.accessId)
            .execute { (guests) in

                var copiedMeeting = meeting
                copiedMeeting.guests = guests
                self.refreshInstance(meeting: copiedMeeting)

                completion(copiedMeeting)
            } failure: { (error) in
                if let backendError = error.backendError,
                   backendError == .notFound {
                    completion(self.closeLocally(meeting: meeting))
                    return
                }
                failure(error)
            }
    }

    func decrypt(guestData: PrivateMeetingGuest, meetingKeyIndex: Int) throws -> PrivateMeetingQRCodeV3AdditionalData {
        guard let guestEncryptedData = guestData.data else {
            throw NSError(domain: "No data available", code: 0, userInfo: nil)
        }
        let venuePrivKey = try privateKeysHistoryRepo.restore(index: meetingKeyIndex)

        guard let keyData = Data(base64Encoded: guestEncryptedData.publicKey),
              let key = try? KeyFactory.create(from: keyData, type: .ecsecPrimeRandom, keyClass: .public),
              let iv = Data(base64Encoded: guestEncryptedData.iv),
              let encData = Data(base64Encoded: guestEncryptedData.data) else {
            throw NSError(domain: "Failed preparing data", code: 0, userInfo: nil)
        }

        let parsed = try traceIdAdditionalDataBuilder.decrypt(
            destination: PrivateMeetingQRCodeV3AdditionalData.self,
            venuePrivKey: ValueKeySource(key: venuePrivKey),
            userPubKey: ValueKeySource(key: key),
            data: encData,
            iv: iv)
        return parsed
    }

    /// It replaces the instance from the array based on the instanceId
    private func refreshInstance(meeting: PrivateMeetingData) {
        var tempMeetings = self.meetings
        tempMeetings.removeAll(where: { $0.ids.locationId == meeting.ids.locationId })
        tempMeetings.append(meeting)
        meetings = tempMeetings
    }

    @discardableResult
    private func closeLocally(meeting: PrivateMeetingData) -> PrivateMeetingData {
        var meetingCopy = meeting
        meetingCopy.deletedAt = timeProvider.now
        refreshInstance(meeting: meetingCopy)
        notifyClosedMeeting(meeting: meetingCopy)
        return meetingCopy
    }

    private func notifyClosedMeeting(meeting: PrivateMeetingData) {
        NotificationCenter.default.post(Notification(
                                            name: Notification.Name(self.onMeetingClosed),
                                            object: self,
                                            userInfo: ["meeting": meeting]))
    }
}

extension PrivateMeetingService: LogUtil, UnsafeAddress {}

extension PrivateMeetingService {

    var onMeetingCreatedRx: Observable<PrivateMeetingData> {
        NotificationCenter.default.rx.notification(NSNotification.Name(self.onMeetingCreated), object: self)
            .map { $0.userInfo?["meeting"] as? PrivateMeetingData }
            .unwrapOptional()
            .logError(self, "onMeetingCreatedRx")
    }

    var onMeetingClosedRx: Observable<PrivateMeetingData> {
        NotificationCenter.default.rx.notification(NSNotification.Name(self.onMeetingClosed), object: self)
            .map { $0.userInfo?["meeting"] as? PrivateMeetingData }
            .unwrapOptional()
            .logError(self, "onMeetingClosedRx")
    }
}

extension PrivateMeetingService {
    func createMeeting() -> Single<PrivateMeetingData> {
        Single<PrivateMeetingData>.create { (observer) -> Disposable in

            self.createMeeting { (meeting) in
                observer(.success(meeting))
            } failure: { (error) in
                observer(.failure(error))
            }

            return Disposables.create()
        }
    }

    func close(meeting: PrivateMeetingData) -> Completable {
        Completable.create { (observer) -> Disposable in

            self.close(meeting: meeting) {
                observer(.completed)
            } failure: { (error) in
                observer(.error(error))
            }

            return Disposables.create()
        }
    }

    func refresh(meeting: PrivateMeetingData) -> Single<PrivateMeetingData> {
        Single<PrivateMeetingData>.create { (observer) -> Disposable in

            self.refresh(meeting: meeting) { (meeting) in
                observer(.success(meeting))
            } failure: { (error) in
                observer(.failure(error))
            }

            return Disposables.create()
        }
    }
}
