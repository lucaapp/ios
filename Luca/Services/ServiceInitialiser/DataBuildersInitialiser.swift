import DependencyInjection

private var dailyKeyRepositoryProvider: BaseInjectionProvider<DailyPubKeyHistoryRepository> = EmptyProvider()
private var ePubKeyHistoryRepositoryProvider: BaseInjectionProvider<EphemeralPublicKeyHistoryRepository> = EmptyProvider()
private var ePrivKeyHistoryRepositoryProvider: BaseInjectionProvider<EphemeralPrivateKeyHistoryRepository> = EmptyProvider()
private var locationPrivateKeyHistoryRepositoryProvider: BaseInjectionProvider<LocationPrivateKeyHistoryRepository> = EmptyProvider()
private var userKeysBundleProvider: BaseInjectionProvider<UserKeysBundle> = EmptyProvider()
private var userDataPackageBuilderV3Provider: BaseInjectionProvider<UserDataPackageBuilderV3> = EmptyProvider()
private var qrCodePayloadBuilderV3Provider: BaseInjectionProvider<QRCodePayloadBuilderV4> = EmptyProvider()
private var checkOutPayloadBuilderV3Provider: BaseInjectionProvider<CheckOutPayloadBuilderV3> = EmptyProvider()
private var checkInPayloadBuilderV3Provider: BaseInjectionProvider<CheckInPayloadBuilderV3> = EmptyProvider()
private var traceIdAdditionalBuilderV3Provider: BaseInjectionProvider<TraceIdAdditionalDataBuilderV3> = EmptyProvider()
private var privateMeetingQRCodeBuilderV3Provider: BaseInjectionProvider<PrivateMeetingQRCodeBuilderV3> = EmptyProvider()
private var userTransferBuilderV4Provider: BaseInjectionProvider<UserTransferBuilderV4> = EmptyProvider()

extension DependencyContext {
    var dailyKeyRepository: BaseInjectionProvider<DailyPubKeyHistoryRepository> {
        get { dailyKeyRepositoryProvider }
        set { dailyKeyRepositoryProvider = newValue }
    }
    var ePubKeyHistoryRepository: BaseInjectionProvider<EphemeralPublicKeyHistoryRepository> {
        get { ePubKeyHistoryRepositoryProvider }
        set { ePubKeyHistoryRepositoryProvider = newValue }
    }
    var ePrivKeyHistoryRepository: BaseInjectionProvider<EphemeralPrivateKeyHistoryRepository> {
        get { ePrivKeyHistoryRepositoryProvider }
        set { ePrivKeyHistoryRepositoryProvider = newValue }
    }
    var locationPrivateKeyHistoryRepository: BaseInjectionProvider<LocationPrivateKeyHistoryRepository> {
        get { locationPrivateKeyHistoryRepositoryProvider }
        set { locationPrivateKeyHistoryRepositoryProvider = newValue }
    }
    var userKeysBundle: BaseInjectionProvider<UserKeysBundle> {
        get { userKeysBundleProvider }
        set { userKeysBundleProvider = newValue }
    }
    var userDataPackageBuilderV3: BaseInjectionProvider<UserDataPackageBuilderV3> {
        get { userDataPackageBuilderV3Provider }
        set { userDataPackageBuilderV3Provider = newValue }
    }
    var qrCodePayloadBuilderV3: BaseInjectionProvider<QRCodePayloadBuilderV4> {
        get { qrCodePayloadBuilderV3Provider }
        set { qrCodePayloadBuilderV3Provider = newValue }
    }
    var checkOutPayloadBuilderV3: BaseInjectionProvider<CheckOutPayloadBuilderV3> {
        get { checkOutPayloadBuilderV3Provider }
        set { checkOutPayloadBuilderV3Provider = newValue }
    }
    var checkInPayloadBuilderV3: BaseInjectionProvider<CheckInPayloadBuilderV3> {
        get { checkInPayloadBuilderV3Provider }
        set { checkInPayloadBuilderV3Provider = newValue }
    }
    var traceIdAdditionalBuilderV3: BaseInjectionProvider<TraceIdAdditionalDataBuilderV3> {
        get { traceIdAdditionalBuilderV3Provider }
        set { traceIdAdditionalBuilderV3Provider = newValue }
    }
    var privateMeetingQRCodeBuilderV3: BaseInjectionProvider<PrivateMeetingQRCodeBuilderV3> {
        get { privateMeetingQRCodeBuilderV3Provider }
        set { privateMeetingQRCodeBuilderV3Provider = newValue }
    }
    var userTransferBuilderV4: BaseInjectionProvider<UserTransferBuilderV4> {
        get { userTransferBuilderV4Provider }
        set { userTransferBuilderV4Provider = newValue }
    }
}

class DataBuildersInitialiser: BaseServiceInitialiser {

    override func initialise() throws {
        try register(DailyPubKeyHistoryRepository())
            .as(\.dailyKeyRepository) { SharedProvider(value: $0) }

        try register(EphemeralPublicKeyHistoryRepository())
            .as(\.ePubKeyHistoryRepository) { SharedProvider(value: $0) }

        try register(EphemeralPrivateKeyHistoryRepository())
            .as(\.ePrivKeyHistoryRepository) { SharedProvider(value: $0) }

        try register(LocationPrivateKeyHistoryRepository())
            .as(\.locationPrivateKeyHistoryRepository) { SharedProvider(value: $0) }

        try register(UserDataPackageBuilderV3())
            .as(\.userDataPackageBuilderV3) { SharedProvider(value: $0) }

        try register(QRCodePayloadBuilderV4())
            .as(\.qrCodePayloadBuilderV3) { SharedProvider(value: $0) }

        try register(CheckOutPayloadBuilderV3())
            .as(\.checkOutPayloadBuilderV3) { SharedProvider(value: $0) }

        try register(CheckInPayloadBuilderV3())
            .as(\.checkInPayloadBuilderV3) { SharedProvider(value: $0) }

        try register(TraceIdAdditionalDataBuilderV3())
            .as(\.traceIdAdditionalBuilderV3) { SharedProvider(value: $0) }

        try register(PrivateMeetingQRCodeBuilderV3())
            .as(\.privateMeetingQRCodeBuilderV3) { SharedProvider(value: $0) }

        try register(UserTransferBuilderV4())
            .as(\.userTransferBuilderV4) { SharedProvider(value: $0) }

        let userKeysBundle = UserKeysBundle()
        try userKeysBundle.generateKeys()
        userKeysBundle.removeUnusedKeys()
        try register(userKeysBundle)
            .as(\.userKeysBundle) { SharedProvider(value: $0) }
    }
}
