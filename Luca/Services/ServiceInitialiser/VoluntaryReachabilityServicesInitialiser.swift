import DependencyInjection

private var lucaConnectHealthDepartmentServiceProvider: BaseInjectionProvider<LucaConnectHealthDepartmentService> = EmptyProvider()
private var voluntaryReachabilityRequestBuilderProvider: BaseInjectionProvider<VoluntaryReachabilityRequestBuilder> = EmptyProvider()
private var lucaConnectContactDataServiceProvider: BaseInjectionProvider<LucaConnectContactDataService> = EmptyProvider()
private var lucaConnectMessageFetcherProvider: BaseInjectionProvider<LucaConnectMessageFetcherService> = EmptyProvider()
private var lucaConnectDecryptionServiceProvider: BaseInjectionProvider<LucaConnectDecryptionService> = EmptyProvider()

extension DependencyContext {
    var lucaConnectHealthDepartmentService: BaseInjectionProvider<LucaConnectHealthDepartmentService> {
        get { lucaConnectHealthDepartmentServiceProvider }
        set { lucaConnectHealthDepartmentServiceProvider = newValue }
    }
    var voluntaryReachabilityRequestBuilder: BaseInjectionProvider<VoluntaryReachabilityRequestBuilder> {
        get { voluntaryReachabilityRequestBuilderProvider }
        set { voluntaryReachabilityRequestBuilderProvider = newValue }
    }
    var lucaConnectContactDataService: BaseInjectionProvider<LucaConnectContactDataService> {
        get { lucaConnectContactDataServiceProvider }
        set { lucaConnectContactDataServiceProvider = newValue }
    }
    var lucaConnectMessageFetcher: BaseInjectionProvider<LucaConnectMessageFetcherService> {
        get { lucaConnectMessageFetcherProvider }
        set { lucaConnectMessageFetcherProvider = newValue }
    }
    var lucaConnectDecryptionService: BaseInjectionProvider<LucaConnectDecryptionService> {
        get { lucaConnectDecryptionServiceProvider }
        set { lucaConnectDecryptionServiceProvider = newValue }
    }
}

class VoluntaryReachabilityServicesInitialiser: BaseServiceInitialiser {

    override func initialise() throws {
        try register(LucaConnectContactDataService())
            .as("LucaConnectContactDataServiceToggleable") { SharedProvider(value: $0 as Toggleable) }
            .as(\.lucaConnectContactDataService) { SharedProvider(value: $0) }

        try register(LucaConnectHealthDepartmentService())
            .as(\.lucaConnectHealthDepartmentService) { SharedProvider(value: $0) }
            .as("LucaConnectHealthDepartmentServiceToggleable") { SharedProvider(value: $0 as Toggleable) }

        try register(VoluntaryReachabilityRequestBuilder())
            .as(\.voluntaryReachabilityRequestBuilder) { SharedProvider(value: $0) }

        try register(LucaConnectMessageFetcherService())
            .as(\.lucaConnectMessageFetcher) { SharedProvider(value: $0) }

        try register(LucaConnectDecryptionService())
            .as(\.lucaConnectDecryptionService) { SharedProvider(value: $0) }
    }
}
