import Foundation
import DependencyInjection
import RxSwift
import RealmSwift

private var _localDBKeyRepository: BaseInjectionProvider<DataKeyRepository> = EmptyProvider()

private var _traceInfoRepo: BaseInjectionProvider<DataRepo<TraceInfo>> = EmptyProvider()
private var _traceIdCoreRepo: BaseInjectionProvider<DataRepo<TraceIdCore>> = EmptyProvider()
private var _locationRepo: BaseInjectionProvider<DataRepo<Location>> = EmptyProvider()
private var _campaignRepo: BaseInjectionProvider<DataRepo<CampaignSubmissionData>> = EmptyProvider()
private var _keyValueRepo: BaseInjectionProvider<KeyValueRepoProtocol> = EmptyProvider()
private var _importantDataKeyValueRepo: BaseInjectionProvider<KeyValueRepoProtocol> = EmptyProvider()
private var _historyRepo: BaseInjectionProvider<DataRepo<HistoryEntry>> = EmptyProvider()
private var _documentRepo: BaseInjectionProvider<DataRepo<DocumentPayload>> = EmptyProvider()
private var _testProviderKeyRepo: BaseInjectionProvider<DataRepo<TestProviderKey>> = EmptyProvider()
private var _personRepo: BaseInjectionProvider<DataRepo<Person>> = EmptyProvider()
private var _notificationHealthDepartmentRepo: BaseInjectionProvider<DataRepo<NotificationHealthDepartment>> = EmptyProvider()
private var _accessedTraceIdRepo: BaseInjectionProvider<DataRepo<AccessedTraceId>> = EmptyProvider()
private var _fetchedMessagesRepo: BaseInjectionProvider<DataRepo<FetchedMessage>> = EmptyProvider()
private var _localNewsMessageRepo: BaseInjectionProvider<DataRepo<LocalNewsMessage>> = EmptyProvider()

private var _lucaPreferences: BaseInjectionProvider<LucaPreferences> = EmptyProvider()

extension DependencyContext {
    var localDBKeyRepository: BaseInjectionProvider<DataKeyRepository> {
        get { _localDBKeyRepository }
        set { _localDBKeyRepository = newValue }
    }

    var traceInfoRepo: BaseInjectionProvider<DataRepo<TraceInfo>> {
        get { _traceInfoRepo }
        set { _traceInfoRepo = newValue }
    }

    var lucaPreferences: BaseInjectionProvider<LucaPreferences> {
        get { _lucaPreferences }
        set { _lucaPreferences = newValue }
    }

    var traceIdCoreRepo: BaseInjectionProvider<DataRepo<TraceIdCore>> {
        get { _traceIdCoreRepo }
        set { _traceIdCoreRepo = newValue }
    }

    var locationRepo: BaseInjectionProvider<DataRepo<Location>> {
        get { _locationRepo }
        set { _locationRepo = newValue }
    }

    var campaignRepo: BaseInjectionProvider<DataRepo<CampaignSubmissionData>> {
        get { _campaignRepo }
        set { _campaignRepo = newValue }
    }

    var fetchedMessagesRepo: BaseInjectionProvider<DataRepo<FetchedMessage>> {
        get { _fetchedMessagesRepo }
        set { _fetchedMessagesRepo = newValue }
    }

    var keyValueRepo: BaseInjectionProvider<KeyValueRepoProtocol> {
        get { _keyValueRepo }
        set { _keyValueRepo = newValue }
    }

    /// Reserved for data, that cannot be removed
    var importantDataKeyValueRepo: BaseInjectionProvider<KeyValueRepoProtocol> {
        get { _importantDataKeyValueRepo }
        set { _importantDataKeyValueRepo = newValue }
    }
    var historyRepo: BaseInjectionProvider<DataRepo<HistoryEntry>> {
        get { _historyRepo }
        set { _historyRepo = newValue }
    }
    var documentRepo: BaseInjectionProvider<DataRepo<DocumentPayload>> {
        get { _documentRepo }
        set { _documentRepo = newValue }
    }
    var testProviderKeyRepo: BaseInjectionProvider<DataRepo<TestProviderKey>> {
        get { _testProviderKeyRepo }
        set { _testProviderKeyRepo = newValue }
    }
    var personRepo: BaseInjectionProvider<DataRepo<Person>> {
        get { _personRepo }
        set { _personRepo = newValue }
    }
    var notificationHealthDepartmentRepo: BaseInjectionProvider<DataRepo<NotificationHealthDepartment>> {
        get { _notificationHealthDepartmentRepo }
        set { _notificationHealthDepartmentRepo = newValue }
    }
    var accessedTraceIdRepo: BaseInjectionProvider<DataRepo<AccessedTraceId>> {
        get { _accessedTraceIdRepo }
        set { _accessedTraceIdRepo = newValue }
    }
    var localNewsMessageRepo: BaseInjectionProvider<DataRepo<LocalNewsMessage>> {
        get { _localNewsMessageRepo }
        set { _localNewsMessageRepo = newValue }
    }
}

class ReposInitialiser: BaseServiceInitialiser {

    @InjectAllDynamic private var realmDatabaseUtils: [RealmDatabaseUtils]

    override func initialise() throws {
        let localDBKeyRepository = DataKeyRepository(tag: "LocalDBKey")

        try register(localDBKeyRepository)
            .as(\.localDBKeyRepository) { SharedProvider(value: $0) }

        var currentKey: Data! = try? localDBKeyRepository.retrieveKey()
        let keyWasAvailable = currentKey != nil
        if !keyWasAvailable {
            self.log("No DB Key found, generating one...")
            guard let bytes = KeyFactory.randomBytes(size: 64) else {
                throw NSError(domain: "Couldn't generate random bytes for local DB key", code: 0, userInfo: nil)
            }
            if !localDBKeyRepository.store(key: bytes, removeIfExists: true) {
                throw NSError(domain: "Couldn't store local DB key", code: 0, userInfo: nil)
            }
            currentKey = bytes
            self.log("DB Key generated and stored succesfully.")
        }

        try registerRepos(with: currentKey)

#if DEBUG
guard ProcessInfo.processInfo.environment["XCODE_RUNNING_FOR_PREVIEWS"] != "1" else {
    return
}
#endif

        if !keyWasAvailable {
            self.log("Applying new key to the repos")

            let changeEncryptionCompletables = self.realmDatabaseUtils
                .map { $0.changeEncryptionSettings(oldKey: nil, newKey: currentKey)
                    .logError(self, "\(String(describing: $0.self)): Changing encryption")
                }

            try Completable.concat(changeEncryptionCompletables)
            .debug("KT TOTAL")
            .do(onError: { error in
                fatalError("failed to change encryption settings. Error: \(error)")
            })
            .blockingWait(nil) // This blocking is crucial here. I want to block the app until the settings are done.

            self.log("New keys applied successfully")
        }
    }

    // swiftlint:disable force_cast
    private func registerRepos(with currentKey: Data) throws {
        try register(TraceInfoRepo(key: currentKey))
            .as(\.traceInfoRepo) { SharedProvider(value: $0) }
            .as("RealmDatabaseUtil00") { SharedProvider(value: $0 as! RealmDatabaseUtils) }

        try register(TraceIdCoreRepo(key: currentKey))
            .as(\.traceIdCoreRepo) { SharedProvider(value: $0) }
            .as("RealmDatabaseUtil01") { SharedProvider(value: $0 as! RealmDatabaseUtils) }

        try register(LocationRepo(key: currentKey))
            .as(\.locationRepo) { SharedProvider(value: $0) }
            .as("RealmDatabaseUtil02") { SharedProvider(value: $0 as! RealmDatabaseUtils) }

        try register(RealmKeyValueRepo(key: currentKey, filenameSalt: "RealmKeyValueUnderlyingRepo"))
            .as(\.keyValueRepo) { SharedProvider(value: $0) }
            .as("RealmDatabaseUtil03") { SharedProvider(value: $0 as! RealmDatabaseUtils) }

        try register(RealmKeyValueRepo(key: currentKey, filenameSalt: "RealmKeyValueUnderlyingRepoForImportantData"))
            .as(\.importantDataKeyValueRepo) { SharedProvider(value: $0) }
            .as("RealmDatabaseUtil04") { SharedProvider(value: $0 as! RealmDatabaseUtils) }

        try register(HistoryRepo(key: currentKey))
            .as(\.historyRepo) { SharedProvider(value: $0) }
            .as("RealmDatabaseUtil06") { SharedProvider(value: $0 as! RealmDatabaseUtils) }

        try register(DocumentRepo(key: currentKey))
            .as(\.documentRepo) { SharedProvider(value: $0) }
            .as("RealmDatabaseUtil07") { SharedProvider(value: $0 as! RealmDatabaseUtils) }

        try register(TestProviderKeyRepo(key: currentKey))
            .as(\.testProviderKeyRepo) { SharedProvider(value: $0) }
            .as("RealmDatabaseUtil08") { SharedProvider(value: $0 as! RealmDatabaseUtils) }

        try register(PersonRepo(key: currentKey))
            .as(\.personRepo) { SharedProvider(value: $0) }
            .as("RealmDatabaseUtil09") { SharedProvider(value: $0 as! RealmDatabaseUtils) }

        try register(NotificationHealthDepartmentRepo(key: currentKey))
            .as(\.notificationHealthDepartmentRepo) { SharedProvider(value: $0) }
            .as("RealmDatabaseUtil10") { SharedProvider(value: $0 as! RealmDatabaseUtils) }

        try register(AccessedTraceIdRepo(key: currentKey))
            .as(\.accessedTraceIdRepo) { SharedProvider(value: $0) }
            .as("RealmDatabaseUtil11") { SharedProvider(value: $0 as! RealmDatabaseUtils) }

        try register(FetchedMessageRepo(key: currentKey))
            .as(\.fetchedMessagesRepo) { SharedProvider(value: $0) }
            .as("RealmDatabaseUtil12") { SharedProvider(value: $0 as! RealmDatabaseUtils) }

        try register(LocalNewsMessageRepo(key: currentKey))
            .as(\.localNewsMessageRepo) { SharedProvider(value: $0) }
            .as("RealmDatabaseUtil13") { SharedProvider(value: $0 as! RealmDatabaseUtils) }

        try register(CampaignSubmissionRepo(key: currentKey))
            .as(\.campaignRepo) { SharedProvider(value: $0) }
            .as("RealmDatabaseUtil14") { SharedProvider(value: $0 as! RealmDatabaseUtils) }

        try register(LucaPreferences())
            .as(\.lucaPreferences) { SharedProvider(value: $0) }

        excludeExistingRealmFilesFromCloudBackups()
    }

    // MARK: Cloud Backup Exclusion

    private func excludeExistingRealmFilesFromCloudBackups() {
        let defaultRealmDirectory = Realm.Configuration.defaultConfiguration.fileURL!.deletingLastPathComponent()
        guard let directoryContents = try? FileManager.default.contentsOfDirectory(at: defaultRealmDirectory, includingPropertiesForKeys: []) else { return }
        for var fileURL in directoryContents where fileURL.path.hasSuffix(".realm") {
            fileURL.excludeFromCloudBackups()
        }
    }
}

extension ReposInitialiser: UnsafeAddress, LogUtil {}
