import Foundation
import DependencyInjection

class BaseContract {
    fileprivate var invalidateClosures: [() -> Void] = []

    private var invalidated = false

    fileprivate func invalidate() {
        if invalidated {
            fatalError("This contract has been invalidated!")
        }
        for closure in invalidateClosures {
            closure()
        }
        invalidateClosures = []
        invalidated = true
    }
}

class InitialisationContractWithFactory<T>: BaseContract {
    var factory: () -> T

    init(factory: @escaping () -> T) {
        self.factory = factory
    }

    @discardableResult
    func `as`<Provider>(_ path: WritableKeyPath<DependencyContext, Provider>, providerFactory: @escaping (() -> T) -> Provider) -> Self where Provider: InjectionProvider, Provider.T == T {
        DependencyContext[path] = providerFactory(self.factory)

        // swiftlint:disable:next force_cast
        invalidateClosures.append { DependencyContext[path] = EmptyProvider<T>() as! Provider }
        return self
    }

    @discardableResult
    func `as`<MappedProvider, MappedValue>(_ path: String, map: @escaping (() -> T) -> MappedProvider) throws -> Self where MappedProvider: InjectionProvider, MappedProvider.T == MappedValue {
        try DependencyContext.register(map(self.factory), keyIdentifier: path)
        invalidateClosures.append { DependencyContext.remove(for: path) }
        return self
    }
}

class InitialisationContract<T>: BaseContract {
    var value: T

    init(value: T) {
        self.value = value
    }

    @discardableResult
    func `as`<Provider>(_ path: WritableKeyPath<DependencyContext, Provider>, providerFactory: @escaping (T) -> Provider) -> Self where Provider: InjectionProvider, Provider.T == T {
        DependencyContext[path] = providerFactory(self.value)
        // swiftlint:disable:next force_cast
        invalidateClosures.append { DependencyContext[path] = EmptyProvider<T>() as! Provider }
        return self
    }

    @discardableResult
    func `as`<MappedProvider, MappedValue>(_ path: String, map: @escaping (T) -> MappedProvider) throws -> Self where MappedProvider: InjectionProvider, MappedProvider.T == MappedValue {
        try DependencyContext.register(map(self.value), keyIdentifier: path)
        invalidateClosures.append { DependencyContext.remove(for: path) }
        return self
    }
}

/// Class that simplifies initialising and deinitialising of services.
class BaseServiceInitialiser: ServiceInitialiser {

    private var contracts: [BaseContract] = []
    private static var eventHandlerCount = 0

    /// Registers a factory of a service. Use this for lazy instance creation. Can be combined with providers such as `AlwaysNewProvider`
    ///
    /// This type of registration _doesn't_ connect Event Handlers such as `AppStateBackgroundHandler` etc.
    func register<T>(_ factory: @escaping () -> T) -> InitialisationContractWithFactory<T> {
        let retVal = InitialisationContractWithFactory(factory: factory)
        contracts.append(retVal)
        return retVal
    }

    /// Registers a service.
    ///
    /// It registers also all EventHandlers such as `AppStateBackgroundHandler`.
    func register<T>(_ value: T) throws -> InitialisationContract<T> {
        let retVal = InitialisationContract(value: value)
        contracts.append(retVal)

        try registerHandler(value, as: AppStateHandler.self)
        try registerHandler(value, as: MemoryWarningHandler.self)
        return retVal
    }

    private func registerHandler<T, V>(_ value: T, as: V.Type) throws {
        if let eventHandler = value as? V {
            let contract = try InitialisationContract(value: eventHandler)
                .as("\(String(describing: value)).\(BaseServiceInitialiser.eventHandlerCount)") { SharedProvider(value: $0) }
            contracts.append(contract)
            BaseServiceInitialiser.eventHandlerCount += 1
        }
    }

    func initialise() throws {
    }

    func invalidate() {
        for contract in contracts {
            contract.invalidate()
        }
        contracts = []
    }
}
