import Foundation
import DependencyInjection

private var dataResetServiceProvider: BaseInjectionProvider<DataResetService> = EmptyProvider<DataResetService>()
private var appServiceInitialiserProvider = SharedProvider<AppServicesInitialiser>(value: AppServicesInitialiser())
private var timeProviderProvider: BaseInjectionProvider<TimeProvider> = EmptyProvider()

extension DependencyContext {
    var appServicesInitialiser: SharedProvider<AppServicesInitialiser> {
        get { appServiceInitialiserProvider }
        set { appServiceInitialiserProvider = newValue }
    }
    var dataResetService: BaseInjectionProvider<DataResetService> {
        get { dataResetServiceProvider }
        set { dataResetServiceProvider = newValue }
    }
    var timeProvider: BaseInjectionProvider<TimeProvider> {
        get { timeProviderProvider }
        set { timeProviderProvider = newValue }
    }
}

class AppServicesInitialiser: BaseServiceInitialiser, Toggleable {

    private var alreadyInitialised = false
    private(set) var isEnabled: Bool = false

    @InjectAllDynamic private var toggleables: [Toggleable]

    private let initialisers: [ServiceInitialiser] = [
        CryptoInitialiser(),
        BackendInitialiser(),
        DataBuildersInitialiser(),
        ReposInitialiser(),
        MiscServicesInitialiser(),
        DocumentsServicesInitialiser(),
        DGCVerificationServiceInitialiser(),
        VoluntaryReachabilityServicesInitialiser(),
        LucaIDInitialiser(),
        ExplanationServiceInitialiser()
    ]

    override func initialise() throws {
        if alreadyInitialised {
            return
        }
        try register(DefaultTimeProvider())
            .as(\.timeProvider) { SharedProvider(value: $0) }

        for initialiser in initialisers {
            try initialiser.initialise()
        }
        try register(DataResetService())
            .as(\.dataResetService) { SharedProvider(value: $0) }

        try register(EventHandlerTrigger())
            .as("EventHandlerTriggerToggleable") { SharedProvider(value: $0 as Toggleable) }

        // Enable all toggleable services
        enable()

        alreadyInitialised = true
    }

    override func invalidate() {
        // Disable all toggleable services
        disable()

        for initialiser in initialisers.reversed() {
            initialiser.invalidate()
        }
        alreadyInitialised = false

        // Invalidate all automatic contracts later on
        super.invalidate()
    }

    func enable() {
        if isEnabled {
            return
        }
        for t in toggleables {

            // Skip LocationUpdater for the start
            if t is LocationUpdater {
                continue
            }
            t.enable()
        }
        isEnabled = true
    }

    func disable() {
        for t in toggleables {
            t.disable()
        }
        isEnabled = false
    }
}
