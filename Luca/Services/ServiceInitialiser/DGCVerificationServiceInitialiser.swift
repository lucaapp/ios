import DependencyInjection

private var dgcVerificationServiceProvider: BaseInjectionProvider<DGCVerificationService> = EmptyProvider<DGCVerificationService>()
private var dgcKeyDataCacheProvider: BaseInjectionProvider<CachedDataSource<DGCVerificationCertificateData>> = EmptyProvider<CachedDataSource<DGCVerificationCertificateData>>()

extension DependencyContext {
    var dgcVerificationService: BaseInjectionProvider<DGCVerificationService> {
        get { dgcVerificationServiceProvider }
        set { dgcVerificationServiceProvider = newValue }
    }
    var dgcKeyDataCache: BaseInjectionProvider<CachedDataSource<DGCVerificationCertificateData>> {
        get { dgcKeyDataCacheProvider }
        set { dgcKeyDataCacheProvider = newValue }
    }
}

class DGCVerificationServiceInitialiser: BaseServiceInitialiser {

    override func initialise() throws {

#if PRODUCTION
        let dgcCertDataCacheValidity = CacheValidity.until(unit: .hour, count: 24)
#else
        let dgcCertDataCacheValidity = CacheValidity.onceInRuntime
#endif

        let dgcCertDataV4 = DGCVerificationCertificateDataV4(backendAddress: BackendAddressV4())
        let cachedDGCCertDataSource = BaseCachedDataSource(
            dataSource: dgcCertDataV4.fetchCerts(),
            cacheDataRepo: KeyValueRepoCacheWrapper(uniqueCacheKey: "DGCVerificationCertificateData"),
            cacheValidity: dgcCertDataCacheValidity,
            uniqueCacheIdentifier: "DGCVerificationCertificateDataCache"
        )
        try register(cachedDGCCertDataSource)
            .as(\.dgcKeyDataCache) { SharedProvider(value: $0) }

        try register(DGCVerificationService())
            .as(\.dgcVerificationService) { SharedProvider(value: $0) }

    }
}

extension DGCVerificationServiceInitialiser: UnsafeAddress, LogUtil {}
