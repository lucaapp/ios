import Foundation
import DependencyInjection

private var randomNumberGeneratorProvider: BaseInjectionProvider<RandomNumberGenerator> = EmptyProvider()
private var commonCryptoProvider: BaseInjectionProvider<CommonCrypto> = EmptyProvider()
private var keyGenProvider: BaseInjectionProvider<KeyGen> = EmptyProvider()

extension DependencyContext {
    var randomNumberGenerator: BaseInjectionProvider<RandomNumberGenerator> {
        get { randomNumberGeneratorProvider }
        set { randomNumberGeneratorProvider = newValue }
    }
    var commonCrypto: BaseInjectionProvider<CommonCrypto> {
        get { commonCryptoProvider }
        set { commonCryptoProvider = newValue }
    }
    var keyGen: BaseInjectionProvider<KeyGen> {
        get { keyGenProvider }
        set { keyGenProvider = newValue }
    }
}

class CryptoInitialiser: BaseServiceInitialiser {

    override func initialise() throws {
        try register(StandardRandomNumberGenerator())
            .as(\.randomNumberGenerator) { SharedProvider(value: $0) }

        try register(LucaCommonCrypto())
            .as(\.commonCrypto) { SharedProvider(value: $0) }

        try register(LucaKeyGen())
            .as(\.keyGen) { SharedProvider(value: $0) }
    }
}
