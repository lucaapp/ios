import Foundation
import DependencyInjection
import RxSwift

private var versionCheckerProvider: BaseInjectionProvider<VersionChecker> = EmptyProvider()
private var timeSyncProvider: BaseInjectionProvider<TimeSyncService> = EmptyProvider()
private var dailyPublicKeyVerifierProvider: BaseInjectionProvider<DailyPublicKeyVerifier> = EmptyProvider()
private var userServiceProvider: BaseInjectionProvider<UserService> = EmptyProvider()
private var locationUpdaterProvider: BaseInjectionProvider<LocationUpdater> = EmptyProvider()
private var autoCheckoutServiceProvider: BaseInjectionProvider<AutoCheckoutService> = EmptyProvider()
private var traceIdServiceProvider: BaseInjectionProvider<TraceIdService> = EmptyProvider()
private var localCheckinServiceProvider: BaseInjectionProvider<LocalCheckinService> = EmptyProvider()
private var historyProvider: BaseInjectionProvider<HistoryService> = EmptyProvider()
private var historyListenerProvider: BaseInjectionProvider<HistoryEventListener> = EmptyProvider()
private var selfCheckinProvider: BaseInjectionProvider<SelfCheckinService> = EmptyProvider()
private var privateMeetingServiceProvider: BaseInjectionProvider<PrivateMeetingService> = EmptyProvider()
private var userSecrectsConsistencyCheckerProvider: BaseInjectionProvider<UserSecretsConsistencyChecker> = EmptyProvider()
private var accessDataChunkHandlerProvider: BaseInjectionProvider<AccessDataChunkHandler> = EmptyProvider()
private var accessTraceIdCheckerProvider: BaseInjectionProvider<AccessedTraceIdChecker> = EmptyProvider()
private var accessTraceIdNotifierProvider: BaseInjectionProvider<AccessedTraceIdNotifier> = EmptyProvider()
private var personServiceProvider: BaseInjectionProvider<PersonService> = EmptyProvider()
private var baerCodeKeyServiceProvider: BaseInjectionProvider<BaerCodeKeyService> = EmptyProvider()
private var notificationServiceProvider: BaseInjectionProvider<NotificationService> = EmptyProvider()
private var contactDataReporterProvider: BaseInjectionProvider<ContentReporterService> = EmptyProvider()
private var documentsReporterProvider: BaseInjectionProvider<ContentReporterService> = EmptyProvider()
private var lucaIDReporterProvider: BaseInjectionProvider<ContentReporterService> = EmptyProvider()
private var paymentDataReporterProvider: BaseInjectionProvider<ContentReporterService> = EmptyProvider()
private var checkinPollingProvider: BaseInjectionProvider<CheckinStatusPollingService> = EmptyProvider()
private var reachabilityServiceProvider: BaseInjectionProvider<ReachabilityService> = EmptyProvider()
private var lucaLocationPermissionWorkflowProvider: BaseInjectionProvider<LucaLocationPermissionWorkflow> = EmptyProvider()
private var checkinTimerProvider: BaseInjectionProvider<CheckinTimer> = EmptyProvider()
private var powServiceProvider: BaseInjectionProvider<PoWService> = EmptyProvider()
private var stagedRolloutServiceProvider: BaseInjectionProvider<StagedRolloutService> = EmptyProvider()
private var lucaPaymentProvider: BaseInjectionProvider<LucaPaymentService> = EmptyProvider()
private var lucaPaymentConsentProvider: BaseInjectionProvider<LucaPaymentConsent> = EmptyProvider()
private var campaignServiceProvider: BaseInjectionProvider<CampaignService> = EmptyProvider()

extension DependencyContext {
    var versionChecker: BaseInjectionProvider<VersionChecker> {
        get { versionCheckerProvider }
        set { versionCheckerProvider = newValue }
    }
    var timeSync: BaseInjectionProvider<TimeSyncService> {
        get { timeSyncProvider }
        set { timeSyncProvider = newValue }
    }
    var dailyPublicKeyVerifier: BaseInjectionProvider<DailyPublicKeyVerifier> {
        get { dailyPublicKeyVerifierProvider }
        set { dailyPublicKeyVerifierProvider = newValue }
    }
    var userService: BaseInjectionProvider<UserService> {
        get { userServiceProvider }
        set { userServiceProvider = newValue }
    }
    var locationUpdater: BaseInjectionProvider<LocationUpdater> {
        get { locationUpdaterProvider }
        set { locationUpdaterProvider = newValue }
    }
    var autoCheckoutService: BaseInjectionProvider<AutoCheckoutService> {
        get { autoCheckoutServiceProvider }
        set { autoCheckoutServiceProvider = newValue }
    }
    var traceIdService: BaseInjectionProvider<TraceIdService> {
        get { traceIdServiceProvider }
        set { traceIdServiceProvider = newValue }
    }
    var localCheckinService: BaseInjectionProvider<LocalCheckinService> {
        get { localCheckinServiceProvider }
        set { localCheckinServiceProvider = newValue }
    }
    var history: BaseInjectionProvider<HistoryService> {
        get { historyProvider }
        set { historyProvider = newValue }
    }
    var historyListener: BaseInjectionProvider<HistoryEventListener> {
        get { historyListenerProvider }
        set { historyListenerProvider = newValue }
    }
    var selfCheckin: BaseInjectionProvider<SelfCheckinService> {
        get { selfCheckinProvider }
        set { selfCheckinProvider = newValue }
    }
    var privateMeetingService: BaseInjectionProvider<PrivateMeetingService> {
        get { privateMeetingServiceProvider }
        set { privateMeetingServiceProvider = newValue }
    }
    var userSecrectsConsistencyChecker: BaseInjectionProvider<UserSecretsConsistencyChecker> {
        get { userSecrectsConsistencyCheckerProvider }
        set { userSecrectsConsistencyCheckerProvider = newValue }
    }
    var accessDataChunkHandler: BaseInjectionProvider<AccessDataChunkHandler> {
        get { accessDataChunkHandlerProvider }
        set { accessDataChunkHandlerProvider = newValue }
    }
    var accessTraceIdChecker: BaseInjectionProvider<AccessedTraceIdChecker> {
        get { accessTraceIdCheckerProvider }
        set { accessTraceIdCheckerProvider = newValue }
    }
    var accessTraceIdNotifier: BaseInjectionProvider<AccessedTraceIdNotifier> {
        get { accessTraceIdNotifierProvider }
        set { accessTraceIdNotifierProvider = newValue }
    }
    var personService: BaseInjectionProvider<PersonService> {
        get { personServiceProvider }
        set { personServiceProvider = newValue }
    }
    var baerCodeKeyService: BaseInjectionProvider<BaerCodeKeyService> {
        get { baerCodeKeyServiceProvider }
        set { baerCodeKeyServiceProvider = newValue }
    }
    var notificationService: BaseInjectionProvider<NotificationService> {
        get { notificationServiceProvider }
        set { notificationServiceProvider = newValue }
    }
    var contactDataReporter: BaseInjectionProvider<ContentReporterService> {
        get { contactDataReporterProvider }
        set { contactDataReporterProvider = newValue }
    }
    var documentsReporter: BaseInjectionProvider<ContentReporterService> {
        get { documentsReporterProvider }
        set { documentsReporterProvider = newValue }
    }
    var lucaIDReporter: BaseInjectionProvider<ContentReporterService> {
        get { lucaIDReporterProvider }
        set { lucaIDReporterProvider = newValue }
    }
    
    var paymentDataReporter: BaseInjectionProvider<ContentReporterService> {
        get { paymentDataReporterProvider }
        set { paymentDataReporterProvider = newValue }
    }
    var checkinPolling: BaseInjectionProvider<CheckinStatusPollingService> {
        get { checkinPollingProvider }
        set { checkinPollingProvider = newValue }
    }
    var reachabilityService: BaseInjectionProvider<ReachabilityService> {
        get { reachabilityServiceProvider }
        set { reachabilityServiceProvider = newValue }
    }
    var lucaLocationPermissionWorkflow: BaseInjectionProvider<LucaLocationPermissionWorkflow> {
        get { lucaLocationPermissionWorkflowProvider }
        set { lucaLocationPermissionWorkflowProvider = newValue }
    }
    var checkinTimer: BaseInjectionProvider<CheckinTimer> {
        get { checkinTimerProvider }
        set { checkinTimerProvider = newValue }
    }
    var powService: BaseInjectionProvider<PoWService> {
        get { powServiceProvider }
        set { powServiceProvider = newValue }
    }
    var stagedRolloutService: BaseInjectionProvider<StagedRolloutService> {
        get { stagedRolloutServiceProvider }
        set { stagedRolloutServiceProvider = newValue }
    }
    var lucaPayment: BaseInjectionProvider<LucaPaymentService> {
        get { lucaPaymentProvider }
        set { lucaPaymentProvider = newValue }
    }
    var lucaPaymentConsent: BaseInjectionProvider<LucaPaymentConsent> {
        get { lucaPaymentConsentProvider }
        set { lucaPaymentConsentProvider = newValue }
    }
    var campaignService: BaseInjectionProvider<CampaignService> {
        get { campaignServiceProvider }
        set { campaignServiceProvider = newValue }
    }
}

class MiscServicesInitialiser: BaseServiceInitialiser {

    @InjectAllDynamic private var toggleables: [Toggleable]

    override func initialise() throws {
        try register(VersionChecker())
            .as(\.versionChecker) { SharedProvider(value: $0) }
            .as("VersionCheckerToggleable") { SharedProvider(value: $0 as Toggleable) }

        try register(CheckinTimer())
            .as(\.checkinTimer) { SharedProvider(value: $0) }

        try register(TimeSyncService())
            .as(\.timeSync) { SharedProvider(value: $0) }

        #if PRODUCTION
        let filenamePrefix = "prod"
        #else
        let filenamePrefix = "dev"
        #endif

        let root = Single.from { Bundle.main.url(forResource: filenamePrefix + "_ca_root", withExtension: "pem") }
            .unwrapOptional()
            .map { try String(contentsOf: $0) }

        let intermediate = Single.from { Bundle.main.url(forResource: filenamePrefix + "_ca_intermediate", withExtension: "pem") }
            .unwrapOptional()
            .map { try String(contentsOf: $0) }

        let dailyPubKeyVerifier = DailyPublicKeyVerifier(
            rootCertificateSource: root,
            intermediateCertificatesSource: intermediate.map { [$0] }
        )
        try register(dailyPubKeyVerifier)
            .as(\.dailyPublicKeyVerifier) { SharedProvider(value: $0) }
            .as("DailyPublicKeyVerifierToggleable") { SharedProvider(value: $0 as Toggleable) }

        try register(ReachabilityService())
            .as(\.reachabilityService) { SharedProvider(value: $0) }
            .as("ReachabilityServiceToggleable") { SharedProvider(value: $0 as Toggleable) }

        try register(LocationUpdater())
            .as(\.locationUpdater) { SharedProvider(value: $0) }
            .as("LocationUpdaterToggleable") { SharedProvider(value: $0 as Toggleable) }

        try register(PrivateMeetingService(preferences: UserDataPreferences(suiteName: "locations")))
            .as(\.privateMeetingService) { SharedProvider(value: $0) }

        try register(UserService())
            .as(\.userService) { SharedProvider(value: $0) }

        try register(TraceIdService(preferences: UserDataPreferences(suiteName: "traceIdService")))
            .as(\.traceIdService) { SharedProvider(value: $0) }

        try register(LocalCheckinService())
            .as(\.localCheckinService) { SharedProvider(value: $0) }

        let autoCheckoutService = AutoCheckoutService()
        autoCheckoutService.register(regionDetector: RegionMonitoringDetector())
//        autoCheckoutService.register(regionDetector: LocationUpdatesRegionDetector(locationUpdater: locationUpdater, allowedAppStates: [.active]))
        autoCheckoutService.register(regionDetector: SingleLocationRequestRegionDetector(allowedAppStates: [.active]))
        try register(autoCheckoutService)
            .as(\.autoCheckoutService) { SharedProvider(value: $0) }
            .as("AutoCheckoutServiceToggleable") { SharedProvider(value: $0 as Toggleable) }

        try register(HistoryService())
            .as(\.history) { SharedProvider(value: $0) }

        try register(HistoryEventListener())
            .as(\.historyListener) { SharedProvider(value: $0) }
            .as("HistoryListenerToggleable") { SharedProvider(value: $0 as Toggleable) }

        try register(SelfCheckinService())
            .as(\.selfCheckin) { SharedProvider(value: $0) }

        try register(UserSecretsConsistencyChecker())
            .as(\.userSecrectsConsistencyChecker) { SharedProvider(value: $0) }
            .as("UserSecretsCheckerToggleable") { SharedProvider(value: $0 as Toggleable) }

        try register(AccessDataChunkHandler())
            .as(\.accessDataChunkHandler) { SharedProvider(value: $0) }

        try register(AccessedTraceIdChecker())
            .as(\.accessTraceIdChecker) { SharedProvider(value: $0) }
            .as("AccessedTraceIdCheckerToggleable") { SharedProvider(value: $0 as Toggleable) }

        try register(AccessedTraceIdNotifier(notificationScheduler: NotificationScheduler.shared))
            .as(\.accessTraceIdNotifier) { SharedProvider(value: $0) }
            .as("AccessTraceIdNotifierToggleable") { SharedProvider(value: $0 as Toggleable) }

        try register(BaerCodeKeyService())
            .as(\.baerCodeKeyService) { SharedProvider(value: $0) }

        try register(NotificationService())
            .as(\.notificationService) { SharedProvider(value: $0) }
            .as("NotificationServiceToggleable") { SharedProvider(value: $0 as Toggleable) }

        try register(PersonService())
            .as(\.personService) { SharedProvider(value: $0) }

        try register(CheckinStatusPollingService())
            .as(\.checkinPolling) { SharedProvider(value: $0) }
            .as("CheckinPollingToggleable") { SharedProvider(value: $0 as Toggleable) }

        try register(LucaLocationPermissionWorkflow())
            .as(\.lucaLocationPermissionWorkflow) { SharedProvider(value: $0) }

        try register(PoWService())
            .as(\.powService) { SharedProvider(value: $0) }
            .as("PoWServiceToggleable") { SharedProvider(value: $0 as Toggleable) }

        try register(StagedRolloutService())
            .as(\.stagedRolloutService) { SharedProvider(value: $0) }
            .as("StagedRolloutServiceToggeable") { SharedProvider(value: $0 as Toggleable) }

        try register(LucaPaymentService())
            .as(\.lucaPayment) { SharedProvider(value: $0) }

        try register(LucaPaymentConsent())
            .as(\.lucaPaymentConsent) { SharedProvider(value: $0) }
        
        try register(CampaignService())
            .as(\.campaignService) {
                SharedProvider(value: $0) }

        try registerContentReporterServices()
    }

    private func registerContentReporterServices() throws {
        try register(contactDataReporterService())
            .as(\.contactDataReporter) { SharedProvider(value: $0) }
        try register(documentsReporterService())
            .as(\.documentsReporter) { SharedProvider(value: $0) }
        try register(lucaIDContentReporterService())
            .as(\.lucaIDReporter) { SharedProvider(value: $0) }
        try register(paymentDataReporterService())
            .as(\.paymentDataReporter) { SharedProvider(value: $0) }
    }

    func contactDataReporterService() -> ContentReporterService {
        let contactDataReporterService = ContentReporterService()
        contactDataReporterService.register(contentReporter: StaticStringReporter(text: L10n.IOSApp.DataReport.ContactData.header))
        contactDataReporterService.register(contentReporter: UserDataReporter())
        contactDataReporterService.register(contentReporter: HistoryReporter())
        contactDataReporterService.register(contentReporter: TraceInfoReporter())
        contactDataReporterService.register(contentReporter: PrivacyPolicyReporter())
        return contactDataReporterService
    }

    func documentsReporterService() -> ContentReporterService {
        let documentsReporterService = ContentReporterService()
        documentsReporterService.register(contentReporter: StaticStringReporter(text: L10n.IOSApp.DataReport.Document.header))
        documentsReporterService.register(contentReporter: DocumentReporter())
        documentsReporterService.register(contentReporter: PrivacyPolicyReporter())
        return documentsReporterService
    }

    func lucaIDContentReporterService() -> ContentReporterService {
        let lucaIDReporterService = ContentReporterService()
        lucaIDReporterService.register(contentReporter: StaticStringReporter(text: L10n.IOSApp.DataReport.LucaID.header))
        lucaIDReporterService.register(contentReporter: LucaIDReporter())
        return lucaIDReporterService
    }
    
    func paymentDataReporterService() -> ContentReporterService {
        let paymentDataReporterService = ContentReporterService()
        paymentDataReporterService.register(contentReporter: StaticStringReporter(text: L10n.IOSApp.DataReport.Payment.prefix))
        paymentDataReporterService.register(contentReporter: PaymentDataReporter())
        return paymentDataReporterService
        
    }
}
