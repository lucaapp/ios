import Foundation
import DependencyInjection

private var lucaIDKeysBundleProvider: BaseInjectionProvider<LucaIDKeysBundle> = EmptyProvider()
private var lucaIDServiceProvider: BaseInjectionProvider<LucaIDService> = EmptyProvider()
private var attestationServiceProvider: BaseInjectionProvider<LucaIDVerificationPreconditionsCheckService> = EmptyProvider()
private var lucaIDProcessFetcherServiceProvider: BaseInjectionProvider<LucaIdProcessFetcherService> = EmptyProvider()
private var lucaIDMessageServiceProvider: BaseInjectionProvider<LucaIDMessageService> = EmptyProvider()
private var lucaIDTermsOfUseProvider: BaseInjectionProvider<LucaIDTermsOfUseService> = EmptyProvider()

extension DependencyContext {
    var lucaIDKeysBundle: BaseInjectionProvider<LucaIDKeysBundle> {
        get { lucaIDKeysBundleProvider }
        set { lucaIDKeysBundleProvider = newValue }
    }
    var lucaIDService: BaseInjectionProvider<LucaIDService> {
        get { lucaIDServiceProvider }
        set { lucaIDServiceProvider = newValue }
    }
    var attestationService: BaseInjectionProvider<LucaIDVerificationPreconditionsCheckService> {
        get { attestationServiceProvider }
        set { attestationServiceProvider = newValue }
    }
    var lucaIDProcessFetcherService: BaseInjectionProvider<LucaIdProcessFetcherService> {
        get { lucaIDProcessFetcherServiceProvider }
        set { lucaIDProcessFetcherServiceProvider = newValue }
    }
    var lucaIDMessageService: BaseInjectionProvider<LucaIDMessageService> {
        get { lucaIDMessageServiceProvider }
        set { lucaIDMessageServiceProvider = newValue }
    }
    var lucaIDTermsOfUse: BaseInjectionProvider<LucaIDTermsOfUseService> {
        get { lucaIDTermsOfUseProvider }
        set { lucaIDTermsOfUseProvider = newValue }
    }
}

class LucaIDInitialiser: BaseServiceInitialiser {

    override func initialise() throws {
        try register(LucaIDKeysBundle())
            .as(\.lucaIDKeysBundle) { SharedProvider(value: $0) }

        try register(LucaIDService())
            .as(\.lucaIDService) { SharedProvider(value: $0) }

        try register(LucaIDVerificationPreconditionsCheckService())
            .as(\.attestationService) { SharedProvider(value: $0) }

        try register(LucaIdProcessFetcherService())
            .as("LucaIDProcessFetcherServiceToggleable") { SharedProvider(value: $0 as Toggleable) }
            .as(\.lucaIDProcessFetcherService) { SharedProvider(value: $0) }

        try register(LucaIDMessageService())
            .as("LucaIDMessageServiceToggleable") { SharedProvider(value: $0 as Toggleable) }
            .as(\.lucaIDMessageService) { SharedProvider(value: $0) }

        try register(LucaIDTermsOfUseService())
            .as(\.lucaIDTermsOfUse) { SharedProvider(value: $0) }
    }

}
