import Foundation
import UIKit
import DependencyInjection
import RxSwift
import RxAppState

class EventHandlerTrigger: Toggleable {
    @InjectAllDynamic private var appStateHandlers: [AppStateHandler]
    @InjectAllDynamic private var memoryWarningHandlers: [MemoryWarningHandler]

    private var disposeBag: DisposeBag?
    var isEnabled: Bool {
        disposeBag != nil
    }

    func enable() {
        if isEnabled {
            return
        }
        let newDisposeBag = DisposeBag()

        UIApplication.shared.rx.appState
            .subscribe(on: MainScheduler.instance)
            .do(onNext: { state in
                for appStateHandler in self.appStateHandlers {
                    appStateHandler.onAppStateChange(state)
                }
            })
            .subscribe()
            .disposed(by: newDisposeBag)

        NotificationCenter.default.rx.notification(UIApplication.protectedDataWillBecomeUnavailableNotification, object: nil)
            .subscribe()
            .disposed(by: newDisposeBag)

        NotificationCenter.default.rx.notification(UIApplication.didReceiveMemoryWarningNotification, object: nil)
            .do(onNext: { _ in
                for handler in self.memoryWarningHandlers {
                    handler.onMemoryWarning()
                }
            })
            .subscribe()
            .disposed(by: newDisposeBag)
        disposeBag = newDisposeBag
    }

    func disable() {
        disposeBag = nil
    }
}
