import Foundation
import RxAppState

protocol AppStateHandler: AnyObject {
    func onAppStateChange(_ appState: AppState)
}

protocol MemoryWarningHandler: AnyObject {
    func onMemoryWarning()
}
