import Foundation
import DependencyInjection
import RxSwift

private var _accessedTraceIdRepo: BaseInjectionProvider<DataRepo<AccessedTraceId>> = EmptyProvider()
private var _documentKeyProviderProvider: BaseInjectionProvider<DocumentKeyProvider> = EmptyProvider()
private var _documentFactoryProvider: BaseInjectionProvider<DocumentFactory> = EmptyProvider()
private var _documentRepoServiceProvider: BaseInjectionProvider<DocumentRepoService> = EmptyProvider()
private var _documentProcessingServiceProvider: BaseInjectionProvider<DocumentProcessingService> = EmptyProvider()
private var _documentUniquenessCheckerProvider: BaseInjectionProvider<DocumentUniquenessChecker> = EmptyProvider()
private var _qrProcessingServiceProvider: BaseInjectionProvider<QRProcessingService> = EmptyProvider()
private var _documentPersonAssociationProvider: BaseInjectionProvider<DocumentPersonAssociationService> = EmptyProvider()
private var _documentsPreferencesProvider: BaseInjectionProvider<DocumentsPreferences<DocumentProperties>> = EmptyProvider()

extension DependencyContext {
    var documentKeyProvider: BaseInjectionProvider<DocumentKeyProvider> {
        get { _documentKeyProviderProvider }
        set { _documentKeyProviderProvider = newValue }
    }
    var documentFactory: BaseInjectionProvider<DocumentFactory> {
        get { _documentFactoryProvider }
        set { _documentFactoryProvider = newValue }
    }
    var documentRepoService: BaseInjectionProvider<DocumentRepoService> {
        get { _documentRepoServiceProvider }
        set { _documentRepoServiceProvider = newValue }
    }
    var documentProcessingService: BaseInjectionProvider<DocumentProcessingService> {
        get { _documentProcessingServiceProvider }
        set { _documentProcessingServiceProvider = newValue }
    }
    var documentUniquenessChecker: BaseInjectionProvider<DocumentUniquenessChecker> {
        get { _documentUniquenessCheckerProvider }
        set { _documentUniquenessCheckerProvider = newValue }
    }
    var qrProcessingService: BaseInjectionProvider<QRProcessingService> {
        get { _qrProcessingServiceProvider }
        set { _qrProcessingServiceProvider = newValue }
    }
    var documentPersonAssociationService: BaseInjectionProvider<DocumentPersonAssociationService> {
        get { _documentPersonAssociationProvider }
        set { _documentPersonAssociationProvider = newValue }
    }
    var documentsPreferences: BaseInjectionProvider<DocumentsPreferences<DocumentProperties>> {
        get { _documentsPreferencesProvider }
        set { _documentsPreferencesProvider = newValue }
    }
}

class DocumentsServicesInitialiser: BaseServiceInitialiser {

    override func initialise() throws {
        try register(DocumentKeyProvider())
            .as(\.documentKeyProvider) { SharedProvider(value: $0) }

        let documentFactory = DocumentFactory()

        documentFactory.register(parser: DGCParser())
        documentFactory.register(parser: AppointmentParser())
        documentFactory.register(parser: DefaultJWTParser())
        documentFactory.register(parser: ConnfairTicketParser())
        documentFactory.register(parser: JWTParserWithOptionalDoctor())
        documentFactory.register(parser: BaerCodeParser())
        try register(documentFactory)
            .as(\.documentFactory) { SharedProvider(value: $0) }

        let documentProcessingService = DocumentProcessingService()

        // Document validators
        documentProcessingService.register(validator: CoronaTestIsNotInFutureValidator())
        documentProcessingService.register(validator: DGCExpirationValidator())
        documentProcessingService.register(validator: CoronaTestIsNegativeValidator())
        documentProcessingService.register(validator: DGCIssuerValidator())
        documentProcessingService.register(validator: UserOrChildValidator(userIdentityValidator: UserOwnershipValidator()))
        documentProcessingService.register(validator: DocumentValidityValidator())

        #if PREPROD || PRODUCTION
        documentProcessingService.register(validator: DGCVerificationValidator())
        #endif
        try register(documentProcessingService)
            .as(\.documentProcessingService) { SharedProvider(value: $0) }

        try register(DocumentRepoService())
            .as(\.documentRepoService) { SharedProvider(value: $0) }

        try register(DocumentUniquenessChecker())
            .as(\.documentUniquenessChecker) { SharedProvider(value: $0) }

        try register(QRProcessingService())
            .as(\.qrProcessingService) { SharedProvider(value: $0) }

        try register(DocumentPersonAssociationService())
            .as(\.documentPersonAssociationService) { SharedProvider(value: $0) }

        try register(DocumentsPreferences(propertySource: DocumentProperties()))
            .as(\.documentsPreferences) { SharedProvider(value: $0) }
    }
}

extension DocumentsServicesInitialiser: UnsafeAddress, LogUtil {}
