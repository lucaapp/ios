import DependencyInjection

private var backendAddressV3Provider: BaseInjectionProvider<BackendAddressV3> = EmptyProvider()
private var backendAddressV4Provider: BaseInjectionProvider<BackendAddressV4> = EmptyProvider()
private var backendUserV3Provider: BaseInjectionProvider<BackendUserV3> = EmptyProvider()
private var backendTraceIdV3Provider: BaseInjectionProvider<BackendTraceIdV3> = EmptyProvider()
private var backendMiscV3Provider: BaseInjectionProvider<CommonBackendMisc> = EmptyProvider()
private var backendSMSV3Provider: BaseInjectionProvider<BaseBackendSMSVerification> = EmptyProvider()
private var backendLocationV3Provider: BaseInjectionProvider<BackendLocationV3> = EmptyProvider()
private var backendDailyKeyV3Provider: BaseInjectionProvider<DefaultBackendDailyKeyV3> = EmptyProvider()
private var backendDailyKeyV4Provider: BaseInjectionProvider<BackendDailyKeyV4> = EmptyProvider()
private var backendAccessDataV4Provider: BaseInjectionProvider<BackendAccessDataV4> = EmptyProvider()
private var backendPoWV4Provider: BaseInjectionProvider<BackendPoWV4> = EmptyProvider()
private var backendStagedRolloutV4Provider: BaseInjectionProvider<BackendStagedRolloutV4> = EmptyProvider()
private var backendTableInfoV4Provider: BaseInjectionProvider<BackendTableInfoV4> = EmptyProvider()
private var backendLucaIDProvider: BaseInjectionProvider<BackendLucaID> = EmptyProvider()
private var backendPaymentProvider: BaseInjectionProvider<BackendLucaPayment> = EmptyProvider()
private var notificationConfigCachedDataSourceProvider: BaseInjectionProvider<CachedDataSource<NotificationConfig>> = EmptyProvider()
private var accessedTracesDataChunkCachedDataSourceProvider: BaseInjectionProvider<CachedDataSource<AccessedTracesDataChunk>> = EmptyProvider()
private var backendVoluntaryReachabilityProvider: BaseInjectionProvider<BackendVoluntaryReachabilityV4> = EmptyProvider()

extension DependencyContext {

    var backendAddressV3: BaseInjectionProvider<BackendAddressV3> {
        get { backendAddressV3Provider }
        set { backendAddressV3Provider = newValue }
    }
    var backendAddressV4: BaseInjectionProvider<BackendAddressV4> {
        get { backendAddressV4Provider }
        set { backendAddressV4Provider = newValue }
    }
    var backendUserV3: BaseInjectionProvider<BackendUserV3> {
        get { backendUserV3Provider }
        set { backendUserV3Provider = newValue }
    }
    var backendTraceIdV3: BaseInjectionProvider<BackendTraceIdV3> {
        get { backendTraceIdV3Provider }
        set { backendTraceIdV3Provider = newValue }
    }
    var backendMiscV3: BaseInjectionProvider<CommonBackendMisc> {
        get { backendMiscV3Provider }
        set { backendMiscV3Provider = newValue }
    }
    var backendSMSV3: BaseInjectionProvider<BaseBackendSMSVerification> {
        get { backendSMSV3Provider }
        set { backendSMSV3Provider = newValue }
    }
    var backendLocationV3: BaseInjectionProvider<BackendLocationV3> {
        get { backendLocationV3Provider }
        set { backendLocationV3Provider = newValue }
    }
    var backendDailyKeyV3: BaseInjectionProvider<DefaultBackendDailyKeyV3> {
        get { backendDailyKeyV3Provider }
        set { backendDailyKeyV3Provider = newValue }
    }
    var backendDailyKeyV4: BaseInjectionProvider<BackendDailyKeyV4> {
        get { backendDailyKeyV4Provider }
        set { backendDailyKeyV4Provider = newValue }
    }
    var backendAccessDataV4: BaseInjectionProvider<BackendAccessDataV4> {
        get { backendAccessDataV4Provider }
        set { backendAccessDataV4Provider = newValue }
    }
    var backendPoWV4: BaseInjectionProvider<BackendPoWV4> {
        get { backendPoWV4Provider }
        set { backendPoWV4Provider = newValue }
    }
    var backendStagedRolloutV4: BaseInjectionProvider<BackendStagedRolloutV4> {
        get { backendStagedRolloutV4Provider }
        set { backendStagedRolloutV4Provider = newValue }
    }
    var backendTableInfoV4: BaseInjectionProvider<BackendTableInfoV4> {
        get { backendTableInfoV4Provider }
        set { backendTableInfoV4Provider = newValue }
    }
    var backendLucaID: BaseInjectionProvider<BackendLucaID> {
        get { backendLucaIDProvider }
        set { backendLucaIDProvider = newValue }
    }
    var notificationConfigCachedDataSource: BaseInjectionProvider<CachedDataSource<NotificationConfig>> {
        get { notificationConfigCachedDataSourceProvider }
        set { notificationConfigCachedDataSourceProvider = newValue }
    }
    var accessedTracesDataChunkCachedDataSource: BaseInjectionProvider<CachedDataSource<AccessedTracesDataChunk>> {
        get { accessedTracesDataChunkCachedDataSourceProvider }
        set { accessedTracesDataChunkCachedDataSourceProvider = newValue }
    }
    var backendVoluntaryReachability: BaseInjectionProvider<BackendVoluntaryReachabilityV4> {
        get { backendVoluntaryReachabilityProvider }
        set { backendVoluntaryReachabilityProvider = newValue }
    }
    var backendPayment: BaseInjectionProvider<BackendLucaPayment> {
        get { backendPaymentProvider }
        set { backendPaymentProvider = newValue }
    }
}

class BackendInitialiser: BaseServiceInitialiser {

    fileprivate func extractedBaseCachedDataSource(_ backendAccessDataV4: BackendAccessDataV4) throws {
#if PRODUCTION
        let notificationCacheValidity = CacheValidity.until(unit: .hour, count: 6)
#else
        let notificationCacheValidity = CacheValidity.onceInRuntime
#endif
        try register(BaseCachedDataSource(
            dataSource: backendAccessDataV4.fetchNotificationConfig(),
            cacheDataRepo: KeyValueRepoCacheWrapper(uniqueCacheKey: "NotificationCache"),
            cacheValidity: notificationCacheValidity,
            uniqueCacheIdentifier: "NotificationConfigCache"
        ))
            .as(\.notificationConfigCachedDataSource) { SharedProvider(value: $0) }
    }

    override func initialise() throws {
        try register(BackendAddressV3())
            .as(\.backendAddressV3) { SharedProvider(value: $0) }
        try register(BackendAddressV4())
            .as(\.backendAddressV4) { SharedProvider(value: $0) }

        let backendAccessDataV4 = BackendAccessDataV4()

        try extractedBaseCachedDataSource(backendAccessDataV4)

        try register(BaseCachedDataSource(
            dataSource: backendAccessDataV4.activeChunk(),
            cacheDataRepo: KeyValueRepoCacheWrapper(uniqueCacheKey: "ActiveChunk"),
            cacheValidity: .until(unit: cacheUnit, count: 30),
            uniqueCacheIdentifier: "ActiveChunkCache"
        ))
        .as(\.accessedTracesDataChunkCachedDataSource) { SharedProvider(value: $0) }

        try register(backendAccessDataV4)
            .as(\.backendAccessDataV4) { SharedProvider(value: $0) }

        try register(BaseBackendSMSVerification())
            .as(\.backendSMSV3) { SharedProvider(value: $0) }

        try register(BackendLocationV3())
            .as(\.backendLocationV3) { SharedProvider(value: $0) }

        try register(BackendUserV3())
            .as(\.backendUserV3) { SharedProvider(value: $0) }

        try register(BackendTraceIdV3())
            .as(\.backendTraceIdV3) { SharedProvider(value: $0) }

        try register(CommonBackendMisc())
            .as(\.backendMiscV3) { SharedProvider(value: $0) }

        try register(BackendPoWV4())
            .as(\.backendPoWV4) { SharedProvider(value: $0) }

        try register(BackendStagedRolloutV4())
            .as(\.backendStagedRolloutV4) { SharedProvider(value: $0) }

        try register(BackendTableInfoV4())
            .as(\.backendTableInfoV4) { SharedProvider(value: $0) }

        try register(BackendLucaID())
            .as(\.backendLucaID) { SharedProvider(value: $0) }

        try register(DefaultBackendDailyKeyV3())
            .as(\.backendDailyKeyV3) { SharedProvider(value: $0) }

        try register(BackendDailyKeyV4())
            .as(\.backendDailyKeyV4) { SharedProvider(value: $0) }

        try register(BackendVoluntaryReachabilityV4())
            .as(\.backendVoluntaryReachability) { SharedProvider(value: $0) }

        try register(BackendLucaPayment())
            .as(\.backendPayment) { SharedProvider(value: $0) }
    }

    private var cacheUnit: Calendar.Component {
        #if PRODUCTION
        return Calendar.Component.minute
        #else
        return Calendar.Component.second
        #endif
    }
}
