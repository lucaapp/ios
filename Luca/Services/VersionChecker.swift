import Foundation
import RxSwift
import DependencyInjection
import RxAppState

enum VersionCheckerError: BlockingError {
    case appOutdated
}

extension VersionCheckerError {
    var localizedTitle: String {
        L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        switch self {
        case .appOutdated: return L10n.IOSApp.VersionSupportChecker.failureMessage
        }
    }
}

class VersionChecker: Toggleable {
    @InjectStatic(\.backendMiscV3) private var backend
    @InjectStatic(\.reachabilityService) private var reachability

    private var disposeBag: DisposeBag?

    weak var errorPresenter: (UIViewController & HandlesLucaErrors)? {
        didSet {
            if isEnabled {
                disable()
                enable()
            }
        }
    }

    var isEnabled: Bool {
        disposeBag != nil
    }

    func enable() {
        if isEnabled {
            return
        }
        let newDisposeBag = DisposeBag()

        UIApplication.shared.rx
            .currentAndChangedAppState
            .subscribe(on: MainScheduler.asyncInstance)
            .filter { $0 == .active }
            .flatMapLatest { _ -> Completable in
                return self.reachability
                    .isReachableDriver
                    .asObservable()
                    .flatMap { [weak self] _ -> Completable in
                        return self?.checkVersion() ?? .empty()
                    }
                    .asCompletable()
            }
            .subscribe()
            .disposed(by: newDisposeBag)
        disposeBag = newDisposeBag
    }

    private func checkVersion() -> Completable {
        guard let presenter = self.errorPresenter else { return .empty() }
        return self.isVersionSupported()
            .do(onSuccess: { isSupported in
                if !isSupported {
                    throw VersionCheckerError.appOutdated
                }
            })
            .asCompletable()
            .catch({ error in
                guard let backendError = error as? BackendError<FetchSupportedVersionError> else { return .error(error) }
                if backendError.networkLayerError == .noInternet {
                    return .empty()
                }
                if backendError.networkLayerError == .upgradeRequired {
                    throw VersionCheckerError.appOutdated
                }
                return .error(error)
            })
            .handleErrors(presenter)
            .onErrorComplete()
    }

    private func isVersionSupported() -> Single<Bool> {
        backend.fetchSupportedVersions()
            .asSingle()
            .flatMap { supportedVersions in
                Single.from { Bundle.main.buildVersionNumber }
                    .asObservable()
                    .unwrapOptional()
                    .map { Int($0) }
                    .unwrapOptional()
                    .map { $0 >= supportedVersions.minimumVersion  }
                    .asSingle()
            }
    }

    func disable() {
        disposeBag = nil
    }
}
