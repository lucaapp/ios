import Foundation
import RxSwift
import UIKit
import DependencyInjection

/// Service for the checkout notification which triggers to remind you to checkout from the location.
class NotificationService: NSObject, Toggleable {
    @InjectStatic(\.lucaPreferences) private var lucaPreferences

    private static let checkoutNotification = "checkoutNotification"
    static let checkoutNotificationAction = "checkoutNotificationAction"
    private static let checkoutNotificationCategory = "checkoutNotificationCategory"
    private let notificationCenter = UNUserNotificationCenter.current()
    @InjectStatic(\.traceIdService) private var traceIdService: TraceIdService
    @InjectStatic(\.autoCheckoutService) private var autoCheckoutService: AutoCheckoutService
    @InjectStatic(\.lucaConnectContactDataService) private var lucaConnectContactDataService

    private var disposeBag: DisposeBag?
    var isEnabled: Bool { disposeBag != nil }

    override init() {
        super.init()
        registerActions()

        _ = removePendingNotificationsIfNotCheckedIn().subscribe()
        notificationCenter.delegate = self
    }

    /// Enables automatic check out and check in events listening
    func enable() {
        guard !isEnabled else {
            return
        }

        let newDisposeBag = DisposeBag()
        traceIdService.onCheckInRx()
            .flatMap {[weak self]  _ -> Single<Location> in
                guard let self = self else { return Single.error(TraceIdServiceError.notCheckedIn)}
                return self.traceIdService.fetchCurrentLocationInfo(checkLocalDBFirst: true)
            }
            .flatMap { [weak self] location -> Completable in
                guard let self = self else { return Completable.empty()}
                return self.addNotification(location: location)
            }
            .subscribe()
            .disposed(by: newDisposeBag)
        self.disposeBag = newDisposeBag
    }

    /// Disables automatic events listening
    func disable() {
        disposeBag = nil
    }

    func addNotification(location: Location) -> Completable {
        guard let averageCheckinTime = location.averageCheckinTime else { return Completable.empty() }

        return lucaPreferences
            .get(\.checkoutNotificationScheduled)
            .flatMapCompletable { checkoutNotificationScheduled in
                if checkoutNotificationScheduled { return Completable.empty() }
                return Completable.from {
                    let content = UNMutableNotificationContent()
                    content.title = L10n.IOSApp.Notification.Checkout.title
                    content.body = L10n.IOSApp.Notification.Checkout.description
                    content.sound = UNNotificationSound.default
                    content.categoryIdentifier = Self.checkoutNotificationCategory
                    let notificationInterval = Double(averageCheckinTime * 60)

                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: notificationInterval, repeats: false)

                    let notificationRequest = UNNotificationRequest(identifier: Self.checkoutNotification, content: content, trigger: trigger)

                    self.notificationCenter.add(notificationRequest)
                }
                .andThen(self.lucaPreferences.set(\.checkoutNotificationScheduled, value: true))
            }
    }

    func removePendingNotifications() -> Completable {
        Completable.from {
            self.notificationCenter.removePendingNotificationRequests(
                withIdentifiers: [Self.checkoutNotification]
            )
            self.notificationCenter.removeDeliveredNotifications(
                withIdentifiers: [Self.checkoutNotification ]
            )
        }
        .andThen(lucaPreferences.set(\.checkoutNotificationScheduled, value: false))
    }

    func removePendingNotificationsIfNotCheckedIn() -> Completable {
        traceIdService.isCurrentlyCheckedIn
            .observe(on: MainScheduler.instance)
            .flatMapCompletable { checkedIn in
                if !checkedIn { return self.removePendingNotifications() }
                return Completable.empty()
            }
    }

    private func registerActions() {
        let checkoutAction = UNNotificationAction(identifier: Self.checkoutNotificationAction,
                                                  title: L10n.IOSApp.Checkout.now)

        let checkoutCategory = UNNotificationCategory(identifier: Self.checkoutNotificationCategory,
                                                      actions: [checkoutAction],
                                                      intentIdentifiers: [],
                                                      hiddenPreviewsBodyPlaceholder: "",
                                                      options: .customDismissAction)

        notificationCenter.setNotificationCategories([checkoutCategory])
    }
}

// import UserNotifications
extension NotificationService: UNUserNotificationCenterDelegate {
    public func userNotificationCenter(_ center: UNUserNotificationCenter,
                                       didReceive response: UNNotificationResponse,
                                       withCompletionHandler completionHandler: @escaping () -> Void) {

        if response.notification.request.identifier == LucaConnectContactDataService.lucaConnectActivationNotificationKey {
            lucaConnectContactDataService.activationNotificationPublisher.onNext(true)
            return
        }

        switch response.actionIdentifier {
        case NotificationService.checkoutNotificationAction:
            _ = traceIdService.checkOut()
                .andThen(Completable.from { completionHandler() }
                            .subscribe(on: MainScheduler.instance))
                .subscribe()
        default:
            break
        }

    }

    public func userNotificationCenter(_ center: UNUserNotificationCenter,
                                       willPresent notification: UNNotification,
                                       withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
}
