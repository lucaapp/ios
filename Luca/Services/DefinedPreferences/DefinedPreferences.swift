import Foundation
import RxSwift

class DefinedPreferences<PropertySource> {
    typealias PropertySource = PropertySource

    func set<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>, value: T) -> Completable where T: Codable {
        fatalError("Not implemented")
    }

    func get<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>) -> Single<T> where T: Codable {
        fatalError("Not implemented")
    }

    func remove<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>) -> Completable where T: Codable {
        fatalError("Not implemented")
    }

    func set<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>, value: T) -> Completable where T: Codable {
        fatalError("Not implemented")
    }

    func get<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>) -> Single<T?> where T: Codable {
        fatalError("Not implemented")
    }

    func remove<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>) -> Completable where T: Codable {
        fatalError("Not implemented")
    }
}
