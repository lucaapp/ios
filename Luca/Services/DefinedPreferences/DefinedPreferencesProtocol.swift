import Foundation
import RxSwift

protocol DefinedPreferencesProtocol {
    associatedtype PropertySource

    func set<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>, value: T) -> Completable where T: Codable
    func get<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>) -> Single<T> where T: Codable
    func remove<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>) -> Completable where T: Codable

    func set<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>, value: T) -> Completable where T: Codable
    func get<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>) -> Single<T?> where T: Codable
    func remove<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>) -> Completable where T: Codable
}
