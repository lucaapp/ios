import Foundation
import RxSwift
import DependencyInjection

class ParametrisedCachedDataSource<T, ParamType: Codable> {

    /// Emits true if the cache is valid for given parameter
    func isCacheValid(parameter: ParamType) -> Single<Bool> {
        fatalError("Not implemented")
    }

    /// Retrieves data for given parameter either from source or from the cache if still valid.
    func retrieve(parameter: ParamType, loadOnlyFromCache: Bool = false) -> Single<[T]> {
        fatalError("Not implemented")
    }

    /// Invalidates the cache for given parameter. Doesn't touch other parameters
    func invalidateCache(parameter: ParamType) -> Completable {
        fatalError("Not implemented")
    }

    /// Invalidates all the cache for all parameters that have been used so far.
    func invalidateCache() -> Completable {
        fatalError("Not implemented")
    }
}

private struct ParamWrapper<T: Codable>: Codable {
    var __internalValue: T
}

class BaseParametrisedCachedDataSource<T, ParamType, DataSourceType, CacheDataRepoType>: ParametrisedCachedDataSource<T, ParamType>
            where DataSourceType: DataSource, DataSourceType.T == T,
                  CacheDataRepoType: CacheDataRepo, CacheDataRepoType.T == T,
                  ParamType: Codable {

    @InjectStatic(\.keyValueRepo) private var keyValueRepo

    private let dispatchQueue: SchedulerType
    private let dataSourceFactory: (ParamType) -> DataSourceType?
    private let cacheDataRepoFactory: (ParamType) -> CacheDataRepoType?
    private let cacheValidity: CacheValidity
    private let uniqueCacheIdentifier: String
    private var allUsedParamsKey: String {
        uniqueCacheIdentifier + ".allParams"
    }

    // This object is synchronised with `dispatchQueue`. It's a serial task scheduler. It means there can be only one task at a time using this object.
    private var caches: [String: CachedDataSource<T>] = [:]

    init(
        dataSourceFactory: @escaping (ParamType) -> DataSourceType?,
        cacheDataRepoFactory: @escaping (ParamType) -> CacheDataRepoType?,
        cacheValidity: CacheValidity,
        uniqueCacheIdentifier: String
    ) {
        self.dispatchQueue = SerialDispatchQueueScheduler(qos: .default, internalSerialQueueName: "\(String(describing: Self.self))")
        self.dataSourceFactory = dataSourceFactory
        self.cacheDataRepoFactory = cacheDataRepoFactory
        self.cacheValidity = cacheValidity
        self.uniqueCacheIdentifier = uniqueCacheIdentifier
    }
    override func isCacheValid(parameter: ParamType) -> Single<Bool> {
        getCachedDataSource(for: parameter).flatMap { $0.isCacheValid }
    }
    override func retrieve(parameter: ParamType, loadOnlyFromCache: Bool = false) -> Single<[T]> {
        registerUsage(of: parameter)
            .andThen(getCachedDataSource(for: parameter))
            .flatMap { $0.retrieve(loadOnlyFromCache: loadOnlyFromCache) }
    }
    override func invalidateCache(parameter: ParamType) -> Completable {
        getCachedDataSource(for: parameter).flatMapCompletable { $0.invalidateCache() }
    }
    override func invalidateCache() -> Completable {
        keyValueRepo
            .loadOptional(allUsedParamsKey)
            .map { $0 ?? [String]() }
            .asObservable()
            .flatMap { Observable.from($0) }
            .map { try self.param(from: $0) }
            .flatMap { self.invalidateCache(parameter: $0) }
            .ignoreElementsAsCompletable()
    }

    /// Saves this parameter to the array to be able to invalidate all caches.
    private func registerUsage(of param: ParamType) -> Completable {
        Single.from { try self.key(for: param) }
        .flatMapCompletable { key in
            self.keyValueRepo
                .loadOptional(self.allUsedParamsKey)
                .map { $0 ?? [String]() }
                .flatMapCompletable { array in
                    if !array.contains(key) {
                        return self.keyValueRepo.store(self.allUsedParamsKey, value: array + [key])
                    }
                    return .empty()
                }
        }
    }

    /// Emits single CachedDataSource for given parameter.
    ///
    /// If there is already an instance for this parameter, it will be reused, if not, it will create one and save to the in-memory cache
    private func getCachedDataSource(for param: ParamType) -> Single<CachedDataSource<T>> {
        Single.from {
            let key = try self.key(for: param)
            if let found = self.caches[key] {
                return found
            }
            guard let dataSource = self.dataSourceFactory(param),
                  let cacheDataRepo = self.cacheDataRepoFactory(param) else {
                throw NSError(domain: "No data source or no cache repo", code: 0, userInfo: nil)
            }
            let cachedDataSource = BaseCachedDataSource(
                dataSource: dataSource,
                cacheDataRepo: cacheDataRepo,
                cacheValidity: self.cacheValidity,
                uniqueCacheIdentifier: self.uniqueCacheIdentifier + "." + key
            )
            self.caches[key] = cachedDataSource
            return cachedDataSource
        }.subscribe(on: self.dispatchQueue)
    }

    /// Returns key for given parameter
    private func key(for param: ParamType) throws -> String {
        (try JSONEncoder().encode(ParamWrapper(__internalValue: param))).base64EncodedString()
    }

    /// Converts the key generated by the `key(for:)` method back to `ParamType`
    private func param(from key: String) throws -> ParamType {
        guard let data = Data(base64Encoded: key) else {
            throw NSError(domain: "Malformed key \(key)", code: 0, userInfo: nil)
        }
        return (try JSONDecoder().decode(ParamWrapper<ParamType>.self, from: data)).__internalValue
    }
}
