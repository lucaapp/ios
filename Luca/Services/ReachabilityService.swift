import Foundation
import RxSwift
import RxReachability
import RxCocoa
import Reachability

protocol ReachabilityServiceKind {
    var isReachableDriver: Driver<Bool> {get}
    func enable()
    func disable()
}

class ReachabilityService: NSObject, ReachabilityServiceKind, Toggleable {
    private var disposeBag: DisposeBag?
    private var reachability: Reachability?
    var isEnabled: Bool { disposeBag != nil }

    private var isReachableRelay = BehaviorRelay<Bool>(value: false)
    var isReachableDriver: Driver<Bool> {
        return isReachableRelay.distinctUntilChanged().asDriver(onErrorJustReturn: false)
    }

    /// It enables only the listeners.
    func enable() {
        if isEnabled {
            return
        }
        let newDisposeBag = DisposeBag()
        reachability = try? Reachability()
        try? reachability?.startNotifier()
        Reachability.rx.isReachable
           .subscribe(onNext: { [weak self] (isReachable: Bool) in
               print("Is reachable: \(isReachable)")
               self?.isReachableRelay.accept(isReachable)
           })
           .disposed(by: newDisposeBag)
        disposeBag = newDisposeBag

    }

    /// Disables the listeners; it doesn't turn off the feature though
    func disable() {
        disposeBag = nil
    }

}
