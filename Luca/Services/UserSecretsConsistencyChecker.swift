import Foundation
import RxSwift
import DependencyInjection

class UserSecretsConsistencyChecker: Toggleable {

    @InjectStatic(\.userKeysBundle) private var userKeysBundle: UserKeysBundle
    @InjectStatic(\.traceIdService) private var traceIdService: TraceIdService
    @InjectStatic(\.userService) private var userService: UserService

    private var disposeBag: DisposeBag!

    var isEnabled: Bool { disposeBag != nil }

    func enable() {
        if isEnabled {
            return
        }
        disposeBag = DisposeBag()
        // If some changes have beed registered
        // 1. Checkout current stay
        // 2. Dispose user ID
        // 3. Register user with same data
        // 4. Dispose all traces

        // 1.
        let checkOutIfNeeded = traceIdService
            .isCurrentlyCheckedIn
            .flatMapCompletable { isCheckedIn in
                if isCheckedIn {
                    return self.traceIdService.checkOut()
                }
                return Completable.empty()
            }
            .debug(logUtil: self, "Check secrets 0")
            .logError(self, "Checkout")
            .retry(delay: .milliseconds(500), scheduler: LucaScheduling.backgroundScheduler)

        // 3.
        _ = userService
            .registerIfNeeded()
            .debug(logUtil: self, "Check secrets 1")
            .logError(self, "Register")
            .retry(delay: .milliseconds(500), scheduler: LucaScheduling.backgroundScheduler)
            .asObservable()
            .ignoreElementsAsCompletable()

        // 4.
        let disposeTraceData = traceIdService.disposeData(clearTraceHistory: true)

        userKeysBundle.onDataPopulationRx
            .debug(logUtil: self, "Check secrets w0")
            .deferredFilter { _ in self.userService.isRequiredDataComplete }
            .flatMap { _ in
                checkOutIfNeeded
                    .andThen(disposeTraceData.debug(logUtil: self, "trace data disposal"))
            }
            .logError(self, "Check secrets consistency")
            .retry(delay: .milliseconds(500), scheduler: LucaScheduling.backgroundScheduler) // Just in case, this stream has to run continuously
            .debug(logUtil: self, "Check secrets whole")
            .subscribe()
            .disposed(by: disposeBag)
    }

    func disable() {
        disposeBag = nil
    }
}

extension UserSecretsConsistencyChecker: LogUtil, UnsafeAddress {}
