import SwiftJWT
import RxSwift

class LucaIdJWTVerifier: CertificateVerifier {
    var rootCertificateSource: Single<String>
    var intermediateCertificatesSource: Single<[String]>

    init(rootCertificateSource: Single<String>, intermediateCertificatesSource: Single<[String]>) {
        self.rootCertificateSource = rootCertificateSource
        self.intermediateCertificatesSource = intermediateCertificatesSource
    }

    /// Checks JWT signature against leaf cert in the x5c chain
    func verifyJWTSignature(for jws: String, withCertificate certificate: String) -> Completable {
        do {
            guard let encodedCertData = Data(base64Encoded: certificate),
                  let certificate = SecCertificateCreateWithData(nil, encodedCertData as CFData),
                  let publicKey = SecCertificateCopyKey(certificate) else {
                throw NSError(domain: "Could not create key from JWS certificate", code: 0, userInfo: nil)
            }

            let keyData = try publicKey.toData()
            let key = ecPublicKeyToPEM(publicKey: keyData)
            let verifier = JWTVerifier.es256(publicKey: key.data(using: .utf8) ?? Data())

            guard JWT<LucaIDReceiptClaims>.verify(jws, using: verifier) else {
                throw NSError(domain: "JWS verification failed", code: 0, userInfo: nil)
            }
        } catch {
            return .error(error)
        }

        return .empty()
    }
}
