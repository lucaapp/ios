import Foundation
import RxSwift
import DependencyInjection

struct ReachabilityAuthKeyPairIndex: Codable, Hashable {
    var timestamp: Date
    var contactId: String
}

class UserKeysBundle {

    @InjectStatic(\.timeProvider) private var timeProvider: TimeProvider

    let onDataPurge = "UserKeysBundle.onDataPurge"
    let onDataPopulation = "UserKeysBundle.onDataPopulation"

    private let publicKeyTag = "userKeysBundle.public"
    private let privateKeyTag = "userKeysBundle.private"
    private let dataSecretTag = "userKeysBundle.data"
    private let oldTraceSecretTag = "userKeysBundle.traceSecret"
    private let traceSecretTag = "userKeysBundle.traceSecrets"
    private let reachabilityMessageEncryptionKeyPairTag = "userKeysBundle.reachabilityMessageEncryption"
    private let reachabilityMessageSigningKeyPairTag = "userKeysBundle.reachabilityMessageSigning"
    private let reachabilityDailyReferenceKeyPairTag = "userKeysBundle.reachabilityDailyReference"
    private let reachabilityNotificationSeedTag = "userKeysBundle.reachabilityNotificationSeed"

    private let _publicKey: KeyRepository<SecKey>
    private let _privateKey: KeyRepository<SecKey>
    private let _dataSecret: KeyRepository<Data>
    private let _oldTraceSecret: KeyRepository<Data>
    private let _traceSecrets: DailyDataKeyRepository

    private let _reachabilityMessageEncryptionKeyPair: SecKeyPairRepository
    private let _reachabilityMessageSigningKeyPair: SecKeyPairRepository
    private let _reachabilityDailyAuthKeyPair: SecKeyPairHistoryRepository<ReachabilityAuthKeyPairIndex>
    private let _reachabilityNotificationSeed: KeyRepository<Data>

    var publicKey: KeySource { _publicKey }
    var privateKey: KeySource { _privateKey }
    var dataSecret: RawKeySource { _dataSecret }
    var traceSecrets: DailyDataKeyRepository { _traceSecrets }

    var reachabilityMessageEncryptionKeyPair: SecKeyPairRepository { _reachabilityMessageEncryptionKeyPair }
    var reachabilityMessageSigningKeyPair: SecKeyPairRepository { _reachabilityMessageSigningKeyPair }
    var reachabilityDailyAuthKeyPair: SecKeyPairHistoryRepository<ReachabilityAuthKeyPairIndex> { _reachabilityDailyAuthKeyPair }
    var reachabilityNotificationSeed: RawKeySource { _reachabilityNotificationSeed }

    init() {
        _publicKey = SecKeyRepository(tag: publicKeyTag)
        _privateKey = SecKeyRepository(tag: privateKeyTag)
        _dataSecret = DataKeyRepository(tag: dataSecretTag)
        _oldTraceSecret = DataKeyRepository(tag: oldTraceSecretTag)
        _traceSecrets = DailyDataKeyRepository(header: traceSecretTag)

        _reachabilityMessageEncryptionKeyPair = SecKeyPairRepository(tag: reachabilityMessageEncryptionKeyPairTag)
        _reachabilityMessageSigningKeyPair = SecKeyPairRepository(tag: reachabilityMessageSigningKeyPairTag)
        _reachabilityDailyAuthKeyPair = SecKeyPairHistoryRepository<ReachabilityAuthKeyPairIndex>(tag: reachabilityDailyReferenceKeyPairTag)
        _reachabilityNotificationSeed = DataKeyRepository(tag: reachabilityNotificationSeedTag)

        _traceSecrets.factory = { [weak self] date in
            if let data = try self?.createUserTraceSecret(for: date) {
                return data
            }
            return Data()
        }
    }

    /// Removes all keys. Use with care as all data, that has been encrypted with those keys, won't be decryptable anymore
    ///
    /// - parameter onlyUserKeyPair:if set to true, only users public and private keys will be removed and no notification will be sent. If set to false, all keys will be purged.
    func removeKeys(onlyUserKeyPair: Bool = false) {
        _publicKey.purge()
        _privateKey.purge()
        if onlyUserKeyPair {
            return
        }
        _dataSecret.purge()
        _traceSecrets.removeAll()
        removeReachabilityKeys()
        NotificationCenter.default.post(Notification(name: Notification.Name(onDataPurge), object: self, userInfo: nil))
    }

    /// Removes all keys related to voluntary reachability feature.
    func removeReachabilityKeys() {
        _reachabilityMessageEncryptionKeyPair.purge()
        _reachabilityMessageSigningKeyPair.purge()
        _reachabilityNotificationSeed.purge()
        _reachabilityDailyAuthKeyPair.removeAll()
    }

    /// It will generate missing keys. It won't remove already filled keys.
    /// Generates new trace secret for current day if there is no key
    func generateKeys(forceRefresh: Bool = false) throws {
        if forceRefresh {
            removeKeys()
        }
        var dataHasBeenCreated = false
        try generatePrivateKey(&dataHasBeenCreated)
        try generateReachabilityMessageSigningKeyPair(&dataHasBeenCreated)
        try generateReachabilityMessageEncryptionKeyPair(&dataHasBeenCreated)
        try generateReachabilityNotificationSeed(&dataHasBeenCreated)
        try generateDataSecret(&dataHasBeenCreated)
        try generateTraceSecret()
        if dataHasBeenCreated {
            NotificationCenter.default.post(Notification(name: Notification.Name(onDataPopulation), object: self, userInfo: nil))
        }
    }

    private func generatePrivateKey(_ dataHasBeenCreated: inout Bool) throws {
        if (try? _privateKey.restore()) == nil || (try? _publicKey.restore()) == nil {

            // Invalidate public key
            _publicKey.purge()
            _privateKey.purge()

            let privateKey = try KeyFactory.createPrivate(tag: privateKeyTag, type: .ecsecPrimeRandom, sizeInBits: 256)

            guard _privateKey.store(key: privateKey) else {
                throw NSError(domain: "Private key couldn't be stored!", code: 0, userInfo: nil)
            }

            guard let publicKey = KeyFactory.derivePublic(from: privateKey) else {
                throw NSError(domain: "Public key couldn't be created!", code: 0, userInfo: nil)
            }

            guard _publicKey.store(key: publicKey) else {
                throw NSError(domain: "Public key couldn't be stored!", code: 0, userInfo: nil)
            }
            dataHasBeenCreated = true
        }
    }

    private func generateReachabilityMessageSigningKeyPair(_ dataHasBeenCreated: inout Bool) throws {
        if (try? _reachabilityMessageSigningKeyPair.restore()) == nil {
            let privateKey = try KeyFactory.createPrivate(tag: privateKeyTag, type: .ecsecPrimeRandom, sizeInBits: 256)

            guard let publicKey = KeyFactory.derivePublic(from: privateKey) else {
                throw NSError(domain: "Public key couldn't be created!", code: 0, userInfo: nil)
            }

            guard _reachabilityMessageSigningKeyPair.store(key: KeyPair(public: publicKey, private: privateKey), removeIfExists: true) else {
                throw NSError(domain: "Message signing keys couldn't be stored!", code: 0, userInfo: nil)
            }
            dataHasBeenCreated = true
        }
    }

    private func generateReachabilityMessageEncryptionKeyPair(_ dataHasBeenCreated: inout Bool) throws {
        if (try? _reachabilityMessageEncryptionKeyPair.restore()) == nil {
            let privateKey = try KeyFactory.createPrivate(tag: privateKeyTag, type: .ecsecPrimeRandom, sizeInBits: 256)

            guard let publicKey = KeyFactory.derivePublic(from: privateKey) else {
                throw NSError(domain: "Public key couldn't be created!", code: 0, userInfo: nil)
            }

            guard _reachabilityMessageEncryptionKeyPair.store(key: KeyPair(public: publicKey, private: privateKey), removeIfExists: true) else {
                throw NSError(domain: "Message encryption keys couldn't be stored!", code: 0, userInfo: nil)
            }
            dataHasBeenCreated = true
        }
    }

    private func generateReachabilityNotificationSeed(_ dataHasBeenCreated: inout Bool) throws {
        let notificationSeed = try? _reachabilityNotificationSeed.restore()
        if notificationSeed == nil || notificationSeed?.bytes.count != 16 {
            guard let randomData = KeyFactory.randomBytes(size: 16) else {
                throw NSError(domain: "Couldn't create random data!", code: 0, userInfo: nil)
            }
            _reachabilityNotificationSeed.purge()
            guard _reachabilityNotificationSeed.store(key: randomData) else {
                throw NSError(domain: "notificationSeed couldn't be stored!", code: 0, userInfo: nil)
            }
            dataHasBeenCreated = true
        }
    }

    private func generateDataSecret(_ dataHasBeenCreated: inout Bool) throws {
        let dataSecret = try? _dataSecret.restore()
        if dataSecret == nil || dataSecret?.bytes.count != 16 {
            guard let randomData = KeyFactory.randomBytes(size: 16) else {
                throw NSError(domain: "Couldn't create random data!", code: 0, userInfo: nil)
            }
            _dataSecret.purge()
            guard _dataSecret.store(key: randomData) else {
                throw NSError(domain: "Data key couldn't be stored!", code: 0, userInfo: nil)
            }
            dataHasBeenCreated = true
        }
    }

    private func generateTraceSecret() throws {
        let traceSecret = try? _traceSecrets.restore(index: timeProvider.now, enableFactoryIfAvailable: false)
        if let oldTraceSecret = try? _oldTraceSecret.restore(),
           (traceSecret == nil || traceSecret?.bytes.count != 16) {

            // Migrate old traceSecret and assign as todays secret
            log("Migrating the old trace secret.")
            // Save the keys for the last two weeks to be compatible with all possible trace IDs saved in the app.
            // Those keys will be removed in the sanity check
            let now = timeProvider.now
            for i in 0...14 {
                if let date = Calendar.current.date(byAdding: .day, value: -i, to: now) {
                    try _traceSecrets.store(key: oldTraceSecret, index: date)
                }
            }
            _oldTraceSecret.purge()
        }
    }

    private func createUserTraceSecret(for date: Date) throws -> Data {
        log("Generating new trace secret for \(date)")
        guard let randomData = KeyFactory.randomBytes(size: 16) else {
            throw NSError(domain: "Couldn't create user trace secret", code: 0, userInfo: nil)
        }
        return randomData
    }

    /// Removes keys that are out of date
    func removeUnusedKeys() {
        if let thresholdDate = Calendar.current.date(byAdding: .day, value: -15, to: timeProvider.now) {
            let tooOldDates = _traceSecrets.indices.filter { $0 < thresholdDate }
            for tooOldDate in tooOldDates {
                _traceSecrets.remove(index: tooOldDate)
            }
        }
    }
}

extension UserKeysBundle {
    var isComplete: Bool {

        (try? publicKey.retrieveKey()) != nil &&
        (try? privateKey.retrieveKey()) != nil &&
        (try? dataSecret.retrieveKey()) != nil
    }
}

extension UserKeysBundle {
    var onDataPurgeRx: Observable<Void> {
        NotificationCenter.default.rx.notification(NSNotification.Name(self.onDataPurge), object: self).map { _ in Void() }
    }
    var onDataPopulationRx: Observable<Void> {
        NotificationCenter.default.rx.notification(NSNotification.Name(self.onDataPopulation), object: self).map { _ in Void() }
    }
}

extension UserKeysBundle: LogUtil, UnsafeAddress {}
