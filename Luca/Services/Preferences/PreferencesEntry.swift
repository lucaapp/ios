import Foundation
import RxSwift

// MARK: - Key path entries
class PreferencesEntry<T> where T: Codable {
    var key: String
    var type: T.Type
    init(key: String, type: T.Type) {
        self.key = key
        self.type = type
    }

    func get(source: KeyValueRepoProtocol, keyPrefix: String) -> Single<T?> {
        source.loadOptional(keyPrefix + key)
    }

    func set(source: KeyValueRepoProtocol, keyPrefix: String, value: T) -> Completable {
        source.store(keyPrefix + key, value: value)
    }

    func remove(source: KeyValueRepoProtocol, keyPrefix: String) -> Completable {
        source.remove(keyPrefix + key)
    }
}

class PreferencesEntryWithDefaultValue<T>: PreferencesEntry<T> where T: Codable {
    var defaultValue: T
    init(key: String, type: T.Type, defaultValue: T) {
        self.defaultValue = defaultValue
        super.init(key: key, type: type)
    }

    func get(source: KeyValueRepoProtocol, keyPrefix: String) -> Single<T> {
        super.get(source: source, keyPrefix: keyPrefix).map { $0 ?? self.defaultValue }
    }
}
