import Foundation
import RxSwift
import DependencyInjection

protocol PreferencesTemplateProtocol {
    associatedtype EntriesSource

    // swiftlint:disable:next line_length
    func set<PreferenceEntryType, ValueType>(_ key: KeyPath<EntriesSource, PreferenceEntryType>, value: ValueType) -> Completable where PreferenceEntryType: PreferencesEntry<ValueType>, ValueType: Codable

    func get<PreferenceEntryType, ValueType>(_ key: KeyPath<EntriesSource, PreferenceEntryType>) -> Single<ValueType?> where PreferenceEntryType: PreferencesEntry<ValueType>, ValueType: Codable

    // swiftlint:disable:next line_length
    func get<PreferenceEntryType, ValueType>(_ key: KeyPath<EntriesSource, PreferenceEntryType>) -> Single<ValueType> where PreferenceEntryType: PreferencesEntryWithDefaultValue<ValueType>, ValueType: Codable

    func remove<PreferenceEntryType, ValueType>(_ key: KeyPath<EntriesSource, PreferenceEntryType>) -> Completable where PreferenceEntryType: PreferencesEntry<ValueType>, ValueType: Codable

    /// Emits next value on every update
    func changes<T>(_ key: KeyPath<EntriesSource, PreferencesEntryWithDefaultValue<T>>) -> Observable<T>

    /// Emits next value on nevery update and the current one on subscription
    func currentAndChanges<T>(_ key: KeyPath<EntriesSource, PreferencesEntryWithDefaultValue<T>>) -> Observable<T> where T: Codable

    /// Emits next value on every update
    func changes<T>(_ key: KeyPath<EntriesSource, PreferencesEntry<T>>) -> Observable<T?> where T: Codable

    /// Emits next value on nevery update and the current one on subscription
    func currentAndChanges<T>(_ key: KeyPath<EntriesSource, PreferencesEntry<T>>) -> Observable<T?> where T: Codable

    /// Changes an array within a single operation to avoid data racing between reading and updating an array.
    func changeArray<PreferencesEntryType, T>(_ key: KeyPath<EntriesSource, PreferencesEntryType>, transaction: @escaping ([T]) -> [T]) -> Single<[T]>
        where T: Codable, PreferencesEntryType: PreferencesEntry<[T]>
}
