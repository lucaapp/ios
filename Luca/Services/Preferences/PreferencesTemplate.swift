import Foundation
import RxSwift
import DependencyInjection

class PreferencesTemplate<EntriesSource>: PreferencesTemplateProtocol {
    typealias EntriesSource = EntriesSource
    @InjectStatic(\.keyValueRepo) private var defaultKeyValueRepo
    private let overridenKeyValueRepo: KeyValueRepoProtocol?

    private var keyValueRepo: KeyValueRepoProtocol {
        overridenKeyValueRepo ?? defaultKeyValueRepo
    }

    private let mainKey: String
    private let publisher = PublishSubject<(keyPathHash: Int, newValue: Any?)>()
    private let template: EntriesSource

    private let operationQueues: DispatchQueueSynced<[Int: OperationQueue]> = DispatchQueueSynced([:])

    init(template: EntriesSource, settingsPrefixKey: String, keyValueRepoTarget: KeyValueRepoProtocol? = nil) {
        self.mainKey = settingsPrefixKey
        self.overridenKeyValueRepo = keyValueRepoTarget
        self.template = template
    }

    // swiftlint:disable:next line_length
    func set<PreferenceEntryType, ValueType>(_ key: KeyPath<EntriesSource, PreferenceEntryType>, value: ValueType) -> Completable where PreferenceEntryType: PreferencesEntry<ValueType>, ValueType: Codable {
        template[keyPath: key].set(source: self.keyValueRepo, keyPrefix: mainKey, value: value)
            .andThen(Completable.from {
                self.publisher.onNext((keyPathHash: key.hashValue, newValue: value))
            })
    }

    func get<PreferenceEntryType, ValueType>(_ key: KeyPath<EntriesSource, PreferenceEntryType>) -> Single<ValueType?> where PreferenceEntryType: PreferencesEntry<ValueType>, ValueType: Codable {
        template[keyPath: key].get(source: self.keyValueRepo, keyPrefix: mainKey)
    }

    // swiftlint:disable:next line_length
    func get<PreferenceEntryType, ValueType>(_ key: KeyPath<EntriesSource, PreferenceEntryType>) -> Single<ValueType> where PreferenceEntryType: PreferencesEntryWithDefaultValue<ValueType>, ValueType: Codable {
        template[keyPath: key].get(source: self.keyValueRepo, keyPrefix: mainKey)
    }

    func remove<PreferenceEntryType, ValueType>(_ key: KeyPath<EntriesSource, PreferenceEntryType>) -> Completable where PreferenceEntryType: PreferencesEntry<ValueType>, ValueType: Codable {
        template[keyPath: key].remove(source: self.keyValueRepo, keyPrefix: mainKey)
            .andThen(Completable.from {
                var defaultValue: ValueType?
                if let key = key as? KeyPath<EntriesSource, PreferencesEntryWithDefaultValue<ValueType>> {
                    defaultValue = self.template[keyPath: key].defaultValue
                }
                self.publisher.onNext((keyPathHash: key.hashValue, newValue: defaultValue))
            })
    }

    /// Emits next value on every update
    func changes<T>(_ key: KeyPath<EntriesSource, PreferencesEntryWithDefaultValue<T>>) -> Observable<T> {
        let defaultValue = template[keyPath: key].defaultValue
        return publisher
            .filter { $0.keyPathHash == key.hashValue }
            .map { $0.newValue as? T }
            .map { $0 ?? defaultValue }
    }

    /// Emits next value on nevery update and the current one on subscription
    func currentAndChanges<T>(_ key: KeyPath<EntriesSource, PreferencesEntryWithDefaultValue<T>>) -> Observable<T> where T: Codable {
        Observable.concat(get(key).asObservable(), changes(key))
    }

    /// Emits next value on every update
    func changes<T>(_ key: KeyPath<EntriesSource, PreferencesEntry<T>>) -> Observable<T?> where T: Codable {
        publisher.filter { $0.keyPathHash == key.hashValue }.map { $0.newValue as? T }
    }

    /// Emits next value on nevery update and the current one on subscription
    func currentAndChanges<T>(_ key: KeyPath<EntriesSource, PreferencesEntry<T>>) -> Observable<T?> where T: Codable {
        Observable.concat(get(key).asObservable(), changes(key))
    }

    // swiftlint:disable:next line_length
    private func getArray<PreferencesEntryType, ValueType>(_ key: KeyPath<EntriesSource, PreferencesEntryType>) -> Single<[ValueType]> where ValueType: Codable, PreferencesEntryType: PreferencesEntry<[ValueType]> {
        if let keyPath = key as? KeyPath<EntriesSource, PreferencesEntryWithDefaultValue<[ValueType]>> {
            return self.get(keyPath)
        } else {
            return self.get(key).map { $0 ?? [] }
        }
    }

    // swiftlint:disable:next line_length
    func changeArray<PreferencesEntryType, T>(_ key: KeyPath<EntriesSource, PreferencesEntryType>, transaction: @escaping ([T]) -> [T]) -> Single<[T]> where T: Codable, PreferencesEntryType: PreferencesEntry<[T]> {
        operationQueue(for: key.hashValue)
            .flatMap { operationQueue in
                Single.create { observer in

                    operationQueue.addOperation(TransactionOperation { completionHandler in
                        _ = self.getArray(key)
                            .map { transaction($0) }
                            .flatMap { self.set(key, value: $0).andThen(.just($0)) }
                            .do(onSuccess: {
                                observer(.success($0))
                                completionHandler()
                            })
                            .do(onError: {
                                observer(.failure($0))
                                completionHandler()
                            })
                            .subscribe()
                    })
                    return Disposables.create()
                }
            }
    }

    private func operationQueue(for keyPathHash: Int) -> Single<OperationQueue> {
        operationQueues.writeRx { dict in
            if dict[keyPathHash] == nil {
                let newOperationQueue = OperationQueue()
                newOperationQueue.maxConcurrentOperationCount = 1
                var newDict = dict
                newDict[keyPathHash] = newOperationQueue
                return newDict
            }
            return dict
        }
        .andThen(operationQueues.readRx())
        .map { $0[keyPathHash]! }
    }
}

extension KeyValueRepoProtocol {
    func createPreferencesTemplate<T>(with template: T, settingsPrefixKey: String) -> PreferencesTemplate<T> {
        PreferencesTemplate(template: template, settingsPrefixKey: settingsPrefixKey, keyValueRepoTarget: self)
    }
}
