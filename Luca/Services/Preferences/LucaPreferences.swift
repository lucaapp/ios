import Foundation
import RxSwift
import DependencyInjection

// MARK: - Implementation
struct LucaPreferencesTemplate {
    let userRegistrationData = PreferencesEntry(key: "userRegistrationData", type: UserRegistrationData.self)
    let uuid = PreferencesEntry(key: "userId", type: UUID.self)

    let kritisData = PreferencesEntry(key: "kritisData", type: KritisData.self)

    /// This property is used to save old UUIDs in case where the user had to be recreated because of 403 issue.
    /// Old UUIDs are needed by HD for recreating the trace ID history.
    let oldUUIDs = PreferencesEntryWithDefaultValue(key: "oldUserIds2", type: [UserIDWithTimestamp].self, defaultValue: [])
    let currentOnboardingPage = PreferencesEntryWithDefaultValue(key: "currentOnboardingPage", type: Int.self, defaultValue: 0)
    let onboardingComplete = PreferencesEntryWithDefaultValue(key: "onboardingComplete", type: Bool.self, defaultValue: false)
    let welcomePresented = PreferencesEntryWithDefaultValue(key: "welcomePresented", type: Bool.self, defaultValue: false)
    let donePresented = PreferencesEntryWithDefaultValue(key: "donePresented", type: Bool.self, defaultValue: false)
    let dataPrivacyPresented = PreferencesEntryWithDefaultValue(key: "dataPrivacyPresented", type: Bool.self, defaultValue: false)
    let phoneNumberVerified = PreferencesEntryWithDefaultValue(key: "phoneNumberVerified", type: Bool.self, defaultValue: false)
    let termsAcceptedVersion = PreferencesEntryWithDefaultValue(key: "termsAcceptedVersion", type: Int.self, defaultValue: 0)
    let verificationRequests = PreferencesEntryWithDefaultValue(key: "verificationRequests", type: [PhoneNumberVerificationRequest].self, defaultValue: [])
    let checkoutNotificationScheduled = PreferencesEntryWithDefaultValue(key: "checkoutNotificationScheduled", type: Bool.self, defaultValue: false)
    let appStoreReviewCheckoutCounter = PreferencesEntryWithDefaultValue(key: "appStoreReviewCheckoutCounter", type: Int.self, defaultValue: 0)
    let shareHealthStatus = PreferencesEntry(key: "shareHealthStatus", type: Bool.self)

    let lucaConnectPostCodeUsageConsent = PreferencesEntry(key: "lucaConnectPostCodeUsageConsent", type: Bool.self)

    let allowOpenExternalLocationLinks = PreferencesEntry(key: "allowOpenExternalLocationLinks", type: Bool.self)
    let lastUsersTouchTimestamp = PreferencesEntryWithDefaultValue(key: "lastUsersTouchTimestamp", type: Date.self, defaultValue: Date(timeIntervalSince1970: 0))

    let dontShowVoluntaryCheckinConsent = PreferencesEntry(key: "dontShowVoluntaryCheckinConsent", type: Bool.self)

    let checkInAnonymousWhenPossible = PreferencesEntry(key: "checkInAnonymousWhenPossible", type: Bool.self)

    let dontShowCheckinConsent = PreferencesEntry(key: "dontShowCheckinConsentNew", type: Bool.self)

    let lucaConnectInitiallyPresented = PreferencesEntryWithDefaultValue(key: "lucaConnectInitiallyPresented", type: Bool.self, defaultValue: false)

    let idProcessStarted = PreferencesEntryWithDefaultValue(key: "idProcessStarted", type: Bool.self, defaultValue: false)

    let lucaConnectNotificationSent = PreferencesEntryWithDefaultValue(key: "lucaConnectNotificationSent", type: Bool.self, defaultValue: false)
}

class LucaPreferences: PreferencesTemplateProtocol {
    typealias EntriesSource = LucaPreferencesTemplate
    @InjectStatic(\.importantDataKeyValueRepo) private var keyValueRepo

    fileprivate let template = LucaPreferencesTemplate()

    private lazy var underlying: PreferencesTemplate<LucaPreferencesTemplate> = {
        PreferencesTemplate(template: template, settingsPrefixKey: "LucaPreferences.", keyValueRepoTarget: keyValueRepo)
    }()

    init() {
        print("LucaPreferences.init")
    }

    deinit {
        print("LucaPreferences.deinit")
    }

    // swiftlint:disable:next line_length
    func set<PreferenceEntryType, ValueType>(_ key: KeyPath<LucaPreferencesTemplate, PreferenceEntryType>, value: ValueType) -> Completable where PreferenceEntryType: PreferencesEntry<ValueType>, ValueType: Codable {
        underlying.set(key, value: value)
    }

    // swiftlint:disable:next line_length
    func get<PreferenceEntryType, ValueType>(_ key: KeyPath<LucaPreferencesTemplate, PreferenceEntryType>) -> Single<ValueType?> where PreferenceEntryType: PreferencesEntry<ValueType>, ValueType: Codable {
        underlying.get(key)
    }

    // swiftlint:disable:next line_length
    func get<PreferenceEntryType, ValueType>(_ key: KeyPath<LucaPreferencesTemplate, PreferenceEntryType>) -> Single<ValueType> where PreferenceEntryType: PreferencesEntryWithDefaultValue<ValueType>, ValueType: Codable {
        underlying.get(key)
    }

    // swiftlint:disable:next line_length
    func remove<PreferenceEntryType, ValueType>(_ key: KeyPath<LucaPreferencesTemplate, PreferenceEntryType>) -> Completable where PreferenceEntryType: PreferencesEntry<ValueType>, ValueType: Codable {
        underlying.remove(key)
    }

    /// Emits next value on every update
    func changes<T>(_ key: KeyPath<LucaPreferencesTemplate, PreferencesEntryWithDefaultValue<T>>) -> Observable<T> {
        underlying.changes(key)
    }

    /// Emits next value on nevery update and the current one on subscription
    func currentAndChanges<T>(_ key: KeyPath<LucaPreferencesTemplate, PreferencesEntryWithDefaultValue<T>>) -> Observable<T> where T: Codable {
        underlying.currentAndChanges(key)
    }

    /// Emits next value on every update
    func changes<T>(_ key: KeyPath<LucaPreferencesTemplate, PreferencesEntry<T>>) -> Observable<T?> where T: Codable {
        underlying.changes(key)
    }

    /// Emits next value on nevery update and the current one on subscription
    func currentAndChanges<T>(_ key: KeyPath<LucaPreferencesTemplate, PreferencesEntry<T>>) -> Observable<T?> where T: Codable {
        underlying.currentAndChanges(key)
    }

    // swiftlint:disable:next line_length
    func changeArray<PreferencesEntryType, T>(_ key: KeyPath<LucaPreferencesTemplate, PreferencesEntryType>, transaction: @escaping ([T]) -> [T]) -> Single<[T]> where PreferencesEntryType: PreferencesEntry<[T]>, T: Decodable, T: Encodable {
        underlying.changeArray(key, transaction: transaction)
    }
}

// MARK: - Shortcut methods for working with user registration data
extension LucaPreferences {
    private var getOrCreateUserRegistrationData: Single<UserRegistrationData> {
        self.get(\.userRegistrationData).map { $0 ?? UserRegistrationData() }
    }
    func get<T>(_ key: KeyPath<UserRegistrationData, T>) -> Single<T> where T: Codable {
        getOrCreateUserRegistrationData.map { $0[keyPath: key] }
    }
    func set<T>(_ key: WritableKeyPath<UserRegistrationData, T>, value: T) -> Completable where T: Codable {
        getOrCreateUserRegistrationData.flatMapCompletable {
            var copy = $0
            copy[keyPath: key] = value
            return self.set(\.userRegistrationData, value: copy)
        }
    }
}

infix operator ??? : NilCoalescingPrecedence
func ??? <T>(lhs: T??, rhs: T) -> T {
    (lhs ?? rhs) ?? rhs
}
func ??? <T>(lhs: T???, rhs: T) -> T {
    ((lhs ?? rhs) ?? rhs) ?? rhs
}
