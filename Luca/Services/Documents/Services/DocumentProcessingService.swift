import Foundation
import RxSwift
import RxRelay
import RealmSwift
import SwiftJWT
import DependencyInjection

/// Tool for document parsing, running validation logic and guaranteing the uniqueness of imported document
class DocumentProcessingService {
    @InjectStatic(\.documentFactory) private var documentFactory
    @InjectStatic(\.documentRepoService) private var documentRepoService
    @InjectStatic(\.documentUniquenessChecker) private var uniquenessChecker
    @InjectStatic(\.backendAddressV3) private var backendAddress

    private let onDocumentWillDeleteInvoker = DeferredEvent<Document>.create(timeout: 5.0)

    private var documentValidators: [DocumentValidator] = []

    private(set) var deeplinkPrefixArray: [String] = []

    /// Captured deep links
    let deeplinkStore = BehaviorSubject(value: String())

    init() {
        deeplinkPrefixArray = [
            backendAddress
            .host
                .appendingPathComponent("webapp")
                .appendingPathComponent("testresult")
                .absoluteString + "/#",

            backendAddress
                .host
                .appendingPathComponent("webapp")
                .appendingPathComponent("appointment")
                .absoluteString + "/?"
        ]
    }

    func register(validator: DocumentValidator) {
        documentValidators.append(validator)
    }

    /// Filter out invalid QR tests, save, and update validTests with new array of tests
    /// - Parameter qr: Payload from qr tag
    /// - Parameter additionalValidators: initial validators required to run once after scan
    /// - Returns: Completable
    func parseQRCode(qr: String, additionalValidators: [DocumentValidator]) -> Completable {
        documentFactory.createDocument(from: qr)
            .flatMap { self.validate($0, with: additionalValidators).andThen(Single.just($0)) }
            .flatMap { self.uniquenessChecker.redeem(document: $0).andThen(Single.just($0)) }
            .asObservable()
            .asSingle()
            .flatMap { [unowned self] in
                self.documentRepoService.store(document: $0).andThen(Single.just($0))
            }
            .asCompletable()
    }

    private func validate(_ document: Document, with additionalValidators: [DocumentValidator] = []) -> Completable {
        let validators = documentValidators + additionalValidators
        return Completable.zip(validators.map { $0.validate(document: document) })
    }

    func revalidateSavedTests() -> Completable {
        documentRepoService.load()
            .asObservable()
            .flatMap { Observable.from($0) }
            .flatMap { document in
                self.validate(document).catch { error in
                    // Do not remove unverified vaccination from already imported documents. Display vaccination with unverified icon.
                    if error is DGCVerificationValidatorError {
                        return Completable.empty()
                    }
                    return self.remove(document: document)
                }
            }
            .ignoreElementsAsCompletable()
    }

    func remove(document: Document) -> Completable {
        onDocumentWillDeleteInvoker.invoke(value: document)
        .andThen(uniquenessChecker.release(document: document))
        .andThen(documentRepoService.remove(identifier: document.identifier))
    }
}

enum CoronaTestProcessingError: LocalizedTitledError {
    case parsingFailed
    case validationFailed
    case verificationFailed
    case nameValidationFailed
    case expired
    case positiveTest
    case inFuture
    case noIssuer
    case invalidChildAge
}

extension CoronaTestProcessingError {
    var errorDescription: String? {
        switch self {
        case .parsingFailed: return L10n.IOSApp.Test.Result.Parsing.error
        case .validationFailed: return L10n.IOSApp.Test.Result.Validation.error
        case .verificationFailed: return L10n.IOSApp.Test.Result.Verification.error
        case .noIssuer: return L10n.IOSApp.Test.Result.Verification.error
        case .nameValidationFailed: return L10n.IOSApp.Test.Result.Name.Validation.error
        case .expired: return L10n.IOSApp.Test.Result.Expiration.error
        case .positiveTest: return L10n.IOSApp.Test.Result.Positive.error
        case .invalidChildAge: return L10n.IOSApp.Test.Result.Child.Age.error
        case .inFuture: return L10n.IOSApp.Test.Result.Future.error
        }
    }

    var localizedTitle: String {
        switch self {
        case .verificationFailed: return L10n.IOSApp.Test.Result.Verification.error
        case .positiveTest: return L10n.IOSApp.Test.Result.Positive.Error.title
        case .inFuture: return L10n.IOSApp.General.OperationFailure.Error.title
        default:
            return L10n.IOSApp.Test.Result.Error.title
        }
    }
}

extension DocumentProcessingService {
    var onDocumentWillDelete: DeferredEvent<Document> {
        onDocumentWillDeleteInvoker.event
    }
}
