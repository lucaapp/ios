import Foundation
import RxSwift
import DependencyInjection

class DocumentPersonAssociationService {
    @InjectStatic(\.documentRepoService) private var documentRepo
    @InjectStatic(\.personService) private var personService
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.dgcVerificationService) private var verificationService

    /// Emits all saved persons and their documents on subscribe and every change
    var currentAndNewTestAssociations: Observable<[Person: [Document]]> {
        self.documentRepo
            .currentAndNewTests
            .flatMap { [weak self] _  -> Single<[Person: [Document]]> in
                guard let self = self else {return Single.just([:]) }
                return Single.zip( self.createUsersPerson(),
                                   self.personService.retrieve() )
                    .map { [$0.0] + $0.1 }
                    .flatMap(self.documents(for:))
            }
    }

    /// Creates a `Person` object from currently logged in user.
    func createUsersPerson() -> Single<Person> {
        lucaPreferences
            .get(\.userRegistrationData)
            .map { Person(firstName: $0?.firstName ?? "", lastName: $0?.lastName ?? "", type: .user) }
    }

    /// Emits documents that are not `AssociableToIdentity`.
    func unownedDocuments() -> Single<[Document]> {
        documentRepo.load()
            .asObservable()
            .flatMap { Observable.from($0) }
            .filter { !($0 is AssociableToIdentity) }
            .toArray()
    }

    /// Emits all documents for every given person
    func documents(for persons: [Person]) -> Single<[Person: [Document]]> {
        Observable.from(persons)
            .flatMap { person in
                self.documentRepo
                    .load()
                    .asObservable()
                    .flatMap { Observable.from($0) }
                    .compactMap { $0 as? Document & AssociableToIdentity }
                    .filter { $0.belongs(to: person) }
                    .map { $0 as Document }
                    .toArray()
                    .flatMap(self.evaluateDocumentCombinations(for:))
                    .map { (person: person, documents: $0) }
            }
            .toArray()
            .map { (items: [(person: Person, documents: [Document])]) -> [Person: [Document]] in
                var retVal: [Person: [Document]] = [:]
                for item in items {
                    retVal[item.person] = item.documents
                }
                return retVal
            }
    }

    /// Check for invalid DGC documents. This just includes documents which confirm to `ConformsDGC` and fail verification. Needed to show specific alerts
    func invalidDocuments(for person: Person) -> Single<[Document]> {
        documents(for: [person])
            .asObservable()
            .map { $0.values.flatMap { $0 } }
            .flatMap { Observable.from($0) }
            .deferredFilter { document in
                if let dgc = document as? ConformsDGC {
                    return self.verificationService.verifyCode(dgc.cert.hCert).map { !$0 }
                }
                return .just(false)
            }
            .toArray()
    }

    /// Analyzes all imported documents and emits current health status.
    func healthStatus(for person: Person) -> Single<UInt8> {
        documents(for: [person])
            .asObservable()
            .map { $0.values.flatMap { $0 } }
            .flatMap { Observable.from($0) }
            .deferredFilter { document in
                if let dgc = document as? ConformsDGC {
                    return self.verificationService.verifyCode(dgc.cert.hCert)
                }
                return .just(true)
            }
            .toArray()
            .flatMap { $0.healthStatus() }
    }

    /// Check if certain documents change users `HealthStatus` and change other document validation accordingly
    private func evaluateDocumentCombinations(for documents: [Document]) -> Single<[Document]> {
        documents.healthStatus()
            .map { [weak self] status -> [Document] in
                if status & HealthStatus.recovered.rawValue == HealthStatus.recovered.rawValue {
                    return self?.updateCompleteVaccinations(in: documents) ?? documents
                }
                return documents
            }
    }

    private func updateCompleteVaccinations(in documents: [Document]) -> [Document] {
        var updatedDocuments = [Document]()
        documents.forEach {
            if var vaccination = $0 as? Vaccination {
                vaccination.skipValidityInterval = true
                updatedDocuments.append(vaccination)
            } else {
                updatedDocuments.append($0)
            }
        }
        return updatedDocuments
    }
}

extension Array where Element == Document {
    /// Analyzes documents and emits current health status.
    ///
    /// It doesn't verify the `DGCVaccination`s. Use `DocumentPersonAssociationService.healthStatus()` instead.
    fileprivate func healthStatus() -> Single<UInt8> {
        Observable<HealthStatus>.merge(compactMap { $0 as? ProvidesHealthStatus }.map { $0.healthStatus.asObservable() })
            .toArray()
            .map { (healthStates: [HealthStatus]) in
                Set(healthStates).reduce(0) { partialResult, healthStatus in
                    partialResult + healthStatus.rawValue
                }
            }
            .map {
                if $0 == 0 {
                    return HealthStatus.notShared.rawValue
                }
                return $0
            }
    }
}
