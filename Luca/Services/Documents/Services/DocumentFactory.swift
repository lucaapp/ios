import Foundation
import RxSwift
import RealmSwift
import DependencyInjection

extension DocumentProperties {
    var parserCacheKey: PreferencesEntryWithDefaultValue<Int> {
        PreferencesEntryWithDefaultValue(key: "parser", type: Int.self, defaultValue: 0)
    }
}

/// Parses serialized strings to `Document` instances with registered parsers
class DocumentFactory {

    @InjectStatic(\.documentsPreferences) private var documentsPreferences
    private var parsers: [DocumentParser] = []

    func register(parser: DocumentParser) {
        parsers.append(parser)
    }

    /// Tries to load a `Document` from `DocumentPayload` using cached parse key if available.
    ///
    /// If something goes wrong with the cached logic, `createDocument(from code: String)` will be used as fallback
    func createDocument(from payload: DocumentPayload) -> Single<Document> {
        documentsPreferences.get(\.parserCacheKey, for: payload)
        .map { (parserKey: Int) in self.parser(for: parserKey) }
        .flatMap { (parser: DocumentParser?) in
            if let parser = parser {
                return parser.parse(code: payload.originalCode)
            }
            throw CoronaTestProcessingError.parsingFailed
        }

        // If there was any error with cached parser (no value or parser failed), try to parse with old logic
        .catch { _ in
            self.createDocument(from: payload.originalCode)
        }
    }

    func createDocument(from code: String) -> Single<Document> {

        let observables = parsers
            .map { parser in
                parser.parse(code: code)

                // Save the succesful parser for future uses
                .flatMap { (document: Document) in
                    self.documentsPreferences.set(\.parserCacheKey, value: self.key(for: parser), for: document)
                        .andThen(Single.just(document))
                        // Return the document if something should go wrong with the caching mechanism.
                        .catchAndReturn(document)
                }
                .asObservable()
                .onErrorComplete()
            }

        return Observable.concat(observables)
            .take(1)
            .asSingle()
            .catch { _ in Single.error(CoronaTestProcessingError.parsingFailed) }
    }

    private func key(for parser: DocumentParser) -> Int {
        let key = String(describing: parser).data(using: .utf8) ?? Data()
        let keyHash = Int(key.crc32)
        return keyHash
    }

    private func parser(for key: Int) -> DocumentParser? {
        parsers
            .map { (key: self.key(for: $0), parser: $0) }
            .filter { $0.key == key }
            .map { $0.parser }
            .first
    }

}
