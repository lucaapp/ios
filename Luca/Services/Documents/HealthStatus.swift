import Foundation
import RxSwift

enum HealthStatus: UInt8 {
    case notShared = 0x01
    case testedQuick = 0x02
    case testedPCR = 0x04
    case recovered = 0x08
    case vaccinated = 0x10
    case boostered = 0x20
}

protocol ProvidesHealthStatus {

    /// Emits health status if it's a valid certificate or completes empty otherwise
    var healthStatus: Maybe<HealthStatus> { get }
}
