import Foundation
import RxSwift
import DependencyInjection

/// Validates if given document belongs to user OR to one of saved children
class UserOrChildValidator: DocumentValidator {
    @InjectStatic(\.personRepo) private var personRepo
    private let userIdentityValidator: DocumentValidator

    init(userIdentityValidator: DocumentValidator) {
        self.userIdentityValidator = userIdentityValidator
    }

    func validate(document: Document) -> Completable {
        childrenValidators()
            .map { (childrenValidators: [ChildValidator]) -> CompoundValidatorOr in
                var validators: [DocumentValidator] = childrenValidators
                validators.append(self.userIdentityValidator)
                return CompoundValidatorOr(validators: validators)
            }
            .flatMapCompletable { $0.validate(document: document) }
    }

    private func childrenValidators() -> Single<[ChildValidator]> {
        personRepo
            .restore()
            .asObservable()
            .flatMap { Observable.from($0) }
            .map { ChildValidator(personSource: Single.just($0)) }
            .toArray()
    }
}
