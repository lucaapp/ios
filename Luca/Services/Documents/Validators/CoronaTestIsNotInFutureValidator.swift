import Foundation
import RxSwift
import DependencyInjection

class CoronaTestIsNotInFutureValidator: DocumentValidator {
    @InjectStatic(\.timeProvider) private var timeProvider

    func validate(document: Document) -> Completable {
        Maybe<CoronaTest>.from { document as? CoronaTest }
            .map { $0.issuedAt }
            .flatMap { issuedAt -> Maybe<Never> in
                if issuedAt < self.timeProvider.now {
                    return Maybe.empty()
                }
                return Maybe.error(CoronaTestProcessingError.inFuture)
            }
            .asObservable()
            .ignoreElementsAsCompletable()
    }

}
