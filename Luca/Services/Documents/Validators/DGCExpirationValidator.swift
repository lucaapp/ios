import Foundation
import RxSwift
import SwiftDGC
import DependencyInjection

class DGCExpirationValidator: DocumentValidator {
    @InjectStatic(\.timeProvider) private var timeProvider

    /// Checks if expiration date of DGC signature is reached
    func validate(document: Document) -> Completable {
        Maybe.from { document as? DGCVaccination }
        .flatMap { document -> Maybe<Never> in
            if document.cert.hCert.iat > self.timeProvider.now {
                return Maybe.error(CoronaTestProcessingError.inFuture)
            }
            if document.cert.hCert.exp < self.timeProvider.now {
                return Maybe.error(CoronaTestProcessingError.expired)
            }
            return Maybe.empty()
        }
        .asObservable()
        .ignoreElementsAsCompletable()
    }
}
