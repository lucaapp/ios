import UIKit
import RxSwift
import DependencyInjection

class DGCVerificationValidator: DocumentValidator {

    @InjectStatic(\.dgcVerificationService) private var dgcVerificationService

    func validate(document: Document) -> Completable {
        Maybe.from { document as? ConformsDGC }
            .flatMap { self.dgcVerificationService.verifyCode($0.cert.hCert).asMaybe().catch { _ in throw DGCVerificationValidatorError.notVerified } }
            .flatMap { isVerified -> Maybe<Void> in
                if !isVerified {
                    throw DGCVerificationValidatorError.notVerified
                }
                return Maybe.empty()
            }
            .asObservable()
            .ignoreElementsAsCompletable()
    }
}

enum DGCVerificationValidatorError: LocalizedTitledError {

    case notVerified

}

extension DGCVerificationValidatorError {

    var localizedTitle: String {
        switch self {
        case .notVerified: return L10n.IOSApp.Document.Verification.Failed.title
        }

    }

    var errorDescription: String? {
        switch self {
        case .notVerified: return L10n.IOSApp.Document.Verification.Failed.description
        }
    }
}
