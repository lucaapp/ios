import Foundation
import RxSwift
import UIKit

/// Warns users if date of birth does not match between several vaccinations. If agreed, user may proceed with the import
class VaccinationBirthDateValidator: DocumentValidator {
    private let presenter: UIViewController
    private let documentSource: Observable<[Document]>
    private let personsSource: Single<[Person]>
    private let userFirstName: String
    private let userLastName: String

    init(presenter: UIViewController, userFirstName: String, userLastName: String, documentSource: Observable<[Document]>, personsSource: Single<[Person]>) {
        self.presenter = presenter
        self.userFirstName = userFirstName
        self.userLastName = userLastName
        self.documentSource = documentSource
        self.personsSource = personsSource
    }

    private func findOwner(vaccination: Vaccination) -> Maybe<(String, String)> {
        Observable.merge(getChildren(), Observable.of((userFirstName, userLastName)))
            .filter { vaccination.belongs(to: Person(firstName: $0.0, lastName: $0.1, type: .unknown)) }
            .take(1)
            .asMaybe()
    }

    private func getChildren() -> Observable<(String, String)> {
        personsSource
            .asObservable()
            .flatMap { Observable.from($0) }
            .map { person -> (String, String) in return (person.firstName, person.lastName) }
    }

    private func findVaccinations(firstName: String, lastName: String) -> Single<[Vaccination]> {
        documentSource
            .take(1)
            .flatMap { Observable.from($0) }
            .compactMap { $0 as? Vaccination }
            .filter { $0.belongs(to: Person(firstName: firstName, lastName: lastName, type: .unknown)) }
            .toArray()
    }

    func validate(document: Document) -> Completable {
        guard let vaccinationDocument = document as? Vaccination else {
            return Completable.empty()
        }

        return findOwner(vaccination: vaccinationDocument)
            .asObservable()
            .flatMapLatest { self.findVaccinations(firstName: $0, lastName: $1).asObservable() }
            .flatMap { vaccinations -> Completable in

                let vaccinationsWithMismatchedBirthday = vaccinations
                    .filter { $0.doseNumber != vaccinationDocument.doseNumber }
                    .filter { $0.dateOfBirth != vaccinationDocument.dateOfBirth }

                if !vaccinationsWithMismatchedBirthday.isEmpty {
                    return UIAlertController.okAndCancelAlertRx(viewController: self.presenter,
                                                                title: L10n.IOSApp.Navigation.Basic.attention,
                                                                message: L10n.IOSApp.Test.BirthdateMismatch.Error.description,
                                                                okTitle: L10n.IOSApp.Test.BirthdateMismatch.Error.ok)
                        .do(onSuccess: { agreed in
                            if !agreed {
                                throw SilentErrorDefaults.userDeclined
                            }
                        })
                        .asCompletable()
                }
                return Completable.empty()
            }
            .ignoreElementsAsCompletable()
    }
}
