import Foundation
import RxSwift

class DocumentValidityValidator: DocumentValidator {
    func validate(document: Document) -> Completable {
        Single.from { document }
        .do(onSuccess: {
            if $0.needsRemoval {
                throw CoronaTestProcessingError.expired
            }
        })
        .asCompletable()
    }
}
