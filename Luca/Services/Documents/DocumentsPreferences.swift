import Foundation
import RxSwift
import DependencyInjection

class DocumentsPreferences<PropertySource> {

    @InjectStatic(\.keyValueRepo) private var keyValueRepo
    private var propertySource: PropertySource

    init(propertySource: PropertySource) {
        self.propertySource = propertySource
    }

    func set<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>, value: T, for document: String) -> Completable where T: Codable {
        preferenes(for: document).set(key, value: value)
    }

    func get<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>, for document: String) -> Single<T> where T: Codable {
        preferenes(for: document).get(key)
    }

    func remove<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>, for document: String) -> Completable where T: Codable {
        preferenes(for: document).remove(key)
    }

    func set<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>, value: T, for document: String) -> Completable where T: Codable {
        preferenes(for: document).set(key, value: value)
    }

    func get<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>, for document: String) -> Single<T?> where T: Codable {
        preferenes(for: document).get(key)
    }

    func remove<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>, for document: String) -> Completable where T: Codable {
        preferenes(for: document).remove(key)
    }

    private func preferenes(for document: String) -> DefinedPreferences<PropertySource> {
        KeyValueRepoBasedDefinedPreferences(
            propertySource: propertySource,
            repo: keyValueRepo,
            keysPrefix: "document." + hash(for: document)
        )
    }

    private func hash(for document: String) -> String {
        "\((document.data(using: .utf8) ?? Data()).crc32)"
    }
}

// Helpers for the same data but in form of a `Document`
extension DocumentsPreferences {

    func set<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>, value: T, for document: Document) -> Completable where T: Codable {
        preferenes(for: document.originalCode).set(key, value: value)
    }

    func get<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>, for document: Document) -> Single<T> where T: Codable {
        preferenes(for: document.originalCode).get(key)
    }

    func remove<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>, for document: Document) -> Completable where T: Codable {
        preferenes(for: document.originalCode).remove(key)
    }

    func set<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>, value: T, for document: Document) -> Completable where T: Codable {
        preferenes(for: document.originalCode).set(key, value: value)
    }

    func get<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>, for document: Document) -> Single<T?> where T: Codable {
        preferenes(for: document.originalCode).get(key)
    }

    func remove<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>, for document: Document) -> Completable where T: Codable {
        preferenes(for: document.originalCode).remove(key)
    }
}

// Helpers for the same data but in form of a `DocumentPayload`
extension DocumentsPreferences {

    func set<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>, value: T, for document: DocumentPayload) -> Completable where T: Codable {
        preferenes(for: document.originalCode).set(key, value: value)
    }

    func get<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>, for document: DocumentPayload) -> Single<T> where T: Codable {
        preferenes(for: document.originalCode).get(key)
    }

    func remove<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>, for document: DocumentPayload) -> Completable where T: Codable {
        preferenes(for: document.originalCode).remove(key)
    }

    func set<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>, value: T, for document: DocumentPayload) -> Completable where T: Codable {
        preferenes(for: document.originalCode).set(key, value: value)
    }

    func get<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>, for document: DocumentPayload) -> Single<T?> where T: Codable {
        preferenes(for: document.originalCode).get(key)
    }

    func remove<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>, for document: DocumentPayload) -> Completable where T: Codable {
        preferenes(for: document.originalCode).remove(key)
    }
}
