import Foundation
import RxSwift

enum VaccinationType {

    /// BioNTech/Pfizer
    case comirnaty

    /// Johnson & Jonson
    case janssen

    case moderna

    /// AstraZeneca
    case vaxzevria

    case sputnikV

    /// Novavax
    case nuvaxovid

    case unknown

    // Incoming, but still not approved by EMA
    //
    // Sanofi-GSK
    // Valneva
    // CureVac
}

extension VaccinationType {

    /// Number of doses needed to complete the vaccination
    var dosesNumber: Int {
        switch self {
        case .comirnaty: return 2
        case .janssen: return 1
        case .moderna: return 2
        case .vaxzevria: return 2
        case .sputnikV: return 2
        case .nuvaxovid: return 2
        case .unknown: return 2
        }
    }
}

extension VaccinationType {

    /// Localized name
    var readableName: String {
        switch self {
        case .comirnaty: return L10n.IOSApp.Vaccine.Result.comirnaty
        case .janssen: return L10n.IOSApp.Vaccine.Result.janssen
        case .moderna: return L10n.IOSApp.Vaccine.Result.moderna
        case .vaxzevria: return L10n.IOSApp.Vaccine.Result.vaxzevria
        case .sputnikV: return L10n.IOSApp.Vaccine.Result.sputnikV
        case .nuvaxovid: return L10n.IOSApp.Vaccine.Result.nuvaxovid
        case .unknown: return L10n.IOSApp.General.unknown
        }
    }
}

protocol Vaccination: Document, AssociableToIdentity, ContainsDateOfBirth, ProvidesHealthStatus {

    /// user date of birth
    var dateOfBirth: Date { get }

    var vaccineType: VaccinationType { get }

    var doseNumber: Int { get }

    /// Total number of doses. It doesn't equal standard amount of doses for various vaccination types.
    var totalDosesNumber: Int { get }

    /// testing laboratory
    var laboratory: String { get }

    /// Is true, if this certificate contains the final dose of those required. E.g. it would be 2 for Moderna, Pfizer etc. and 1 for Johnsson&Jonhnsson
    var isCompleteDose: Bool { get }

    /// booster vaccination
    var isAdditionalDose: Bool { get }

    /// complete vaccinations might skip waiting period if other documents cross-validate their status (e.g. documents after recovery don't need waiting period to be valid)
    var skipValidityInterval: Bool { get set }
}

extension Vaccination {
    var identifier: Int {
        guard let payloadData = originalCode.data(using: .utf8) else {
            return -1
        }
        return Int(payloadData.crc32)
    }

    var daysUntilValidity: Int {
        return 15
    }

    var daysSinceIssue: Int {
        return Calendar.current.dateComponents([.day], from: issuedAt, to: Date.now).day ?? Int.max
    }

    var fullVaccinationDate: Date? {
        if isCompleteDose {
            return Calendar.current.date(byAdding: .day, value: daysUntilValidity, to: issuedAt)
        }
        return issuedAt
    }

    var fullyVaccinatedInDays: Int {
        return daysUntilValidity - daysSinceIssue
    }

    var isAdditionalDose: Bool {
        return doseNumber > vaccineType.dosesNumber ||
        // Covers the case when receiving a 2/1 booster shot after recovery, for vaccines that require 2 shots.
        (totalDosesNumber < vaccineType.dosesNumber && doseNumber > totalDosesNumber)
    }

    var isCompleteDose: Bool {
        return doseNumber == vaccineType.dosesNumber ||
        // Covers the case when receiving a 1/1 shot after recovery, for vaccines that require 2 shots.
        (totalDosesNumber < vaccineType.dosesNumber && doseNumber == totalDosesNumber)
    }

    func isComplete() -> Bool {
        let dateIsValid = daysSinceIssue >= daysUntilValidity
        return isAdditionalDose || skipValidityInterval || (isCompleteDose && dateIsValid)
    }

    var recentUntil: Date {
        Calendar.current.date(byAdding: .month, value: 3, to: issuedAt) ?? issuedAt
    }

    var expiresAt: Date {
        Calendar.current.date(byAdding: .month, value: 9, to: issuedAt) ?? issuedAt
    }

    var removedAt: Date {
        Calendar.current.date(byAdding: .year, value: 5, to: issuedAt) ?? issuedAt
    }
}

extension Vaccination {
    var healthStatus: Maybe<HealthStatus> {
        .from {

            if self.isValid {
                if self.isAdditionalDose {
                    return .boostered
                }

                if self.isComplete() {
                    return .vaccinated
                }
            }

            return nil
        }
    }
}
