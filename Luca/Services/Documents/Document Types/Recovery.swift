import Foundation
import RxSwift

protocol Recovery: Document, AssociableToIdentity, ContainsDateOfBirth, ProvidesHealthStatus {

    /// Encoded QR code
    var originalCode: String { get set }

    /// issue date
    var validFromDate: Date { get }

    /// expiration date
    var validUntilDate: Date { get }

    /// testing laboratory
    var laboratory: String { get }

    /// user date of birth
    var dateOfBirth: Date { get }
}

extension Recovery {
    var identifier: Int {
        guard let payloadData = originalCode.data(using: .utf8) else {
            return -1
        }
        return Int(payloadData.crc32)
    }

    var recentUntil: Date {
        Calendar.current.date(byAdding: .month, value: 3, to: issuedAt) ?? issuedAt
    }

    var issuedAt: Date {
        validFromDate
    }

    var expiresAt: Date { validUntilDate }

    var removedAt: Date {
        Calendar.current.date(byAdding: .year, value: 5, to: issuedAt) ?? issuedAt
    }
}

extension Recovery {
    var healthStatus: Maybe<HealthStatus> {
        .from {
            self.isValid ? .recovered : nil
        }
    }
}
