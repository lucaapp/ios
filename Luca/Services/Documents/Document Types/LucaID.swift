import Foundation
import RxSwift

class LucaID: Document, AssociableToIdentity, HasFirstAndLastName {
    var identifier: Int {
        guard let payloadData = data.state.receiptJWS?.data(using: .utf8) else {
           return -1
       }
       return Int(payloadData.crc32)
    }

    var recentUntil: Date {
        Calendar.current.date(byAdding: .year, value: 200, to: Date(timeIntervalSince1970: Double(timestamp))) ?? Date.now
    }

    var issuedAt: Date {
        data.minimalIdentityJWT?.claims.iat ?? Date()
    }

    func belongs(to identity: HasFirstAndLastName) -> Bool {
        lastName == identity.lastName && firstName == identity.firstName
    }

    var dateOfBirth: String {
        if let dateStr = data.minimalIdentityJWT?.claims.vc.credentialSubject.birthDate,
           let d =  Date.formatDateString(dateString: dateStr, format: "yyyy-MM-dd") {
            return d.formattedDate
        } else {
            return ""
        }
    }

    var data: LucaIDParsedData

    var timestamp: Int

    var originalCode: String {
        data.state.receiptJWS ?? ""
    }

    var hashSeed: String { originalCode }

    var expiresAt: Date {
        Calendar.current.date(byAdding: .year, value: 200, to: Date(timeIntervalSince1970: Double(timestamp))) ?? Date.now
    }

    init(payload: LucaIDParsedData) {
        self.data = payload
        self.timestamp = 0
    }

    var firstName: String {
        data.minimalIdentityJWT?.claims.vc.credentialSubject.givenName ?? ""
    }

    var lastName: String {
        data.minimalIdentityJWT?.claims.vc.credentialSubject.familyName ?? ""
    }

    var image: UIImage? {
        guard let imageAsBase64 = data.faceJWT?.claims.vc.credentialSubject.image else {
            return nil
        }
        let dataDecoded = Data(base64Encoded: imageAsBase64, options: .ignoreUnknownCharacters)!
        return UIImage(data: dataDecoded)
    }

    var deletionConfirmationMessage: String {
        L10n.IOSApp.Id.Delete.description
    }

}

extension LucaID {
    var contentDescription: String {
        let description = createDescription()

        return L10n.IOSApp.DataReport.Document.Appointment.description(
            L10n.IOSApp.DataReport.Document.CertificateType.appointment,
            description,
            originalCode,
            L10n.IOSApp.DataReport.Document.Certificate.storagePeriodAppointment
        )
    }

    private func createDescription() -> String {
        ""
    }
}
