import Foundation

protocol Ticket: Document, AssociableToIdentity {

    var startTime: Date { get }

    var endTime: Date { get }

    var firstName: String { get }

    var lastName: String { get }

    var issuer: String { get }

    var category: TicketCategory? { get }

    var ticketId: String { get }

    var verificationId: String? { get }

}
extension Ticket {

    func belongs(to identity: HasFirstAndLastName) -> Bool {
        firstName == identity.firstName && lastName == identity.lastName
    }

}
