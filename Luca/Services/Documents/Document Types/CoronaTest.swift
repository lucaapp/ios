import Foundation
import RxSwift
import SwiftJWT

public typealias TestClaims = Codable & Claims
public typealias TestClaimsWithFingerprint = Codable & ClaimsWithFingerprint

protocol CoronaTest: Document, AssociableToIdentity, ProvidesHealthStatus {

    /// Encoded QR code
    var originalCode: String { get set }

    /// test type e.g. PCR
    var testType: CoronaTestType { get }

    /// testing laboratory
    var laboratory: String { get }

    /// testing doctor
    var doctor: String { get }

    /// check if test result is negative
    var isNegative: Bool { get }

    /// check if positive test is valid (if it is a positive PCR test over 14 days)
    var isValidPositive: Bool { get }

    /// test provider
    var provider: String { get }

    /// show QR code in `CoronaTestView`
    var showQRCode: Bool { get }
}

extension CoronaTest {
    var identifier: Int {
        guard let payloadData = originalCode.data(using: .utf8) else {
            return -1
        }
        return Int(payloadData.crc32)
    }

    var validity: (duration: Int, unit: Calendar.Component) {
        let validity: Int
        var unit: Calendar.Component = .hour
        switch testType {
        case .pcr:
            validity = isNegative ? 72 : 6
            unit = isNegative ? .hour : .month
        case .fast:
            validity = 48
        case .other:
            validity = 48
        }
        return (validity, unit)
    }

    var recentUntil: Date {
        Calendar.current.date(byAdding: .day, value: 1, to: issuedAt) ?? issuedAt
    }

    var expiresAt: Date {
        Calendar.current.date(byAdding: validity.unit, value: validity.duration, to: issuedAt) ?? issuedAt
    }

    var isValidPositive: Bool {
        guard testType == .pcr, !isNegative else {
            return false
        }

        if let validityFrom = Calendar.current.date(byAdding: .day, value: 14, to: issuedAt) {
            return Date.now > validityFrom
        }
        return false
    }

    var showQRCode: Bool {
        return false
    }
}

enum CoronaTestType {
    case pcr
    case fast
    case other
}

extension CoronaTestType {
    var localized: String {
        switch self {
        case .pcr:
            return L10n.IOSApp.Test.Result.pcr
        case .fast:
            return L10n.IOSApp.Test.Result.fast
        case .other:
            return L10n.IOSApp.Test.Result.other
        }
    }
}

extension CoronaTest {
    var healthStatus: Maybe<HealthStatus> {
        .from {
            if isValidPositive {
                return .recovered
            }
            guard isNegative, isValid else {
                return nil
            }
            if testType == .pcr {
                return .testedPCR
            }
            if testType == .fast {
                return .testedQuick
            }
            return nil
        }
    }
}
