import Foundation
import RxSwift
import SwiftDGC

class DGCParser: DocumentParser {
    func parse(code: String) -> Single<Document> {
        .from {
            // Remove URL if present
            var parameters = code
            if let index = code.firstIndex(of: "#"),
               code.contains("http://") {
                parameters = String(code.suffix(from: index))
                parameters.removeFirst()
            }

            guard let hCert = HCert(from: parameters) else {
                throw CoronaTestProcessingError.parsingFailed
            }

            let dgcCert = DGCCert(hCert: hCert)

            switch hCert.type {
            case .test:
                if let dgcTest = dgcCert.testStatements.first {
                    return DGCCoronaTest(cert: dgcCert, test: dgcTest, originalCode: parameters)
                }
            case .vaccine:
                if let dgcVaccine = dgcCert.vaccineStatements.first {
                    return DGCVaccination(cert: dgcCert, vaccine: dgcVaccine, originalCode: parameters)
                }
            case .recovery:
                if let dgcRecovery = dgcCert.recoveryStatements.first {
                    return DGCRecovery(cert: dgcCert, recovery: dgcRecovery, originalCode: parameters)
                }
            case .unknown:
                throw CoronaTestProcessingError.parsingFailed
            }
            throw CoronaTestProcessingError.parsingFailed
        }
    }
}
