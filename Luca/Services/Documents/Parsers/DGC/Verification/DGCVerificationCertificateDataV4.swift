import Foundation
import RxSwift

enum DGCKeyServiceBackendError {
}

extension DGCKeyServiceBackendError: RequestError {
    var localizedTitle: String {
        L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        ""
    }
}

struct DGCVerificationCertificates: Codable {
    var certificates: [DGCVerificationCertificate]
}

struct DGCVerificationCertificate: Codable {
    var kid: String
    var certificateType: String
    var country: String
    var rawData: String
    var signature: String
    var thumbprint: String
    var timestamp: String

    var publicKey: SecKey? {
        if let data = rawData.data(using: .utf8) as CFData?,
            let certificate = SecCertificateCreateWithData(nil, data),
           let publicKey = SecCertificateCopyKey(certificate) {
            return publicKey
        } else {
            return nil
        }
    }
}

class DGCVerificationCertificateDataV4 {
    private let backendAddress: BackendAddressV4

    init(backendAddress: BackendAddressV4) {
        self.backendAddress = backendAddress
    }

    func fetchCerts() -> AsyncDataOperation<BackendError<DGCKeyServiceBackendError>, DGCVerificationCertificateData> {
        FetchDGCVerificationCertificateAsyncDataOperation(backendAddress: backendAddress)
    }
}
