import RxSwift
import SwiftDGC
import DependencyInjection

class DGCVerificationService {

    @InjectStatic(\.dgcKeyDataCache) private var dgcKeyDataCache: CachedDataSource<DGCVerificationCertificateData>

    func verifyCode(_ cert: HCert) -> Single<Bool> {
        dgcKeyDataCache.retrieve()
            .compactMap { $0.first }
            .asObservable()
            .asSingle()
            .map { keys in
                let matchingKeys = keys.certs.certificates.filter { $0.kid == cert.kidStr }
                    .filter { X509.isCertificateValid(cert: $0.rawData)
                        && X509.isValid(cert: $0.rawData, withIssueDate: cert.iat, expirationDate: cert.exp)
                        && X509.checkisSuitable(cert: $0.rawData, certType: cert.type)
                        && COSE.verify(_cbor: cert.cborData, with: $0.rawData) }

                return !matchingKeys.isEmpty
            }
    }
}
