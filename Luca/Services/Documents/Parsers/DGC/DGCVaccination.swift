import UIKit
import RxSwift

enum DGCVaccinationType: String, Codable {

    case comirnaty = "EU/1/20/1528"
    case janssen = "EU/1/20/1525"
    case moderna = "EU/1/20/1507"
    case vaxzevria = "EU/1/21/1529"
    case sputnikV = "Sputnik-V"
    case nuvaxovid = "EU/1/21/1618"

    var category: String {
        switch self {
        case .comirnaty: return L10n.IOSApp.Vaccine.Result.comirnaty
        case .janssen: return L10n.IOSApp.Vaccine.Result.janssen
        case .moderna: return L10n.IOSApp.Vaccine.Result.moderna
        case .vaxzevria: return L10n.IOSApp.Vaccine.Result.vaxzevria
        case .sputnikV: return L10n.IOSApp.Vaccine.Result.sputnikV
        case .nuvaxovid: return L10n.IOSApp.Vaccine.Result.nuvaxovid
        }
    }
}

extension VaccinationType {
    init(from dgcVaccination: DGCVaccinationType) {
        switch dgcVaccination {
        case .comirnaty:
            self = .comirnaty
        case .janssen:
            self = .janssen
        case .moderna:
            self = .moderna
        case .vaxzevria:
            self = .vaxzevria
        case .sputnikV:
            self = .sputnikV
        case .nuvaxovid:
            self = .nuvaxovid
        }
    }
}

struct DGCVaccination: Vaccination, ConformsDGC, HasFirstAndLastName {

    var firstName: String
    var lastName: String
    var dateOfBirth: Date
    var vaccineType: VaccinationType
    var doseNumber: Int
    var issuedAt: Date
    var originalCode: String
    var hashSeed: String { originalCode }
    var issuer: String
    var laboratory: String { issuer }
    var vaccinationEntry: DGCVaccinationEntry
    var cert: DGCCert
    var totalDosesNumber: Int
    var skipValidityInterval: Bool

    init(cert: DGCCert, vaccine: DGCVaccinationEntry, originalCode: String) {
        self.firstName = cert.firstName
        self.lastName = cert.lastName
        self.dateOfBirth = cert.dateOfBirth
        self.issuedAt = vaccine.date
        if let gdcVaccination = DGCVaccinationType(rawValue: vaccine.medicalProduct) {
            self.vaccineType = VaccinationType(from: gdcVaccination)
        } else {
            self.vaccineType = .unknown
        }
        self.totalDosesNumber = vaccine.dosesTotal
        self.doseNumber = vaccine.doseNumber
        self.issuer = vaccine.issuer
        self.vaccinationEntry = vaccine
        self.originalCode = originalCode
        self.cert = cert
        self.skipValidityInterval = false
    }
}

extension DGCVaccination {
    var contentDescription: String {
        let contactData = createContactData()
        let description = createDescription()

        return contactData
        + "\n"
        + L10n.IOSApp.DataReport.Document.Certificate.description(
            L10n.IOSApp.DataReport.Document.CertificateType.dgcVaccination,
            description,
            originalCode,
            L10n.IOSApp.DataReport.Document.Certificate.storagePeriodVaccine)
    }

    private func createContactData() -> String {
        L10n.IOSApp.DataReport.Document.ContactData.dateOfBirth(dateOfBirth.formattedDate)
    }

    private func createDescription() -> String {
        L10n.IOSApp.DataReport.Document.DescriptionDetails.dgcVaccination(
            vaccinationEntry.diseaseTargeted,
            vaccinationEntry.vaccineOrProphylaxis,
            vaccineType,
            vaccinationEntry.manufacturer,
            vaccinationEntry.doseNumber,
            vaccinationEntry.dosesTotal,
            vaccinationEntry.date.formattedDate,
            vaccinationEntry.countryCode,
            vaccinationEntry.issuer,
            vaccinationEntry.uvci
        )
    }
}
