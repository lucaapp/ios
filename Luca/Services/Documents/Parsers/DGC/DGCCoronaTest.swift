import Foundation
import RxSwift

enum DGCCoronaTestType: String, Codable {

    case fast = "LP217198-3"
    case pcr = "LP6464-4"
    case unknown

    public init(from decoder: Decoder) throws {
        self = try DGCCoronaTestType(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .unknown
    }

    var category: String {
        switch self {
        case .fast: return L10n.IOSApp.Test.Result.fast
        case .pcr: return L10n.IOSApp.Test.Result.pcr
        default: return L10n.IOSApp.Test.Result.other
        }
    }
}

struct DGCCoronaTest: CoronaTest, ConformsDGC, HasFirstAndLastName {
    var firstName: String
    var lastName: String
    var dateRaw: String
    var issuedAt: Date
    var testType: CoronaTestType
    var laboratory: String
    var isNegative: Bool
    var originalCode: String
    var hashSeed: String { originalCode }
    var showQRCode: Bool { true }
    var testEntry: DGCTestEntry
    var cert: DGCCert
    var provider = "DGC"

    var issuer: String
    var doctor: String { issuer }

    init(cert: DGCCert, test: DGCTestEntry, originalCode: String) {
        self.firstName = cert.firstName
        self.lastName = cert.lastName
        self.dateRaw = test.sampleTimeRaw
        self.issuedAt = test.sampleTime
        self.isNegative = test.resultNegative
        self.laboratory = test.testCenter
        self.issuer = test.issuer
        self.originalCode = originalCode
        self.testEntry = test
        self.cert = cert

        switch DGCCoronaTestType(rawValue: test.type) ?? .unknown {
        case .fast:
            self.testType = .fast
        case .pcr:
            self.testType = .pcr
        case .unknown:
            self.testType = .other
        }
    }
}

extension DGCCoronaTest {
    var contentDescription: String {
        let description = createDescription()

        var formattedDuration = ""
        switch validity.unit {
        case .month:
            formattedDuration = L10n.IOSApp.Test.Result.Duration.months(validity.duration)
        default:
            formattedDuration = L10n.IOSApp.Test.Result.Duration.hours(validity.duration)
        }

        return L10n.IOSApp.DataReport.Document.Certificate.description(
            L10n.IOSApp.DataReport.Document.CertificateType.dgcTest,
            description,
            originalCode,
            L10n.IOSApp.DataReport.Document.Certificate.storagePeriodTest(formattedDuration)
        )
    }

    private func createDescription() -> String {
        L10n.IOSApp.DataReport.Document.DescriptionDetails.dgcTest(
            testEntry.diseaseTargeted,
            testType.localized,
            dateRaw,
            isNegative ? "n" : "p",
            laboratory,
            testEntry.countryCode,
            issuer,
            testEntry.uvci
        )
    }
}
