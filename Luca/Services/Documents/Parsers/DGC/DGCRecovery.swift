import UIKit

struct DGCRecovery: Recovery, ConformsDGC, HasFirstAndLastName {
    var firstName: String
    var lastName: String
    var dateOfBirth: Date
    var validFromDate: Date
    var validUntilDate: Date
    var originalCode: String
    var hashSeed: String { originalCode }
    var issuer: String
    var laboratory: String { issuer }
    var recoveryEntry: DGCRecoveryEntry
    var cert: DGCCert

    init(cert: DGCCert, recovery: DGCRecoveryEntry, originalCode: String) {
        self.firstName = cert.firstName
        self.lastName = cert.lastName
        self.dateOfBirth = cert.dateOfBirth
        self.validFromDate = recovery.validFrom
        self.validUntilDate = recovery.validUntil
        self.issuer = recovery.issuer
        self.recoveryEntry = recovery
        self.originalCode = originalCode
        self.cert = cert
    }
}

extension DGCRecovery {
    var contentDescription: String {
        let contactData = createContactData()
        let description = createDescription()

        return contactData
            + "\n"
            + L10n.IOSApp.DataReport.Document.Certificate.description(
                L10n.IOSApp.DataReport.Document.CertificateType.dgcRecovery,
                description,
                originalCode,
                L10n.IOSApp.DataReport.Document.Certificate.storagePeriodAppointment)
    }

    private func createContactData() -> String {
        L10n.IOSApp.DataReport.Document.ContactData.dateOfBirth(dateOfBirth.formattedDate)
    }

    private func createDescription() -> String {
        L10n.IOSApp.DataReport.Document.DescriptionDetails.dgcRecovery(
            recoveryEntry.diseaseTargeted,
            recoveryEntry.firstPositiveDate,
            recoveryEntry.countryCode,
            issuer,
            validFromDate.formattedDate,
            validUntilDate.formattedDate,
            recoveryEntry.uvci
        )
    }
}
