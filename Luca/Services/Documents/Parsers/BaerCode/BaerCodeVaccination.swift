import Foundation
import RxSwift

struct BaerCodeVaccination: Vaccination, HasFirstAndLastName {

    var version: Int
    var firstName: String
    var lastName: String
    var dateOfBirthInt: Int
    var diseaseType: Int
    var procedures: [BaerCoronaProcedure]
    var procedureOperator: String
    var originalCode: String
    var hashSeed: String { originalCode }
    var skipValidityInterval: Bool

    var dateOfBirth: Date {
        return Date(timeIntervalSince1970: TimeInterval(dateOfBirthInt))
    }

    var vaccineType: VaccinationType {
        procedures.first?.type.vaccinationType ?? .unknown
    }

    var totalDosesNumber: Int {
        vaccineType.dosesNumber
    }

    var doseNumber: Int {
        return procedures.count
    }

    init(payload: BaerCodePayload, originalCode: String) {
        self.version = payload.version
        self.firstName = payload.firstName
        self.lastName = payload.lastName
        self.dateOfBirthInt = payload.dateOfBirthInt
        self.diseaseType = payload.diseaseType
        self.procedures = payload.procedures
        self.procedureOperator = payload.procedureOperator
        self.originalCode = originalCode
        self.skipValidityInterval = false
    }

    var issuedAt: Date {
        let date = procedures[0].date
        return Date(timeIntervalSince1970: TimeInterval(date))
    }

    var testType: String {
        let type = procedures[0].type
        return type.category
    }

    var laboratory: String {
        return procedureOperator
    }

    func daysSinceLastVaccine() -> Int {
        let lastVaccinationDate = Date(timeIntervalSince1970: TimeInterval(procedures[0].date))
        return Calendar.current.dateComponents([.day], from: lastVaccinationDate, to: Date.now).day ?? Int.max
    }

    var identifier: Int {
        var checksum = Data()
        guard let nameData = (firstName + lastName).data(using: .utf8),
              let labData = procedureOperator.data(using: .utf8) else {
            return -1
        }
        checksum = nameData
        checksum.append(procedures[0].date.data)
        checksum.append(labData)
        return Int(checksum.crc32)
    }
}

extension BaerCodeVaccination {
    var contentDescription: String {
        let contactData = createContactData()
        let description = createDescription()

        return contactData
        + "\n"
        + L10n.IOSApp.DataReport.Document.Certificate.description(
            L10n.IOSApp.DataReport.Document.CertificateType.baercodeVaccination,
            description,
            originalCode,
            L10n.IOSApp.DataReport.Document.Certificate.storagePeriodVaccine
        )
    }

    private func createContactData() -> String {
        L10n.IOSApp.DataReport.Document.ContactData.dateOfBirth(dateOfBirth.formattedDate)
    }

    private func createDescription() -> String {
        var descriptionDetails = L10n.IOSApp.DataReport.Document.DescriptionDetails.baercodeVaccination(diseaseType, procedureOperator)
        for (index, procedure) in procedures.enumerated() {
            let string = L10n.IOSApp.DataReport.Document.DescriptionDetails.Baercode.procedure(
                index,
                procedure.type.category,
                Date(timeIntervalSince1970: TimeInterval(procedure.date)).formattedDateTime
            )
            descriptionDetails.append(string)
        }

        return descriptionDetails
    }
}
