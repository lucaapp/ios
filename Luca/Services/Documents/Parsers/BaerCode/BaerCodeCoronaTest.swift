import Foundation
import RxSwift

struct BaerCodeCoronaTest: CoronaTest, HasFirstAndLastName {
    var version: Int
    var firstName: String
    var lastName: String
    var diseaseType: Int
    var procedures: [BaerCoronaProcedure]
    var procedureOperator: String
    var result: Bool
    var originalCode: String
    var hashSeed: String { originalCode }
    var provider = "BärCode"

    init(payload: BaerCodePayload, originalCode: String) {
        self.version = payload.version
        self.firstName = payload.firstName
        self.lastName = payload.lastName
        self.diseaseType = payload.diseaseType
        self.procedures = payload.procedures
        self.procedureOperator = payload.procedureOperator
        self.result = payload.result
        self.originalCode = originalCode
    }

    var issuedAt: Date {
        let date = procedures[0].date
        return Date(timeIntervalSince1970: TimeInterval(date))
    }

    var testType: CoronaTestType {
        switch procedures[0].type {
        case .fast:
            return .fast
        case .pcr:
            return .pcr
        default:
            return .other
        }
    }

    var laboratory: String {
        return procedureOperator
    }

    var doctor: String {
        return " - "
    }

    var isNegative: Bool {
        return !result
    }

    var identifier: Int {
        var checksum = Data()
        guard let nameData = (firstName + lastName).data(using: .utf8),
              let labData = procedureOperator.data(using: .utf8) else {
            return -1
        }
        checksum = nameData
        checksum.append(procedures[0].date.data)
        checksum.append(labData)
        return Int(checksum.crc32)
    }
}

extension BaerCodeCoronaTest {
    var contentDescription: String {
        let description = createDescription()

        var formattedDuration = ""
        switch validity.unit {
        case .month:
            formattedDuration = L10n.IOSApp.Test.Result.Duration.months(validity.duration)
        default:
            formattedDuration = L10n.IOSApp.Test.Result.Duration.hours(validity.duration)
        }

        return L10n.IOSApp.DataReport.Document.Certificate.description(
            L10n.IOSApp.DataReport.Document.CertificateType.baercodeTest,
            description,
            originalCode,
            L10n.IOSApp.DataReport.Document.Certificate.storagePeriodTest(formattedDuration)
        )
    }

    private func createDescription() -> String {
        var descriptionDetails = L10n.IOSApp.DataReport.Document.DescriptionDetails.baercodeTest(isNegative ? "n" : "p", laboratory)
        for (index, procedure) in procedures.enumerated() {
            let string = L10n.IOSApp.DataReport.Document.DescriptionDetails.Baercode.procedure(
                index,
                procedure.type.category,
                Date(timeIntervalSince1970: TimeInterval(procedure.date)).formattedDateTime
            )
            descriptionDetails.append(string)
        }

        return descriptionDetails
    }
}
