import Foundation
import SwiftJWT
import RxSwift
import DependencyInjection

class JWTTicketParser<ClaimsType>: DocumentParser where ClaimsType: Claims & Codable {

    @InjectStatic(\.timeProvider) private var timeProvider

    private func createJWT(code: String) -> Single<JWT<ClaimsType>> {
        Single.from {
            let jwt: JWT<ClaimsType>
            do {
                jwt = try JWT<ClaimsType>(jwtString: code)
            } catch let error {
                if let jwtError = error as? JWTError,
                   jwtError.localizedDescription.contains("JWT verifier failed") {
                    throw JWTTicketParserError.verificationFailed
                } else {
                    throw JWTTicketParserError.parsingFailed
                }
            }
            return jwt
        }
    }

    func parse(code: String) -> Single<Document> {
        // Remove URL from code
        var parameters = code
        if let index = code.firstIndex(of: "#") {
            parameters = String(code.suffix(from: index))
            parameters.removeFirst()
        }

        return createJWT(code: parameters)
            .flatMap { jwt -> Single<Document> in
                self.verifyKey(code: parameters)
                    .andThen(self.validateClaims(jwt: jwt))
                    .andThen(self.parseToTicket(jwt: jwt, code: parameters))
            }
    }

    func fetchVerifier() -> Single<JWTVerifier> {
        fatalError("Needs override")
    }

    func verifyKey(code: String) -> Completable {
        return fetchVerifier()
            .flatMapCompletable { verifier in
                if !JWT<ConnfairTicketClaims>.verify(code, using: verifier) {
                    return Completable.error(JWTTicketParserError.verificationFailed)
                }
                return Completable.empty()
            }
    }

    func validateClaims(jwt: JWT<ClaimsType>) -> Completable {
        return Completable.from {
            guard jwt.validateClaims(now: self.timeProvider.now) == .success else {
                throw JWTTicketParserError.validationFailed
            }
        }
    }

    func parseToTicket(jwt: JWT<ClaimsType>, code: String) -> Single<Document> {
        fatalError("Needs override")
    }

}

enum JWTTicketParserError: LocalizedTitledError {

    case verificationFailed
    case parsingFailed
    case validationFailed
    case noKey

}

extension JWTTicketParserError {

    var localizedTitle: String {
        L10n.IOSApp.Navigation.Basic.error
    }

}
