import Foundation
import SwiftJWT
import RxSwift
import DependencyInjection

class ConnfairTicketParser: JWTTicketParser<ConnfairTicketClaims> {
    @InjectStatic(\.timeProvider) private var timeProvider

    #if PRODUCTION
    let keyResource = "ConnfairPubKeyProduction"
    #else
    let keyResource = "ConnfairPubKeyStaging"
    #endif

    func fetchKey() -> Single<Data> {
        Single.from {
            let pubKeyPath = Bundle.main.url(forResource: self.keyResource, withExtension: "pub")
            guard let keyPath = pubKeyPath else { throw JWTTicketParserError.noKey }

            let pubKey: Data = try Data(contentsOf: keyPath, options: .alwaysMapped)
            return pubKey
        }
    }

    override func fetchVerifier() -> Single<JWTVerifier> {
        fetchKey()
            .map { JWTVerifier.es256(publicKey: $0) }
    }

    override func parseToTicket(jwt: JWT<ConnfairTicketClaims>, code: String) -> Single<Document> {
        Single.from {
            ConnfairTicket(claims: jwt.claims, code: code)
        }
    }

}
