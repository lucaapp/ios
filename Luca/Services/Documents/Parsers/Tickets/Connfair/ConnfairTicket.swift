import Foundation
import RxSwift
import SwiftJWT

struct ConnfairTicket: Ticket {

    var event: String

    var location: String?

    var startTime: Date

    var endTime: Date

    var firstName: String

    var lastName: String

    var issuer: String

    var category: TicketCategory?

    var ticketId: String

    var verificationId: String?

    var identifier: Int {
        guard let payload = originalCode.data(using: .utf8) else {
            return -1
        }
        return Int(payload.crc32)
    }

    var originalCode: String

    var hashSeed: String { originalCode }

    var recentUntil: Date { expiresAt }

    var expiresAt: Date

    var issuedAt: Date

    var contentDescription: String { String() }

    var formattedName: String {
        firstName + " " + lastName
    }

    init(claims: ConnfairTicketClaims, code: String) {
        self.originalCode = code
        self.event = claims.e
        self.location = claims.l
        self.startTime = claims.st
        self.endTime = claims.et
        self.firstName = claims.fn
        self.lastName = claims.ln
        self.issuer = claims.iss
        self.category = claims.c
        self.ticketId = claims.sub
        self.verificationId = claims.v
        self.expiresAt = claims.exp ?? Date()
        self.issuedAt = claims.iat ?? Date()
    }
}
