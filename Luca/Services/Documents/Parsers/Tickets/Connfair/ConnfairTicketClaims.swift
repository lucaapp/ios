import Foundation
import SwiftJWT

struct ConnfairTicketClaims: Codable, Claims {

    var iss: String
    var st: Date
    var et: Date
    var sub: String
    var l: String?
    var e: String
    var c: TicketCategory?
    var nbf: Date?
    var exp: Date?
    var iat: Date?
    var fn: String
    var ln: String
    var v: String?

}

enum TicketCategory: Int, Codable {

    case music = 0
    case party = 1
    case classic = 2
    case culture = 3
    case exhibition = 4
    case conference = 5
    case movie = 6
    case family = 7
    case sport = 8
    case other = 9

}
