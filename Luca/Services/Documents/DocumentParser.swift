import Foundation
import RxSwift

protocol Document: Reportable {

    /// Unique identifier to differentiate between documents
    var identifier: Int { get }

    /// Serialized string
    var originalCode: String { get }

    /// Safe string that is used for hash generation. It's based on `originalCode` but stripped from all hash-irrelevant stuff to provide uniqueness.
    var hashSeed: String { get }

    /// Date until this document is marked as recent. Might be still valid afterwards
    var recentUntil: Date { get }

    /// Date until this document is valid
    var expiresAt: Date { get }

    /// some documents are presented after expiration and need to be removed later on
    var removedAt: Date { get }

    /// issue date
    var issuedAt: Date { get }

    /// deletion message
    var deletionConfirmationMessage: String { get }
}

protocol DocumentParser {
    func parse(code: String) -> Single<Document>
}

extension Document {

    var removedAt: Date {
        return expiresAt
    }

    /// Document needs to be removed from device
    var needsRemoval: Bool {
        Date.now >= removedAt
    }

    /// Checks if this document is still valid
    var isValid: Bool {
        Date.now < expiresAt
    }

    var isRecent: Bool {
        Date.now < recentUntil
    }

    /// default document deletion confirmation message
    var deletionConfirmationMessage: String {
        L10n.IOSApp.Test.Delete.description
    }
}

/// Used to define all document specific properties
class DocumentProperties {}
