import Foundation
import UIKit
import PhoneNumberKit
import RxSwift
import DependencyInjection

struct PhoneNumberVerificationRequest: Codable, Equatable {
    var challengeId: String
    var date: Date
    var phoneNumber: String
    var verified: Bool = false
}

public class PhoneNumberVerificationService {
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.backendSMSV3) private var backend
    @InjectStatic(\.timeProvider) private var timeProvider

    private let parentViewController: UIViewController

    private let phoneNumberKit = PhoneNumberKit()

    private let timeoutBase = 30.0

    init(presenting viewController: UIViewController) {
        self.parentViewController = viewController
    }

    /*
     1. Check if there is no timer and show it if so
     2. show the confirmation screen
     3. show the TAN input screen
     */
    func verify(phoneNumber: String) -> Completable {
        // QA Builds have no phone verification. From now on, anything will be accepted as a valid phone number.
        #if QA
        return lucaPreferences
            .set(\.phoneNumberVerified, value: true)
        #endif

        let requestNewTan = parseNumber(phoneNumber)
            .asObservable()
            .flatMap { parsedNumber -> Single<PhoneNumber> in
                self.confirmPhoneNumber(phoneNumber: parsedNumber)
            }
            .flatMap { self.requestNewTAN(parsedNumber: $0).ifEmpty(switchTo: Maybe.error(PhoneNumberVerificationServiceError.verificationFailed)) }

        return handleRequestDelay()
            .asObservable()
            .flatMap { timeoutActive -> Observable<String> in
                if !timeoutActive {
                    return requestNewTan
                }
                return Observable.just("")
            }
            .asSingle()
            .flatMap { _ in self.retrieveOpenChallenges() }
            .flatMapCompletable { challenges in
                self.handlePhoneNumberVC(challenges: challenges)
                    .observe(on: MainScheduler.instance)
                    .andThen(self.lucaPreferences.set(\.phoneNumberVerified, value: true))
            }
            .observe(on: MainScheduler.instance)
            .debug("verify sms")
    }

    private func retrieveOpenChallenges() -> Single<[String]> {
        lucaPreferences.get(\.verificationRequests).map { requests in
            let refDate = self.timeProvider.now.addingTimeInterval(-24 * 60 * 60)
            let retVal = requests
                .filter { !$0.verified }
                .filter { $0.date > refDate }
                .sorted { $0.date > $1.date }
                .prefix(10)

            return retVal.map { $0.challengeId }
        }
    }
    /// Emits the challenge that has been verified
    private func handlePhoneNumberVC(challenges: [String]) -> Completable {
        Completable.create { (observer) -> Disposable in

            let phoneNumberVC = ViewControllerFactory.Alert.createPhoneNumberVerificationViewController(challengeIDs: challenges)
            phoneNumberVC.modalTransitionStyle = .crossDissolve
            phoneNumberVC.modalPresentationStyle = .overCurrentContext
            phoneNumberVC.onSuccess = { challenge in
                _ = self.lucaPreferences.get(\.verificationRequests)
                    .flatMapCompletable { verificationRequests in
                        var requests = verificationRequests
                        if var request = requests.first(where: { $0.challengeId == challenge }) {
                            request.verified = true
                            requests.removeAll(where: { $0.challengeId == challenge })
                            requests.append(request)
                            requests.sort { $0.date > $1.date }
                            return self.lucaPreferences.set(\.verificationRequests, value: requests)
                        }
                        return Completable.empty()
                    }
                    .do(onError: { observer(.error($0)) })
                    .do(onCompleted: {
                        observer(.completed)
                    })
                    .do(onDispose: {
                        phoneNumberVC.onSuccess = nil
                        phoneNumberVC.onUserCanceled = nil
                    })
                    .subscribe()
            }
            phoneNumberVC.onUserCanceled = { observer(.error(PhoneNumberVerificationServiceError.verificationCancelled)) }
            self.parentViewController.present(phoneNumberVC, animated: true, completion: nil)

            return Disposables.create {
                phoneNumberVC.onSuccess = nil
                phoneNumberVC.onUserCanceled = nil
                DispatchQueue.main.async { phoneNumberVC.dismiss(animated: true, completion: nil) }
            }
        }
        .subscribe(on: MainScheduler.instance)
    }

    func confirmPhoneNumber(phoneNumber: PhoneNumber) -> Single<PhoneNumber> {
        Single.create { observer -> Disposable in
            let viewController = ViewControllerFactory.Alert.createPhoneNumberConfirmationViewController(phoneNumber: phoneNumber)
            viewController.modalTransitionStyle = .crossDissolve
            viewController.modalPresentationStyle = .overFullScreen
            viewController.onSuccess = { observer(.success(phoneNumber)) }
            viewController.onCancel = { observer(.failure(PhoneNumberVerificationServiceError.verificationUnconfirmed)) }
            self.parentViewController.present(viewController, animated: true, completion: nil)
            return Disposables.create {
                DispatchQueue.main.async { viewController.dismiss(animated: true, completion: nil) }
            }
        }
        .subscribe(on: MainScheduler.asyncInstance)
    }

    private func parseNumber(_ number: String) -> Single<PhoneNumber> {
        Single.from {
            let parsedNumber = try self.phoneNumberKit.parse(number)
            return parsedNumber
        }
        .catch { _ in
            return Single.error(LocalizedTitledErrorValue(localizedTitle: L10n.IOSApp.Navigation.Basic.error, errorDescription: L10n.IOSApp.Verification.PhoneNumber.wrongFormat))
        }
    }

    /// Emits `true` if timeout is active
    private func handleRequestDelay() -> Single<Bool> {
        getNextAllowedRequestTimestamp()
        .filter { self.timeProvider.now.timeIntervalSince1970 < $0 }
        .flatMap { nextAllowedTimestamp -> Maybe<Bool> in

            // If there is next allowed timestamp and it is in future, show the timer
            ViewControllerFactory.Alert.createAlertViewControllerRx(
                presentingViewController: self.parentViewController,
                title: L10n.IOSApp.Verification.PhoneNumber.TimerDelay.title,
                message: "",
                firstButtonTitle: L10n.IOSApp.Navigation.Basic.ok.uppercased())
                .subscribe(on: MainScheduler.asyncInstance)
                .materialize()
                .flatMapLatest { event -> Single<Void> in
                    if let alert = event.element {
                        return Observable<Int>.timer(.seconds(0), period: .seconds(1), scheduler: MainScheduler.instance)
                            .take(until: { _ in self.timeProvider.now.timeIntervalSince1970 >= nextAllowedTimestamp }, behavior: .inclusive)
                            .do(onNext: { _ in
                                let totalSeconds = Int(abs(nextAllowedTimestamp - self.timeProvider.now.timeIntervalSince1970))
                                let minutes = totalSeconds / 60
                                let seconds = totalSeconds - (minutes * 60)
                                alert.message = L10n.IOSApp.Verification.PhoneNumber.TimerDelay.message(String(format: "%02i:%02i", minutes, seconds))
                                alert.messageLabel.accessibilityLabel = L10n.IOSApp.Verification.PhoneNumber.TimerDelay.Message.accessibility(minutes, seconds)
                            })
                            .ignoreElementsAsCompletable()
                            .andThen(Single.just(Void()))
                    } else if let error = event.error {
                        throw error // Just push back any errors
                    }
                    return Single.just(Void())
                }
                .take(1)
                .ignoreElementsAsCompletable()
                .andThen(.from { self.timeProvider.now.timeIntervalSince1970 < nextAllowedTimestamp })
        }
        .ifEmpty(default: false)
    }

    /// Requests new TAN and emits a challenge if everything went well. It handles all internal errors by itself, so it won't emit any errors.
    private func requestNewTAN(parsedNumber: PhoneNumber) -> Maybe<String> {
        let formattedNumber = phoneNumberKit.format(parsedNumber, toType: .e164)
        return backend.requestChallenge(phoneNumber: formattedNumber)
            .asSingle()
            .debug("Challenge request info")
            .map { $0.challenge }
            .asMaybe()
            .flatMap { challenge in
                self.lucaPreferences.get(\.verificationRequests)
                    .map { requests in
                        var mutableRequests = requests
                        mutableRequests.append(PhoneNumberVerificationRequest(
                            challengeId: challenge,
                            date: self.timeProvider.now,
                            phoneNumber: formattedNumber))
                        return mutableRequests
                    }
                    .flatMapCompletable {
                        self.lucaPreferences.set(\.verificationRequests, value: $0)

                    }
                    .andThen(Maybe.just(challenge))
            }
            .observe(on: MainScheduler.instance)
            .catch { error in
                var alert: UIAlertController
                if let localizedTitledError = error as? LocalizedTitledError {
                    alert = UIAlertController.infoAlert(title: localizedTitledError.localizedTitle, message: localizedTitledError.localizedDescription)
                } else {
                    alert = UIAlertController.infoAlert(title: L10n.IOSApp.Navigation.Basic.error, message: L10n.IOSApp.Verification.PhoneNumber.requestFailure)
                }
                self.parentViewController.present(alert, animated: true, completion: nil)
                return Maybe.empty()
            }
    }

    private func getNextAllowedRequestTimestamp() -> Maybe<Double> {
        lucaPreferences.get(\.verificationRequests)
            .asMaybe()
            .flatMap { verificationRequests -> Maybe<Double> in
                let timeframe = self.timeoutBase*pow(2, 4)
                let timeframeBegin = self.timeProvider.now.timeIntervalSince1970 - timeframe
                let requestsInTimeoutTimeframe = verificationRequests

                    // Take only entries from current day
                    .filter { self.timeProvider.now.timeIntervalSince1970 - $0.date.timeIntervalSince1970 < 24.0 * 60.0 * 60.0 }

                    .filter { $0.date.timeIntervalSince1970 > timeframeBegin }

                let sorted = requestsInTimeoutTimeframe.sorted(by: { $0.date > $1.date })

                // If there are some requests sent in given timeframe
                if let last = sorted.first {
                    let nextTimeframe = self.timeoutBase * pow(2, Double(requestsInTimeoutTimeframe.count - 1))
                    return .just(last.date.timeIntervalSince1970 + nextTimeframe)
                }

                // There are no requests in given timeframe
                return .empty()
            }
    }

}

enum PhoneNumberVerificationServiceError: LocalizedTitledError {

    case verificationFailed
    case verificationCancelled
    case verificationUnconfirmed

}
extension PhoneNumberVerificationServiceError {

    var errorDescription: String? {
        switch self {
        case .verificationFailed: return L10n.IOSApp.FormViewController.PhoneVerificationFailure.message
        case .verificationCancelled: return L10n.IOSApp.FormViewController.VerificationCancelled.message
        case .verificationUnconfirmed: return L10n.IOSApp.FormViewController.VerificationUnconfirmed.message
        }
    }

    var localizedTitle: String {
        return L10n.IOSApp.FormViewController.PhoneVerificationFailure.title
    }

}
