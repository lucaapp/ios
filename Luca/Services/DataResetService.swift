import Foundation
import DependencyInjection
import RxSwift

class DataResetService {

    let log = GeneralPurposeLog(subsystem: "App", category: "DataResetService", subDomains: [])
    @InjectAllDynamic private var realmDatabaseUtils: [RealmDatabaseUtils]
    @InjectStatic(\.appServicesInitialiser) private var appServicesInitialiser
    @InjectStatic(\.localDBKeyRepository) private var localDBKeyRepository
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.userKeysBundle) private var userKeysBundle
    @InjectStatic(\.historyRepo) private var historyRepo
    @InjectStatic(\.userService) private var userService
    @InjectStatic(\.traceIdService) private var traceIdService
    @InjectStatic(\.attestationService) private var attestationService
    @InjectStatic(\.lucaIDService) private var lucaIDService
    @InjectStatic(\.lucaConnectContactDataService) private var lucaConnectContactDataService
    @InjectStatic(\.documentRepoService) private var documentRepoService
    @InjectStatic(\.documentUniquenessChecker) private var documentUniquenessChecker
    @InjectStatic(\.lucaPayment) private var lucaPayment

    func resetAll() -> Completable {
        deleteLucaID()
            .andThen(deactivateLucaPay())
            .andThen(deleteLucaConnect())
            .andThen(deleteDocuments())
            .andThen(userService.deleteUserData().debug("Delete user data"))
            .andThen(disableAllServices())
            .andThen(traceIdService.disposeData(clearTraceHistory: true))
            .andThen(resetUserData())
            .andThen(invalidateServices())
            .andThen(Completable.zip(self.realmDatabaseUtils.map { $0.removeFile() }))
            .andThen(Completable.from {
                UserDataPreferences.removeAllSuites()
            })
            .andThen(reinitializeServices())
    }

    private func deactivateLucaPay() -> Completable {
        lucaPayment.isUserActivated
            .flatMapCompletable({ $0 ? self.lucaPayment.remove() : .empty() })
    }

    private func invalidateServices() -> Completable {
        Completable.from { self.appServicesInitialiser.invalidate() }
    }

    private func disableAllServices() -> Completable {
        Completable.from { self.appServicesInitialiser.disable() }
    }

    private func reinitializeServices() -> Completable {
        Completable.from { try self.appServicesInitialiser.initialise() }
        .logError(log, "Error in invalidating and reinitialisation of services")
    }

    private func resetUserData() -> Completable {
        Completable.from {
            self.userKeysBundle.removeKeys()
            self.localDBKeyRepository.purge()
        }
        .andThen(Completable.from { try KeyStorage.purge() }.logError(log, "Error in purging key storage"))
    }

    private func deleteLucaID() -> Completable {
        lucaIDService
            .archiveData()
            .andThen(attestationService.resetIdentifiers())
    }

    private func deleteLucaConnect() -> Completable {
        lucaConnectContactDataService.disableFeature()
    }

    private func deleteDocuments() -> Completable {
        documentRepoService
            .currentAndNewTests
            .take(1)
            .asSingle()
            .flatMapCompletable { [weak self] docs in
                guard let self = self else {return .empty()}
                return self.lucaConnectContactDataService.disableFeature()
                    .andThen(self.documentUniquenessChecker.release(documents: docs))
            }
    }

    func resetHistory() -> Completable {
        historyRepo.removeAll()
    }

}
