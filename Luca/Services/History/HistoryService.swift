import Foundation
import RxSwift
import DependencyInjection

// Do not rename the properties, it won't be deserializable than.
// And if you have to, define the old values as strings to preserve the serialization.
// Or implement proper migration
enum HistoryEntryType: String, Codable {
    case checkIn
    case checkOut
    case userDataUpdate
    case userDataTransfer
}

struct HistoryEntry: Codable {
    var date: Date
    var type: HistoryEntryType

    // Optional value for check in and check out events
    var location: Location?
    var role: Role?
    var guestlist: [String]?

    // Optional value for data shared events
    var numberOfDaysShared: Int?

    // Optional value for check ins as guest
    var traceInfo: TraceInfo?

    /// Produces a history event that correspondets to a check in as guest
    init(date: Date, type: HistoryEntryType, location: Location?, traceInfo: TraceInfo?) {
        self.date = date
        self.type = type
        self.location = location
        self.role = .guest
        self.traceInfo = traceInfo
    }

    /// Produces a history event that correspondents to a private meetin as host
    init(date: Date, type: HistoryEntryType, location: Location?, guestlist: [String]?) {
        self.date = date
        self.type = type
        self.location = location
        self.guestlist = guestlist
        self.role = .host
    }

    /// Produces a history event for history share
    init(date: Date, type: HistoryEntryType, numberOfDaysShared: Int?) {
        self.date = date
        self.type = type
        self.numberOfDaysShared = numberOfDaysShared
    }
}

extension HistoryEntry: DataRepoModel {
    var identifier: Int? {
        get {
            var checksum = Data()

            checksum.append(date.timeIntervalSince1970.data)
            checksum.append(type.rawValue.data(using: .utf8)!)

            return Int(checksum.crc32)
        }
        set {

        }
    }
}

protocol HistoryEvent: Reportable {

    var date: Date { get }
}

class UserEvent: HistoryEvent {

    var date: Date
    var checkin: HistoryEntry
    var checkout: HistoryEntry?

    var contentDescription: String {
        self.createDescription()
    }

    init(checkin: HistoryEntry) {
        self.checkin = checkin
        self.date = checkin.date
    }

    init(checkin: HistoryEntry, checkout: HistoryEntry) {
        self.checkin = checkin
        self.checkout = checkout
        self.date = checkin.date
    }

    /// Returns date as string, and date as accessibility string
    func formattingCheckinCheckoutDate() -> (String, String) {
        if let checkout = checkout {
            // Backend automaticaly checks out users after 24h.
            // Since we can't detect a checkout without starting the app, this visual fix is neccessary
            let maxCheckoutDate = Calendar.current.date(byAdding: .hour, value: 24, to: checkin.date) ?? Date.now

            let validDate = checkout.date < maxCheckoutDate ? checkout.date : maxCheckoutDate
						let formatter = DateComponentsFormatter()
						formatter.unitsStyle = .abbreviated
						formatter.allowedUnits = [.hour, .minute]

					if let durationString = formatter.string(from: checkin.date, to: validDate) {
                        let date = "\(checkin.date.formattedDateTime)\n\(durationString)"
                        let accessibilityDate = "\(checkin.date.accessibilityDate)\n\(durationString)"
						return (date, accessibilityDate)
					}
        }

        return (checkin.date.formattedDateTime, checkin.date.accessibilityDate)
    }

    func createDescription() -> String {
        var duration = L10n.IOSApp.DataReport.ContactData.CheckIn.currentlyCheckedIn
        if let checkout = checkout {
            duration = checkin.date.durationUntil(date: checkout.date)
        }

        switch checkin.role {
        case .guest:
            if checkin.location?.isPrivate ?? false {
                return L10n.IOSApp.DataReport.ContactData.privateMeetingCheckIn(
                    checkin.location?.formattedName ?? "Unknown location",
                    checkin.date.formattedDateTime,
                    duration
                )
            } else {
                return L10n.IOSApp.DataReport.ContactData.checkIn(
                    checkin.date.formattedDateTime,
                    duration,
                    checkin.location?.formattedName ?? "Unknown location"
                )
            }
        default:
            return L10n.IOSApp.DataReport.ContactData.privateMeetingHost(
                checkin.guestlist?.joined(separator: ", ") ?? "-",
                checkin.date.formattedDateTime,
                duration
            )
        }
    }
}

class UserDataTransfer: HistoryEvent {

    var date: Date
    var entry: HistoryEntry

    var contentDescription: String {
        return L10n.IOSApp.DataReport.ContactData.userDataTransfer(date.formattedDateTime, entry.numberOfDaysShared ?? 14)
    }

    init(entry: HistoryEntry) {
        self.entry = entry
        self.date = entry.date
    }
}

class UserDataUpdate: HistoryEvent {

    var date: Date
    var entry: HistoryEntry

    var contentDescription: String {
        "UserDataUpdate"
    }

    init(entry: HistoryEntry) {
        self.entry = entry
        self.date = entry.date
    }
}

enum Role: String, Codable {

    case guest
    case host

}

class HistoryService {
    @InjectStatic(\.historyRepo) private var historyRepo
    @InjectStatic(\.traceInfoRepo) private var traceInfoRepo
    @InjectStatic(\.timeProvider) private var timeProvider

    public let onEventAdded = "HistoryService.onEventAdded"

    private var entriesBuffer: [HistoryEntry]?

    var historyEvents: Single<[HistoryEvent]> {
        historyRepo.restore().map { entries in
            var historyEntries = entries.sorted(by: { $0.date < $1.date })

            let firstCheckin = historyEntries.firstIndex(where: { $0.type == .checkIn })
            let firstCheckout = historyEntries.firstIndex(where: { $0.type == .checkOut })

            // If first entry is a checkout, then history was cleared while checked in.
            if let firstOut = firstCheckout, let firstIn = firstCheckin, firstOut < firstIn {
                historyEntries.remove(at: firstOut)
            }

            let userDataTransfers = historyEntries.filter { $0.type == .userDataTransfer }.map { UserDataTransfer(entry: $0) }
            let checkins = historyEntries.filter { $0.type == .checkIn }
            var checkouts = historyEntries.filter { $0.type == .checkOut }
            var checkoutsSorted: [HistoryEntry] = []
            checkins.forEach { entry in
                if let index = checkouts.firstIndex(where: { $0.location != nil && $0.location == entry.location }) {
                    checkoutsSorted.append(checkouts[index])
                    checkouts.remove(at: index)
                }
            }

            // Zip together checkins that have a corresponding checkout
            let events = zip(checkins, checkoutsSorted)
            var userEvents = events.map { UserEvent(checkin: $0.0, checkout: $0.1) }

            // There can only be one leftover checkin (if any), as a user cannot check into events simultaneously
            if checkoutsSorted.count != checkins.count, let lastCheckin = checkins.last {
                userEvents.append(UserEvent(checkin: lastCheckin))
            }

            var historyUserEvents = userEvents as [HistoryEvent]
            let historyUserDataTransfers = userDataTransfers as [HistoryEvent]
            historyUserEvents.append(contentsOf: historyUserDataTransfers)

            return historyUserEvents.sorted(by: { $0.date < $1.date })
        }
    }

    func removeOldEntries() -> Completable {
        historyRepo.restore()
            .map { restored -> [HistoryEntry] in

                let currentDate = self.timeProvider.now
                guard let last28Days = Calendar.current.date(byAdding: .day, value: -28, to: currentDate) else {
                    return []
                }
                return restored.filter { $0.date < last28Days }
            }
            .flatMap { [weak self] historyEntries -> Single<[HistoryEntry]> in
                guard let self = self else { return .just([]) }
                let traceInfos = historyEntries.compactMap { $0.traceInfo?.identifier }
                return self.traceInfoRepo.remove(identifiers: traceInfos)
                    .andThen(.just(historyEntries))
            }
            .map { array in array.compactMap { $0.identifier } }
            .asObservable()
            .filter { !$0.isEmpty }
            .flatMap(self.historyRepo.remove)
            .asCompletable()
    }

    func add(entry: HistoryEntry) -> Completable {
        historyRepo.store(object: entry)
            .do(onSuccess: { stored in
                NotificationCenter.default.post(
                    Notification(
                        name: Notification.Name(self.onEventAdded),
                        object: self,
                        userInfo: ["last": stored]
                    )
                )
            })
            .asObservable()
            .ignoreElementsAsCompletable()
    }
}

extension HistoryService {
    var onEventAddedRx: Observable<HistoryEntry> {
        NotificationCenter.default.rx.notification(NSNotification.Name(self.onEventAdded), object: self)
            .map { $0.userInfo }
            .unwrapOptional()
            .map { $0["last"] as? HistoryEntry }
            .unwrapOptional()
    }
}

extension HistoryService: UnsafeAddress, LogUtil {}
