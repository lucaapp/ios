import Foundation
import RxSwift
import DependencyInjection

class HistoryEventListener: Toggleable {
    @InjectStatic(\.history) private var historyService: HistoryService
    @InjectStatic(\.traceIdService) private var traceIdService: TraceIdService
    @InjectStatic(\.userService) private var userService: UserService
    @InjectStatic(\.privateMeetingService) private var privateMeetingService: PrivateMeetingService
    @InjectStatic(\.historyRepo) private var historyRepo: DataRepo<HistoryEntry>
    @InjectStatic(\.traceInfoRepo) private var traceInfoRepo: DataRepo<TraceInfo>
    @InjectStatic(\.locationRepo) private var locationRepo: DataRepo<Location>
    @InjectStatic(\.timeProvider) private var timeProvider: TimeProvider

    private var disposeBag: DisposeBag?
    var isEnabled: Bool { disposeBag != nil }

    /// Enables listening
    func enable() {
        let newDisposeBag = DisposeBag()
        Completable.zip(onUserUpdated,
                        onUserDataTransfered,
                        onCheckIn,
                        onCheckOut,
                        onMeetingCreated,
                        onMeetingClosed,
                        restoreHistory)
            .logError(self)
            .retry(delay: .milliseconds(100), scheduler: LucaScheduling.backgroundScheduler)
            .subscribe()
            .disposed(by: newDisposeBag)
        disposeBag = newDisposeBag
    }

    /// Disables listening
    func disable() {
        disposeBag = nil
    }

    private var onUserUpdated: Completable {
        return userService.onUserUpdatedRx.flatMap { _ in self.add(.userDataUpdate) }.ignoreElementsAsCompletable()
    }

    private var onUserDataTransfered: Completable {
        return userService.onUserDataTransferedRx
            .flatMap { self.add(.userDataTransfer, numberOfDaysShared: $0) }
            .ignoreElementsAsCompletable()
    }

    private var onCheckIn: Completable {
        return traceIdService.onCheckInRx()
            .flatMap {
                self.fetchCurrentLocation()
                    .asObservable()
                    .ignoreElementsAsCompletable()
                    .andThen(Single.just($0))
            }
            .flatMap { self.add(.checkIn, traceInfo: $0) }
            .ignoreElementsAsCompletable()
    }

    private var onCheckOut: Completable {
        return traceIdService.onCheckOutRx()
            .flatMap { self.add(.checkOut, traceInfo: $0) }
            .ignoreElementsAsCompletable()
    }

    private var onMeetingCreated: Completable {
        return privateMeetingService
            .onMeetingCreatedRx
            .flatMap { meeting -> Completable in
                let guestList = self.guestLists(from: meeting)
                let namesList = guestList.map { "\($0.fn) \($0.ln)" }
                return self.add(.checkIn, guestlist: namesList)
            }
            .ignoreElementsAsCompletable()
    }

    private var onMeetingClosed: Completable {
        return privateMeetingService
            .onMeetingClosedRx
            .flatMap { meeting -> Completable in
                let guestList = self.guestLists(from: meeting)
                let namesList = guestList.map { "\($0.fn) \($0.ln)" }
                return self.add(.checkOut, guestlist: namesList)
            }
            .ignoreElementsAsCompletable()
    }

    private var restoreHistory: Completable {
        return isHistoryCorrupted()
            .flatMapCompletable { isCorrupted in
                if isCorrupted {
                    return self.saveLocationsFromMeetings().andThen(self.recreateHistory())
                }
                return Completable.empty()
            }
    }

    private func isHistoryCorrupted() -> Single<Bool> {
        let checkInsWithoutTraceInfo = historyRepo.restore()
            .map { events in events.filter { $0.guestlist == nil } }                            // Ignore private meeting events
            .map { events in events.filter { $0.type == .checkIn || $0.type == .checkOut } }    // Take checkIn and checkOut events
            .map { events in events.filter { $0.traceInfo == nil } }                            // Take events without traceInfo
            .map { !$0.isEmpty }                                                                // It should be empty

        let areCheckInsBalanced = historyRepo.restore()
            .map { events in events.filter { $0.type == .checkIn || $0.type == .checkOut } }
            .map { events -> Bool in
                let checkIns = events.filter { $0.type == .checkIn }.count
                let checkOuts = events.filter { $0.type == .checkOut }.count

                // There can be only one checkOut left over (when user is checked in)
                return checkIns == checkOuts || checkIns == checkOuts + 1
            }

        return Single.zip(checkInsWithoutTraceInfo, areCheckInsBalanced)
            .map { (checkInsWithoutTraceInfo, areCheckInsBalanced) in

                // If there are any checkIns without traceInfos or the checkIns and checkOuts are unbalanced
                return checkInsWithoutTraceInfo || !areCheckInsBalanced
            }
            .debug("is history corrupted")
    }

    /// It's needed when recreating history. Old locations (prior to 1.6.3) haven't been saved with a name to the repo for private meetings
    private func saveLocationsFromMeetings() -> Completable {
        historyRepo.restore()
            .map { events in events.map { $0.location }.filter { $0 != nil }.map { $0! } }                              // Filter all non-nil locations
            .map { locations in locations.filter { $0.formattedName != nil } }  // Take only locations that have some name
            .flatMap(locationRepo.store)
            .asCompletable()
    }

    private func recreateHistory() -> Completable {
        historyRepo.restore()
            .map { events in events.filter { $0.guestlist == nil } }                            // Ignore meetings as host
            .map { events in events.filter { $0.type == .checkIn || $0.type == .checkOut } }    // Take only checkIns and checkOuts
            .map { events in events.map { $0.identifier ?? 0 } }
            .flatMapCompletable(historyRepo.remove)
            .andThen(traceInfoRepo.restore())
            .asObservable()
            .flatMap { Observable.from($0) }
            .flatMap { traceInfo -> Completable in
                if traceInfo.checkOutDate == nil {
                    return self.add(.checkIn, traceInfo: traceInfo)
                }
                return self.add(.checkIn, traceInfo: traceInfo).andThen(self.add(.checkOut, traceInfo: traceInfo))
            }
            .ignoreElementsAsCompletable()
    }

    private func guestLists(from meeting: PrivateMeetingData) -> [PrivateMeetingQRCodeV3AdditionalData] {
        var guestList: [PrivateMeetingQRCodeV3AdditionalData] = []
        let guestsData = meeting.guests
            .filter { $0.data != nil }

        for guest in guestsData {
            if let data = try? self.privateMeetingService.decrypt(guestData: guest, meetingKeyIndex: meeting.keyIndex) {
                guestList.append(data)
            }
        }
        return guestList
    }

    private func fetchCurrentLocation() -> Single<Location> {
        self.traceIdService.fetchCurrentLocationInfo()
    }

    private func add(_ type: HistoryEntryType, numberOfDaysShared: Int) -> Completable {
        self.historyService.add(entry: HistoryEntry(date: timeProvider.now, type: type, numberOfDaysShared: numberOfDaysShared))
    }

    private func add(_ type: HistoryEntryType) -> Completable {
        self.historyService.add(entry: HistoryEntry(date: timeProvider.now, type: type, location: nil, traceInfo: nil))
    }

    private func add(_ type: HistoryEntryType, traceInfo: TraceInfo) -> Completable {
        Single<Date?>.from {
            if type == .checkIn {
                return traceInfo.checkInDate
            }
            return traceInfo.checkOutDate
        }
        .asObservable()
        .unwrapOptional()
        .flatMap { date in
            self.locationRepo.restore()
                .asObservable()
                .map { locations -> Location? in
                    let location = locations.first(where: { $0.locationId == traceInfo.locationId })

                    return location
                }
                .unwrapOptional()
                .flatMap { self.historyService.add(entry: HistoryEntry(date: date, type: type, location: $0, traceInfo: traceInfo)) }
        }
        .ignoreElementsAsCompletable()
    }

    private func add(_ type: HistoryEntryType, location: Location? = nil, guestlist: [String]?) -> Completable {
        self.historyService.add(entry: HistoryEntry(date: timeProvider.now, type: type, location: location, guestlist: guestlist))
    }

}

extension HistoryEventListener: LogUtil, UnsafeAddress {}
