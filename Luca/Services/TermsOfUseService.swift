import Foundation
import DependencyInjection
import RxSwift

class TermsOfUseService<T> where T: Codable & Equatable {
    @InjectStatic(\.keyValueRepo) private var keyValueRepo
    @InjectStatic(\.timeProvider) private var timeProvider

    private struct CurrentState: Codable {
        var accepted: Bool
        var versionAtTheTime: T
        var date: Date
    }

    private let serviceKey: String
    private let newestVersion: T
    private let onChangeStatus = PublishSubject<Bool?>()
    private var fullKey: String {
        "\(serviceKey).version"
    }

    /// - parameter serviceKey: a string to differentiate between various terms of services
    /// - parameter newestVersion: an identifier to determine last version, when the ToS have changed
    init(serviceKey: String, newestVersion: T) {
        self.serviceKey = serviceKey
        self.newestVersion = newestVersion
    }

    /// Emits current state and every subsequent change.
    /// `nil` value means that user has not accept nor denied it.
    var isAcceptedChanges: Observable<Bool?> {
        onChangeStatus.asObservable() + isAccepted.asObservable()
    }

    /// Emits current state.
    /// `nil` value means that user has not accept nor denied it.
    var isAccepted: Single<Bool?> {
        keyValueRepo
            .loadOptional(self.fullKey)
            .catchAndReturn(nil)
            .map { (currentValue: CurrentState?) -> Bool? in
                if let value = currentValue {
                    return self.newestVersion == value.versionAtTheTime && value.accepted
                }
                return nil
            }
    }

    /// Sets current version as accepted
    func setAsAccepted() -> Completable {
        Single.from { self.timeProvider.now }
        .flatMapCompletable {
            self.keyValueRepo.store(self.fullKey, value: CurrentState(
                accepted: true,
                versionAtTheTime: self.newestVersion,
                date: $0)
            )
            .do(onCompleted: { self.onChangeStatus.onNext(true) })
        }
    }

    /// Sets current version as denied
    func setAsDenied() -> Completable {
        Single.from { self.timeProvider.now }
        .flatMapCompletable {
            self.keyValueRepo.store(self.fullKey, value: CurrentState(
                accepted: false,
                versionAtTheTime: self.newestVersion,
                date: $0)
            )
            .do(onCompleted: { self.onChangeStatus.onNext(false) })
        }
    }
}
