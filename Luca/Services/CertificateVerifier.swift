import SwiftJWT
import RxSwift

protocol CertificateVerifier {
    var rootCertificateSource: Single<String> { get }
    var intermediateCertificatesSource: Single<[String]> { get }
}

extension CertificateVerifier {
    func verifyCertificates(with pemData: String) -> Completable {
        let root = rootCertificateSource
            .flatMap { self.loadCertificate(from: $0) }

        let intermediate = self.intermediateCertificatesSource
            .asObservable()
            .flatMap { Observable.from($0) }
            .flatMap { self.loadCertificate(from: $0) }
            .toArray()

        let leaf = Single.from { pemData }
            .flatMap { self.loadCertificate(from: $0) }

        return Single.zip(root, intermediate, leaf)
            .flatMap {
                self.checkCertificateChain(root: $0, intermediateCertificates: $1, leafCertificate: $2)
                    .do(onSuccess: {
                        if !$0 {
                            throw DailyKeyVerifierError.certificateChainNotTrusted
                        }
                    })
            }
            .asCompletable()
    }

    /// Checks if `certificates` belong to the same chain with the `root`
    func checkCertificateChain(root: SecCertificate, intermediateCertificates: [SecCertificate], leafCertificate: SecCertificate) -> Single<Bool> {
        let policy = SecPolicyCreateBasicX509()
        var trust: SecTrust?
        var status = SecTrustCreateWithCertificates(([leafCertificate] + intermediateCertificates) as CFArray, [policy] as CFArray, &trust)
        if status != errSecSuccess {
            return .just(false)
        }
        var error: CFError?
        if let trust = trust {
            status = SecTrustSetAnchorCertificates(trust, [root] as CFArray)
            if status != errSecSuccess {
                return .just(false)
            }
            return Single.create { observer in

                let result = SecTrustEvaluateWithError(trust, &error)

                if let error = error {
                    // This error indicates that the chain is not complete. Some node is missing here thus this setup shouldn't be trusted
                    if CFErrorGetCode(error) == -25318 {
                        observer(.success(false))
                    } else {
                        observer(.failure(error))
                    }
                } else {
                    observer(.success(result))
                }
                return Disposables.create()
            }
            .subscribe(on: ConcurrentDispatchQueueScheduler(qos: .background))
        }
        return .just(false)
    }

    /// Converts PEM certificate to DER format
    func PEMCertificateToDER(pem: String) -> Data? {
        let cut = pem.removeOccurences(of: ["\n", "-----BEGIN CERTIFICATE-----", "-----END CERTIFICATE-----"])
        return Data(base64Encoded: cut, options: .ignoreUnknownCharacters)
    }

    /// Computes the fingerprint of DER Certificate
    func fingerprint(of der: Data) -> Data {
        der.sha1()
    }

    /// Loads `SecCertificate` from PEM data
    func loadCertificate(from pemData: String) -> Single<SecCertificate> {
        Single.from { pemData }
        .compactMap { self.PEMCertificateToDER(pem: $0) }
        .asObservable()
        .map {
            guard let cert = SecCertificateCreateWithData(kCFAllocatorDefault, $0 as CFData) else {
                throw DailyKeyVerifierError.certificateNotValid
            }
            return cert
        }
        .asSingle()
    }

    /// Converts public key in form `04 | X | Y` to PEM String
    func ecPublicKeyToPEM(publicKey: Data) -> String {
        let PEM_PUBLIC_PREFIX = Data(hex: "3059301306072A8648CE3D020106082A8648CE3D030107034200")
        return "-----BEGIN PUBLIC KEY-----\n\((PEM_PUBLIC_PREFIX + publicKey).base64EncodedString())\n-----END PUBLIC KEY-----"
    }
}
