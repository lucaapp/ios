import RxSwift
import CoreLocation
import RxAppState
import DependencyInjection

class SingleLocationRequestRegionDetector: RegionDetector {
    @InjectStatic(\.locationUpdater) private var locationUpdater: LocationUpdater
    @InjectStatic(\.timeProvider) private var timeProvider: TimeProvider

    var allowedAppStates: [AppState]

    init(allowedAppStates: [AppState]) {
        self.allowedAppStates = allowedAppStates
    }

    func isInsideRegion(center: CLLocationCoordinate2D, radius: CLLocationDistance) -> Observable<Bool> {
        UIApplication.shared.rx.currentAndChangedAppState
            .flatMapLatest { appState -> Observable<Bool> in
                if self.allowedAppStates.contains(appState) {
                    return self.performCheck(center: center, radius: radius)
                }
                return Observable.empty()
            }
    }

    private func performCheck(center: CLLocationCoordinate2D, radius: CLLocationDistance) -> Observable<Bool> {
        locationUpdater.locationChanges
            .do(onSubscribe: { self.locationUpdater.requestCurrentLocation() })
            .compactMap { (positions: [CLLocation]) -> CLLocation? in
                positions

                    // Take only last 5 minutes positions
                    .filter { self.timeProvider.now.timeIntervalSince1970 - $0.timestamp.timeIntervalSince1970 < 60.0 * 5.0 }

                    .sorted { $0.timestamp > $1.timestamp }

                    // Take only the newest position
                    .first
            }
            .map { (position: CLLocation) in

                // Check if the position is inside of the region (given the accuracy)
                position.distance(from: CLLocation(latitude: center.latitude, longitude: center.longitude)) < position.horizontalAccuracy + radius
            }
            .distinctUntilChanged()
    }

}
