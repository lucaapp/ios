import Foundation
import Realm
import RxSwift

class KeyValueRepoEntryRealmModel: RealmSaveModel<KeyValueRepoEntry> {

    @objc dynamic var data = Data()
    @objc dynamic var key = ""

    override func create() -> KeyValueRepoEntry {
        return KeyValueRepoEntry(key: "", data: Data())
    }

    override func populate(from: KeyValueRepoEntry) {
        super.populate(from: from)
        data = from.data
        key = from.key
    }

    override var model: KeyValueRepoEntry {
        var m = super.model
        m.data = data
        m.key = key
        return m
    }
}

class RealmKeyValueUnderlyingRepo: RealmDataRepo<KeyValueRepoEntryRealmModel, KeyValueRepoEntry> {
    override func createSaveModel() -> KeyValueRepoEntryRealmModel {
        return KeyValueRepoEntryRealmModel()
    }

    init(key: Data?, filenameSalt: String) {
        super.init(filenameSalt: filenameSalt, schemaVersion: 0, encryptionKey: key)
    }
}

enum RealmKeyValueRepoError: LocalizedTitledError {
    case removingFailed
    case unknown(error: Error)
}

extension RealmKeyValueRepoError {
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        return "\(self)"
    }
}

class RealmKeyValueRepo: KeyValueRepoWithUnderlying {

    private let underlying: RealmKeyValueUnderlyingRepo

    init(key: Data?, filenameSalt: String) {
        underlying = RealmKeyValueUnderlyingRepo(key: key, filenameSalt: filenameSalt)
        super.init(underlying: underlying)
    }
}

extension RealmKeyValueRepo: RealmDatabaseUtils {
    func removeFile(completion: @escaping () -> Void, failure: @escaping ((Error) -> Void)) {
        self.underlying.removeFile(completion: completion, failure: failure)
    }

    func changeEncryptionSettings(oldKey: Data?, newKey: Data?, completion: @escaping () -> Void, failure: @escaping ((Error) -> Void)) {
        self.underlying.changeEncryptionSettings(oldKey: oldKey, newKey: newKey, completion: completion, failure: failure)
    }
}
