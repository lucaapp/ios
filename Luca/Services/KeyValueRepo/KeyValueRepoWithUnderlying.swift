import Foundation
import Realm
import RxSwift

struct KeyValueRepoEntry: Codable {
    var key: String
    var data: Data
}

extension KeyValueRepoEntry: DataRepoModel {
    var identifier: Int? {
        get {
            Int((key.data(using: .utf8) ?? Data()).crc32)
        }
        set {}
    }
}

/// Needs for coding values on iOS 12
struct ValueWrapper<T>: Codable where T: Codable {

    // This name is on purpose cryptic to reduce the probability of collisions with other custom data types
    var __temporaryValueWrapper: T
}

enum KeyValueRepoWithUnderlyingError: LocalizedTitledError {
    case encodingFailed
    case decodingFailed
    case storingFailed
    case keyNotFound(key: String)
    case restoringFailed(error: Error)
    case restoringForRemovalFailed(error: Error)
    case removingFailed(error: Error)
    case removingAllFailed(error: Error)
    case unknown(error: Error)
}

extension KeyValueRepoWithUnderlyingError {
    var localizedTitle: String {
        L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        "\(self)"
    }
}

class KeyValueRepoWithUnderlying: KeyValueRepoProtocol {

    private let underlying: DataRepo<KeyValueRepoEntry>

    typealias ErrorType = KeyValueRepoWithUnderlyingError

    init(underlying: DataRepo<KeyValueRepoEntry>) {
        self.underlying = underlying
    }

    func store<T>(_ key: String, value: T, completion: @escaping (() -> Void), failure: @escaping ((LocalizedTitledError) -> Void)) where T: Codable {
        guard let data = try? JSONEncoder().encode(ValueWrapper(__temporaryValueWrapper: value)) else {
            failure(ErrorType.encodingFailed)
            return
        }
        underlying.store(
            object: KeyValueRepoEntry(key: key, data: data),
            completion: {_ in completion()},
            failure: { _ in failure(ErrorType.storingFailed) }
        )
    }

    func load<T>(_ key: String, defaultValue: T?, completion: @escaping ((T) -> Void), failure: @escaping ((LocalizedTitledError) -> Void)) where T: Codable {
        underlying.restore { (entries) in
            guard let entry = entries.first(where: { $0.key == key }) else {
                if let defaultValue = defaultValue {
                    completion(defaultValue)
                    return
                }
                failure(ErrorType.keyNotFound(key: key))
                return
            }
            let decoded: T

            // There may be some values written with the wrapper so try to do it first
            if let firstAttempt = try? JSONDecoder().decode(ValueWrapper<T>.self, from: entry.data) {
                decoded = firstAttempt.__temporaryValueWrapper
            } else {

                // if that didn't work, try to decode it without the wrapper
                guard let secondAttempt = try? JSONDecoder().decode(T.self, from: entry.data) else {
                    failure(ErrorType.decodingFailed)
                    return
                }
                decoded = secondAttempt
            }
            completion(decoded)
        } failure: { (error) in
            if let expectedError = error as? LocalizedTitledError {
                failure(expectedError)
                return
            }
            failure(ErrorType.restoringFailed(error: error))
        }
    }

    func remove(_ key: String, completion: @escaping (() -> Void), failure: @escaping ((LocalizedTitledError) -> Void)) {
        underlying.restore { (entries) in
            guard let entry = entries.first(where: { $0.key == key }) else {
                completion()
                return
            }
            self.underlying.remove(
                identifiers: [entry.identifier!],
                completion: completion,
                failure: { error in
                    if let expectedError = error as? LocalizedTitledError {
                        failure(expectedError)
                        return
                    }
                    failure(ErrorType.removingFailed(error: error))
                })
        } failure: { (error) in
            if let expectedError = error as? LocalizedTitledError {
                failure(expectedError)
                return
            }
            failure(ErrorType.restoringForRemovalFailed(error: error))
        }
    }

    func removeAll(completion: @escaping (() -> Void), failure: @escaping ((LocalizedTitledError) -> Void)) {
        underlying.removeAll(completion: completion, failure: { (error) in
            if let expectedError = error as? LocalizedTitledError {
                failure(expectedError)
                return
            }
            failure(ErrorType.removingAllFailed(error: error))
        })
    }
}
