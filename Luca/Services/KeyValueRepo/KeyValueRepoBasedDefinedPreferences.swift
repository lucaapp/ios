import RxSwift

class KeyValueRepoBasedDefinedPreferences<PropertySource>: DefinedPreferences<PropertySource> {

    typealias PropertySource = PropertySource

    private var propertySource: PropertySource
    private var keyValueRepo: KeyValueRepoProtocol
    private var keysPrefix: String

    init(propertySource: PropertySource, repo: KeyValueRepoProtocol, keysPrefix: String = "") {
        self.propertySource = propertySource
        self.keyValueRepo = repo
        self.keysPrefix = keysPrefix
    }

    // MARK: - Preferences with default value
    override func set<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>, value: T) -> Completable where T: Codable {
        self.keyValueRepo.store(self.key(propertySource[keyPath: key].key), value: value)
    }

    override func get<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>) -> Single<T> where T: Codable {
        self.keyValueRepo
            .loadOptional(self.key(propertySource[keyPath: key].key))
            .map { $0 ?? self.propertySource[keyPath: key].defaultValue }
    }

    override func remove<T>(_ key: KeyPath<PropertySource, PreferencesEntryWithDefaultValue<T>>) -> Completable where T: Codable {
        self.keyValueRepo.remove(self.key(propertySource[keyPath: key].key))
    }

    // MARK: - Preferences without default value
    override func set<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>, value: T) -> Completable where T: Codable {
        self.keyValueRepo.store(self.key(propertySource[keyPath: key].key), value: value)
    }

    override func get<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>) -> Single<T?> where T: Codable {
        self.keyValueRepo
            .loadOptional(self.key(propertySource[keyPath: key].key))
    }

    override func remove<T>(_ key: KeyPath<PropertySource, PreferencesEntry<T>>) -> Completable where T: Codable {
        self.keyValueRepo.remove(self.key(propertySource[keyPath: key].key))
    }

    private func key(_ key: String) -> String {
        if keysPrefix != "" {
            return key
        }
        return keysPrefix + "." + key
    }
}
