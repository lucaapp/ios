import Foundation
import RxSwift

extension KeyValueRepoProtocol {

    func store<T>(_ key: String, value: T) -> Completable where T: Codable {
        Completable.create { (observer) -> Disposable in

            self.store(key, value: value) {
                observer(.completed)
            } failure: { (error) in
                observer(.error(error))
            }

            return Disposables.create()
        }
    }
    /// Emits requested object or emits an error `.objectNotFound` if not found. If `defaultValue` is not nil and the object is not found, the default value will be emitted instead of the error
    func load<T>(_ key: String, defaultValue: T? = nil) -> Single<T> where T: Codable {
        Single.create { (observer) -> Disposable in

            self.load(key, defaultValue: defaultValue) { (value) in
                observer(.success(value))
            } failure: { (error) in
                observer(.failure(error))
            }

            return Disposables.create()
        }
    }
    /// Emits requested object. If key not found, it will complete without value
    func loadMaybe<T>(_ key: String) -> Maybe<T> where T: Codable {
        load(key)
            .asMaybe()
            .catch {
                if let error = $0 as? KeyValueRepoWithUnderlyingError,
                   case KeyValueRepoWithUnderlyingError.keyNotFound = error {
                    return Maybe.empty()
                }
                throw $0
            }
    }

    func loadOptional<T>(_ key: String) -> Single<T?> where T: Codable {
        loadMaybe(key).map { $0 as T? }.ifEmpty(default: nil)
    }
    func remove(_ key: String) -> Completable {
        Completable.create { (observer) -> Disposable in

            self.remove(key) {
                observer(.completed)
            } failure: { (error) in
                observer(.error(error))
            }

            return Disposables.create()
        }
    }
    func removeAll() -> Completable {
        Completable.create { (observer) -> Disposable in

            self.removeAll {
                observer(.completed)
            } failure: { (error) in
                observer(.error(error))
            }

            return Disposables.create()
        }
    }
}
