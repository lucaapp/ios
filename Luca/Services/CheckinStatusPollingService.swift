import Foundation
import RxSwift
import RxAppState
import DependencyInjection

enum PollingFrequency: TimeInterval {
    case high = 3.0
    case low = 10.0
    case none = 0
}

class CheckinStatusPollingService: Toggleable {

    @InjectStatic(\.traceIdService) private var traceIdService: TraceIdService
    @InjectStatic(\.lucaPreferences) private var lucaPreferences

    private var disposeBag: DisposeBag! = nil
    private let frequencyBehavior = BehaviorSubject<PollingFrequency>(value: .none)

    /// The frequency the polling is running with. Setting a new value will stop and kickstart the polling with new interval
    var frequency: PollingFrequency {
        get {
            return (try? frequencyBehavior.value()) ?? .none
        } set {
            frequencyBehavior.onNext(newValue)
        }
    }

    /// Returns true if the service is enabled. It doesn't mean though that the actual polling is running.
    var isEnabled: Bool { disposeBag != nil }

    /// Enable the service. Polling will start accordingly to some settings so enabling alone won't kickstart the polling automatically.
    func enable() {
        guard disposeBag == nil else { return }

        disposeBag = DisposeBag()
        updateSignal()
            .flatMapLatest { _ in
                self.shouldStartPolling()
                    .asObservable()
                    .filter { $0 }
                    .flatMap { _ in self.frequencyBehavior.take(1) }
                    .map { RxTimeInterval.milliseconds(Int($0.rawValue * 1000.0)) }
                    .flatMap(self.poll(with:))
            }

            // It should run continuously until manually disabled
            .retry(delay: .seconds(1), scheduler: LucaScheduling.backgroundScheduler)
            .subscribe()
            .disposed(by: disposeBag)
    }

    /// Disable the service. It will force the polling to stop and not to kickstart ever again until the service is enabled back again.
    func disable() {
        disposeBag = nil
    }

    /// Emits a `Void` everytime when something changes that could stop or kickstart the polling
    private func updateSignal() -> Observable<Void> {
        Observable.merge(
            lucaPreferences.changes(\.uuid).map { _ in Void() },
            UIApplication.shared.rx.currentAndChangedAppState.map { _ in Void() },
            frequencyBehavior.distinctUntilChanged().map { _ in Void() }
        )
    }

    /// Emits `true` if everything is OK and the polling can start. `false` otherwise
    private func shouldStartPolling() -> Single<Bool> {
        Observable.combineLatest(
            lucaPreferences.get(\.uuid).map { $0 != nil }.asObservable(),
            UIApplication.shared.rx.currentAndChangedAppState,
            frequencyBehavior.asObservable()
        )
        .take(1)
        .asSingle()
        .map { $0.0 && $0.1 == .active && $0.2 != .none }
    }

    /// The actual polling mechanism
    private func poll(with interval: RxTimeInterval) -> Completable {
        Observable<Int>.timer(
            .seconds(2), // Start after some time to prevent spamming requests in case someone switches fequency too often causing this interval to start and stop quickly
            period: interval,
            scheduler: LucaScheduling.backgroundScheduler
        )
        .flatMap { _ in self.traceIdService.fetchTraceStatus() }
        .ignoreElementsAsCompletable()
    }
}
