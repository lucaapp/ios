import Foundation
import RxSwift
import RxCocoa
import DependencyInjection

class StagedRolloutService: NSObject, Toggleable {
    @InjectStatic(\.keyValueRepo) private var keyValueRepo
    @InjectStatic(\.backendStagedRolloutV4) private var backend

    private let stagedRolloutsKey = "stagedRolloutsKey"

    private var disposeBag: DisposeBag?
    var isEnabled: Bool { disposeBag != nil }

    private let isFeatureEnabledPublisher = PublishSubject<[StagedRollout]>()

    func isFeatureEnabled(type: StagedFeatureType) -> Observable<Bool> {
        Observable.merge(isFeatureEnabledPublisher.asObservable(),
                         getStoredStagedRollouts().asObservable())
            .flatMap { features -> Observable<Bool> in
                let enabled = features.filter({ $0.name == type }).first?.isEnabled ?? false
                return Observable.just(enabled)
            }
            .distinctUntilChanged()
    }

    func enable() {
        if isEnabled {
            return
        }

        let newDisposeBag = DisposeBag()
        Single.zip(
            getStoredStagedRollouts(),
            backend.fetchStagedRollouts().asSingle()
        )
            .flatMapCompletable { (storedFeatures, fetchedFeatures) in

                Single.just(self.updateFeatures(storedFeatures: storedFeatures, fetchedFeatures: fetchedFeatures))
                    .flatMapCompletable { updatedFeatures in
                        self.keyValueRepo.store(self.stagedRolloutsKey, value: updatedFeatures)
                            .andThen( Completable.from { self.isFeatureEnabledPublisher.onNext(updatedFeatures) })
                    }
            }
            .subscribe()
            .disposed(by: newDisposeBag)
        disposeBag = newDisposeBag

    }

    func disable() {
        disposeBag = nil
    }

    private func getStoredStagedRollouts() -> Single<[StagedRollout]> {
        keyValueRepo.load(stagedRolloutsKey, defaultValue: [])
    }

    private func updateFeatures(storedFeatures: [StagedRollout], fetchedFeatures: [StagedRollout]) -> [StagedRollout] {
        var updatedFeatures = [StagedRollout]()
        for fetchedFeature in fetchedFeatures {
            var gambledValue = Float.random(in: 0..<1)
            if let storedFeature = storedFeatures.filter({ $0.name == fetchedFeature.name }).first,
               let storedGambledValue = storedFeature.gambledValue {
                gambledValue = storedGambledValue
            }
            updatedFeatures.append(StagedRollout(name: fetchedFeature.name, percentage: fetchedFeature.percentage, gambledValue: gambledValue))
        }

        return updatedFeatures
    }
}
