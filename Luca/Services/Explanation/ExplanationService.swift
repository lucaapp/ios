import Foundation
import RxSwift
import DependencyInjection

class ExplanationService {

    private(set) var topics: [ExplanationTopic] = []
    private let explanationVersionKey = "explanationVersionKey"
    @InjectStatic(\.keyValueRepo) private var keyValueRepo

    var lastSeenAppVersion: Single<Int> {
        return keyValueRepo.load(explanationVersionKey)
            .catch { _ in Single.just(0) }
    }

    func add(_ topic: ExplanationTopic) {
        topics.append(topic)
    }

    func setDisplayed(_ topic: ExplanationTopic) -> Completable {
        return lastSeenAppVersion
            .flatMapCompletable({ [weak self] lastSeenAppVersion in
                guard let unwrappedSelf = self, lastSeenAppVersion < topic.appVersion else { return .empty() }
                return unwrappedSelf.keyValueRepo.store(unwrappedSelf.explanationVersionKey, value: topic.appVersion)
            })
            .onErrorComplete()
    }
}
