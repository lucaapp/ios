import Foundation
import UIKit

struct ExplanationTopic {

    var title: String
    var screens: [ExplanationScreen]
    var appVersion: Int = 0
}

struct ExplanationScreen {

    var text: String
    var image: UIImage
}
