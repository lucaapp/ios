import Foundation
import DependencyInjection

private var explanationServiceProvider: BaseInjectionProvider<ExplanationService> = EmptyProvider()

extension DependencyContext {

    var explanationService: BaseInjectionProvider<ExplanationService> {
        get { explanationServiceProvider }
        set { explanationServiceProvider = newValue }
    }
}

class ExplanationServiceInitialiser: BaseServiceInitialiser {

    override func initialise() throws {
        let explanationService = ExplanationService()
        explanationService.add(luca2Topic)
        explanationService.add(checkInTopic)
        explanationService.add(messagesTopic)
        explanationService.add(contactDataTopic)
        explanationService.add(paymentTopic)
        explanationService.add(checkinRedesignTopic)
        try register(explanationService)
            .as(\.explanationService) { SharedProvider(value: $0) }
    }

    private var luca2Topic: ExplanationTopic {
        ExplanationTopic(
            title: L10n.IOSApp.Explanation.Luca20.title,
            screens: [
                ExplanationScreen(text: L10n.IOSApp.Explanation.Luca20._1, image: Asset.explanationLuca201.image),
                ExplanationScreen(text: L10n.IOSApp.Explanation.Luca20._2, image: Asset.explanationLuca202.image),
                ExplanationScreen(text: L10n.IOSApp.Explanation.Luca20._3, image: Asset.explanationLuca203.image),
                ExplanationScreen(text: L10n.IOSApp.Explanation.Luca20._4, image: Asset.explanationLuca204.image),
                ExplanationScreen(text: L10n.IOSApp.Explanation.Luca20._5, image: Asset.explanationLuca205.image),
                ExplanationScreen(text: L10n.IOSApp.Explanation.Luca20._6, image: Asset.explanationLuca206.image),
                ExplanationScreen(text: L10n.IOSApp.Explanation.Luca20._7, image: Asset.explanationLuca207.image)
            ]
        )
    }

    private var checkInTopic: ExplanationTopic {
        ExplanationTopic(
            title: L10n.IOSApp.Explanation.Checkin.title,
            screens: [
                ExplanationScreen(text: L10n.IOSApp.Explanation.Checkin._1, image: Asset.explanationCheckin1.image),
                ExplanationScreen(text: L10n.IOSApp.Explanation.Checkin._2, image: Asset.explanationCheckin2.image),
                ExplanationScreen(text: L10n.IOSApp.Explanation.Checkin._3, image: Asset.explanationCheckin3.image),
                ExplanationScreen(text: L10n.IOSApp.Explanation.Checkin._4, image: Asset.explanationCheckin4.image)
            ]
        )
    }

    private var messagesTopic: ExplanationTopic {
        ExplanationTopic(
            title: L10n.IOSApp.Explanation.Messages.title,
            screens: [
                ExplanationScreen(text: L10n.IOSApp.Explanation.Messages._1, image: Asset.explanationMessages1.image),
                ExplanationScreen(text: L10n.IOSApp.Explanation.Messages._2, image: Asset.explanationMessages2.image),
                ExplanationScreen(text: L10n.IOSApp.Explanation.Messages._3, image: Asset.explanationMessages3.image)
            ]
        )
    }

    private var lucaIdTopic: ExplanationTopic {
        ExplanationTopic(
            title: L10n.IOSApp.Explanation.LucaID.title,
            screens: [
                ExplanationScreen(text: L10n.IOSApp.Explanation.LucaID._1, image: Asset.explanationLucaID1.image)
            ]
        )
    }

    private var contactDataTopic: ExplanationTopic {
        ExplanationTopic(
            title: L10n.IOSApp.Explanation.Contactdata.title,
            screens: [
                ExplanationScreen(text: L10n.IOSApp.Explanation.Contactdata._1, image: Asset.explanationContactdata1.image),
                ExplanationScreen(text: L10n.IOSApp.Explanation.Contactdata._2, image: Asset.explanationContactdata2.image),
                ExplanationScreen(text: L10n.IOSApp.Explanation.Contactdata._3, image: Asset.explanationContactdata3.image)
            ]
        )
    }

    private var paymentTopic: ExplanationTopic {
        ExplanationTopic(
            title: L10n.UserApp.Pay.Account.title,
            screens: [
                ExplanationScreen(text: L10n.IOSApp.Explanation.Payment._1, image: Asset.explanationPayment1.image),
                ExplanationScreen(text: L10n.IOSApp.Explanation.Payment._2, image: Asset.explanationPayment2.image)
            ],
            appVersion: 110
        )
    }

    private var checkinRedesignTopic: ExplanationTopic {
        ExplanationTopic(title: L10n.UserApp.Checkin.Redesign.Account.title,
                         screens: [ExplanationScreen(text: L10n.IOSApp.Explanation.Checkin.redesign, image: Asset.explanationCheckinRedesign.image)],
                         appVersion: 119)
    }
}
