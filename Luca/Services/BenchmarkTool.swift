import Foundation

class Benchmark {
    static func currentTime() -> TimeInterval {
        var info = mach_timebase_info()
        guard mach_timebase_info(&info) == KERN_SUCCESS else { return -1 }
        let currentTime = mach_absolute_time()
        let nanos = currentTime * UInt64(info.numer) / UInt64(info.denom)
        return TimeInterval(nanos) / 1000.0 / 1000.0 / 1000.0
    }

    static func measure(jobName: String, runs: UInt, block: () -> Void) -> PerformanceResults {
        var measures: [TimeInterval] = []
        for _ in 0..<runs {
            let begin = currentTime()
            block()
            measures.append(currentTime() - begin)
        }
        return PerformanceResults(jobName: jobName, durations: measures)
    }
}

struct PerformanceResults: Codable {
    let jobName: String
    var min: TimeInterval {
        durations.sorted().first ?? 0.0
    }
    var max: TimeInterval {
        durations.sorted().last ?? 0.0
    }
    let durations: [TimeInterval]
    var avg: TimeInterval {
        durations.reduce(0) { $0 + $1 } / TimeInterval(durations.count)
    }
}
