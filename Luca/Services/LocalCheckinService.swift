import Foundation
import RxSwift
import DependencyInjection

struct LocalCheckin {
    var qrCode: String
    var location: Location
    var selfCheckin: SelfCheckin
}

private struct LocalCheckinStoredData: Codable {
    var qrCode: String
    var locationID: String
}

extension LocalCheckin: Equatable {
    static func == (lhs: LocalCheckin, rhs: LocalCheckin) -> Bool {
        lhs.qrCode == rhs.qrCode
    }
}

struct LocalCheckinPreferencesTemplate {
    fileprivate var checkedInQRCode: PreferencesEntry<LocalCheckinStoredData> {
        PreferencesEntry(key: "checkedInData", type: LocalCheckinStoredData.self)
    }
}

enum LocalCheckinServiceError: LocalizedTitledError {

    case alreadyCheckedInSameLocation
    case alreadyCheckedInDifferentLocation

}

extension LocalCheckinServiceError {
    var errorDescription: String? {
        switch self {
        case .alreadyCheckedInSameLocation: return L10n.IOSApp.Checkin.Failure.AlreadyCheckedIn.message
        case .alreadyCheckedInDifferentLocation: return "\(self)"
        }
    }

    var localizedTitle: String {
        L10n.IOSApp.Navigation.Basic.error
    }
}

typealias LocalCheckinPayload = LocalCheckin

class LocalCheckinService {
    @InjectStatic(\.traceIdService) private var traceIdService
    @InjectStatic(\.locationRepo) private var locationRepo
    private let preferences = PreferencesTemplate(template: LocalCheckinPreferencesTemplate(), settingsPrefixKey: "localCheckInService")

    /// Emits current check in state and every subsequent change of it. `nil` if not checked in.
    var currentCheckIn: Observable<LocalCheckin?> {
        preferences.currentAndChanges(\.checkedInQRCode)
            .flatMap { checkInData -> Single<LocalCheckin?> in
                guard let checkInData = checkInData else {
                    return .just(nil)
                }
                return self.locationRepo.restore()
                    .map { $0.first(where: { $0.locationId == checkInData.locationID }) }
                    .map { location in
                        guard let location = location else {
                            return nil
                        }
                        return LocalCheckin(
                            qrCode: checkInData.qrCode,
                            location: location,
                            selfCheckin: try self.selfCheckin(for: checkInData.qrCode)
                        )
                    }
            }
    }

    /// Used to checkin with given qr code string.
    func checkin(with qrCode: String) -> Single<LocalCheckin> {
        preferences.get(\.checkedInQRCode)
            .flatMapCompletable {
                if $0 != nil {
                    throw $0?.qrCode == qrCode ? LocalCheckinServiceError.alreadyCheckedInSameLocation : LocalCheckinServiceError.alreadyCheckedInDifferentLocation
                }
                return .empty()
            }
            .andThen(generateLocalCheckinPayload(with: qrCode))
            .flatMap { localCheckin in
                self.preferences
                    .set(\.checkedInQRCode, value: LocalCheckinStoredData(qrCode: qrCode, locationID: localCheckin.location.locationId))
                    .andThen(.just(localCheckin))
            }

    }

    /// Used to parse qr code to the local checkin payload. It does not perform the checkin though.
    func generateLocalCheckinPayload(with qrCode: String) -> Single<LocalCheckinPayload> {
        Single.from { try self.selfCheckin(for: qrCode) }
            .flatMap { selfCheckin in
                self.traceIdService.fetchScanner(for: selfCheckin)
                    .flatMap { self.traceIdService.downloadLocationInfo(for: $0.locationId) }
                    .flatMap { .just(LocalCheckinPayload(qrCode: qrCode, location: $0, selfCheckin: selfCheckin)) }
            }

    }

    private func selfCheckin(for qrCode: String) throws -> SelfCheckin {
        guard let url = URL(string: qrCode),
              let selfCheckin = CheckInURLParser.parse(url: url) else {
            throw QRProcessingError.parsingFailed
        }
        return selfCheckin
    }

    func checkout() -> Completable {
        preferences.remove(\.checkedInQRCode)
    }
}

extension LocalCheckinService {

    /// Emits true when checked in and false when checked out.
    var isCheckedIn: Observable<Bool> {
        currentCheckIn.map { $0 != nil }
    }
}
