import Foundation
import DependencyInjection

protocol TimeProvider {

    /// Generates current timestamp
    var now: Date { get }
}

class DefaultTimeProvider: TimeProvider {

    var now: Date {
        Date()
    }
}

extension Date {

    /// Date value provided by the current `TimeProvider`
    static var now: Date {

        // The force unwrap is on purpose here. TimeProvider is such an essential service so it should crash when it's not defined
        (try? DependencyContext[\.timeProvider].provide())!.now
    }
}
