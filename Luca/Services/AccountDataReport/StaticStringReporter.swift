import Foundation
import RxSwift

class StaticStringReporter: ContentReporter {

    private let text: String

    init(text: String) {
        self.text = text
    }

    func createReport() -> Single<String> {
        Single.just(text)
    }
}
