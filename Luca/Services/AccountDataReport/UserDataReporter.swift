import Foundation
import RxSwift
import DependencyInjection

class UserDataReporter: ContentReporter {

    @InjectStatic(\.userService) private var userService: UserService

    func createReport() -> Single<String> {
        self.userService.userDescription()
    }
}
