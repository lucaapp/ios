import Foundation
import RxSwift
import DependencyInjection

class HistoryReporter: ContentReporter {

    @InjectStatic(\.history) private var historyService: HistoryService

    func createReport() -> Single<String> {
        historyService
            .removeOldEntries()
            .andThen(historyService.historyEvents)
            .asObservable()
            .flatMap { Observable.from($0) }
            .map { $0.contentDescription }
            .toArray()
            .map { $0.joined(separator: "\n-\n") }
    }
}
