import Foundation
import RxSwift
import DependencyInjection

class TraceInfoReporter: ContentReporter {

    @InjectStatic(\.history) private var historyService: HistoryService
    @InjectStatic(\.traceInfoRepo) private var traceInfoRepo: DataRepo<TraceInfo>

    func createReport() -> Single<String> {
        findHistoryTraceIds()
            .flatMap { ids in
                self.traceInfoRepo.restore()
                    .asObservable()
                    .flatMap { Observable.from($0) }
                    .filter { !ids.contains($0.traceId) }
                    .map { $0.contentDescription }
                    .toArray()
            }
            .map { $0.joined(separator: "\n-\n") }
    }

    private func findHistoryTraceIds() -> Single<[String]> {
        historyService
            .removeOldEntries()
            .andThen(historyService.historyEvents)
            .asObservable()
            .flatMap { Observable.from($0) }
            .compactMap { event in
                if let userEvent = event as? UserEvent,
                   let traceId = userEvent.checkin.traceInfo?.traceId {
                    return traceId
                }
                return nil
            }
            .toArray()
    }
}
