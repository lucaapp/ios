import RxSwift

protocol Reportable {
    var contentDescription: String { get }
}

protocol ContentReporter {
    func createReport() -> Single<String>
}

class ContentReporterService {

    private(set) var contentReporters: [ContentReporter] = []

    func register(contentReporter: ContentReporter) {
        contentReporters.append(contentReporter)
    }

    func createReport() -> Single<String> {
        Single.zip(contentReporters.map { $0.createReport() })
            .map { reports in
                reports.filter { !$0.isEmpty }
                    .joined(separator: "\n-\n")
            }
    }
}
