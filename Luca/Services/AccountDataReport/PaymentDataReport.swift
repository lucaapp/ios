import Foundation
import RxSwift
import DependencyInjection

class PaymentDataReporter: ContentReporter {

    @InjectStatic(\.lucaPayment) private var lucaPaymentService

    func createReport() -> Single<String> {
        Single.zip(lucaPaymentService.fetchUser(), lucaPaymentService.revocationCode)
            .map { user, revocationCode in
                let revocationEntry = "\(L10n.UserApp.Pay.Data.Request.Content.User.Revocation.code) \(revocationCode ?? L10n.IOSApp.General.unknown)"
                return "\(user.dataReport)\n\(revocationEntry)"
            }.flatMap { (userReport: String) in
                self.lucaPaymentService
                    .fetchPaymentHistory()
                    .map { history -> String in
                        let payments = history.map { $0.payments }.joined()
                        let entries = payments.map { $0.dataReport }
                        var entriesJoined = entries.joined(separator: "\n-\n")

                        if payments.isEmpty { entriesJoined = L10n.IOSApp.DataReport.Payment.notApplicable }
                        return userReport + "\n-\n" + L10n.UserApp.Pay.Data.Request.Content.Payment.History.info + " " + entriesJoined
                    }
            }
            .map { report in
                report + "\n-\n" +
                L10n.UserApp.Pay.Data.Request.Content.suffix + "\n\n" +
                L10n.IOSApp.DataReport.PrivacyPolicy.prefix + "\n\n" +
                L10n.IOSApp.DataReport.PrivacyPolicy.suffix
            }
    }

}

private extension Payment {

    var dataReport: String {
        L10n.IOSApp.DataReport.Payment.list(
            locationName as Any,
            amount as Any,
            tipAmount as Any,
            paymentVerifier ?? "" as Any
        )
    }

}

private extension PaymentUser {

    var dataReport: String {
        var paymentInformation = L10n.IOSApp.DataReport.Payment.notApplicable
        if let cards = tokenizedCards, !cards.isEmpty {
            paymentInformation = cards.map { L10n.IOSApp.DataReport.Payment.card($0.name, $0.type, $0.last4digits, $0.expirationYear, $0.expirationMonth) }.joined(separator: "\n-\n")
        }

        return L10n.IOSApp.DataReport.Payment.data(uuid, paymentInformation)
    }

}
