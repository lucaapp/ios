import Foundation
import RxSwift
import DependencyInjection

class LucaIDReporter: ContentReporter {

    @InjectStatic(\.lucaIDService) private var lucaIDService

    func createReport() -> Single<String> {
        lucaIDService.lucaIDProcessChanges
            .map({ (data: LucaIDParsedData?) -> String in
                guard let data = data else { return PrivacyPolicyReporter.defaultPrivacyPolicy }
                return data.dataReport()
            })
            .take(1)
            .asSingle()
    }

}

private extension LucaIDParsedData {

    func dataReport() -> String {
        return """
        \(L10n.IOSApp.DataReport.LucaID.IDnow.encryptedData)
        \(encryptedDataForReport())
        -
        \(L10n.IOSApp.DataReport.LucaID.IDnow.decryptedData)
        \(decryptedDataForReport())
        -
        \(L10n.IOSApp.DataReport.LucaID.IDnow.signedData)
        \(signedDataForReport())
        -
        \(privacyPolicyForReport())
        """
    }

    func encryptedDataForReport() -> String {
        let faceJWE = state.data?.valueFace ?? ""
        let identityJWE = state.data?.valueIdentity ?? ""
        let minimalIdentityJWE = state.data?.valueMinimalIdentity ?? ""
        return """
        \(L10n.IOSApp.DataReport.LucaID.IDnow.EncryptedData.faceJWE): \(faceJWE)
        \(L10n.IOSApp.DataReport.LucaID.IDnow.EncryptedData.identityJWE): \(identityJWE)
        \(L10n.IOSApp.DataReport.LucaID.IDnow.EncryptedData.minimalIdentityJwe): \(minimalIdentityJWE)
        """
    }

    func decryptedDataForReport() -> String {
        let firstName = identityJWT?.claims.vc.credentialSubject.givenName ?? ""
        let lastName = identityJWT?.claims.vc.credentialSubject.familyName ?? ""
        let birthDate = identityJWT?.claims.vc.credentialSubject.birthDate ?? ""
        return """
        \(L10n.IOSApp.DataReport.LucaID.IDnow.DecryptedData.firstName): \(firstName)
        \(L10n.IOSApp.DataReport.LucaID.IDnow.DecryptedData.lastName): \(lastName)
        \(L10n.IOSApp.DataReport.LucaID.IDnow.DecryptedData.birthdayTimestamp): \(birthDate)
        """
    }

    func signedDataForReport() -> String {
        let enrollmentToken = receiptJWS?.claims.autoIdentId ?? ""
        return """
        \(L10n.IOSApp.DataReport.LucaID.IDnow.SignedData.revocationCode): \(state.revocationCode)
        \(L10n.IOSApp.DataReport.LucaID.IDnow.SignedData.enrollmentToken): \(enrollmentToken)
        \(L10n.IOSApp.DataReport.LucaID.IDnow.SignedData.verificationStatus): \(state.state.rawValue)
        """
    }

    func privacyPolicyForReport() -> String {
        let idNowIdentityCode = receiptJWS?.claims.autoIdentId ?? ""
        return """
        \(L10n.IOSApp.DataReport.PrivacyPolicy.prefix)
        -
        \(L10n.IOSApp.DataReport.PrivacyPolicy.lucaID)
        \(L10n.IOSApp.DataReport.PrivacyPolicy.LucaID.iDnowIdentityCode): \(idNowIdentityCode)
        \(L10n.IOSApp.DataReport.PrivacyPolicy.LucaID.iDnowRevocationCode): \(state.revocationCode)
        -
        \(L10n.IOSApp.DataReport.PrivacyPolicy.suffix)
        """
    }
}
