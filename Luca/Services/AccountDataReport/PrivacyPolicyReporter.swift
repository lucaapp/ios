import Foundation
import RxSwift

class PrivacyPolicyReporter: ContentReporter {

    func createReport() -> Single<String> {
        Single.from {
            return PrivacyPolicyReporter.defaultPrivacyPolicy
        }
    }

    static var defaultPrivacyPolicy: String {
        L10n.IOSApp.DataReport.PrivacyPolicy.prefix + "\n" + L10n.IOSApp.DataReport.PrivacyPolicy.suffix
    }
}
