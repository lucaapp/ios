import Foundation
import RxSwift
import DependencyInjection

class DocumentReporter: ContentReporter {
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.documentRepoService) private var documentRepoService
    @InjectStatic(\.personRepo) private var personRepo

    func createReport() -> Single<String> {
        documentRepoService
            .currentAndNewTests
            .take(1)
            .flatMap { Observable.from($0) }
            .flatMap { document in
                self.documentOwner(document)
                    .map { $0 + "\n" + document.contentDescription }
                    .ifEmpty(default: document.contentDescription)
            }
            .toArray()
            .map { descriptions in
                descriptions.joined(separator: "\n-\n")
            }
    }

    private func documentOwner(_ document: Document) -> Maybe<String> {
        Maybe.from { document as? AssociableToIdentity }
            .flatMap { associableToIdentity in
                self.retrieveIdentities()
                    .map { identities in
                        identities.first(where: { associableToIdentity.belongs(to: Person(firstName: $0.firstName, lastName: $0.lastName, type: .unknown)) })
                    }
                    .asMaybe()
                    .unwrapOptional()
                    .map { L10n.IOSApp.DataReport.Document.ContactData.general($0.firstName, $0.lastName) }

            }
    }

    private func retrieveIdentities() -> Single<[(firstName: String, lastName: String)]> {
        lucaPreferences.get(\.userRegistrationData)
            .flatMap { userRegistrationData in
                self.personRepo.restore()
                    .map { persons in persons.map { (firstName: $0.firstName, lastName: $0.lastName) } }
                    .map { $0 + [(firstName: userRegistrationData?.firstName ??? "",
                                 lastName: userRegistrationData?.lastName ??? "")] }
            }
    }
}
