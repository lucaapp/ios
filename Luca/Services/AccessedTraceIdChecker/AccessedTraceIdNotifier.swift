import Foundation
import RxSwift
import BackgroundTasks
import UIKit
import DependencyInjection

class AccessedTraceIdNotifier: Toggleable {

    private let notificationScheduler: NotificationScheduler
    @InjectStatic(\.accessTraceIdChecker) private var accessedTraceIdChecker: AccessedTraceIdChecker
    @InjectStatic(\.notificationConfigCachedDataSource) private var notificationConfigSource
    @InjectStatic(\.lucaConnectMessageFetcher) private var lucaConnectMessageFetcher

    private var disposeBag: DisposeBag?
    private var notificationDisposeBag: DisposeBag?

    var isEnabled: Bool { disposeBag != nil }

    init(notificationScheduler: NotificationScheduler) {
        self.notificationScheduler = notificationScheduler
    }

    func enable() {
        if isEnabled {
            return
        }
        let newDisposeBag = DisposeBag()
        accessedTraceIdChecker.onNewData
            .observe(on: MainScheduler.instance)
            .flatMap { newNotifications in
                self.notificationConfigSource.retrieve()
                    .compactMap { $0.first }
                    .asObservable()
                    .take(1)
                    .do(onNext: { config in
                        let warningLevels = Set(newNotifications.map { $0.warningLevel })
                        for warningLevel in warningLevels {
                            if let msg = config.default.retrieveMessages(for: warningLevel),
                               let title = msg.title,
                               let banner = msg.banner {

                                self.notificationScheduler.scheduleNotification(title: title, message: banner)
                            }
                        }
                    })
            }
            .subscribe()
            .disposed(by: newDisposeBag)

        lucaConnectMessageFetcher.newMessages
            .observe(on: MainScheduler.instance)
            .do(onNext: { self.notificationScheduler.scheduleNotification(title: $0.message.title, message: $0.message.message) })
            .subscribe()
            .disposed(by: newDisposeBag)

        self.disposeBag = newDisposeBag
    }

    func disable() {
        disposeNotificationOnMatch()
        disposeBag = nil
    }

    func sendNotificationOnMatch(task: BGAppRefreshTask) {
        let newDisposeBag = DisposeBag()
        accessedTraceIdChecker.fetch()
            .do(onSuccess: { _ in
                task.setTaskCompleted(success: true)
            }, onError: { _ in
                task.setTaskCompleted(success: false)
            })
            .subscribe()
            .disposed(by: newDisposeBag)
        notificationDisposeBag = newDisposeBag
    }

    func sendNotificationOnMatch(completionHandler: @escaping(UIBackgroundFetchResult) -> Void) {
        let newDisposeBag = DisposeBag()
        accessedTraceIdChecker.fetch()
            .do(onSuccess: { data in
                if data.isEmpty {
                    completionHandler(.noData)
                } else {
                    completionHandler(.newData)
                }
            }, onError: { _ in
                completionHandler(.noData)
            })
            .subscribe()
            .disposed(by: newDisposeBag)
        notificationDisposeBag = newDisposeBag
    }

    func disposeNotificationOnMatch() {
        notificationDisposeBag = nil
    }
}
