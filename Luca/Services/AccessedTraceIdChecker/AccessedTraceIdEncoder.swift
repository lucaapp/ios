import Foundation
import RxSwift

class AccessedTraceIdEncoder {

    static func encode(
        traceIds: [TraceInfo],
        for healthDepartmentIDs: [UUID],
        warningLevels: [UInt8]) -> [EncryptedTraceInfo] {

        print("TEST.Encoding traceIds count: \(traceIds.count)    For HDs: \(healthDepartmentIDs.count)    For WarningLevels: \(warningLevels)")
        let startDate = Date.now

        let retVal = healthDepartmentIDs
            .map { healthDepartmentId in

                warningLevels
                    .map { warningLevel -> [EncryptedTraceInfo] in

                        let encryptedTraceIds = self.encode(traceIds: traceIds, for: healthDepartmentId, warningLevel: warningLevel)
                        return encryptedTraceIds
                    }
                    .flatMap { $0 }
            }
            .flatMap { $0 }
        print("TEST.DONE in \(Date.now.timeIntervalSince1970 - startDate.timeIntervalSince1970)!")
        return retVal
    }

    static func encode(traceIds: [TraceInfo], for healthDepartmentID: UUID, warningLevel: UInt8) -> [EncryptedTraceInfo] {
        let payload = healthDepartmentID.data + Data([warningLevel])
        let date = Date.now

        return traceIds.compactMap { traceId -> EncryptedTraceInfo? in
            guard let data = Data(base64Encoded: traceId.traceId),
                  let encrypted = try? HMACSHA256(key: data).encrypt(data: payload) else { return nil }

            return EncryptedTraceInfo(traceInfo: traceId, warningLevel: warningLevel, healthDepartmentID: healthDepartmentID, encryptedTraceId: encrypted, createdAt: date)
        }
    }
}
