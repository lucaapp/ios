import Foundation
import RxSwift
import BackgroundTasks
import DependencyInjection

extension AccessedTraceId {
    var isRead: Bool {
        readDate != nil
    }
}

struct EncryptedTraceInfo: Codable {
    var traceInfo: TraceInfo
    var warningLevel: UInt8
    var healthDepartmentID: UUID
    var encryptedTraceId: Data = Data()
    var createdAt: Date
}

class AccessedTraceIdChecker: Toggleable {

    // MARK: - Dependencies
    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.traceInfoRepo) private var traceInfoRepo
    @InjectStatic(\.locationRepo) private var locationRepo
    @InjectStatic(\.backendAccessDataV4) private var backendAccessData
    @InjectStatic(\.accessedTraceIdRepo) private var accessedTraceIdRepo
    @InjectStatic(\.notificationConfigCachedDataSource) private var notificationConfigSource
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.accessDataChunkHandler) private var accessDataHandler: AccessDataChunkHandler

    private let onNewDataSubject = BehaviorSubject<[AccessedTraceId]>(value: [])
    private var disposeBag: DisposeBag?

    /// New data spotted in the last `fetch` operation. Emits the current data set on subscription if any exist and on every change.
    var onNewData: Observable<[AccessedTraceId]> {
        onNewDataSubject.asObservable().filter { !$0.isEmpty }
    }

    private var fetchShared: Single<[AccessedTraceId]> = .just([])

    init() {

        // It's shared now, but it would be better if it would be cached for some time (<10 hours).
        fetchShared = Single.deferred { [weak self] in
            guard let self = self else { throw RxError.unknown }

            return Single.zip(
                self.generateLocalHashes(),
                self.accessDataHandler.fetch()
            )
            .map { (localHashes, dataChunks) -> [EncryptedTraceInfo] in
                self.filter(localHashes: localHashes, with: dataChunks)
            }
            .asObservable()
            .map { encryptedTraceInfos in
                encryptedTraceInfos.map {
                        AccessedTraceId(
                        healthDepartmentId: $0.healthDepartmentID,
                        traceInfoId: $0.traceInfo.identifier ?? -1,
                        warningLevel: $0.warningLevel,
                        traceId: $0.traceInfo.traceId,
                        createdAt: $0.createdAt,
                        sightDate: self.timeProvider.now,
                        readDate: nil
                    )
                }
            }
            .flatMap { (accessedTraceIds: [AccessedTraceId]) -> Single<[AccessedTraceId]> in
                self.filterNewData(accessTraceIds: accessedTraceIds)
                    .do(onSuccess: {
                        if !$0.isEmpty {
                            self.onNewDataSubject.onNext($0)
                        }
                    })
                }
                .flatMap { (accessedTraceIds: [AccessedTraceId]) -> Single<[AccessedTraceId]> in
                    self.accessedTraceIdRepo.store(objects: accessedTraceIds)
                }
                .asSingle()
        }
        .asObservable()
        .share(replay: 1, scope: .whileConnected)
        .asSingle()
    }

    /// Fetches the trace ids and emits new ones.
    func fetch() -> Single<[AccessedTraceId]> {
        fetchShared
    }

    /// Saves the read timestamp and emits the updated object.
    public func setAsRead(accessedTraceId: AccessedTraceId) -> Single<AccessedTraceId> {
        Single.from {
            var temp = accessedTraceId
            temp.readDate = self.timeProvider.now
            return temp
        }
        .flatMap { self.accessedTraceIdRepo.store(object: $0) }
    }

    // MARK: - Private helpers for filtering and managing new data

    /// Emits those entries, that have not been found in the DB.
    private func filterNewData(accessTraceIds: [AccessedTraceId]) -> Single<[AccessedTraceId]> {
        accessedTraceIdRepo.restore()
            .map { loadedTraceIds in

                let newOnes = accessTraceIds.subtract(loadedTraceIds,
                    equalityChecker: { $0.identifier == $1.identifier }
                )
                return newOnes
            }
    }

    /// Filters local hashes against the remote ones.
    /// - Returns: Local hashes that have been spotted in the backend.
    private func filter(localHashes: [EncryptedTraceInfo], with remoteData: [AccessedTracesDataChunk]) -> [EncryptedTraceInfo] {
        localHashes.filter { localHash in
            remoteData.contains(where: { dataChunk in
                let localValue = Data(localHash.encryptedTraceId.prefix(Int(dataChunk.hashLength)))
                return dataChunk.hashes.contains(localValue)
            })
        }
    }

    // MARK: - Generating local hashes
    private func generateLocalHashes() -> Single<[EncryptedTraceInfo]> {
        let thresholdDate = Calendar.current.date(byAdding: .day, value: -14, to: timeProvider.now) ?? timeProvider.now
        let filteredTraceInfos: Single<[TraceInfo]> = traceInfoRepo
            .restore()
            .map { traceIds in
                traceIds.filter { $0.checkInDate > thresholdDate }
            }

        return Single.zip(getOrFetchHealthDepartments(), filteredTraceInfos)
            .map { healthDepartments, traceIds in

                let warningLevels: [UInt8] = [1, 2, 3, 4]
                let healthDepartmentsIDs = healthDepartments.compactMap { UUID(uuidString: $0.uuid) }

                let encoded = AccessedTraceIdEncoder.encode(
                    traceIds: traceIds,
                    for: healthDepartmentsIDs,
                    warningLevels: warningLevels
                )
                return encoded
            }
    }

    // MARK: - Health department data management
    private func getOrFetchHealthDepartments(loadOnlyFromCache: Bool = false) -> Single<[NotificationHealthDepartment]> {
        notificationConfigSource.retrieve(loadOnlyFromCache: loadOnlyFromCache)
            .compactMap { $0.first }
            .asObservable()
            .map { $0.departments }
            .take(1)
            .asSingle()
            .catch { error in
                if let backendError = error as? BackendError<FetchHealthDepartmentError>,
                   let networkError = backendError.networkLayerError,
                   case NetworkError.noInternet = networkError {
                    return self.getOrFetchHealthDepartments(loadOnlyFromCache: true)
                }
                throw error
            }
    }
    var isEnabled: Bool {
        disposeBag != nil
    }
    func enable() {
        if isEnabled {
            return
        }
        let newDisposeBag = DisposeBag()

        UIApplication.shared.rx.currentAndChangedAppState
            .filter { $0 == .active || $0 == .background }
            .deferredFilter { _ in self.lucaPreferences.get(\.uuid).map { $0 != nil } }
            .flatMapFirst { _ in
                self.fetch()
                    .asCompletable()
                    .logError(self, "Fetching accessed trace ids")
                    .onErrorComplete()
            }
            .subscribe()
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

    func disable() {
        disposeBag = nil
    }

}

struct SectionedAccessedTraceId {
    struct Entry {
        var traceInfo: TraceInfo
        var location: Location
        var accessedTraceId: AccessedTraceId
    }
    var healthDepartment: NotificationHealthDepartment
    var accesses: [Entry]
}

 extension AccessedTraceIdChecker {

    func sort(data: [AccessedTraceId]) -> Single<[SectionedAccessedTraceId]> {
        getOrFetchHealthDepartments()
            .map { healthDepartments -> [NotificationHealthDepartment: [AccessedTraceId]] in
                var hd: [String: NotificationHealthDepartment] = [:]
                healthDepartments.forEach {
                    hd[$0.uuid.lowercased()] = $0
                }

                var dataAccessPerHD: [NotificationHealthDepartment: [AccessedTraceId]] = [:]

                data.forEach { dataAccess in
                    if let healthDepartment = hd[dataAccess.healthDepartmentId.uuidString.lowercased()] {
                        if dataAccessPerHD[healthDepartment] == nil {
                            dataAccessPerHD[healthDepartment] = []
                        }

                        dataAccessPerHD[healthDepartment]?.append(dataAccess)
                    }
                }
                return dataAccessPerHD
            }
            .flatMap { dataAccessPerHD in

                Single.zip(
                    self.traceInfoRepo.restore(), self.locationRepo.restore(),
                    resultSelector: { traceInfos, locations -> [SectionedAccessedTraceId] in

                        dataAccessPerHD.map { section -> SectionedAccessedTraceId in

                            let entries = section.value.compactMap { accessedTraceId -> SectionedAccessedTraceId.Entry? in
                                guard let traceInfo = traceInfos.first(where: { $0.identifier == accessedTraceId.traceInfoId}),
                                      let location = locations.first(where: {$0.locationId == traceInfo.locationId }) else {
                                    return nil
                                }
                                return SectionedAccessedTraceId.Entry(
                                    traceInfo: traceInfo,
                                    location: location,
                                    accessedTraceId: accessedTraceId
                                )
                            }

                            return SectionedAccessedTraceId(healthDepartment: section.key, accesses: entries)
                        }
                    })
            }
    }
 }

extension AccessedTraceIdChecker: LogUtil, UnsafeAddress {}
