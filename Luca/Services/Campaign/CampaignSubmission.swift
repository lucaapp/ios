import Foundation

struct CampaignSubmission: Codable {

    var uuid: String
    var consumerId: String
    var paymentId: String   // internal backend presentation of paymentId
    var campaign: CampaignType
    var email: String
    var language: String
    var revocationSecret: String
    var createdAt: Double

    var createdAtDate: Date? {
        return Date(timeIntervalSince1970: createdAt)
    }

}
