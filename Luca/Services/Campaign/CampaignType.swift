import Foundation

enum CampaignType: String, Codable {

    case lucaDiscount = "luca-discount"
    case tipTopUpAndRaffle = "PayRaffleAndTipTopUp"
    case unknown = "Unknown"

}
