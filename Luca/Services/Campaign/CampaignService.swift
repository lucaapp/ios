import Foundation
import DependencyInjection
import RxSwift

class CampaignService {

    @InjectStatic(\.backendPayment) private var backend
    @InjectStatic(\.lucaPayment) private var paymentService
    @InjectStatic(\.campaignRepo) private var campaignRepo

    var cache: ParametrisedCachedDataSource<PaymentCampaign, String>!

    init() {
        let cacheValidity = CacheValidity.until(unit: .hour, count: 1)

        cache = BaseParametrisedCachedDataSource(
            dataSourceFactory: { [weak self] param -> DataSourceWrapper<PaymentCampaign>? in
                guard let self = self else { return nil }
                return DataSourceWrapper(source: self.fetchCampaignData(param))
            },
            cacheDataRepoFactory: { KeyValueRepoCacheWrapper(uniqueCacheKey: "campaigns.\($0)") },
            cacheValidity: cacheValidity,
            uniqueCacheIdentifier: "cacheCampaigns"
       )
    }

    private func fetchCampaignData(_ locationId: String) -> Single<[PaymentCampaign]> {
        return backend.fetchCampaigns(locationId: locationId)
                    .asSingle()
    }

    func fetchCampaigns(forLocationId locationId: String) -> Single<[PaymentCampaign]> {
        return cache.retrieve(parameter: locationId).map { $0 }
    }

    func checkCampaign(for locationId: String, type: CampaignType, date: Date? = nil) -> Single<PaymentCampaign?> {
        return fetchCampaigns(forLocationId: locationId)
            .map { campaigns in
                return campaigns.first(where: {
                    let typeCondition = $0.campaignType == type
                    guard let startDate = $0.startDate else { return false }
                    guard let date = date else { return typeCondition }
                    let startCondition = startDate <= date
                    let endCondition = date <= $0.endDate ?? Date.distantFuture
                    return typeCondition && startCondition && endCondition
                })
            }
    }

    func submitParticipation(paymentId: String, campaign: String, email: String, language: String) -> Single<CampaignSubmissionData> {
        let params = CampaignSubmissionParams(campaign: campaign, email: email, language: language)

        return paymentService.retrieveAccessToken()
            .flatMap { token in
                self.backend.submitCampaignParticipation(paymentId: paymentId, params: params, accessToken: token)
                    .asSingle()
                    .flatMap { submission in
                        let submission = CampaignSubmissionData(paymentId: paymentId, submission: submission)
                        return self.campaignRepo.store(object: submission)
                    }
            }
    }

}
