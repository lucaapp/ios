import Foundation
import RxSwift
import BigInt
import DependencyInjection

struct PoWChallengeSolution: Codable {
    var id: PoWChallenge.Id
    var w: String
}

enum PoWServiceError: LocalizedTitledError {
    case malformedChallenge
    case challengeExpired
}

extension PoWServiceError {
    var localizedTitle: String {
        L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        "\(self)"
    }
}

struct PoWChallengeTask {
    enum State {
        case enqueued
        case inProgress
        case completed(solution: PoWChallengeSolution)
        case failed(error: Error)
    }
    var state: State = .enqueued
    var type: PoWType
    var challenge: PoWChallenge
}

// Request a new task -> retrieve challenge ID
// Check status of the challenge ID
// If expired

class PoWService: Toggleable {
    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.backendPoWV4) private var backend

    private let challengeScheduler = SerialDispatchQueueScheduler(qos: .default)
    private var disposeBag: DisposeBag?
    private let enqueuedChallengesQueue = DispatchQueueSynced<[PoWChallengeTask]>([])
    private let solvedChallengesQueue = DispatchQueueSynced<[PoWChallengeTask]>([])
    private let challengeInProgress = DispatchQueueSynced<PoWChallengeTask?>(nil)

    var isEnabled: Bool {
        disposeBag != nil
    }

    func requestAndScheduleChallenge(type: PoWType) -> Single<PoWChallenge.Id> {
        backend.requestChallenge(type: type)
            .asSingle()
            .flatMap { self.scheduleComputation(of: $0, type: type).map { $0.challenge.id } }
    }

    /// Emits current task and every status change of the challenge id.
    func task(of challengeId: PoWChallenge.Id) -> Observable<PoWChallengeTask> {
        (
            enqueuedChallengesQueue.currentAndChanges +
            solvedChallengesQueue.currentAndChanges +
            challengeInProgress.currentAndChanges.compactMap { $0 }.map { [$0] }
        )
            .flatMap { Observable.from($0) }
            .filter { $0.challenge.id == challengeId }
    }

    private func scheduleComputation(of challenge: PoWChallenge, type: PoWType) -> Single<PoWChallengeTask> {
        Single.from { PoWChallengeTask(type: type, challenge: challenge) }
            .flatMap { task in self.enqueuedChallengesQueue.writeRx { $0 + [task] }.andThen(.just(task)) }
    }

    /// Solves given challenge in a background thread
    private func solve(challenge: PoWChallenge) -> Single<PoWChallengeSolution> {
        Single.from {
            challenge
        }
        .continueInBackground {
            try self.solveBlocking(challenge: challenge)
        }
        .subscribe(on: challengeScheduler)
    }

    /// Solves given challenge immediately. It can run for multiple minutes.
    func solveBlocking(challenge: PoWChallenge) throws -> PoWChallengeSolution {
        let begin = timeProvider.now
        guard Date(timeIntervalSince1970: TimeInterval(challenge.expiresAt)) > begin else {
            throw PoWServiceError.challengeExpired
        }
        guard let t = Int(challenge.t),
              let n = BigInt(challenge.n) else {
            throw PoWServiceError.malformedChallenge
        }
        let two = BigInt(2)
        let w = two.power(two.power(t), modulus: n)

        guard Date(timeIntervalSince1970: TimeInterval(challenge.expiresAt)) > timeProvider.now else {
            throw PoWServiceError.challengeExpired
        }
        return PoWChallengeSolution(id: challenge.id, w: "\(w)")
    }

    func enable() {
        if isEnabled {
            return
        }
        let newDisposeBag = DisposeBag()
        let appState = UIApplication.shared.rx
            .currentAndChangedAppState
            .filter { $0 == .active }
            .map { _ in Void() }

        let updateSignal = appState + enqueuedChallengesQueue.onChange

        updateSignal
            .flatMapFirst { _ in
                self.solveNextTaskIfPending()
            }
            .subscribe()
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

    func disable() {
        disposeBag = nil
    }

    /// It takes the next pending challenge task and solves it. When failed or completed it puts it to the solved tasks queue and tries to solve the next one.
    private func solveNextTaskIfPending() -> Completable {
        self.enqueuedChallengesQueue
            .readRx()
            .asObservable()
            .compactMap { $0.first }
            .flatMap { task in
                self.enqueuedChallengesQueue.writeRx { $0.filter { $0.challenge.id != task.challenge.id } }
                    .andThen(self.challengeInProgress.writeRx { _ in

                        // Mark this task in progress
                        var inProgress = task
                        inProgress.state = .inProgress
                        return inProgress
                    })
                    .andThen(self.solve(challenge: task.challenge).debug("Solving task: \(task)").catch { error in

                        // Mark this task as failed
                        var failedTask = task
                        failedTask.state = .failed(error: error)
                        return self.solvedChallengesQueue.writeRx { $0 + [failedTask] }.andThen(.error(error))
                    })
                    .flatMapCompletable { (solution: PoWChallengeSolution) in

                        // Mark this task as completed
                        var solvedTask = task
                        solvedTask.state = .completed(solution: solution)
                        return self.solvedChallengesQueue.writeRx { $0 + [solvedTask] }
                    }
                    .logError(self, "Challenges queue")
                    .onErrorComplete()
                    .andThen(self.challengeInProgress.writeRx { _ in nil })

                    // It seems like an infinite recursion, but it's not.
                    // This code block (flatMap) is executed only if there is an element in powChallengesQueue.
                    // If it's empty, it won't be called at all.
                    .andThen(self.solveNextTaskIfPending())
            }
            .ignoreElementsAsCompletable()
    }
}

extension PoWService: LogUtil, UnsafeAddress {}
