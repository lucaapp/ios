import Foundation
import UIKit
import Alamofire
import DependencyInjection
import RxSwift

enum UserServiceError: LocalizedTitledError {
    case localDataIncomplete
    case unableToGenerateKeys(error: Error)
    case networkError(error: NetworkError)
    case userRegistrationError(error: BackendError<CreateUserError>)
    case userTransferError(error: BackendError<UserTransferError>)
    case userUpdateError(error: BackendError<UpdateUserError>)
    case userDeletionError(error: BackendError<DeleteUserError>)
    case unknown(error: Error)
}

extension UserServiceError {
    var errorDescription: String? {
        switch self {
        case .networkError(let error):
            return error.localizedDescription
        case .userRegistrationError(let error):
            return error.localizedDescription
        case .userTransferError(let error):
            return error.localizedDescription
        case .userUpdateError(let error):
            return error.localizedDescription
        default:
            return "\(self)"
        }
    }

    var error: Error? {
        switch self {
        case .localDataIncomplete:
            return self
        case .unableToGenerateKeys(error: let error):
            return error
        case .networkError(let error):
            return error
        case .userRegistrationError(error: let error):
            return error.backendError
        case .userTransferError(error: let error):
            return error.backendError
        case .userUpdateError(error: let error):
            return error.backendError
        case .userDeletionError(error: let error):
            return error.backendError
        case .unknown(error: let error):
            return error
        }
    }

    var localizedTitle: String {
        switch self {
        case .networkError(let error):
            return error.localizedTitle
        case .userRegistrationError(let error):
            return error.localizedTitle
        case .userTransferError(let error):
            return error.localizedTitle
        case .userUpdateError(let error):
            return error.localizedTitle
        default:
            return L10n.IOSApp.Navigation.Basic.error
        }
    }
}

struct Validity {
    var unit: Calendar.Component
    var count: Int
}

struct UserIDWithTimestamp: Codable, Equatable {
    var id: UUID
    var timestamp: Date
}

class UserService {

    enum Result {
        case userExists
        case userRecreated
    }

    // MARK: - dependencies
    @InjectStatic(\.lucaPreferences) private var lucaPreferences: LucaPreferences
    @InjectStatic(\.backendUserV3) private var backend: BackendUserV3
    @InjectStatic(\.userKeysBundle) private var userKeysBundle: UserKeysBundle
    @InjectStatic(\.timeProvider) private var timeProvider: TimeProvider
    @InjectStatic(\.userTransferBuilderV4) private var transferBuilder: UserTransferBuilderV4

    // MARK: - events
    public let onUserRegistered = "UserService.onUserRegistered"
    public let onUserDeleted = "UserService.onUserDeleted"
    public let onUserUpdated = "UserService.onUserUpdated"
    public let onUserDataTransfered = "UserService.onUserDataTransfered"
    public let onUserDataTransferedNumberOfDays = "UserService.onUserDataTransfered.numberOfDays"

    private let touchValidity = Validity(unit: .month, count: 6)

    var isUserDataComplete: Single<Bool> {
        lucaPreferences.get(\.userRegistrationData).map { $0?.isDataComplete ?? false }
    }

    var isRequiredDataComplete: Single<Bool> {
        lucaPreferences.get(\.userRegistrationData).map { $0?.isRequiredDataComplete ?? false }
    }

    func update(data: UserRegistrationData) -> Completable {
        Completable.from {
            guard data.isRequiredDataComplete else {
                self.log("Upload current data: local data incomplete", entryType: .error)
                throw UserServiceError.localDataIncomplete
            }
        }
        .andThen(lucaPreferences.get(\.uuid))
        .map { (uuid: UUID?) -> UUID in
            guard let uuid = uuid else {
                throw UserServiceError.localDataIncomplete
            }
            return uuid
        }
        .flatMapCompletable {
            self.backend.update(userId: $0, userData: data).asCompletable()
                .catch { error in
                    if let backendError = error as? BackendError<UpdateUserError>,
                       let updateUserError = backendError.backendError {
                        switch updateUserError {
                        case .invalidSignature, .notFound: return self.recreateUser(with: data)
                        default: break
                        }
                    }
                    throw error
                }
                .andThen(self.lucaPreferences.set(\.userRegistrationData, value: data))
                .andThen(self.lucaPreferences.set(\.lastUsersTouchTimestamp, value: self.timeProvider.now))
        }
        .do(onCompleted: {
            NotificationCenter.default.post(Notification(name: Notification.Name(self.onUserUpdated), object: self, userInfo: nil))
        })
        .catch { error in
            if let specificError = error as? BackendError<UpdateUserError> {
                throw UserServiceError.userUpdateError(error: specificError)
            } else {
                throw UserServiceError.unknown(error: error)
            }
        }
    }

    /// Performs a touch which marks the user as active.
    func touchIfNeeded() -> Completable {
        checkOldUUIDsSanity()
            .andThen(lucaPreferences.get(\.lastUsersTouchTimestamp))
            .flatMapCompletable { date in
                if date.isWithinLast(self.touchValidity.count, self.touchValidity.unit) {
                    return .empty()
                }
                return self.lucaPreferences
                    .get(\.userRegistrationData)
                    .map {
                        guard let data = $0 else {
                            throw UserServiceError.localDataIncomplete
                        }
                        return data
                }
                .flatMapCompletable { self.update(data: $0) }
            }
    }

    func deleteUserData() -> Completable {
        lucaPreferences.get(\.uuid).map {
            guard let userId = $0 else {
                self.log("Delete user: no user id", entryType: .error)
                throw UserServiceError.localDataIncomplete
            }
            return userId
        }
        .flatMapCompletable {
            self.backend.delete(userId: $0)
                .asCompletable()
                .catch { error in
                    if let backendError = error as? BackendError<DeleteUserError>,
                       backendError.backendError == .alreadyDeleted || backendError.backendError == .userNotFound {
                        return Completable.empty()
                    }
                    throw error
                }
        }
        .andThen(self.lucaPreferences.remove(\.oldUUIDs))
        .andThen(self.lucaPreferences.remove(\.uuid))
        .andThen(Completable.from { NotificationCenter.default.post(Notification(name: Notification.Name(self.onUserDeleted), object: self, userInfo: nil)) })
    }

    func transferUserData(forNumberOfDays numberOfDays: Int) -> Single<String> {
        checkOldUUIDsSanity()
            .andThen(transferBuilder.build(numberOfDays: numberOfDays, customIV: nil))
            .flatMap { self.backend.userTransfer(payload: $0).asSingle() }
            .do(onSuccess: { _ in
                NotificationCenter.default.post(
                    Notification(
                        name: Notification.Name(self.onUserDataTransfered),
                        object: self,
                        userInfo: [self.onUserDataTransferedNumberOfDays: numberOfDays])
                )
            })
    }

    func registerIfNeeded() -> Single<Result> {
        lucaPreferences.get(\.userRegistrationData)
            .flatMap { optionalUserRegistrationData in
                guard let userRegistrationData = optionalUserRegistrationData,
                      userRegistrationData.isRequiredDataComplete else {
                    throw UserServiceError.localDataIncomplete
                }
                return self.lucaPreferences.get(\.uuid)
                    .flatMap { uuid -> Single<Result> in
                        if uuid != nil {
                            return .just(.userExists)
                        }
                        return self.registerUser(userData: userRegistrationData)
                            .andThen(.just(.userRecreated))
                    }
            }
    }

    /// Removes users keypair and creates a new user without clearing any additional data.
    ///
    /// Used in the case when the signature is invalid but user wants to use the data.
    private func recreateUser(with data: UserRegistrationData) -> Completable {
        Completable.from {
            self.log("Recreating user: regenerating keys")
            self.userKeysBundle.removeKeys(onlyUserKeyPair: true)
            try self.userKeysBundle.generateKeys(forceRefresh: false)
            self.log("Recreating user: keys regenerated")
        }
        .andThen(checkOldUUIDsSanity())
        .andThen(backend.create(userData: data).asSingle())
        .flatMapCompletable { uuid in
            self.lucaPreferences.set(\.uuid, value: uuid)
                .andThen(self.add(userId: uuid, with: self.timeProvider.now))
        }
        .andThen(self.lucaPreferences.set(\.lastUsersTouchTimestamp, value: timeProvider.now))
    }

    private func registerUser(userData: UserRegistrationData) -> Completable {
        Completable.from {
            self.log("Registering user: regenerating keys")
            try self.userKeysBundle.generateKeys(forceRefresh: true)
            self.log("Registering user: keys regenerated")
        }
        .andThen(backend.create(userData: userData).asSingle())
        .flatMapCompletable { uuid in
            self.lucaPreferences.set(\.uuid, value: uuid)
                .andThen(self.lucaPreferences.set(\.lastUsersTouchTimestamp, value: self.timeProvider.now))
                .andThen(self.lucaPreferences.set(\.onboardingComplete, value: true))
                .andThen(self.lucaPreferences.remove(\.oldUUIDs))
                .andThen(self.add(userId: uuid, with: self.timeProvider.now))
        }
        .andThen(Completable.from {
            NotificationCenter.default.post(
                Notification(name: Notification.Name(self.onUserRegistered),
                             object: self,
                             userInfo: nil)
            )
        })
    }

    /// Adds given `UUID` to the archive of all userIds with given timestamp.
    private func add(userId: UUID, with timestamp: Date) -> Completable {
        lucaPreferences.get(\.oldUUIDs)
            .map { $0 + [UserIDWithTimestamp(id: userId, timestamp: timestamp)] }
            .flatMapCompletable { self.lucaPreferences.set(\.oldUUIDs, value: $0) }
    }

    /// This is in case where current UUID is not in the list of UUIDs with timestamps.
    private func checkOldUUIDsSanity() -> Completable {
        Single.zip(
            lucaPreferences.get(\.uuid).compactMap { $0 }.asObservable().asSingle(),
            lucaPreferences.get(\.oldUUIDs)
        ).flatMapCompletable { uuid, oldUUIDs in
            let containsCurrentUser = oldUUIDs.contains(where: { $0.id == uuid })
            if !containsCurrentUser {
                if let oldestEntry = self.userKeysBundle.traceSecrets.indices.sorted(by: { $0 < $1 }).first {
                    return self.lucaPreferences.set(\.oldUUIDs, value: [UserIDWithTimestamp(id: uuid, timestamp: oldestEntry)] + oldUUIDs)
                } else {
                    return self.lucaPreferences.set(\.oldUUIDs, value: [UserIDWithTimestamp(id: uuid, timestamp: self.timeProvider.now)] + oldUUIDs)
                }
            }
            return .empty()
        }
        .onErrorComplete()
    }

    func userDescription() -> Single<String> {
        Single.zip(lucaPreferences.get(\.userRegistrationData), lucaPreferences.get(\.phoneNumberVerified)) { userData, phoneNumberVerified in
            let name = "\(userData?.firstName ?? "") \(userData?.lastName ?? "")"
            let address = "\(userData?.street ?? "") \(userData?.houseNumber ?? ""), \(userData?.postCode ?? "") \(userData?.city ?? "")"
            let phoneVerified = phoneNumberVerified ? "(\(L10n.IOSApp.Verification.PhoneNumber.verified))" : ""
            let phone = "\(userData?.phoneNumber ?? "") \(phoneVerified)"
            let mail = userData?.email ?? "-"

            return L10n.IOSApp.DataReport.ContactData.userData(name, address, phone, mail)
        }
    }
}

extension UserService: LogUtil, UnsafeAddress {}
