import Foundation
import Security
import LocalAuthentication

class SecKeyPairHistoryRepository<IndexType>: KeyHistoryRepositoryProtocol where IndexType: Hashable & Codable {

    typealias KeyType = KeyPair<SecKey>
    typealias IndexType = IndexType

    private let privateRepository: SecKeyHistoryRepository<IndexType>
    private let publicRepository: SecKeyHistoryRepository<IndexType>

    /// Used when the factory is accessed
    private var cachedFactoriedEntries: [IndexType: KeyPair<SecKey>] = [:]

    var indices: Set<IndexType> {
        privateRepository.indices
    }

    var factory: ((IndexType) throws -> KeyPair<SecKey>)? {
        didSet {
            if factory == nil {
                privateRepository.factory = nil
                publicRepository.factory = nil
                return
            }

            publicRepository.factory = { [weak self] index in
                guard let self = self else {
                    throw NSError(domain: "No main instance", code: 0, userInfo: nil)
                }
                return try self.getOrCreateEntry(for: index).public
            }

            privateRepository.factory = { [weak self] index in
                guard let self = self else {
                    throw NSError(domain: "No main instance", code: 0, userInfo: nil)
                }
                return try self.getOrCreateEntry(for: index).private
            }
        }
    }

    var indexHeader: String

    init(tag: String) {
        indexHeader = tag
        privateRepository = SecKeyHistoryRepository<IndexType>(header: tag + ".private")
        publicRepository = SecKeyHistoryRepository<IndexType>(header: tag + ".public")
    }

    func store(key: KeyPair<SecKey>, index: IndexType) throws {
        try privateRepository.store(key: key.private, index: index)
        try publicRepository.store(key: key.public, index: index)
    }

    func restore(index: IndexType, context: LAContext? = nil, enableFactoryIfAvailable: Bool = true) throws -> KeyPair<SecKey> {
        let publicKey = try publicRepository.restore(index: index, context: context, enableFactoryIfAvailable: enableFactoryIfAvailable)
        let privateKey = try privateRepository.restore(index: index, context: context, enableFactoryIfAvailable: enableFactoryIfAvailable)
        return KeyPair(public: publicKey, private: privateKey)
    }

    func remove(index: IndexType) {
        publicRepository.remove(index: index)
        privateRepository.remove(index: index)
    }

    func removeAll() {
        publicRepository.removeAll()
        privateRepository.removeAll()
    }

    private func getOrCreateEntry(for index: IndexType) throws -> KeyPair<SecKey> {
        if let found = cachedFactoriedEntries[index] {
            return found
        }
        if let created = try self.factory?(index) {
            self.cachedFactoriedEntries[index] = created
            return created
        }
        throw NSError(domain: "No factory", code: 0, userInfo: nil)
    }
}
