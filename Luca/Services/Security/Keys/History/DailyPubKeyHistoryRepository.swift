import Foundation
import Security

class DailyPubKeyHistoryRepository: SecKeyHistoryRepository<DailyKeyIndex>, DailyPubKeyHistoryRepositoryKind {
    init() {
        super.init(header: "MasterPubGesAmt")
    }

    /// Get newest element
    var newestId: IndexType? {
        let retVal = [IndexType](self.indices)
            .sorted(by: { $0.createdAt > $1.createdAt })
            .first

        print("Newest keyId: \(String(describing: retVal))")
        return retVal
    }

    /// Get newest element with given ID. There could be multiple keys with the same ID but with various `createdAt` timestamps.
    func newest(withId: Int) -> IndexType? {
        return [IndexType](self.indices)
            .filter({ $0.keyId == withId })
            .sorted(by: { $0.createdAt > $1.createdAt })
            .first
    }
}
