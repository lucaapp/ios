import Foundation
import Security

struct DailyKeyIndex: Codable, Hashable {
    var keyId: Int
    var createdAt: Date
    var validUntil: Date

    func isValid(now: Date) -> Bool {
        validUntil > now
    }
}

protocol DailyPubKeyHistoryRepositoryKind: SecKeyHistoryRepository<DailyKeyIndex> {
    var newestId: DailyKeyIndex? {get}
    func newest(withId: Int) -> DailyKeyIndex?
}
