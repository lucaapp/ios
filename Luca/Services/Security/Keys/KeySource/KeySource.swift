import Foundation

public protocol KeySource {
    func retrieveKey() throws -> SecKey
}
