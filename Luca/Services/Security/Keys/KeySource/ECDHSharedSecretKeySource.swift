import Foundation

class ECDHSharedSecretKeySource: RawKeySource {

    private let privateKeySource: KeySource
    private let publicKeySource: KeySource

    init(publicKeySource: KeySource, privateKeySource: KeySource) {
        self.privateKeySource = privateKeySource
        self.publicKeySource = publicKeySource
    }

    func retrieveKey() throws -> Data {
        let privateKey = try privateKeySource.retrieveKey()
        let publicKey = try publicKeySource.retrieveKey()

        guard let sharedSecret = try? KeyFactory.exchangeKeys(privateKey: privateKey,
                                                         publicKey: publicKey,
                                                         algorithm: .ecdhKeyExchangeStandard,
                                                         requestedKeySize: 32) else {
            log("Couldn't create shared secret from ECDH", entryType: .error)
            throw NSError(domain: "Couldn't create shared secret from EC", code: 0, userInfo: nil)
        }

        return sharedSecret
    }
}

extension ECDHSharedSecretKeySource: UnsafeAddress, LogUtil {}
