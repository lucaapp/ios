import Foundation

/// This protocol is used the retrieve raw keys. It is used for types not officially supported by the Security library in iOS
public protocol RawKeySource {
    func retrieveKey() throws -> Data
}
