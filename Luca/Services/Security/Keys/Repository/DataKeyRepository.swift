import Foundation
import Security
import LocalAuthentication

class DataKeyRepository: KeyRepository<Data> {
    private let tag: String

    init(tag: String) {
        self.tag = tag
    }

    override func store(key: Data, removeIfExists: Bool = true) -> Bool {
        let addQuery: [String: Any] = [kSecClass as String: kSecClassKey,
                                       kSecAttrApplicationTag as String: tag,
                                       kSecValueData as String: key,
                                       kSecAttrAccessible as String: kSecAttrAccessibleAfterFirstUnlock]

        if removeIfExists {
            purge()
        }

        let result = SecItemAdd(addQuery as CFDictionary, nil)
        return result == errSecSuccess
    }

    override func restore(context: LAContext? = nil) throws -> Data {
        var getQuery: [String: Any] = [kSecClass as String: kSecClassKey,
                                       kSecAttrApplicationTag as String: tag,
                                       kSecReturnData as String: true]
        if let unwrappedContext = context {
            getQuery[kSecUseAuthenticationContext as String] = unwrappedContext
        }
        var item: CFTypeRef?
        let status = SecItemCopyMatching(getQuery as CFDictionary, &item)
        if status != errSecSuccess {
            throw NSError(domain: "Couldn't retrieve data key: \(status)", code: 0, userInfo: nil)
        }
        if let data = item as? Data {
            return data
        }
        throw NSError(domain: "Couldn't retrieve data key. Data is nil.", code: 0, userInfo: nil)
    }

    override func purge() {
        let query: [String: Any] = [kSecClass as String: kSecClassKey,
                                    kSecAttrApplicationTag as String: tag]

        SecItemDelete(query as CFDictionary)
    }
}

extension KeyRepository: RawKeySource where KeyType == Data {
    func retrieveKey() throws -> Data {
        try restore(context: nil)
    }
}
