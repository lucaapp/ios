import Foundation
import Security
import LocalAuthentication

class SecKeyPairRepository: KeyRepository<KeyPair<SecKey>> {
    private let privateRepo: SecKeyRepository
    private let publicRepo: SecKeyRepository
    init(tag: String) {
        privateRepo = SecKeyRepository(tag: tag + ".private")
        publicRepo = SecKeyRepository(tag: tag + ".public")
    }

    override func store(key: KeyPair<SecKey>, removeIfExists: Bool = true) -> Bool {
        guard privateRepo.store(key: key.private, removeIfExists: removeIfExists),
              publicRepo.store(key: key.public, removeIfExists: removeIfExists) else {
            return false
        }
        return true
    }

    override func restore(context: LAContext? = nil) throws -> KeyPair<SecKey> {
        let privateKey = try privateRepo.restore(context: context)
        let publicKey = try publicRepo.restore(context: context)
        return KeyPair(public: publicKey, private: privateKey)
    }

    override func purge() {
        privateRepo.purge()
        publicRepo.purge()
    }
}
