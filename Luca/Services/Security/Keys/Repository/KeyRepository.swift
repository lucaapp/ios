import Foundation
import Security
import LocalAuthentication

class KeyRepository<KeyType>: KeyRepositoryProtocol {

    func store(key: KeyType, removeIfExists: Bool = true) -> Bool {
        fatalError("Not implemented")
    }

    func restore(context: LAContext? = nil) throws -> KeyType {
        fatalError("Not implemented")
    }

    func purge() {
        fatalError("Not implemented")
    }
}
