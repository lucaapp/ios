import Foundation
import LocalAuthentication

protocol KeyRepositoryProtocol {

    associatedtype KeyType
    /// Stores the key.
    /// - returns: True if stored successfully, false if not
    /// - parameter key: Key to store
    /// - parameter removeIfExists: It will overwrite existed item if true. If false, it will save two items. Some implementation allow such situations.
    func store(key: KeyType, removeIfExists: Bool) -> Bool

    /// Restores the key
    ///
    /// - parameter context: it will reuse the same authentication context to prevent multiple passcode or biometry prompts in a row
    /// - returns: Key, if present. Nil if something prevented the restoration
    func restore(context: LAContext?) throws -> KeyType

    /// Removes the underlying item.
    func purge()
}
