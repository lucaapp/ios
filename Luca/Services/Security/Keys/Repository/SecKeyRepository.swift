import Foundation
import Security
import LocalAuthentication

class SecKeyRepository: KeyRepository<SecKey> {
    let tag: String

    init(tag: String) {
        self.tag = tag
    }

    override func store(key: SecKey, removeIfExists: Bool = true) -> Bool {
        let addQuery: [String: Any] = [kSecClass as String: kSecClassKey,
                                       kSecAttrApplicationTag as String: tag,
                                       kSecValueRef as String: key,
                                       kSecAttrAccessible as String: kSecAttrAccessibleAfterFirstUnlock]

        if removeIfExists {
            purge()
        }

        let result = SecItemAdd(addQuery as CFDictionary, nil)
        return result == errSecSuccess
    }

    override func restore(context: LAContext? = nil) throws -> SecKey {
        var getQuery: [String: Any] = [kSecClass as String: kSecClassKey,
                                       kSecAttrApplicationTag as String: tag,
                                       kSecReturnRef as String: true]

        if let unwrappedContext = context {
            getQuery[kSecUseAuthenticationContext as String] = unwrappedContext
        }
        var item: CFTypeRef?
        let status = SecItemCopyMatching(getQuery as CFDictionary, &item)
        if status != errSecSuccess {
            throw NSError(domain: "Couldn't restore Key: \(status)", code: 0, userInfo: nil)
        }

        // swiftlint:disable:next force_cast
        return (item as! SecKey)
    }

    override func purge() {
        let query: [String: Any] = [kSecClass as String: kSecClassKey,
                                    kSecAttrApplicationTag as String: tag]

        SecItemDelete(query as CFDictionary)
    }
}

extension KeyRepository: KeySource where KeyType == SecKey {
    func retrieveKey() throws -> SecKey {
        return try restore(context: nil)
    }
}
