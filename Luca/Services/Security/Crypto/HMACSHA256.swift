import Foundation
import CryptoSwift

class HMACSHA256: Encryption {
    private let key: Data

    init(key: Data) {
        self.key = key
    }

    func encrypt(data: Data) throws -> Data {
        let hmac = HMAC(key: key.bytes, variant: .sha2(.sha256))
        let bytes = try hmac.authenticate(data.bytes)
        return Data(bytes)
    }

    /// Verify sent HMAC with expected HMAC
    func verify(message: Data, receivedHMAC: Data) throws {
        let expectedHMAC = try encrypt(data: message)
        guard let randomKey = KeyFactory.randomBytes(size: 16) else {
            throw NSError(domain: "Random key could not be generated", code: 0, userInfo: nil)
        }

        // use HMAC with random key to avoid timing attacks based on time to compare two HMACs
        let randomHMAC = HMACSHA256(key: randomKey)
        let randomReceivedHMACEncryption = try randomHMAC.encrypt(data: receivedHMAC)
        let randomExpectedHMACEncryption = try randomHMAC.encrypt(data: expectedHMAC)

        if randomReceivedHMACEncryption != randomExpectedHMACEncryption {
            throw CryptoError.hmacNotVerified
        }
    }
}
