import Foundation
import Cryptor

protocol IESData: Codable {
    var data: String { get }
    var iv: String { get }
    var publicKey: String? { get }
    var mac: String { get }
}

class IES<Result: IESData> {
    func encrypt(data: Data) throws -> Result {
        fatalError("Not implemented")
    }

    private var generatedBaseSecret: Data?
    // swiftlint:disable implicit_getter
    fileprivate var baseSecret: Data {
        get throws {
            if let secret = generatedBaseSecret {
                return secret
            }
            let generatedSecret = try generateBaseSecret()
            generatedBaseSecret = generatedSecret
            return generatedSecret
        }
    }

    fileprivate func buildEncryptionKey() throws -> Data {
        var secret = try baseSecret
        secret.append(0x01)

        let encKey = secret.sha256().prefix(16)
        return encKey
    }

    fileprivate func buildMac(encData: Data) throws -> Data {
        let authKey = try buildAuthKey()
        let hmac = HMACSHA256(key: authKey)
        return try hmac.encrypt(data: encData)
    }

    fileprivate func buildAuthKey() throws -> Data {
        var secret = try baseSecret
        secret.append(0x02)

        let encKey = secret.sha256()
        return encKey
    }

    fileprivate func generateBaseSecret() throws -> Data {
        throw NSError(domain: "Not implemented", code: 0, userInfo: nil)
    }
}

struct DLIESEncryptedData: IESData {
    var data: String
    var publicKey: String?
    var iv: String
    var mac: String
}

struct ECIESEncryptedData: IESData {
    var data: String
    var iv: String
    var publicKey: String?
    var signature: String
    var mac: String
}

class DLIES: IES<DLIESEncryptedData> {

    private let ephemeralKeyPair: KeyPair<SecKey>
    private let receiverPublicKeySource: KeySource
    private let customSecret: Data?
    private let compressPublicKey: Bool
    private var iv: Data?

    init(ephemeralKeyPair: KeyPair<SecKey>, receiverPublicKeySource: KeySource, customSecret: Data? = nil, compressPublicKey: Bool = false, iv: Data? = nil) {
        self.receiverPublicKeySource = receiverPublicKeySource
        self.ephemeralKeyPair = ephemeralKeyPair
        self.customSecret = customSecret
        self.compressPublicKey = compressPublicKey
        self.iv = iv
    }

    private func crypto(iv: Data) throws -> Encryption & Decryption {
        let crypto = AESCTRCrypto(keySource: ValueRawKeySource(key: try buildEncryptionKey()), iv: iv.bytes)
        return crypto
    }

    override func encrypt(data: Data) throws -> DLIESEncryptedData {
        guard let iv = self.iv ?? KeyFactory.randomBytes(size: 16) else {
            throw NSError(domain: "Couldn't generate random iv", code: 0, userInfo: nil)
        }

        // Encode personal data
        let crypto = try self.crypto(iv: iv)
        let encData = try crypto.encrypt(data: data)

        let mac = try buildMac(encData: encData)

        let publicKey = ephemeralKeyPair.public
        var publicKeyData: Data = try publicKey.toData()
        if compressPublicKey {
            publicKeyData = try KeyFactory.compressPublicEC(key: publicKey)
        }

        return DLIESEncryptedData(
            data: encData.base64EncodedString(),
            publicKey: publicKeyData.base64EncodedString(),
            iv: iv.base64EncodedString(),
            mac: mac.base64EncodedString()
        )
    }

    override fileprivate func generateBaseSecret() throws -> Data {
        if let custom = customSecret {
            return custom
        }
        let secret = try KeyFactory.exchangeKeys(
            privateKey: ephemeralKeyPair.private,
            publicKey: try receiverPublicKeySource.retrieveKey()
        )
        return secret
    }
}

class ECIES: IES<ECIESEncryptedData> {
    private let dlies: DLIES
    private let ephemeralKeyPair: KeyPair<SecKey>
    init(ephemeralKeyPair: KeyPair<SecKey>, receiverPublicKeySource: KeySource, customSecret: Data? = nil, compressPublicKey: Bool = false) {
        self.ephemeralKeyPair = ephemeralKeyPair
        self.dlies = DLIES(
            ephemeralKeyPair: ephemeralKeyPair,
            receiverPublicKeySource: receiverPublicKeySource,
            customSecret: customSecret,
            compressPublicKey: compressPublicKey
        )
    }

    override func encrypt(data: Data) throws -> ECIESEncryptedData {
        let dliesResult = try dlies.encrypt(data: data)

        guard let encData = Data(base64Encoded: dliesResult.data),
              let mac = Data(base64Encoded: dliesResult.mac),
              let iv = Data(base64Encoded: dliesResult.iv) else {
                  throw NSError(domain: "Failed DLIES encryption", code: 0, userInfo: nil)
              }

        // Build signature
        var dataToSign = Data(encData)
        dataToSign.append(mac)
        dataToSign.append(iv)

        let ecdsa = ECDSA(privateKeySource: ValueKeySource(key: ephemeralKeyPair.private), publicKeySource: nil)

        let signature = try ecdsa.sign(data: dataToSign)

        return ECIESEncryptedData(
            data: dliesResult.data,
            iv: dliesResult.iv,
            publicKey: dliesResult.publicKey,
            signature: signature.base64EncodedString(),
            mac: dliesResult.mac
        )
    }
}
