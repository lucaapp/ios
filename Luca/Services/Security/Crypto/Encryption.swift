import Foundation

public enum CryptoError: Error {
    case privateKeyNotRetrieved
    case publicKeyNotRetrieved
    case symmetricKeyNotRetrieved

    case noPublicKeySource
    case noPrivateKeySource
    case noSharedKeySource

    case hmacNotVerified
}

public protocol Encryption {
    func encrypt(data: Data) throws -> Data
}
