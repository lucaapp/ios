import Foundation
import CryptoSwift

class AESGCMCrypto: Encryption, Decryption {
    private let keySource: RawKeySource

    private var iv: [UInt8] = []
    private var additionalAuthenticatedData: [UInt8]?
    private var tagLength: Int
    private var padding: Padding
    private var mode: GCM.Mode

    init(keySource: RawKeySource,
         iv: [UInt8],
         additionalAuthenticatedData: [UInt8]?,
         tagLength: Int = 16,
         padding: Padding = .pkcs7,
         mode: GCM.Mode = .detached) {
        self.keySource = keySource
        self.iv = iv
        self.additionalAuthenticatedData = additionalAuthenticatedData
        self.tagLength = tagLength
        self.padding = padding
        self.mode = mode
    }

    /// Encrypts using given key and previously set IV
    func encrypt(data: Data) throws -> Data {
        do {
            let key = try keySource.retrieveKey()
            let blockMode = GCM(
                iv: self.iv,
                additionalAuthenticatedData: additionalAuthenticatedData,
                tagLength: tagLength,
                mode: mode
            )
            let aes = try AES(key: key.bytes, blockMode: blockMode, padding: padding)
            let bytes = try aes.encrypt(data.bytes)
            return Data(bytes)
        } catch let error {
            log("Couldn't encrypt data! \(error)")
            throw error
        }
    }

    /// Decrypts using given key and previously set IV
    func decrypt(data: Data) throws -> Data {
        do {
            let key = try keySource.retrieveKey()
            let gcm = GCM(iv: self.iv, additionalAuthenticatedData: self.additionalAuthenticatedData, mode: .combined)
            let aes = try AES(key: key.bytes, blockMode: gcm)
            let bytes = try aes.decrypt(data.bytes)
            return Data(bytes)
        } catch let error {
            log("Couldn't decrypt data! \(error)")
            throw error
        }
    }
}

extension AESGCMCrypto: UnsafeAddress, LogUtil {}
