import Foundation
import CryptoSwift
import DependencyInjection
import CryptoKit
import Argon2Swift
import SwiftDGC

class LucaCommonCrypto: CommonCrypto {

    enum ErrorType: Error {
        case invalidKeyLength
        case invalidNonceLength
        case encryptedDataTooShort
        case invalidSignatureSize
        case invalidSignature
        case invalidTanSize
        case invalidUUID
    }

    enum CryptoLabels: String {
        case historyTan = "share_history_tan"
    }

    @InjectStatic(\.randomNumberGenerator) private var randomNumberGenerator
    @InjectStatic(\.keyGen) private var keyGen

    private let signatureAlgorithm = SecKeyAlgorithm.ecdsaSignatureMessageX962SHA256

    func symmetricEncrypt(secret: RawKeySource, label: Data, data: Data, associatedData: [Data]) throws -> Data {
        let salt = try randomNumberGenerator.generate(bytesCount: 8)
        let ciphertext = try symmetricEncrypt(secret: secret, label: label, data: data, associatedData: associatedData, salt: salt)

        return Data(salt.bytes + ciphertext.bytes)
    }

    private func symmetricEncrypt(secret: RawKeySource, label: Data, data: Data, associatedData: [Data], salt: Data?) throws -> Data {
        let keyMaterial = try deriveAESGCMKeyMaterial(secret: secret, label: label, salt: salt)
        let hashedAssociatedData = concatSHA256(data: associatedData)

        if keyMaterial.key.count != 16 {
            throw ErrorType.invalidKeyLength
        }
        if keyMaterial.nonce.count != 12 {
            throw ErrorType.invalidNonceLength
        }

        let encrypted = try AESGCMCrypto(
            keySource: ValueRawKeySource(key: keyMaterial.key),
            iv: keyMaterial.nonce.bytes,
            additionalAuthenticatedData: hashedAssociatedData.bytes,
            tagLength: 16,
            padding: .noPadding,
            mode: .combined
        )
        .encrypt(data: data)

        return encrypted
    }

    func symmetricDecrypt(secret: RawKeySource, label: Data, encryptedData: Data, associatedData: [Data]) throws -> Data {
        if encryptedData.count < 8 + 16 {
            throw ErrorType.encryptedDataTooShort
        }

        let salt = encryptedData.prefix(8)
        let ciphertext = encryptedData.suffix(from: 8)
        return try symmetricDecrypt(
            secret: secret,
            label: label,
            salt: salt,
            encryptedData: ciphertext,
            associatedData: associatedData
        )
    }

    func symmetricDecrypt(secret: RawKeySource, label: Data, salt: Data, encryptedData: Data, associatedData: [Data]) throws -> Data {

        let keyMaterial = try deriveAESGCMKeyMaterial(secret: secret, label: label, salt: salt)
        let hashedAssociatedData = concatSHA256(data: associatedData)

        if keyMaterial.key.count != 16 {
            throw ErrorType.invalidKeyLength
        }
        if keyMaterial.nonce.count != 12 {
            throw ErrorType.invalidNonceLength
        }

        let decrypted = try AESGCMCrypto(
            keySource: ValueRawKeySource(key: keyMaterial.key),
            iv: keyMaterial.nonce.bytes,
            additionalAuthenticatedData: hashedAssociatedData.bytes,
            tagLength: 16,
            padding: .noPadding,
            mode: .combined
        )
        .decrypt(data: encryptedData)

        return decrypted
    }

    func asymmetricEncrypt(publicKey: KeySource, label: Data, data: Data, associatedData: [Data], pepper: Data? = nil) throws -> Data {
        let keyPair = try keyGen.generateKeyPair()

        let ecdh = ECDHSharedSecretKeySource(publicKeySource: publicKey, privateKeySource: ValueKeySource(key: keyPair.privateKey))

        let ciphertext = try symmetricEncrypt(
            secret: ecdh,
            label: label,
            data: data,
            associatedData: associatedData,
            salt: pepper)

        let compressed = try KeyFactory.compressPublicEC(key: keyPair.publicKey)

        return Data(compressed.bytes + ciphertext.bytes)
    }

//    func asymmetricDecrypt(privateKey: KeySource, label: Data, encryptedData: Data, associatedData: [Data], pepper: Data?) throws -> Data {
//        guard encryptedData.count >= 33 + 16 else {
//            throw Self.ErrorType.encryptedDataTooShort
//        }
//
//        let compressedEphemeralPublicKey = encryptedData.prefix(33)
//        let ciphertext = encryptedData.suffix(from: 33)
//
//        let ephemeralPublicKey = try KeyFactory.create(from: compressedEphemeralPublicKey, type: .ecsecPrimeRandom, keyClass: .public)
//
//        let sharedKey = ECDHSharedSecretKeySource(
//            publicKeySource: ValueKeySource(key: ephemeralPublicKey),
//            privateKeySource: privateKey
//        )
//
//        return try symmetricDecrypt(
//            secret: sharedKey,
//            label: label,
//            salt: pepper ?? Data(),
//            encryptedData: ciphertext,
//            associatedData: associatedData
//        )
//    }

    func sign(privateKey: KeySource, data: [Data]) throws -> Data {
        let ecdsa = ECDSA(privateKeySource: privateKey, publicKeySource: nil, algorithm: signatureAlgorithm)
        let signature = try ecdsa.sign(data: concatSHA256(data: data))
        var decoded = ASN1.decode(from: signature)
        // since s (the second half) is always 32 bytes, append 0x00 to the r component
        while decoded.count < 64 {
            decoded.insert(0, at: 0)
        }
        return decoded
    }

    func verify(publicKey: KeySource, signature: Data, data: [Data]) throws {
        let ecdsa = ECDSA(privateKeySource: nil, publicKeySource: publicKey, algorithm: signatureAlgorithm)
        let encodedSignature = ASN1.encode(signature)
        do {
            guard try ecdsa.verify(data: concatSHA256(data: data), signature: encodedSignature) else {
                throw Self.ErrorType.invalidSignature
            }
        } catch let error {
            let nserror = error as NSError
            if nserror.code == -67808 {
                throw Self.ErrorType.invalidSignature
            }
            throw error
        }
    }

    /// Derive cryptographic key material that can be used in AES-GCM
    ///
    /// This method is a helper method to be used within our symmetric and
    /// asymmetric encrytion and decryption methods. Asymmetric crypto methods need
    /// to derive a symmetric secret via ECDH first.
    ///
    /// This method does not need to be exposed outside the crypto-specific code.
    ///
    /// We use HDKF to derive key material from a secret. To reduce the risk of key
    /// re-use, a random salt can be used for HKDF during key derivation. This is
    /// particularly useful when more than one encrypted "envelope" is created from
    /// the same initial secret.
    ///
    /// - Parameter secret: The symmetric secret used as initial key material
    /// - Parameter label:  The label used within HKDF key derivation
    /// - Parameter salt :  (optional) The salt used within HKDF key derivation
    ///
    /// - Returns: The key (16 bytes) and the nonce (12 bytes) to be used in AES-GCM
    private func deriveAESGCMKeyMaterial(secret: RawKeySource, label: Data, salt: Data? = nil) throws -> (key: Data, nonce: Data) {
        let keyMaterialLength = 28 // AES-key: 16 bytes + GCM-nonce: 12 bytes
        let key: Data = try secret.retrieveKey()
        if key.count != 16 && key.count != 32 {
            throw ErrorType.invalidKeyLength
        }
        let keyMaterial = try HKDF(
            password: key.bytes,
            salt: salt?.bytes,
            info: label.bytes,
            keyLength: keyMaterialLength,
            variant: .sha2(.sha256)
        )
        .calculate()

        if keyMaterial.count != keyMaterialLength {
            throw ErrorType.invalidKeyLength
        }

        let generatedKey = Data(keyMaterial[0..<16])
        let generatedNonce = Data(keyMaterial[16..<28])

        return (key: generatedKey, nonce: generatedNonce)
    }

    func argon2id(data: Data, salt: Data, length: Int) throws -> Data {
        try Argon2Swift.hashPasswordBytes(
            password: data,
            salt: Salt(bytes: salt),
            iterations: 11,
            memory: 32 * 1024,
            parallelism: 1,
            length: length,
            type: .id,
            version: .V13
        ).hashData()
    }

    func deriveSecret(from tanBytes: Data) throws -> TANSecrets {
        guard tanBytes.count == 7 else {
            throw Self.ErrorType.invalidTanSize
        }

        let publicSalt = "e2ca02296910dedd".data(using: .utf8)!
        let seed = try argon2id(data: tanBytes, salt: publicSalt, length: 16)
        print(seed.hexString)
        let derivedKey = try HKDF(
            password: seed.bytes,
            salt: nil,
            info: Self.CryptoLabels.historyTan.rawValue.data(using: .utf8)!.bytes,
            keyLength: 32,
            variant: .sha2(.sha256)
        )
            .calculate()

        let uuidData = Data(derivedKey.prefix(16))
        let keyData = Data(derivedKey.suffix(16))

        guard let uuid = UUID.init(data: uuidData, version: .v4, variant: .variant1) else {
            throw Self.ErrorType.invalidUUID
        }

        return TANSecrets(uuid: uuid, derivedKey: keyData)
    }

    func concatSHA256(data: [Data]) -> Data {
        Data(data.map { $0.sha256() }.joined())
    }
}
