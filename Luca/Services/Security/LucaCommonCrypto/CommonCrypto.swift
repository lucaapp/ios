import Foundation

struct TANSecrets {
    var uuid: UUID
    var derivedKey: Data
}

protocol CommonCrypto {
    /// Encrypt data based on a symmetric secret
    ///
    /// A random salt is used for key derivation to prevent key re-use. This is a
    /// resiliancy measure in case of misuse (performing multiple encryption
    /// operations with the same (secret, label) tuple).
    ///
    /// - Parameter secret: The symmetric secret.
    /// - Parameter label: Context specific label for the HKDF key derivation. The same label must be used for decryption.
    /// - Parameter data: The plaintext data.
    /// - Parameter associatedData: Context specific associated data for AES-GCM. The same associated data must be used for decryption.
    ///
    /// - Returns: Encrypted data consisting of ( 8 bytes salt | ciphertext ).
    func symmetricEncrypt(secret: RawKeySource, label: Data, data: Data, associatedData: [Data]) throws -> Data

    /// Decrypt data based on a symmetric secret
    ///
    /// - Parameter secret: The symmetric secret.
    /// - Parameter label: Context specific label for the HKDF key derivation. The same label must have been used for encryption.
    /// - Parameter encryptedData: The encrypted data consisting of ( 8 bytes salt | ciphertext ).
    /// - Parameter associatedData: Context specific associated data for AES-GCM. The same associated data must have been used for encryption.
    ///
    /// - Returns: The plaintext data.
    func symmetricDecrypt(secret: RawKeySource, label: Data, encryptedData: Data, associatedData: [Data]) throws -> Data

    /// Encrypt data based on a public key
    ///
    /// - Parameter publicKey: The receiver's public key.
    /// - Parameter label: Context specific label for the HKDF key derivation. The same label must be used for decryption.
    /// - Parameter data: The plaintext data.
    /// - Parameter associatedData: Context specific associated data for AES-GCM. The same associated data must be used for decryption.
    /// - Parameter pepper: Optional secret salt to be added to the key derivation. In contrast to symmetric_encrypt the salt is not included in the encryption result.
    ///
    /// - Returns Encrypted data consisting of ( 33 bytes ephemeral public key | ciphertext ).
    func asymmetricEncrypt(publicKey: KeySource, label: Data, data: Data, associatedData: [Data], pepper: Data?) throws -> Data

    /// Decrypt data using a private key
    ///
    /// - Parameter privateKey: The private key.
    /// - Parameter label: Context specific label for the HKDF key derivation. The same label must have been used for encryption.
    /// - Parameter encryptedData: The encrypted data consisting of ( 33 bytes ephemeral public key | ciphertext ).
    /// - Parameter associatedData: Context specific associated data for AES-GCM. The same associated data must be used for decryption.
    /// - Parameter pepper: Optional secret salt to be used in the key derivation.
    ///
    /// - Returns The plaintext data.
//    func asymmetricDecrypt(privateKey: KeySource, label: Data, encryptedData: Data, associatedData: [Data], pepper: Data?) throws -> Data

    /// Sign data with the given private key
    ///
    /// - Parameter privateKey: The private key
    /// - Parameter data: The data to be signed as a list of individual fields
    ///
    /// - Returns The signature in IEEE P.1363 format (binary concatenation of r and s)
    func sign(privateKey: KeySource, data: [Data]) throws -> Data

    /// Verify a signature
    ///
    /// Does not return anything but raises an exception if the signature is invalid.
    ///
    /// - Parameter publicKey: The public key (not encoded/compressed).
    /// - Parameter signature: The signature in IEEE P.1363 format (see sign_ecdsa_sha256).
    /// - Parameter data: The signed data.
    ///
    /// - Throws: InvalidSignature
    func verify(publicKey: KeySource, signature: Data, data: [Data]) throws

    func argon2id(data: Data, salt: Data, length: Int) throws -> Data

    /// Derive a history sharing ID and a history sharing key from a TAN
    ///
    /// Note: A constant salt used. While it might look scary at first, the fact
    /// that it is static is not a problem in this case. argon2id is not used for
    /// password hashing here but for key stretching from random input data.
    func deriveSecret(from tanBytes: Data) throws -> TANSecrets

    /// Hash each value with sha256 and concatenate the results
    ///
    /// This method is used to marshal the associated data It is important that the encoding of all
    /// for AES-GCM and the signature data.
    ///
    /// Note that the encoding of each value needs to be well-defined, but this specification is not
    /// part of this reference.
    ///
    /// - Parameter fields: The list of values to concatenate
    /// - returns: A bytestring of the concatenated hashes
    func concatSHA256(data: [Data]) -> Data
}
