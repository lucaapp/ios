import Foundation

struct GeneratedKeyPair {
    var privateKey: SecKey
    var publicKey: SecKey
}

protocol KeyGen {
    func generateKeyPair() throws -> GeneratedKeyPair
}

class LucaKeyGen: KeyGen {
    enum ErrorType: Error {
        case unableToDerivePublicKey
    }
    func generateKeyPair() throws -> GeneratedKeyPair {
        let privateKey = try KeyFactory.createPrivate(tag: "", type: .ecsecPrimeRandom, sizeInBits: 256, persistedInKeychain: false)
        guard let publicKey = KeyFactory.derivePublic(from: privateKey) else {
            throw ErrorType.unableToDerivePublicKey
        }
        return GeneratedKeyPair(privateKey: privateKey, publicKey: publicKey)
    }
}
