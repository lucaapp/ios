import Foundation
import SwiftJWT
import DependencyInjection
import RxSwift
import ASN1Decoder
import Reachability
import RxCocoa

struct DailyKeyClaims: Claims {
    var type: String
    var iss: String
    var keyId: Int
    var key: String

    var iat: Date?
    var exp: Date?
    var nbf: Date?
}

struct KeyIssuerClaims: Claims {
    var sub: String
    var iss: String
    var name: String
    var key: String
    var type: String

    var iat: Date?
    var exp: Date?
    var nbf: Date?
}

private struct KeyAndIssuerMaterial {
    var dailyJWT: JWT<DailyKeyClaims>
    var dailyJWTRaw: String
    var issuerJWT: JWT<KeyIssuerClaims>
    var issuerJWTRaw: String
    var issuerPublicCertificate: String
}

enum DailyKeyVerifierError: LocalizedTitledError {
    case dailyKeyMalformed
    case dailyKeyNotVerified
    case publicHDSKPKeyNotVerified
    case certificatesFingerprintMismatch
    case certificateChainNotTrusted
    case certificateMissingProperties
    case certificateNotValid
    case dailyKeyNotValid
    case publicHDSKPKeyNotValid
    case issuerDataIncomplete
    case systemClockAltered
}

extension DailyKeyVerifierError {
    var localizedTitle: String {
        L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        L10n.IOSApp.DailyKey.Fetch.FailedToSave.message("Signature is not valid")
    }
}

class DailyPublicKeyVerifier: CertificateVerifier, Toggleable, SharedProviderConnectionEventHandler {
    @InjectStatic(\.backendDailyKeyV4) private var backend
    @InjectStatic(\.dailyKeyRepository) private var dailyKeyRepo
    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.timeSync) private var timeSync

    let rootCertificateSource: Single<String>
    let intermediateCertificatesSource: Single<[String]>

    private var cachedDailyKey: CachedDataSource<CurrentDailyKey>! = nil
    private var cachedIssuer: CachedDataSource<[IssuerKeys]>! = nil
    private let numberOfIndicesToKeep = 35
    private let dailyKeyDefaultValidity: TimeInterval = Measurement(value: 24.0 * 7, unit: UnitDuration.hours)
        .converted(to: UnitDuration.seconds)
        .value

    /// Emits true if currently saved and previously checked key is still valid.
    ///
    /// Will emit new values after every change
    var isKeyValid: Driver<Bool> {
        Observable.merge(
            Observable.just(Void()),
            keyRefreshSignal.asObservable()
        )
        .asDriver(onErrorJustReturn: Void())
        .flatMap { _ -> Driver<Bool> in
            self.checkSystemTime().andThen(Single.just(self.isCurrentKeyValid)).asDriver(onErrorJustReturn: false)
        }
        .distinctUntilChanged()
    }

    /// Returns `true` if there is a key for current day. Returns `false` if not.
    ///
    /// This does not indicate whether current key is valid or not. For this information check `isCurrentKeyValid`.
    private var isTodaysKeyDownloaded: Bool {
        if let lastDate = dailyKeyRepo.newestId?.createdAt,
           Calendar.current.isDate(timeProvider.now, inSameDayAs: lastDate) {
            return true
        }
        return false
    }

    private var isCurrentKeyValid: Bool {
        dailyKeyRepo.newestId?.isValid(now: timeProvider.now) ?? false
    }

    private let keyRefreshSignal = PublishSubject<Void>()

    /// Verifies current backend. Fails if some of the checks didn't succeed
    ///
    /// It verifies following checks:
    /// - If system clock is in sync
    /// - If daily key is valid and signed by the HDSKP
    /// - If Public HDSKP is valid and signed
    /// - If HDSKP issuers certificate is valid
    /// - If HDSKP issuers certificate is signed by the intermediate and the root certificate
    /// - If HD Certificate contains specific properties
    ///
    /// It is defined as a property because it needs to be shared until completed.
    private var fetchAndVerify: Completable = .empty()

    init(rootCertificateSource: Single<String>, intermediateCertificatesSource: Single<[String]>) {
        self.rootCertificateSource = rootCertificateSource
        self.intermediateCertificatesSource = intermediateCertificatesSource
        cachedIssuer = BaseCachedDataSource(
            dataSource: backend.fetchAllIssuerKeys(),
            cacheDataRepo: KeyValueRepoCacheWrapper(uniqueCacheKey: "AllIsuers"),
            cacheValidity: .onceIn(unit: .day),
            uniqueCacheIdentifier: "AllIsuersCache"
        )
        cachedDailyKey = BaseCachedDataSource(
            dataSource: backend.fetchDailyPubKey(),
            cacheDataRepo: KeyValueRepoCacheWrapper(uniqueCacheKey: "DailyKey"),
            cacheValidity: .onceIn(unit: .day),
            uniqueCacheIdentifier: "DailyKeyCache"
        )

        // This rather unusual construct is needed because:
        // - This routine has to be shared because it will be accessed by:
        //    - the internal login defined in `enable`
        //    - and in LaunchViewController
        // - Lazy initialization is not thread safe so it has to be done in the init
        // - if `self` would be captured, it would produce retain cycle and would never deinitialize, hence the `deferred` and `weak self`
        fetchAndVerify = Completable.deferred { [weak self] in
            guard let self = self else { return .empty() }
            return self.checkSystemTime()
                .andThen(self.fetchAndValidateKeyAndIssuerMaterial())
                .flatMap { keyAndIssuerMaterial in
                    let pemString = keyAndIssuerMaterial.issuerPublicCertificate
                    return self.verifyCertificates(with: pemString).andThen(Single.just(keyAndIssuerMaterial))
                }
                .do(onSuccess: { material in
                    try self.verifyCNAndOUInCertificate(with: material)
                    try self.verifyHDSKP(with: material)
                    try self.verifyDailyKey(with: material)
                })
                .do(onSuccess: { try self.saveDailyKey(with: $0) })
                .asCompletable()
                .andThen(Completable.from { self.keyRefreshSignal.onNext(Void()) })
                .catch { error in
                    // Invalidate all key caches in case the error was only temporary. Without this invalidation user would need to wait til the end of the day
                    self.cachedIssuer
                        .invalidateCache()
                        .andThen(self.cachedDailyKey.invalidateCache())
                        .andThen(Completable.error(error))
                }
        }
        .asObservable()
        .share(replay: 1, scope: .whileConnected)
        .asCompletable()
    }

    func didDisconnectFromSharedProvider() {
        fetchAndVerify = .empty()
        cachedDailyKey = nil
        cachedIssuer = nil
    }

    /// Fetches and verifies todays key.
    ///
    /// Failure of this stream doesn't indicate whether there is no key at all! It would only indicate that there was something wrong with current update (no internet, invalid key etc.)
    /// For the indication whether there is a valid key (newer than 7 days), use `isKeyValid`
    func fetchAndVerifyIfNeeded() -> Completable {
        shouldUpdate
            .flatMapCompletable { shouldUpdate in
                if shouldUpdate {
                    return self.fetchAndVerify
                }
                return .empty()
            }
    }

    /// Checks if dailyKey is valid
    ///
    /// Performs following checks:
    /// - Checks if the downloaded `dailyKey` is yet and still valid
    /// - Checks if the `dailyKey`s signature is valid against `signedPublicHDSKP`
    /// - Checks if the issuer from the `dailyKey` matches the subject of `signedPublicHDSKP`
    private func verifyDailyKey(with material: KeyAndIssuerMaterial) throws {
        guard let keyData = Data(base64Encoded: material.issuerJWT.claims.key),
              keyData.count > 0 else {
            throw DailyKeyVerifierError.dailyKeyNotValid
        }
        let key = self.ecPublicKeyToPEM(publicKey: keyData)

        let verifier = JWTVerifier.es256(publicKey: key.data(using: .utf8) ?? Data())

        guard JWT<DailyKeyClaims>.verify(material.dailyJWTRaw, using: verifier) else {
            throw DailyKeyVerifierError.dailyKeyNotVerified
        }

        guard material.dailyJWT.claims.iss == material.issuerJWT.claims.sub else {
            throw DailyKeyVerifierError.dailyKeyNotVerified
        }
    }

    /// Checks if signedPublicHDSKP is valid
    ///
    /// Performs following checks:
    /// - If the signature matches given certificate
    /// - If the fingerprint of given certificate matches the one in the JWT Token
    private func verifyHDSKP(with material: KeyAndIssuerMaterial) throws {
        let verifier = JWTVerifier.rs512(certificate: material.issuerPublicCertificate.data(using: .utf8) ?? Data())

        guard JWT<KeyIssuerClaims>.verify(material.issuerJWTRaw, using: verifier) else {
            throw DailyKeyVerifierError.publicHDSKPKeyNotVerified
        }

        guard let der = self.PEMCertificateToDER(pem: material.issuerPublicCertificate) else {
            throw DailyKeyVerifierError.certificateNotValid
        }
        let generatedFingerprint = self.fingerprint(of: der).hexString
        let claimedFingerprint = material.issuerJWT.claims.iss
        guard generatedFingerprint == claimedFingerprint else {
            throw DailyKeyVerifierError.certificatesFingerprintMismatch
        }
    }

    /// Verifies if the certificate from the HD contains specific properties
    ///
    /// It checks following:
    /// - OU = `LUCA`
    /// - CN = ends with `.luca`
    private func verifyCNAndOUInCertificate(with material: KeyAndIssuerMaterial) throws {
        #if PRODUCTION
        let certificate = try X509Certificate(data: material.issuerPublicCertificate.data(using: .utf8) ?? Data())
        let subjectDistinguishedNames = Array((certificate.subjectDistinguishedName ?? "").split(separator: ","))
        var dict: [String: String] = [:]
        for entry in subjectDistinguishedNames {
            let pair = entry.split(separator: "=")
            if pair.count == 2 {
                dict[pair[0].trimmingCharacters(in: .whitespacesAndNewlines)] = pair[1].trimmingCharacters(in: .whitespacesAndNewlines)
            }
        }
        guard let cn = dict["CN"],
              let ou = dict["OU"] else {
            throw DailyKeyVerifierError.certificateMissingProperties
        }
        if ou != "LUCA" || !cn.hasSuffix(".luca") {
            throw DailyKeyVerifierError.certificateNotValid
        }
        #endif
    }

    /// Fetches daily key and its issuer and validates its dates. It does not verify its signatures!
    private func fetchAndValidateKeyAndIssuerMaterial() -> Single<KeyAndIssuerMaterial> {
        fetchDailyKey()
            .flatMap { daily in
                let jwt = try JWT<DailyKeyClaims>(jwtString: daily.signedPublicDailyKey)
                guard jwt.validateClaims(now: self.timeProvider.now, validityTime: self.dailyKeyDefaultValidity) == .success else {
                    throw DailyKeyVerifierError.dailyKeyNotValid
                }
                return self.fetchIssuer(jwt.claims.iss)
                    .map { issuer in
                        guard let signedPublicHDSKP = issuer.signedPublicHDSKP,
                              let certificate = issuer.publicCertificate else {
                            throw DailyKeyVerifierError.issuerDataIncomplete
                        }
                        let issuerJWT = try JWT<KeyIssuerClaims>(jwtString: signedPublicHDSKP)
                        guard issuerJWT.validateClaims(now: self.timeProvider.now) == .success else {
                            throw DailyKeyVerifierError.publicHDSKPKeyNotValid
                        }

                        return KeyAndIssuerMaterial(
                            dailyJWT: jwt,
                            dailyJWTRaw: daily.signedPublicDailyKey,
                            issuerJWT: issuerJWT,
                            issuerJWTRaw: signedPublicHDSKP,
                            issuerPublicCertificate: certificate
                        )
                    }
            }
    }

    /// Saves current daily key. Call it after all verifications have been run.
    private func saveDailyKey(with material: KeyAndIssuerMaterial) throws {
        guard let keyData = Data(base64Encoded: material.dailyJWT.claims.key) else {
            throw DailyKeyVerifierError.dailyKeyMalformed
        }

        let publicDailyKey = try KeyFactory.create(from: keyData, type: .ecsecPrimeRandom, keyClass: .public)
        let createdAt = material.dailyJWT.claims.iat?.timeIntervalSince1970 ?? 0
        let validuntil = material.dailyJWT.claims.exp?.timeIntervalSince1970 ?? createdAt + dailyKeyDefaultValidity
        try self.updateDailyKeyRepo(
            with: publicDailyKey,
            iat: createdAt,
            exp: validuntil,
            id: material.dailyJWT.claims.keyId
        )
    }

    /// Emits `CurrentDailyKey`.
    ///
    /// It tries to get the data from cache or downloads if not available or cache is expired.
    private func fetchDailyKey() -> Single<CurrentDailyKey> {
        cachedDailyKey.retrieve().asObservable().compactMap { $0.first }.asSingle()
    }

    /// Emits `IssuerKeys`.
    ///
    /// It tries to get the data from cache or downloads if not available or cache is expired.
    private func fetchIssuer(_ uuid: String) -> Single<IssuerKeys> {
        cachedIssuer
            .retrieve()
            .asObservable()
            .compactMap { $0.first }
            .compactMap { $0.first { $0.issuerId == uuid } }
            .asSingle()
    }

    /// Checks if system time is not been played around. Emits an error if so
    private func checkSystemTime() -> Completable {
        timeSync.isInSync()
            .do(onSuccess: { isInSync in
                if !isInSync {
                    throw DailyKeyVerifierError.systemClockAltered
                }
            })
            .asCompletable()
    }

    /// Saves the key in the repo
    private func updateDailyKeyRepo(with key: SecKey, iat: TimeInterval, exp: TimeInterval, id: Int) throws {
        try self.dailyKeyRepo.store(key: key, index: DailyKeyIndex(keyId: id,
                                                                   createdAt: Date(timeIntervalSince1970: iat),
                                                                   validUntil: Date(timeIntervalSince1970: exp)))

        // Get only N newest keys and dispose rest
        let allIndices = Array(self.dailyKeyRepo.indices)
        let sortedIndices = allIndices.sorted(by: { $0.createdAt.timeIntervalSince1970 > $1.createdAt.timeIntervalSince1970 })
        let indicesToKeep = sortedIndices.prefix(numberOfIndicesToKeep)
        let indicesToRemove = allIndices.difference(from: Array(indicesToKeep))
        for index in indicesToRemove {
            dailyKeyRepo.remove(index: index)
        }
    }

    // MARK: - Toggleable
    private var disposeBag: DisposeBag?

    /// Emits `true` if last successfull key fetch was yesterday or later. Emits `false` if last fetch happened in current day.
    private var shouldUpdate: Single<Bool> {
        let isTodaysKeyDownloaded = Single<Bool>.from { self.isTodaysKeyDownloaded }
        let isCacheValid = Single.zip(cachedIssuer.isCacheValid, cachedDailyKey.isCacheValid).map { $0 && $1 }
        return Single.zip(isCacheValid, isTodaysKeyDownloaded).map { !$0 || !$1 }
    }

    var isEnabled: Bool {
        return disposeBag != nil
    }

    func enable() {
        if isEnabled {
            return
        }
        let disposeBag = DisposeBag()
        let appStateChanged = UIApplication.shared.rx
            .currentAndChangedAppState
            .subscribe(on: MainScheduler.asyncInstance)
            .filter { $0 == .active }
            .map { _ in Void() }

        let connectivityChanged = Reachability.rx.isConnected
        let updateSignal = Observable.merge(appStateChanged, connectivityChanged)

        updateSignal
            .flatMapFirst { _ in
                self.shouldUpdate.flatMapCompletable { shouldUpdate in
                    self.keyRefreshSignal.onNext(Void())
                    if shouldUpdate {
                        return self.fetchAndVerify.logError(self, "internal key update on app activation.").onErrorComplete()
                    }
                    return .empty()
                }
            }
            .subscribe()
            .disposed(by: disposeBag)

        self.disposeBag = disposeBag
    }

    func disable() {
        disposeBag = nil
    }
}

extension DailyPublicKeyVerifier: UnsafeAddress, LogUtil {}
