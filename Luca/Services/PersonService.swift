import Foundation
import RxSwift
import DependencyInjection

enum PersonType: String, Codable {
    case user
    case child
    case placeholder // Dummy user required to show a document not linked to a certain user 
    case unknown
}

struct Person: Codable, DataRepoModel, Hashable, HasFirstAndLastName {
    var identifier: Int? = Int.random(in: Int.min...Int.max)
    var firstName: String
    var lastName: String
    var type: PersonType

    /// List of all TraceInfo identifiers this person was checked in
    var traceInfoIDs: [Int] = []
}

extension Person {
    var formattedName: String {
        return "\(firstName) \(lastName)"
    }

    func isAssociated(with traceInfo: TraceInfo) -> Bool {
        traceInfoIDs.contains(traceInfo.identifier ?? -1)
    }
}

class PersonService {

    @InjectStatic(\.personRepo) private var personRepo
    @InjectStatic(\.documentProcessingService) private var documentProcessing

    /// Creates a new person with given names
    /// - Parameters:
    ///   - firstName: first name
    ///   - lastName: last name
    ///   - type: PersonType
    func create(firstName: String, lastName: String, type: PersonType) -> Single<Person> {
        let person = Person(firstName: firstName, lastName: lastName, type: type)
        return personRepo.store(object: person)
    }

    /// Updates the person.
    ///
    /// The `identifier` has to stay unchanged to guarantee the object to update
    /// - Parameter person: person to update
    func update(person: Person) -> Completable {
        personRepo.store(object: person)
            .asCompletable()
            .andThen(documentProcessing.revalidateSavedTests())
    }

    /// Removes a specific person
    /// - Parameter person: person to remove
    func remove(person: Person) -> Completable {
        Maybe<Int>.from { person.identifier }
            .asObservable()
            .flatMap { self.personRepo.remove(identifiers: [$0]) }
            .asCompletable()
            .andThen(documentProcessing.revalidateSavedTests())
    }

    /// Retrieves all persons with or without a specific predicate
    /// - Parameter predicate: if not given, all persons would be retrieved
    func retrieve(_ predicate: ((Person) -> Bool)? = nil) -> Single<[Person]> {
        personRepo
            .restore()
            .map { array in array.filter { predicate?($0) ?? true } }
    }

    /// Retrieves all persons that are associated with given TraceInfo
    /// - Parameter traceInfo: TraceInfo to search for
    func retrieveAssociated(with traceInfo: TraceInfo) -> Single<[Person]> {
        retrieve {
					guard let traceInfoID = traceInfo.identifier else {return true}
					return $0.traceInfoIDs.contains(traceInfoID)
				}
    }

    /// Adds and saves an association of specific persons
    /// - Parameters:
    ///   - persons: persons to associate the traceInfo with
    ///   - traceInfo: traceInfo to associate
    func associate(persons: [Person], with traceInfo: TraceInfo) -> Completable {
        Single.from { persons.map { $0.identifier } }
            .asObservable()
            .flatMap { inputIDs in self.retrieve { inputIDs.contains($0.identifier) } }
            .flatMap { Observable.from($0) }
            .map { immutablePerson in
                var temp = immutablePerson
                temp.traceInfoIDs.append(traceInfo.identifier ?? -1)
                return temp
            }
            .toArray()
            .flatMap(personRepo.store)
            .asCompletable()
    }

    /// Removes the association of selected persons with a given traceInfo
    func removeAssociation(persons: [Person], from traceInfo: TraceInfo) -> Completable {
        Single.from { persons.map { $0.identifier } }
            .asObservable()
            .flatMap { inputIDs in self.retrieve { inputIDs.contains($0.identifier) } }
            .flatMap { Observable.from($0) }
            .map { immutablePerson in
                var temp = immutablePerson
                temp.traceInfoIDs = immutablePerson.traceInfoIDs.filter { $0 != traceInfo.identifier ?? -1 }
                return temp
            }
            .toArray()
            .flatMap(personRepo.store)
            .asCompletable()
    }
}
