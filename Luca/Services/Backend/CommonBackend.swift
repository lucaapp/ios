import Foundation
import Alamofire
import DependencyInjection

class BaseBackendSMSVerification: BackendSMSVerification {

    @InjectStatic(\.backendAddressV3) private var backendAddress: BackendAddressV3

    func requestChallenge(phoneNumber: String) -> AsyncDataOperation<BackendError<RequestChallengeError>, RequestChallengeResult> {
        SMSRequestChallengeAsyncDataOperation(backendAddress: backendAddress, phoneNumber: phoneNumber)
    }
    func verify(tan: String, challenge: String) -> AsyncOperation<BackendError<VerifyChallengeError>> {
        VerifyChallengeRequestAsyncOperation(backendAddress: backendAddress, challengeId: challenge, tan: tan)
    }
    func verify(tan: String, challenges: [String]) -> AsyncDataOperation<BackendError<VerifyChallengeError>, String> {
        VerifyChallengeBulkRequestAsyncOperation(backendAddress: backendAddress, challengeIds: challenges, tan: tan)
    }
}

class CommonBackendMisc: BackendMisc {

    @InjectStatic(\.backendAddressV3) private var backendAddress: BackendAddressV3

    func fetchHealthDepartment(healthDepartmentId: UUID) -> AsyncDataOperation<BackendError<FetchHealthDepartmentError>, HealthDepartment> {
        FetchDepartmentAsyncOperation(backendAddress: backendAddress, departmentId: healthDepartmentId)
    }

    func fetchScanner(scannerId: String) -> AsyncDataOperation<BackendError<FetchScannerError>, ScannerInfo> {
        FetchScannerAsyncOperation(backendAddress: backendAddress, scannerId: scannerId)
    }

    func fetchSupportedVersions() -> AsyncDataOperation<BackendError<FetchSupportedVersionError>, SupportedVersions> {
        FetchSupportedVersionsAsyncOperation(backendAddress: backendAddress)
    }

    func fetchAccessedTraces() -> AsyncDataOperation<BackendError<FetchAccessedTracesError>, [AccessedTrace]> {
        FetchAccessedTracesAsyncDataOperation(backendAddress: backendAddress)
    }

    func fetchTimesync() -> AsyncDataOperation<BackendError<TimesyncError>, Timesync> {
        FetchTimesyncAsyncOperation(backendAddress: backendAddress)
    }

    func fetchTestProviderKeys() -> AsyncDataOperation<BackendError<FetchTestProviderKeysError>, [TestProviderKey]> {
        FetchTestProviderKeysDataAsyncOperation(backendAddress: backendAddress)
    }

    func redeemDocument(hash: Data, tag: Data) -> AsyncOperation<BackendError<RedeemDocumentError>> {
        RedeemDocumentAsyncOperation(backendAddress: backendAddress, hash: hash, tag: tag)
    }

    func releaseDocument(hash: Data, tag: Data) -> AsyncOperation<BackendError<ReleaseDocumentError>> {
        ReleaseDocumentAsyncOperation(backendAddress: backendAddress, hash: hash, tag: tag)
    }
}

class BackendLucaID {
    @InjectStatic(\.backendAddressV4) private var backendAddress: BackendAddressV4

    func requestIdentProcess(assertionJWT: String, payload: LucaIDRequestProcessRequest, mock: Bool) -> AsyncDataOperation<BackendError<LucaIDRequestProcessAsyncDataOperationError>, LucaIDRequestProcessResult> {
        LucaIDRequestProcessAsyncDataOperation(
            backendAddress: backendAddress,
            payload: payload,
            assertionJWT: assertionJWT,
            mock: mock
        )
    }

    func fetchIDState(attestationJWT: String) -> AsyncDataOperation<BackendError<LucaIDFetchStateError>, LucaIDProcess> {
        LucaIDFetchStateAsyncDataOperation(backendAddress: backendAddress, attestationJWT: attestationJWT)
    }

    func requestAttestationNonce() -> AsyncDataOperation<BackendError<RequestAttestationNonceError>, RequestAttestationNonceResult> {
        RequestAttestationNonceAsyncOperation(backendAddress: backendAddress)
    }

    func deleteData(attestationJWT: String) -> AsyncOperation<BackendError<LucaIDDeleteDataError>> {
        LucaIDDeleteDataAsyncOperation(backendAddress: backendAddress, attestationJWT: attestationJWT)
    }

    func archiveData(attestationJWT: String) -> AsyncOperation<BackendError<LucaIDArchiveDataError>> {
        LucaIDArchiveDataAsyncOperation(backendAddress: backendAddress, attestationJWT: attestationJWT)
    }

    func registerAttestation(payload: RegisterAttestationRequest) -> AsyncDataOperation<BackendError<RegisterAttestationError>, RegisterAttestationResult> {
        RegisterAttestationAsyncOperation(backendAddress: backendAddress, payload: payload)
    }

    func requestAssertionJWT(payload: RequestAssertionJWTRequest) -> AsyncDataOperation<BackendError<RequestAssertionJWTError>, RequestAssertionJWTResult> {
        RequestAssertionJWTAsyncOperation(backendAddress: backendAddress, payload: payload)
    }
}

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}
