import Foundation
import Alamofire

#if DEBUG
import Mocker
#endif

struct RequestChallengeResult: Codable {
    var challenge: String
}

enum RequestChallengeError: RequestError {
    case validationFailed
    case smsGateFailure
    case smsLimitReached
    case ipBlock
}

extension RequestChallengeError {
    var errorDescription: String? {
        switch self {
        case .ipBlock:
            return L10n.IOSApp.Verification.PhoneNumber.IpBlock.message
        case .smsLimitReached:
            return L10n.IOSApp.Verification.PhoneNumber.LimitReached.message
        default:
            return L10n.IOSApp.Verification.PhoneNumber.requestFailure
        }
    }
    var localizedTitle: String {
        switch self {
        case .smsLimitReached:
            return L10n.IOSApp.Verification.PhoneNumber.LimitReached.title
        default:
            return L10n.IOSApp.Navigation.Basic.error
        }
    }
}

class SMSRequestChallengeAsyncDataOperation: AsyncDataOperation<BackendError<RequestChallengeError>, RequestChallengeResult> {

    private let url: URL
    private let parameters: [String: String]
    private var session: Session!

    init(backendAddress: BackendAddress, phoneNumber: String) {

        url = backendAddress.apiUrl
            .appendingPathComponent("sms")
            .appendingPathComponent("request")

        session = LucaAlamofireSessionBuilder.build(pinnedCertificateHost: url.host ?? "", disableCache: true)

        self.parameters = ["phone": phoneNumber]
    }

    override func execute(completion: @escaping (RequestChallengeResult) -> Void, failure: @escaping (BackendError<RequestChallengeError>) -> Void) -> (() -> Void) {
        let headers = standardHeaders.merged(authHeaders)

        let request = session.request(
                url,
                method: .post,
                parameters: parameters,
                encoder: JSONParameterEncoder(encoder: JSONEncoderUnescaped()),
                headers: headers)
            .map(code: 400, to: RequestChallengeError.validationFailed)
            .map(code: 403, to: RequestChallengeError.ipBlock)
            .map(code: 429, to: RequestChallengeError.smsLimitReached)
            .map(code: 503, to: RequestChallengeError.smsGateFailure)

        let startedRequest = request
            .validate(statusCode: 200...299)
            .responseDict { (response) in
                _ = self // capture self

                if let error = response.retrieveBackendError(RequestChallengeError.self) {
                    failure(error)
                    return
                }

                guard let dict = response.retrieveData(failure: failure) else { return }
                guard let challenge = dict["challengeId"] as? String else {
                    failure(BackendError<RequestChallengeError>(networkLayerError: .invalidResponsePayload))
                    return
                }

                let result = RequestChallengeResult(challenge: challenge)
                completion(result)
            }

        return { startedRequest.cancel() }
    }
}
