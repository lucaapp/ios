import Foundation

enum FetchLocationsURLsError: RequestError {
    case notFound
}

extension FetchLocationsURLsError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class FetchLocationsURLsDataAsyncOperation: MappedBackendAsyncDataOperation<KeyValueParameters, [LocationURL], FetchLocationsURLsError> {

    init(backendAddress: BackendAddress, locationId: UUID) {
        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("locations")
            .appendingPathComponent(locationId.uuidString.lowercased())
            .appendingPathComponent("urls")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                errorMappings: [404: .notFound]
            )
        )
    }

    override func map(dict: [String: Any]) throws -> [LocationURL] {
        dict.compactMap { entry -> LocationURL? in
            if let type = LocationURLType(rawValue: entry.key),
               let url = entry.value as? String {
                let id = UUID().uuidString
                return LocationURL(urlId: id, type: type, url: url)
            }
            return nil
        }
    }
}
