import Foundation
import Alamofire

enum SubmitCampaignParticipationError: RequestError {

    case badRequest
    case unauthorized
    case notFound
    case noParticipation
 }

 extension SubmitCampaignParticipationError {

    var errorDescription: String? {
        switch self {
        case .badRequest: return L10n.UserApp.Raffle.Error.Mail.Missing.text
        default: return "\(self)"
        }
    }

    var localizedTitle: String {
        switch self {
        case .badRequest:
            return L10n.UserApp.Raffle.Error.Mail.Missing.title
        default: return L10n.IOSApp.Navigation.Basic.error
        }
    }

 }

struct CampaignSubmissionParams {

    var campaign: String
    var email: String
    var language: String

}

 class SubmitCampaignParticipationAsyncDataOperation: BackendAsyncDataOperation<[String: String], CampaignSubmission, SubmitCampaignParticipationError> {

     init(backendAddress: BackendAddress, paymentId: String, params: CampaignSubmissionParams, accessToken: String) {

         let fullUrl = backendAddress.host
             .appendingPathComponent("pay")
             .appendingPathComponent("api")
             .appendingPathComponent("v1")
             .appendingPathComponent("consumers")
             .appendingPathComponent("payments")
             .appendingPathComponent(paymentId)
             .appendingPathComponent("campaign")

         let parameters: [String: String] = [
            "campaign": params.campaign,
            "email": params.email,
            "language": params.language
         ]
         let header = HTTPHeader.xAuth("\(accessToken)")

         super.init(
            RequestConfig(url: fullUrl,
                          method: .post,
                          parameters: parameters,
                          enableLog: true,
                          errorMappings: [400: .badRequest,
                                          401: .unauthorized,
                                          404: .notFound,
                                          412: .noParticipation],
                         additionalHeaders: [header]))
    }

 }
