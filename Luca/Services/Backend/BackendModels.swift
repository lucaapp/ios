import Foundation
import UIKit

struct HealthDepartment: Codable, Equatable, Hashable {
    var departmentId: String
    var name: String
    var signedPublicHDSKP: String
    var signedPublicHDEKP: String
    var email: String
    var phone: String
    var zipCodes: [String]
}

extension HealthDepartment: DataRepoModel {

    var identifier: Int? {
        get {
            let checksum = departmentId.data(using: .utf8)!.crc32

            return Int(checksum)
        }
        set { }
    }
}

extension HealthDepartment {
    var parsedDepartmentId: UUID? {
        UUID(uuidString: departmentId)
    }
}

enum LocationURLType: String, Codable {
    case map
    case menu
    case schedule
    case general
    case website
    case report
}

extension LocationURLType {
    var localized: String {
        switch self {
        case .map: return L10n.IOSApp.Checkin.Urls.map
        case .menu: return L10n.IOSApp.Checkin.Urls.menu
        case .schedule: return L10n.IOSApp.Checkin.Urls.schedule
        case .general: return L10n.IOSApp.Checkin.Urls.general
        case .website: return L10n.IOSApp.Checkin.Urls.website
        case .report: return L10n.IOSApp.Checkin.Urls.report
        }
    }

    var icon: UIImage {
        switch self {
        case .map: return Asset.locationURLPin.image
        case .menu: return Asset.locationURLMenu.image
        case .schedule: return Asset.locationURLTimetable.image
        case .general: return Asset.locationURLMore.image
        case .website: return Asset.locationURLWebsite.image
        case .report: return Asset.warningTriangle.image
        }
    }
}

struct LocationURL: Codable, Equatable {
    var urlId: String
    var type: LocationURLType
    var url: String
}

struct Location: Codable {
    var locationId: String
    var publicKey: String?
    var groupName: String?
    var locationName: String?
    var firstName: String?
    var lastName: String?
    var phone: String?
    var streetName: String?
    var streetNr: String?
    var zipCode: String?
    var city: String?
    var state: String?
    var lat: Double?
    var lng: Double?
    var radius: Double
    var startsAt: Int?
    var endsAt: Int?
    var isPrivate: Bool?
    var isContactDataMandatory: Bool?
    var entryPolicy: String?
    var urls: [LocationURL]?
    /// in minutes
    var averageCheckinTime: Int?
}

extension Location: DataRepoModel, Hashable {

    var identifier: Int? {
        get {
            let checksum = locationId.data(using: .utf8)!.crc32

            return Int(checksum)
        }
        set { }
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(locationId)
    }

    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.locationId == rhs.locationId
    }
}

extension Location {
    var startsAtDate: Date? {
        if let starts = startsAt {
            return Date(timeIntervalSince1970: Double(starts))
        }
        return nil
    }
    var endsAtDate: Date? {
        if let ends = endsAt {
            return Date(timeIntervalSince1970: Double(ends))
        }
        return nil
    }
}

extension Location {
    var geoLocationRequired: Bool {
        return radius > 0
    }
}

extension Location {
    var formattedName: String? {
        switch (groupName, locationName) {
        case (.some(let groupName), nil):
            return groupName
        case (.some(let groupName), .some(let locationName)):
            return groupName + " - " + locationName
        case (nil, .some(let locationName)):
            return locationName
        default:
            return nil
        }
    }
}

struct UserInfoRetrieval: Codable {
    var retrievedAt: Int
    var department: String
    var employee: String
}

struct TraceInfo: Codable {

    /// Base64 encoded TraceId
    var traceId: String

    var checkin: Int
    var checkout: Int?
    var locationId: String
    var createdAt: Int?
}

extension TraceInfo: DataRepoModel {

    var identifier: Int? {
        get {
            var checksum = Data()
            checksum = traceId.data(using: .utf8)!
            checksum.append(checkin.data)
            return Int(checksum.crc32)
        }
        set { }
    }
}

extension TraceInfo {
    var parsedLocationId: UUID? {
        UUID(uuidString: locationId)
    }
    var checkInDate: Date {
        Date(timeIntervalSince1970: Double(checkin))
    }
    var checkOutDate: Date? {
        if let checkout = checkout {
            return Date(timeIntervalSince1970: Double(checkout))
        }
        return nil
    }
    var createdAtDate: Date? {
        if let createdAt = createdAt {
            return Date(timeIntervalSince1970: Double(createdAt))
        }
        return nil
    }
    var isCheckedIn: Bool {
        checkout == nil || Int(Date.now.timeIntervalSince1970) < checkout!
    }
    var traceIdData: TraceId? {
        if let data = Data(base64Encoded: traceId) {
            return TraceId(data: data, checkIn: checkInDate)
        }
        return nil
    }
}

 extension TraceInfo: Reportable {
    var contentDescription: String {
        return L10n.IOSApp.DataReport.ContactData.traceInfo(
            checkInDate.formattedDateTime,
            checkOutDate?.formattedDateTime ?? "-",
            locationId
        )
    }
 }

struct ScannerInfo: Codable {
    var scannerId: String
    var locationId: String
    var publicKey: String?
    var name: String?
    var tableCount: Int?
}
