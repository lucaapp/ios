import Foundation
import Alamofire
import DeviceKit

typealias KeyValueParameters = [String: String]

/// Contains current app version. Eg `1.2.3`
private let appVersion = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) ?? "unknown version"

/// User agent of luca client
private let userAgent = "luca/iOS \(appVersion)"

private let authorizationCredentials = "\(secrets.backendLogin):\(secrets.backendPassword)".data(using: .utf8)!.base64EncodedString()
private let authorizationContent = "Basic \(authorizationCredentials)"

/// Default headers added to every request in Luca
let standardHeaders: HTTPHeaders = [
    "User-Agent": userAgent
]
let authHeaders: HTTPHeaders = [
    "Authorization": authorizationContent
]

#if AQS
let defaultEnableLog = true
#else
let defaultEnableLog = false
#endif

class LucaAlamofireSessionBuilder {

    static func build(
        pinnedCertificateHost: String?,
        cachePolicy: NSURLRequest.CachePolicy = NSURLRequest.CachePolicy.useProtocolCachePolicy,
        disableCache: Bool = true,
        enableLog: Bool = false) -> Session {

        let configuration = URLSessionConfiguration.af.default

        configuration.requestCachePolicy = cachePolicy
        if disableCache {
            configuration.urlCache = nil
        } else {
            let MB = 1024 * 1024
            configuration.urlCache = URLCache(memoryCapacity: 40 * MB, diskCapacity: 40 * MB, diskPath: "urlCache")
        }
        var trustManager: ServerTrustManager?
        if let pinnedCertificateHost = pinnedCertificateHost {
            trustManager = ServerTrustManager(
                evaluators: [ pinnedCertificateHost: PinnedCertificatesTrustEvaluator() ]
            )
        }
        var eventMonitors: [EventMonitor] = []
        if enableLog {
            eventMonitors.append(AlamofireLogger())
        }
        configuration.timeoutIntervalForRequest = 20
        if let trustManager = trustManager {
            return Session(configuration: configuration, serverTrustManager: trustManager, eventMonitors: eventMonitors)
        }
        return Session(configuration: configuration, eventMonitors: eventMonitors)
    }
}

struct RequestConfig<ParametersType, RequestErrorType> {
    var url: URL
    var method: HTTPMethod
    var parameters: ParametersType?
    var cachePolicy: NSURLRequest.CachePolicy = NSURLRequest.CachePolicy.useProtocolCachePolicy
    var disableCache: Bool = true
    var enableLog: Bool = defaultEnableLog
    var requestModifier: Session.RequestModifier?
    var errorMappings: [Int: RequestErrorType]
    var additionalHeaders: HTTPHeaders = [:]
    var pinToLucaCertificate: Bool = true
    #if PRODUCTION
    var lucaDebugAuth: Bool = false
    #else
    var lucaDebugAuth: Bool = true
    #endif

    var session: Session {
        LucaAlamofireSessionBuilder.build(
            pinnedCertificateHost: pinToLucaCertificate ? url.host : nil,
            cachePolicy: cachePolicy,
            disableCache: disableCache,
            enableLog: enableLog
        )
    }
}

class BackendBinaryAsyncDataOperation<ParametersType, Result, RequestErrorType>: AsyncDataOperation<BackendError<RequestErrorType>, Result> where ParametersType: Encodable,
                                                                     RequestErrorType: RequestError {
    var config: RequestConfig<ParametersType, RequestErrorType>
    private var session: Session!

    init(_ config: RequestConfig<ParametersType, RequestErrorType>) {
        self.config = config
        session = config.session
    }

    override func execute(completion: @escaping (Result) -> Void, failure: @escaping (BackendError<RequestErrorType>) -> Void) -> (() -> Void) {
        var headers = standardHeaders.merged(config.additionalHeaders)
        if config.lucaDebugAuth {
            headers = headers.merged(authHeaders)
        }
        var request = session.request(
            config.url,
            method: config.method,
            parameters: config.parameters,
            encoder: JSONParameterEncoder(encoder: JSONEncoderUnescaped()),
            headers: headers,
            requestModifier: config.requestModifier)

        for errorMapping in config.errorMappings {
            request = request.map(code: errorMapping.key, to: errorMapping.value)
        }

        let startedRequest = request
            .validate(statusCode: 200...299)
            .response(completionHandler: { (response) in

                _ = self

                if let error = response.retrieveBackendError(RequestErrorType.self) {
                    failure(error)
                    return
                }

                guard let data = response.data else {
                    failure(BackendError(networkLayerError: .invalidResponsePayload))
                    return
                }

                do {
                    completion(try self.map(data: data))
                } catch let error {
                    if let requestError = error as? RequestErrorType {
                        failure(BackendError(backendError: requestError))
                    } else {
                        failure(BackendError(networkLayerError: .invalidResponsePayload))
                    }
                }
            })

        return { startedRequest.cancel() }
    }

    func map(data: Data) throws -> Result {
        throw NetworkError.invalidResponsePayload
    }
}

class BackendAsyncDataOperation<ParametersType, Result, RequestErrorType>: AsyncDataOperation<BackendError<RequestErrorType>, Result>
where Result: Decodable,
      ParametersType: Encodable,
      RequestErrorType: RequestError {
    var config: RequestConfig<ParametersType, RequestErrorType>
    private var session: Session!

    init(_ config: RequestConfig<ParametersType, RequestErrorType>) {
        self.config = config
        session = config.session
    }

    override func execute(completion: @escaping (Result) -> Void, failure: @escaping (BackendError<RequestErrorType>) -> Void) -> (() -> Void) {
        var headers = standardHeaders.merged(config.additionalHeaders)
        if config.lucaDebugAuth {
            headers = headers.merged(authHeaders)
        }
        var request = session.request(
            config.url,
            method: config.method,
            parameters: config.parameters,
            encoder: JSONParameterEncoder(encoder: JSONEncoderUnescaped()),
            headers: headers,
            requestModifier: config.requestModifier)

        for errorMapping in config.errorMappings {
            request = request.map(code: errorMapping.key, to: errorMapping.value)
        }

        let startedRequest = request
            .validate(statusCode: 200...299)
            .responseDecodable(of: Result.self, completionHandler: { (response) in

                _ = self

                guard let data = response.retrieveData(failure: failure) else {
                    return
                }
                completion(data)
            })

        return { startedRequest.cancel() }
    }
}

class BackendAsyncOperation<ParametersType, RequestErrorType>: AsyncOperation<BackendError<RequestErrorType>> where ParametersType: Encodable, RequestErrorType: RequestError {

    var config: RequestConfig<ParametersType, RequestErrorType>
    private var session: Session!

    init(_ config: RequestConfig<ParametersType, RequestErrorType>) {
        self.config = config
        session = config.session
    }

    override func execute(completion: @escaping () -> Void, failure: @escaping (BackendError<RequestErrorType>) -> Void) -> (() -> Void) {var headers = standardHeaders.merged(config.additionalHeaders)
        if config.lucaDebugAuth {
            headers = headers.merged(authHeaders)
        }
        var request = session!.request(
            config.url,
            method: config.method,
            parameters: config.parameters,
            encoder: JSONParameterEncoder(encoder: JSONEncoderUnescaped()),
            headers: headers,
            requestModifier: config.requestModifier)

        for errorMapping in config.errorMappings {
            request = request.map(code: errorMapping.key, to: errorMapping.value)
        }

        let startedRequest = request
            .validate(statusCode: 200...299)
            .response { (response) in

                _ = self
                if let error = response.retrieveBackendError(RequestErrorType.self) {
                    failure(error)
                    return
                }

                completion()
            }

        return { startedRequest.cancel() }
    }
}

class MappedBackendAsyncDataOperation<ParametersType, Result, RequestErrorType>: AsyncDataOperation<BackendError<RequestErrorType>, Result> where ParametersType: Encodable,
                                                                     RequestErrorType: RequestError {

    var config: RequestConfig<ParametersType, RequestErrorType>
    private var session: Session!

    init(_ config: RequestConfig<ParametersType, RequestErrorType>) {
        self.config = config
        session = config.session
    }

    override func execute(
        completion: @escaping (Result) -> Void,
        failure: @escaping (BackendError<RequestErrorType>) -> Void) -> (() -> Void
        ) {
        var headers = standardHeaders.merged(config.additionalHeaders)
        if config.lucaDebugAuth {
            headers = headers.merged(authHeaders)
        }
        var request = session.request(
            config.url,
            method: config.method,
            parameters: config.parameters,
            encoder: JSONParameterEncoder(encoder: JSONEncoderUnescaped()),
            headers: headers,
            requestModifier: config.requestModifier)

        for errorMapping in config.errorMappings {
            request = request.map(code: errorMapping.key, to: errorMapping.value)
        }

        let startedRequest = request
            .validate(statusCode: 200...299)
            .responseDict { (response) in

                _ = self

                guard let dict = response.retrieveData(failure: failure) else {
                    return
                }
                do {
                    completion(try self.map(dict: dict))
                } catch let error as NetworkError {
                    failure(BackendError<RequestErrorType>(networkLayerError: error, backendError: nil))
                } catch let error {
                    failure(BackendError<RequestErrorType>(networkLayerError: NetworkError.unknown(error: error), backendError: nil))
                }
            }

        return { startedRequest.cancel() }
    }

    func map(dict: [String: Any]) throws -> Result {
        throw NetworkError.invalidResponsePayload
    }
}

extension HTTPHeaders {
    func merged(_ with: HTTPHeaders, overwrite: Bool = false) -> HTTPHeaders {
        var d = dictionary
        d.merge(with.dictionary, uniquingKeysWith: {
            if overwrite {
                return $1
            }
            return $0
        })
        return HTTPHeaders(d)
    }
}

extension HTTPHeader {
    static func xAuth(_ content: String) -> HTTPHeader {
        HTTPHeader(name: "X-Auth", value: content)
    }
    static func xDelay(_ delay: TimeInterval) -> HTTPHeader {
        HTTPHeader(name: "X-Delay", value: "\(delay * 1000.0)")
    }
    static func xBypassRatelimit() -> HTTPHeader {
        HTTPHeader(name: "X-Rate-limit-Bypass", value: "1")
    }
    static func xMockIdent() -> HTTPHeader {
        HTTPHeader(name: "X-Mock", value: "1")
    }
}
