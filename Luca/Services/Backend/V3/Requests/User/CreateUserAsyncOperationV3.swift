import Foundation

enum CreateUserError: RequestError {
    case invalidInput
    case invalidSignature
    case userAlreadyExists
    case unableToBuildUserData(error: Error)
}

extension CreateUserError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class CreateUserAsyncOperationV3: MappedBackendAsyncDataOperation<UserDataPackageV3, UUID, CreateUserError> {

    private var build: ()throws->UserDataPackageV3

    init(backendAddress: BackendAddressV3,
         builder: UserDataPackageBuilderV3,
         data: UserRegistrationData) {

        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("users")

        build = { try builder.build(userData: data) }

        #if PRODUCTION
        let enableLog = false
        #else
        let enableLog = true
        #endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                enableLog: enableLog,
                errorMappings: [
                    400: .invalidInput,
                    403: .invalidSignature,
                    409: .userAlreadyExists
                ]
            )
        )
    }

    override func execute(completion: @escaping (UUID) -> Void, failure: @escaping (BackendError<CreateUserError>) -> Void) -> (() -> Void) {

        do {
            config.parameters = try build()
        } catch let error {
            failure(BackendError(backendError: .unableToBuildUserData(error: error)))
            return {}
        }

        return super.execute(completion: completion, failure: failure)
    }

    override func map(dict: [String: Any]) throws -> UUID {

        if let userIdString = dict["userId"] as? String,
           let userId = UUID(uuidString: userIdString) {

            return userId

        } else {
            throw NetworkError.invalidResponsePayload
        }
    }

}
