import Foundation

enum DeleteUserError: RequestError {

    case badInput
    case invalidSignature
    case userNotFound
    case rateLimit
    case alreadyDeleted
    case unableToBuildPayload

}
extension DeleteUserError {

    var errorDescription: String? {
        switch self {
        case .badInput: return L10n.IOSApp.Error.DeleteUser.badInput
        case .invalidSignature: return L10n.IOSApp.Error.DeleteUser.invalidSignature
        case .userNotFound: return L10n.IOSApp.Error.DeleteUser.userNotFound
        case .rateLimit: return L10n.IOSApp.Error.DeleteUser.rateLimit
        case .alreadyDeleted: return L10n.IOSApp.Error.DeleteUser.alreadyDeleted
        case .unableToBuildPayload: return L10n.IOSApp.Error.DeleteUser.unableToBuildPayload
        }
    }

    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class DeleteUserAsyncOperationV3: BackendAsyncOperation<KeyValueParameters, DeleteUserError> {

    private var build: ()throws->Data

    init(backendAddress: BackendAddress,
         userId: UUID,
         builder: UserDataPackageBuilderV3) {
        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("users")
            .appendingPathComponent(userId.uuidString.lowercased())

        build = {
            let bytes = Data(userId.bytes)

            if let payload = "DELETE_USER".data(using: .utf8) {
                var data = payload
                data.append(bytes)
                data = try builder.signature.sign(data: data)
                return data
            }
            throw DeleteUserError.unableToBuildPayload
        }

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .delete,
                errorMappings: [400: .badInput,
                                403: .invalidSignature,
                                404: .userNotFound,
                                410: .alreadyDeleted,
                                429: .rateLimit]
            )
        )
    }

    override func execute(completion: @escaping () -> Void, failure: @escaping (BackendError<DeleteUserError>) -> Void) -> (() -> Void) {

        do {
            config.parameters = ["signature": (try build()).base64EncodedString()]
        } catch {
            failure(BackendError(backendError: .unableToBuildPayload))
            return {}
        }

        return super.execute(completion: completion, failure: failure)
    }

}
