import Foundation

enum UserTransferError: RequestError {
    case invalidInput
    case invalidSignature
    case notFound
    case unableToBuildUserTransferData(error: Error)
}

extension UserTransferError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class UserTransferAsyncOperationV3: MappedBackendAsyncDataOperation<UserTransferPayload, String, UserTransferError> {

    init(backendAddress: BackendAddressV3, payload: UserTransferPayload) {

        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("userTransfers")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                parameters: payload,
                errorMappings: [400: .invalidInput,
                                403: .invalidSignature,
                                404: .notFound]
            )
        )
    }

    override func map(dict: [String: Any]) throws -> String {
        if let tan = dict["tan"] as? String {
            return tan
        }
        throw NetworkError.invalidResponsePayload
    }
}
