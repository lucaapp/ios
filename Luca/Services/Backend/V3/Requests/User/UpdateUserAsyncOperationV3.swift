import UIKit

enum UpdateUserError: RequestError {
    case invalidInput
    case invalidSignature
    case notFound
    case unableToBuildUserData(error: Error)
}

extension UpdateUserError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class UpdateUserAsyncOperationV3: BackendAsyncOperation<UserDataPackageV3, UpdateUserError> {

    private var build: ()throws->UserDataPackageV3

    init(backendAddress: BackendAddressV3, builder: UserDataPackageBuilderV3, data: UserRegistrationData, userId: UUID) {

        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("users")
            .appendingPathComponent(userId.uuidString.lowercased())

        build = { try builder.build(userData: data, withPublicKey: false) }

        #if PRODUCTION
        let enableLog = false
        #else
        let enableLog = true
        #endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .patch,
                enableLog: enableLog,
                errorMappings: [
                    400: .invalidInput,
                    403: .invalidSignature,
                    404: .notFound
                ]
            )
        )
    }

    override func execute(completion: @escaping () -> Void, failure: @escaping (BackendError<UpdateUserError>) -> Void) -> (() -> Void) {

        do {
            config.parameters = try build()
        } catch let error {
            failure(BackendError(backendError: .unableToBuildUserData(error: error)))
            return {}
        }

        return super.execute(completion: completion, failure: failure)
    }

}
