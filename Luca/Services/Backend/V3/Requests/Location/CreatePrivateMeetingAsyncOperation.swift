import Foundation
import Security

enum CreatePrivateMeetingError: RequestError {
    case invalidKey
    case invalidInput
}

extension CreatePrivateMeetingError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class CreatePrivateMeetingAsyncOperation: BackendAsyncDataOperation<KeyValueParameters, PrivateMeetingIds, CreatePrivateMeetingError> {

    private var build: ()throws->Data

    init(backendAddress: BackendAddressV3, publicKey: SecKey) {

        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("locations")
            .appendingPathComponent("private")

        build = { try publicKey.toData() }

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                errorMappings: [400: .invalidInput]
            )
        )
    }

    override func execute(completion: @escaping (PrivateMeetingIds) -> Void, failure: @escaping (BackendError<CreatePrivateMeetingError>) -> Void) -> (() -> Void) {
        do {
            config.parameters = ["publicKey": (try build()).base64EncodedString()]
        } catch {
            failure(BackendError(backendError: .invalidKey))
            return {}
        }
        return super.execute(completion: completion, failure: failure)
    }
}
