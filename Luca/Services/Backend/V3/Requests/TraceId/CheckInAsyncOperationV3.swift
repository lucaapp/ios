import Foundation

enum CheckInError: RequestError {
    case invalidInput
    case notFound
    case timeMismatch
    case unableToBuildCheckInPayload(error: Error)
}

extension CheckInError {
    var errorDescription: String? {

        switch self {
        case .timeMismatch:
            return "Check-in timestamp must be greater than the meeting start timestamp. Please wait a minute and try again."
        default:
            return "\(self)"
        }
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class CheckInAsyncOperationV3: BackendAsyncOperation<CheckInPayloadV3, CheckInError> {

    private var build: ()throws->CheckInPayloadV3

    init(backendAddress: BackendAddressV3,
         checkInBuilder: CheckInPayloadBuilderV3,
         qrCodePayload: QRCodePayloadV4,
         venuePubKey: KeySource,
         scannerId: String,
         anonymous: Bool = false) {

        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("traces")
            .appendingPathComponent("checkin")

        self.build = { try checkInBuilder.build(qrCode: qrCodePayload, venuePublicKey: venuePubKey, scannerId: scannerId, anonymous: anonymous) }

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                errorMappings: [400: .invalidInput,
                                404: .notFound,
                                409: .timeMismatch]
            )
        )
    }

    override func execute(completion: @escaping () -> Void, failure: @escaping (BackendError<CheckInError>) -> Void) -> (() -> Void) {
        do {
            config.parameters = try build()
        } catch let error {
            failure(BackendError(backendError: .unableToBuildCheckInPayload(error: error)))
            return {}
        }
        return super.execute(completion: completion, failure: failure)
    }
}
