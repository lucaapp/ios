import Foundation

enum UploadAdditionalDataError: RequestError {
    case notFound
    case invalidInput
    case failedToBuildAdditionalDataPayload(error: Error)
}

extension UploadAdditionalDataError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class UploadAdditionalDataRequestAsyncOperationV3<T>: BackendAsyncOperation<TraceIdAdditionalDataPayloadV3, UploadAdditionalDataError> where T: Encodable {

    private var build: ()throws->TraceIdAdditionalDataPayloadV3

    init(backendAddress: BackendAddressV3,
         additionalDataBuilder: TraceIdAdditionalDataBuilderV3,
         traceId: TraceId,
         scannerId: String,
         venuePubKey: KeySource,
         additionalData: T) {

        build = { try additionalDataBuilder.build(
            traceId: traceId,
            scannerId: scannerId,
            venuePubKey: venuePubKey,
            additionalData: additionalData
        )}

        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("traces")
            .appendingPathComponent("additionalData")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                errorMappings: [400: .invalidInput,
                                404: .notFound]
            )
        )
    }

    override func execute(completion: @escaping () -> Void, failure: @escaping (BackendError<UploadAdditionalDataError>) -> Void) -> (() -> Void) {
        do {
            config.parameters = try build()
        } catch let error {
            failure(BackendError(backendError: .failedToBuildAdditionalDataPayload(error: error)))
            return {}
        }
        return super.execute(completion: completion, failure: failure)
    }
}
