import Foundation
import Alamofire

enum CheckOutError: RequestError {
    case invalidInput
    case invalidSignature
    case notFound
    case checkInTimeLargerThanCheckOutTime
    case failedToBuildCheckOutPayload(error: Error)
}

extension CheckOutError {
    var errorDescription: String? {
        switch self {
        case .checkInTimeLargerThanCheckOutTime: return L10n.IOSApp.LocationCheckinViewController.CheckOutFailed.LowDuration.message
        default: return "\(self)"
        }
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class CheckOutAsyncOperationV3: BackendAsyncOperation<CheckOutPayloadV3, CheckOutError> {

    private var build: () throws->CheckOutPayloadV3

    init(backendAddress: BackendAddressV3,
         checkOutBuilder: CheckOutPayloadBuilderV3,
         traceId: TraceId,
         timestamp: Date) {

        build = {try checkOutBuilder.build(traceId: traceId, checkOutDate: timestamp)}

        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("traces")
            .appendingPathComponent("checkout")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                requestModifier: { $0.timeoutInterval = 10 },
                errorMappings: [400: .invalidInput,
                                403: .invalidSignature,
                                404: .notFound,
                                409: .checkInTimeLargerThanCheckOutTime]
            )
        )
    }

    override func execute(completion: @escaping () -> Void, failure: @escaping (BackendError<CheckOutError>) -> Void) -> (() -> Void) {
        do {
            self.config.parameters = try build()
        } catch let error {
            failure(BackendError(backendError: .failedToBuildCheckOutPayload(error: error)))
            return {}
        }
        return super.execute(completion: completion, failure: failure)
    }
}
