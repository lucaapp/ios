import Foundation

enum RedeemDocumentError: RequestError {

    case alreadyRedeemed
    case rateLimitReached

}

extension RedeemDocumentError {

    var errorDescription: String? {
        switch self {
        case .alreadyRedeemed: return L10n.IOSApp.Test.Uniqueness.Redeemed.error
        case .rateLimitReached: return L10n.IOSApp.Test.Uniqueness.Rate.Limit.error
        }
    }

    var localizedTitle: String {
        return L10n.IOSApp.Test.Result.Error.title
    }

}

struct RedeemDocumentParams: Codable {
    var hash: String
    var tag: String
}

class RedeemDocumentAsyncOperation: BackendAsyncOperation<RedeemDocumentParams, RedeemDocumentError> {
    init(backendAddress: BackendAddress, hash: Data, tag: Data) {
        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("tests")
            .appendingPathComponent("redeem")

        #if PRODUCTION
        let enableLog = false
        #else
        let enableLog = true
        #endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                parameters: RedeemDocumentParams(
                    hash: hash.base64EncodedString(),
                    tag: tag.base64EncodedString()
                ),
                enableLog: enableLog,
                errorMappings: [
                    409: .alreadyRedeemed,
                    429: .rateLimitReached
                ]
            )
        )
    }
}
