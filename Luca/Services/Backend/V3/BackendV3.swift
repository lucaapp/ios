import Foundation
import Alamofire
import DependencyInjection

class BackendAddressV3: BackendAddress {

    var host: URL
    var dataHost: URL
    let apiUrl: URL
    let dataUrl: URL

    var privacyPolicyUrl: URL? = URL(string: "https://www.luca-app.de/app-privacy-policy/")

    init(version: String) {
        host = BackendAddressV3.hostForCurrentScheme()
        dataHost = BackendAddressV3.dataHostForCurrentScheme()
        apiUrl = host.appendingPathComponent("api").appendingPathComponent(version)
        dataUrl = dataHost.appendingPathComponent("api").appendingPathComponent(version)
    }

    convenience init() {
        self.init(version: "v3")
    }

    private static func hostForCurrentScheme() -> URL {
 #if DEVELOPMENT
        return URL(string: "https://app-dev.luca-app.de")!
 #elseif QA
        return URL(string: "https://app-qs.luca-app.de")!
 #elseif PENTEST
        return URL(string: "https://app-pentest.luca-app.de")!
 #elseif PREPROD
        return URL(string: "https://app-preprod.luca-app.de")!
 #elseif RELEASE
        return URL(string: "https://app-release.luca-app.de")!
 #elseif HOTFIX
        return URL(string: "https://app-hotfix.luca-app.de")!
 #elseif AQS
        return URL(string: "https://app-aqs.luca-app.de")!
 #elseif P1
        return URL(string: "https://app-p1.luca-app.de")!
 #elseif P2
        return URL(string: "https://app-p2.luca-app.de")!
 #elseif P3
        return URL(string: "https://app-p3.luca-app.de")!
 #elseif DEMO
        return URL(string: "https://app-demo.luca-app.de")!
 #else
        return URL(string: "https://app.luca-app.de")!
 #endif
    }

    private static func dataHostForCurrentScheme() -> URL {
 #if DEVELOPMENT
        return URL(string: "https://data-dev.luca-app.de")!
 #elseif QA
        return URL(string: "https://data-qs.luca-app.de")!
 #elseif PENTEST
        return URL(string: "https://data-pentest.luca-app.de")!
 #elseif PREPROD
        return URL(string: "https://data-preprod.luca-app.de")!
 #elseif RELEASE
        return URL(string: "https://data-release.luca-app.de")!
 #elseif HOTFIX
        return URL(string: "https://data-hotfix.luca-app.de")!
 #elseif AQS
        return URL(string: "https://data-aqs.luca-app.de")!
 #elseif P1
        return URL(string: "https://data-p1.luca-app.de")!
 #elseif P2
        return URL(string: "https://data-p2.luca-app.de")!
 #elseif P3
        return URL(string: "https://data-p3.luca-app.de")!
 #elseif DEMO
        return URL(string: "https://data-demo.luca-app.de")!
 #else
        return URL(string: "https://data.luca-app.de")!
 #endif
    }
}

class DefaultBackendDailyKeyV3: BackendDailyKeyV3 {

    @InjectStatic(\.backendAddressV3) private var backendAddress: BackendAddressV3

    func retrieveDailyPubKey() -> AsyncDataOperation<BackendError<RetrieveDailyKeyError>, PublicKeyFetchResultV3> {
        RetrieveDailyKeyAsyncOperationV3(backendAddress: backendAddress)
    }

    func retrievePubKey(keyId: Int) -> AsyncDataOperation<BackendError<RetrieveDailyKeyError>, PublicKeyFetchResultV3> {
        RetrieveKeyAsyncOperationV3(backendAddress: backendAddress, keyId: keyId)
    }

    func retrieveIssuerKeys(issuerId: String) -> AsyncDataOperation<BackendError<RetrieveDailyKeyError>, IssuerKeysFetchResultV3> {
        RetrieveIssuerKeysAsyncOperationV3(backendAddress: backendAddress, issuerId: issuerId)
    }
}

class BackendUserV3: BackendUserV3Protocol {

    @InjectStatic(\.backendAddressV3) private var backendAddress: BackendAddressV3
    @InjectStatic(\.userDataPackageBuilderV3) private var userDataBuilder: UserDataPackageBuilderV3

    func create(userData: UserRegistrationData) -> AsyncDataOperation<BackendError<CreateUserError>, UUID> {
        CreateUserAsyncOperationV3(backendAddress: backendAddress, builder: userDataBuilder, data: userData)
    }

    func update(userId: UUID, userData: UserRegistrationData) -> AsyncOperation<BackendError<UpdateUserError>> {
        UpdateUserAsyncOperationV3(backendAddress: backendAddress, builder: userDataBuilder, data: userData, userId: userId)
    }

    func userTransfer(payload: UserTransferPayload) -> AsyncDataOperation<BackendError<UserTransferError>, String> {
        UserTransferAsyncOperationV3(backendAddress: backendAddress, payload: payload)
    }

    func delete(userId: UUID) -> AsyncOperation<BackendError<DeleteUserError>> {
        DeleteUserAsyncOperationV3(backendAddress: backendAddress, userId: userId, builder: userDataBuilder)
    }

}

class BackendTraceIdV3: BackendTraceIdV3Protocol {

    @InjectStatic(\.backendAddressV3) private var backendAddress: BackendAddressV3
    @InjectStatic(\.checkInPayloadBuilderV3) private var checkInBuilder: CheckInPayloadBuilderV3
    @InjectStatic(\.checkOutPayloadBuilderV3) private var checkOutBuilder: CheckOutPayloadBuilderV3
    @InjectStatic(\.traceIdAdditionalBuilderV3) private var additionalDataBuilder: TraceIdAdditionalDataBuilderV3

    func checkIn(qrCode: QRCodePayloadV4, venuePubKey: KeySource, scannerId: String, anonymous: Bool) -> AsyncOperation<BackendError<CheckInError>> {
        CheckInAsyncOperationV3(
            backendAddress: backendAddress,
            checkInBuilder: checkInBuilder,
            qrCodePayload: qrCode,
            venuePubKey: venuePubKey,
            scannerId: scannerId,
            anonymous: anonymous)
    }

    func fetchInfo(traceId: TraceId) -> AsyncDataOperation<BackendError<FetchTraceInfoError>, TraceInfo> {
        FetchTraceInfoRequestAsyncOperation(backendAddress: backendAddress, traceId: traceId)
    }

    func fetchInfo(traceIds: [TraceId]) -> AsyncDataOperation<BackendError<FetchTraceInfoError>, [TraceInfo]> {
        FetchTraceInfosRequestAsyncOperation(backendAddress: backendAddress, traceIds: traceIds)
    }

    func checkOut(traceId: TraceId, timestamp: Date) -> AsyncOperation<BackendError<CheckOutError>> {
        CheckOutAsyncOperationV3(backendAddress: backendAddress, checkOutBuilder: checkOutBuilder, traceId: traceId, timestamp: timestamp)
    }

    func updateAdditionalData<T>(traceId: TraceId, scannerId: String, venuePubKey: KeySource, additionalData: T) -> AsyncOperation<BackendError<UploadAdditionalDataError>> where T: Encodable {

        UploadAdditionalDataRequestAsyncOperationV3(
            backendAddress: backendAddress,
            additionalDataBuilder: additionalDataBuilder,
            traceId: traceId,
            scannerId: scannerId,
            venuePubKey: venuePubKey,
            additionalData: additionalData)
    }
}

class BackendLocationV3: BackendLocation {

    @InjectStatic(\.backendAddressV3) private var backendAddress: BackendAddressV3

    func fetchLocation(locationId: UUID) -> AsyncDataOperation<BackendError<FetchLocationError>, Location> {
        FetchLocationAsyncOperation(backendAddress: backendAddress, locationId: locationId)
    }

    func fetchLocationsURLs(locationId: UUID) -> AsyncDataOperation<BackendError<FetchLocationsURLsError>, [LocationURL]> {
        FetchLocationsURLsDataAsyncOperation(backendAddress: backendAddress, locationId: locationId)
    }

    func createPrivateMeeting(publicKey: SecKey) -> AsyncDataOperation<BackendError<CreatePrivateMeetingError>, PrivateMeetingIds> {
        CreatePrivateMeetingAsyncOperation(backendAddress: backendAddress, publicKey: publicKey)
    }

    func fetchLocationGuests(accessId: String) -> AsyncDataOperation<BackendError<FetchLocationGuestsError>, [PrivateMeetingGuest]> {
        FetchGuestListAsyncOperation(backendAddress: backendAddress, accessId: accessId)
    }

    func deletePrivateMeeting(accessId: String) -> AsyncOperation<BackendError<DeletePrivateMeetingError>> {
        DeletePrivateMeetingAsyncOperation(backendAddress: backendAddress, accessId: accessId)
    }
}
