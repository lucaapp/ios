import Foundation

enum NetworkError: LocalizedTitledError, Equatable {

    case noInternet
    case timeout
    case invalidResponse
    case invalidResponsePayload
    case invalidRequest
    case badGateway
    case upgradeRequired

    /// Used when parser couldn't find required key
    case keyNotFound(key: String)

    case certificateValidationFailed

    case unknown(error: Error)

    static func == (lhs: NetworkError, rhs: NetworkError) -> Bool {
        switch (lhs, rhs) {
        case (.noInternet, .noInternet): return true
        case (.timeout, .timeout): return true
        case (.invalidResponse, .invalidResponse): return true
        case (.invalidResponsePayload, .invalidResponsePayload): return true
        case (.invalidRequest, .invalidRequest): return true
        case (.badGateway, .badGateway): return true
        case (.upgradeRequired, .upgradeRequired): return true
        case (.certificateValidationFailed, .certificateValidationFailed): return true
        case (.keyNotFound(let l), .keyNotFound(let r)): return l == r
        default: return false
        }
    }
}

extension NetworkError {

    var localizedTitle: String {
        switch self {
        case .certificateValidationFailed:
            return L10n.IOSApp.General.Failure.InvalidCertificate.title
        case .noInternet:
            return L10n.IOSApp.General.Failure.NoInternet.title
        default:
            return L10n.IOSApp.Navigation.Basic.error
        }
    }

    var errorDescription: String? {
        switch self {
        case .noInternet:
            return L10n.IOSApp.General.Failure.NoInternet.message
        case .certificateValidationFailed:
            return L10n.IOSApp.General.Failure.InvalidCertificate.message
        case .badGateway:
            return L10n.IOSApp.Error.Network.badGateway
        case .upgradeRequired:
            return L10n.IOSApp.VersionSupportChecker.failureMessage
        case .timeout:
            return L10n.UserApp.Error.Request.timeout
        default:
            return "\(self)"
        }
    }
}
