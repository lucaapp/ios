import Foundation

enum RegisterAttestationError: RequestError {
    case invalidRequestBody
    case nonceInvalid
    case attestationInvalid
    case rateLimitReached
    case unknownError
}

extension RegisterAttestationError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

struct RegisterAttestationRequest: Codable {
    var nonce: String
    var attestation: String
    var keyIdentifier: String
}

struct RegisterAttestationResult: Codable {
    var deviceId: String
}

class RegisterAttestationAsyncOperation: BackendAsyncDataOperation<RegisterAttestationRequest, RegisterAttestationResult, RegisterAttestationError> {
    init(backendAddress: BackendAddress, payload: RegisterAttestationRequest) {
        let fullUrl = backendAddress.host
                    .appendingPathComponent("attestation")
                    .appendingPathComponent("api")
                    .appendingPathComponent("v1")
                    .appendingPathComponent("devices")
                    .appendingPathComponent("ios")
                    .appendingPathComponent("register")

        #if PRODUCTION
        let enableLog = false
        #else
        let enableLog = true
        #endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                parameters: payload,
                enableLog: enableLog,
                errorMappings: [400: .invalidRequestBody,
                                401: .nonceInvalid,
                                403: .attestationInvalid,
                                409: .rateLimitReached,
                                500: .unknownError]
            )
        )
    }
}
