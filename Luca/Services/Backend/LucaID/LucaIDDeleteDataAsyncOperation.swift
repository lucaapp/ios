import Foundation
import Alamofire

enum LucaIDDeleteDataError: RequestError {
    case unauthorized
    case identDoesNotExist
    case ratelimitReached
}

extension LucaIDDeleteDataError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class LucaIDDeleteDataAsyncOperation: BackendAsyncOperation<KeyValueParameters, LucaIDDeleteDataError> {
    init(backendAddress: BackendAddress, attestationJWT: String) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("id")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("ident")
            .appendingPathComponent("data")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .delete,
                enableLog: true,
                errorMappings: [
                    401: .unauthorized,
                    404: .identDoesNotExist,
                    429: .ratelimitReached
                ],
                additionalHeaders: [
                    HTTPHeader.xAuth(attestationJWT),
                    HTTPHeader.xBypassRatelimit()
                ]
            )
        )
    }
}
