import Foundation

enum RequestAssertionJWTError: RequestError {
    case invalidRequestBody
    case nonceInvalid
    case assertionInvalid
    case deviceIDNotFound
    case rateLimitReached
    case unknownError
}

extension RequestAssertionJWTError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

struct RequestAssertionJWTRequest: Codable {
    var deviceId: String
    var nonce: String
    var assertion: String
}

struct RequestAssertionJWTResult: Codable {
    var jwt: String
}

class RequestAssertionJWTAsyncOperation: BackendAsyncDataOperation<RequestAssertionJWTRequest, RequestAssertionJWTResult, RequestAssertionJWTError> {
    init(backendAddress: BackendAddress, payload: RequestAssertionJWTRequest) {
        let fullUrl = backendAddress.host
                    .appendingPathComponent("attestation")
                    .appendingPathComponent("api")
                    .appendingPathComponent("v1")
                    .appendingPathComponent("devices")
                    .appendingPathComponent("ios")
                    .appendingPathComponent("assert")

        #if PRODUCTION
        let enableLog = false
        #else
        let enableLog = true
        #endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                parameters: payload,
                enableLog: enableLog,
                errorMappings: [400: .invalidRequestBody,
                                401: .nonceInvalid,
                                403: .assertionInvalid,
                                404: .deviceIDNotFound,
                                409: .rateLimitReached,
                                500: .unknownError]
            )
        )
    }
}
