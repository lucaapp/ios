import Foundation
import Alamofire

enum LucaIDFetchStateError: RequestError {
    case unauthorized // (JWT missing or invalid)
    case ratelimitReached
    case identDoesNotExistYet
}

extension LucaIDFetchStateError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

enum IdentProcessState: String, Codable, Equatable {
    case queued = "QUEUED" // just created, waiting for IdentID from IDnow
    case pending = "PENDING" // IdentId exists and Ident process can start
    case failed = "FAILED" // Ident process failed
    case success = "SUCCESS" // Ident process succeeded
}

struct LucaIDProcessData: Codable, Equatable {
    var valueFace: String
    var valueIdentity: String
    var valueMinimalIdentity: String
}

struct LucaIDProcess: Codable, Equatable {
    var state: IdentProcessState
    @available(*, deprecated, message: "use autoIdentId from receiptJWS instead")
    var identId: String? // IdentID from IDnow
    var data: LucaIDProcessData? // encrypted VC data
    var revocationCode: String // revocation code to delete data without phone (not final format!)
    var receiptJWS: String?
}

class LucaIDFetchStateAsyncDataOperation: BackendAsyncDataOperation<KeyValueParameters, LucaIDProcess, LucaIDFetchStateError> {
    init(backendAddress: BackendAddress, attestationJWT: String) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("id")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("ident")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                enableLog: true,
                errorMappings: [
                    401: .unauthorized,
                    404: .identDoesNotExistYet,
                    429: .ratelimitReached
                ],
                additionalHeaders: [
                    HTTPHeader.xAuth(attestationJWT),
                    HTTPHeader.xBypassRatelimit()
                ]
            )
        )
    }
}
