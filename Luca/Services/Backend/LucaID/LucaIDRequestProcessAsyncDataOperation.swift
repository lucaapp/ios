import Foundation
import Alamofire

struct LucaIDRequestProcessRequest: Codable {
    var encPublicKey: String
    var idPublicKey: String
    var assertion: String
    var nonce: String
}

enum LucaIDRequestProcessAsyncDataOperationError: RequestError {
    case unauthorized // (JWT missing or invalid)
    case ratelimitReached
    case queueFull
}

extension LucaIDRequestProcessAsyncDataOperationError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

struct LucaIDRequestProcessResult: Codable {
    var waitForMs: Double // estimated time in ms until ident will be scheduled
}

class LucaIDRequestProcessAsyncDataOperation: BackendAsyncDataOperation<LucaIDRequestProcessRequest, LucaIDRequestProcessResult, LucaIDRequestProcessAsyncDataOperationError> {

    init(backendAddress: BackendAddress, payload: LucaIDRequestProcessRequest, assertionJWT: String, mock: Bool = false) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("id")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("ident")

        var headers: HTTPHeaders = [
            HTTPHeader.xAuth(assertionJWT),
            HTTPHeader.xBypassRatelimit()
        ]

        if mock {
            headers.add(HTTPHeader.xMockIdent())
            headers.add(HTTPHeader.xDelay(30))
        }

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                parameters: payload,
                enableLog: true,
                errorMappings: [
                    401: .unauthorized,
                    429: .ratelimitReached,
                    503: .queueFull
                ],
                additionalHeaders: headers
            )
        )
    }
}
