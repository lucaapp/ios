import Foundation
import Alamofire

enum LucaIDArchiveDataError: RequestError {
    case unauthorized
    case identDoesNotExist
    case ratelimitReached
    case identIsQueued
    case IDnowUnavailable
}

extension LucaIDArchiveDataError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class LucaIDArchiveDataAsyncOperation: BackendAsyncOperation<KeyValueParameters, LucaIDArchiveDataError> {
    init(backendAddress: BackendAddress, attestationJWT: String) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("id")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("ident")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .delete,
                enableLog: true,
                errorMappings: [
                    401: .unauthorized,
                    404: .identDoesNotExist,
                    403: .identIsQueued,
                    429: .ratelimitReached,
                    503: .IDnowUnavailable
                ],
                additionalHeaders: [
                    HTTPHeader.xAuth(attestationJWT),
                    HTTPHeader.xBypassRatelimit()
                ]
            )
        )
    }
}
