import Foundation

enum RequestAttestationNonceError: RequestError {
    case rateLimitReached
    case unknownError
}

extension RequestAttestationNonceError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

struct RequestAttestationNonceResult: Codable {
    var nonce: String
}

class RequestAttestationNonceAsyncOperation: BackendAsyncDataOperation<KeyValueParameters, RequestAttestationNonceResult, RequestAttestationNonceError> {
    init(backendAddress: BackendAddress) {
        let fullUrl = backendAddress.host
                    .appendingPathComponent("attestation")
                    .appendingPathComponent("api")
                    .appendingPathComponent("v1")
                    .appendingPathComponent("nonce")
                    .appendingPathComponent("request")

        #if PRODUCTION
        let enableLog = false
        #else
        let enableLog = true
        #endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                enableLog: enableLog,
                errorMappings: [409: .rateLimitReached,
                                500: .unknownError]
            )
        )
    }
}
