import Foundation

enum FetchAccessedTracesArchivedChunkError {
    case notFound
    case invalidInput
}

extension FetchAccessedTracesArchivedChunkError: RequestError {
    var localizedTitle: String {
        L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        return "\(self)" // This won't be localized as this error is not supposed to be printed for the user. notFound in this case means that there is no more data to download.
    }
}

class FetchAccessedTracesArchivedChunkAsyncDataOperation: BackendBinaryAsyncDataOperation<KeyValueParameters, AccessedTracesDataChunk, FetchAccessedTracesArchivedChunkError> {

    init(backendAddress: BackendAddressV4, chunkId: Data) {
        let fullUrl = backendAddress.dataUrl
            .appendingPathComponent("notifications")
            .appendingPathComponent("traces")
            .appendingPathComponent("hex")
            .appendingPathComponent(chunkId.hexString)

        #if PRODUCTION
        let enableLog = false
        #else
        let enableLog = true
        #endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                enableLog: enableLog,
                errorMappings: [
                    404: .notFound,
                    400: .invalidInput]
            )
        )
    }

    override func map(data: Data) throws -> AccessedTracesDataChunk {
        try AccessedTracesDataChunk(data: data)
    }
}
