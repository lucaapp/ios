import Foundation

enum StagedFeatureType: String {
    case lucaId = "luca-id"
    case unknown
}
extension StagedFeatureType: Codable {
    public init(from decoder: Decoder) throws {
        self = try StagedFeatureType(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .unknown
    }
}

struct StagedRollout: Codable {
    var name: StagedFeatureType
    var percentage: Float
    var gambledValue: Float?

    var isEnabled: Bool {
        percentage != 0 &&
        (percentage == 1 ||
        gambledValue ?? 0 <  percentage)
    }
}

enum FetchStagedRolloutsError {
    case rateLimitReached
}

extension FetchStagedRolloutsError: RequestError {
    var localizedTitle: String {
        L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        return "\(self)"
    }
}

class FetchStagedRolloutsAsyncDataOperation: BackendAsyncDataOperation<KeyValueParameters, [StagedRollout], FetchStagedRolloutsError> {

    init(backendAddress: BackendAddressV4) {
        let fullUrl = backendAddress.dataUrl
            .appendingPathComponent("features")

        #if PRODUCTION
        let enableLog = false
        #else
        let enableLog = true
        #endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                enableLog: enableLog,
                errorMappings: [
                    429: .rateLimitReached]
            )
        )
    }
}
