import Foundation
import SwiftyJSON

struct Messages: Codable, Hashable {
    var message: String?
    var shortMessage: String?
    var banner: String?
    var title: String?
}

extension Messages {
    func update(with source: Messages) -> Messages {
        Messages(
            message: source.message ?? message,
            shortMessage: source.shortMessage ?? shortMessage,
            banner: source.banner ?? banner,
            title: source.title ?? title
        )
    }

    func replacePlaceholders(with hd: NotificationHealthDepartment) -> Messages {
        let formattedMessage =  message?.replacingOccurrences(of: "((name))", with: hd.name ?? "-")
            .replacingOccurrences(of: "((email))", with: hd.email ?? "-")
            .replacingOccurrences(of: "((phone))", with: hd.phone ?? "-")

        return Messages(
            message: formattedMessage,
            shortMessage: shortMessage,
            banner: banner,
            title: title
        )
    }
}

struct LocalizedMessages: Codable, Hashable {
    var messages: [String: Messages] = [:]
}

struct MessagesConfig: Codable, Hashable {
    var warningLevels: [String: LocalizedMessages] = [:]
}

extension MessagesConfig {
    func retrieveMessages(for warningLevel: UInt8, locale: String? = nil) -> Messages? {
        let language = (locale ?? Locale.current.languageCode) ?? "en"
        return warningLevels["\(warningLevel)"]?.messages[language]
    }
}

struct NotificationConfig: Codable {
    var `default`: MessagesConfig
    var departments: [NotificationHealthDepartment]
}

extension NotificationConfig {
    func retrieveMessages(for warningLevel: UInt8, healthDepartment: UUID, locale: String? = nil) -> Messages {

        let defaultMessages = self.default.retrieveMessages(for: warningLevel, locale: locale) ?? Messages()

        guard let hd = departments.first(where: { $0.uuid.lowercased() == healthDepartment.uuidString.lowercased() }) else {
            return defaultMessages
        }

        if let hdConfig = hd.config,
           let hdMessages = hdConfig.retrieveMessages(for: warningLevel, locale: locale) {
            return defaultMessages.update(with: hdMessages).replacePlaceholders(with: hd)
        }

        return defaultMessages.replacePlaceholders(with: hd)
    }
}

enum FetchNotificationConfigError {

}

extension FetchNotificationConfigError: RequestError {
    var localizedTitle: String {
        return ""
    }
}

class FetchNotificationConfigBinaryAsyncOperation: BackendBinaryAsyncDataOperation<KeyValueParameters, NotificationConfig, FetchNotificationConfigError> {
    init(backendAddress: BackendAddressV4) {
        let fullUrl = backendAddress.dataUrl
            .appendingPathComponent("notifications")
            .appendingPathComponent("config")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                cachePolicy: .useProtocolCachePolicy,
                disableCache: false,
                errorMappings: [:]
            )
        )
    }

    override func map(data: Data) throws -> NotificationConfig {

        let json = try JSON(data: data)

        let defaultConfig = try parseConfig(json["default"])

        guard let departments = json["departments"].array else {
            throw NetworkError.invalidResponsePayload
        }

        // Parse departments
        var parsedDepartments: [NotificationHealthDepartment] = []
        for department in departments {
            guard let uuid = department["uuid"].string else {
                throw NetworkError.invalidResponsePayload
            }
            var parsedDepartment = NotificationHealthDepartment(
                uuid: uuid,
                name: department["name"].string,
                email: department["email"].string,
                phone: department["phone"].string,
                config: nil
            )
            if department["config"].exists() {
                parsedDepartment.config = try parseConfig(department["config"])
            }
            parsedDepartments.append(parsedDepartment)
        }

        return NotificationConfig(default: defaultConfig, departments: parsedDepartments)
    }

    private func parseConfig(_ json: JSON) throws -> MessagesConfig {
        var retVal = MessagesConfig()
        for entry in json {
            retVal.warningLevels[entry.0] = try parseLocalizedMessages(entry.1["messages"])
        }
        return retVal
    }

    private func parseLocalizedMessages(_ json: JSON) throws -> LocalizedMessages {
        var retVal = LocalizedMessages()
        for entry in json {
            retVal.messages[entry.0] = try parseMessages(entry.1)
        }
        return retVal
    }

    private func parseMessages(_ json: JSON) throws -> Messages {
        let data = try json.rawData()
        return try JSONDecoder().decode(Messages.self, from: data)
    }
}
