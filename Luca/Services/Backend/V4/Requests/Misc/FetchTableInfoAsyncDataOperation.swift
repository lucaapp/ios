import Foundation

struct TableInfo: Codable {
    var id: String
    var customName: String?
    var internalNumber: Int
    var locationId: String

    var description: String {
        if let name = customName, !name.isEmpty {
            return name
        }
        return String(internalNumber)
    }
}

enum FetchTableError: RequestError {
    case badRequest
    case notFound
}

extension FetchTableError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class FetchTableInfoAsyncDataOperation: BackendAsyncDataOperation<KeyValueParameters, TableInfo, FetchTableError> {

    init(backendAddress: BackendAddress, tableId: String) {
        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("locationTables")
            .appendingPathComponent(tableId)

#if PRODUCTION
        let enableLog = false
#else
        let enableLog = true
#endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                enableLog: enableLog,
                errorMappings: [400: .badRequest,
                                404: .notFound]
            )
        )
    }
}
