import Foundation

enum FetchAccessedTracesErrorV4 {
}

extension FetchAccessedTracesErrorV4: RequestError {
    var localizedTitle: String {
        L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        ""
    }
}

enum AccessedTracesDataChunkError: Error {
    case dataChunkInvalidSize
    case dataChunkInvalidSchemaVersion
    case dataChunkHashesBufferIncompatibleWithDeclaredHashLength
    case dataChunkInvalidAlgorithm
}

struct AccessedTracesDataChunk: Equatable, Codable {
    var schemaVersion: UInt8
    var algorithm: UInt8
    var hashLength: UInt8
    var createdAt: Date
    var alignmentBytes: [UInt8]
    var previousHash: Data
    var hashes: Set<Data>

    /// Data this object has been created from
    var originalData: Data

    init(data: Data) throws {
        print("AccessedTracesDataChunk.begin \(Date.now)")
        guard data.count >= 32 else {
            throw AccessedTracesDataChunkError.dataChunkInvalidSize
        }
        var bytes = data.bytes
        schemaVersion = bytes.removeFirst()
        if schemaVersion != 1 {
            throw AccessedTracesDataChunkError.dataChunkInvalidSchemaVersion
        }
        algorithm = bytes.removeFirst()
        if algorithm != 0 {
            throw AccessedTracesDataChunkError.dataChunkInvalidAlgorithm
        }
        hashLength = bytes.removeFirst()

        let rui = bytes.prefix(8).reversed().withUnsafeBytes { $0.load(as: UInt64.self) }
        let timeInterval = TimeInterval(rui)/1000.0

        createdAt = Date(timeIntervalSince1970: timeInterval)
        bytes.removeFirst(8)
        alignmentBytes = Array(bytes.prefix(5))
        bytes.removeFirst(5)
        previousHash = Data(bytes.prefix(16))
        bytes.removeFirst(16)

        let intHashLength = Int(hashLength)
        guard bytes.count % intHashLength == 0 else {
            throw AccessedTracesDataChunkError.dataChunkHashesBufferIncompatibleWithDeclaredHashLength
        }

        hashes = []
        while !bytes.isEmpty {
            let hash = Data(bytes.prefix(intHashLength))
            hashes.insert(hash)
            bytes.removeFirst(intHashLength)
        }
        originalData = data
        print("AccessedTracesDataChunk.end \(Date.now)")
    }
}

class FetchAccessedTracesV4AsyncDataOperation: BackendBinaryAsyncDataOperation<KeyValueParameters, AccessedTracesDataChunk, FetchAccessedTracesErrorV4> {

    init(backendAddress: BackendAddressV4) {
        let fullUrl = backendAddress.dataUrl
            .appendingPathComponent("notifications")
            .appendingPathComponent("traces")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                errorMappings: [:]
            )
        )
    }

    override func map(data: Data) throws -> AccessedTracesDataChunk {
        try AccessedTracesDataChunk(data: data)
    }
}
