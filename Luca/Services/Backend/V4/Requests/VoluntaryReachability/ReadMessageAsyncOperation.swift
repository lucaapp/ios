import Foundation

struct ReadMessageRequest: Codable {
    var messageId: String
    var timestamp: Int
    var signature: String
}

enum ReadMessageError: RequestError {

}

extension ReadMessageError {
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        return "\(self)"
    }
}

class ReadMessageAsyncOperationAsyncOperation: BackendAsyncOperation<ReadMessageRequest, ReadMessageError> {
    init(backendAddress: BackendAddressV4, payload: ReadMessageRequest) {
        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("connect")
            .appendingPathComponent("messages")
            .appendingPathComponent("read")

        #if PRODUCTION
        let enableLog = false
        #else
        let enableLog = true
        #endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                parameters: payload,
                enableLog: enableLog,
                errorMappings: [:]
            )
        )
    }
}
