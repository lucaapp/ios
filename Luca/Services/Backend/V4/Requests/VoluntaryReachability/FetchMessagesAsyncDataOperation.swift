import Foundation

struct FetchedMessageContent: Codable {
    var title: String
    var message: String

    enum CodingKeys: String, CodingKey {
        case title = "sub"
        case message = "msg"
    }
}

struct FetchMessagesElement: Codable {
    var messageId: String
    var data: String
    var iv: String
    var mac: String
    var createdAt: Int
}

enum FetchMessagesError: RequestError {

}

extension FetchMessagesError {
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        return "\(self)"
    }
}

class FetchMessagesAsyncDataOperation: BackendAsyncDataOperation<[String: [String]], [FetchMessagesElement], FetchMessagesError> {
    init(backendAddress: BackendAddressV4, messageIds: [String]) {
        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("connect")
            .appendingPathComponent("messages")
            .appendingPathComponent("receive")

        #if PRODUCTION
        let enableLog = false
        #else
        let enableLog = true
        #endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                parameters: ["messageIds": messageIds],
                enableLog: enableLog,
                errorMappings: [:]
            )
        )
    }
}
