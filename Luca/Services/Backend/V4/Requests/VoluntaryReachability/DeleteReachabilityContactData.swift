import Foundation

enum DeleteReachabilityContactDataError: RequestError {
    case invalidSignature
}

extension DeleteReachabilityContactDataError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

struct DeleteReachabilityContactDataRequest: Codable {
    var contactId: String
    var timestamp: Date
    var signature: String
}

class DeleteReachabilityContactDataAsyncDataOperation: BackendAsyncOperation<DeleteReachabilityContactDataRequest, DeleteReachabilityContactDataError> {
    init(backendAddress: BackendAddressV4, payload: DeleteReachabilityContactDataRequest) {
        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("connect")
            .appendingPathComponent("contacts")

        #if PRODUCTION
        let enableLog = false
        #else
        let enableLog = true
        #endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .delete,
                parameters: payload,
                enableLog: enableLog,
                errorMappings: [403: .invalidSignature]
            )
        )
    }
}
