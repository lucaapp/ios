import Foundation

enum FetchZipCodeHealthDepartmentError: RequestError {
}

extension FetchZipCodeHealthDepartmentError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class FetchZipCodeDepartmentAsyncOperation: BackendAsyncDataOperation<KeyValueParameters, [HealthDepartment], FetchZipCodeHealthDepartmentError> {
    init(backendAddress: BackendAddressV4) {
        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("zipCodes")
            .appendingPathComponent("connect")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                disableCache: false,
                errorMappings: [:]
            )
        )
    }
}
