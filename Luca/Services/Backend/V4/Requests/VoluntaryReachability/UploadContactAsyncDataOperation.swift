import Foundation

enum UploadContactError: RequestError {
    case invalidSignature
}

extension UploadContactError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

struct UploadContactResult: Codable {
    var contactId: String
}

class UploadContactAsyncDataOperation: BackendAsyncDataOperation<ReachableContactRequest, UploadContactResult, UploadContactError> {
    init(backendAddress: BackendAddressV4, data: ReachableContactRequest) {
        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("connect")
            .appendingPathComponent("contacts")

        #if PRODUCTION
        let enableLog = false
        #else
        let enableLog = true
        #endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                parameters: data,
                enableLog: enableLog,
                errorMappings: [403: .invalidSignature]
            )
        )
    }
}
