import Foundation

struct PoWChallenge: Codable, Equatable {
    typealias Id = String
    var id: Id
    var t: String
    var n: String
    var expiresAt: Int
}

enum PoWType: String {
    case test
    case createConnectContact
    case requestIdent
}

enum RequestPoWChallengeError: RequestError {
}

extension RequestPoWChallengeError {
    var localizedTitle: String {
        L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        return ""
    }
}

class RequestPoWChallengeAsyncDataOperation: BackendAsyncDataOperation<KeyValueParameters, PoWChallenge, RequestPoWChallengeError> {

    init(backendAddress: BackendAddressV4, type: PoWType) {
        let fullUrl = backendAddress.dataUrl
            .appendingPathComponent("pow")
            .appendingPathComponent("request")

        #if PRODUCTION
        let enableLog = false
        #else
        let enableLog = true
        #endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                parameters: ["type": type.rawValue],
                enableLog: enableLog,
                errorMappings: [:]
            )
        )
    }
}
