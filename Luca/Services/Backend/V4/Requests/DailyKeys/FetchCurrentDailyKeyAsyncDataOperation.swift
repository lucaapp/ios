import Foundation

struct CurrentDailyKey: Codable {
    var signedPublicDailyKey: String
}

enum CurrentDailyKeyError: RequestError {
    case notFound
}

extension CurrentDailyKeyError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class FetchCurrentDailyKeyAsyncDataOperation: BackendAsyncDataOperation<KeyValueParameters, CurrentDailyKey, CurrentDailyKeyError> {

    init(backendAddress: BackendAddressV4) {
        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("keys")
            .appendingPathComponent("daily")
            .appendingPathComponent("current")

        #if PRODUCTION
        let enableLog = false
        #else
        let enableLog = true
        #endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                enableLog: enableLog,
                errorMappings: [404: .notFound]
            )
        )
    }
}
