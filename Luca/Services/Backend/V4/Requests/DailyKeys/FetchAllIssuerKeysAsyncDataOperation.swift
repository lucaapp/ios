import Foundation

enum FetchIssuerKeysError: RequestError {
    case notFound
}

extension FetchIssuerKeysError {
    var localizedTitle: String {
        L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        switch self {
        case .notFound: return "\(self)"
        }
    }
}

struct IssuerKeys: Codable {
    var issuerId: String?
    var publicCertificate: String?
    var signedPublicHDEKP: String?
    var signedPublicHDSKP: String?
}

class FetchAllIssuerKeysAsyncDataOperation: BackendAsyncDataOperation<KeyValueParameters, [IssuerKeys], FetchIssuerKeysError> {

    init(backendAddress: BackendAddressV4) {
        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("keys")
            .appendingPathComponent("issuers")

        #if PRODUCTION
        let enableLog = false
        #else
        let enableLog = true
        #endif

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                disableCache: false,
                enableLog: enableLog,
                errorMappings: [404: .notFound]
            )
        )
    }
}
