import Foundation
import DependencyInjection

class BackendAddressV4: BackendAddressV3 {
    init() {
        super.init(version: "v4")
    }
}

class BackendDailyKeyV4 {
    @InjectStatic(\.backendAddressV4) private var backendAddress: BackendAddressV4

    func fetchDailyPubKey() -> AsyncDataOperation<BackendError<CurrentDailyKeyError>, CurrentDailyKey> {
        FetchCurrentDailyKeyAsyncDataOperation(backendAddress: backendAddress)
    }

    func fetchAllIssuerKeys() -> AsyncDataOperation<BackendError<FetchIssuerKeysError>, [IssuerKeys]> {
        FetchAllIssuerKeysAsyncDataOperation(backendAddress: backendAddress)
    }
}

class BackendAccessDataV4 {
    @InjectStatic(\.backendAddressV4) private var backendAddress: BackendAddressV4

    func activeChunk() -> AsyncDataOperation<BackendError<FetchAccessedTracesErrorV4>, AccessedTracesDataChunk> {
        FetchAccessedTracesV4AsyncDataOperation(backendAddress: backendAddress)
    }
    func archivedChunk(chunkId: Data) -> AsyncDataOperation<BackendError<FetchAccessedTracesArchivedChunkError>, AccessedTracesDataChunk> {
        FetchAccessedTracesArchivedChunkAsyncDataOperation(backendAddress: backendAddress, chunkId: chunkId)
    }
    func fetchNotificationConfig() -> AsyncDataOperation<BackendError<FetchNotificationConfigError>, NotificationConfig> {
        FetchNotificationConfigBinaryAsyncOperation(backendAddress: backendAddress)
    }
}

class BackendVoluntaryReachabilityV4 {
    @InjectStatic(\.backendAddressV4) private var backendAddress: BackendAddressV4

    func fetchHealthDepartments() -> AsyncDataOperation<BackendError<FetchZipCodeHealthDepartmentError>, [HealthDepartment]> {
        FetchZipCodeDepartmentAsyncOperation(backendAddress: backendAddress)
    }
    func uploadContact(data: ReachableContactRequest) -> AsyncDataOperation<BackendError<UploadContactError>, UploadContactResult> {
        UploadContactAsyncDataOperation(backendAddress: backendAddress, data: data)
    }
    func deleteData(data: DeleteReachabilityContactDataRequest) -> AsyncOperation<BackendError<DeleteReachabilityContactDataError>> {
        DeleteReachabilityContactDataAsyncDataOperation(backendAddress: backendAddress, payload: data)
    }
    func fetchMessages(messageIds: [String]) -> AsyncDataOperation<BackendError<FetchMessagesError>, [FetchMessagesElement]> {
        FetchMessagesAsyncDataOperation(backendAddress: backendAddress, messageIds: messageIds)
    }
    func markAsRead(payload: ReadMessageRequest) -> AsyncOperation<BackendError<ReadMessageError>> {
        ReadMessageAsyncOperationAsyncOperation(backendAddress: backendAddress, payload: payload)
    }
}

class BackendPoWV4 {
    @InjectStatic(\.backendAddressV4) private var backendAddress: BackendAddressV4

    func requestChallenge(type: PoWType) -> AsyncDataOperation<BackendError<RequestPoWChallengeError>, PoWChallenge> {
        RequestPoWChallengeAsyncDataOperation(backendAddress: backendAddress, type: type)
    }
}

class BackendStagedRolloutV4 {
    @InjectStatic(\.backendAddressV4) private var backendAddress: BackendAddressV4

    func fetchStagedRollouts() -> AsyncDataOperation<BackendError<FetchStagedRolloutsError>, [StagedRollout]> {
        FetchStagedRolloutsAsyncDataOperation(backendAddress: backendAddress)
    }
}

class BackendTableInfoV4 {
    @InjectStatic(\.backendAddressV4) private var backendAddress: BackendAddressV4

    func fetchTableInfo(tableId: String) -> AsyncDataOperation<BackendError<FetchTableError>, TableInfo> {
        FetchTableInfoAsyncDataOperation(backendAddress: backendAddress, tableId: tableId)
    }
}
