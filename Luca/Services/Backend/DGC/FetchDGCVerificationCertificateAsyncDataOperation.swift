import Foundation
import Alamofire
import RxSwift
import SwiftDGC
import CryptorECC

enum DGCVerificationCertificateDataError: LocalizedError {
    case parsingFailed
    case verificationFailed
}

extension DGCVerificationCertificateDataError {
    var errorDescription: String? {
        switch self {
        case .parsingFailed: return L10n.IOSApp.Test.Result.Parsing.error
        case .verificationFailed: return L10n.IOSApp.Test.Result.Verification.error
        }
    }

    var localizedTitle: String {
        switch self {
        case .verificationFailed: return L10n.IOSApp.Test.Result.Verification.error
        default:
            return L10n.IOSApp.Test.Result.Error.title
        }
    }
}

struct DGCVerificationCertificateData: Codable {
    var certs: DGCVerificationCertificates!

    init(data: Data) throws {
        let dataString = String(data: data, encoding: .utf8) ?? ""
        let payload = dataString.split(separator: "\n", maxSplits: 1).map(String.init)

        guard payload.count == 2 else {
            throw DGCVerificationCertificateDataError.parsingFailed
        }

        guard let signatureData = Data(base64Encoded: payload[0]),
              let jsonData = payload[1].data(using: .utf8) else {
                  throw DGCVerificationCertificateDataError.parsingFailed
              }

        // verify all signing keys with provided ECDSA-SHA256 signature
        guard try verify(payload: jsonData, signature: signatureData) else {
            throw DGCVerificationCertificateDataError.verificationFailed
        }

        certs =  try JSONDecoder().decode(DGCVerificationCertificates.self, from: jsonData)
    }

    private func verify(payload: Data, signature: Data) throws -> Bool {

        // Key is decoded from ubirch pem public key (https://de.dscg.ubirch.com/) :
        //        -----BEGIN PUBLIC KEY-----
        //        MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAETHfi8foQF4UtSNVxSFxeu7W+gMxd
        //        SGElhdo7825SD3Lyb+Sqh4G6Kra0ro1BdrM6Qx+hsUx4Qwdby7QY0pzxyA==
        //        -----END PUBLIC KEY-----
        let key = try KeyFactory.createPublicEC(
            x: Data(hex: "4c77e2f1fa1017852d48d571485c5ebbb5be80cc5d48612585da3bf36e520f72"),
            y: Data(hex: "f26fe4aa8781ba2ab6b4ae8d4176b33a431fa1b14c7843075bcbb418d29cf1c8"),
            sizeInBits: 256
        )

        let publicKeySource = ValueKeySource(key: key)

        let ecdsa = ECDSA(privateKeySource: nil, publicKeySource: publicKeySource, algorithm: .ecdsaSignatureMessageX962SHA256)
        // DER encoding
        let asnEncodedSignature = ASN1.encode(signature)

        return try ecdsa.verify(data: payload, signature: asnEncodedSignature)
    }
}

class FetchDGCVerificationCertificateAsyncDataOperation: BackendBinaryAsyncDataOperation<KeyValueParameters, DGCVerificationCertificateData, DGCKeyServiceBackendError> {

    init(backendAddress: BackendAddressV4) {
        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("trustList").appendingPathComponent("DSC")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                disableCache: false,
                errorMappings: [:]
            )
        )
    }

    override func map(data: Data) throws -> DGCVerificationCertificateData {
        try DGCVerificationCertificateData(data: data)
    }
}
