import Foundation
import Alamofire

enum PaymentCreateCheckoutForOpenRequestError: RequestError {
    case paymentRequestNotFound
    case invalidPartialAmount
}

extension PaymentCreateCheckoutForOpenRequestError {
    var errorDescription: String? {
        switch self {
        case .paymentRequestNotFound:
            return L10n.UserApp.Pay.Error.Expired.text
        case .invalidPartialAmount:
            return L10n.UserApp.Error.Pay.Split.amount
        }
    }
    var localizedTitle: String {
        switch self {
        case .paymentRequestNotFound:
            return L10n.UserApp.Pay.Error.Expired.title
        case .invalidPartialAmount:
            return L10n.IOSApp.Navigation.Basic.error
        }
    }
}

struct PaymentCreateCheckoutForOpenRequestParams: Codable {
    var locationId: String

    /// Optional parameter. If present, it represents a split payment.
    var amount: Decimal?

    var tipAmount: Decimal
    var table: String
    var waiter: String?
    let referer = "luca/ios"
}

class PaymentCreateCheckoutForOpenRequestAsyncOperation<C: Currency>: BackendAsyncDataOperation<PaymentCreateCheckoutForOpenRequestParams, PaymentCreateCheckoutResponse, PaymentCreateCheckoutForOpenRequestError> {
    init(backendAddress: BackendAddress,
         accessToken: String,
         locationId: String,
         paymentRequestId: String,
         amount: Money<C>? = nil,
         tip: Money<C>,
         table: String,
         waiter: String? = nil
    ) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("pay")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("locations")
            .appendingPathComponent(locationId)
            .appendingPathComponent("paymentRequests")
            .appendingPathComponent(paymentRequestId)
            .appendingPathComponent("checkout")

        let params = PaymentCreateCheckoutForOpenRequestParams(
            locationId: locationId,
            amount: amount?.rounded.amount,
            tipAmount: tip.rounded.amount,
            table: table,
            waiter: waiter
        )
        let header = HTTPHeader.xAuth("\(accessToken)")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                parameters: params,
                enableLog: true,
                errorMappings: [
                    404: .paymentRequestNotFound,
                    406: .invalidPartialAmount
                ],
                additionalHeaders: [header]
            )
        )
    }
}
