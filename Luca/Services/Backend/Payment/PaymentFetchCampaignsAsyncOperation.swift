import Foundation
import Alamofire

struct PaymentCampaign: Codable {
    struct Extended: Codable {
        var name: String
        fileprivate var startsAt: Double?
        fileprivate var endsAt: Double?
        var discountPercentage: Int?
        var discountMaxAmount: Double?
    }

    var id: String
    var locationGroupId: String
    var createdAt: Double
    var paymentCampaign: Extended

    var campaignType: CampaignType {
        if paymentCampaign.name.hasPrefix(CampaignType.lucaDiscount.rawValue) &&
            paymentCampaign.discountPercentage != nil &&
            paymentCampaign.discountMaxAmount != nil {
            return .lucaDiscount
        }
        return CampaignType(rawValue: paymentCampaign.name) ?? .unknown
    }

    var startDate: Date? {
        guard let date = paymentCampaign.startsAt else { return nil }
        return Date(timeIntervalSince1970: date)
    }

    var endDate: Date? {
        guard let date = paymentCampaign.endsAt else { return nil }
        return Date(timeIntervalSince1970: date)
    }

    func discount<C: Currency>(amount: Money<C>) -> Money<C>? {
        guard let discountPercent = paymentCampaign.discountPercentage,
              let discountMaxAmount = paymentCampaign.discountMaxAmount else { return nil }

        let discountFraction = amount * Double(discountPercent) / 100.0
        let discountAmount = discountFraction.rounded
        let discountMaxAmountMoney = Money(double: discountMaxAmount, currency: amount.currency)
        if discountAmount > discountMaxAmountMoney {
            return amount - discountMaxAmount
        }
        return amount - discountAmount
    }

    func undiscount<C: Currency>(amount: Money<C>) -> Money<C>? {
        guard let discountPercent = paymentCampaign.discountPercentage,
              let discountMaxAmount = paymentCampaign.discountMaxAmount else { return nil }

        let undiscountedAmount = amount * (1.0 / Double(discountPercent))
        let discountAmount = undiscountedAmount - amount
        let discountMaxAmountMoney = Money(double: discountMaxAmount, currency: amount.currency)
        if discountAmount > discountMaxAmountMoney {
            return amount + discountMaxAmount
        }
        return amount + discountAmount
    }
}

enum PaymentFetchCampaignsError: RequestError {
    case unknownLocation
}

extension PaymentFetchCampaignsError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class PaymentFetchCampaignsAsyncOperation: BackendAsyncDataOperation<KeyValueParameters, [PaymentCampaign], PaymentFetchCampaignsError> {
    init(backendAddress: BackendAddress, locationId: String) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("pay")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("locations")
            .appendingPathComponent(locationId)
            .appendingPathComponent("campaigns")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                enableLog: true,
                errorMappings: [
                    404: .unknownLocation
                ]
            )
        )
    }
}
