import Foundation
import Alamofire

struct PaymentSignInResponse: Codable {
    var accessToken: String
    var expiresIn: Double
}

enum PaymentSignInError: RequestError {
    case unauthorized
}

extension PaymentSignInError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class PaymentSignInAsyncOperation: BackendAsyncDataOperation<KeyValueParameters, PaymentSignInResponse, PaymentSignInError> {
    init(backendAddress: BackendAddress, username: String, password: String) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("pay")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("consumers")
            .appendingPathComponent("signin")

        let params = [
            "username": username,
            "password": password
        ]

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                parameters: params,
                enableLog: true,
                errorMappings: [
                    401: .unauthorized
                ]
            )
        )
    }
}
