import Foundation
import Alamofire

struct PaymentFetchOpenRequestResponse: Codable, Equatable {
    var uuid: String
    var table: String?
    var invoiceAmount: Double
    var locationId: String
    var status: PaymentStatus
    var allowSplitPayment: Bool?
    var openAmount: Double?
    var paidAmount: Double?
}

enum PaymentFetchOpenRequestError: RequestError {
    case noOpenPayment
}

extension PaymentFetchOpenRequestError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class PaymentFetchOpenRequestAsyncOperation: BackendAsyncDataOperation<KeyValueParameters, PaymentFetchOpenRequestResponse, PaymentFetchOpenRequestError> {
    init(backendAddress: BackendAddress, locationId: String, table: String?) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("pay")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("locations")
            .appendingPathComponent(locationId)
            .appendingPathComponent("paymentRequests")
            .appendingPathComponent("open")

        var urlComponents = URLComponents(url: fullUrl, resolvingAgainstBaseURL: true)!
        if let table = table {
            urlComponents.queryItems = [
                URLQueryItem(name: "table", value: table)
            ]
        }

        super.init(
            RequestConfig(
                url: urlComponents.url!,
                method: .get,
                enableLog: true,
                errorMappings: [
                    404: .noOpenPayment
                ]
            )
        )
    }
}
