import Foundation
import Alamofire

struct PaymentUser: Codable {
    struct TokenizedCard: Codable {
        var name: String
        var type: String
        var last4digits: String
        var expirationYear: String
        var expirationMonth: String
    }
    var uuid: String
    var username: String
    var tokenizedCards: [TokenizedCard]?
}

enum PaymentFetchUserError: RequestError {

}

extension PaymentFetchUserError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class PaymentFetchUserAsyncOperation: BackendAsyncDataOperation<KeyValueParameters, PaymentUser, PaymentFetchUserError> {
    init(backendAddress: BackendAddress, accessToken: String) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("pay")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("consumers")
            .appendingPathComponent("me")

        let headers = HTTPHeader.xAuth("\(accessToken)")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                enableLog: true,
                errorMappings: [:],
                additionalHeaders: [headers]
            )
        )
    }
}
