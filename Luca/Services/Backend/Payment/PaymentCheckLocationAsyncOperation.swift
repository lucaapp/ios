import Foundation
import Alamofire

struct PaymentActiveResponse: Codable {
    var uuid: String
    var paymentActive: Bool
}

enum PaymentCheckLocationError: RequestError {
    case unauthorized
}

extension PaymentCheckLocationError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class PaymentCheckLocationAsyncOperation: BackendAsyncDataOperation<KeyValueParameters, PaymentActiveResponse, PaymentCheckLocationError> {
    init(backendAddress: BackendAddress, locationId: String) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("pay")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("locations")
            .appendingPathComponent(locationId)
            .appendingPathComponent("paymentActive")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                enableLog: true,
                errorMappings: [
                    401: .unauthorized
                ]
            )
        )
    }
}
