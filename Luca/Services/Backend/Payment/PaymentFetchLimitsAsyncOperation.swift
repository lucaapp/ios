import Foundation
import Alamofire

struct PaymentLimits: Codable {
    var minInvoiceAmount: Double
    var maxInvoiceAmount: Double
    var maxTipAmount: Double
}

enum PaymentFetchLimitsError: RequestError {}
extension PaymentFetchLimitsError {

    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class PaymentFetchLimitsAsyncOperation: BackendAsyncDataOperation<KeyValueParameters, PaymentLimits, PaymentFetchLimitsError> {
    init(backendAddress: BackendAddress, accessToken: String) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("pay")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("configs")
            .appendingPathComponent("paymentLimits")

        let headers = HTTPHeader.xAuth("\(accessToken)")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                enableLog: true,
                errorMappings: [:],
                additionalHeaders: [headers]
            )
        )
    }
}
