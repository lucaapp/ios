import Foundation
import Alamofire

enum PaymentDeleteUserError: RequestError {
    case userNotExists
}

extension PaymentDeleteUserError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class PaymentDeleteUserAsyncOperation: BackendAsyncOperation<KeyValueParameters, PaymentDeleteUserError> {
    init(backendAddress: BackendAddress, accessToken: String) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("pay")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("consumers")
            .appendingPathComponent("me")

        let header = HTTPHeader.xAuth("\(accessToken)")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .delete,
                enableLog: true,
                errorMappings: [
                    401: .userNotExists
                ],
                additionalHeaders: [header]
            )
        )
    }
}
