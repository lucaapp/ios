import Foundation
import Alamofire

enum PaymentStatus: String, Codable {
    case open = "OPEN"
    case started = "STARTED"
    case closed = "CLOSED"
    case error = "ERROR"
    case unknown = "UNKNOWN"
}

struct Payment: Codable {
    struct Discount: Codable {

        /// Amount that the `originalInvoiceAmount` was discounted by
        var discountAmount: Double

        /// "Intended" amount without tip and without any discounts
        var originalInvoiceAmount: Double
    }

    var paymentId: String?
    var locationName: String
    var locationId: String

    /// Total amount paid by the customer. `invoiceAmount` + `tipAmount`. It is discounted if there were some discounts at this point.
    var amount: Double

    /// Amount without `tipAmount`. It is discounted if there were some discounts at this point.
    var invoiceAmount: Double?

    /// Tip.
    var tipAmount: Double?

    var dateTime: Double
    var status: PaymentStatus
    var refunded: Bool?
    var refundedAmount: Double?
    var paymentVerifier: String?
    var table: String?
    var lucaDiscount: Discount?

    /*
     example purchase for 100 €

     invoiceAmount: 50,
     tipAmount: 10,
     lucaDiscount: {
       originalInvoiceAmount: 100,
       discountAmount: 50
     }

     */
}

extension Payment {
    var isSuccessful: Bool {
        status == .closed
    }

    /// Original amount to be paid. Doesn't contain any discount and tip.
    ///
    /// If no amount is present, `0.0` will be returned
    var originalInvoiceAmount: Double {
        (lucaDiscount?.originalInvoiceAmount ?? invoiceAmount) ?? 0.0
    }

    /// Discounted amount to be paid. If there is no discount, the unchanged amount is returned. Does not contain tip.
    ///
    /// If no amount is present, `0.0` will be returned
    var discountedInvoiceAmount: Double {
        invoiceAmount ?? 0.0
    }

    var dateTimeParsed: Date {
        Date(timeIntervalSince1970: dateTime)
    }

}

struct PaymentHistoryPage: Codable {
    var cursor: String?
    var payments: [Payment]
}

enum PaymentFetchHistoryError: RequestError {
}

extension PaymentFetchHistoryError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class PaymentFetchHistoryAsyncOperation: BackendAsyncDataOperation<KeyValueParameters, PaymentHistoryPage, PaymentFetchHistoryError> {
    init(backendAddress: BackendAddress, accessToken: String, cursor: String?) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("pay")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("consumers")
            .appendingPathComponent("me")
            .appendingPathComponent("payments")

        var urlComponents = URLComponents(url: fullUrl, resolvingAgainstBaseURL: true)!
        if let cursor = cursor {
            urlComponents.queryItems = [
                URLQueryItem(name: "cursor", value: cursor)
            ]
        }

        let header = HTTPHeader.xAuth("\(accessToken)")

        super.init(
            RequestConfig(
                url: urlComponents.url!,
                method: .get,
                enableLog: true,
                errorMappings: [:],
                additionalHeaders: [header]
            )
        )
    }
}
