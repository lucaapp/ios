import DependencyInjection
import Foundation

class BackendLucaPayment {

    @InjectStatic(\.backendAddressV4) private var backendAddress: BackendAddressV4

    func checkPaymentActive(locationId: String) -> AsyncDataOperation<BackendError<PaymentCheckLocationError>, PaymentActiveResponse> {
        PaymentCheckLocationAsyncOperation(backendAddress: backendAddress, locationId: locationId)
    }

    func createPaymentUser(name: String, username: String, password: String) -> AsyncDataOperation<BackendError<PaymentCreateUserError>, PaymentCreatedUser> {
        PaymentCreateUserAsyncOperation(backendAddress: backendAddress, name: name, username: username, password: password)
    }

    func signIn(username: String, password: String) -> AsyncDataOperation<BackendError<PaymentSignInError>, PaymentSignInResponse> {
        PaymentSignInAsyncOperation(backendAddress: backendAddress, username: username, password: password)
    }

    func fetchUser(accessToken: String) -> AsyncDataOperation<BackendError<PaymentFetchUserError>, PaymentUser> {
        PaymentFetchUserAsyncOperation(backendAddress: backendAddress, accessToken: accessToken)
    }

    func createPayment<C: Currency>(
        accessToken: String,
        locationId: String,
        invoice: Money<C>,
        tip: Money<C>,
        table: String,
        waiter: String? = nil
    ) -> AsyncDataOperation<BackendError<PaymentCreateCheckoutError>, PaymentCreateCheckoutResponse> {
        PaymentCreateCheckoutAsyncOperation(
            backendAddress: backendAddress,
            accessToken: accessToken,
            locationId: locationId,
            invoice: invoice,
            tip: tip,
            table: table,
            waiter: waiter
        )
    }

    func createPayment<C: Currency>(
        accessToken: String,
        locationId: String,
        paymentRequestId: String,
        amount: Money<C>? = nil,
        tip: Money<C>,
        table: String,
        waiter: String? = nil
    ) -> AsyncDataOperation<BackendError<PaymentCreateCheckoutForOpenRequestError>, PaymentCreateCheckoutResponse> {
        PaymentCreateCheckoutForOpenRequestAsyncOperation(
            backendAddress: backendAddress,
            accessToken: accessToken,
            locationId: locationId,
            paymentRequestId: paymentRequestId,
            amount: amount,
            tip: tip,
            table: table,
            waiter: waiter
        )
    }

    func fetchPaymentHistory(accessToken: String, cursor: String? = nil) -> AsyncDataOperation<BackendError<PaymentFetchHistoryError>, PaymentHistoryPage> {
        PaymentFetchHistoryAsyncOperation(backendAddress: backendAddress, accessToken: accessToken, cursor: cursor)
    }

    func fetchCheckout(checkoutId: String) -> AsyncDataOperation<BackendError<PaymentFetchCheckoutError>, PaymentFetchCheckoutResponse> {
        PaymentFetchCheckoutAsyncOperation(backendAddress: backendAddress, checkoutId: checkoutId)
    }

    func fetchOpenPaymentRequest(locationId: String, table: String?) -> AsyncDataOperation<BackendError<PaymentFetchOpenRequestError>, PaymentFetchOpenRequestResponse> {
        PaymentFetchOpenRequestAsyncOperation(backendAddress: backendAddress, locationId: locationId, table: table)
    }

    func deleteUser(accessToken: String) -> AsyncOperation<BackendError<PaymentDeleteUserError>> {
        PaymentDeleteUserAsyncOperation(backendAddress: backendAddress, accessToken: accessToken)
    }

    func submitCampaignParticipation(paymentId: String, params: CampaignSubmissionParams, accessToken: String) -> AsyncDataOperation<BackendError<SubmitCampaignParticipationError>, CampaignSubmission> {
        SubmitCampaignParticipationAsyncDataOperation(backendAddress: backendAddress, paymentId: paymentId, params: params, accessToken: accessToken)
    }

    func fetchLimits(accessToken: String) -> AsyncDataOperation<BackendError<PaymentFetchLimitsError>, PaymentLimits> {
        PaymentFetchLimitsAsyncOperation(backendAddress: backendAddress, accessToken: accessToken)
    }

    func fetchCampaigns(locationId: String) -> AsyncDataOperation<BackendError<PaymentFetchCampaignsError>, [PaymentCampaign]> {
        PaymentFetchCampaignsAsyncOperation(backendAddress: backendAddress, locationId: locationId)
    }
}
