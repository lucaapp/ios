import Foundation
import Alamofire

struct PaymentFetchCheckoutResponse: Codable {
    var table: String?
    var checkoutId: String
    var locationId: String
    var payment: Payment
}

enum PaymentFetchCheckoutError: RequestError {
    case checkoutNotFound
    case unexpectedPaymentProviderError
}

extension PaymentFetchCheckoutError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class PaymentFetchCheckoutAsyncOperation: BackendAsyncDataOperation<KeyValueParameters, PaymentFetchCheckoutResponse, PaymentFetchCheckoutError> {
    init(backendAddress: BackendAddress, checkoutId: String) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("pay")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("checkouts")
            .appendingPathComponent(checkoutId)

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .get,
                enableLog: true,
                errorMappings: [
                    404: .checkoutNotFound,
                    502: .unexpectedPaymentProviderError
                ]
            )
        )
    }
}
