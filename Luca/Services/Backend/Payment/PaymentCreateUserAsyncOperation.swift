import Foundation
import Alamofire

struct PaymentCreatedUser: Codable {
    var uuid: String
    var username: String
    var revocationCode: String
}

enum PaymentCreateUserError: RequestError {
    case unexpectedPaymentProviderError
    case userAlreadyExists
}

extension PaymentCreateUserError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

class PaymentCreateUserAsyncOperation: BackendAsyncDataOperation<KeyValueParameters, PaymentCreatedUser, PaymentCreateUserError> {
    init(backendAddress: BackendAddress, name: String, username: String, password: String) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("pay")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("consumers")
            .appendingPathComponent("signup")

        let params = [
            "username": username,
            "name": name,
            "password": password
        ]

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                parameters: params,
                enableLog: true,
                errorMappings: [
                    502: .unexpectedPaymentProviderError,
                    409: .userAlreadyExists
                ]
            )
        )
    }
}
