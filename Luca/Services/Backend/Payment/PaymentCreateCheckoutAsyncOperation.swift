import Foundation
import Alamofire

struct PaymentCreateCheckoutResponse: Codable {
    var uuid: String
    var checkoutId: String
    var checkoutUrl: String
}

enum PaymentCreateCheckoutError: RequestError {
}

extension PaymentCreateCheckoutError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }
}

struct PaymentCreateCheckoutParams: Codable {
    var locationId: String
    var invoiceAmount: Decimal
    var tipAmount: Decimal
    var table: String
    var waiter: String?
    let referer = "luca/ios"
}

class PaymentCreateCheckoutAsyncOperation<C: Currency>: BackendAsyncDataOperation<PaymentCreateCheckoutParams, PaymentCreateCheckoutResponse, PaymentCreateCheckoutError> {
    init(backendAddress: BackendAddress,
         accessToken: String,
         locationId: String,
         invoice: Money<C>,
         tip: Money<C>,
         table: String,
         waiter: String? = nil
    ) {
        let fullUrl = backendAddress.host
            .appendingPathComponent("pay")
            .appendingPathComponent("api")
            .appendingPathComponent("v1")
            .appendingPathComponent("checkouts")

        let params = PaymentCreateCheckoutParams(
            locationId: locationId,
            invoiceAmount: invoice.rounded.amount,
            tipAmount: tip.rounded.amount,
            table: table,
            waiter: waiter
        )
        let header = HTTPHeader.xAuth("\(accessToken)")

        super.init(
            RequestConfig(
                url: fullUrl,
                method: .post,
                parameters: params,
                enableLog: true,
                errorMappings: [:],
                additionalHeaders: [header]
            )
        )
    }
}
