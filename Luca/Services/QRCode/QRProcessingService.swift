import Foundation
import RxSwift
import JGProgressHUD
import DependencyInjection
import CoreImage

enum QRProcessingServiceSilentError: SilentError {
    case userNotAgreedOnDifferentQRType
    case userNotAgreedOnPrivacyConsent
    case userNotAgreedOnCheckIn
}

enum QRProcessingServiceLocalizedError: LocalizedTitledError {
    case notCheckin
    case notDocument
    case notURL
    case notPayment
}

extension QRProcessingServiceLocalizedError {
    var localizedTitle: String {
        return L10n.IOSApp.Camera.Warning.Processing.title
    }

    var errorDescription: String? {
        switch self {
        case .notCheckin:
            return L10n.IOSApp.Camera.Warning.Processing.NotCheckin.description
        case .notDocument:
            return L10n.IOSApp.Camera.Warning.Processing.NotDocument.description
        case .notURL:
            return L10n.IOSApp.Camera.Warning.Processing.NotURL.description
        case .notPayment:
            return L10n.IOSApp.Camera.Warning.Processing.NotPayment.description
        }
    }
}

class QRProcessingService {
    @InjectStatic(\.documentProcessingService) private var documentProcessingService
    @InjectStatic(\.documentRepoService) private var documentRepoService
    @InjectStatic(\.traceIdService) private var traceIdService
    @InjectStatic(\.lucaPreferences) private var preferences
    @InjectStatic(\.personRepo) private var personRepo
    @InjectStatic(\.backendPayment) private var backend
    @InjectStatic(\.backendAddressV3) private var backendAddress: BackendAddressV3

    public struct ProcessingStrategy {
        enum ContinuationStrategy {
            case selfHandled
            case `default`
        }
        let strategy: (QRType) -> Single<ContinuationStrategy>
    }

    private let qrParser: QRParser

    init() {
        self.qrParser = QRParser()
    }

    public func determineQrType(qr: String) -> Single<QRType> {
        qrParser.processQRType(qr: qr)
            .flatMap({ (type: QRType) -> Single<QRType> in
                return self.evaluatePaymentCapabilities(type: type, qr: qr)
            })
    }

    public func processQRCode(qr: String, processingStrategy: ProcessingStrategy, presenter: UIViewController) -> Single<QRType> {
        determineQrType(qr: qr)
            .flatMap { qrType in
                processingStrategy.strategy(qrType)
                    .flatMap {
                        Single.just((qrType, $0))
                }
            }
            .flatMap { (type: QRType, continuationStrategy: ProcessingStrategy.ContinuationStrategy) -> Single<QRType> in
                guard continuationStrategy == .default else {return Single.just(type)}

                let selectedCompletable: Completable
                switch type {
                case .checkin: selectedCompletable = self.checkin(qr: qr, presenter: presenter)
                case .document: selectedCompletable = self.processDocument(url: qr, presenter: presenter)
                case .url: selectedCompletable = self.openURL(qr: qr)
                case .payment: selectedCompletable = .empty()
                }

                return selectedCompletable.andThen(Single.just(type))
            }
    }

    /// for qr codes of type '.checkin' that come in as deep links check if payments have been activated for this location. if so change the qrtype to .payment
    private func evaluatePaymentCapabilities(type: QRType, qr: String) -> Single<QRType> {
        guard let url = URL(string: qr),
              let checkin = CheckInURLParser.parse(url: url) as? TableSelfCheckin else { return .just(type) }

        return self.traceIdService.fetchScanner(for: checkin)
            .map { (checkin, $0) }
            .flatMap { [weak self] (_: SelfCheckin, scannerInfo: ScannerInfo) -> Single<PaymentActiveResponse> in
                guard let self = self else {
                    throw QRProcessingError.paymentNotActiveAtLocation }
                return self.backend.checkPaymentActive(locationId: scannerInfo.locationId).asSingle()
            }
            .map {
                return .checkin(supportsPayment: $0.paymentActive == true)
            }
    }

    private func checkin(qr: String, presenter: UIViewController) -> Completable {
        LocalCheckinInteractor(qrString: qr).interact()
            .catch { error in
                if let err = error as? LocalCheckinServiceError,
                    err == LocalCheckinServiceError.alreadyCheckedInDifferentLocation {
                        return LocalCheckoutInteractor().interact()
                        .andThen(LocalCheckinInteractor(qrString: qr).interact())
                    }
                throw error
            }
    }

    private func processDocument(url: String, presenter: UIViewController) -> Completable {
        showTestPrivacyConsent(viewController: presenter)
            .andThen(initAdditionalValidators(presenter: presenter))
            .flatMapCompletable { additionalValidators in
                self.documentProcessingService
                    .parseQRCode(qr: url, additionalValidators: additionalValidators)
            }
    }

    private func initAdditionalValidators(presenter: UIViewController) -> Single<[DocumentValidator]> {
        preferences.get(\.userRegistrationData)
            .map { userRegistrationData in
                [VaccinationBirthDateValidator(
                    presenter: presenter,
                    userFirstName: userRegistrationData?.firstName ??? "",
                    userLastName: userRegistrationData?.lastName ??? "",
                    documentSource: self.documentRepoService.currentAndNewTests,
                    personsSource: self.personRepo.restore())]
            }
    }

    private func openURL(qr: String) -> Completable {
        Completable.from {
            guard let url = URL(string: qr) else {
                throw QRProcessingError.parsingFailed
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        .subscribe(on: MainScheduler.instance)
    }

    /// An alert with privacy consent will be shown. If user disagrees, a silent error will be emitted.
    private func showTestPrivacyConsent(viewController: UIViewController) -> Completable {
        let link = L10n.IOSApp.History.Alert.link
        let interactorType = UserInputInteractorConfiguration.InteractorType.modal(linkText: link, url: backendAddress.privacyPolicyUrl)
        let interactorConfiguration = UserInputInteractorConfiguration(title: L10n.IOSApp.Tests.Uniqueness.Consent.title,
                                                             message: L10n.IOSApp.Tests.Uniqueness.Consent.description(link),
                                                             type: interactorType,
                                                             error: QRProcessingServiceSilentError.userNotAgreedOnPrivacyConsent,
                                                             displayCancelOption: true)

        return viewController.showDialog(configuration: interactorConfiguration)
            .subscribe(on: MainScheduler.instance)

    }

}

extension QRProcessingService.ProcessingStrategy {
    static func documentScanner(_ presenter: UIViewController) -> Self {
        QRProcessingService.ProcessingStrategy {
            switch $0 {
            case .payment: return .error(QRProcessingServiceLocalizedError.notDocument)
            case .url: return .error(QRProcessingServiceLocalizedError.notDocument)
            case .document: return .just(.default)
            case .checkin:
                return UIAlertController
                    .okAndCancelAlertRx(
                        viewController: presenter,
                        title: L10n.IOSApp.Camera.Warning.Document.title,
                        message: L10n.IOSApp.Camera.Warning.Document.description,
                        okTitle: L10n.IOSApp.Navigation.Basic.continue
                    )
                    .asObservable()
                    .do(onNext: { if !$0 { throw QRProcessingServiceSilentError.userNotAgreedOnDifferentQRType } })
                    .ignoreElementsAsCompletable()
                    .andThen(Single.just(QRProcessingService.ProcessingStrategy.ContinuationStrategy.default))
            }
        }
    }

    static func checkinScanner(_ presenter: UIViewController) -> Self {
        QRProcessingService.ProcessingStrategy {
            switch $0 {
            case .payment: return .error(QRProcessingServiceLocalizedError.notCheckin)
            case .url: return .error(QRProcessingServiceLocalizedError.notCheckin)
            case .checkin: return .just(.default)
            case .document: return UIAlertController
                    .okAndCancelAlertRx(
                        viewController: presenter,
                        title: L10n.IOSApp.Camera.Warning.Checkin.title,
                        message: L10n.IOSApp.Camera.Warning.Checkin.description,
                        okTitle: L10n.IOSApp.Navigation.Basic.continue
                    )
                    .asObservable()
                    .do(onNext: { if !$0 { throw QRProcessingServiceSilentError.userNotAgreedOnDifferentQRType } })
                    .ignoreElementsAsCompletable()
                    .andThen(Single.just(QRProcessingService.ProcessingStrategy.ContinuationStrategy.default))
            }
        }
    }

    static func paymentScanner(_ presenter: UIViewController) -> Self {
        QRProcessingService.ProcessingStrategy {
                switch $0 {
                case .payment: return .just(.default)
                case .url: return .error(QRProcessingServiceLocalizedError.notPayment)
                case .checkin(let supportsPayment):
                    if supportsPayment == true {
                        return .just(.selfHandled)
                    }
                    return .error(QRProcessingServiceLocalizedError.notPayment)
                case .document: return .error(QRProcessingServiceLocalizedError.notPayment)
            }
        }
    }

    static var lucaConnectProof: Self {
        QRProcessingService.ProcessingStrategy {
                switch $0 {
                case .payment: return .error(QRProcessingServiceLocalizedError.notDocument)
                case .url: return .error(QRProcessingServiceLocalizedError.notDocument)
                case .checkin: return .error(QRProcessingServiceLocalizedError.notDocument)
                case .document: return .just(.default)
            }
        }
    }
}
