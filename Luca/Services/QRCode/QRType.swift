import Foundation

enum QRType {

    case checkin(supportsPayment: Bool?)
    case document
    case url

    /// For future use. Currently there are only checkins or checkins with payment support
    case payment

}

enum QRProcessingError: LocalizedTitledError {

    case parsingFailed
    case paymentNotActiveAtLocation

}

extension QRProcessingError {

    var localizedTitle: String {
        switch self {
        case .parsingFailed: return L10n.IOSApp.Camera.Warning.Incompatible.title
        case .paymentNotActiveAtLocation: return L10n.IOSApp.Payment.NotAvailable.title
        }
    }

    var errorDescription: String? {
        switch self {
        case .parsingFailed: return L10n.IOSApp.Camera.Warning.Incompatible.description
        case .paymentNotActiveAtLocation: return L10n.IOSApp.Payment.notAvailable
        }
    }

}
