import RxSwift
import Foundation
import AVFoundation

enum MetadataScannerServiceError: Error {
    case failedToInitializeCaptureDevice
    case cannotAddVideoInput
    case cannotAddVideoOutput
    case imageNotValid
}

enum MetadataScannerServiceLocalizedError: LocalizedTitledError {
    case captureDeviceNotAuthorized
    case noQRCode
}

extension MetadataScannerServiceLocalizedError {
    var localizedTitle: String {
        switch self {
        case .captureDeviceNotAuthorized:
            return L10n.IOSApp.Camera.Access.title
        case .noQRCode:
            return L10n.IOSApp.Camera.Warning.NoQRCodeContained.title
        }
    }

    var errorDescription: String? {
        switch self {
        case .captureDeviceNotAuthorized:
            return L10n.IOSApp.Camera.Access.description
        case .noQRCode:
            return L10n.IOSApp.Camera.Warning.NoQRCodeContained.description
        }
    }
}

class MetadataScannerService: NSObject {

    private static let sharedDelegate = MetadataScannerService()
    private static let delegateOutputs = PublishSubject<(output: AVCaptureMetadataOutput, didOutput: [AVMetadataObject], from: AVCaptureConnection)>()

    static func createCaptureSession(for metadataObjectTypes: [AVMetadataObject.ObjectType]) -> Single<AVCaptureSession> {
        Single.from {
            guard AVCaptureDevice.authorizationStatus(for: .video) == .authorized else {
                throw MetadataScannerServiceLocalizedError.captureDeviceNotAuthorized
            }
            let captureSession = AVCaptureSession()

            guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
                throw MetadataScannerServiceError.failedToInitializeCaptureDevice
            }

            let videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)

            if captureSession.canAddInput(videoInput) {
                captureSession.addInput(videoInput)
            } else {
                throw MetadataScannerServiceError.cannotAddVideoInput
            }

            let metadataOutput = AVCaptureMetadataOutput()

            if captureSession.canAddOutput(metadataOutput) {
                captureSession.addOutput(metadataOutput)
                metadataOutput.setMetadataObjectsDelegate(Self.sharedDelegate, queue: DispatchQueue.main)
                metadataOutput.metadataObjectTypes = metadataObjectTypes
            } else {
                throw MetadataScannerServiceError.cannotAddVideoOutput
            }

            return captureSession
        }
        .subscribe(on: MainScheduler.asyncInstance)
    }

    static func scan(with captureSession: AVCaptureSession) -> Observable<[AVMetadataObject]> {
        Completable.from {
            guard AVCaptureDevice.authorizationStatus(for: .video) == .authorized else {
                throw MetadataScannerServiceLocalizedError.captureDeviceNotAuthorized
            }
        }
        .andThen(delegateOutputs)
        .asObservable()
        .filter { call in captureSession.outputs.contains { call.output == $0 } }
        .map { $0.didOutput }
        .do(onSubscribe: {
            captureSession.startRunning()
        })
        .do(onDispose: {
            captureSession.stopRunning()
        })
    }

    static func process(image: UIImage?) -> Single<[CIFeature]?> {
        guard let image = image, let ciImage = CIImage.init(image: image) else {
            return Single.error(MetadataScannerServiceError.imageNotValid)
        }

        return Single.from {
            let context = CIContext()

            let detector = CIDetector(ofType: CIDetectorTypeQRCode, context: context, options: [CIDetectorAccuracy: CIDetectorAccuracyHigh])
            let orientation = kCGImagePropertyOrientation as String

            var options: [String: Any] = [CIDetectorImageOrientation: 1]

            if ciImage.properties.keys.contains(orientation), let property = ciImage.properties[orientation] {
                options = [CIDetectorImageOrientation: property]
            }
            let features = detector?.features(in: ciImage, options: options)

            return features
        }
    }

    static func process(features: [CIFeature]?) -> Single<String?> {
        guard let features = features,
              !features.isEmpty,
              let feature = features.first(where: { $0 is CIQRCodeFeature }),
              let qrFeature = feature as? CIQRCodeFeature else {
                  return Single.error(MetadataScannerServiceLocalizedError.noQRCode)
              }

        return Single.just(qrFeature.messageString)
    }

}

extension MetadataScannerService: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        Self.delegateOutputs.onNext((output: output, didOutput: metadataObjects, from: connection))
    }
}
