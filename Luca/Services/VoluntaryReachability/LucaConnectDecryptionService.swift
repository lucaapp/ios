import Foundation
import DependencyInjection
import CryptoSwift
import SwiftJWT

enum LucaConnectDecryptionServiceError: LocalizedTitledError {
    case noResponsibleHealthDepartment
    case invalidMessage
}

extension LucaConnectDecryptionServiceError {

    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String {
        return "\(self)"
    }

}
class LucaConnectDecryptionService {

    @InjectStatic(\.userKeysBundle) private var userKeysBundle

    func decrypt(message: FetchMessagesElement, healthDepartment: HealthDepartment) throws -> FetchedMessage {
        let messageEncryptionKeyPair = try userKeysBundle.reachabilityMessageEncryptionKeyPair.restore()

        let hdClaims = try healthDepartment.jwtSignedPublicHDEKP.claims
        guard let keyData = Data(base64Encoded: hdClaims.key) else {
            throw LucaConnectDecryptionServiceError.noResponsibleHealthDepartment
        }

        guard let messageIdParsed = Data(base64Encoded: message.messageId),
              let iv = Data(base64Encoded: message.iv),
              let encryptedData = Data(base64Encoded: message.data),
              let serverHMAC = Data(base64Encoded: message.mac) else {
            throw LucaConnectDecryptionServiceError.invalidMessage
        }

        let healthDepartmentEncryptionPublicKey = try KeyFactory.create(from: keyData, type: .ecsecPrimeRandom, keyClass: .public)
        let ecdh: Data = try ECDHSharedSecretKeySource(
            publicKeySource: ValueKeySource(key: healthDepartmentEncryptionPublicKey),
            privateKeySource: ValueKeySource(key: messageEncryptionKeyPair.private)
        ).retrieveKey()

        let key = try HKDF(
            password: ecdh.bytes,
            salt: messageIdParsed.bytes,
            info: ("encryption".data(using: .utf8) ?? Data()).bytes,
            keyLength: 16,
            variant: .sha2(.sha256)
        ).calculate()

        let authKey = try HKDF(
            password: ecdh.bytes,
            salt: messageIdParsed.bytes,
            info: ("authentication".data(using: .utf8) ?? Data()).bytes,
            keyLength: 16,
            variant: .sha2(.sha256)
        ).calculate()

        let hmac = HMACSHA256(key: Data(authKey))

        try hmac.verify(message: messageIdParsed + encryptedData + iv, receivedHMAC: serverHMAC)

        let decryptedData = try AESCTRCrypto(keySource: ValueRawKeySource(key: Data(key)), iv: iv.bytes).decrypt(data: encryptedData)

        let decryptedMessage = try JSONDecoder().decode(FetchedMessageContent.self, from: decryptedData)

        return FetchedMessage(
            healthDepartmentId: healthDepartment.departmentId,
            messageId: message.messageId,
            message: decryptedMessage,
            createdAt: Date(timeIntervalSince1970: TimeInterval(message.createdAt)),
            readDate: nil
        )
    }

}
extension HealthDepartment {

    // swiftlint:disable implicit_getter
    var jwtSignedPublicHDEKP: JWT<KeyIssuerClaims> {
        get throws { try JWT<KeyIssuerClaims>(jwtString: signedPublicHDEKP) }
    }

    // swiftlint:disable implicit_getter
    var jwtSignedPublicHDSKP: JWT<KeyIssuerClaims> {
        get throws { try JWT<KeyIssuerClaims>(jwtString: signedPublicHDSKP) }
    }
}
