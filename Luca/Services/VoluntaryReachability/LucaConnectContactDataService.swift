import Foundation
import DependencyInjection
import RxSwift
import PhoneNumberKit
import RxAppState

enum LucaConnectContactDataServiceError: LocalizedTitledError {
    case userNotEligible // No vaccination or recovery document
}

extension LucaConnectContactDataServiceError {
    var localizedTitle: String {
        L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        "\(self)"
    }
}

/// Service handling Contact Data Feature.
///
/// This service is responsible for updating daily data reference.
class LucaConnectContactDataService: Toggleable {
    @InjectStatic(\.powService) private var powService
    @InjectStatic(\.documentPersonAssociationService) private var documentPersonAssociationService
    @InjectStatic(\.documentProcessingService) private var documentProcessingService
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.keyValueRepo) private var keyValueRepo
    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.userKeysBundle) private var userKeysBundle
    @InjectStatic(\.lucaConnectHealthDepartmentService) private var lucaConnectHealthDepartmentService
    @InjectStatic(\.voluntaryReachabilityRequestBuilder) private var voluntaryReachabilityRequestBuilder
    @InjectStatic(\.backendVoluntaryReachability) private var backendVoluntaryReachability
    @InjectStatic(\.commonCrypto) private var commonCrypto
    @InjectStatic(\.localNewsMessageRepo) private var localMessagesRepo

    /// It contains currently subscribed `sendUpdate` instance. Will be reset on completion.
    private let currentUpdateCompletable = DispatchQueueSynced<Completable?>(nil)

    static let lucaConnectActivationNotificationKey = "LucaConnectActivationNotification"

    private let lastUpdateKey = "LucaConnectContactDataService.lastUpdateTimestamp"
    private let isFeatureEnabledKey = "LucaConnectContactDataService.isFeatureEnabled"

    private var disposeBag: DisposeBag?
    var isEnabled: Bool { disposeBag != nil }

    private let featureEnabledPublisher = PublishSubject<Bool>()
    let activationNotificationPublisher = PublishSubject<Bool>()

    /// Emits `UserContactData`
    func getUsersContactData() -> Single<UserContactData> {
        lucaPreferences.get(\.userRegistrationData)
            .map { userData in
                return UserContactData(
                    firstName: userData?.firstName ?? "",
                    lastName: userData?.lastName ?? "",
                    street: userData?.street ?? "",
                    houseNumber: userData?.houseNumber ?? "",
                    city: userData?.city ?? "",
                    postCode: userData?.postCode ?? "",
                    phoneNumber: userData?.phoneNumber ?? "",
                    email: userData?.email == "" ? nil : userData?.email
                )
            }
    }

    /// Emits users documents with status `vaccinated` or `recovery`
    func getEligibleDocuments() -> Single<[Document]> {
        let vaccinated = self.getLatestEligibleDocument(for: .vaccinated)
        let recovered = self.getLatestEligibleDocument(for: .recovered)
        let boostered = self.getLatestEligibleDocument(for: .boostered)

        return Observable.zip(vaccinated.asObservable(), recovered.asObservable(), boostered.asObservable()).map {
            [$0, $1, $2].compactMap {$0}
        }.asSingle()
    }

    /// Emits the latests document with the given `HealthStatus`
    private func getLatestEligibleDocument(for status: HealthStatus) -> Single<Document?> {
        documentPersonAssociationService
            .createUsersPerson()
            .asObservable()
            .map { [$0] }
            .flatMap(documentPersonAssociationService.documents(for:))
            .map { $0.values.flatMap { $0 } }
            .flatMap { Observable.from($0) }
            .compactMap { $0 as? ProvidesHealthStatus & Document & ContainsDateOfBirth }
            .deferredFilter { (document: Document & ProvidesHealthStatus & ContainsDateOfBirth ) -> Single<Bool> in
                document.healthStatus
                    .ifEmpty(default: HealthStatus.notShared)
                    .map { $0 == status }
            }
            .map { $0 as Document }
            .toArray()
            .flatMap { (documents: [Document]) -> Single<Document?> in
                let sortedDocs = documents.sorted(by: { $0.issuedAt.compare($1.issuedAt) == .orderedDescending })
                return .just(sortedDocs.first)
            }
    }

    /// Emits `true` if a responsible health department for user is known
    ///
    /// It checks if users postCode is under some health department jurisdiction with this feature enabled
    func hasResponsibleHealthDepartment() -> Single<Bool> {
        self.lucaConnectHealthDepartmentService
                            .getResponsibleHealthDepartment()
                            .map { _ in true}
                            .ifEmpty(switchTo: .just(false))
                            .catchAndReturn(false)
    }

    /// Emits `true` if user is eligible for this feature
    ///
    /// It checks if user has documents that give him the status `vaccinated` or `recovered` and his postCode is under some health department jurisdiction with this feature enabled
    func isUserEligible() -> Single<Bool> {
        lucaPreferences.get(\.uuid)
            .flatMap { uuid in
                if uuid == nil {
                    return .just(false)
                }
                return Single.zip(
                    self.hasResponsibleHealthDepartment(),
                    self.getEligibleDocuments().map { !$0.isEmpty }
                )
                    .map { $0 && $1 }
            }
    }

    /// Emits current value upon subscription and every subsequent change.
    func isFeatureEnabled() -> Observable<Bool> {
        return Observable.merge(keyValueRepo.loadOptional(isFeatureEnabledKey).map { $0 ?? false }.asObservable(), featureEnabledPublisher)
    }

    /// Emits `true` if this feature has been once enabled by user and he is eligible, `false` otherwise
    func isUserEligibleFeatureEnabled() -> Single<Bool> {
        Single.zip(
            isFeatureEnabled().take(1).asSingle(),
            isUserEligible()
        )
        .map { $0 && $1 }
    }

    /// Enables the feature if user is eligible and sends the update to backend
    ///
    /// - Parameter requestedPoWChallengeId: If there is already a PoW challenge enqueued or computed, you can give its ID to use it instead of requesting a new one. If `nil`, there will be a new PoW challenge requested.
    func enableFeature(requestedPoWChallengeId: String?) -> Completable {
        isUserEligibleFeatureEnabled().flatMapCompletable { isEnabled in
            if isEnabled {
                return .empty()
            }
            return self.isUserEligible()
                .flatMapCompletable { isEligible in
                    if !isEligible {
                        throw LucaConnectContactDataServiceError.userNotEligible
                    }
                    // Trigger notification seed generation
                    try self.userKeysBundle.generateKeys()
                    return self.keyValueRepo.store(self.isFeatureEnabledKey, value: true)
                        .andThen(self.sendUpdateIfNeeded(requestedPoWChallengeId: requestedPoWChallengeId))
                        .andThen(Completable.from { self.featureEnabledPublisher.onNext(true) })
                }
        }
    }

    /// Disables the feature and removes users data from local store and backend.
    func disableFeature() -> Completable {
        deleteData()
            .andThen(keyValueRepo.store(isFeatureEnabledKey, value: false))
            .andThen(Completable.from { self.featureEnabledPublisher.onNext(false) })
    }

    /// Emits `true` if the feature is available and has not been enabled
    /*
     determine whether to show the button to show the voluntary reachability dialog; based on:
     - is the feature already enabled?
     - does the health department support the feature?
     */

    var canBeActivated: Single<Bool> {
        lucaPreferences.get(\.uuid)
            .flatMap { [weak self] uuid in
                guard let self = self, uuid != nil else { return .just(false) }
                return self.isUserEligibleFeatureEnabled()
            }
            .flatMap { (enabled: Bool) in
                if enabled == true { return Single.just(false) }
                return self.lucaConnectHealthDepartmentService
                    .getResponsibleHealthDepartment()
                    .map { _ in true}
                    .catchAndReturn(false)
                    .ifEmpty(switchTo: .just(false))
            }
    }

    /// Deletes the data on the backend.
    func deleteData() -> Completable {
        Single.from {
            let threshold = Calendar.current.date(byAdding: .day, value: -7, to: self.timeProvider.now) ?? self.timeProvider.now
            return self.userKeysBundle.reachabilityDailyAuthKeyPair.indices.filter {
                $0.timestamp > threshold || Calendar.current.isDate($0.timestamp, inSameDayAs: threshold)
            }
        }
        .flatMapCompletable { (indices: [ReachabilityAuthKeyPairIndex]) in
            Completable.zip(indices.map { self.deleteData(for: $0) })
        }
        .andThen(Completable.from { self.userKeysBundle.removeReachabilityKeys() })
        .andThen(keyValueRepo.remove(lastUpdateKey))
    }

    /// Deletes data locally and remotely for one index (day)
    private func deleteData(for index: ReachabilityAuthKeyPairIndex) -> Completable {
        Single.from { try self.userKeysBundle.reachabilityDailyAuthKeyPair.restore(index: index) }
            .map { (restoredKeyPair: KeyPair<SecKey>) -> DeleteReachabilityContactDataRequest in
                let ecdsa = ECDSA(privateKeySource: ValueKeySource(key: restoredKeyPair.private), publicKeySource: nil)
                let signatureContent = ["DELETE_CONNECTCONTACT", index.contactId].compactMap { $0.data(using: .utf8) }
                let sha = self.commonCrypto.concatSHA256(data: signatureContent)
                let signature = try ecdsa.sign(data: sha)
                return DeleteReachabilityContactDataRequest(
                    contactId: index.contactId,
                    timestamp: index.timestamp,
                    signature: signature.base64EncodedString()
                )
            }
            .flatMapCompletable { self.backendVoluntaryReachability.deleteData(data: $0).asCompletable().onErrorComplete() }
            .andThen(Completable.from { self.userKeysBundle.reachabilityDailyAuthKeyPair.remove(index: index) })
    }

    /// Sends update when needed.
    ///
    /// It does following:
    /// - if `requestedPoWChallengeId` is nil it requests a POW challenge and solves it in background, otherwise it waits for the given challenge to be solved.
    /// - As soon as this PoW challenge is solved, eligible documents and user data will be collected
    /// - The request will be built and encrypted
    /// - The request will be sent and the auth key pair will be saved to `usersKeysBundle.reachabilityDailyAuthKeyPair`
    /// - The timestamp will be saved to `lastUpdateKey`.
    func sendUpdateIfNeeded(requestedPoWChallengeId: String?) -> Completable {
        shouldSendUpdate()
            .flatMapCompletable { shouldUpdate in
                if shouldUpdate {
                    return self.sendUpdateShared(requestedPoWChallengeId: requestedPoWChallengeId)
                }
                return .empty()
            }
    }

    private func sendUpdateShared(requestedPoWChallengeId: String?) -> Completable {
        currentUpdateCompletable.readRx()
            .flatMapCompletable { currentUpdate in

                // If there is already an update running, return it and ignore the parameter `requestedPoWChallengeId`
                if let runningUpdate = currentUpdate {
                    return runningUpdate
                }

                // if no, create a new one and save it to currentUpdateCompletable
                let challengeId = Single<PoWChallenge.Id>.deferred {
                    if let id = requestedPoWChallengeId {
                        return .just(id)
                    }
                    return self.powService.requestAndScheduleChallenge(type: .createConnectContact)
                }
                let updateCompletable = challengeId
                    .flatMapCompletable {
                        self.sendUpdate(challengeId: $0)
                        .andThen(self.currentUpdateCompletable.writeRx { _ in nil })
                    }
                    .asObservable()
                    .share(replay: 1, scope: .whileConnected)
                    .ignoreElementsAsCompletable()

                return self.currentUpdateCompletable.writeRx { _ in
                    updateCompletable
                }.andThen(updateCompletable)
            }
    }

    private func sendUpdate(challengeId: PoWChallenge.Id) -> Completable {
        Completable.deferred { [weak self] in
            guard let self = self else {
                return .empty()
            }
            let challenge = self.challenge(for: challengeId)
            let documents = self.getEligibleDocuments()
            let userData = self.getUsersContactData()
            let kritisData = self.lucaPreferences.get(\.kritisData)
            return Single.zip(userData, kritisData, challenge, documents)
                .flatMapCompletable { userData, kritisData, solution, documents in
                    let authKeyPair = try KeyFactory.createECKeyPair(tag: "authKeyPair")
                    let ephemeralKeyPair = try KeyFactory.createECKeyPair(tag: "ephemeralKeyPair")
                    return self.voluntaryReachabilityRequestBuilder.buildRequest(
                        for: userData,
                        powSolution: solution,
                        selectedDocuments: documents,
                        authKeyPair: authKeyPair,
                        ephemeralKeyPair: ephemeralKeyPair,
                        kritisData: kritisData
                    )
                    .flatMapCompletable { self.backendVoluntaryReachability
                        .uploadContact(data: $0)
                        .asSingle()
                        .do(onSuccess: { result in
                            try self.userKeysBundle.reachabilityDailyAuthKeyPair.store(
                                key: authKeyPair,
                                index: ReachabilityAuthKeyPairIndex(timestamp: self.timeProvider.now, contactId: result.contactId)
                            )
                        })
                        .asCompletable()
                    }
                }
                .andThen(self.keyValueRepo.store(self.lastUpdateKey, value: self.timeProvider.now))
        }
        .asObservable()
        .logError(self, "Sending daily update")
        .share(replay: 1, scope: .whileConnected)
        .asCompletable()
    }

    private func challenge(for challengeId: PoWChallenge.Id) -> PrimitiveSequence<SingleTrait, PoWChallengeSolution> {
        return self.powService.task(of: challengeId)
            .filter {
                switch $0.state {
                case PoWChallengeTask.State.completed(solution: _): return true
                case PoWChallengeTask.State.failed(error: _): return true
                default: return false
                }
            }
            .take(1)
            .asSingle()
            .map { task -> PoWChallengeSolution in
                if case let PoWChallengeTask.State.completed(solution: solution) = task.state {
                    return solution
                }
                if case let PoWChallengeTask.State.failed(error: error) = task.state {
                    throw error
                }
                throw NSError(domain: "Unknown error while solving PoW Challenge", code: 0, userInfo: nil)
            }
    }

    private func shouldSendUpdate() -> Single<Bool> {
        isUserEligibleFeatureEnabled()
            .flatMap { isEnabledAndEligible in
                if !isEnabledAndEligible {
                    return .just(false)
                }
                return self.keyValueRepo.loadOptional(self.lastUpdateKey)
                    .map { (last: Date?) in
                        if let last = last {
                            return !Calendar.current.isDate(last, inSameDayAs: self.timeProvider.now)
                        }
                        return true
                    }
            }
    }

    func enable() {
        if isEnabled {
            return
        }
        let newDisposeBag = DisposeBag()

        documentProcessingService.onDocumentWillDelete.addListener(for: self.unsafeAddress) {
            self.processDocumentDeletion(document: $0)
                .logError(self, "Error processing document delete")
        }

        let updateSignal = Observable.merge(
            lucaPreferences.currentAndChanges(\.uuid).filter { $0 != nil }.map { _ in Void() },
            UIApplication.shared.rx
                .currentAndChangedAppState
                .filter { $0 == .active }
                .map { _ in Void() }
        )

        updateSignal
            .flatMapFirst { _ in
                self.sendUpdateIfNeeded(requestedPoWChallengeId: nil)
                    .onErrorComplete()
            }
            .subscribe()
            .disposed(by: newDisposeBag)

        // Trigger notification for luca connect activation in responsible HD
        (backgroundState() + postCodeConsentGranted())
            .flatMapFirst { _ in
                self.lucaConnectHealthDepartmentService
                    .getResponsibleHealthDepartment()
                    .asObservable()
                    .catch { _ in .empty() }
                    .deferredFilter { _ in self.lucaPreferences.get(\.lucaConnectNotificationSent).map { !$0 } }
                    .deferredFilter({ _ in self.isUserEligibleFeatureEnabled().map { !$0 } })
                    .observe(on: MainScheduler.instance)
                    .flatMap { _ -> Single<LocalNewsMessage> in
                        let message = LocalNewsMessage(type: .lucaConnectActivate, createDate: self.timeProvider.now)
                        return self.localMessagesRepo.store(object: message)
                    }
                    .do(onNext: { message in
                        NotificationScheduler.shared.scheduleNotification(
                            title: message.title,
                            message: message.message,
                            requestIdentifier: LucaConnectContactDataService.lucaConnectActivationNotificationKey
                        )
                    })
                    .flatMap { _ in self.lucaPreferences.set(\.lucaConnectNotificationSent, value: true) }
                    .ignoreElementsAsCompletable()
                    .onErrorComplete()
            }
            .subscribe()
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

    private func backgroundState() -> Observable<Void> {
        return UIApplication.shared.rx
            .currentAndChangedAppState
            .filter { $0 == .background }
            .map { _ in Void() }
    }

    private func postCodeConsentGranted() -> Observable<()> {
        return lucaPreferences
            .currentAndChanges(\.lucaConnectPostCodeUsageConsent)
            .filter { $0 == true }
            .map { _ in Void() }
    }

    func disable() {
        documentProcessingService.onDocumentWillDelete.removeListener(for: self.unsafeAddress)
        disposeBag = nil
    }

    /// check if user deleted a document that was used as an `eligible` document. If so, deactivate LucaConnect
    private func processDocumentDeletion(document: Document) -> Completable {
        self.getEligibleDocuments()
            .flatMapCompletable { [weak self] (docs: [Document]) in
                guard let self = self else {return .empty()}
                if docs.contains(where: {$0.identifier == document.identifier}) {
                    return self.disableFeature()
                }
                return .empty()
            }
    }
}

extension LucaConnectContactDataService: UnsafeAddress, LogUtil {}
