import Foundation
import DependencyInjection
import RxSwift
import CryptoSwift
import RxAppState

enum LucaConnectMessageFetcherServiceError: LocalizedTitledError {
    case noResponsibleHealthDepartment
    case noNotificationSeed
    case noMessageEncryptionKeyPair
    case noMessageSigningKeyPair
}

extension LucaConnectMessageFetcherServiceError {
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        return "\(self)"
    }
}

class LucaConnectMessageFetcherService: AppStateHandler {

    @InjectStatic(\.keyValueRepo) private var keyValueRepo
    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.lucaConnectHealthDepartmentService) private var healthDepartmentService
    @InjectStatic(\.lucaConnectContactDataService) private var contactDataService
    @InjectStatic(\.backendVoluntaryReachability) private var backend
    @InjectStatic(\.fetchedMessagesRepo) private var fetchedMessagesRepo
    @InjectStatic(\.commonCrypto) private var commonCrypto
    @InjectStatic(\.userKeysBundle) private var userKeysBundle
    @InjectStatic(\.lucaConnectDecryptionService) private var lucaConnectDecryptionService

    private let newMessagesPublisher = PublishSubject<FetchedMessage>()
    var newMessages: Observable<FetchedMessage> {
        newMessagesPublisher.asObservable()
    }

    private var disposeBag: DisposeBag?
    var isEnabled: Bool {
        disposeBag != nil
    }

    private let lastFetchTimestampKey = "lucaConnectLastFetchTimestamp"
    private let timestampGranularity: TimeInterval = 300
    private let archiveDuration = Validity(unit: .day, count: 7)
    private var archiveBegin: Date {
        Calendar.current.date(byAdding: archiveDuration.unit, value: -archiveDuration.count, to: timeProvider.now) ?? timeProvider.now
    }
    private var maxArchiveSize: Int {
        let now = roundDateToLastTimeslice(timeProvider.now)
        let lowerBoundary = archiveBegin
        let diff = now.timeIntervalSince1970 - lowerBoundary.timeIntervalSince1970
        return Int(diff / timestampGranularity)
    }

    private lazy var fetchMessageShared: Single<[FetchedMessage]> = {
        Single.deferred { [weak self] in
            guard let self = self else {
                return .just([])
            }
            return Single.zip(

                self.healthDepartmentService
                    .getResponsibleHealthDepartment()
                    .ifEmpty(switchTo: .error(LucaConnectMessageFetcherServiceError.noResponsibleHealthDepartment)),

                Maybe.from { try? self.userKeysBundle.reachabilityNotificationSeed.retrieveKey() }
                    .ifEmpty(switchTo: .error(LucaConnectMessageFetcherServiceError.noNotificationSeed)),

                self.generateUnfetchedTimestamps()
            )
            .flatMap { healthDepartment, notificationSeed, unfetchedTimestamps in
                if unfetchedTimestamps.isEmpty {
                    return .just([])
                }
                let messageIds: [String] = Array(try unfetchedTimestamps.map {
                    try self.messageId(
                        for: $0,
                        healthDepartmentId: healthDepartment.departmentId,
                        notificationSeed: notificationSeed
                    )
                })
                let randomMessageIds = (0..<max(0, self.maxArchiveSize - messageIds.count)).compactMap { _ in try? self.generateRandomMessageId() }
                return self.backend.fetchMessages(messageIds: messageIds + randomMessageIds)
                    .asSingle()
                    .map { try $0
                        .filter { !randomMessageIds.contains($0.messageId) }
                        .map { try self.lucaConnectDecryptionService.decrypt(message: $0, healthDepartment: healthDepartment) }
                    }
                    .flatMap { self.fetchedMessagesRepo.store(objects: $0) }
                    .flatMap { messages in
                        self.keyValueRepo.store(self.lastFetchTimestampKey, value: unfetchedTimestamps.last ?? self.timeProvider.now)
                            .andThen(.just(messages))
                    }
            }
            .asObservable()
            .do(onNext: { messages in
                for msg in messages {
                    self.newMessagesPublisher.onNext(msg)
                }
            })
            .share(replay: 1, scope: .whileConnected)
            .asSingle()
            .logError(self, "Message fetching")
        }
    }()

    func fetchMessages() -> Single<[FetchedMessage]> {
        fetchMessageShared
    }

    func markAsRead(_ message: FetchedMessage) -> Single<FetchedMessage> {
        if message.readDate != nil {
            return .just(message)
        }
        return Single.deferred {
            let now = self.timeProvider.now
            let signatureContent = self.commonCrypto.concatSHA256(data: [
                "READ_MESSAGE".data(using: .utf8) ?? Data(),
                message.messageId.data(using: .utf8) ?? Data(),
                Int32(now.timeIntervalSince1970).data
            ])
            let keyPair = try self.userKeysBundle.reachabilityMessageSigningKeyPair.restore()
            let signature = try ECDSA(privateKeySource: ValueKeySource(key: keyPair.private), publicKeySource: nil).sign(data: signatureContent)
            let payload = ReadMessageRequest(
                messageId: message.messageId,
                timestamp: Int(now.timeIntervalSince1970),
                signature: signature.base64EncodedString()
            )
            return self.backend.markAsRead(payload: payload).asCompletable()
                .andThen(Single.just(now))
        }
        .flatMap { (readDate: Date) in
            var changedMessage = message
            changedMessage.readDate = readDate
            return self.fetchedMessagesRepo.store(object: changedMessage)
        }
        .debug("MARK AS READ")
    }

    private func generateUnfetchedTimestamps() -> Single<[Date]> {
        keyValueRepo
            .loadOptional(lastFetchTimestampKey)
            .map { $0 ?? self.archiveBegin }
            .map { lastTimestamp in
                let now = Date(timeIntervalSince1970: self.roundDateToLastTimeslice(self.timeProvider.now).timeIntervalSince1970 - self.timestampGranularity)
                var iteration = Date(timeIntervalSince1970: self.roundDateToLastTimeslice(lastTimestamp).timeIntervalSince1970 + self.timestampGranularity)
                var retVals: [Date] = []
                while iteration <= now {
                    retVals.append(iteration)
                    iteration = Date(timeIntervalSince1970: iteration.timeIntervalSince1970 + self.timestampGranularity)
                }
                return retVals
            }
    }

    /// Used to generate some fakes for lowering the possibility of request linkability.
    private func generateRandomMessageId() throws -> String {
        let lowerBound = archiveBegin
        let range = timeProvider.now.timeIntervalSince1970 - lowerBound.timeIntervalSince1970
        let dateInBetween = Date(timeIntervalSince1970: TimeInterval.random(in: 0..<range) + lowerBound.timeIntervalSince1970)
        guard let notificationSeed = KeyFactory.randomBytes(size: 16),
              let uuid = UUID(version: .v4, variant: .variant1)else {
            throw NSError(domain: "Couldn't create random data", code: 0, userInfo: nil)
        }
        return try messageId(for: dateInBetween, healthDepartmentId: uuid.uuidString, notificationSeed: notificationSeed)
    }

    func messageId(for timeslice: Date, healthDepartmentId: String, notificationSeed: Data) throws -> String {
        guard let uuid = UUID(uuidString: healthDepartmentId) else {
            throw NSError(domain: "Invalid HD UUID", code: 0, userInfo: nil)
        }

        let rounded = roundDateToLastTimeslice(timeslice)
        let timesliceData = Int32(rounded.timeIntervalSince1970).data
        let ikm = notificationSeed
        let salt = timesliceData + uuid.data
        let hkdf = try HKDF(
            password: ikm.bytes,
            salt: salt.bytes,
            info: ("messageId".data(using: .utf8) ?? Data()).bytes,
            keyLength: 16,
            variant: .sha2(.sha256)
        ).calculate()
        return Data(hkdf).base64EncodedString()
    }

    private func roundDateToLastTimeslice(_ date: Date) -> Date {
        Date(timeIntervalSince1970: Double(Int(date.timeIntervalSince1970 / timestampGranularity)) * timestampGranularity)
    }

    func onAppStateChange(_ appState: AppState) {
        guard appState == .background || appState == .active else {
            return
        }
        _ = contactDataService
            .isFeatureEnabled()
            .take(1)
            .filter { $0 }
            .flatMap { [weak self] _ in self?.fetchMessageShared.catchAndReturn([]) ?? .just([]) }
            .logError(self, "Fetching messages")
            .subscribe()
    }
}

extension LucaConnectMessageFetcherService: UnsafeAddress, LogUtil {}
