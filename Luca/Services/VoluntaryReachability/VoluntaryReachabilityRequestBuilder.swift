import Foundation
import DependencyInjection
import RxSwift
import CryptorRSA
import PhoneNumberKit
import SwiftJWT

struct UserContactData: Codable {
    var firstName: String
    var lastName: String
    var street: String
    var houseNumber: String
    var city: String
    var postCode: String

    /// E.164 format
    var phoneNumber: String
    var email: String?
    var version: Int = 1

    enum CodingKeys: String, CodingKey {
        case firstName = "fn"
        case lastName = "ln"
        case street = "st"
        case houseNumber = "hn"
        case city = "c"
        case postCode = "pc"
        case phoneNumber = "pn"
        case email = "e"
        case version = "v"
    }
}

extension UserContactData: HasFirstAndLastName {}
extension UserContactData: HasAddress {}

struct ReachableContactPayload: Codable {
    var contactData: UserContactData

    /// Min 1 max 2 certificates
    var certificates: [String]
    var notificationSeed: String
    var encryptionPublicKey: String
    var signaturePublicKey: String
    var deviceType: Int = 1
    var version: Int = 1
    var criticalInfrastructure: Bool?
    var vulnerableGroup: Bool?
    var industry: String?
    var company: String?

    enum CodingKeys: String, CodingKey {
        case contactData = "cd"
        case certificates = "ct"
        case notificationSeed = "ns"
        case encryptionPublicKey = "ep"
        case signaturePublicKey = "sp"
        case deviceType = "dt"
        case criticalInfrastructure = "ci"
        case vulnerableGroup = "vg"
        case industry = "jb"
        case company = "em"
        case version = "v"
    }
}

struct ReachableContactRequest: Codable {
    var namePrefix: String
    var phonePrefix: String
    var authPublicKey: String
    var departmentId: String
    var reference: DLIESEncryptedData
    var data: DLIESEncryptedData
    var signature: String
    var pow: PoWChallengeSolution
}

class VoluntaryReachabilityRequestBuilder {
    @InjectStatic(\.userKeysBundle) private var userKeysBundle
    @InjectStatic(\.lucaConnectHealthDepartmentService) private var lucaConnectHealthDepartmentService
    @InjectStatic(\.commonCrypto) private var crypto

    // swiftlint:disable function_parameter_count
    func buildRequest(
        for usersData: UserContactData,
        powSolution: PoWChallengeSolution,
        selectedDocuments: [Document],
        authKeyPair: KeyPair<SecKey>,
        ephemeralKeyPair: KeyPair<SecKey>,
        kritisData: KritisData?) -> Single<ReachableContactRequest> {
        lucaConnectHealthDepartmentService.getResponsibleHealthDepartment()
        .ifEmpty(switchTo: .error(NSError(domain: "No luca enabled HDs", code: 0, userInfo: nil)))
        .map { healthDepartment in
            let normalisedName = Person(firstName: usersData.firstName, lastName: usersData.lastName, type: .user).simplifiedFullName
            let normalisedPhoneNumber = try self.getNormalisedPhoneNumber(from: usersData)

            let fullNameHashPrefix = self.hashPrefix(of: normalisedName.data(using: .utf8)!)
            let phoneHashPrefix = self.hashPrefix(of: normalisedPhoneNumber.data(using: .utf8)!)

            let authPublicKeyCompressed = try KeyFactory.compressPublicEC(key: authKeyPair.public)

            let dlies = DLIES(
                ephemeralKeyPair: ephemeralKeyPair,
                receiverPublicKeySource: ValueKeySource(key: try self.getEncryptionPublicKey(for: healthDepartment)),
                compressPublicKey: true
            )

            let notificationSeed = try self.userKeysBundle.reachabilityNotificationSeed.retrieveKey()

            let request = ReachableContactRequest(
                namePrefix: fullNameHashPrefix.base64EncodedString(),
                phonePrefix: phoneHashPrefix.base64EncodedString(),
                authPublicKey: authPublicKeyCompressed.base64EncodedString(),
                departmentId: healthDepartment.departmentId,
                reference: try self.createEncryptedReference(normalisedFullName: normalisedName, normalisedPhoneNumber: normalisedPhoneNumber, notificationSeed: notificationSeed, with: dlies),
                data: try self.createEncryptedPayload(usersData: usersData, kritisData: kritisData, notificationSeed: notificationSeed, certificates: selectedDocuments, with: dlies),
                signature: "",
                pow: powSolution
            )
            return try self.sign(request: request, privateKey: authKeyPair.private)
        }
    }

    private func sign(request: ReachableContactRequest, privateKey: SecKey) throws -> ReachableContactRequest {
        let signatureComponents = [
            "CREATE_CONNECTCONTACT",
            request.departmentId,
            request.namePrefix,
            request.phonePrefix,
            request.reference.data,
            request.reference.publicKey ?? "",
            request.reference.iv,
            request.reference.mac,
            request.data.data,
            request.data.publicKey ?? "",
            request.data.iv,
            request.data.mac
        ]

        let signatureContent = signatureComponents.compactMap { $0.data(using: .utf8) }

        let ecdsa = ECDSA(privateKeySource: ValueKeySource(key: privateKey), publicKeySource: nil)

        let signature = try ecdsa.sign(data: crypto.concatSHA256(data: signatureContent))

        var requestCopy = request
        requestCopy.signature = signature.base64EncodedString()
        return requestCopy
    }

    private func getEncryptionPublicKey(for hd: HealthDepartment) throws -> SecKey {
        let keyString = try JWT<KeyIssuerClaims>(jwtString: hd.signedPublicHDEKP).claims.key
        guard let keyData = Data(base64Encoded: keyString) else {
            throw NSError(domain: "No key data in signedPublicHDEKP", code: 0, userInfo: nil)
        }
        return try KeyFactory.create(from: keyData, type: .ecsecPrimeRandom, keyClass: .public)
    }

    private func getNormalisedPhoneNumber(from: UserContactData) throws -> String {
        let phoneKit = PhoneNumberKit()
        let phoneNumber = try phoneKit.parse(from.phoneNumber)
        return phoneKit.format(phoneNumber, toType: .e164)
    }

    private func createEncryptedReference(normalisedFullName: String, normalisedPhoneNumber: String, notificationSeed: Data, with dlies: DLIES) throws -> DLIESEncryptedData {
        try dlies.encrypt(data: buildReferenceData(
            fullNameHash: normalisedFullName.data(using: .utf8)!.sha256(),
            phoneNumberHash: normalisedPhoneNumber.data(using: .utf8)!.sha256(),
            notificationSeed: notificationSeed)
        )
    }

    private func createEncryptedPayload(usersData: UserContactData, kritisData: KritisData?, notificationSeed: Data, certificates: [Document], with dlies: DLIES) throws -> DLIESEncryptedData {
        let messageSigningPublicSecKey = try userKeysBundle.reachabilityMessageSigningKeyPair.restore().public
        let messageEncryptionPublicSecKey = try userKeysBundle.reachabilityMessageEncryptionKeyPair.restore().public

        let kritis = (kritisData?.vulnerableGroup ?? false) || (kritisData?.criticalInfrastructure ?? false)
        let industry = sanitize(data: kritisData?.industry)
        let company = sanitize(data: kritisData?.company)

        let messageSigningPublicKey: Data = try KeyFactory.compressPublicEC(key: messageSigningPublicSecKey)
        let messageEncryptionPublicKey: Data = try KeyFactory.compressPublicEC(key: messageEncryptionPublicSecKey)
        let payload = ReachableContactPayload(
            contactData: usersData,
            certificates: certificates.map { $0.originalCode },
            notificationSeed: notificationSeed.base64EncodedString(),
            encryptionPublicKey: messageEncryptionPublicKey.base64EncodedString(),
            signaturePublicKey: messageSigningPublicKey.base64EncodedString(),
            criticalInfrastructure: kritisData?.criticalInfrastructure == false ? nil : kritisData?.criticalInfrastructure,
            vulnerableGroup: kritisData?.vulnerableGroup == false ? nil : kritisData?.vulnerableGroup,
            industry: kritis && industry != "" && industry != nil ? industry.prefix(chars: 100) : nil ,
            company: kritis && company != "" && company != nil ? company.prefix(chars: 100) : nil
        )
        return try dlies.encrypt(data: try JSONEncoder().encode(payload))
    }

    // Sanitize to prevent csv injections and accidental downloads when data is exported. Different from general sanitization method in characters and substrings.
    private func sanitize(data: String?) -> String? {
        let sanitized = data?.replacingOccurrences(of: "[^\\w !&()+,./:@`|£À-ÿāăąćĉċčđēėęěĝğģĥħĩīįİıĵķĸĺļłńņōőœŗřśŝşšţŦũūŭůűųŵŷźżžơưếệ–-]", with: " ", options: .regularExpression)
            .replacingOccurrences(of: "http", with: " ")
            .replacingOccurrences(of: "ftp", with: " ")
            .trimmingCharacters(in: .whitespacesAndNewlines)
        return sanitized
    }

    private func buildReferenceData(fullNameHash: Data, phoneNumberHash: Data, notificationSeed: Data) -> Data {
        Data([0x01] + fullNameHash + phoneNumberHash + notificationSeed)
    }

    private func hashPrefix(of data: Data) -> Data {
        var prefix = data.sha256().prefix(3)
        prefix[2] = prefix[2] & 0xE0
        return prefix
    }
}
