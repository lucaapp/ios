import RxSwift
import DependencyInjection

enum LucaConnectHealthDepartmentServiceError: LocalizedTitledError {
    case postCodeConsentNotGranted
}

extension LucaConnectHealthDepartmentServiceError {
    var localizedTitle: String {
        return L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        switch self {
        case .postCodeConsentNotGranted: return L10n.IOSApp.Postcode.Check.Activation.error
        }
    }
}

class LucaConnectHealthDepartmentService: Toggleable {
    @InjectStatic(\.backendVoluntaryReachability) private var backend
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.lucaIDTermsOfUse) private var lucaIDTermsOfUse
    @InjectStatic(\.keyValueRepo) private var keyValueRepo
    @InjectStatic(\.localNewsMessageRepo) private var localMesssagesRepo
    @InjectStatic(\.timeProvider) private var timeProvider

    private var disposeBag: DisposeBag?
    var isEnabled: Bool {
        disposeBag != nil
    }

    private let currentHealthDepartmentIDKey = "CurrentHealthDepartment"
    private let newHDIDPublisher = PublishSubject<(previous: String?, new: String?)>()
    var onHealthDepartmentChange: Observable<(previous: String?, new: String?)> {
        newHDIDPublisher.asObservable()
    }

    private lazy var cachedResponsibleHealthDepartments: CachedDataSource<[HealthDepartment]> = {
        BaseCachedDataSource(
            dataSource: backend.fetchHealthDepartments(),
            cacheDataRepo: KeyValueRepoCacheWrapper(uniqueCacheKey: "HealthDepartmentCache"),
            cacheValidity: .onceIn(unit: .day),
            uniqueCacheIdentifier: "HealthDepartmentCacheKey"
        )
    }()

    func getResponsibleHealthDepartment() -> Maybe<HealthDepartment> {
        checkIfUserGaveConsentForUsingPostCode()
            .andThen(lucaPreferences.get(\.userRegistrationData))
            .compactMap { $0?.postCode }
            .flatMap { postCode in
                self.cachedResponsibleHealthDepartments
                    .retrieve()
                    .compactMap { $0.first ?? [] }
                    .map { $0.filter { $0.zipCodes.contains(postCode) } }
            }
            .flatMap { (responsibleHDs: [HealthDepartment]) in
                self.keyValueRepo.loadOptional(self.currentHealthDepartmentIDKey)
                    .asMaybe()
                    .flatMap { (currentHDIDOptional: String?) -> Maybe<HealthDepartment> in
                        guard let currentHDID = currentHDIDOptional else {
                            if let someHD = responsibleHDs.first {
                                return self.emitNewHD(nil, newHDID: someHD.departmentId).andThen(.just(someHD))
                            }
                            return .empty()
                        }

                        if responsibleHDs.isEmpty {
                            return self.emitNewHD(currentHDID, newHDID: nil).andThen(.empty())
                        }

                        // Is there previous HD still available?
                        if let stillSameHD = responsibleHDs.first(where: { $0.departmentId == currentHDID }) {
                            return .just(stillSameHD)

                        // Previously saved HD is not luca connect available anymore, find a new one if possible
                        } else {
                            if let someHD = responsibleHDs.first {
                                return self.emitNewHD(currentHDIDOptional, newHDID: someHD.departmentId)
                                    .andThen(.just(someHD))
                            } else {
                                return .empty()
                            }
                        }
                    }
            }
    }

    private func checkIfUserGaveConsentForUsingPostCode() -> Completable {
        lucaPreferences
            .get(\.lucaConnectPostCodeUsageConsent)
            .do(onSuccess: { consentGranted in
                if consentGranted != true {
                    throw LucaConnectHealthDepartmentServiceError.postCodeConsentNotGranted
                }
            })
            .asCompletable()
    }

    /// Determines if there is a difference between current and new health department id. Save the new value if so and emit an update.
    private func emitNewHD(_ previousHDID: String?, newHDID: String?) -> Completable {
        Completable.deferred {
            if previousHDID != newHDID {
                return self.keyValueRepo
                    .store(self.currentHealthDepartmentIDKey, value: newHDID)
                    .andThen(Completable.from { self.newHDIDPublisher.onNext((previous: previousHDID, new: newHDID)) })
            }
            return .empty()
        }
    }

    func enable() {
        if isEnabled {
            return
        }

        let newDisposeBag = DisposeBag()

        let appStateSignal = UIApplication.shared.rx.currentAndChangedAppState
            .filter { $0 == .active }
            .map { _ in Void() }

        let userRegisteredSignal = lucaPreferences
            .currentAndChanges(\.uuid)
            .filter { $0 != nil }
            .map { _ in Void() }

        let updateSignal = appStateSignal + userRegisteredSignal

        updateSignal
            .flatMapFirst { _ in
                self.emitMessageWithZipCodeConsentIfNeeded()
                    .logError(self, "emitMessageWithZipCodeConsentIfNeeded")
                    .andThen(self.emitMessageForTermsOfUseIfNeeded())
                    .onErrorComplete()
            }
            .subscribe()
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

    func disable() {
        disposeBag = nil
    }

    private func emitMessageWithZipCodeConsentIfNeeded() -> Completable {
        Single.zip(
            lucaPreferences.get(\.uuid).map { $0 != nil },
            lucaPreferences.get(\.lucaConnectPostCodeUsageConsent).map { $0 ?? false },
            localMesssagesRepo.restore().map { $0.contains(where: { $0.type == .lucaConnectZipCodeUsageConsent }) }
        )
        .flatMapCompletable { userIsRegistered, postCodeUsageConsentGranted, localMessageAlreadyGenerated in
            guard userIsRegistered && !postCodeUsageConsentGranted && !localMessageAlreadyGenerated else {
                return .empty()
            }
            let message = LocalNewsMessage(type: .lucaConnectZipCodeUsageConsent, createDate: self.timeProvider.now)
            return self.localMesssagesRepo.store(object: message).asCompletable()
        }
    }

    private func emitMessageForTermsOfUseIfNeeded() -> Completable {
        Single.zip(
            lucaPreferences.get(\.uuid).map { $0 != nil },
            lucaIDTermsOfUse.isAccepted.map { $0 ?? false },
            localMesssagesRepo.restore().map { $0.contains(where: { $0.type == .newTermsOfUse }) }
        )
        .flatMapCompletable { userIsRegistered, granted, localMessageAlreadyGenerated in
            guard userIsRegistered && !granted && !localMessageAlreadyGenerated else {
                return .empty()
            }
            let message = LocalNewsMessage(type: .newTermsOfUse, createDate: self.timeProvider.now)
            return self.localMesssagesRepo.store(object: message).asCompletable()
        }
    }

}

extension LucaConnectHealthDepartmentService: LogUtil, UnsafeAddress {}
