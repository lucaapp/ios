import Foundation
import RxSwift
import DependencyInjection

class LucaPaymentConsent: TermsOfUseService<String> {
    init() {
        super.init(serviceKey: "LucaPayment", newestVersion: "108")
    }
}
