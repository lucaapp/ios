import DependencyInjection
import RxSwift
import SwiftJWT

private struct PaymentUserClaims: Claims {
    var sub: String
    var iat: Date?
    var exp: Date?
}

private struct LucaPaymentTemplate {
    let credentials     = PreferencesEntry(key: "credentials", type: LucaPaymentUserCredentials.self)
    let registeredUser  = PreferencesEntry(key: "registeredUser", type: PaymentUser.self)
    let lastToken       = PreferencesEntry(key: "lastToken", type: String.self)
    let revocationCode  = PreferencesEntry(key: "revocationCode", type: String.self)
    let lastSelectedTipPercentage = PreferencesEntryWithDefaultValue(key: "tipPercentage", type: Double.self, defaultValue: 0.1)
}

private struct LucaPaymentUserCredentials: Codable {
    var name: String
    var username: String
    var password: String
}

enum LucaPaymentServiceError: LocalizedTitledError {
    case userAlreadyExists
    case noCredentials
    case paymentNotActivated
}

extension LucaPaymentServiceError {
    var localizedTitle: String {
        L10n.IOSApp.Navigation.Basic.error
    }

    var errorDescription: String? {
        switch self {
        case .userAlreadyExists: return "\(self)"
        case .noCredentials: return "\(self)"
        case .paymentNotActivated: return "\(self)"
        }
    }
}

class LucaPaymentService: SharedProviderConnectionEventHandler {
    @InjectStatic(\.keyValueRepo) private var keyValueRepo
    @InjectStatic(\.backendPayment) private var backend
    @InjectStatic(\.timeProvider) private var timeProvider
    @InjectStatic(\.localNewsMessageRepo) private var localMesssagesRepo
    @InjectStatic(\.lucaIDMessageService) private var messageService
    @InjectStatic(\.campaignService) private var campaignService

    lazy private var preferences: PreferencesTemplate<LucaPaymentTemplate> = {
        keyValueRepo.createPreferencesTemplate(with: LucaPaymentTemplate(), settingsPrefixKey: "LucaPaymentService.")
    }()

    lazy private var historyCache: ParametrisedCachedDataSource<PaymentHistoryPage, String?>! = {
        BaseParametrisedCachedDataSource(
            dataSourceFactory: { [weak self] param -> DataSourceWrapper<PaymentHistoryPage>? in
                guard let self = self else { return nil }
                return DataSourceWrapper(source: self.fetchPaymentHistorySource(cursor: param).map { [$0] })
            },
            cacheDataRepoFactory: { KeyValueRepoCacheWrapper(uniqueCacheKey: "paymentHistoryCache.\($0 ?? "init")") },
            cacheValidity: .until(unit: .minute, count: 5),
            uniqueCacheIdentifier: "PaymentHistoryCacheKey"
        )
    }()

    var revocationCode: Single<String?> {
        preferences.get(\.revocationCode)
    }

    func didDisconnectFromSharedProvider() {
        historyCache = nil
    }

    func remove() -> Completable {
        retrieveAccessToken()
            .flatMapCompletable {
                self.backend.deleteUser(accessToken: $0)
                    .asCompletable()
                    .catch { (error) -> Completable in
                        if let error = error as? BackendError<PaymentDeleteUserError>,
                           let deleteError = error.backendError,
                           case PaymentDeleteUserError.userNotExists = deleteError {
                            return Completable.empty()
                        }
                        throw error
                    }
            }
            .andThen(preferences.remove(\.lastToken))
            .andThen(preferences.remove(\.credentials))
            .andThen(preferences.remove(\.registeredUser))
            .andThen(preferences.remove(\.lastSelectedTipPercentage))
            .andThen(preferences.remove(\.revocationCode))
            .andThen(invalidateCaches())
            .andThen(self.messageService.deleteNotifications(for: .paymentRevocation))
    }

    func pullTipPreference() -> Single<Double> {
        preferences.get(\.lastSelectedTipPercentage)
    }

    func setTipPreference(value: Double) -> Completable {
        preferences.set(\.lastSelectedTipPercentage, value: value)
    }

    /// Used to be able to get all new data with the next call of `fetchPaymentHistory` without needing to wait until cache expires.
    func invalidateCaches() -> Completable {
        historyCache.invalidateCache()
    }

    var isUserActivated: Single<Bool> {
        preferences.get(\.registeredUser).map { $0 != nil }
    }

    /// Emits current value on subscribe and every subsequent change until disposed
    var isUserActivatedChanges: Observable<Bool> {
        preferences.currentAndChanges(\.registeredUser).map { $0 != nil }
    }

    func activate() -> Completable {
        isUserActivated
            .flatMapCompletable { isActivated in
                if isActivated {
                    throw LucaPaymentServiceError.userAlreadyExists
                }
                return .empty()
            }
            .andThen(getOrCreateCredentials())
            .flatMap { self.backend.createPaymentUser(name: $0.name, username: $0.username, password: $0.password).asSingle() }
            .flatMapCompletable { self.preferences.set(\.revocationCode, value: $0.revocationCode) }
            .andThen(self.fetchUser(loadCached: false).asCompletable())
            .andThen(emitPaymentRevocationMessageIfNeeded())
    }

    func signIn() -> Completable {
        preferences.get(\.credentials)
            .map { (credentials: LucaPaymentUserCredentials?) -> LucaPaymentUserCredentials in
                guard let credentials = credentials else { throw LucaPaymentServiceError.noCredentials }
                return credentials
            }
            .flatMap { self.backend.signIn(username: $0.username, password: $0.password).asSingle() }
            .flatMapCompletable { self.preferences.set(\.lastToken, value: $0.accessToken) }
    }

    /// Retrieve history of successful payments
    func fetchPaymentHistory(cursor: String? = nil) -> Single<[PaymentHistoryPage]> {
        historyCache
            .retrieve(parameter: cursor)
            .debug("PAYMENT.HISTORY")
    }

    func fetchUser(loadCached: Bool = false) -> Single<PaymentUser> {
        if loadCached {
            return preferences.get(\.registeredUser)
                .map {
                    guard let user = $0 else {
                        throw LucaPaymentServiceError.paymentNotActivated
                    }
                    return user
                }
        }
        return retrieveAccessToken()
            .flatMap {
                self.backend.fetchUser(accessToken: $0).asSingle()
                    .flatMap { self.preferences.set(\.registeredUser, value: $0).andThen(.just($0)) }
            }
    }

    /// Used for user initiated payment
    func makePayment<C: Currency>(
        locationId: String,
        invoice: Money<C>,
        tip: Money<C>,
        table: String,
        waiter: String? = nil
    ) -> Single<PaymentCreateCheckoutResponse> {
        retrieveAccessToken()
            .flatMap {
                self.backend.createPayment(
                    accessToken: $0,
                    locationId: locationId,
                    invoice: invoice,
                    tip: tip,
                    table: table,
                    waiter: waiter
                )
                .asSingle()
            }
            .flatMap { checkout in
                self.invalidateCaches()
                    .andThen(.just(checkout))
            }
    }

    /// Used for operator initiated payment. Supports split payments by setting the `partialAmount` property.
    func makePayment<C: Currency>(
        for paymentRequest: PaymentFetchOpenRequestResponse,
        partialAmount: Money<C>? = nil,
        tip: Money<C>,
        table: String,
        waiter: String? = nil
    ) -> Single<PaymentCreateCheckoutResponse> {
        retrieveAccessToken()
            .flatMap {
                self.backend.createPayment(
                    accessToken: $0,
                    locationId: paymentRequest.locationId,
                    paymentRequestId: paymentRequest.uuid,
                    amount: partialAmount,
                    tip: tip,
                    table: table,
                    waiter: waiter
                )
                .asSingle()
            }
            .flatMap { checkout in
                self.invalidateCaches()
                    .andThen(.just(checkout))
            }
    }

    func fetchCheckout(id: String) -> Single<PaymentFetchCheckoutResponse> {
        backend.fetchCheckout(checkoutId: id).asSingle()
    }

    func fetchOpenRequest(forLocationId locationId: String, atTable table: String?) -> Single<PaymentFetchOpenRequestResponse?> {
        backend.fetchOpenPaymentRequest(locationId: locationId, table: table)
            .asInfallibleResult()
            .asObservable()
            .take(1)
            .asSingle()
            .map {
                switch $0 {
                case .failure(let error):
                    if error.backendError == .noOpenPayment { return nil } // It's not an error. It's just a no-value case
                    throw error
                case .success(let result):
                    return result
                }
            }
    }

    func fetchTipTopUpCampaignAmountFormatted(forPayment payment: Payment) -> Single<String?> {
        campaignService.checkCampaign(for: payment.locationId, type: .tipTopUpAndRaffle, date: payment.dateTimeParsed)
            .observe(on: MainScheduler.instance)
            .catchAndReturn(nil)
            .flatMap { campaign in
                guard let tipAmount = payment.tipAmount, campaign != nil else { return .just(nil) }
                let amount = Money(double: tipAmount, currency: EUR())
                return .just((amount * 0.1).formattedAmountWithCode)
            }
    }

    private func getOrCreateCredentials() -> Single<LucaPaymentUserCredentials> {
        preferences.get(\.credentials)
            .flatMap { currentCredentials in
                if let credentials = currentCredentials {
                    return .just(credentials)
                }
                guard let name = UUID(data: nil, version: .v4, variant: .variant1)?.uuidString.lowercased(),
                      let username = UUID(data: nil, version: .v4, variant: .variant1)?.uuidString.lowercased(),
                      let password = UUID(data: nil, version: .v4, variant: .variant1)?.uuidString.lowercased() else {
                    throw RxError.unknown
                }
                let newCredentials = LucaPaymentUserCredentials(
                    name: name,
                    username: username,
                    password: password
                )
                return self.preferences.set(\.credentials, value: newCredentials)
                    .andThen(.just(newCredentials))
            }
    }

    func retrieveAccessToken(refreshIfNeeded: Bool = true) -> Single<String> {
        preferences.get(\.lastToken)
            .flatMap { token in
                if let token = token,
                   let jwt = try? JWT<PaymentUserClaims>(jwtString: token),
                   jwt.validateClaims(now: self.timeProvider.now) == .success {
                    return .just(token)
                }
                if refreshIfNeeded {
                    return self.signIn().andThen(self.retrieveAccessToken(refreshIfNeeded: false))
                }
                throw LucaPaymentServiceError.paymentNotActivated
            }
    }

    private func fetchPaymentHistorySource(cursor: String? = nil) -> Single<PaymentHistoryPage> {
        retrieveAccessToken()
            .flatMap {
                self.backend.fetchPaymentHistory(accessToken: $0, cursor: cursor).asSingle()
            }
    }

    private func emitPaymentRevocationMessageIfNeeded() -> Completable {
        Single.zip(
            preferences.get(\.registeredUser).map { $0 != nil },
            localMesssagesRepo.restore().map { $0.contains(where: { $0.type == .paymentRevocation }) }
        )
        .flatMapCompletable { registeredUserExists, localMessageAlreadyGenerated in
            guard registeredUserExists, !localMessageAlreadyGenerated else {
                return .empty()
            }
            let message = LocalNewsMessage(type: .paymentRevocation, createDate: self.timeProvider.now)
            return self.localMesssagesRepo.store(object: message).asCompletable()
        }
    }

    func fetchLimits() -> Single<PaymentLimits> {
        return retrieveAccessToken()
            .flatMap {
                self.backend.fetchLimits(accessToken: $0).asSingle()
            }
    }
}
