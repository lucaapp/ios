import Foundation
import RxSwift
import DependencyInjection

class TimeSyncService {
    @InjectStatic(\.timeProvider) private var timeProvider: TimeProvider
    @InjectStatic(\.backendMiscV3) private var backend: CommonBackendMisc
    @InjectStatic(\.keyValueRepo) private var keyValueRepo: KeyValueRepoProtocol

    private let timestampArchiveKey = "timestampsArchive"
    private let clockMarginInMinutes = 5

    private lazy var cached: CachedDataSource<Timesync> = {
        BaseCachedDataSource(
            dataSource: backend.fetchTimesync(),
            cacheDataRepo: KeyValueRepoCacheWrapper(uniqueCacheKey: "Timesync"),
            cacheValidity: .until(unit: .minute, count: clockMarginInMinutes - 1),
            uniqueCacheIdentifier: "TimesyncCache"
        )
    }()

    /// Emits true if the clock is in sync.
    /// If there is no internet connection, it will check the timestamp archive to check if there are some elements in false order. If so, this would mean the clock has been altered
    func isInSync() -> Single<Bool> {
        isLocalClockInSyncWithBackend()
            .flatMap { isInSyncOnline -> Single<Bool> in

                // If clock is in sync, delete the archive and save the current timestamp as future reference point.
                if isInSyncOnline {
                    return self.resetTimestampArchiveAndSaveCurrentTimestamp().andThen(.just(true))
                }

                // If clock is not in sync, save the current timestamp to invalidate the whole archive.
                return self.saveCurrentLocalTimestamp().andThen(.just(false))
            }
            .catch { _ in
                self.saveCurrentLocalTimestamp()
                    .andThen(self.isKeystampArchiveContinuous())
            }
    }

    /// Checks if local clock is in sync with the backend.
    private func isLocalClockInSyncWithBackend() -> Single<Bool> {
        cached.retrieve()
            .map { $0.first ?? Timesync(unix: 0) }
            .map { ($0.unix - self.clockMarginInMinutes * 60) ... ($0.unix + self.clockMarginInMinutes * 60) ~= Int(self.timeProvider.now.timeIntervalSince1970) }
    }

    /// Checks if every element of the keystamp archive is newer than the previous one plus the current local time.
    /// If current time or any other element is in false order, it means that the clock has been altered
    private func isKeystampArchiveContinuous() -> Single<Bool> {
        keyValueRepo.loadOptional(timestampArchiveKey)
            .map { $0 ?? [] }
            .map { $0 + [self.timeProvider.now] }
            .map { timestamps -> Bool in
                var lastTimestamp = Date(timeIntervalSince1970: -10000)
                for t in timestamps {
                    if t < lastTimestamp {
                        return false
                    }
                    lastTimestamp = t
                }
                return true
            }
    }

    /// Resets old timestamps and saves current timestamp. Should be used only if current timestamp has been somehow confirmed
    private func resetTimestampArchiveAndSaveCurrentTimestamp() -> Completable {
        Completable.deferred { self.keyValueRepo.store(self.timestampArchiveKey, value: [self.timeProvider.now]) }
    }

    /// It saves current timestamp
    private func saveCurrentLocalTimestamp() -> Completable {
        keyValueRepo.loadOptional(timestampArchiveKey)
            .map { $0 ?? [] }
            .flatMapCompletable { (timestamps: [Date]) in
                self.keyValueRepo.store(self.timestampArchiveKey, value: timestamps + [self.timeProvider.now])
            }
    }
}
