@testable import Luca
import XCTest
import RxSwift
import Combine

class CombineToRxSwiftTests: XCTestCase {

    func testSusbcription() throws {
        var subscribed = false
        let subject = PassthroughSubject<Void, Never>()
        _ = subject
            .eraseToAnyPublisher()
            .asObservable()
            .do(onSubscribe: { subscribed = true })
            .subscribe()
        XCTAssertTrue(value: subscribed)
    }

    func testFailure() throws {
        var error: Error?
        let expectedError = PrintableError(title: "Error", message: "")
        let subject = PassthroughSubject<Void, Error>()
        _ = subject
            .eraseToAnyPublisher()
            .asObservable()
            .do(onError: { error = $0 })
            .subscribe()
        subject.send(completion: .failure(expectedError))
        XCTAssertEqual((error as? PrintableError)?.title, to: expectedError.title)
    }

    func testValues() throws {
        var collectedValues: [Int] = [0]
        let subject = PassthroughSubject<Int, Never>()
        _ = subject
            .eraseToAnyPublisher()
            .asObservable()
            .toArray()
            .do(onSuccess: { collectedValues = $0 })
            .subscribe()

        subject.send(0)
        subject.send(1)
        subject.send(2)
        subject.send(3)
        subject.send(completion: .finished)
        XCTAssertEqual(collectedValues, to: [0, 1, 2, 3])
    }

    func testPublisherDisposal() {
        let subject = PassthroughSubject<Void, Never>()
        var disposeBag: DisposeBag! = DisposeBag()
        var canceled = false
        subject
            .handleEvents(receiveCancel: { canceled = true })
            .eraseToAnyPublisher()
            .asObservable()
            .subscribe()
            .disposed(by: disposeBag)

        disposeBag = nil
        XCTAssertTrue(value: canceled)
    }

}
