import XCTest
import RxSwift
import SwiftDGC
import DependencyInjection
@testable import Luca

// swiftlint:disable:next type_body_length
class HealthStatusTests: XCTestCase {
    var validPCRTest: MockedCoronaTest!
    var validQuickTest: MockedCoronaTest!
    var validVaccination: MockedVaccination!
    var validBooster: MockedBooster!
    var validRecovery: MockedRecovery!
    var validPCRRecovery: MockedCoronaTest!

    var invalidPCRTest: MockedCoronaTest!
    var invalidQuickTest: MockedCoronaTest!
    var invalidPositiveTest: MockedCoronaTest!
    var notYetValidVaccination: MockedVaccination!
    var invalidVaccination: MockedVaccination!
    var invalidBooster: MockedBooster!
    var invalidRecovery: MockedRecovery!

    private var documentRepoService: MockedDocumentRepoService!
    private var verificationService: MockedDGCVerificationService!
    private var documentPersonAssociationService: DocumentPersonAssociationService!

    let person = Person(firstName: "Erika", lastName: "Mustermann", type: .user)
    // swiftlint:disable:next line_length
    let dgcData = "HC1:6BF+70790T9WJWG.FKY*4GO0.O1CV2 O5 N2FBBRW1*70HS8WY04AC*WIFN0AHCD8KD97TK0F90KECTHGWJC0FDC:5AIA%G7X+AQB9746HS80:54IBQF60R6$A80X6S1BTYACG6M+9XG8KIAWNA91AY%67092L4WJCT3EHS8XJC$+DXJCCWENF6OF63W5NW6WF6%JC QE/IAYJC5LEW34U3ET7DXC9 QE-ED8%E.JCBECB1A-:8$96646AL60A60S6Q$D.UDRYA 96NF6L/5QW6307KQEPD09WEQDD+Q6TW6FA7C466KCN9E%961A6DL6FA7D46JPCT3E5JDLA7$Q6E464W5TG6..DX%DZJC6/DTZ9 QE5$CB$DA/D JC1/D3Z8WED1ECW.CCWE.Y92OAGY8MY9L+9MPCG/D5 C5IA5N9$PC5$CUZCY$5Y$527B+A4KZNQG5TKOWWD9FL%I8U$F7O2IBM85CWOC%LEZU4R/BXHDAHN 11$CA5MRI:AONFN7091K9FKIGIY%VWSSSU9%01FO2*FTPQ3C3F"

    override func setUp() {

        DependencyContext[\.timeProvider] = SharedProvider(value: DefaultTimeProvider())

        validPCRTest = MockedCoronaTest()
        validPCRTest.testType = .pcr
        validPCRTest.isNegative = true
        validPCRTest.issuedAt = Calendar.current.date(byAdding: .day, value: -1, to: Date())!
        validQuickTest = MockedCoronaTest()
        validQuickTest.testType = .fast
        var vaccinationTime = Calendar.current.date(byAdding: .day, value: -15, to: Date())
        validVaccination = MockedVaccination(issuedAt: vaccinationTime)
        _ = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        validBooster = MockedBooster(issuedAt: vaccinationTime)
        validRecovery = MockedRecovery()
        validRecovery.validFromDate = Date(timeIntervalSince1970: Date().timeIntervalSince1970 - 100)
        validRecovery.validUntilDate = Date(timeIntervalSince1970: Date().timeIntervalSince1970 + 100)
        validPCRRecovery = MockedCoronaTest()
        validPCRRecovery.testType = .pcr
        validPCRRecovery.issuedAt = Calendar.current.date(byAdding: .month, value: -7, to: Date())!
        validPCRRecovery.isNegative = false

        invalidPCRTest = MockedCoronaTest(issuedAt: Date(timeIntervalSince1970: 1))
        invalidPCRTest.testType = .pcr
        invalidQuickTest = MockedCoronaTest(issuedAt: Date(timeIntervalSince1970: 1))
        invalidQuickTest.testType = .fast
        invalidPositiveTest = MockedCoronaTest(issuedAt: Date(timeIntervalSince1970: 1))
        invalidPositiveTest.isNegative = false
        vaccinationTime = Calendar.current.date(byAdding: .day, value: -12, to: Date())
        notYetValidVaccination = MockedVaccination(issuedAt: vaccinationTime)
        invalidVaccination = MockedVaccination(issuedAt: Calendar.current.date(byAdding: .year, value: -2, to: Date()))
        invalidBooster = MockedBooster(issuedAt: Calendar.current.date(byAdding: .year, value: -2, to: Date()))
        invalidRecovery = MockedRecovery(issuedAt: Calendar.current.date(byAdding: .month, value: -12, to: Date())!)
        invalidRecovery.validFromDate = Calendar.current.date(byAdding: .month, value: -12, to: Date())!
        invalidRecovery.validUntilDate = Calendar.current.date(byAdding: .month, value: -6, to: Date())!

        let mockedRepoService = MockedDocumentRepoService()
        mockedRepoService.documentsToEmit = []
        documentRepoService = mockedRepoService
        verificationService = MockedDGCVerificationService()
        DependencyContext[\.dgcVerificationService] = SharedProvider(value: verificationService as DGCVerificationService)
        DependencyContext[\.documentRepoService] = SharedProvider(value: documentRepoService)
        documentPersonAssociationService = DocumentPersonAssociationService()

    }

    func test_validDocumentTypes() async throws {
        var result = try await validPCRTest.healthStatus.task()
        XCTAssertEqual(result, HealthStatus.testedPCR)
        result = try await validQuickTest.healthStatus.task()
        XCTAssertEqual(result, HealthStatus.testedQuick)
        result = try await validVaccination.healthStatus.task()
        XCTAssertEqual(result, HealthStatus.vaccinated)
        result = try await validBooster.healthStatus.task()
        XCTAssertEqual(result, HealthStatus.boostered)
        result = try await validRecovery.healthStatus.task()
        XCTAssertEqual(result, HealthStatus.recovered)
        result = try await validPCRRecovery.healthStatus.task()
        XCTAssertEqual(result, HealthStatus.recovered)
    }

    func test_invalidDocumentTypes() async throws {
        var result = try await invalidPCRTest.healthStatus.task()
        XCTAssertEqual(result, nil)
        result = try await invalidQuickTest.healthStatus.task()
        XCTAssertEqual(result, nil)
        result = try await invalidPositiveTest.healthStatus.task()
        XCTAssertEqual(result, nil)
        result = try await invalidVaccination.healthStatus.task()
        XCTAssertEqual(result, nil)
        result = try await invalidBooster.healthStatus.task()
        XCTAssertEqual(result, nil)
        result = try await invalidRecovery.healthStatus.task()
        XCTAssertEqual(result, nil)
    }

    // swiftlint:disable:next function_body_length
    func test_healthStatusSummedBitfields() async throws {
        let documentsWithHighestBoostered: [Document] = [
            validPCRTest,
            validQuickTest,
            validVaccination,
            validBooster,
            validRecovery,
            invalidPCRTest,
            invalidQuickTest,
            invalidVaccination,
            invalidBooster,
            invalidRecovery
        ]
        let documentsWithHighestVaccinated: [Document] = [
            validPCRTest,
            validQuickTest,
            validVaccination,
            validRecovery,
            invalidPCRTest,
            invalidQuickTest,
            invalidVaccination,
            invalidBooster,
            invalidRecovery
        ]
        let documentsWithHighestRecovered: [Document] = [
            validPCRTest,
            validQuickTest,
            validRecovery,
            invalidPCRTest,
            invalidQuickTest,
            invalidVaccination,
            invalidBooster,
            invalidRecovery
        ]
        let documentsWithHighestTestedPCR: [Document] = [
            validQuickTest,
            validPCRTest,
            invalidPCRTest,
            invalidQuickTest,
            invalidVaccination,
            invalidRecovery
        ]
        let documentsWithHighestTestedQuick: [Document] = [
            validQuickTest,
            invalidPCRTest,
            invalidQuickTest,
            invalidVaccination,
            invalidRecovery
        ]
        let documentsWithNoValue: [Document] = [
            invalidPCRTest,
            invalidQuickTest,
            invalidVaccination,
            invalidRecovery
        ]
        let documentsWithNotYetValidVaccinationSetAsValidBecauseOfRecoveryValue: [Document] = [
            invalidPCRTest,
            invalidQuickTest,
            invalidVaccination,
            notYetValidVaccination,
            validRecovery,
            invalidRecovery
        ]
        let documentsWithNotYetValidVaccinationSetAsInvalidBecauseOfRecoveryValue: [Document] = [
            invalidPCRTest,
            invalidQuickTest,
            invalidVaccination,
            notYetValidVaccination,
            invalidRecovery
        ]

        documentRepoService.documentsToEmit = documentsWithHighestBoostered
        var result = try await documentPersonAssociationService.healthStatus(for: person).task()
        XCTAssertEqual(
            result,
            HealthStatus.boostered.rawValue +
            HealthStatus.vaccinated.rawValue +
            HealthStatus.recovered.rawValue +
            HealthStatus.testedPCR.rawValue +
            HealthStatus.testedQuick.rawValue
        )
        validBooster.skipValidityInterval = false
        invalidBooster.skipValidityInterval = false
        invalidVaccination.skipValidityInterval = false
        validVaccination.skipValidityInterval = false
        notYetValidVaccination.skipValidityInterval = false

        documentRepoService.documentsToEmit = documentsWithHighestVaccinated
        result = try await documentPersonAssociationService.healthStatus(for: person).task()
        XCTAssertEqual(
            result,
            HealthStatus.vaccinated.rawValue + HealthStatus.recovered.rawValue + HealthStatus.testedPCR.rawValue + HealthStatus.testedQuick.rawValue
        )
        invalidBooster.skipValidityInterval = false
        invalidVaccination.skipValidityInterval = false
        validVaccination.skipValidityInterval = false
        notYetValidVaccination.skipValidityInterval = false

        documentRepoService.documentsToEmit = documentsWithHighestRecovered
        result = try await documentPersonAssociationService.healthStatus(for: person).task()
        XCTAssertEqual(
            result,
            HealthStatus.recovered.rawValue + HealthStatus.testedPCR.rawValue + HealthStatus.testedQuick.rawValue
        )
        invalidBooster.skipValidityInterval = false
        invalidVaccination.skipValidityInterval = false
        validVaccination.skipValidityInterval = false
        notYetValidVaccination.skipValidityInterval = false

        documentRepoService.documentsToEmit = documentsWithHighestTestedPCR
        result = try await documentPersonAssociationService.healthStatus(for: person).task()
        XCTAssertEqual(result, HealthStatus.testedPCR.rawValue + HealthStatus.testedQuick.rawValue)
        invalidVaccination.skipValidityInterval = false
        validVaccination.skipValidityInterval = false
        notYetValidVaccination.skipValidityInterval = false

        documentRepoService.documentsToEmit = documentsWithHighestTestedQuick
        result = try await documentPersonAssociationService.healthStatus(for: person).task()
        XCTAssertEqual(result, HealthStatus.testedQuick.rawValue)
        invalidVaccination.skipValidityInterval = false
        validVaccination.skipValidityInterval = false
        notYetValidVaccination.skipValidityInterval = false

        documentRepoService.documentsToEmit = documentsWithNoValue
        result = try await documentPersonAssociationService.healthStatus(for: person).task()
        XCTAssertEqual(result, HealthStatus.notShared.rawValue)
        invalidVaccination.skipValidityInterval = false
        validVaccination.skipValidityInterval = false
        notYetValidVaccination.skipValidityInterval = false

        documentRepoService.documentsToEmit = documentsWithNotYetValidVaccinationSetAsValidBecauseOfRecoveryValue
        result = try await documentPersonAssociationService.healthStatus(for: person).task()
        XCTAssertEqual(result, HealthStatus.recovered.rawValue + HealthStatus.vaccinated.rawValue)
        invalidVaccination.skipValidityInterval = false
        validVaccination.skipValidityInterval = false
        notYetValidVaccination.skipValidityInterval = false

        documentRepoService.documentsToEmit = documentsWithNotYetValidVaccinationSetAsInvalidBecauseOfRecoveryValue
        result = try await documentPersonAssociationService.healthStatus(for: person).task()
        XCTAssertEqual(result, HealthStatus.notShared.rawValue)
    }

    func test_unverifiedVaccinactionIsOmitted() async throws {
        guard var document = try await DGCParser().parse(code: dgcData).task() as? DGCVaccination else {
            XCTFail("Document didn't parse")
            return
        }
        document.issuedAt = Calendar.current.date(byAdding: .day, value: -15, to: Date())!
        verificationService.verificationValue = true
        documentRepoService.documentsToEmit = [document]
        var result = try await documentPersonAssociationService.healthStatus(for: person).task()
        XCTAssertEqual(result, HealthStatus.vaccinated.rawValue)

        verificationService.verificationValue = false
        result = try await documentPersonAssociationService.healthStatus(for: person).task()
        XCTAssertEqual(result, HealthStatus.notShared.rawValue)
        let invalidDocuments = try await documentPersonAssociationService.invalidDocuments(for: person).task()
        XCTAssertEqual(invalidDocuments.count, 1)
    }

    func test_wrongPerson_documentWillBeOmitted() async throws {
        guard var document = try await DGCParser().parse(code: dgcData).task() as? DGCVaccination else {
            XCTFail("Document didn't parse")
            return
        }
        document.issuedAt = Calendar.current.date(byAdding: .day, value: -15, to: Date())!
        verificationService.verificationValue = true
        documentRepoService.documentsToEmit = [document]
        let healthStatus = try await documentPersonAssociationService.healthStatus(for: Person(firstName: "", lastName: "", type: .unknown)).task()
        XCTAssertEqual(healthStatus, HealthStatus.notShared.rawValue)
    }
}

class MockedDGCVerificationService: DGCVerificationService {
    var verificationValue: Bool = true
    override func verifyCode(_ cert: HCert) -> Single<Bool> {
        .just(verificationValue)
    }
}

class MockedDocumentRepoService: DocumentRepoService {
    var documentsToEmit: [Document] = []
    override var currentAndNewTests: Observable<[Document]> {
        .just(documentsToEmit)
    }
    override func load() -> Single<[Document]> {
        .just(documentsToEmit)
    }
}

class MockedCoronaTest: CoronaTest {
    init(issuedAt: Date? = nil) { self.issuedAt = issuedAt ?? Date(timeIntervalSince1970: Date().timeIntervalSince1970 - 100) }
    var originalCode: String = ""
    var hashSeed: String = ""
    var issuedAt: Date
    func belongs(to: HasFirstAndLastName) -> Bool { true }
    var contentDescription: String = ""

    var testType: CoronaTestType = .other
    var laboratory: String = ""
    var doctor: String = ""
    var isNegative: Bool = true
    var provider: String = ""
}

class MockedRecovery: Recovery {
    init(issuedAt: Date? = nil) { self.issuedAt = issuedAt ?? Date(timeIntervalSince1970: Date().timeIntervalSince1970 - 100) }
    var originalCode: String = ""
    var hashSeed: String = ""
    var issuedAt: Date
    func belongs(to: HasFirstAndLastName) -> Bool { true }
    var contentDescription: String = ""

    var validFromDate: Date = Date()
    var validUntilDate: Date = Date()
    var laboratory: String = ""
    var dateOfBirth: Date = Date()
}

class MockedVaccination: Vaccination {
    var skipValidityInterval: Bool = false

    var totalDosesNumber: Int = 2

    var vaccineType: VaccinationType = .unknown

    init(issuedAt: Date? = nil) { self.issuedAt = issuedAt ?? Date(timeIntervalSince1970: Date().timeIntervalSince1970 - 100) }
    var originalCode: String = ""
    var hashSeed: String = ""
    var issuedAt: Date
    func belongs(to: HasFirstAndLastName) -> Bool { true }
    var contentDescription: String = ""
    var isCompleteDose: Bool = true
    var isAdditionalDose: Bool = false

    var dateOfBirth: Date = Date()
    var doseNumber: Int = 2
    var laboratory: String = ""
}

class MockedBooster: Vaccination {
    var skipValidityInterval: Bool = false

    var totalDosesNumber: Int = 3

    var vaccineType: VaccinationType = .unknown

    init(issuedAt: Date? = nil) { self.issuedAt = issuedAt ?? Date(timeIntervalSince1970: Date().timeIntervalSince1970 - 100) }
    var originalCode: String = ""
    var hashSeed: String = ""
    var issuedAt: Date
    func belongs(to: HasFirstAndLastName) -> Bool { true }
    var contentDescription: String = ""
    var isCompleteDose: Bool = true
    var isAdditionalDose: Bool = true

    var dateOfBirth: Date = Date()
    var doseNumber: Int = 3
    var laboratory: String = ""
}
