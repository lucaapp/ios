import Foundation
import UIKit
@testable import Luca

class NavigationControllerMock: UINavigationController {

    var pushedViewController: UIViewController?
    var popViewControllerCallCount: Int = 0

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushedViewController = viewController
        super.pushViewController(viewController, animated: animated)
    }

    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }

    override func popViewController(animated: Bool) -> UIViewController? {
        popViewControllerCallCount += 1
        return nil
    }
}
