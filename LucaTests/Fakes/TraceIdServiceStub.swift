@testable import Luca
import Foundation
import RxSwift

class TraceIdServiceStub: TraceIdService {

    init() {
        super.init(preferences: UserDataPreferences(suiteName: ""))
    }

    override public func fetchTraceStatus() -> Completable {
        return Completable.empty()
    }

    override var isCurrentlyCheckedIn: Single<Bool> {
        return Single.just(true)
    }
}
