import Foundation
import UIKit
@testable import Luca

class ApplicationMock: Application {

    var openURLCallCount: Int = 0
    var openURLParameter: URL?

    func canOpenURL(_ url: URL) -> Bool {
        return true
    }

    func open(_ url: URL, options: [UIApplication.OpenExternalURLOptionsKey: Any], completionHandler completion: ((Bool) -> Void)?) {
        openURLParameter = url
        openURLCallCount += 1
    }
}
