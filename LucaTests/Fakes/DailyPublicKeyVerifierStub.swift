@testable import Luca
import Foundation
import RxSwift
import RxCocoa

class DailyPublicKeyVerifierStub: DailyPublicKeyVerifier {

    var isKeyValidMockedValue: Bool = true

    override var isKeyValid: Driver<Bool> {
        return Driver.just(isKeyValidMockedValue)
    }
}
