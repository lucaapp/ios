import Foundation
import UIKit

extension UIWindow {

    static func create(for sut: UIViewController) {
        let window = UIWindow()
        window.rootViewController = sut
        window.isHidden = false
    }

    static func destroy() {
        RunLoop.current.run(until: Date())
    }
}

extension UIButton {

    func tap() {
        sendActions(for: .touchUpInside)
    }
}

extension UIBarButtonItem {

    func tap() {
        _ = target?.perform(action, with: nil)
    }
}
