import Foundation
@testable import Luca
import RxSwift
import RxCocoa

class DocumentListViewModelMock: DocumentListViewModel {

    var revalidationCallCount: Int = 0
    var removeCallCount: Int = 0
    var authenticateDeviceUserCallCount: Int = 0
    var authenticateDeviceUserCompletionResult: Bool = true

    var stubbedTimesync: Bool = true
    var stubbedPeople: [Person: [Document]] = [:]
    var removeShouldFail: Bool = false

    override var persons: Driver<[Person: [Document]]> {
        get { return Observable.just(stubbedPeople).asDriver(onErrorJustReturn: [:]) }
        set { return }
    }

    override var isTimeInSync: Driver<Bool> {
        return Driver.just(stubbedTimesync)
    }

    override var revalidation: Observable<Void> {
        revalidationCallCount += 1
        return Observable.from(optional: nil)
    }

    override func remove(document: Document) -> Completable {
        removeCallCount += 1
        if removeShouldFail {
            return .error(RxError.unknown)
        }
        return .empty()
    }

    override func authenticateDeviceUser(completion: @escaping (Bool) -> Void) {
        authenticateDeviceUserCallCount += 1
        completion(authenticateDeviceUserCompletionResult)
    }
}
