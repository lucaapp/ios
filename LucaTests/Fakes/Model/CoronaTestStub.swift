import Foundation
@testable import Luca

struct CoronaTestStub: CoronaTest {

    var originalCode: String = "123"

    var testType: CoronaTestType = CoronaTestType.fast

    var laboratory: String = ""

    var doctor: String = ""

    var isNegative: Bool = true

    var provider: String = ""

    var hashSeed: String = ""

    var issuedAt: Date = Date()

    func belongs(to identity: HasFirstAndLastName) -> Bool {
        return true
    }

    var contentDescription: String = ""
}
