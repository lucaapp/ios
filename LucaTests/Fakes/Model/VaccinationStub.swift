import Foundation
@testable import Luca

class VaccinationStub: Vaccination {

    var dateOfBirth: Date = Date.distantPast

    var vaccineType: VaccinationType = .moderna

    var doseNumber: Int

    var totalDosesNumber: Int = 2

    var laboratory: String = ""

    var skipValidityInterval: Bool = false

    var contentDescription: String = ""

    var originalCode: String = UUID().uuidString

    var hashSeed: String = ""

    var issuedAt: Date

    func belongs(to identity: HasFirstAndLastName) -> Bool {
        true
    }

    init(issuedAt: Date, doseNumber: Int) {
        self.issuedAt = issuedAt
        self.doseNumber = doseNumber
    }
}
