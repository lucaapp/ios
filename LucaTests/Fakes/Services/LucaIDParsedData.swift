@testable import Luca
import Foundation
import SwiftJWT

extension LucaIDParsedData {

    static func testObject(with state: IdentProcessState) -> LucaIDParsedData {
        let lucaIDProcess = LucaIDProcess(state: state, revocationCode: "")
        let claimsPayload = LucaIDReceiptInputDataClaimsPayload(bindingKey: "", encryptionKey: "")
        let claims = LucaIDReceiptClaims(input_data: claimsPayload, autoIdentId: "test_id", transactionNumber: "123")
        let receiptJWS = JWT<LucaIDReceiptClaims>.init(claims: claims)
        return LucaIDParsedData(state: lucaIDProcess, minimalIdentityJWT: nil, identityJWT: nil, faceJWT: nil, receiptJWS: receiptJWS)
    }
}
