import Foundation
@testable import Luca
import RxSwift

class LucaIDServiceMock: LucaIDService {

    var archiveDataCallCount: Int = 0

    var fakedProcessState: IdentProcessState = .success
    var fakedIdProcessStarted: Bool = true
    var fetchStateShouldFail: Bool = false
    var archiveDataShouldFail: Bool = false

    private var parsedData: LucaIDParsedData {
        return LucaIDParsedData.testObject(with: fakedProcessState)
    }

    override var lucaIDProcessChanges: Observable<LucaIDParsedData?> {
        return Observable.just(parsedData)
    }

    override var idProcessStarted: Single<Bool> {
        return Single.just(fakedIdProcessStarted)
    }

    override var idProcessStartedCurrentAndChanges: Observable<Bool> {
        return Observable.just(fakedIdProcessStarted)
    }

    override func fetchState() -> Single<LucaIDParsedData?> {
        if fetchStateShouldFail {
            return Single.error(RxError.unknown)
        }
        return Single.just(parsedData)
    }

    override func archiveData() -> Completable {
        if archiveDataShouldFail {
            return Completable.error(RxError.unknown)
        }
        archiveDataCallCount += 1
        return Completable.empty()
    }
}
