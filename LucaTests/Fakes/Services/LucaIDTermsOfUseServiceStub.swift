import Foundation
@testable import Luca
import RxSwift

class LucaIDTermsOfUseServiceStub: LucaIDTermsOfUseService {

    override var isAccepted: Single<Bool?> {
        return Single.just(true)
    }
}
