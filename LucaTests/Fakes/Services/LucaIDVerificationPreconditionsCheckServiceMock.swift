import Foundation
@testable import Luca
import RxSwift

// swiftlint:disable type_name
class LucaIDVerificationPreconditionsCheckServiceMock: LucaIDVerificationPreconditionsCheckService {

    override func checkPreconditionsIfNeeded() -> Completable {
        return .empty()
    }
}
