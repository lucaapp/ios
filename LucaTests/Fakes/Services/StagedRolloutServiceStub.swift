import Foundation
@testable import Luca
import RxSwift

class StagedRolloutServiceStub: StagedRolloutService {

    var fakedIsFeatureEnabled: Bool = true

    override func isFeatureEnabled(type: StagedFeatureType) -> Observable<Bool> {
        return Observable.just(fakedIsFeatureEnabled)
    }
}
