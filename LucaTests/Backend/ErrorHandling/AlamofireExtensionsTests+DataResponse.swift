@testable import Luca
import XCTest
import Alamofire

final class AlamofireExtensionsTestsDataResponse: XCTestCase {

    func test_lucaNetworkError_withHTTP502_shouldReturnBadGatewayNetworkError() throws {
        let error: AFError = .responseValidationFailed(reason: .unacceptableStatusCode(code: 502))
        let sut = DataResponse<Any, AFError>(request: nil, response: nil, data: nil, metrics: nil, serializationDuration: 0, result: .failure(error))

        XCTAssertTrue(sut.lucaNetworkError == .badGateway)
    }

    func test_lucaNetworkError_withHTTP426_shouldReturnUpgradeRequiredNetworkError() throws {
        let error: AFError = .responseValidationFailed(reason: .unacceptableStatusCode(code: 426))
        let sut = DataResponse<Any, AFError>(request: nil, response: nil, data: nil, metrics: nil, serializationDuration: 0, result: .failure(error))

        XCTAssertTrue(sut.lucaNetworkError == .upgradeRequired)
    }
}
