@testable import Luca
import XCTest

final class NetworkErrorTests: XCTestCase {

    func test_noInternet_localizedTitle_shouldReturnCorrectLocalization() throws {
        let sut = NetworkError.noInternet

        XCTAssertEqual(sut.localizedTitle, L10n.IOSApp.General.Failure.NoInternet.title)
    }

    func test_noInternet_errorDescription_shouldReturnCorrectLocalization() throws {
        let sut = NetworkError.noInternet

        XCTAssertEqual(sut.errorDescription, L10n.IOSApp.General.Failure.NoInternet.message)
    }

    func test_certificateValidationFailed_localizedTitle_shouldReturnCorrectLocalization() throws {
        let sut = NetworkError.certificateValidationFailed

        XCTAssertEqual(sut.localizedTitle, L10n.IOSApp.General.Failure.InvalidCertificate.title)
    }

    func test_certificateValidationFailed_errorDescription_shouldReturnCorrectLocalization() throws {
        let sut = NetworkError.certificateValidationFailed

        XCTAssertEqual(sut.errorDescription, L10n.IOSApp.General.Failure.InvalidCertificate.message)
    }

    func test_certificateValidationFailed_badGateway_shouldReturnBasicErrorTitle() throws {
        let sut = NetworkError.badGateway

        XCTAssertEqual(sut.localizedTitle, L10n.IOSApp.Navigation.Basic.error)
    }

    func test_certificateValidationFailed_badGateway_shouldReturnCorrectLocalization() throws {
        let sut = NetworkError.badGateway

        XCTAssertEqual(sut.errorDescription, L10n.IOSApp.Error.Network.badGateway)
    }

    func test_certificateValidationFailed_upgradeRequired_shouldReturnBasicErrorTitle() throws {
        let sut = NetworkError.upgradeRequired

        XCTAssertEqual(sut.localizedTitle, L10n.IOSApp.Navigation.Basic.error)
    }

    func test_certificateValidationFailed_upgradeRequired_shouldReturnCorrectLocalization() throws {
        let sut = NetworkError.upgradeRequired

        XCTAssertEqual(sut.errorDescription, L10n.IOSApp.VersionSupportChecker.failureMessage)
    }
}
