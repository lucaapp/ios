import Quick
import Nimble
import Cuckoo
import XCTest
import DependencyInjection
import RxSwift
import RxNimble
@testable import Luca

class UserServiceTests: QuickSpec {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    // swiftlint:disable:next function_body_length
    override func spec() {
        var timeProvider: MockDefaultTimeProvider!
        var backend: MockBackendUserV3!
        var keyValueRepo: KeyValueRepoProtocol!
        var userService: UserService!
        let userRegistrationData = UserRegistrationData.completeUserRegistrationData
        let keysBundle = MockUserKeysBundle()
        let registeredUUID = UUID()

        describe("UserService") {
            beforeEach {
                timeProvider = MockDefaultTimeProvider()
                stub(timeProvider) { m in
                    when(m.now.get).thenCallRealImplementation()
                }
                stub(keysBundle) { m in
                    when(m.generateKeys(forceRefresh: any())).then { _ in }
                    when(m.removeKeys(onlyUserKeyPair: any())).then { _ in }
                }
                backend = MockBackendUserV3()
                keyValueRepo = KeyValueRepoWithUnderlying(underlying: MemoryDataRepo<KeyValueRepoEntry>())
                DependencyContext[\.timeProvider] = SharedProvider(value: timeProvider)
                DependencyContext[\.importantDataKeyValueRepo] = SharedProvider(value: keyValueRepo)
                DependencyContext[\.lucaPreferences] = SharedProvider(value: LucaPreferences())
                DependencyContext[\.backendUserV3] = SharedProvider(value: backend)
                DependencyContext[\.userKeysBundle] = SharedProvider(value: keysBundle)
                try? self.lucaPreferences.set(\.userRegistrationData, value: userRegistrationData).blockingWait()
                try? self.lucaPreferences.set(\.uuid, value: registeredUUID).blockingWait()
                userService = UserService()
            }
            context("Recreate user in case of 403") {
                let uuid = UUID()
                let registerRequest = MockAsyncDataOperation<BackendError<CreateUserError>, UUID>()
                let deleteRequest = MockAsyncOperation<BackendError<DeleteUserError>>()
                beforeEach {
                    let updateUserRequest403 = MockAsyncOperation<BackendError<UpdateUserError>>()
                    stub(backend) { m in
                        when(m.update(userId: any(), userData: any())).thenReturn(updateUserRequest403)
                    }
                    stub(updateUserRequest403) { m in
                        when(m.execute(completion: any(), failure: any())).then {
                            $0.1(BackendError(backendError: UpdateUserError.invalidSignature))
                            return {}
                        }
                    }
                    stub(registerRequest) { m in
                        when(m.execute(completion: any(), failure: any())).then {
                            $0.0(uuid)
                            return {}
                        }
                    }
                    stub(deleteRequest) { m in
                        when(m.execute(completion: any(), failure: any())).then {
                            $0.0()
                            return {}
                        }
                    }
                    stub(backend) { m in
                        when(m.create(userData: any())).thenReturn(registerRequest)
                        when(m.delete(userId: any())).thenReturn(deleteRequest)
                    }
                    stub(keysBundle) { m in
                        when(m.traceSecrets.get).thenCallRealImplementation()
                    }
                }
                it("Check if oldUUIDs will be resetted after successful user registration") {
                    let expectation = UserIDWithTimestamp(id: uuid, timestamp: timeProvider.now)
                    try? self.lucaPreferences.set(\.oldUUIDs, value: [expectation]).blockingWait()

                    expect(userService.deleteUserData()).array.to(beEmpty())
                    expect(self.lucaPreferences.get(\.oldUUIDs)).array.to(equal([[]]))

                    expect(self.lucaPreferences.remove(\.uuid)).array.to(beEmpty())

                    expect(userService.registerIfNeeded()).array.to(equal([UserService.Result.userRecreated]))
                    expect(self.lucaPreferences.get(\.oldUUIDs).map { $0.map { $0.id } }).array.to(equal([[expectation.id]]))
                    expect(self.lucaPreferences.get(\.uuid)).array.to(equal([expectation.id]))
                }
                it("Check if old UUIDs are resetted after user deletion") {
                    let deleteRequest = MockAsyncOperation<BackendError<DeleteUserError>>()
                    stub(backend) { m in
                        when(m.delete(userId: any())).thenReturn(deleteRequest)
                    }
                    stub(deleteRequest) { m in
                        when(m.execute(completion: any(), failure: any())).then {
                            $0.0()
                            return {}
                        }
                    }
                    expect(self.lucaPreferences.set(\.oldUUIDs, value: [UserIDWithTimestamp(id: registeredUUID, timestamp: timeProvider.now)])).array.to(beEmpty())
                    expect(userService.deleteUserData()).array.to(beEmpty())
                    expect(self.lucaPreferences.get(\.oldUUIDs)).array.to(equal([[]]))
                }
                it("Check if user will be recreated after 403 error") {
                    expect(self.lucaPreferences.get(\.uuid)).array.to(equal([registeredUUID]))
                    expect(userService.update(data: userRegistrationData)).array.to(beEmpty())
                    expect(self.lucaPreferences.get(\.uuid)).array.to(equal([uuid]))
                }
                it("Check if current and previous UUIDs are present in old UUIDs after recreation") {
                    let date = Date()
                    stub(timeProvider) { m in
                        when(m.now).get.thenReturn(date)
                    }
                    expect(self.lucaPreferences.get(\.oldUUIDs)).array.to(equal([[]]))
                    expect(userService.update(data: userRegistrationData)).array.to(beEmpty())
                    expect(self.lucaPreferences.get(\.oldUUIDs)).array.to(equal([[
                        UserIDWithTimestamp(id: registeredUUID, timestamp: date),
                        UserIDWithTimestamp(id: uuid, timestamp: date)
                    ]]))
                }
                it("Check if there will be so many UUIDs how many recreations were attempted + the first one") {
                    var counter = 0
                    var nextUUIDs = [UUID(), UUID(), UUID()]
                    let date = Date()
                    stub(timeProvider) { m in
                        when(m.now).get.thenReturn(date)
                    }
                    stub(registerRequest) { m in
                        when(m.execute(completion: any(), failure: any())).then {
                            $0.0(nextUUIDs[counter])
                            counter += 1
                            return {}
                        }
                    }

                    expect(self.lucaPreferences.get(\.oldUUIDs)).array.to(equal([[]]))

                    for nextUUID in nextUUIDs {
                        expect(userService.update(data: userRegistrationData)).array.to(beEmpty())
                        expect(self.lucaPreferences.get(\.uuid)).array.to(equal([nextUUID]))
                    }
                    nextUUIDs.insert(registeredUUID, at: 0)
                    let nextUUIDsConverted = nextUUIDs.map { UserIDWithTimestamp(id: $0, timestamp: timeProvider.now) }
                    expect(self.lucaPreferences.get(\.oldUUIDs)).array.to(equal([nextUUIDsConverted]))
                }
                it("Insert currently existing UUID to oldUUIDs") {
                    expect(userService.deleteUserData()).array.to(beEmpty())
                    expect(userService.registerIfNeeded()).array.to(equal([.userRecreated]))
                    expect(self.lucaPreferences.remove(\.oldUUIDs)).array.to(beEmpty())
                    expect(self.lucaPreferences.get(\.oldUUIDs)).array.to(equal([[]]))
                    expect(userService.touchIfNeeded()).array.to(beEmpty())
                    expect(self.lucaPreferences.get(\.oldUUIDs).map { $0.map { $0.id } }).array.to(equal([[uuid]]))
                }
            }
            context("touch") {
                let mockedUpdateRequest = MockAsyncOperation<BackendError<UpdateUserError>>()

                stub(mockedUpdateRequest) { m in
                    when(m.execute(completion: any(), failure: any())).then {
                        $0.0()
                        return {}
                    }
                }

                it("Simple run") {
                    stub(backend) { m in
                        when(m.update(userId: any(), userData: any()))
                            .thenReturn(mockedUpdateRequest)
                    }
                    expect(userService.touchIfNeeded()).array.to(beEmpty())

                    verify(backend, times(1)).update(userId: any(), userData: any())
                }

                it("Won't run twice") {
                    stub(backend) { m in
                        when(m.update(userId: any(), userData: any()))
                            .thenReturn(mockedUpdateRequest)
                    }
                    expect(userService.touchIfNeeded()).array.to(beEmpty())
                    expect(userService.touchIfNeeded()).array.to(beEmpty())

                    verify(backend, times(1)).update(userId: any(), userData: any())
                }

                it("Will fail if no registrationData") {
                    try? self.lucaPreferences.remove(\.userRegistrationData).blockingWait()
                    expect(userService.touchIfNeeded()).array().to(throwError(UserServiceError.localDataIncomplete))
                }

                it("Will run twice after some time") {
                    stub(backend) { m in
                        when(m.update(userId: any(), userData: any()))
                            .thenReturn(mockedUpdateRequest)
                    }
                    let now = Date()
                    stub(timeProvider) { m in
                        when(m.now.get).thenReturn(now)
                    }

                    expect(userService.touchIfNeeded()).array.to(beEmpty())

                    stub(timeProvider) { m in
                        when(m.now.get).thenReturn(Calendar.current.date(byAdding: .month, value: 7, to: now)!)
                    }
                    expect(userService.touchIfNeeded()).array.to(beEmpty())

                    verify(backend, times(2)).update(userId: any(), userData: any())
                }
            }
        }
    }
}
