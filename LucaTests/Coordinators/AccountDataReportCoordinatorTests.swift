@testable import Luca
import XCTest
import DependencyInjection

final class AccountDataReportCoordinatorTests: XCTestCase {

    func test_start_shouldPresentAlertController_withContactReportAction() throws {
        let presenter = MockedPresenter()
        let sut = AccountDataReportInteractor(presenter: presenter)
        DependencyContext[\.stagedRolloutService] = SharedProvider(value: StagedRolloutServiceStub())

        _ = sut.interact()
            .subscribe()

        let alertController = try XCTUnwrap(presenter.viewControllerToPresent as? UIAlertController)
        XCTAssertEqual(alertController.actions.first?.title, L10n.IOSApp.DataReport.AccountSettings.contactReport)
    }

    func test_start_shouldPresentAlertController_withDocumentsReportAction() throws {
        let presenter = MockedPresenter()
        let sut = AccountDataReportInteractor(presenter: presenter)

        _ = sut.interact()
            .subscribe()

        let alertController = try XCTUnwrap(presenter.viewControllerToPresent as? UIAlertController)
        XCTAssertEqual(alertController.actions[1].title, L10n.IOSApp.DataReport.AccountSettings.certificatesReport)
    }

    func test_start_shouldPresentAlertController_withLucaIDReportAction() throws {
        let presenter = MockedPresenter()
        let sut = AccountDataReportInteractor(presenter: presenter)

        _ = sut.interact()
            .subscribe()

        let alertController = try XCTUnwrap(presenter.viewControllerToPresent as? UIAlertController)
        XCTAssertEqual(alertController.actions[2].title, L10n.IOSApp.DataReport.AccountSettings.lucaIDReport)
    }
}
