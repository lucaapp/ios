@testable import Luca
import XCTest

class SplitStringByMarkdownLinksTests: XCTestCase {

    func test_noSplits() {
        let testString = "Some string without links and with [some disturbances]."
        XCTAssertEqual(testString.splitByMarkdownLinks, to: ["Some string without links and with [some disturbances]."])
    }

    func test_placeholderAndUrl() {
        let testString = "This is some test [placeholder one] with some link [link placeholder](asds) hello."
        XCTAssertEqual(
            testString.splitByMarkdownLinks,
            to: [
                "This is some test [placeholder one] with some link ",
                "[link placeholder](asds)",
                " hello."
            ]
        )
    }

    func test_splits() {
        let testString = "[Some](some url) string with links and with [some dis[turbance(s](d)isturbed-link). Some [link at the end](url)"
        XCTAssertEqual(
            testString.splitByMarkdownLinks,
            to: [
                "[Some](some url)",
                " string with links and with [some dis[turbance(s](d)isturbed-link). Some ",
                "[link at the end](url)"
            ]
        )
    }

    func test_linkInTheMiddle() {
        let testString = "Some String with a [link](some-url) in the middle."
        XCTAssertEqual(
            testString.splitByMarkdownLinks,
            to: [
                "Some String with a ",
                "[link](some-url)",
                " in the middle."
            ]
        )
    }

}
