import XCTest
import RxSwift
@testable import Luca

class IdentityTests: XCTestCase {
    let persons = [
        Person(firstName: "Paweł Mateusz", lastName: "Sulik", type: .unknown),
        Person(firstName: "Jonas", lastName: "Zimmer", type: .unknown),
        Person(firstName: "Jan Thomas", lastName: "Müller", type: .unknown),
        Person(firstName: "Dryxeryset", lastName: "Drosten", type: .unknown),
        Person(firstName: "Alexandr", lastName: "Robin", type: .unknown),
        Person(firstName: "Khidr", lastName: "Joseph", type: .unknown)
    ]
    let simplifiedIdentities: [(firstName: String, lastName: String)] = [
        (firstName: "PAWE", lastName: "SULIK"),
        (firstName: "JONAS", lastName: "ZIMMER"),
        (firstName: "JAN", lastName: "MLLER"),
        (firstName: "DRYXERYSET", lastName: "DROSTEN"),
        (firstName: "ALEXANDR", lastName: "ROBIN"),
        (firstName: "KHIDR", lastName: "JOSEPH")
    ]
    let academicTitles = ["Dr", "Prof"]

    var academicTitlesWithDots: [String] {
        academicTitles.map { $0 + "." }
    }

    var allAcademicTitlesCombinationsBase: [String] {
        academicTitles +
        academicTitlesWithDots +
        [academicTitles.reduce("") { $0 + " " + $1 }] +
        [academicTitlesWithDots.reduce("") { $0 + " " + $1 }] +
        [academicTitles.reversed().reduce("") { $0 + " " + $1 }] +
        [academicTitlesWithDots.reversed().reduce("") { $0 + " " + $1 }]
    }

    var allAcademicTitlesCombinations: [String] {
        allAcademicTitlesCombinationsBase +
        allAcademicTitlesCombinationsBase.map { $0.uppercased() } +
        allAcademicTitlesCombinationsBase.map { $0.lowercased() }
    }

    func test_simplifiedNames() {
        for i in 0..<persons.count {
            XCTAssertEqual(persons[i].simplifiedFirstName, simplifiedIdentities[i].firstName)
            XCTAssertEqual(persons[i].simplifiedLastName, simplifiedIdentities[i].lastName)
            XCTAssertEqual(persons[i].simplifiedFullName, "\(simplifiedIdentities[i].firstName) \(simplifiedIdentities[i].lastName)")
            let someDocument = IdentifiableDocument(
                firstName: simplifiedIdentities[i].firstName,
                lastName: simplifiedIdentities[i].lastName
            )
            XCTAssertTrue(someDocument.belongs(to: persons[i]))
        }
    }

    func test_academicTitlesCombinations() {

        for personIndex in 0..<persons.count {
            let p = persons[personIndex]
            let firstNames = stringWithAcademicTitles(p.firstName)
            let lastNames = stringWithAcademicTitles(p.lastName)
            let allPersons = zip(firstNames, lastNames).map { Person(firstName: $0, lastName: $1, type: .unknown) }
            for person in allPersons {
                personEquals(person, simplifiedIdentity: simplifiedIdentities[personIndex])
            }
        }

    }

    private func stringWithAcademicTitles(_ string: String) -> [String] {
        allAcademicTitlesCombinations
            .map { title in
                [
                    string,
                    "\(title) \(string)",
                    "\(title), \(string)",
                    "\(string) \(title)",
                    "\(string), \(title)"
                ]
            }
            .flatMap { $0 }
    }

    private func personEquals(_ person: Person, simplifiedIdentity: (firstName: String, lastName: String)) {
        XCTAssertEqual(person.simplifiedFirstName, simplifiedIdentity.firstName)
        XCTAssertEqual(person.simplifiedLastName, simplifiedIdentity.lastName)
        XCTAssertEqual(person.simplifiedFullName, "\(simplifiedIdentity.firstName) \(simplifiedIdentity.lastName)")
        let someDocument = IdentifiableDocument(
            firstName: simplifiedIdentity.firstName,
            lastName: simplifiedIdentity.lastName
        )
        XCTAssertTrue(someDocument.belongs(to: person))
    }
}
