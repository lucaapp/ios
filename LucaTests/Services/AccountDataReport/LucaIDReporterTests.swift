@testable import Luca
import XCTest
import RxTest
import RxSwift
import RxBlocking
import DependencyInjection
import SwiftJWT

final class LucaIDReporterTests: XCTestCase {

    func test_createReport_shouldCreateLucaIDReportCorrectly() async throws {
        let keyValueRepo = KeyValueRepoWithUnderlying(underlying: MemoryDataRepo<KeyValueRepoEntry>())
        DependencyContext[\.importantDataKeyValueRepo] = SharedProvider(value: keyValueRepo)
        DependencyContext[\.lucaIDService] = SharedProvider(value: MockedLucaIDService())
        DependencyContext[\.lucaPreferences] = SharedProvider(value: LucaPreferences())
        let sut = LucaIDReporter()

        let createdReport = try await sut.createReport().task()

        XCTAssertEqual(createdReport, correctLucaIDReport)
    }

    private var correctLucaIDReport: String {
        if Locale.current.languageCode == "de" {
            return correctLucaIDReport_de
        }
        return correctLucaIDReport_en
    }

    // swiftlint:disable line_length
    private var correctLucaIDReport_en: String {
        return """
        Encrypted Data (stored on IDNow-Server max. 7 days, stored in the luca app until manual deletion):
        faceJWE: value_face
        identityJWE: value_identity
        minimalIdentityJwe: value_minimal_identity
        -
        Decrypted Data (stored on IDNow-Server max. 7 days, stored in the luca app until manual deletion):
        First name: first_name
        Last name: last_name
        Date of birth: 21031968
        -
        Signed Data (stored on the luca Server and in the luca App until manual deletion):
        revocationCode: revocation_code
        enrollmentToken: ident_id
        verificationStatus: SUCCESS
        -
        All information about the processing, purposes, legal bases and deletion periods can be found in our privacy policy: https://www.luca-app.de/app-privacy-policy/. Your data will not be transferred to third countries. Automated decision-making and profiling according to Art. 22 DSGVO are not carried out by us or the contractors of the luca system.
        If you would like to receive information about your data, which we have received as clear data through your own contact (e.g. support requests), please contact our Privacy Team at privacy@culture4life.de. Please share any other information that would help us identify you and research your related data with us. If you have been in contact with us via social media, information such as your e-mail address or the pseudonym you use in those social media channels are important in order to provide you with your requested information.
        -
        If you would like to know whether your data is stored by our customer IDnow, please provide us with your IDnow identity or revocation code. With the help of these codes, we can determine for you whether your personal data is held by our order taker Idnow.
        IDnow identity code: ident_id
        Revocation code: revocation_code
        -
        In accordance with Art. 77 DSGVO, you have the right to complain to the supervisory authority if you believe that your personal data is not processed lawfully. The address of the supervisory authority responsible for our company is: Berliner Beauftragte für Datenschutz und Informationsfreiheit, Friedrichstr. 219, 10969 Berlin, Tel.: 030/13889-0, e-mail: mailbox@datenschutz-berlin.de.
        """
    }

    // swiftlint:disable line_length
    private var correctLucaIDReport_de: String {
        return "Encrypted Data (auf dem IDNow-Server max. 7 Tage, in der luca app bis zur manuellen Löschung):\nfaceJWE: value_face\nidentityJWE: value_identity\nminimalIdentityJwe: value_minimal_identity\n-\nDecrypted Data (auf dem IDNow-Server max. 7 Tage, in der luca app bis zur manuellen Löschung):\nFirst name: first_name\nLast name: last_name\nDate of birth: 21031968\n-\nSigned Data (auf dem luca Server, in der luca app bis zur manuellen Löschung):\nrevocationCode: revocation_code\nenrollmentToken: ident_id\nverificationStatus: SUCCESS\n-\nAlle Informationen über die Verarbeitungen, Zwecke, Rechtsgrundlagen und Löschfristen findest du in unserer Datenschutzerklärung: https://www.luca-app.de/app-privacy-policy/. Deine Daten werden nicht in Drittländer übermittelt. Automatisierte Entscheidungsfindungen sowie Profiling gemäß Art. 22 DSGVO werden von uns sowie den Auftragnehmern des luca Systems nicht getätigt.\nMöchtest du eine Auskunft über deine Daten erhalten, die wir durch deine eigene Kontaktaufnahme (z. B. bei Supportanfragen) bei uns als Klardaten von dir erhalten haben, wende dich bitte an unser Privacy Team unter privacy@culture4life.de. Bitte teile uns auch weitere Hinweise mit, die uns ermöglichen, dich zu identifizieren und dazugehörige Daten bei uns zu recherchieren. Wenn du mit uns über Social Media in Kontakt warst, sind weitere Hinweise wie deine E-Mail-Adresse oder dein Pseudonym, das du in den Social-Media-Kanälen verwendest, wichtig um dir die Auskunft erteilen zu können.\n-\nMöchtest du Auskunft erlangen, ob deine Daten bei unserem Auftragsnehmer IDnow gespeichert sind, teile uns bitte deinen IDnow-Identcode mit. Mit Hilfe dieser Codes können wir für dich ermitteln, ob deine personenbezogenen Daten bei unserem Auftragsnehmer Idnow vorliegen.\nIDnow-Identcode: ident_id\nSperrcode: revocation_code\n-\nDu hast gemäß Art. 77 DSGVO das Recht, dich bei der Aufsichtsbehörde zu beschweren, wenn du der Ansicht bist, dass die Verarbeitung deiner personenbezogenen Daten nicht rechtmäßig erfolgt. Die Anschrift der für unser Unternehmen zuständigen Aufsichtsbehörde lautet: Berliner Beauftragte für Datenschutz und Informationsfreiheit, Friedrichstr. 219, 10969 Berlin, Tel.: 030/13889-0, E-Mail: mailbox@datenschutz-berlin.de. "
    }
}

private class MockedLucaIDService: LucaIDService {

    override func fetchState() -> Single<LucaIDParsedData?> {
        return Single.from {
            let processData = LucaIDProcessData(valueFace: "value_face", valueIdentity: "value_identity", valueMinimalIdentity: "value_minimal_identity")
            let state = LucaIDProcess(state: .success, identId: "identID", data: processData, revocationCode: "revocation_code", receiptJWS: "receiptJWS")
            return LucaIDParsedData(state: state,
                                    minimalIdentityJWT: self.mockedMinimalIdentityJWT,
                                    identityJWT: self.mockedIdentityJWT,
                                    faceJWT: self.mockedFaceJWT,
                                    receiptJWS: self.mockedReceiptJWS)
        }
    }

    private var mockedMinimalIdentityJWT: JWT<LucaIDMinimalIdentityClaims> {
        let subject = LucaIDMinimalIdentityClaimsPayload.CredentialsSubject(familyName: "last_name", givenName: "first_name", birthDate: "21031968")
        let minimalIdentityPayload = LucaIDMinimalIdentityClaimsPayload(type: [String](), credentialSubject: subject)
        let minimalIdentityClaims = LucaIDMinimalIdentityClaims(sub: "", nbf: Date(), iss: "", exp: Date(), vc: minimalIdentityPayload, jti: "")
        return JWT<LucaIDMinimalIdentityClaims>(claims: minimalIdentityClaims)
    }

    private var mockedIdentityJWT: JWT<LucaIDIdentityClaims> {
        let subject = LucaIDCredentialSubject(familyName: "last_name", givenName: "first_name", birthDate: "21031968", nationality: LucaIDCredentialSubject.Nationality(identifier: "de"))
        let identityPayload = LucaIDIdentityClaimsPayload(type: [String](), credentialSubject: subject)
        let identityClaims = LucaIDIdentityClaims(sub: "", nbf: Date(), iss: "", exp: Date(), vc: identityPayload, jti: "")
        return JWT<LucaIDIdentityClaims>.init(claims: identityClaims)
    }

    private var mockedFaceJWT: JWT<LucaIDImageClaims> {
        let subject = LucaIDImageClaimsPayload.CredentialsSubject.init(image: "VVNFUl9QSE9UTw==")
        let imagePayload = LucaIDImageClaimsPayload(type: [String](), credentialSubject: subject)
        let imageClaims = LucaIDImageClaims(sub: "", nbf: Date(), iss: "", exp: Date(), jti: "", vc: imagePayload)
        return JWT<LucaIDImageClaims>.init(claims: imageClaims)
    }

    private var mockedReceiptJWS: JWT<LucaIDReceiptClaims> {
        let receiptPayload = LucaIDReceiptInputDataClaimsPayload(bindingKey: "", encryptionKey: "")
        let receiptClaims = LucaIDReceiptClaims(input_data: receiptPayload, autoIdentId: "ident_id", transactionNumber: "")
        return JWT<LucaIDReceiptClaims>(claims: receiptClaims)
    }
}
