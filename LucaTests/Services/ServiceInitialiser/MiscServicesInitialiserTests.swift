@testable import Luca
import XCTest
import RxBlocking
import DependencyInjection

final class MiscServicesInitialiserTests: XCTestCase {

    var sut: MiscServicesInitialiser!

    override func setUp() {
        super.setUp()
        sut = MiscServicesInitialiser()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func test_lucaIdContentReporterService_shouldBeginWithLucaIDHeader() async throws {
        let lucaIdContentReporterService = sut.lucaIDContentReporterService()

        let firstContentReporter = try XCTUnwrap(lucaIdContentReporterService.contentReporters.first as? StaticStringReporter)
        let firstContentReportersContent = try await firstContentReporter.createReport().task()
        XCTAssertEqual(firstContentReportersContent, L10n.IOSApp.DataReport.LucaID.header)
    }

    func test_lucaIdContentReporterService_atSecondIndex_shouldContainLucaIDReporter() throws {
        let lucaIdContentReporterService = sut.lucaIDContentReporterService()

        XCTAssertTrue(lucaIdContentReporterService.contentReporters[1] is LucaIDReporter)
    }

    // MARK: Characterization Test: Contact Data Reporter

    func test_contactDataReporterService_createReport_shouldCreateContactDataReportCorrectly() async throws {
        let keyValueRepo = KeyValueRepoWithUnderlying(underlying: MemoryDataRepo<KeyValueRepoEntry>())
        DependencyContext[\.importantDataKeyValueRepo] = SharedProvider(value: keyValueRepo)
        DependencyContext[\.traceInfoRepo] = SharedProvider(value: TraceInfoRepo(key: Data(count: 64)))
        DependencyContext[\.lucaPreferences] = SharedProvider(value: LucaPreferences())
        DependencyContext[\.userService] = SharedProvider(value: UserService())
        DependencyContext[\.history] = SharedProvider(value: HistoryService())
        DependencyContext[\.historyRepo] = SharedProvider(value: HistoryRepo(key: Data(count: 64)))
        DependencyContext[\.timeProvider] = SharedProvider(value: MockedTimeProvider())

        let createdReport = try await sut.contactDataReporterService().createReport().task()

        XCTAssertEqual(createdReport, correctContactDataReport)
    }

    private var correctContactDataReport: String {
        if Locale.current.languageCode == "de" {
            return correctContactDataReport_de
        }
        return correctContactDataReport_en
    }

    // swiftlint:disable line_length
    private var correctContactDataReport_en: String {
        return "Contact tracing:\nYour data collected and processed for contact tracing is stored locally in your app and encrypted on the luca server. The luca server is hosted by Deutsche Telekom AG. Certification of this can be found at: https://open-telekom-cloud.com/de/sicherheit/datenschutz-compliance. Furthermore, Bundesdruckerei Gruppe GmbH provides other IT infrastructure services that enable secure transmission of your data to health authorities. Our subcontractor neXenio provides software development, software maintenance and software operation services. Neither we nor neXenio GmbH can decrypt or clearly view your data at any time. During registration we verify your phone number by sending an automated SMS. For this purpose, your phone number will be sent to our SMS service provider. The current recipients can be found in our current privacy policy: https://www.luca-app.de/app-privacy-policy/\n\nOnly you can view, edit and delete your data within your app. If you have already checked in at a luca location and a health department has requested it from the operator, the dual-encrypted data will be decrypted by the operator and then by the health department. Only the health department can access the decrypted data while contact tracing and view your data. With the exception of the health authorities (legal basis Art. 6 para. 1 lit. c DSGVO in conjunction with the respective state ordinance to combat COVID-19 infections, Infection Protection Act), we do not pass on your data to third parties.\n-\nContact data:\nName:  \nAddress:  ,  \nPhone number:  \nMail: -\nStorage location: luca-Server, local\nStorage period: Deletion takes place 28 days after the deletion of the account (delete button) or annually at the end of each year\n-\nAll information about the processing, purposes, legal bases and deletion periods can be found in our privacy policy: https://www.luca-app.de/app-privacy-policy/. Your data will not be transferred to third countries. Automated decision-making and profiling according to Art. 22 DSGVO are not carried out by us or the contractors of the luca system.\nIf you would like to receive information about your data, which we have received as clear data through your own contact (e.g. support requests), please contact our Privacy Team at privacy@culture4life.de. Please share any other information that would help us identify you and research your related data with us. If you have been in contact with us via social media, information such as your e-mail address or the pseudonym you use in those social media channels are important in order to provide you with your requested information.\nIn accordance with Art. 77 DSGVO, you have the right to complain to the supervisory authority if you believe that your personal data is not processed lawfully. The address of the supervisory authority responsible for our company is: Berliner Beauftragte für Datenschutz und Informationsfreiheit, Friedrichstr. 219, 10969 Berlin, Tel.: 030/13889-0, e-mail: mailbox@datenschutz-berlin.de."
    }

    // swiftlint:disable line_length
    private var correctContactDataReport_de: String {
        return "Kontaktnachverfolgung:\nDeine Daten, die für die Kontaktnachverfolgung erhoben und verarbeitet wurden, sind lokal in deiner App und verschlüsselt auf dem luca-Server gespeichert. Der luca-Server wird von der Deutschen Telekom AG gehostet. Zertifizierung zu dieser finden Sie unter: https://open-telekom-cloud.com/de/sicherheit/datenschutz-compliance. Ebenfalls stellt die Bundesdruckerei Gruppe GmbH weitere IT-Infrastrukturleistungen, die eine sichere Übermittlung deiner Daten an die Gesundheitsämter ermöglichen. Unser Unterauftragnehmer nexenio übernimmt die Softwareentwicklung, Softwarewartungs- und Softwarebetriebsleistungen. Sowohl wir als auch die nexenio GmbH können zu keinem Zeitpunkt deine Daten entschlüsseln und in der Klarform einsehen. Bei der Registrierung verifizieren wir deine Telefonnummer durch automatisierten SMS-Versand. Hierfür erfolgt die Übermittlung der Telefonnummer an unsere SMS-Versanddienstleister:innen. Die aktuellen Empfänger kannst du unserer aktuellen Datenschutzerklärung entnehmen: https://www.luca-app.de/app-privacy-policy/\nNur du kannst deine Daten innerhalb deiner App einsehen, bearbeiten und löschen. Wenn du bereits bei einer luca Location eingecheckt und ein Gesundheitsamt diese bei der Betreiber:in angefragt hat, werden die zweifach verschlüsselten Daten von der Betreiber:in und anschließend von dem Gesundheitsamt entschlüsselt. Nur das Gesundheitsamt kann auf die entschlüsselten Daten im Zuge der Kontaktnachverfolgung zugreifen und deine Daten einsehen. Deine Daten geben wir mit Ausnahme der Gesundheitsämter (Rechtsgrundlage Art. 6 Abs. 1 lit. c DSGVO in Verbindung mit der jeweiligen Landesverordnung zur Bekämpfung von COVID-19 Infektionen, Infektionsschutzgesetz) grundsätzlich nicht an Dritte weiter.\n-\nKontaktdaten:\nName:  \nAdresse:  ,  \nTelefonnummer:  \nMailadresse: -\nSpeicherort: luca-Server, lokal\nSpeicherfrist: Löschung erfolgt 28 Tage nach der Löschung des Accounts (Löschbutton) oder jährlich am Ende jedes Jahres\n-\nAlle Informationen über die Verarbeitungen, Zwecke, Rechtsgrundlagen und Löschfristen findest du in unserer Datenschutzerklärung: https://www.luca-app.de/app-privacy-policy/. Deine Daten werden nicht in Drittländer übermittelt. Automatisierte Entscheidungsfindungen sowie Profiling gemäß Art. 22 DSGVO werden von uns sowie den Auftragnehmern des luca Systems nicht getätigt.\nMöchtest du eine Auskunft über deine Daten erhalten, die wir durch deine eigene Kontaktaufnahme (z. B. bei Supportanfragen) bei uns als Klardaten von dir erhalten haben, wende dich bitte an unser Privacy Team unter privacy@culture4life.de. Bitte teile uns auch weitere Hinweise mit, die uns ermöglichen, dich zu identifizieren und dazugehörige Daten bei uns zu recherchieren. Wenn du mit uns über Social Media in Kontakt warst, sind weitere Hinweise wie deine E-Mail-Adresse oder dein Pseudonym, das du in den Social-Media-Kanälen verwendest, wichtig um dir die Auskunft erteilen zu können.\nDu hast gemäß Art. 77 DSGVO das Recht, dich bei der Aufsichtsbehörde zu beschweren, wenn du der Ansicht bist, dass die Verarbeitung deiner personenbezogenen Daten nicht rechtmäßig erfolgt. Die Anschrift der für unser Unternehmen zuständigen Aufsichtsbehörde lautet: Berliner Beauftragte für Datenschutz und Informationsfreiheit, Friedrichstr. 219, 10969 Berlin, Tel.: 030/13889-0, E-Mail: mailbox@datenschutz-berlin.de. "
    }

    // MARK: Characterization Test: Documents Reporter

    func test_documentsReporterService_createReport_shouldCreateDocumentsReportCorrectly() async throws {
        DependencyContext[\.documentRepoService] = SharedProvider(value: MockedDocumentRepoService())

        let createdReport = try await sut.documentsReporterService().createReport().task()

        XCTAssertEqual(createdReport, correctDocumentsReport)
    }

    private var correctDocumentsReport: String {
        if Locale.current.languageCode == "de" {
            return correctDocumentsReport_de
        }
        return correctDocumentsReport_en
    }

    // swiftlint:disable line_length
    private var correctDocumentsReport_en: String {
        return "Covid certificates:\nWithin your luca app, you have the option to store a test result, vaccination or recovery certificate. Your data stored and transferred to the luca app will not leave your smartphone. Your data is only stored locally in your app on your smartphone. A transmission to our servers, to the operator or to the health authorities does not take place. Your data will not be passed on to third parties.\nOnly information necessary for identity matching (first and last name, date of birth, vaccination date, issuer and vaccine) is displayed in the app. Other data may be included in the test or ID document, but will not be displayed to you. All data contained in the QR code is listed here. To prevent misuse, so that the same document cannot be stored by different people in the luca app, a pseudonymized identifier is created by your app and transmitted to the luca system. Only the identifier is stored in the luca system. We cannot assign it to you.\n-\nAll information about the processing, purposes, legal bases and deletion periods can be found in our privacy policy: https://www.luca-app.de/app-privacy-policy/. Your data will not be transferred to third countries. Automated decision-making and profiling according to Art. 22 DSGVO are not carried out by us or the contractors of the luca system.\nIf you would like to receive information about your data, which we have received as clear data through your own contact (e.g. support requests), please contact our Privacy Team at privacy@culture4life.de. Please share any other information that would help us identify you and research your related data with us. If you have been in contact with us via social media, information such as your e-mail address or the pseudonym you use in those social media channels are important in order to provide you with your requested information.\nIn accordance with Art. 77 DSGVO, you have the right to complain to the supervisory authority if you believe that your personal data is not processed lawfully. The address of the supervisory authority responsible for our company is: Berliner Beauftragte für Datenschutz und Informationsfreiheit, Friedrichstr. 219, 10969 Berlin, Tel.: 030/13889-0, e-mail: mailbox@datenschutz-berlin.de."
    }

    // swiftlint:disable line_length
    private var correctDocumentsReport_de: String {
        return "Covidzertifikate:\nInnerhalb deiner luca App hast du die Möglichkeit ein Testergebnis, ein Impf- oder Genesenausweis zu hinterlegen. Deine dort hinterlegten und an die luca App übertragenen Daten verlassen dein Smartphone nicht. Deine Daten sind nur lokal in deiner App auf deinem Smartphone gespeichert. Eine Übermittlung an unsere Server, an den Betreiber oder an die Gesundheitsämter findet nicht statt. Deine Daten werden nicht an Dritte weitergegeben.\nIn der App werden nur die notwendigen Informationen zum Identitätsabgleich (Vor- und Nachname, Geburtsdatum, Impfdatum, Aussteller und Impfstoff) angezeigt. Weitere Daten können in dem Test oder Ausweisdokument enthalten sein, werden dir innerhalb der App jedoch nicht angezeigt. Alle in dem QR Code enthaltene Daten sind hier gelistet. Zur Vorbeugung von Missbrauch, sodass das Dokument nicht mehrfach von unterschiedlichen Personen in der luca App hinterlegt werden kann, wird von deiner luca App eine pseudonymisierte Kennung erstellt und an das luca System übertragen. Nur diese Kennung wird im luca System gespeichert. Diese können wir dir nicht zuordnen.\n-\nAlle Informationen über die Verarbeitungen, Zwecke, Rechtsgrundlagen und Löschfristen findest du in unserer Datenschutzerklärung: https://www.luca-app.de/app-privacy-policy/. Deine Daten werden nicht in Drittländer übermittelt. Automatisierte Entscheidungsfindungen sowie Profiling gemäß Art. 22 DSGVO werden von uns sowie den Auftragnehmern des luca Systems nicht getätigt.\nMöchtest du eine Auskunft über deine Daten erhalten, die wir durch deine eigene Kontaktaufnahme (z. B. bei Supportanfragen) bei uns als Klardaten von dir erhalten haben, wende dich bitte an unser Privacy Team unter privacy@culture4life.de. Bitte teile uns auch weitere Hinweise mit, die uns ermöglichen, dich zu identifizieren und dazugehörige Daten bei uns zu recherchieren. Wenn du mit uns über Social Media in Kontakt warst, sind weitere Hinweise wie deine E-Mail-Adresse oder dein Pseudonym, das du in den Social-Media-Kanälen verwendest, wichtig um dir die Auskunft erteilen zu können.\nDu hast gemäß Art. 77 DSGVO das Recht, dich bei der Aufsichtsbehörde zu beschweren, wenn du der Ansicht bist, dass die Verarbeitung deiner personenbezogenen Daten nicht rechtmäßig erfolgt. Die Anschrift der für unser Unternehmen zuständigen Aufsichtsbehörde lautet: Berliner Beauftragte für Datenschutz und Informationsfreiheit, Friedrichstr. 219, 10969 Berlin, Tel.: 030/13889-0, E-Mail: mailbox@datenschutz-berlin.de. "
    }
}
