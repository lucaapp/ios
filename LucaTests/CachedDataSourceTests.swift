import XCTest
import RxSwift
import DependencyInjection
@testable import Luca

class CachedDataSourceTests: XCTestCase {

    var dataSource: MockDataSource!
    var cache: CachedDataSource<String>!
    var timeProvider: MockedTimeProvider!

    override func setUpWithError() throws {
        timeProvider = MockedTimeProvider()
        DependencyContext[\.keyValueRepo] = SharedProvider(value: KeyValueRepoWithUnderlying(underlying: MemoryDataRepo<KeyValueRepoEntry>()))
        DependencyContext[\.timeProvider] = SharedProvider(value: timeProvider)
    }

    func test_invalidateEmptyCacheAndLoadFromCache_shouldThrowError() throws {
        prepareCache(.onceInRuntime)
        try cache.invalidateCache().blockingWait()
        try cache.invalidateCache().blockingWait()
        XCTAssertThrowsError(try cache.retrieve(loadOnlyFromCache: true).retrieveBlocking())
    }

    func test_onceInRuntime_loadTwoTimes_sourceCalledOneTime() async throws {
        prepareCache(.onceInRuntime)
        let retrievedFirst = try await cache.retrieve().task()
        let retrievedSecond = try await cache.retrieve().task()
        XCTAssertEqual(retrievedFirst, [dataSource.valueToEmit])
        XCTAssertEqual(retrievedSecond, [dataSource.valueToEmit])
        XCTAssertEqual(dataSource.callCounter, 1)
    }

    func test_retrieveOnce_invalidate_checkValidityIsFalse() async throws {
        prepareCache(.onceInRuntime)
        let retrievedFirst = try await cache.retrieve().task()
        try await cache.invalidateCache().task()
        XCTAssertEqual(try await cache.isCacheValid.task(), to: false)
        let retrievedSecond = try await cache.retrieve().task()
        XCTAssertEqual(retrievedFirst, [dataSource.valueToEmit])
        XCTAssertEqual(retrievedSecond, [dataSource.valueToEmit])
        XCTAssertEqual(dataSource.callCounter, 2)
    }

    func test_onceInRuntime_loadTwoTimesAndInvalidateInBetween_sourceCalledTwoTimes() throws {
        prepareCache(.onceInRuntime)
        let retrievedFirst = try cache.retrieve().retrieveBlocking()
        try cache.invalidateCache().blockingWait()
        let retrievedSecond = try cache.retrieve().retrieveBlocking()
        XCTAssertEqual(retrievedFirst, [dataSource.valueToEmit])
        XCTAssertEqual(retrievedSecond, [dataSource.valueToEmit])
        XCTAssertEqual(dataSource.callCounter, 2)
    }

    func test_onceInRuntime_loadTwoTimesAndChangeSourceInBetween_sourceCalledOneTime() throws {
        prepareCache(.onceInRuntime)
        let firstValue = dataSource.valueToEmit
        let retrievedFirst = try cache.retrieve().retrieveBlocking()
        dataSource.valueToEmit = "TEMP2"
        let retrievedSecond = try cache.retrieve().retrieveBlocking()
        XCTAssertEqual(retrievedFirst, [firstValue])
        XCTAssertEqual(retrievedSecond, [firstValue])
        XCTAssertEqual(dataSource.callCounter, 1)
    }

    func test_cacheNone_loadTwoTimes_sourceCalledTwoTimes() throws {
        prepareCache(.none)
        let retrievedFirst = try cache.retrieve().retrieveBlocking()
        let retrievedSecond = try cache.retrieve().retrieveBlocking()
        XCTAssertEqual(retrievedFirst, [dataSource.valueToEmit])
        XCTAssertEqual(retrievedSecond, [dataSource.valueToEmit])
        XCTAssertEqual(dataSource.callCounter, 2)
    }

    func test_cacheNone_loadTwoTimesAndOnlyFromCache_sourceCalledOneTime() throws {
        prepareCache(.none)
        let retrievedFirst = try cache.retrieve().retrieveBlocking()
        let retrievedSecond = try cache.retrieve(loadOnlyFromCache: true).retrieveBlocking()
        XCTAssertEqual(retrievedFirst, [dataSource.valueToEmit])
        XCTAssertEqual(retrievedSecond, [dataSource.valueToEmit])
        XCTAssertEqual(dataSource.callCounter, 1)
    }

    func test_cacheHalfYear_loadTwoTimes_sourceCalledOneTime() throws {
        prepareCache(.until(unit: .month, count: 6))
        // Set initial time
        timeProvider.valueToEmit = Calendar.current.date(from: DateComponents.init(year: 2000, month: 0, day: 0))!

        let retrievedFirst = try cache.retrieve().retrieveBlocking()

        // Let 5 months and 29 days pass by, cache should still be valid
        timeProvider.valueToEmit = Calendar.current.date(from: DateComponents.init(year: 2000, month: 5, day: 29))!

        let retrievedSecond = try cache.retrieve().retrieveBlocking()
        XCTAssertEqual(retrievedFirst, [dataSource.valueToEmit])
        XCTAssertEqual(retrievedSecond, [dataSource.valueToEmit])
        XCTAssertEqual(dataSource.callCounter, 1)
    }

    func test_cacheHalfYear_loadTwoTimesAndLetCacheExpire_sourceCalledTwoTimes() throws {
        prepareCache(.until(unit: .month, count: 6))
        // Set initial time
        timeProvider.valueToEmit = Calendar.current.date(from: DateComponents.init(year: 2000, month: 0))!

        let retrievedFirst = try cache.retrieve().retrieveBlocking()

        // Let 6 months pass by, cache should be invalid and source should be loaded second time
        timeProvider.valueToEmit = Calendar.current.date(from: DateComponents.init(year: 2000, month: 6))!
        timeProvider.valueToEmit = Calendar.current.date(byAdding: .second, value: 1, to: timeProvider.valueToEmit)!

        let retrievedSecond = try cache.retrieve().retrieveBlocking()
        XCTAssertEqual(retrievedFirst, [dataSource.valueToEmit])
        XCTAssertEqual(retrievedSecond, [dataSource.valueToEmit])
        XCTAssertEqual(dataSource.callCounter, 2)
    }

    func test_cacheOnceADay_loadTwoTimes_sourceCalledOneTime() throws {
        prepareCache(.onceIn(unit: .day))
        // Set initial time at the end of the day
        timeProvider.valueToEmit = Calendar.current.date(from: DateComponents.init(year: 2000, month: 0, day: 0, hour: 23, minute: 59, second: 58))!

        let retrievedFirst = try cache.retrieve().retrieveBlocking()

        // Let one second pass by, cache should still be valid
        timeProvider.valueToEmit = Calendar.current.date(from: DateComponents.init(year: 2000, month: 0, day: 0, hour: 23, minute: 59, second: 59))!

        let retrievedSecond = try cache.retrieve().retrieveBlocking()
        XCTAssertEqual(retrievedFirst, [dataSource.valueToEmit])
        XCTAssertEqual(retrievedSecond, [dataSource.valueToEmit])
        XCTAssertEqual(dataSource.callCounter, 1)
    }

    func test_cacheOnceADay_loadTwoTimesAndLetTwoSecondsPassBy_sourceCalledTwoTimes() throws {
        prepareCache(.onceIn(unit: .day))
        // Set initial time at the end of the day
        timeProvider.valueToEmit = Calendar.current.date(from: DateComponents.init(year: 2000, month: 0, day: 0, hour: 23, minute: 59, second: 58))!

        let retrievedFirst = try cache.retrieve().retrieveBlocking()

        // Let two seconds pass by, cache should be invalid
        timeProvider.valueToEmit = Date(timeIntervalSince1970: timeProvider.valueToEmit.timeIntervalSince1970 + 2)

        let retrievedSecond = try cache.retrieve().retrieveBlocking()
        XCTAssertEqual(retrievedFirst, [dataSource.valueToEmit])
        XCTAssertEqual(retrievedSecond, [dataSource.valueToEmit])
        XCTAssertEqual(dataSource.callCounter, 2)
    }

    private func prepareCache(_ cacheValidity: CacheValidity) {
        dataSource = MockDataSource()
        cache = BaseCachedDataSource(
           dataSource: dataSource,
           cacheDataRepo: KeyValueRepoCacheWrapper(uniqueCacheKey: "cacheTests"),
           cacheValidity: cacheValidity,
           uniqueCacheIdentifier: "cacheTestsCache"
       )
    }

}

class MockDataSource: DataSource {
    typealias T = String

    var valueToEmit = "TEMP"
    var callCounter = 0
    var delay: TimeInterval?

    func retrieve() -> Single<[String]> {
        if let delay = delay {
            return .just([valueToEmit])
                .delay(.milliseconds(Int(delay * 1000)), scheduler: ConcurrentDispatchQueueScheduler(qos: .default))
                .do(onSuccess: { _ in self.callCounter += 1 })

        }
        return .just([valueToEmit])
            .do(onSuccess: { _ in self.callCounter += 1 })
    }
}

class MockedTimeProvider: TimeProvider {
    var valueToEmit: Date = Date(timeIntervalSince1970: 0.0)

    var now: Date {
        valueToEmit
    }
}
