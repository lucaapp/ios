import XCTest
@testable import Luca

class AdditionalDataTests: XCTestCase {

    func test_intAndStringNotation() throws {

        let json = """
        {
            "table": "23"
        }
        """
        let json2 = """
        {
            "table": 23
        }
        """
        let json3 = """
        {
            "table": "23Z"
        }
        """

        let additionalData = try? JSONDecoder().decode(TraceIdAdditionalData.self, from: json.data(using: .utf8)!)
        let additionalData2 = try? JSONDecoder().decode(TraceIdAdditionalData.self, from: json2.data(using: .utf8)!)
        let additionalData3 = try? JSONDecoder().decode(TraceIdAdditionalData.self, from: json3.data(using: .utf8)!)

        XCTAssertEqual(additionalData?.tableIdentifier, "23")
        XCTAssertEqual(additionalData2?.tableIdentifier, "23")
        XCTAssertEqual(additionalData3?.tableIdentifier, "23Z")
    }

    func test_tableIdPresent() throws {
        let json = """
        {
            "tableId": "23"
        }
        """
        let json2 = """
        {
            "tableId": 23
        }
        """
        let json3 = """
        {
            "tableId": "23Z"
        }
        """

        let additionalData = try? JSONDecoder().decode(TraceIdAdditionalData.self, from: json.data(using: .utf8)!)
        let additionalData2 = try? JSONDecoder().decode(TraceIdAdditionalData.self, from: json2.data(using: .utf8)!)
        let additionalData3 = try? JSONDecoder().decode(TraceIdAdditionalData.self, from: json3.data(using: .utf8)!)

        XCTAssertEqual(additionalData?.tableIdentifier, "23")
        XCTAssertEqual(additionalData2?.tableIdentifier, nil) // It should only be a string!
        XCTAssertEqual(additionalData3?.tableIdentifier, "23Z")
    }

}
