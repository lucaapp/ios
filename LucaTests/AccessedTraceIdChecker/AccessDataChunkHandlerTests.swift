import XCTest
import RxSwift
import RxTest
@testable import Luca
import DependencyInjection

class AccessDataChunkHandlerTests: XCTestCase {

    var keyValueRepo: KeyValueRepoProtocol!
    let mockedBackend = MockedBackendAccessDataV4()
    var dataHandler: AccessDataChunkHandler!
    let timeProvider = MockedTimeProvider()

    var activeChunk: AccessedTracesDataChunk! = nil

    var archivedChunks: [String: AccessedTracesDataChunk] = [:]

    override func setUpWithError() throws {

        DependencyContext[\.timeProvider] = SharedProvider(value: DefaultTimeProvider())

        // swiftlint:disable:next force_try
        activeChunk = try! AccessedTracesDataChunk(data: Data(hex: "01000c0000017b63b5409c0000000000207dd5f8f746b44813397b75bdd571a8f7b2179f416249999580fa50"))

        // swiftlint:disable:next force_try
        let archivedChunk0 = try! AccessedTracesDataChunk(data: Data(hex: "01000c0000017b63b5409c0000000000207dd5f8f746b44813397b75bdd571ff"))
        archivedChunks["207dd5f8f746b44813397b75bdd571a8"] = archivedChunk0

        timeProvider.valueToEmit = Date(timeIntervalSince1970: TimeInterval(1629465428124) / 1000.0)
        keyValueRepo = RealmKeyValueRepo(key: nil, filenameSalt: "AccessDataChunkHandlerTests")
        DependencyContext[\.keyValueRepo] = SharedProvider(value: keyValueRepo)
        DependencyContext[\.timeProvider] = SharedProvider(value: timeProvider)
    }

    func setUpDelayed() {
        let cachedDataSource = BaseCachedDataSource(
            dataSource: mockedBackend.activeChunk(),
            cacheDataRepo: KeyValueRepoCacheWrapper(uniqueCacheKey: "ChunkCache"),
            cacheValidity: .none,
            uniqueCacheIdentifier: "ChunkCache"
        )

        DependencyContext[\.backendAccessDataV4] = SharedProvider(value: mockedBackend)
        DependencyContext[\.accessedTracesDataChunkCachedDataSource] = SharedProvider(value: cachedDataSource)

        dataHandler = AccessDataChunkHandler()
    }

    override func tearDown(completion: @escaping (Error?) -> Void) {
        mockedBackend.mockedActiveChunk = nil
        mockedBackend.mockedArchivedChunks = [:]
        Task {
            try? await keyValueRepo.removeAll().task()
            completion(nil)
        }
    }

    func test_dataCompleteness() async throws {
        mockedBackend.mockedActiveChunk = MockedValueAsyncDataOperation(data: activeChunk)
        mockedBackend.mockedArchivedChunks = [archivedChunks.keys.first!: MockedValueAsyncDataOperation(data: archivedChunks.values.first!)]
        setUpDelayed()

        let items = (try await dataHandler.fetch().task()).flatMap { $0 }
        XCTAssertEqual(items.count, 2)

        XCTAssertEqual(items[0], activeChunk)
        XCTAssertEqual(items[1], archivedChunks.values.first!)
    }

    func test_oldChunkWontBeDownloaded() async throws {
        timeProvider.valueToEmit = Date(timeIntervalSince1970: 1641566449)
        mockedBackend.mockedActiveChunk = MockedValueAsyncDataOperation(data: activeChunk)
        mockedBackend.mockedArchivedChunks = [archivedChunks.keys.first!: MockedValueAsyncDataOperation(data: archivedChunks.values.first!)]
        setUpDelayed()

        let items = (try await dataHandler.fetch().task()).flatMap { $0 }
        XCTAssertEqual(items.count, 1)

        XCTAssertEqual(items[0], activeChunk)
    }

    func test_dataCompleteness_failsOnChangedData() async throws {
        mockedBackend.mockedActiveChunk = MockedValueAsyncDataOperation(data: activeChunk)
        mockedBackend.mockedArchivedChunks = [archivedChunks.keys.first!: MockedValueAsyncDataOperation(data: archivedChunks.values.first!)]
        setUpDelayed()

        let items = (try await dataHandler.fetch().task()).flatMap { $0 }
        XCTAssertEqual(items.count, 2)

        var temp = activeChunk!
        temp.algorithm = 3
        XCTAssertNotEqual(items[0], temp)
        XCTAssertEqual(items[1], archivedChunks.values.first!)
    }

    func test_fetchMultipleTimes_archivedChunkCalledOnlyOnce() async throws {
        let fetchCount = 5
        var activeChunkCallCount: [Void] = []
        var archivedChunkCallCount: [Void] = []
        mockedBackend.mockedActiveChunk = MockedCallAsyncDataOperation(valueClosure: {
            activeChunkCallCount.append(Void())
            return self.activeChunk
        })
        mockedBackend.mockedArchivedChunks = [archivedChunks.keys.first!: MockedCallAsyncDataOperation(valueClosure: {
            archivedChunkCallCount.append(Void())
            return self.archivedChunks.values.first!
        })]
        setUpDelayed()

        for _ in 0..<fetchCount {

            let items = (try await dataHandler.fetch().task()).flatMap { $0 }
            XCTAssertEqual(items.count, 2)

            XCTAssertEqual(items[0], activeChunk)
            XCTAssertEqual(items[1], archivedChunks.values.first!)
        }

        XCTAssertEqual(activeChunkCallCount.count, fetchCount)
        XCTAssertEqual(archivedChunkCallCount.count, 1)
    }

    func test_fetchMultipleTimesAndResetCache_archivedChunkCalledTwice() async throws {
        let fetchCount = 5
        var activeChunkCallCount: [Void] = []
        var archivedChunkCallCount: [Void] = []
        mockedBackend.mockedActiveChunk = MockedCallAsyncDataOperation(valueClosure: {
            activeChunkCallCount.append(Void())
            return self.activeChunk
        })
        mockedBackend.mockedArchivedChunks = [archivedChunks.keys.first!: MockedCallAsyncDataOperation(valueClosure: {
            archivedChunkCallCount.append(Void())
            return self.archivedChunks.values.first!
        })]
        setUpDelayed()

        for _ in 0..<fetchCount {

            let items = (try await dataHandler.fetch().task()).flatMap { $0 }
            XCTAssertEqual(items.count, 2)

            XCTAssertEqual(items[0], activeChunk)
            XCTAssertEqual(items[1], archivedChunks.values.first!)
        }

        // Reset cache to force the handler to download archived chunks once more
        try await keyValueRepo.removeAll().task()

        let items = (try await dataHandler.fetch().task()).flatMap { $0 }
        XCTAssertEqual(items.count, 2)

        XCTAssertEqual(items[0], activeChunk)
        XCTAssertEqual(items[1], archivedChunks.values.first!)

        XCTAssertEqual(activeChunkCallCount.count, fetchCount + 1)
        XCTAssertEqual(archivedChunkCallCount.count, 2)
    }

}

class MockedCallAsyncDataOperation<T, E>: AsyncDataOperation<BackendError<E>, T> where E: RequestError {

    private let valueClosure: (() -> T)?
    private let errorClosure: (() -> BackendError<E>)?

    init(valueClosure: @escaping () -> T) {
        self.valueClosure = valueClosure
        self.errorClosure = nil
    }

    init(errorClosure: @escaping (() -> BackendError<E>)) {
        self.valueClosure = nil
        self.errorClosure = errorClosure
    }

    override func execute(completion: @escaping (T) -> Void, failure: @escaping (BackendError<E>) -> Void) -> (() -> Void) {
        if let value = valueClosure?() {
            completion(value)
        } else if let error = errorClosure?() {
            failure(error)
        }
        return {}
    }
}

class MockedValueAsyncDataOperation<T, E>: AsyncDataOperation<BackendError<E>, T> where E: RequestError {

    private let data: T?
    private let error: BackendError<E>?

    init(data: T) {
        self.data = data
        self.error = nil
    }

    init(error: BackendError<E>) {
        self.data = nil
        self.error = error
    }

    override func execute(completion: @escaping (T) -> Void, failure: @escaping (BackendError<E>) -> Void) -> (() -> Void) {
        if let retVal = data {
            completion(retVal)
        } else if let error = error {
            failure(error)
        }
        return {}
    }
}

class MockedBackendAccessDataV4: BackendAccessDataV4 {

    var mockedActiveChunk: AsyncDataOperation<BackendError<FetchAccessedTracesErrorV4>, AccessedTracesDataChunk>?
    var mockedArchivedChunks: [String: AsyncDataOperation<BackendError<FetchAccessedTracesArchivedChunkError>, AccessedTracesDataChunk>] = [:]

    override func activeChunk() -> AsyncDataOperation<BackendError<FetchAccessedTracesErrorV4>, AccessedTracesDataChunk> {
        mockedActiveChunk!
    }

    override func archivedChunk(chunkId: Data) -> AsyncDataOperation<BackendError<FetchAccessedTracesArchivedChunkError>, AccessedTracesDataChunk> {
        if let chunk = mockedArchivedChunks[chunkId.hexString] {
            return chunk
        }
        return MockedValueAsyncDataOperation(error: BackendError(backendError: .notFound))
    }
}
