import XCTest
@testable import Luca

class UUIDGenerationTests: XCTestCase {

    func test_discoveringVersion() {
        let v1 = UUID(uuidString: "33b935a6-37f3-11ec-8d3d-0242ac130003")!
        let v2 = UUID(uuidString: "33b935a6-37f3-21ec-8d3d-0242ac130003")!
        let v3 = UUID(uuidString: "33b935a6-37f3-31ec-8d3d-0242ac130003")!
        let v4 = UUID(uuidString: "33b935a6-37f3-41ec-8d3d-0242ac130003")!
        let invalid = UUID(uuidString: "33b935a6-37f3-a1ec-8d3d-0242ac130003")!

        XCTAssertEqual(v1.version, .v1)
        XCTAssertEqual(v2.version, .v2)
        XCTAssertEqual(v3.version, .v3)
        XCTAssertEqual(v4.version, .v4)
        XCTAssertEqual(invalid.version, nil)
    }

    func test_discoveringVariant() {
        let variant0 = UUID(uuidString: "33b935a6-37f3-11ec-0d3d-0242ac130003")!
        let variant1 = UUID(uuidString: "33b935a6-37f3-21ec-8d3d-0242ac130003")!
        let variant2 = UUID(uuidString: "33b935a6-37f3-31ec-cd3d-0242ac130003")!
        let invalid = UUID(uuidString: "33b935a6-37f3-a1ec-fe3d-0242ac130003")!

        XCTAssertEqual(variant0.variant, .variant0)
        XCTAssertEqual(variant1.variant, .variant1)
        XCTAssertEqual(variant2.variant, .variant2)
        XCTAssertEqual(invalid.variant, nil)
    }

    func test_generatingRandomBufferWithSpecificVersion() {
        let v1 = UUID(version: .v1, variant: .variant0)
        let v2 = UUID(version: .v2, variant: .variant0)
        let v3 = UUID(version: .v3, variant: .variant0)
        let v4 = UUID(version: .v4, variant: .variant0)

        XCTAssertEqual(v1?.version, .v1)
        XCTAssertEqual(v2?.version, .v2)
        XCTAssertEqual(v3?.version, .v3)
        XCTAssertEqual(v4?.version, .v4)
    }

    func test_generatingRandomBufferWithSpecificVariant() {
        let variant0 = UUID(version: .v1, variant: .variant0)
        let variant1 = UUID(version: .v1, variant: .variant1)
        let variant2 = UUID(version: .v1, variant: .variant2)

        XCTAssertEqual(variant0?.variant, .variant0)
        XCTAssertEqual(variant1?.variant, .variant1)
        XCTAssertEqual(variant2?.variant, .variant2)
    }

    func test_failingToGenerateUUIDIfBufferNot16Bytes() {
        XCTAssertEqual(UUID(data: Data([UInt8](repeating: 0, count: 15)), version: .v4, variant: .variant1), nil)
        XCTAssertEqual(UUID(data: Data([UInt8](repeating: 0, count: 17)), version: .v4, variant: .variant1), nil)
    }

}
