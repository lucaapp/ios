import XCTest
import RxSwift
import DependencyInjection
@testable import Luca
import BigInt

// swiftlint:disable line_length
class PoWTests: XCTestCase {

    var backend = MockedBackendPoWV4()
    var pow: PoWService!

    override func setUpWithError() throws {
        DependencyContext[\.timeProvider] = SharedProvider(value: DefaultTimeProvider())
        DependencyContext[\.backendPoWV4] = SharedProvider(value: backend)
        pow = PoWService()
    }

    func test_retrieveChallenge() throws {
        let challenge = PoWChallenge.init(id: "SomeTempID", t: "123", n: "2412", expiresAt: 928374)
        backend.challengeToEmit = challenge
        let retrievedChallenge = try pow.requestAndScheduleChallenge(type: .test).retrieveBlocking()
        XCTAssertEqual(challenge.id, retrievedChallenge)
    }

    func test_correctComputation() throws {
        let challenge = PoWChallenge(id: "aljsdlkajsd", t: "10", n: "113381873001135028388878506815502767020045389814250450418647587174337097693592514644350458936961039096658092456838891336880685017625646550243676978918856100541259452093417396354885954762684889698815113498306773096931836005652433450230487045620686994454870627357496657414722132064168151188593669998956272083821", expiresAt: Int(Date().timeIntervalSince1970) + 10000)
        let solution = try pow.solveBlocking(challenge: challenge)
        XCTAssertEqual(solution.id, challenge.id)
        XCTAssertEqual( "66387440485096562384052012263399706341752308079980206854782493983395578111908448488358018385446496924462021423032502020778104751188770072249170451720618023836508441331448088921416264838561204420637969453778232671906314676690029431243426064920140242708479883327089640825225113874311565116241686330667952053395", solution.w)
    }

    func test_challengeExpired() throws {
        let challenge = PoWChallenge(id: "aljsdlkajsd", t: "10", n: "1234567890", expiresAt: Int(Date().timeIntervalSince1970) - 10000)
        XCTAssertThrowsError(try pow.solveBlocking(challenge: challenge)) { error in
            XCTAssertEqual(error as? PoWServiceError, PoWServiceError.challengeExpired)
        }
    }

    func test_malformedT() throws {
        let challenge = PoWChallenge(id: "aljsdlkajsd", t: "asd", n: "1234567890", expiresAt: Int(Date().timeIntervalSince1970) + 10000)
        XCTAssertThrowsError(try pow.solveBlocking(challenge: challenge)) { error in
            XCTAssertEqual(error as? PoWServiceError, PoWServiceError.malformedChallenge)
        }
    }

    func test_malformedN() throws {
        let challenge = PoWChallenge(id: "aljsdlkajsd", t: "132", n: "asd", expiresAt: Int(Date().timeIntervalSince1970) + 10000)
        XCTAssertThrowsError(try pow.solveBlocking(challenge: challenge)) { error in
            XCTAssertEqual(error as? PoWServiceError, PoWServiceError.malformedChallenge)
        }
    }
}

class MockedBackendPoWV4: BackendPoWV4 {

    var challengeToEmit: PoWChallenge!

    override func requestChallenge(type: PoWType) -> AsyncDataOperation<BackendError<RequestPoWChallengeError>, PoWChallenge> {
        MockedValueAsyncDataOperation<PoWChallenge, RequestPoWChallengeError>(data: challengeToEmit)
    }

}
