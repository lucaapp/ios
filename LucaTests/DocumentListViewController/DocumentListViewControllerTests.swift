@testable import Luca
import DependencyInjection
import Cuckoo
import XCTest
import ViewControllerPresentationSpy
import RxTest

// swiftlint:disable type_body_length
final class DocumentListViewControllerTests: XCTestCase {

    var sut: DocumentListViewController!
    var sutViewModelMock: DocumentListViewModelMock!
    var lucaIdServiceMock: LucaIDServiceMock!
    var stagedRolloutServiceStub: StagedRolloutServiceStub!
    var notificationCenter: NotificationCenter!
    var applicationMock: ApplicationMock!

    override func setUpWithError() throws {
        try super.setUpWithError()

        lucaIdServiceMock = LucaIDServiceMock()
        DependencyContext[\.lucaIDService] = SharedProvider(value: lucaIdServiceMock)
        stagedRolloutServiceStub = StagedRolloutServiceStub()
        DependencyContext[\.stagedRolloutService] = SharedProvider(value: stagedRolloutServiceStub)
        DependencyContext[\.lucaIDTermsOfUse] = SharedProvider(value: LucaIDTermsOfUseServiceStub())
        DependencyContext[\.userService] = SharedProvider(value: UserService())
        DependencyContext[\.timeProvider] = SharedProvider(value: MockedTimeProvider())

        sut = DocumentListViewController.fromStoryboard()
        sutViewModelMock = DocumentListViewModelMock()
        sut.viewModel = sutViewModelMock
        notificationCenter = NotificationCenter()
        sut.notificationCenter = notificationCenter
        applicationMock = ApplicationMock()
        sut.application = applicationMock
    }

    func stubTimeSync(_ isInSync: Bool = true) {
        let timeSync = MockTimeSyncService()
        DependencyContext[\.timeSync] = SharedProvider(value: timeSync)
        stub(timeSync) { m in
            when(m).isInSync().thenReturn(.just(isInSync))
        }
    }

    func stubViewModelWithOnePersonAndNoDocuments() throws {
        let person = Person(firstName: "firstname", lastName: "lastname", type: .placeholder)
        sutViewModelMock.stubbedPeople = [person: [Document]()]
    }

    func stubViewModelWithOnePersonAndOneCoronaTest() throws {
        let document = CoronaTestStub()
        let person = Person(firstName: "firstname", lastName: "lastname", type: .placeholder)
        sutViewModelMock.stubbedPeople = [person: [document]]
    }

    func stubViewModelWithTwoPeopleWithCoronaTests() throws {
        let document = CoronaTestStub()
        let firstPerson = Person(firstName: "firstname", lastName: "lastname", type: .user)
        let secondPerson = Person(firstName: "firstname_2", lastName: "lastname_2", type: .placeholder)
        sutViewModelMock.stubbedPeople = [firstPerson: [document], secondPerson: [document]]
    }

    func stubViewModelWithOnePersonAndChildWithCoronaTests() throws {
        let document = CoronaTestStub()
        let firstPerson = Person(firstName: "firstname", lastName: "lastname", type: .placeholder)
        let secondPerson = Person(firstName: "firstname_2", lastName: "lastname_2", type: .child)
        sutViewModelMock.stubbedPeople = [firstPerson: [document], secondPerson: [document]]
    }

    func stubViewModelWithOnePersonAndMultipleVaccinations() throws {
        let vaccination1 = VaccinationStub(issuedAt: Calendar.current.date(byAdding: .day, value: -2, to: Date())!, doseNumber: 0)
        let vaccination2 = VaccinationStub(issuedAt: Calendar.current.date(byAdding: .month, value: -4, to: Date())!, doseNumber: 1)
        let vaccination3 = VaccinationStub(issuedAt: Calendar.current.date(byAdding: .month, value: -6, to: Date())!, doseNumber: 2)
        let vaccination4 = VaccinationStub(issuedAt: Calendar.current.date(byAdding: .day, value: -18, to: Date())!, doseNumber: 3)
        let person = Person(firstName: "firstname", lastName: "lastname", type: .placeholder)
        sutViewModelMock.stubbedPeople = [person: [vaccination1, vaccination2, vaccination3, vaccination4]]
    }

    override func tearDown() {
        sut = nil
        notificationCenter = nil
        UIWindow.destroy()
        super.tearDown()
    }

    func test_sut_subscribesToRevalidationOfViewModel() {
        UIWindow.create(for: sut)

        XCTAssertEqual(sutViewModelMock.revalidationCallCount, 1)
    }

    // MARK: View Setup

    func test_outlets_shouldBeConnected() throws {
        sut.loadViewIfNeeded()

        XCTAssertNotNil(sut.addButton, "addButton")
        XCTAssertNotNil(sut.notificationStackView, "notificationStackView")
        XCTAssertNotNil(sut.emptyStateView, "emptyStateView")
        XCTAssertNotNil(sut.welcomeImageView, "welcomeImageView")
        XCTAssertNotNil(sut.scrollView, "scrollView")
        XCTAssertNotNil(sut.stackView, "stackView")
    }

    func test_notificationStackView_initially_shouldBeEmpty() {
        sut.loadViewIfNeeded()

        XCTAssertEqual(sut.notificationStackView.arrangedSubviews.count, 0)
    }

    func test_notificationStackView_initially_shouldBeHidden() {
        sut.loadViewIfNeeded()

        XCTAssertTrue(sut.notificationStackView.isHidden)
    }

    func test_emptyStateView_initially_shouldHaveCorrectLayoutMargins() {
        sut.loadViewIfNeeded()

        XCTAssertEqual(sut.emptyStateView.layoutMargins, UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16))
        XCTAssertTrue(sut.emptyStateView.isLayoutMarginsRelativeArrangement)
    }

    func test_title_shouldBeCustomNavigationbarTitleLabel() {
        sut.loadViewIfNeeded()

        XCTAssertEqual(sut.navigationItem.title, L10n.IOSApp.My.Luca.title)
        XCTAssertTrue(sut.navigationItem.titleView is UIStackView)
    }

    func test_navigationItem_backButton_shouldBeEmpty() {
        sut.loadViewIfNeeded()
        guard let backButton = sut.navigationItem.backBarButtonItem else {
            XCTFail("Expected a back button")
            return
        }

        XCTAssertEqual(backButton.title, "")
        XCTAssertEqual(backButton.style, .plain)
        XCTAssertNil(backButton.target)
        XCTAssertNil(backButton.action)
    }

    func test_scrollView_contentInset_shouldBeSetCorrectly() {
        sut.loadViewIfNeeded()

        XCTAssertEqual(sut.scrollView.contentInset, UIEdgeInsets(top: 0, left: 0, bottom: 43, right: 0))
    }

    // MARK: Navigation Bar

    func test_navigationItem_shouldContainOneButton() {
        sut.loadViewIfNeeded()

        XCTAssertEqual(sut.navigationItem.rightBarButtonItems?.count, 1)
    }

    func test_addChildrenButtonView_colorMode_shouldBeLight() {
        sut.loadViewIfNeeded()

        XCTAssertEqual(sut.addChildrenButtonView?.colorMode, .light)
    }

    func test_addChildrenButtonView_accessibilityLabel_shouldBeSetCorrectly() {
        sut.loadViewIfNeeded()

        XCTAssertEqual(sut.addChildrenButtonView?.button.accessibilityLabel, L10n.IOSApp.Children.List.title)
    }

    func test_addChildrenButtonView_whenTapped_shouldPresentChildren() {
        sut.loadViewIfNeeded()

        DependencyContext[\.personRepo] = SharedProvider(value: PersonRepo(key: Data(count: 64)))
        DependencyContext[\.personService] = SharedProvider(value: PersonService())
        let navigationControllerMock = NavigationControllerMock(rootViewController: sut)
        UIWindow.create(for: navigationControllerMock)

        sut.addChildrenButtonView?.button.tap()

        XCTAssertTrue(navigationControllerMock.pushedViewController is ChildrenListViewController)
    }

    // MARK: Application State Observing

    func test_whenApplicationDidEnterBackground_shouldDismissScanner() {
        sut.loadViewIfNeeded()
        let presentationVerifier = PresentationVerifier()
        let dismissalVerifier = DismissalVerifier()
        sut.presentQRCodeScanner()
        let presentedScanner = presentationVerifier.verify(animated: true, presentingViewController: sut)

        notificationCenter.post(name: UIApplication.didEnterBackgroundNotification, object: nil)

        dismissalVerifier.verify(animated: true, dismissedViewController: presentedScanner)
    }

    // MARK: IBActions & Actions

    func test_addTestPressed_shouldPresentAlert() {
        let alertVerifier = AlertVerifier()

        sut.addTestPressed(UIButton())

        alertVerifier.verify(title: nil, message: nil, animated: true, actions: [.cancel(L10n.IOSApp.Navigation.Basic.cancel), .default(L10n.IOSApp.My.Luca.Add.document), .default(L10n.IOSApp.My.Luca.calendar)], preferredStyle: .actionSheet)
    }

    func test_didTapPending_shouldOpenIdNowURL() throws {
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainScheduler = testScheduler

        sut.didTapPending(UITapGestureRecognizer())
        testScheduler.start()

        XCTAssertEqual(applicationMock.openURLCallCount, 1)
        XCTAssertTrue(applicationMock.openURLParameter?.absoluteString.hasSuffix("test_id") ?? false)
    }

    func test_didTapPending_withError_shouldPresentAlert() throws {
        lucaIdServiceMock.fetchStateShouldFail = true
        let alertVerifier = AlertVerifier()
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainScheduler = testScheduler

        sut.didTapPending(UITapGestureRecognizer())
        testScheduler.start()

        XCTAssertEqual(alertVerifier.presentedCount, 1)
    }

    func test_didLongPressPending_shouldPresentDeletionAlert() throws {
        let alertVerifier = AlertVerifier()
        let gestureRecognizer = UILongPressGestureRecognizer()
        gestureRecognizer.state = .began

        sut.didLongPressPending(gestureRecognizer)

        XCTAssertEqual(alertVerifier.presentedCount, 1)
        alertVerifier.verify(title: L10n.IOSApp.Id.deleteDialogId, message: "", animated: true, actions: [.default(L10n.IOSApp.Navigation.Basic.yes), .cancel(L10n.IOSApp.Navigation.Basic.cancel)])
    }

    func test_didLongPressPending_deletionAlert_whenConfirmed_shouldArchive() throws {
        let alertVerifier = AlertVerifier()
        let gestureRecognizer = UILongPressGestureRecognizer()
        gestureRecognizer.state = .began

        sut.didLongPressPending(gestureRecognizer)

        try alertVerifier.executeAction(forButton: L10n.IOSApp.Navigation.Basic.yes)
        XCTAssertEqual(lucaIdServiceMock.archiveDataCallCount, 1)
    }

    func test_didLongPressPending_deletionAlert_whenConfirmedButFailed_shouldPresentAlert() throws {
        lucaIdServiceMock.archiveDataShouldFail = true
        let alertVerifier = AlertVerifier()
        let gestureRecognizer = UILongPressGestureRecognizer()
        gestureRecognizer.state = .began
        sut.didLongPressPending(gestureRecognizer)

        try alertVerifier.executeAction(forButton: L10n.IOSApp.Navigation.Basic.yes)

        XCTAssertEqual(alertVerifier.presentedCount, 2)
    }

    func test_didTapDelete_shouldPresentAlert() {
        let alertVerifier = AlertVerifier()
        let document = CoronaTestStub()

        sut.didTapDelete(for: document, viewController: sut)

        alertVerifier.verify(title: L10n.IOSApp.Test.Delete.title, message: document.deletionConfirmationMessage, animated: true, actions: [.default(L10n.IOSApp.Navigation.Basic.yes), .cancel(L10n.IOSApp.Navigation.Basic.no)])
    }

    func test_didTapDelete_presentedAlert_whenConfirmed_shouldCallRemoveDocumentOnViewModelAndPopNavigation() throws {
        let navigationControllerMock = NavigationControllerMock(rootViewController: sut)
        let alertVerifier = AlertVerifier()
        let document = CoronaTestStub()
        sut.didTapDelete(for: document, viewController: sut)

        try alertVerifier.executeAction(forButton: L10n.IOSApp.Navigation.Basic.yes)

        XCTAssertEqual(sutViewModelMock.removeCallCount, 1)
        XCTAssertEqual(navigationControllerMock.popViewControllerCallCount, 1)
    }

    func test_didTapDelete_presentedAlert_whenConfirmedButFailed_PresentAlert() throws {
        sutViewModelMock.removeShouldFail = true
        let alertVerifier = AlertVerifier()
        let document = CoronaTestStub()
        sut.didTapDelete(for: document, viewController: sut)

        try alertVerifier.executeAction(forButton: L10n.IOSApp.Navigation.Basic.yes)

        XCTAssertEqual(alertVerifier.presentedCount, 2)
    }

    // MARK: Time Difference View

    func test_whenTimeIsInSync_shouldHideTimeDifferenceViewAndNotificationStackView() {
        UIWindow.create(for: sut)

        let isTimeDifferenceViewNil = sut.timeDifferenceView == nil
        let isTimeDifferenceViewHidden = sut.timeDifferenceView?.isHidden
        XCTAssertTrue(isTimeDifferenceViewHidden ?? isTimeDifferenceViewNil)
        XCTAssertTrue(sut.notificationStackView.isHidden)
    }

    func test_whenTimeIsNotInSync_shouldShowTimeDifferenceViewAndNotificationStackView() throws {
        (sut.viewModel as? DocumentListViewModelMock)?.stubbedTimesync = false
        UIWindow.create(for: sut)

        let timeDifferenceView = try XCTUnwrap(sut.timeDifferenceView)
        XCTAssertFalse(timeDifferenceView.isHidden)
        XCTAssertFalse(sut.notificationStackView.isHidden)
        XCTAssertTrue(sut.notificationStackView.arrangedSubviews.contains(timeDifferenceView))
    }

    // MARK: Accessibility

    func test_viewDidAppear_shouldSetAccessibilityLabelOfAddButtonCorrectly() {
        sut.loadViewIfNeeded()
        sut.viewDidAppear(false)

        XCTAssertEqual(sut.addButton.accessibilityLabel, L10n.IOSApp.Test.Add.title)
    }

    // MARK: Document Listing

    func test_stackView_withoutAnyPeople_shouldBeEmpty() throws {
        try stubViewModelWithOnePersonAndNoDocuments()
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        sut.loadViewIfNeeded()

        testScheduler.start()
        XCTAssertEqual(sut.stackView.arrangedSubviews.count, 0)
    }

    func test_stackView_withOnePerson_withoutAnyDocuments_withoutLucaId_shouldBeEmptyAndHidden() throws {
        stagedRolloutServiceStub.fakedIsFeatureEnabled = false
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        UIWindow.create(for: sut)

        testScheduler.start()
        XCTAssertEqual(sut.stackView.arrangedSubviews.count, 0)
        XCTAssertTrue(sut.stackView.isHidden)
    }

    func test_stackView_withOnePersonAndCoronaTest_containsOneDocumentGroupViewWithCoronaTestView() throws {
        try stubViewModelWithOnePersonAndOneCoronaTest()
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        UIWindow.create(for: sut)

        testScheduler.start()
        XCTAssertEqual(sut.stackView.arrangedSubviews.count, 1)
        guard let documentGroupView = sut.stackView.arrangedSubviews[0] as? DocumentGroupView else {
            XCTFail("Expected to contain DocumentGroupView")
            return
        }
        XCTAssertEqual(documentGroupView.person, sutViewModelMock.stubbedPeople.first!.key)
        XCTAssertTrue(documentGroupView.documentViewList!.first is CoronaTestView)
    }

    func test_stackView_withOnePersonAndCoronaTest_whenWarningPressed_shouldPresentAlertView() throws {
        let alertVerifier = AlertVerifier()
        try stubViewModelWithOnePersonAndOneCoronaTest()
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        UIWindow.create(for: sut)
        testScheduler.start()
        guard let documentGroupView = sut.stackView.arrangedSubviews.first as? DocumentGroupView,
              let documentView = documentGroupView.documentViewList?.first else {
                  XCTFail("Expected to contain DocumentGroupView with DocumentView")
                  return
              }

        documentView.warningPressed?()

        alertVerifier.verify(title: L10n.IOSApp.Document.Verification.Failed.title,
                             message: L10n.IOSApp.Document.Verification.Failed.description,
                             animated: true,
                             actions: [AlertVerifier.Action.default(L10n.IOSApp.Navigation.Basic.ok)])
    }

    func test_stackView_withTwoPeopleWithCoronaTest_containsTwoDocumentGroupsViewWithCoronaTestViews() throws {
        try stubViewModelWithTwoPeopleWithCoronaTests()
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        UIWindow.create(for: sut)

        testScheduler.start()
        XCTAssertEqual(sut.stackView.arrangedSubviews.count, 2)
        guard let firstDocumentGroupView = sut.stackView.arrangedSubviews[0] as? DocumentGroupView,
              let secondDocumentGroupView = sut.stackView.arrangedSubviews[1] as? DocumentGroupView else {
                  XCTFail("Expected to contain DocumentGroupViews")
                  return
              }
        XCTAssertTrue(sutViewModelMock.stubbedPeople.keys.contains(firstDocumentGroupView.person))
        XCTAssertTrue(sutViewModelMock.stubbedPeople.keys.contains(secondDocumentGroupView.person))
        XCTAssertTrue(firstDocumentGroupView.documentViewList!.first is CoronaTestView)
        XCTAssertTrue(secondDocumentGroupView.documentViewList!.first is CoronaTestView)
    }

    func test_stackView_withOnePersonAndChildWithCoronaTests_containsTwoDocumentGroupsViewWithSeparatorInBetween() throws {
        try stubViewModelWithOnePersonAndChildWithCoronaTests()
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        UIWindow.create(for: sut)

        testScheduler.start()
        XCTAssertEqual(sut.stackView.arrangedSubviews.count, 3)
        XCTAssertTrue(sut.stackView.arrangedSubviews[0] is DocumentGroupView)
        XCTAssertTrue(sut.stackView.arrangedSubviews[1] is DocumentListSeparatorView)
        XCTAssertTrue(sut.stackView.arrangedSubviews[2] is DocumentGroupView)
    }

    func test_stackView_withOnePersonAndMultipleVaccinations_listsVaccinationsInCorrectOrder() throws {
        try stubViewModelWithOnePersonAndMultipleVaccinations()
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        UIWindow.create(for: sut)

        testScheduler.start()
        XCTAssertEqual(sut.stackView.arrangedSubviews.count, 1)
        guard let documentGroupView = sut.stackView.arrangedSubviews[0] as? DocumentGroupView else {
            XCTFail("Expected to contain DocumentGroupView")
            return
        }
        let doseNumbers = documentGroupView.documentViewList!.compactMap { documentView -> Int in
            guard let vaccination = documentView.document as? Vaccination else { return 0 }
            return vaccination.doseNumber
        }
        XCTAssertEqual(doseNumbers, [3, 0, 1, 2])
    }

    // MARK: DocumentViewDelegate

    func test_documentViewDelegate_didSelect_shouldPresentDocumentViewController() throws {
        let navigationControllerMock = NavigationControllerMock(rootViewController: sut)
        let document = CoronaTestStub()
        let person = Person(firstName: "firstname", lastName: "lastname", type: .placeholder)
        let documentView = DocumentView()
        documentView.owner = person

        sut.didSelect(documentView, document: document)

        guard let pushedViewController = try XCTUnwrap(navigationControllerMock.pushedViewController) as? DocumentViewController else {
            XCTFail("Expected DocumentViewController")
            return
        }
        XCTAssertEqual(pushedViewController.owner, person)
        XCTAssertEqual(pushedViewController.documents.count, 1)
        XCTAssertEqual(pushedViewController.focusedDocument?.originalCode, document.originalCode)
        XCTAssertTrue(pushedViewController.delegate is DocumentListViewController)
    }

    func test_documentViewDelegate_didSelect_withLucaID_shouldAuthenticateDeviceUser() throws {
        let navigationControllerMock = NavigationControllerMock(rootViewController: sut)
        let document = LucaID(payload: LucaIDParsedData.testObject(with: .success))
        let person = Person(firstName: "firstname", lastName: "lastname", type: .placeholder)
        let documentView = DocumentView()
        documentView.owner = person

        sut.didSelect(documentView, document: document)

        guard let pushedViewController = try XCTUnwrap(navigationControllerMock.pushedViewController) as? DocumentViewController else {
            XCTFail("Expected DocumentViewController")
            return
        }
        XCTAssertEqual(sutViewModelMock.authenticateDeviceUserCallCount, 1)
        XCTAssertEqual(pushedViewController.owner, person)
        XCTAssertEqual(pushedViewController.documents.count, 1)
        XCTAssertEqual(pushedViewController.focusedDocument?.originalCode, document.originalCode)
        XCTAssertTrue(pushedViewController.delegate is DocumentListViewController)
    }

    func test_documentViewDelegate_didSelect_withLucaID_whenUserAuthenticationFails_shouldPresentAlertView() throws {
        sutViewModelMock.authenticateDeviceUserCompletionResult = false
        let alertVerifier = AlertVerifier()
        let document = LucaID(payload: LucaIDParsedData.testObject(with: .success))
        let person = Person(firstName: "firstname", lastName: "lastname", type: .placeholder)
        let documentView = DocumentView()
        documentView.owner = person

        sut.didSelect(documentView, document: document)

        alertVerifier.verify(title: L10n.IOSApp.Id.Authentication.Failed.title, message: L10n.IOSApp.Id.Authentication.Failed.message, animated: true, actions: [.default(L10n.IOSApp.Navigation.Basic.ok)])
    }

    // MARK: Empty State View & Welcome Image View & Count Label

    func test_emptyStateView_withoutAnyPeople_shouldNotBeHidden() throws {
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        sut.loadViewIfNeeded()
        sut.emptyStateView.isHidden = true
        UIWindow.create(for: sut)

        testScheduler.start()
        XCTAssertFalse(sut.emptyStateView.isHidden)
    }

    func test_welcomeImageView_withoutAnyPeople_withoutLucaId_shouldNotBeHidden() throws {
        stagedRolloutServiceStub.fakedIsFeatureEnabled = false
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        sut.loadViewIfNeeded()
        sut.emptyStateView.isHidden = true

        testScheduler.start()
        XCTAssertFalse(sut.welcomeImageView.isHidden)
    }

    func test_countLabel_withoutChildren_shouldBeEmpty() throws {
        try stubViewModelWithOnePersonAndOneCoronaTest()
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        UIWindow.create(for: sut)
        testScheduler.start()

        XCTAssertEqual(sut.addChildrenButtonView?.countLabel.text, "")
        XCTAssertEqual(sut.addChildrenButtonView?.countLabel.accessibilityLabel, L10n.IOSApp.Children.Number.zero)
    }

    func test_countLabel_withOneChild_shouldBe1() throws {
        try stubViewModelWithOnePersonAndChildWithCoronaTests()
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        UIWindow.create(for: sut)
        testScheduler.start()

        XCTAssertEqual(sut.addChildrenButtonView?.countLabel.text, "1")
        XCTAssertEqual(sut.addChildrenButtonView?.countLabel.accessibilityLabel, L10n.IOSApp.Children.Number.one)
    }

    // MARK: LucaID

    func test_stackView_withLucaIdProcessNotStarted_shouldContainLucaIdInfoButtonView() throws {
        let expectation = XCTestExpectation(description: "Assertions on main thread should be called")
        lucaIdServiceMock.fakedIdProcessStarted = false
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        UIWindow.create(for: sut)
        testScheduler.start()

        DispatchQueue.main.async {
            guard let lucaIdInfoButtonView = self.sut.stackView.arrangedSubviews[0] as? LucaIdInfoButtonView else {
                XCTFail("Expected LucaIdInfoButtonView")
                return
            }
            XCTAssertEqual(lucaIdInfoButtonView.imageView.image, Asset.addIDCard.image)
            XCTAssertEqual(lucaIdInfoButtonView.titleLabel.text, L10n.IOSApp.Id.addIdPromptHeadline)
            XCTAssertEqual(lucaIdInfoButtonView.descriptionLabel.text, L10n.IOSApp.Id.addIdPromptText)
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 0.1)
    }

    func test_stackView_withIdDataStateSuccess_shouldRemoveAllStackViewSubviews() throws {
        let expectation = XCTestExpectation(description: "Assertions on main thread should be called")
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        UIWindow.create(for: sut)
        testScheduler.start()
        sut.idInfoButtonView = UIView()
        sut.stackView.addArrangedSubview(sut.idInfoButtonView!)

        DispatchQueue.main.async {
            XCTAssertEqual(self.sut.stackView.arrangedSubviews.count, 0)
            XCTAssertNil(self.sut.idInfoButtonView)
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 0.1)
    }

    func test_stackView_withIdDataStateFailure_shouldRemoveAllStackViewSubviewsAndShowFailedAlert() throws {
        let alertVerifier = AlertVerifier()
        let expectation = XCTestExpectation(description: "Assertions on main thread should be called")
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        lucaIdServiceMock.fakedProcessState = .failed
        UIWindow.create(for: sut)
        testScheduler.start()
        sut.idInfoButtonView = UIView()
        sut.stackView.addArrangedSubview(sut.idInfoButtonView!)

        DispatchQueue.main.async {
            XCTAssertEqual(self.sut.stackView.arrangedSubviews.count, 0)
            XCTAssertNil(self.sut.idInfoButtonView)
            alertVerifier.verify(title: L10n.IOSApp.Id.idVerificationFailureHeadline, message: L10n.IOSApp.Id.idGenericStatusShortText, animated: true, actions: [AlertVerifier.Action.default(L10n.IOSApp.Navigation.Basic.ok)])
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 0.1)
    }

    func test_stackView_withIdDataStateQueued_shouldContainQueuedIdInfoView() throws {
        let expectation = XCTestExpectation(description: "Assertions on main thread should be called")
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        lucaIdServiceMock.fakedProcessState = .queued
        UIWindow.create(for: sut)
        testScheduler.start()

        DispatchQueue.main.async {
            guard let lucaIdInfoButtonView = self.sut.stackView.arrangedSubviews[0] as? LucaIdInfoButtonView else {
                XCTFail("Expected LucaIdInfoButtonView")
                return
            }
            XCTAssertEqual(lucaIdInfoButtonView.imageView.image, Asset.spinningWheel.image)
            XCTAssertEqual(lucaIdInfoButtonView.titleLabel.text, L10n.IOSApp.Id.queuedIdPromptHeadline)
            XCTAssertEqual(lucaIdInfoButtonView.descriptionLabel.text, L10n.IOSApp.Id.queuedIdPromptText)
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 0.1)
    }

    func test_stackView_withIdDataStatePending_shouldContainPendingIdInfoView() throws {
        let expectation = XCTestExpectation(description: "Assertions on main thread should be called")
        let testScheduler = TestScheduler(initialClock: 0)
        sut.mainSchedulerAsync = testScheduler
        lucaIdServiceMock.fakedProcessState = .pending
        UIWindow.create(for: sut)
        testScheduler.start()

        DispatchQueue.main.async {
            guard let lucaIdInfoButtonView = self.sut.stackView.arrangedSubviews[0] as? LucaIdPendingButtonView else {
                XCTFail("Expected LucaIdPendingButtonView")
                return
            }
            XCTAssertEqual(lucaIdInfoButtonView.identLabel.text, "test_id")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 0.1)
    }
}
