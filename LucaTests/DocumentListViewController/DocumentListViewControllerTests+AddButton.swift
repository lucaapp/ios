@testable import Luca
import UIKit
import XCTest
import RxSwift
import ViewControllerPresentationSpy

final class DocumentListViewControllerAddButtonTests: XCTestCase {

    var sut: DocumentListViewController!

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = DocumentListViewController.fromStoryboard()
        sut.loadViewIfNeeded()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func test_addButton_shouldHaveCorrectTitle() throws {
        XCTAssertEqual(sut.addButton.titleLabel?.text, L10n.IOSApp.My.Luca.add)
    }

    func test_addButtonTapped_shouldPresentActionSheet() throws {
        let alertVerifier = AlertVerifier()

        sut.addButton.tap()

        XCTAssertEqual(alertVerifier.presentedCount, 1)
        XCTAssertEqual(alertVerifier.preferredStyle, .actionSheet)
        XCTAssertEqual(alertVerifier.actions.count, 3)
    }

    func test_addButtonTapped_presentedActionSheet_shouldContainCancelButton() throws {
        let alertVerifier = AlertVerifier()

        sut.addButton.tap()

        XCTAssertEqual(alertVerifier.actions[0].style, .cancel)
        XCTAssertEqual(alertVerifier.actions[0].title, L10n.IOSApp.Navigation.Basic.cancel)
        XCTAssertEqual(alertVerifier.actions[0].accessibilityLabel, L10n.IOSApp.Navigation.Basic.cancel)
    }

    func test_addButtonTapped_presentedActionSheet_shouldContainAddDocumentButton() throws {
        let alertVerifier = AlertVerifier()

        sut.addButton.tap()

        XCTAssertEqual(alertVerifier.actions[1].style, .default)
        XCTAssertEqual(alertVerifier.actions[1].title, L10n.IOSApp.My.Luca.Add.document)
        XCTAssertEqual(alertVerifier.actions[1].accessibilityLabel, L10n.IOSApp.My.Luca.Add.document)
    }

    func test_addButtonTapped_presentedActionSheet_shouldContainTestAppointmentButton() throws {
        let alertVerifier = AlertVerifier()

        sut.addButton.tap()

        XCTAssertEqual(alertVerifier.actions[2].style, .default)
        XCTAssertEqual(alertVerifier.actions[2].title, L10n.IOSApp.My.Luca.calendar)
        XCTAssertEqual(alertVerifier.actions[2].accessibilityLabel, L10n.IOSApp.My.Luca.calendar + L10n.IOSApp.External.Link.Button.accessibility)
    }
}
