import XCTest
import RxSwift
import RxTest
@testable import Luca

class AccessedTraceIdEncoderTests: XCTestCase {

    func testEncoding() throws {

        let traceId = "+b/VUZDEF8Uc0i+qoDrZaQ=="
        let healthDepartmentId = UUID(uuidString: "a8d96fdf-fc6c-454c-a5e3-41abdc702208")!
        let warningLevel: UInt8 = 1
        let expectedOutput = Data(hex: "f7b2179f416249999580fa50")

        let traceInfo = TraceInfo(traceId: traceId, checkin: 0, checkout: 0, locationId: "", createdAt: 0)

        let encoded = AccessedTraceIdEncoder.encode(
            traceIds: [traceInfo],
            for: healthDepartmentId,
            warningLevel: warningLevel
        )

        XCTAssertEqual(encoded.count, 1)
        XCTAssertEqual(encoded.first?.encryptedTraceId.prefix(expectedOutput.count), expectedOutput)
    }

}
