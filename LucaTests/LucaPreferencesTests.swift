import XCTest
import DependencyInjection
import RxSwift
@testable import Luca

class LucaPreferencesTests: XCTestCase {
    var keyValueRepo: RealmKeyValueRepo!
    var preferences: LucaPreferences!

    override func setUpWithError() throws {
        keyValueRepo = RealmKeyValueRepo(key: nil, filenameSalt: "TestRepo")
        DependencyContext[\.importantDataKeyValueRepo] = SharedProvider(value: keyValueRepo)
        preferences = LucaPreferences()
    }

    override func tearDownWithError() throws {
        try keyValueRepo.removeFile().blockingWait()
    }

    var propertyTest_hasUUID: Bool {
        get async {
            let uuid = try? await preferences.get(\.uuid).task()
            let hasUUID = uuid != nil
            return hasUUID
        }
    }

    func test_saveToPropertyWithDefaultValue() throws {
        try preferences.set(\.welcomePresented, value: true).blockingWait()
        let retrieved = try preferences.get(\.welcomePresented).retrieveBlocking()
        XCTAssertEqual(retrieved, true)
    }

    func test_saveToPropertyWithoutDefaultValue() throws {
        let data = UserRegistrationData()
        data.firstName = "Temp"

        try preferences.set(\.userRegistrationData, value: data).blockingWait()
        let retrieved = try preferences.get(\.userRegistrationData).retrieveBlocking()
        XCTAssertEqual(retrieved?.firstName, data.firstName)
        XCTAssertNotNil(retrieved?.firstName)
    }

    func test_saveToUserDataProperty() throws {
        try preferences.set(\.firstName, value: "Something").blockingWait()
        let retrieved = try preferences.get(\.firstName).retrieveBlocking()
        XCTAssertEqual(retrieved, "Something")
    }

    func test_allProperties() async throws {
        let data = UserRegistrationData()
        data.firstName = "Something"
        let uuid = UUID(uuidString: "3d0a4bdd-bb83-4243-9eca-e3cb89303553")!
        let requests = [PhoneNumberVerificationRequest.init(challengeId: "123", date: Date(timeIntervalSince1970: 123), phoneNumber: "123", verified: true)]
        try await preferences.set(\.userRegistrationData, value: data)
            .andThen(preferences.set(\.uuid, value: uuid))
            .andThen(preferences.set(\.currentOnboardingPage, value: 2))
            .andThen(preferences.set(\.onboardingComplete, value: true))
            .andThen(preferences.set(\.welcomePresented, value: true))
            .andThen(preferences.set(\.donePresented, value: true))
            .andThen(preferences.set(\.dataPrivacyPresented, value: true))
            .andThen(preferences.set(\.phoneNumberVerified, value: true))
            .andThen(preferences.set(\.termsAcceptedVersion, value: 100))
            .andThen(preferences.set(\.verificationRequests, value: requests))
            .andThen(preferences.set(\.checkoutNotificationScheduled, value: true))
            .andThen(preferences.set(\.appStoreReviewCheckoutCounter, value: 2))
            .task()

        let retrievedData = try preferences.get(\.userRegistrationData).retrieveBlocking()
        XCTAssertEqual(retrievedData?.firstName, data.firstName)
        XCTAssertNotNil(retrievedData?.firstName)
        XCTAssertEqual(try await preferences.get(\.uuid).task(), to: uuid)
        XCTAssertEqual(try await preferences.get(\.currentOnboardingPage).task(), to: 2)
        XCTAssertEqual(try await preferences.get(\.onboardingComplete).task(), to: true)
        XCTAssertEqual(try await preferences.get(\.welcomePresented).task(), to: true)
        XCTAssertEqual(try await preferences.get(\.donePresented).task(), to: true)
        XCTAssertEqual(try await preferences.get(\.dataPrivacyPresented).task(), to: true)
        XCTAssertEqual(try await preferences.get(\.phoneNumberVerified).task(), to: true)
        XCTAssertEqual(try await preferences.get(\.termsAcceptedVersion).task(), to: 100)
        XCTAssertEqual(try await preferences.get(\.verificationRequests).task(), to: requests)
        XCTAssertEqual(try await preferences.get(\.checkoutNotificationScheduled).task(), to: true)
        XCTAssertEqual(try await preferences.get(\.appStoreReviewCheckoutCounter).task(), to: 2)
    }

    func test_remove_withDefaultValue() async throws {
        try await preferences.set(\.welcomePresented, value: true).task()
        XCTAssertTrue(try preferences.get(\.welcomePresented).retrieveBlocking())
        try await preferences.remove(\.welcomePresented).task()
        XCTAssertFalse(try preferences.get(\.welcomePresented).retrieveBlocking())
    }

    func test_remove_withoutDefaultValue() async throws {
        let uuid = UUID(uuidString: "3d0a4bdd-bb83-4243-9eca-e3cb89303553")!
        try await preferences.set(\.uuid, value: uuid).task()
        XCTAssertEqual(try preferences.get(\.uuid).retrieveBlocking(), uuid)
        try await preferences.remove(\.uuid).task()
        XCTAssertNil(try preferences.get(\.uuid).retrieveBlocking())
    }

    func test_changes_withDefaultValue() async throws {
        var values: [Bool] = []
        var disposeBag = DisposeBag()
        preferences.currentAndChanges(\.welcomePresented)
            .do(onNext: { values.append($0) })
            .subscribe()
            .disposed(by: disposeBag)

        try preferences.set(\.welcomePresented, value: true)
            .andThen(preferences.set(\.welcomePresented, value: false))
            .andThen(preferences.set(\.welcomePresented, value: true))
            .andThen(preferences.remove(\.welcomePresented))
            .blockingWait()
        let welcomePresented = try await preferences.get(\.welcomePresented).task()
        XCTAssertEqual(welcomePresented, false)
        disposeBag = DisposeBag()
        XCTAssertEqual(values, [false, true, false, true, false])
    }

    func test_changes_inUserDataShortcuts() async throws {
        var values: [String?] = []
        var disposeBag = DisposeBag()
        preferences.currentAndChanges(\.userRegistrationData)
            .map { $0?.firstName }
            .do(onNext: { values.append($0) })
            .subscribe()
            .disposed(by: disposeBag)

        try await preferences.set(\.firstName, value: "true").task()
        try await preferences.set(\.firstName, value: "false").task()
        let firstName = try await preferences.get(\.firstName).task()
        XCTAssertEqual(firstName, "false")
        disposeBag = DisposeBag()
        XCTAssertEqual(values, [nil, "true", "false"])
    }

    func test_changes_withoutDefaultValue() async throws {
        let uuid = UUID(uuidString: "3d0a4bdd-bb83-4243-9eca-e3cb89303553")!
        var values: [UUID?] = []
        var disposeBag = DisposeBag()
        preferences.currentAndChanges(\.uuid)
            .do(onNext: { values.append($0) })
            .subscribe()
            .disposed(by: disposeBag)

        try await preferences.set(\.uuid, value: uuid).task()
        try await preferences.remove(\.uuid).task()
        disposeBag = DisposeBag()
        XCTAssertEqual(values.count, 3)
        XCTAssertEqual(values.filter { $0 == nil }.count, 2)
        XCTAssertEqual(values.filter { $0 != nil }.count, 1)
    }

    func test_blockingInProperty() async throws {
        try await preferences.set(\.uuid, value: UUID()).task()
        let hasUUID = await propertyTest_hasUUID
        XCTAssertTrue(hasUUID)
        XCTAssertTrue(try preferences.get(\.uuid).retrieveBlocking() != nil)
    }
}

public func XCTAssertTrue(value: Bool) {
    XCTAssertTrue(value)
}

public func XCTAssertEqual<T: Equatable>(_ leftValue: T, to: T) {
    XCTAssertEqual(leftValue, to)
}
