@testable import Luca
import XCTest

final class UserRegistrationDataTests: XCTestCase {

    // MARK: isRequiredDataComplete

    func test_isRequiredDataComplete_withoutFirstName_shouldReturnFalse() throws {
        let sut = UserRegistrationData.completeUserRegistrationData
        sut.firstName = nil

        XCTAssertFalse(sut.isRequiredDataComplete)
    }

    func test_isRequiredDataComplete_withoutLastName_shouldReturnFalse() throws {
        let sut = UserRegistrationData.completeUserRegistrationData
        sut.lastName = nil

        XCTAssertFalse(sut.isRequiredDataComplete)
    }

    func test_isRequiredDataComplete_withBothNames_shouldReturnTrue() throws {
        let sut = UserRegistrationData.completeUserRegistrationData

        XCTAssertTrue(sut.isRequiredDataComplete)
    }

    // MARK: isDataComplete

    func test_isDataComplete_withoutFirstName_shouldReturnFalse() throws {
        let sut = UserRegistrationData.completeUserRegistrationData
        sut.firstName = nil

        XCTAssertFalse(sut.isDataComplete)
    }

    func test_isDataComplete_withoutLastName_shouldReturnFalse() throws {
        let sut = UserRegistrationData.completeUserRegistrationData
        sut.lastName = nil

        XCTAssertFalse(sut.isDataComplete)
    }

    func test_isDataComplete_withoutStreet_shouldReturnFalse() throws {
        let sut = UserRegistrationData.completeUserRegistrationData
        sut.street = nil

        XCTAssertFalse(sut.isDataComplete)
    }

    func test_isDataComplete_withoutHouseNumber_shouldReturnFalse() throws {
        let sut = UserRegistrationData.completeUserRegistrationData
        sut.houseNumber = nil

        XCTAssertFalse(sut.isDataComplete)
    }

    func test_isDataComplete_withoutPostCode_shouldReturnFalse() throws {
        let sut = UserRegistrationData.completeUserRegistrationData
        sut.postCode = nil

        XCTAssertFalse(sut.isDataComplete)
    }

    func test_isDataComplete_withoutCity_shouldReturnFalse() throws {
        let sut = UserRegistrationData.completeUserRegistrationData
        sut.city = nil

        XCTAssertFalse(sut.isDataComplete)
    }

    func test_isDataComplete_withoutPhoneNumber_shouldReturnFalse() throws {
        let sut = UserRegistrationData.completeUserRegistrationData
        sut.phoneNumber = nil

        XCTAssertFalse(sut.isDataComplete)
    }
}
