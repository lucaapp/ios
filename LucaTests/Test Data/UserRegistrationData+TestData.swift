@testable import Luca
import Foundation

extension UserRegistrationData {

    static var completeUserRegistrationData: UserRegistrationData {
        let userRegistrationData = UserRegistrationData()
        userRegistrationData.firstName = "some firstName"
        userRegistrationData.lastName = "some lastName"
        userRegistrationData.street = "some street"
        userRegistrationData.houseNumber = "some houseNumber"
        userRegistrationData.postCode = "some postCode"
        userRegistrationData.city = "some city"
        userRegistrationData.phoneNumber = "some phoneNumber"
        userRegistrationData.email = "some email"
        return userRegistrationData
    }
}
