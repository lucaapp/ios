@testable import Luca
import XCTest
import DependencyInjection
import RxSwift
import RxCocoa
import RxBlocking
import Cuckoo
import RxTest

final class CheckInInteractorTests: XCTestCase {

    private var sut: CheckInInteractor!
    private var presenterForSUT: UIViewController!
    private var dailyPublicKeyVerifierStub: DailyPublicKeyVerifierStub!
    private var lucaPreferences: LucaPreferences!

    // MARK: Set up

    override func setUp() {
        super.setUp()
        DependencyContext[\.userService] = SharedProvider(value: UserService())
        stubTraceIdService()
        stubBackendDailyKeyV4()
        stubDailyPublicKeyVerifier()
        stubLucaPreferences()
        presenterForSUT = UIViewController()
        sut = CheckInInteractor(presenter: presenterForSUT, qrString: "")
    }

    private func stubTraceIdService() {
        DependencyContext[\.traceInfoRepo] = SharedProvider(value: TraceInfoRepo(key: Data.init(count: 64)))
        DependencyContext[\.timeProvider] = SharedProvider(value: MockedTimeProvider())
        DependencyContext[\.traceIdService] = SharedProvider(value: TraceIdServiceStub())
    }

    private func stubBackendDailyKeyV4() {
        let requestDaily = MockAsyncDataOperation<BackendError<CurrentDailyKeyError>, CurrentDailyKey>()
        let requestIssuers = MockAsyncDataOperation<BackendError<FetchIssuerKeysError>, [IssuerKeys]>()
        let backend = MockBackendDailyKeyV4()
        stub(backend) { m in
            when(m.fetchAllIssuerKeys()).thenReturn(requestIssuers)
            when(m.fetchDailyPubKey()).thenReturn(requestDaily)
        }
        DependencyContext[\.backendDailyKeyV4] = SharedProvider(value: backend)
    }

    private func stubDailyPublicKeyVerifier() {
        dailyPublicKeyVerifierStub = DailyPublicKeyVerifierStub(rootCertificateSource: Single.just(""),
                                                                    intermediateCertificatesSource: Single.just([""]))
        DependencyContext[\.dailyPublicKeyVerifier] = SharedProvider(value: dailyPublicKeyVerifierStub)
    }

    private func stubLucaPreferences() {
        lucaPreferences = LucaPreferences()
        DependencyContext[\.importantDataKeyValueRepo] = SharedProvider(value: RealmKeyValueRepo(key: Data.init(count: 64), filenameSalt: ""))
        DependencyContext[\.lucaPreferences] = SharedProvider(value: lucaPreferences)
    }

    // MARK: Tear down

    override func tearDown() {
        sut = nil
        presenterForSUT = nil
        dailyPublicKeyVerifierStub = nil
        lucaPreferences = nil
        super.tearDown()
    }

    // MARK: Error Handling

    func test_interact_withInvalidDailyKey_shouldThrowError() throws {
        dailyPublicKeyVerifierStub.isKeyValidMockedValue = false
        try lucaPreferences.set(\.userRegistrationData, value: UserRegistrationData.completeUserRegistrationData).blockingWait()

        do {
            try sut.interact().blockingWait()
            XCTFail("Expected sut.interact() to throw")
        } catch let error as CheckInInteractorError {
            XCTAssertEqual(error, .dailyKeyOutdated)
        }
    }

    func test_interact_withIncompleteUserData_shouldThrowError() throws {
        let userRegistrationData = UserRegistrationData.completeUserRegistrationData
        userRegistrationData.firstName = nil
        try lucaPreferences.set(\.userRegistrationData, value: userRegistrationData).blockingWait()

        do {
            try sut.interact().blockingWait()
            XCTFail("Expected sut.interact() to throw")
        } catch let error as CheckInInteractorError {
            XCTAssertEqual(error, .incompleteContactData)
        }
    }
}
