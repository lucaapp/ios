@testable import Luca
import RxSwift
import XCTest
import DependencyInjection

final class PaymentDetailViewModelTests: XCTestCase {

    var sut: PaymentDetailViewModel!
    var campaignRepo: CampaignSubmissionRepo!
    var payment: Payment {
        var payment = Payment(locationName: "some location", locationId: "1", amount: 1100, invoiceAmount: 123, tipAmount: 500, dateTime: 5648392, status: .closed)
        payment.invoiceAmount = 600
        return payment
    }

    override func setUpWithError() throws {
        try super.setUpWithError()
        campaignRepo = CampaignSubmissionRepo(key: KeyFactory.randomBytes(size: 64)!)
        DependencyContext[\.lucaPayment] = SharedProvider(value: StubbedPaymentService())
        DependencyContext[\.campaignService] = SharedProvider(value: StubbedCampaignService())
        DependencyContext[\.campaignRepo] = SharedProvider(value: campaignRepo)
        sut = PaymentDetailViewModel(payment: payment)
    }

    override func tearDown() async throws {
        try await super.tearDown()
        sut = nil
        try await campaignRepo.removeFile().task()
    }

    func test_paymentTitle_shouldBeSetCorrectly() {
        XCTAssertEqual(sut.paymentTitle, payment.locationName)
    }

    func test_date_shouldBeSetCorrectly() {
        XCTAssertEqual(sut.date, payment.dateTimeParsed.formattedDate)
    }

    func test_time_shouldBeSetCorrectly() {
        XCTAssertEqual(sut.time, payment.dateTimeParsed.formattedTime(.short))
    }

    func test_table_shouldBeSetCorrectly() {
        XCTAssertNil(sut.table)
    }

    func test_sum_shouldBeSetCorrectly() {
        let money = Money<EUR>(double: payment.amount, currency: EUR())
        XCTAssertEqual(sut.sum, money.formattedAmountWithCode)
    }

    func test_amount_shouldBeSetCorrectly() throws {
        let invoiceAmount = try XCTUnwrap(payment.invoiceAmount)
        let money = Money<EUR>(double: invoiceAmount, currency: EUR())
        XCTAssertEqual(sut.amount, money.formattedAmountWithCode)
    }

    func test_supportMailRecipient_shouldBeSetCorrectly() {
        XCTAssertEqual(sut.supportMailRecipient, L10n.IOSApp.Payment.Support.Email.recipient)
    }

    func test_supportMailSubject_shouldBeSetCorrectly() {
        XCTAssertEqual(sut.supportMailSubject, L10n.UserApp.Pay.Support.Mail.subject)
    }
}

class StubbedPaymentService: LucaPaymentService {
    override func fetchTipTopUpCampaignAmountFormatted(forPayment payment: Payment) -> Single<String?> {
        .just(nil)
    }
}

class StubbedCampaignService: CampaignService {
    override func checkCampaign(for locationId: String, type: CampaignType, date: Date? = nil) -> Single<PaymentCampaign?> {
        .just(nil)
    }
}
