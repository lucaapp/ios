@testable import Luca
import XCTest
import DependencyInjection
import RxSwift
import RxCocoa
import Cuckoo

final class MainCheckinViewModelTests: XCTestCase {

    private var sut: MainCheckinViewModel!
    private var dailyPublicKeyVerifierStub: DailyPublicKeyVerifierStub!
    private var lucaPreferences: LucaPreferences!

    // MARK: Set up

    override func setUpWithError() throws {
        stubBackendDailyKeyV4()
        stubDailyPublicKeyVerifier()
        stubLucaPreferences()
        sut = MainCheckinViewModel()
        try super.setUpWithError()
    }

    private func stubBackendDailyKeyV4() {
        let requestDaily = MockAsyncDataOperation<BackendError<CurrentDailyKeyError>, CurrentDailyKey>()
        let requestIssuers = MockAsyncDataOperation<BackendError<FetchIssuerKeysError>, [IssuerKeys]>()
        let backend = MockBackendDailyKeyV4()
        stub(backend) { m in
            when(m.fetchAllIssuerKeys()).thenReturn(requestIssuers)
            when(m.fetchDailyPubKey()).thenReturn(requestDaily)
        }
        DependencyContext[\.backendDailyKeyV4] = SharedProvider(value: backend)
    }

    private func stubDailyPublicKeyVerifier() {
        dailyPublicKeyVerifierStub = DailyPublicKeyVerifierStub(rootCertificateSource: Single.just(""),
                                                                    intermediateCertificatesSource: Single.just([""]))
        DependencyContext[\.dailyPublicKeyVerifier] = SharedProvider(value: dailyPublicKeyVerifierStub)
    }

    private func stubLucaPreferences() {
        lucaPreferences = LucaPreferences()
        DependencyContext[\.importantDataKeyValueRepo] = SharedProvider(value: RealmKeyValueRepo(key: Data.init(count: 64), filenameSalt: ""))
        DependencyContext[\.lucaPreferences] = SharedProvider(value: lucaPreferences)
    }

    // MARK: Tear down

    override func tearDown() {
        sut = nil
        dailyPublicKeyVerifierStub = nil
        super.tearDown()
    }

    // MARK: isShowQRCodeButtonEnabled

    func test_optionalButtonsEnabled_withValidDailyKey_andCompleteUserData_shouldReturnTrue() async throws {
        try await lucaPreferences.set(\.userRegistrationData, value: UserRegistrationData.completeUserRegistrationData).task()
        let isEnabled = (await sut.optionalButtonsEnabled.asInfallible(onErrorJustReturn: false).take(1).task()).first
        let isEnabledUnwrapped = try XCTUnwrap(isEnabled)
        XCTAssertTrue(isEnabledUnwrapped)
    }

    func test_optionalButtonsEnabled_withInvalidDailyKey_shouldReturnFalse() async throws {
        dailyPublicKeyVerifierStub.isKeyValidMockedValue = false
        try await lucaPreferences.set(\.userRegistrationData, value: UserRegistrationData.completeUserRegistrationData).task()

        let isEnabled = (await sut.optionalButtonsEnabled.asInfallible(onErrorJustReturn: false).take(1).task()).first!
        let isEnabledUnwrapped = try XCTUnwrap(isEnabled)
        XCTAssertFalse(isEnabledUnwrapped)
    }

    func test_optionalButtonsEnabled_withIncompleteUserData_shouldReturnFalse() async throws {
        let userRegistrationData = UserRegistrationData.completeUserRegistrationData
        userRegistrationData.firstName = nil
        try await lucaPreferences.set(\.userRegistrationData, value: userRegistrationData).task()

        let isEnabled = (await sut.optionalButtonsEnabled.asInfallible(onErrorJustReturn: true).take(1).task()).first
        let isEnabledUnwrapped = try XCTUnwrap(isEnabled)
        XCTAssertFalse(isEnabledUnwrapped)
    }

    // MARK: dailyPublicKeyAvailable

    func test_dailyPublicKeyAvailable_withValidKey_shouldReturnTrue() async throws {
        let isValid = (await sut.dailyPublicKeyAvailable.asInfallible(onErrorJustReturn: false).take(1).task()).first
        let isValidUnwrapped = try XCTUnwrap(isValid)
        XCTAssertTrue(isValidUnwrapped)
    }

    func test_dailyPublicKeyAvailable_withValidKey_shouldReturnFalse() async throws {
        dailyPublicKeyVerifierStub.isKeyValidMockedValue = false

        let isValid = (await sut.dailyPublicKeyAvailable.asInfallible(onErrorJustReturn: true).take(1).task()).first
        let isValidUnwrapped = try XCTUnwrap(isValid)
        XCTAssertFalse(isValidUnwrapped)
    }

    // MARK: isUserContactDataComplete

    func test_isUserContactDataComplete_withCompleteData_shouldReturnTrue() async throws {
        try await lucaPreferences.set(\.userRegistrationData, value: UserRegistrationData.completeUserRegistrationData).task()

        let isComplete = (await sut.isUserContactDataComplete.asInfallible(onErrorJustReturn: false).take(1).task()).first
        let isCompleteUnwrapped = try XCTUnwrap(isComplete)
        XCTAssertTrue(isCompleteUnwrapped)
    }

    func test_isUserContactDataComplete_withoutFirstName_shouldReturnFalse() async throws {
        let userRegistrationData = UserRegistrationData.completeUserRegistrationData
        userRegistrationData.firstName = nil
        try await lucaPreferences.set(\.userRegistrationData, value: userRegistrationData).task()

        let isComplete = (await sut.isUserContactDataComplete.asInfallible(onErrorJustReturn: true).take(1).task()).first
        let isCompleteUnwrapped = try XCTUnwrap(isComplete)
        XCTAssertFalse(isCompleteUnwrapped)
    }
}
