import XCTest
@testable import Luca

class MoneyTests: XCTestCase {

    func testMoneyNumberInitialisation() {
        let doubleAmount = 10.0
        XCTAssertEqual(Money(double: doubleAmount, currency: EUR()).amount, Decimal(doubleAmount))
    }

    func testMoneyStringInitialisation() {

        let formatter = NumberFormatter()
        formatter.generatesDecimalNumbers = true
        formatter.currencyCode = "EUR"
        formatter.numberStyle = NumberFormatter.Style.decimal

        let stringEURAmount = "10,00"
        let stringIntEURAmount = "10"
        let stringNegativeEURAmount = "-10,00"
        let stringEURCodeAmount = "10,00€"
        XCTAssertEqual(Money(stringWithLocale: stringEURAmount, currency: EUR())?.amount, formatter.number(from: stringEURAmount)?.decimalValue)
        XCTAssertEqual(Money(stringWithLocale: stringIntEURAmount, currency: EUR())?.amount, 10.00)
        XCTAssertEqual(Money(stringWithLocale: stringNegativeEURAmount, currency: EUR())?.amount, formatter.number(from: stringNegativeEURAmount)?.decimalValue)
        XCTAssertNil(Money(stringWithLocale: stringEURCodeAmount, currency: EUR())?.amount)
    }

    func testAddOperations() {

        let amount1EUR = Money(double: 5.15, currency: EUR())
        let amount2EUR = Money(stringWithLocale: "12", currency: EUR())!
        let amount3EUR = Money(string: "12", currency: EUR())!
        let amount1USD = Money(double: 7.01, currency: USD())
        let convertedAmount2USD = amount2EUR.convert(to: USD(), ratio: 0.5)

        XCTAssertNotNil(convertedAmount2USD)
        XCTAssertEqual((amount1EUR + amount2EUR).amount, NSNumber(value: 17.15).decimalValue)
        XCTAssertEqual((amount2EUR + amount3EUR).amount, NSNumber(value: 24).decimalValue)
        XCTAssertEqual((amount1EUR + 2).amount, NSNumber(value: 7.15).decimalValue)
        XCTAssertEqual((2.12 + amount1EUR).amount, NSNumber(value: 7.27).decimalValue)
        XCTAssertEqual((convertedAmount2USD + amount1USD).amount, NSNumber(value: 13.01).decimalValue)
    }

    func testSubstrOperations() {

        let amount1EUR = Money(double: 5.15, currency: EUR())
        let amount2EUR = Money(string: "12", currency: EUR())

        XCTAssertEqual((amount1EUR - amount2EUR!).amount, NSNumber(value: -6.85).decimalValue)
        XCTAssertEqual((amount2EUR! - amount1EUR).amount, NSNumber(value: 6.85).decimalValue)
        XCTAssertEqual((amount1EUR - 0.12).amount, NSNumber(value: 5.03).decimalValue)
        XCTAssertEqual((55.34 - amount1EUR).amount, NSNumber(value: 50.19).decimalValue)
    }

    func testMultOperations() {

        let amount1EUR = Money(double: 5.15, currency: EUR())
        let amount2EUR = Money(string: "12", currency: EUR())

        XCTAssertEqual((amount1EUR * amount2EUR!).amount, NSNumber(value: 61.80).decimalValue)
        XCTAssertEqual((amount2EUR! * 0.15).amount, NSNumber(value: 1.80).decimalValue)
        XCTAssertEqual((1.20 * amount2EUR!).amount, NSNumber(value: 14.40).decimalValue)
    }

    func testDivideOperations() {

        let amount1EUR = Money(double: 6.18, currency: EUR())
        let amount2EUR = Money(string: "2", currency: EUR())!

        XCTAssertEqual((amount1EUR/3).amount, NSNumber(value: 2.06).decimalValue)
        XCTAssertEqual((amount1EUR/amount2EUR).amount, NSNumber(value: 3.09).decimalValue)
        XCTAssertEqual((6/amount2EUR).amount, NSNumber(value: 3).decimalValue)
    }

    func testCompareOperations() {

        let amount1USD = Money(double: 5.15, currency: USD())
        let amount2USD = Money(string: "12", currency: USD())!

        XCTAssertTrue(amount1USD < amount2USD)
        XCTAssertFalse(20.2 < amount2USD)
        XCTAssertTrue(amount1USD < 6)
        XCTAssertFalse(amount1USD > amount2USD)
        XCTAssertFalse(5 > amount2USD)
        XCTAssertFalse(amount1USD > 6)
        XCTAssertTrue(amount1USD == 5.15)
        XCTAssertFalse(amount1USD == amount2USD)
        XCTAssertFalse(1.01 == amount2USD)
    }

    func testFormatOperations() {

        let codeFormatter = NumberFormatter()
        codeFormatter.numberStyle = .currency
        codeFormatter.currencyCode = "EUR"

        let amount1EUR = Money(double: 1115.15, currency: EUR())

        XCTAssertEqual(amount1EUR.formattedAmountWithCode, codeFormatter.string(from: amount1EUR.amount as NSNumber))

        let numberFormatter = Formatters.currencyAmountWithoutCodeFormatter
        numberFormatter.locale = Locale(identifier: "de_DE")

        let amount2USD = Money(double: 1222.46, currency: USD())

        XCTAssertEqual(numberFormatter.string(from: amount2USD.amount as NSNumber), "1.222,46")
    }

    func test_378Case() {
        let amount = 37.8
        let inCents = Int(amount * 100)
        XCTAssertEqual(inCents, to: 3779)

        let money = Money(double: amount, currency: EUR())
        let moneyInCents = (money * 100).toInt32
        XCTAssertEqual(moneyInCents, to: 3780)
    }

    func testSpecificError() {

        let pQuantity = Money(string: "0.2857142857142857", currency: EUR())!
        let pPrice = Money(string: "7.00000000000000035", currency: EUR())!

        let calced = pQuantity * pPrice * Money(double: 100, currency: EUR())
        XCTAssertEqual("\(calced.amount)", to: "199.9999999999999999999999999999995")
        XCTAssertEqual("\(calced.toInt32)", to: "200")

        XCTAssertEqual("\(Money(string: "199.9999999999999995", currency: EUR())!.toInt32)", to: "200")
        XCTAssertEqual("\(Money(string: "199.999999999999995", currency: EUR())!.toInt32)", to: "200")
        XCTAssertEqual("\(Money(string: "199.99999999999995", currency: EUR())!.toInt32)", to: "199")
        XCTAssertEqual("\(Money(string: "199.9999999999995", currency: EUR())!.toInt32)", to: "199")

        // 64 bit values have a bug. Always use 32 bit versions. More information: https://stackoverflow.com/a/56037951
        XCTAssertEqual("\(NSDecimalNumber(decimal: calced.amount).intValue)", to: "0")
        XCTAssertEqual("\(NSDecimalNumber(decimal: calced.amount).int32Value)", to: "200")

    }
}
