import XCTest
@testable import Luca

class PaymentCampaignTests: XCTestCase {

    func test_discountAmount_withoutDiscountValues_shouldReturnNil() throws {
        let sut = try PaymentCampaign.raffle()
        let amount = Money<EUR>.init(double: 100, currency: EUR())

        let discountAmount = sut.discount(amount: amount)
        XCTAssertNil(discountAmount)
    }

    func test_discountAmount_with50PercentDiscount_whenDiscountAmountIsLowerThanMaxDiscountAmount_shouldReturnHalfAmount() throws {
        let sut = try PaymentCampaign.lucaDiscount()
        let amount = Money<EUR>.init(double: 8.99, currency: EUR())

        let discountAmount = sut.discount(amount: amount)
        XCTAssertEqual(discountAmount?.amount, to: NSNumber(value: 4.49).decimalValue)
    }

    func test_discountAmount_with50PercentDiscount_whenDiscountAmountIsHigherThanMaxDiscountAmount_shouldReturnAmountMinusMaxDiscount() throws {
        let sut = try PaymentCampaign.lucaDiscount()
        let amount = Money<EUR>.init(double: 3000, currency: EUR())

        let discountAmount = sut.discount(amount: amount)
        XCTAssertEqual(discountAmount?.toInt32, to: 2800)
    }
}

private extension PaymentCampaign {

    static func raffle() throws -> PaymentCampaign {
        return try JSONDecoder().decode(PaymentCampaign.self, from: raffleJSON().data(using: .utf8)!)
    }

    static func lucaDiscount() throws -> PaymentCampaign {
        return try JSONDecoder().decode(PaymentCampaign.self, from: discountCampaignJSON().data(using: .utf8)!)
    }

    static func raffleJSON() -> String {
        """
              {
                  "id": "c2a46bd6-61de-4dc3-a8a8-1dcd854527ac",
                  "locationGroupId": "c53627cc-426e-437f-aae4-478b5d69e54b",
                  "campaign": "PayRaffleAndTipTopUp",
                  "paymentCampaign": {
                    "name": "PayRaffleAndTipTopUp",
                    "discountPercentage": null,
                    "discountMaxAmount": null,
                    "startsAt": 1656509478,
                    "endsAt": null
                  },
                  "startsAt": 1656509478,
                  "endsAt": null,
                  "createdAt": 1653483146
                }
        """
    }

    static func discountCampaignJSON() -> String {
        """
              {
                  "id": "64a2877c-000b-4ad5-ae72-0dbe0c492e43",
                  "locationGroupId": "c53627cc-426e-437f-aae4-478b5d69e54b",
                  "campaign": "luca-discount-40",
                  "paymentCampaign": {
                    "name": "luca-discount-40",
                    "discountPercentage": 50,
                    "discountMaxAmount": 200,
                    "startsAt": 1656509439,
                    "endsAt": null
                  },
                  "startsAt": 1656509439,
                  "endsAt": null,
                  "createdAt": 1656502343
                }
        """
    }
}
