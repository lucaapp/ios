import XCTest
@testable import Luca
import DependencyInjection

class PaymentFlowViewModelTests: XCTestCase {

    var viewModel: PaymentFlowViewModel!

    override func setUpWithError() throws {
        DependencyContext[\.keyValueRepo] = SharedProvider(value: RealmKeyValueRepo(key: nil, filenameSalt: ""))
        DependencyContext[\.lucaPayment] = SharedProvider(value: LucaPaymentService())
        viewModel = PaymentFlowViewModel(paymentLocationData: nil)
        viewModel.amountInCentsString = Money(double: 700, currency: EUR()).formattedAmountWithoutCode
    }

    func testFormattedTipIsCorrect() {
        let formattedTipPercentages = viewModel.tipPercentages.map { $0.formattedPercentage }
        let formattedTipAmounts = viewModel.tipPercentages.map { viewModel.formatTip(tipPercent: $0) }

        XCTAssertEqual(formattedTipPercentages, ["0 %", "10 %", "15 %", "20 %"])
        XCTAssertEqual(formattedTipAmounts, ["0,00 €", "0,70 €", "1,05 €", "1,40 €"])
    }

    func testSetTipEqualsCorrectAmountWithTip() {
        viewModel.tipSelected = 0.15
        XCTAssertEqual(viewModel.amountToBePaidWithTip, Money(double: 8.05, currency: EUR()).rounded)
        XCTAssertEqual(viewModel.formattedAmountWithTip, "8,05 €")
    }

    func testUpdatingAmountInCentsSetsCorrectAmount() {
        viewModel.amountInCentsString = "700102,00"
        viewModel.tipSelected = 0.0
        XCTAssertEqual(viewModel.enteredAmount, Money(double: 7001.02, currency: EUR()))
        XCTAssertEqual(viewModel.formattedAmountWithTip, "7.001,02 €")
        // 7.001,02 €
        // 7.001,02 €
    }

    func testUpdatingAmountInCentsShowsNoticeCorrectly() {
        viewModel.minimumAmount = Money(double: 0.5, currency: EUR())
        viewModel.maximumAmount = Money(double: 50, currency: EUR())
        viewModel.amountInCentsString = "5"
        XCTAssertTrue(viewModel.isContinueButtonDisabled)

        viewModel.amountInCentsString = "500"
        XCTAssertNil(viewModel.notice)
    }

}
