import XCTest
import RxSwift
import DependencyInjection
@testable import Luca

// Scenarios:
// 1. Retrieve multiple params multiple times and make sure that source is called only once per source
// 2. Retrieve multiple params and invalidate cache and retrieve once again and make sure the source is called twice
// 3.:
//   - retrieve param A
//   - advance time by the half of the cache validity
//   - retrieve param b
//   - advance time by the second half of the cache validity
//   - retrieve param A and retrieve param B
//   - source for the param A should be called twice
//   - source for the param B should be called once
// 4. Retrieve param a and param b, invalidate the first one, retrieve once again both. Source for A should be called twice, source for B should be called once
// 5. Retrieve param A and param B, invalidate all cache, retrieve both again, both sources should be called twice
// 6. No data source - throws an error on all methods
// 7. Data racing with concurrent dispatch queue and no data racing with serial dispatch queue

class ParametrisedCachedDataSourceTests: XCTestCase {

    var dataSource: MockDataSource!
    var cache: ParametrisedCachedDataSource<String, String>!
    var timeProvider: MockedTimeProvider!

    override func setUpWithError() throws {
        timeProvider = MockedTimeProvider()
        DependencyContext[\.keyValueRepo] = SharedProvider(value: KeyValueRepoWithUnderlying(underlying: MemoryDataRepo<KeyValueRepoEntry>()))
        DependencyContext[\.timeProvider] = SharedProvider(value: timeProvider)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_scenario1A() throws {
        try testMultipleRetrieval(with: ["TEST"])
    }

    func test_scenario1B() throws {
        try testMultipleRetrieval(with: ["TEST", "test ljdskf", "lkasjdlaksjd s"])
    }

    func test_scenario2() throws {
        try testScenario2(firstParameter: "FIRST", secondParameter: "SECOND")
    }

    func test_scenario3() throws {
        timeProvider.valueToEmit = Calendar.current.date(from: DateComponents(year: 2021, month: 12, day: 15, hour: 12))!
        var dataSources: [String: MockDataSource] = [:]
        prepareCache(.until(unit: .hour, count: 2), dataSourceFactory: {
            return dataSources[$0]!
        })
        let param = "A"
        let otherParam = "B"

        dataSources[param] = MockDataSource()
        dataSources[param]!.valueToEmit = "First value"
        dataSources[otherParam] = MockDataSource()
        dataSources[otherParam]!.valueToEmit = "Second value"

        //   - retrieve param A
        var retrieved = try cache.retrieve(parameter: param).map { $0.first }.retrieveBlocking()
        XCTAssertEqual(retrieved, dataSources[param]!.valueToEmit)
        _ = try cache.retrieve(parameter: param).map { $0.first }.retrieveBlocking()

        //   - advance time by the half of the cache validity
        timeProvider.valueToEmit = Calendar.current.date(byAdding: .hour, value: 1, to: timeProvider.valueToEmit)!
        XCTAssertEqual(try cache.isCacheValid(parameter: param).retrieveBlocking(), true)
        XCTAssertEqual(try cache.isCacheValid(parameter: otherParam).retrieveBlocking(), false)

        //   - retrieve param b
        retrieved = try cache.retrieve(parameter: otherParam).map { $0.first }.retrieveBlocking()
        XCTAssertEqual(retrieved, dataSources[otherParam]!.valueToEmit)
        _ = try cache.retrieve(parameter: otherParam).map { $0.first }.retrieveBlocking()

        //   - advance time by the second half of the cache validity
        timeProvider.valueToEmit = Calendar.current.date(byAdding: .hour, value: 1, to: timeProvider.valueToEmit)!
        timeProvider.valueToEmit = Calendar.current.date(byAdding: .minute, value: 1, to: timeProvider.valueToEmit)!
        XCTAssertEqual(try cache.isCacheValid(parameter: param).retrieveBlocking(), false)
        XCTAssertEqual(try cache.isCacheValid(parameter: otherParam).retrieveBlocking(), true)

        //   - retrieve param A and retrieve param B
        _ = try cache.retrieve(parameter: param).retrieveBlocking()
        _ = try cache.retrieve(parameter: otherParam).retrieveBlocking()
        XCTAssertEqual(dataSources[param]!.callCounter, 2)
        XCTAssertEqual(dataSources[otherParam]!.callCounter, 1)
    }

    func test_scenario4() async throws {
        timeProvider.valueToEmit = Calendar.current.date(from: DateComponents(year: 2021, month: 12, day: 15, hour: 12))!
        var dataSources: [String: MockDataSource] = [:]
        prepareCache(.until(unit: .hour, count: 2), dataSourceFactory: {
            return dataSources[$0]!
        })
        let param = "A"
        let otherParam = "B"

        dataSources[param] = MockDataSource()
        dataSources[param]!.valueToEmit = "First value"
        dataSources[otherParam] = MockDataSource()
        dataSources[otherParam]!.valueToEmit = "Second value"

        //   - retrieve param A
        var retrieved = try cache.retrieve(parameter: param).map { $0.first }.retrieveBlocking()
        XCTAssertEqual(retrieved, dataSources[param]!.valueToEmit)
        _ = try cache.retrieve(parameter: param).map { $0.first }.retrieveBlocking()
        // Check if cache validity is correct
        XCTAssertEqual(try await cache.isCacheValid(parameter: param).task(), to: true)
        XCTAssertEqual(try await cache.isCacheValid(parameter: otherParam).task(), to: false)

        //   - retrieve param b
        retrieved = try cache.retrieve(parameter: otherParam).map { $0.first }.retrieveBlocking()
        XCTAssertEqual(retrieved, dataSources[otherParam]!.valueToEmit)
        _ = try cache.retrieve(parameter: otherParam).map { $0.first }.retrieveBlocking()

        // Check if cache validity is correct
        XCTAssertEqual(try await cache.isCacheValid(parameter: param).task(), to: true)
        XCTAssertEqual(try await cache.isCacheValid(parameter: otherParam).task(), to: true)

        // Invalidate the second one
        try cache.invalidateCache(parameter: otherParam).blockingWait()

        // Check if cache validity is correct
        XCTAssertEqual(try await cache.isCacheValid(parameter: param).task(), to: true)
        XCTAssertEqual(try await cache.isCacheValid(parameter: otherParam).task(), to: false)

        // Retrieve a and b
        _ = try cache.retrieve(parameter: param).retrieveBlocking()
        _ = try cache.retrieve(parameter: otherParam).retrieveBlocking()

        XCTAssertEqual(dataSources[param]!.callCounter, 1)
        XCTAssertEqual(dataSources[otherParam]!.callCounter, 2)
    }

    func test_scenario5() throws {
        timeProvider.valueToEmit = Calendar.current.date(from: DateComponents(year: 2021, month: 12, day: 15, hour: 12))!
        var dataSources: [String: MockDataSource] = [:]
        prepareCache(.until(unit: .hour, count: 2), dataSourceFactory: {
            return dataSources[$0]!
        })
        let param = "A"
        let otherParam = "B"

        dataSources[param] = MockDataSource()
        dataSources[param]!.valueToEmit = "First value"
        dataSources[otherParam] = MockDataSource()
        dataSources[otherParam]!.valueToEmit = "Second value"

        //   - retrieve param A
        var retrieved = try cache.retrieve(parameter: param).map { $0.first }.retrieveBlocking()
        XCTAssertEqual(retrieved, dataSources[param]!.valueToEmit)
        _ = try cache.retrieve(parameter: param).map { $0.first }.retrieveBlocking()

        // Check if cache validity is correct
        XCTAssertEqual(try cache.isCacheValid(parameter: param).retrieveBlocking(), true)
        XCTAssertEqual(try cache.isCacheValid(parameter: otherParam).retrieveBlocking(), false)

        //   - retrieve param b
        retrieved = try cache.retrieve(parameter: otherParam).map { $0.first }.retrieveBlocking()
        XCTAssertEqual(retrieved, dataSources[otherParam]!.valueToEmit)
        _ = try cache.retrieve(parameter: otherParam).map { $0.first }.retrieveBlocking()

        // Check if cache validity is correct
        XCTAssertEqual(try cache.isCacheValid(parameter: param).retrieveBlocking(), true)
        XCTAssertEqual(try cache.isCacheValid(parameter: otherParam).retrieveBlocking(), true)

        // Invalidate all caches
        try cache.invalidateCache().blockingWait()

        // Check if cache validity is correct
        XCTAssertEqual(try cache.isCacheValid(parameter: param).retrieveBlocking(), false)
        XCTAssertEqual(try cache.isCacheValid(parameter: otherParam).retrieveBlocking(), false)

        // Retrieve a and b
        _ = try cache.retrieve(parameter: param).retrieveBlocking()
        _ = try cache.retrieve(parameter: otherParam).retrieveBlocking()

        XCTAssertEqual(dataSources[param]!.callCounter, 2)
        XCTAssertEqual(dataSources[otherParam]!.callCounter, 2)
    }

    func test_scenario6() throws {
        prepareCache(.none, dataSourceFactory: { _ in nil })
        XCTAssertThrowsError(try cache.isCacheValid(parameter: "").retrieveBlocking())
        XCTAssertThrowsError(try cache.retrieve(parameter: "").retrieveBlocking())
        XCTAssertThrowsError(try cache.invalidateCache(parameter: "").blockingWait())
        XCTAssertThrowsError(try cache.invalidateCache().blockingWait())
    }

    func test_scenario7() throws {
        let source = MockDataSource()

        // Simulate a network request that can last for ~500ms
        source.delay = 0.05
        prepareCache(.onceInRuntime, dataSourceFactory: { _ in source })

        // Retrieve simultanously the same value for the same paramenter
        _ = try Observable.merge(
            cache.retrieve(parameter: "asd").asObservable(),
            cache.retrieve(parameter: "asd").asObservable()
        )
            .toArray().retrieveBlocking()

        // Call counter should be called once although the source has been accessed at the same time
        XCTAssertEqual(source.callCounter, 1)
    }

    private func testScenario2(firstParameter: String, secondParameter: String) throws {
        var dataSources: [String: MockDataSource] = [:]
        prepareCache(.onceInRuntime, dataSourceFactory: {
            return dataSources[$0]!
        })
        let param = firstParameter
        let otherParam = secondParameter

        dataSources[param] = MockDataSource()
        dataSources[param]!.valueToEmit = firstParameter
        dataSources[otherParam] = MockDataSource()
        dataSources[otherParam]!.valueToEmit = secondParameter

        // test the current param
        var retrieved = try cache.retrieve(parameter: param).map { $0.first }.retrieveBlocking()
        XCTAssertEqual(retrieved, param)

        retrieved = try cache.retrieve(parameter: param).map { $0.first }.retrieveBlocking()
        XCTAssertEqual(retrieved, param)

        try cache.invalidateCache(parameter: param).blockingWait()

        retrieved = try cache.retrieve(parameter: param).map { $0.first }.retrieveBlocking()
        XCTAssertEqual(retrieved, param)

        retrieved = try cache.retrieve(parameter: param).map { $0.first }.retrieveBlocking()
        XCTAssertEqual(retrieved, param)

        XCTAssertEqual(dataSources[param]!.callCounter, 2)

        // test the other one
        _ = try cache.retrieve(parameter: otherParam).map { $0.first }.retrieveBlocking()
        _ = try cache.retrieve(parameter: otherParam).map { $0.first }.retrieveBlocking()
        _ = try cache.retrieve(parameter: otherParam).map { $0.first }.retrieveBlocking()

        XCTAssertEqual(dataSources[otherParam]!.callCounter, 1)
    }

    private func testMultipleRetrieval(with params: [String]) throws {
        var dataSources: [String: MockDataSource] = [:]
        prepareCache(.onceInRuntime, dataSourceFactory: {
            if dataSources[$0] == nil {
                dataSources[$0] = MockDataSource()
            }
            return dataSources[$0]
        })
        for key in params {
            let value = "test value" + key
            dataSources[key] = MockDataSource()
            dataSources[key]!.valueToEmit = value

            var retrieved = try cache.retrieve(parameter: key).map { $0.first }.retrieveBlocking()
            XCTAssertEqual(retrieved, value)
            retrieved = try cache.retrieve(parameter: key).map { $0.first }.retrieveBlocking()
            XCTAssertEqual(retrieved, value)
            retrieved = try cache.retrieve(parameter: key).map { $0.first }.retrieveBlocking()
            XCTAssertEqual(retrieved, value)

            XCTAssertEqual(dataSources[key]!.callCounter, 1)
        }
    }

    private func prepareCache(_ cacheValidity: CacheValidity, dataSourceFactory: @escaping (String) -> MockDataSource?) {
        cache = BaseParametrisedCachedDataSource(
            dataSourceFactory: { dataSourceFactory($0) },
            cacheDataRepoFactory: { KeyValueRepoCacheWrapper(uniqueCacheKey: "cacheTests.\($0)") },
            cacheValidity: cacheValidity,
            uniqueCacheIdentifier: "cacheTestsCache"
       )
    }

}
