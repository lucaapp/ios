import RxTest
import RxSwift
import XCTest
import DependencyInjection
@testable import Luca

class DocumentValidityValidatorTests: XCTestCase {

    var scheduler: TestScheduler!
    var validator = DocumentValidityValidator()

    override func setUpWithError() throws {
        scheduler = TestScheduler(initialClock: 0)

        DependencyContext[\.timeProvider] = SharedProvider(value: DefaultTimeProvider())
    }

    func test_validDocument() {
        let expiredDate = Calendar.current.date(byAdding: .month, value: -2, to: Date.now)
        let expiredVaccination = MockedVaccination(issuedAt: expiredDate)

        let validation = scheduler.createObserver(Never.self)
        _ = validator.validate(document: expiredVaccination)
            .subscribe(on: scheduler)
            .observe(on: scheduler)
            .asObservable()
            .subscribe(validation)

        scheduler.start()

        XCTAssertTrue(expiredVaccination.isValid)
        XCTAssertTrue(expiredVaccination.isRecent)
        XCTAssertFalse(expiredVaccination.needsRemoval)
        XCTAssertEqual(validation.events, [.completed(2)])
    }

    func test_expiredDocument_notRemoved() {
        let expiredDate = Calendar.current.date(byAdding: .month, value: -10, to: Date.now)
        let expiredVaccination = MockedVaccination(issuedAt: expiredDate)

        let validation = scheduler.createObserver(Never.self)
        _ = validator.validate(document: expiredVaccination)
            .subscribe(on: scheduler)
            .observe(on: scheduler)
            .asObservable()
            .subscribe(validation)

        scheduler.start()

        XCTAssertFalse(expiredVaccination.isValid)
        XCTAssertFalse(expiredVaccination.isRecent)
        XCTAssertFalse(expiredVaccination.needsRemoval)
        XCTAssertEqual(validation.events, [.completed(2)])
    }

    func test_expiredDocument_invalid_notRemoved() {
        let expiredDate = Calendar.current.date(byAdding: .month, value: -13, to: Date.now)
        let expiredVaccination = MockedVaccination(issuedAt: expiredDate)

        let validation = scheduler.createObserver(Never.self)
        _ = validator.validate(document: expiredVaccination)
            .subscribe(on: scheduler)
            .observe(on: scheduler)
            .asObservable()
            .subscribe(validation)

        scheduler.start()

        XCTAssertFalse(expiredVaccination.isValid)
        XCTAssertFalse(expiredVaccination.needsRemoval)
        XCTAssertEqual(validation.events, [.completed(2)])
    }

    func test_validate_withValidTest_shouldReturnError() throws {
        let mockedDocument = MockedCoronaTest(issuedAt: nil)
        let validation = scheduler.createObserver(Never.self)

        _ = validator.validate(document: mockedDocument)
            .subscribe(on: scheduler)
            .observe(on: scheduler)
            .asObservable()
            .subscribe(validation)
        scheduler.start()

        XCTAssertEqual(validation.events, [.completed(2)])
    }

    func test_validate_withExpiredTest_shouldReturnError() throws {
        let mockedDocument = MockedCoronaTest(issuedAt: Date.distantPast)
        let validation = scheduler.createObserver(Never.self)

        _ = validator.validate(document: mockedDocument)
            .subscribe(on: scheduler)
            .observe(on: scheduler)
            .asObservable()
            .subscribe(validation)
        scheduler.start()

        XCTAssertFalse(mockedDocument.isValid)
        XCTAssertFalse(mockedDocument.isRecent)
        XCTAssertEqual(validation.events, [.error(2, CoronaTestProcessingError.expired)])
    }
}
