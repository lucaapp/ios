import XCTest
import RxSwift
@testable import Luca
import DependencyInjection

class DocumentFactoryTests: XCTestCase {

    var factory: MockedDocumentFactory!
    fileprivate var validParser: MockedDocumentParser!
    fileprivate var invalidParser: MockedDocumentParser!
    @InjectStatic(\.documentsPreferences) private var documentsPreferences

    override func setUpWithError() throws {
        DependencyContext[\.keyValueRepo] = SharedProvider(value: KeyValueRepoWithUnderlying(underlying: MemoryDataRepo<KeyValueRepoEntry>()))
        DependencyContext[\.documentsPreferences] = SharedProvider(value: DocumentsPreferences(propertySource: DocumentProperties()))
        validParser = MockedDocumentParser()
        validParser.shouldFail = false
        invalidParser = FailingDocumentParser()
        factory = MockedDocumentFactory()
        factory.register(parser: invalidParser)
        factory.register(parser: validParser)
    }

    func test_shouldCallFirstParserOnlyOnce() throws {
        let document = try factory.createDocument(from: "asd").retrieveBlocking()
        let payload = DocumentPayload.init(originalCode: document.originalCode)
        _ = try factory.createDocument(from: payload).retrieveBlocking()

        XCTAssertEqual(invalidParser.counter, 1)
        XCTAssertEqual(validParser.counter, 2)
        XCTAssertEqual(factory.createDocumentFromPayloadCounter, 1)
        XCTAssertEqual(factory.createDocumentFromCodeCounter, 1)
    }

    func test_shouldCallFirstParserTwice_parserCacheDestroyed() throws {
        let document = try factory.createDocument(from: "dsa").retrieveBlocking()

        // Destroy cache
        try documentsPreferences.remove(\.parserCacheKey, for: document).blockingWait()

        let payload = DocumentPayload(originalCode: document.originalCode)
        _ = try factory.createDocument(from: payload).retrieveBlocking()

        XCTAssertEqual(invalidParser.counter, 2)
        XCTAssertEqual(validParser.counter, 2)
        XCTAssertEqual(factory.createDocumentFromPayloadCounter, 1)
        XCTAssertEqual(factory.createDocumentFromCodeCounter, 2)
    }

}

class MockedDocumentFactory: DocumentFactory {
    var createDocumentFromCodeCounter = Int(0)
    var createDocumentFromPayloadCounter = Int(0)
    override func createDocument(from code: String) -> Single<Document> {
        Completable.from { self.createDocumentFromCodeCounter += 1 }
        .andThen(super.createDocument(from: code))
    }

    override func createDocument(from payload: DocumentPayload) -> Single<Document> {
        Completable.from { self.createDocumentFromPayloadCounter += 1 }
        .andThen(super.createDocument(from: payload))
    }
}

private class MockedDocumentParser: DocumentParser {
    var shouldFail = false
    var counter = 0
    func parse(code: String) -> Single<Document> {
        counter += 1
        if shouldFail {
            return .error(NSError(domain: "", code: 0, userInfo: nil))
        }
        return .just(MockedDocument())
    }
}

// The class must have different name in order to differentiate between multiple parsers caches
private class FailingDocumentParser: MockedDocumentParser {
    override init() {
        super.init()
        shouldFail = true
    }
}

private class MockedDocument: Document {
    var identifier: Int = 0

    var originalCode: String = ""

    var hashSeed: String = ""

    var recentUntil: Date = Date()

    var expiresAt: Date = Date()

    var issuedAt: Date = Date()

    var contentDescription: String = ""

}
