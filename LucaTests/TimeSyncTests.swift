import Quick
import Nimble
import Cuckoo
import Foundation
import RxNimble
import RxSwift
import DependencyInjection

@testable import Luca

// Scenarios:
// 1. happy run with internet -> clock in sync, timestamps reseted, current timestamp saved
// 2. happy run with internet, then no internet for some time and then check -> clock in sync, timestamps saved
// 3. not happy run with internet -> clock not in sync, timestamp added to save the alteration
// 4. happy run with internet, then no internet for some time, clock alteration and then check -> clock not in sync, timestamp added to save the alteration

class TimeSyncTests: QuickSpec {
    // swiftlint:disable:next function_body_length
    override func spec() {
        describe("TimeSync") {
            var timeSync: TimeSyncService! = nil
            var backend = MockCommonBackendMisc()
            var keyValueRepo: KeyValueRepoProtocol!
            var timeProvider: MockedTimeProvider!
            let timestampArchiveKey = "timestampsArchive"

            beforeEach {
                backend = MockCommonBackendMisc()
                keyValueRepo = KeyValueRepoWithUnderlying(underlying: MemoryDataRepo<KeyValueRepoEntry>())
                timeProvider = MockedTimeProvider()

                DependencyContext[\.backendMiscV3] = SharedProvider(value: backend)
                DependencyContext[\.keyValueRepo] = SharedProvider(value: keyValueRepo)
                DependencyContext[\.timeProvider] = SharedProvider(value: timeProvider)

                timeSync = TimeSyncService()
            }

            context("Happy case") {
                it("With internet") {
                    let now = Date(timeIntervalSince1970: 1000)
                    timeProvider.valueToEmit = now
                    let request = MockAsyncDataOperation<BackendError<TimesyncError>, Timesync>()
                    stub(request) { m in
                        when(m.execute(completion: any(), failure: any())).then {
                            $0.0(Timesync(unix: Int(now.timeIntervalSince1970)))
                            return {}
                        }
                    }
                    stub(backend) { m in
                        when(m.fetchTimesync()).thenReturn(request)
                    }
                    expect(timeSync.isInSync()).array.to(equal([true]))
                    expect(keyValueRepo.loadOptional(timestampArchiveKey)).array.to(equal([[now]]))
                }
                it("With internet and then without") {
                    let now = Date(timeIntervalSince1970: 1000)
                    timeProvider.valueToEmit = now
                    let request = MockAsyncDataOperation<BackendError<TimesyncError>, Timesync>()
                    stub(request) { m in
                        when(m.execute(completion: any(), failure: any())).then {
                            $0.0(Timesync(unix: Int(now.timeIntervalSince1970)))
                            return {}
                        }.then {
                            // Force to fail every other request
                            $0.1(BackendError(networkLayerError: .noInternet))
                            return {}
                        }
                    }
                    stub(backend) { m in
                        when(m.fetchTimesync()).thenReturn(request)
                    }
                    expect(timeSync.isInSync()).array.to(equal([true]))

                    // Let the cache expire
                    timeProvider.valueToEmit = Calendar.current.date(byAdding: .hour, value: 1, to: now)!

                    // Force a new request which will fail (see the second stub) and will only check if the local clock is not earlier than the previous one
                    expect(timeSync.isInSync()).array.to(equal([true]))

                    // Let some more time pass by
                    timeProvider.valueToEmit = Calendar.current.date(byAdding: .hour, value: 1, to: timeProvider.valueToEmit)!

                    expect(timeSync.isInSync()).array.to(equal([true]))
                }
            }
            context("Negative case") {
                it("With internet - clock altered towards future") {
                    let now = Date(timeIntervalSince1970: 1000)
                    timeProvider.valueToEmit = Calendar.current.date(byAdding: .hour, value: 1, to: now)!
                    let request = MockAsyncDataOperation<BackendError<TimesyncError>, Timesync>()
                    stub(request) { m in
                        when(m.execute(completion: any(), failure: any())).then {
                            $0.0(Timesync(unix: Int(now.timeIntervalSince1970)))
                            return {}
                        }
                    }
                    stub(backend) { m in
                        when(m.fetchTimesync()).thenReturn(request)
                    }
                    expect(timeSync.isInSync()).array.to(equal([false]))
                    expect(keyValueRepo.loadOptional(timestampArchiveKey)).array.to(equal([[timeProvider.valueToEmit]]))
                }
                it("With internet - clock altered towards past") {
                    let now = Calendar.current.date(from: DateComponents(year: 2000))!
                    timeProvider.valueToEmit = Calendar.current.date(byAdding: .hour, value: -1, to: now)!
                    let request = MockAsyncDataOperation<BackendError<TimesyncError>, Timesync>()
                    stub(request) { m in
                        when(m.execute(completion: any(), failure: any())).then {
                            $0.0(Timesync(unix: Int(now.timeIntervalSince1970)))
                            return {}
                        }
                    }
                    stub(backend) { m in
                        when(m.fetchTimesync()).thenReturn(request)
                    }
                    expect(timeSync.isInSync()).array.to(equal([false]))
                    expect(keyValueRepo.loadOptional(timestampArchiveKey)).array.to(equal([[timeProvider.valueToEmit]]))
                }
                it("First request with internet, second without, third request altered") {
                    var expectedTimestamps = [Date]()
                    let now = Calendar.current.date(from: DateComponents(year: 2000))!
                    expectedTimestamps.append(now)

                    timeProvider.valueToEmit = now
                    let request = MockAsyncDataOperation<BackendError<TimesyncError>, Timesync>()
                    stub(request) { m in
                        when(m.execute(completion: any(), failure: any())).then {
                            $0.0(Timesync(unix: Int(now.timeIntervalSince1970)))
                            return {}
                        }.then {
                            // Force to fail every other request
                            $0.1(BackendError(networkLayerError: .noInternet))
                            return {}
                        }
                    }
                    stub(backend) { m in
                        when(m.fetchTimesync()).thenReturn(request)
                    }
                    expect(timeSync.isInSync()).array.to(equal([true]))

                    // Let cache expire
                    timeProvider.valueToEmit = Calendar.current.date(byAdding: .hour, value: 1, to: timeProvider.valueToEmit)!
                    expectedTimestamps.append(timeProvider.valueToEmit)

                    expect(timeSync.isInSync()).array.to(equal([true]))

                    // Alter the clock
                    timeProvider.valueToEmit = Calendar.current.date(byAdding: .hour, value: -2, to: timeProvider.valueToEmit)!
                    expectedTimestamps.append(timeProvider.valueToEmit)

                    expect(timeSync.isInSync()).array.to(equal([false]))

                    expect(keyValueRepo.loadOptional(timestampArchiveKey)).array.to(equal([expectedTimestamps]))
                }
            }
        }
    }
}
