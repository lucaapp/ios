import XCTest
@testable import Luca

class AccessedDataChunkTests: XCTestCase {
    func testValidChunk() throws {
        let chunk = try AccessedTracesDataChunk(data: Data(hex: "01000c0000017b63b5409c0000000000207dd5f8f746b44813397b75bdd571a8f7b2179f416249999580fa50"))

        XCTAssertEqual(chunk.schemaVersion, 1)
        XCTAssertEqual(chunk.algorithm, 0)
        XCTAssertEqual(chunk.hashLength, 12)
        XCTAssertEqual(chunk.createdAt, Date(timeIntervalSince1970: TimeInterval(1629465428124) / 1000.0))
        XCTAssertEqual(chunk.alignmentBytes, [0, 0, 0, 0, 0])
        XCTAssertEqual(chunk.previousHash, Data(hex: "207dd5f8f746b44813397b75bdd571a8"))
        XCTAssertEqual(chunk.hashes, [Data(hex: "f7b2179f416249999580fa50")])
    }

    func testInvalidSize() {
        let randomBytes = KeyFactory.randomBytes(size: 31)!
        do {
            _ = try AccessedTracesDataChunk(data: randomBytes)
        } catch let error {
            XCTAssertEqual(error as? AccessedTracesDataChunkError, .dataChunkInvalidSize)
            return
        }
        XCTFail("No errors thrown")
    }

    func testInvalidSchemaVersion() {
        do {
            _ = try AccessedTracesDataChunk(data: Data(hex: "02000c0000017b63b5409c0000000000207dd5f8f746b44813397b75bdd571a8f7b2179f416249999580fa50"))
        } catch let error {
            XCTAssertEqual(error as? AccessedTracesDataChunkError, .dataChunkInvalidSchemaVersion)
            return
        }
        XCTFail("No errors thrown")
    }

    func testInvalidAlgorithm() {
        do {
            _ = try AccessedTracesDataChunk(data: Data(hex: "01010c0000017b63b5409c0000000000207dd5f8f746b44813397b75bdd571a8f7b2179f416249999580fa50"))
        } catch let error {
            XCTAssertEqual(error as? AccessedTracesDataChunkError, .dataChunkInvalidAlgorithm)
            return
        }
        XCTFail("No errors thrown")
    }

    func testBufferSize() {
        do {
            _ = try AccessedTracesDataChunk(data: Data(hex: "01000c0000017b63b5409c0000000000207dd5f8f746b44813397b75bdd571a8f7b2179f416249999580fa50aa"))
        } catch let error {
            XCTAssertEqual(error as? AccessedTracesDataChunkError, .dataChunkHashesBufferIncompatibleWithDeclaredHashLength)
            return
        }
        XCTFail("No errors thrown")
    }
}
