import XCTest
@testable import Luca

class RandomNumberGeneratorTest: XCTestCase {

    var rng: RandomNumberGenerator!
    let bufferSize: UInt = 32
    override func setUp() {
        rng = StandardRandomNumberGenerator()
    }

    func test_randomBytesSize_0() throws {
        let data = try rng.generate(bytesCount: 0)
        XCTAssertEqual(data.count, 0)
    }

    func test_randomBytesSizeLargerThan_0() throws {
        let data = try rng.generate(bytesCount: bufferSize)
        XCTAssertEqual(data.count, Int(bufferSize))
    }

    func test_randomBytesNTimesUnique() throws {
        let N = 50
        let randomData = Set(try (0..<N).map { _ in try rng.generate(bytesCount: bufferSize) })
        XCTAssertEqual(randomData.count, N)
        for data in randomData {
            XCTAssertEqual(data.count, Int(bufferSize))
        }

    }
}
