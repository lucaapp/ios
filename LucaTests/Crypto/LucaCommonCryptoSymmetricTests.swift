import XCTest
import DependencyInjection
import CryptoSwift
@testable import Luca

class LucaCommonCryptoSymmetricTests: XCTestCase {

    var secret = Data(hexString: [String](repeating: "ca55e77e", count: 8).joined())!
    var label = "TEST_SYM_ENC".data(using: .utf8)!
    var plain = "Reality is frequently inaccurate.".data(using: .utf8)!
    var aad = ["aad1".data(using: .utf8)!, "aad2".data(using: .utf8)!]
    var expectedSalt = Data(hexString: [String](repeating: "ff", count: 8).joined())!
    var expectedTag = Data(hexString: "b2f7ecbdb744d1c0a34e96399146e8bc")!
    var expectedCipher = Data(hexString: "a748624c6e6bd9c564cb9c64988d47a2158680650d81d122879d2e4dd981c98e7b")!

    @InjectStatic(\.commonCrypto) private var crypto

    override func setUpWithError() throws {
        DependencyContext[\.randomNumberGenerator] = SharedProvider(value: MockedRandomNumberGenerator())
        DependencyContext[\.commonCrypto] = SharedProvider(value: LucaCommonCrypto())
    }

    func test_encryptionDecryption_invalidSecret() throws {

        // Empty secret
        XCTAssertThrowsError(try crypto.symmetricEncrypt(
                secret: ValueRawKeySource(key: Data()),
                label: label,
                data: plain,
                associatedData: aad
            )
        ) { XCTAssertEqual($0 as? LucaCommonCrypto.ErrorType, LucaCommonCrypto.ErrorType.invalidKeyLength)}

        XCTAssertThrowsError(try crypto.symmetricDecrypt(
                secret: ValueRawKeySource(key: Data()),
                label: label,
                encryptedData: expectedCipher,
                associatedData: aad
            )
        ) { XCTAssertEqual($0 as? LucaCommonCrypto.ErrorType, LucaCommonCrypto.ErrorType.invalidKeyLength)}

        // only 8 byte secret
        let tempSecret = Data(hexString: [String](repeating: "ff", count: 8).joined())!
        XCTAssertThrowsError(try crypto.symmetricEncrypt(
                secret: ValueRawKeySource(key: tempSecret),
                label: label,
                data: plain,
                associatedData: aad
            )
        ) { XCTAssertEqual($0 as? LucaCommonCrypto.ErrorType, LucaCommonCrypto.ErrorType.invalidKeyLength)}

        XCTAssertThrowsError(try crypto.symmetricDecrypt(
                secret: ValueRawKeySource(key: tempSecret),
                label: label,
                encryptedData: expectedCipher,
                associatedData: aad
            )
        ) { XCTAssertEqual($0 as? LucaCommonCrypto.ErrorType, LucaCommonCrypto.ErrorType.invalidKeyLength)}
    }

    func test_encryptionDecryption_emptyData() throws {
        let encrypted = try crypto.symmetricEncrypt(
            secret: ValueRawKeySource(key: secret),
            label: Data(),
            data: Data(),
            associatedData: []
        )

        XCTAssertEqual(encrypted.count, 24)
        XCTAssertEqual(encrypted, Data(hex: "fffffffffffffffffd183623a65855aac741b1de2469e98e"))

        let decrypted = try crypto.symmetricDecrypt(
            secret: ValueRawKeySource(key: secret),
            label: Data(),
            encryptedData: encrypted,
            associatedData: []
        )
        XCTAssertEqual(decrypted, Data())
    }

    func test_encryptionDecryption_success() throws {
        let encrypted = try crypto.symmetricEncrypt(
            secret: ValueRawKeySource(key: secret),
            label: label,
            data: plain,
            associatedData: aad
        )

        var tempEncrypted = encrypted
        XCTAssertEqual(encrypted.count, 57)

        let salt = tempEncrypted.prefix(8)
        tempEncrypted.removeFirst(8)
        let tag = tempEncrypted.suffix(16)
        tempEncrypted.removeLast(16)
        let ct = tempEncrypted

        XCTAssertEqual(salt, expectedSalt)
        XCTAssertEqual(ct, expectedCipher)
        XCTAssertEqual(tag, expectedTag)

        let decrypted = try crypto.symmetricDecrypt(
            secret: ValueRawKeySource(key: secret),
            label: label,
            encryptedData: encrypted,
            associatedData: aad
        )
        XCTAssertEqual(decrypted, plain)
    }

}

class MockedRandomNumberGenerator: RandomNumberGenerator {
    var fillWith: UInt8 = 255
    func generate(bytesCount: UInt) throws -> Data {
        return Data([UInt8](repeating: fillWith, count: Int(bytesCount)))
    }
}
