import XCTest
import DependencyInjection
@testable import Luca

class LucaCommonCryptoMiscTests: XCTestCase {

    func test_basicArgon2id() throws {
        let data = Data(hex: "badf00dbadcafe")
        let salt = "e2ca02296910dedd".data(using: .utf8)!
        let crypto = LucaCommonCrypto()
        let argonResult = try crypto.argon2id(data: data, salt: salt, length: 16)
        XCTAssertEqual(argonResult, Data(hex: "77f84ac8bb5ea7cde72f983bce000987"))
    }

    func test_deriveSecretsFromTan() throws {
        let data = Data(hex: "badf00dbadcafe")
        let crypto = LucaCommonCrypto()
        let secrets = try crypto.deriveSecret(from: data)
        XCTAssertEqual(secrets.uuid.uuidString.lowercased(), "b35bca06-abed-487a-b839-9b559c67fbde")
        XCTAssertEqual(secrets.derivedKey, Data(hex: "2b5f53aac2fa2d3f0840a6a9eeeb4b6a"))
    }

    func test_failToDeriveSecretsFromTan() throws {
        let data = Data(hex: "badf00dbadcafefe")
        let crypto = LucaCommonCrypto()
        XCTAssertThrowsError(try crypto.deriveSecret(from: data)) { XCTAssertEqual($0 as? LucaCommonCrypto.ErrorType, .invalidTanSize) }
    }

}
