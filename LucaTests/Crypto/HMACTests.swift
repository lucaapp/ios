import XCTest
import DependencyInjection
@testable import Luca

class HMACTests: XCTestCase {

    let messageId = Data(base64Encoded: "N6hX4StG7saImLxKjOpccg==")!
    let iv = Data(base64Encoded: "mghdUvU2eVfU6Rir+GdLbw==")!
    let encryptedData = Data(base64Encoded: "8LNf8I6c3nKpaFasClQ+XhEwPRenizsOYBEKxuwe7gjsE0Av4aU0hVbDAErS")!
    let validHMAC = Data(base64Encoded: "PC830VUEBT4qEuBr3p0CRWu74wPCPY3QQ4STwyCpkHg=")!

    let authKey: [UInt8] =  [91, 9, 224, 166, 173, 42, 187, 102, 22, 111, 112, 252, 207, 78, 217, 162]

    var hmac: HMACSHA256!

    override func setUp() {
        hmac = HMACSHA256(key: Data(authKey))
    }

    func test_HMACVerification_success() throws {
        try hmac.verify(message: messageId + encryptedData + iv, receivedHMAC: validHMAC)
    }

    func test_HMACVerification_wrongMessage_fail() throws {
        XCTAssertThrowsError(try hmac.verify(message: messageId + iv + encryptedData, receivedHMAC: validHMAC))
    }
}
