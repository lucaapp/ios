import XCTest
import DependencyInjection
import CryptoSwift
import SwiftDGC
@testable import Luca

class LucaCommonCryptoAsymmetricTests: XCTestCase {

    var keyPair: GeneratedKeyPair! = nil
    var label = "TEST_ASYM_ENC".data(using: .utf8)!
    var plain = "Time is an illusion. Lunchtime doubly so.".data(using: .utf8)!
    var aad = ["aad1".data(using: .utf8)!, "aad2".data(using: .utf8)!]
    var expectedSalt = Data(hexString: [String](repeating: "ff", count: 8).joined())!
    var expectedTag = Data(hexString: "84a90eff69bdbce9d98985db4e90562c")!
    var expectedCipher = Data(hexString: "e8337d34aa3ef50e5b7d52251e1cec0cbf5af6a98c9409bbd512c0d7f8b556afa32d577d48dfaf3679")!
    var expectedEphPub = Data(hexString: "026780c5fc70275e2c7061a0e7877bb174deadeb9887027f3fa83654158ba7f50c")!

    @InjectStatic(\.commonCrypto) private var crypto

    override func setUpWithError() throws {
        let keyGen = MockedKeyGen()
        keyGen.privateKey = try KeyFactory.createPrivateEC(
            x: Data(hex: "6780c5fc70275e2c7061a0e7877bb174deadeb9887027f3fa83654158ba7f50c"),
            y: Data(hex: "3cba8c34bc35d20e81f730ac1c7bd6d661a942f90c6a9ca55c512f9e4a001266"),
            d: Data(hex: "000000000000000000000000000000000000000000000000000000000000002a")
        )
        keyPair = try keyGen.generateKeyPair()
        DependencyContext[\.randomNumberGenerator] = SharedProvider(value: MockedRandomNumberGenerator())
        DependencyContext[\.keyGen] = SharedProvider(value: keyGen)
        DependencyContext[\.commonCrypto] = SharedProvider(value: LucaCommonCrypto())
    }

    func test_encryptionDecryptionWithPepper_success() throws {
        let pepper = Data(hex: "aabbccdd")
        expectedTag = Data(hex: "e43b059fd52835aec54a56921b161f20")
        expectedCipher = Data(hex: "c5bb2e1e6296a08cf8dfc7efd8220589319eb3d5c1a5f6a139efd48188805e3f46fe21c0769f7ba621")

        let encrypted = try crypto.asymmetricEncrypt(
            publicKey: ValueKeySource(key: keyPair.publicKey),
            label: label,
            data: plain,
            associatedData: aad,
            pepper: pepper
        )
        XCTAssertEqual(encrypted.count, 90)

        let ephPub = Data(encrypted.bytes.prefix(33))
        let ct = Data(encrypted.bytes[33..<encrypted.count - 16])
        let tag = Data(encrypted.bytes.suffix(16))

        XCTAssertEqual(ephPub, expectedEphPub)
        XCTAssertEqual(tag, expectedTag)
        XCTAssertEqual(ct, expectedCipher)
    }

    func test_encryptionDecryption_success() throws {
        let encrypted = try crypto.asymmetricEncrypt(
            publicKey: ValueKeySource(key: keyPair.publicKey),
            label: label,
            data: plain,
            associatedData: aad,
            pepper: nil
        )
        XCTAssertEqual(encrypted.count, 90)

        let ephPub = Data(encrypted.bytes.prefix(33))
        let ct = Data(encrypted.bytes[33..<encrypted.count - 16])
        let tag = Data(encrypted.bytes.suffix(16))

        XCTAssertEqual(ephPub, expectedEphPub)
        XCTAssertEqual(tag, expectedTag)
        XCTAssertEqual(ct, expectedCipher)
    }
}

class MockedKeyGen: KeyGen {

    var privateKey: SecKey!
    func generateKeyPair() throws -> GeneratedKeyPair {
        guard let publicKey = KeyFactory.derivePublic(from: privateKey) else {
            throw NSError(domain: "Couldn't derive public key", code: 0, userInfo: nil)
        }
        return GeneratedKeyPair(privateKey: privateKey, publicKey: publicKey)
    }
}
