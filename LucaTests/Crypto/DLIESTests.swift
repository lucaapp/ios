import XCTest
import DependencyInjection
@testable import Luca

class DLIESTests: XCTestCase {

    let keyPairPublicKeyHex = "04ef1f3aec2068ddf3b80f678d243b3d15bc626d285d9c75f4e577af7e441ce5f205fe9c8690b74b7276ff2394689005491de5988d2e8d9385bdd1e805b23e03da"
    let keyPairPrivateKeyHex = "c857528c67f0fed7846aa8bcd7eed7e8902d4b6f394107239e180598a0f42ad5"
    let receiverPublicKey = "04d67dccad8a42930d4f64e78842716c58a645c12eb852c33a63ae75796093c1d94f7c6746a5feeb7af7cf209912db398b497470fa2c40fec4fdce8a75e8a888d4"
    let iv = "a4b558b89e2557256271de4a21fa71be"

    let input = "000000"
    var output: DLIESEncryptedData!

    override func setUp() {
        output = DLIESEncryptedData(
            data: Data(hex: "4913b8").base64EncodedString(),
            publicKey: Data(hex: keyPairPublicKeyHex).base64EncodedString(),
            iv: Data(hex: iv).base64EncodedString(),
            mac: Data(hex: "dd8889a49a000963b0243665dc1af30370289641c32629ab57a6a10215dcc972").base64EncodedString()
        )
    }

    func test_DLIESEncrypt_sameOutput() throws {
        let dlies = DLIES(
            ephemeralKeyPair: try createKeyPair(publicHex: keyPairPublicKeyHex, privateHex: keyPairPublicKeyHex + keyPairPrivateKeyHex),
            receiverPublicKeySource: ValueKeySource(key: try publicKey(from: receiverPublicKey)),
            compressPublicKey: false,
            iv: Data(hex: iv)
        )
        let encryptedOutput = try dlies.encrypt(data: Data(hex: input))

        XCTAssertEqual(encryptedOutput.data, output.data)
        XCTAssertEqual(encryptedOutput.publicKey, output.publicKey)
        XCTAssertEqual(encryptedOutput.iv, output.iv)
        XCTAssertEqual(encryptedOutput.mac, output.mac)

    }

    private func publicKey(from: String) throws -> SecKey {
        try KeyFactory.create(from: Data(hex: from), type: .ecsecPrimeRandom, keyClass: .public)
    }

    private func createKeyPair(publicHex: String, privateHex: String) throws -> KeyPair<SecKey> {

        let privateKey = try KeyFactory.create(from: Data(hex: privateHex), type: .ecsecPrimeRandom, keyClass: .private)
        let publicKey = try KeyFactory.create(from: Data(hex: publicHex), type: .ecsecPrimeRandom, keyClass: .public)

        let derivedPublicKey = KeyFactory.derivePublic(from: privateKey)!
        let _: Data = try derivedPublicKey.toData()
        return KeyPair(public: publicKey, private: privateKey)
    }

}
