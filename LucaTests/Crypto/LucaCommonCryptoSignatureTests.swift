import XCTest
import DependencyInjection
@testable import Luca

class LucaCommonCryptoSignatureTests: XCTestCase {

    var keyPair: GeneratedKeyPair! = nil
    var plain = "Time is an illusion. Lunchtime doubly so.".data(using: .utf8)!

    @InjectStatic(\.commonCrypto) private var crypto

    override func setUpWithError() throws {
        let keyGen = MockedKeyGen()
        keyGen.privateKey = try KeyFactory.createPrivateEC(
            x: Data(hex: "6780c5fc70275e2c7061a0e7877bb174deadeb9887027f3fa83654158ba7f50c"),
            y: Data(hex: "3cba8c34bc35d20e81f730ac1c7bd6d661a942f90c6a9ca55c512f9e4a001266"),
            d: Data(hex: "000000000000000000000000000000000000000000000000000000000000002a")
        )
        keyPair = try keyGen.generateKeyPair()
        DependencyContext[\.randomNumberGenerator] = SharedProvider(value: MockedRandomNumberGenerator())
        DependencyContext[\.keyGen] = SharedProvider(value: keyGen)
        DependencyContext[\.commonCrypto] = SharedProvider(value: LucaCommonCrypto())
    }

    func test_signAndVerify() throws {
        let msg = [
            "For a moment, nothing happened.".data(using: .utf8)!,
            "Then, after a second or so, nothing continued to happen.".data(using: .utf8)!
        ]
        // It is needed as this algorithm was not stable in specific cases.
        for _ in 0..<500 {
            let signature = try crypto.sign(privateKey: ValueKeySource(key: keyPair.privateKey), data: msg)
            XCTAssertEqual(signature.count, 64)
            try crypto.verify(publicKey: ValueKeySource(key: keyPair.publicKey), signature: signature, data: msg)
        }
    }

    func test_signAndVerifyEmptyData() throws {
        let signature = try crypto.sign(privateKey: ValueKeySource(key: keyPair.privateKey), data: [])
        XCTAssertEqual(signature.count, 64)
        try crypto.verify(publicKey: ValueKeySource(key: keyPair.publicKey), signature: signature, data: [])
    }

    func test_signECDSA_sha256_failure() throws {
        let msg = [
            "For a moment, nothing happened.".data(using: .utf8)!
        ]

        let signature = try crypto.sign(privateKey: ValueKeySource(key: keyPair.privateKey), data: msg)

        XCTAssertEqual(signature.count, 64)

        // fails gracefully if message is wrong
        XCTAssertThrowsError(try crypto.verify(
            publicKey: ValueKeySource(key: keyPair.publicKey),
            signature: signature,
            data: ["Then, after a second or so, nothing continued to happen.".data(using: .utf8)!]
        )) { print($0); XCTAssertEqual($0 as? LucaCommonCrypto.ErrorType, .invalidSignature) }

        // fails gracefully if signature is wrong
        XCTAssertThrowsError(try crypto.verify(
            publicKey: ValueKeySource(key: keyPair.publicKey),
            signature: Data(hexString: [String](repeating: "ff", count: 32).joined())!,
            data: msg
        )) { XCTAssertEqual($0 as? LucaCommonCrypto.ErrorType, .invalidSignature) }
    }

}
