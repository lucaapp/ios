import XCTest
@testable import Luca

class ArrayDifferenceTests: XCTestCase {

    func testHashable() throws {

        let first = [0, 1, 2, 3]
        let second = [2, 3, 4, 5]

        XCTAssertEqual(
            Set(first.difference(from: second)),
            Set(second.difference(from: first))
        )
        XCTAssertEqual(
            Set(first.difference(from: second)),
            Set([0, 1, 4, 5])
        )
    }

    func testCustomChecker() {

        let first = [0, 1, 2, 3]
        let second = [2, 3, 4, 5]

        XCTAssertEqual(
            Set(first.difference(from: second, equalityChecker: { $0 == $1 })),
            Set(second.difference(from: first, equalityChecker: { $0 == $1 }))
        )

        XCTAssertEqual(
            Set(first.difference(from: second, equalityChecker: { $0 == $1 })),
            Set([0, 1, 4, 5])
        )

    }

    func testUniqueness() {
        typealias Element = (AnyClass?, String)

        let input: [Element] = [(nil, "First"), (nil, "First"), (nil, "Second")]
        let expectedOutput: [Element] = [(nil, "First"), (nil, "Second")]

        let output = input.unique { $0.1 }

        XCTAssertEqual(output.count, expectedOutput.count)
        XCTAssertEqual(output[0].1, expectedOutput[0].1)
        XCTAssertEqual(output[1].1, expectedOutput[1].1)
    }

}
