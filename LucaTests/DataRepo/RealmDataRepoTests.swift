import XCTest
import RealmSwift
import RxSwift
import RxBlocking
import RxTest
@testable import Luca

class RealmDataRepoTests: BaseDataRepoTests {

    // Both repos work on the same file, so accessing the same file should always produce errors as the encryption settings are different
    var realmRepo: SomeDatasetRepo!
    var encryptedRepo: SomeDatasetEncryptedRepo!

    override func setUp() async throws {
        realmRepo = SomeDatasetRepo()
        repo = realmRepo
        encryptedRepo = SomeDatasetEncryptedRepo()
        try await realmRepo.removeFile().task()

        continueAfterFailure = false
    }

    func test_realmDatabaseFileOnDisk_shouldBeExcludedFromCloudBackups() throws {
        try writeAndReadSingleDataSet_loadedDataSetEqualsOrigin(with: realmRepo)

        let hashedName = "TestRepo".data(using: .utf8)!.crc32().toHexString()
        let defaultRealmDirectory = try XCTUnwrap(Realm.Configuration.defaultConfiguration.fileURL?.deletingLastPathComponent())
        let fileURL = defaultRealmDirectory.appendingPathComponent("\(hashedName).realm")
        let resourceValues = try fileURL.resourceValues(forKeys: [URLResourceKey.isExcludedFromBackupKey])
        let isDatabaseFileExcludedFromCloudBackups = try XCTUnwrap(resourceValues.isExcludedFromBackup)
        XCTAssertTrue(isDatabaseFileExcludedFromCloudBackups)
    }

    func test_writeAndReadEncryptedSingleDataSet_loadedDataSetEqualsOrigin() throws {
        for _ in 0..<25 {
            try writeAndReadSingleDataSet_loadedDataSetEqualsOrigin(with: encryptedRepo)
        }
    }

    func test_writeAndReadEncryptedMultipleDataSet_loadedDataSetEqualsOrigin() throws {
        for _ in 0..<25 {
            try writeAndReadMultipleDataSet_loadedDataSetEqualsOrigin(with: encryptedRepo)
        }
    }

    func test_encrypted_writeAndRemoveAllData_restoresEmptyArray() throws {
        for _ in 0..<25 {
            try writeAndRemoveAllData_restoresEmptyArray(with: encryptedRepo)
        }
    }

    func test_encrypted_writeMultipleDataAndRemoveOneObject_restoresSingleItem() throws {
        for _ in 0..<25 {
            try writeMultipleDataAndRemoveOneObject_restoresSingleItem(with: encryptedRepo)
        }
    }

    func test_writeNonEncryptedAndLoadWithKey_fails() throws {
        for _ in 0..<25 {
            let stream = try repo.store(object: singleInputData)
                .flatMap { _ in self.encryptedRepo.restore() }
                .materializeAndBlock()

            XCTAssertEqual(stream.count, 1)
            XCTAssert(stream.first?.error != nil)
        }
    }

    func test_writeEncryptedAndLoadWithNoKey_fails() throws {
        let stream = try encryptedRepo.store(object: singleInputData)
            .flatMap { _ in self.repo.restore() }
            .materializeAndBlock()

        XCTAssertEqual(stream.count, 1)
        XCTAssert(stream.first?.error != nil)
    }

    func test_changeNonEncryptedToEncrypted_readEncrypted() throws {
        let stream = try repo.store(object: singleInputData)
            .flatMapCompletable { _ in self.realmRepo.changeEncryptionSettings(oldKey: nil, newKey: encryptionKey) }
            .andThen(encryptedRepo.restore())
            .materializeAndBlock()

        XCTAssertEqual(stream, [.next([singleInputData]), .completed])
    }

    func test_changeNonEncryptedToEncrypted_failAtReadNonEncrypted() throws {
        let stream = try repo.store(object: singleInputData)
            .flatMapCompletable { _ in self.realmRepo.changeEncryptionSettings(oldKey: nil, newKey: encryptionKey) }
            .andThen(repo.restore())
            .materializeAndBlock()

        XCTAssertEqual(stream.count, 1)
        XCTAssert(stream.first?.error != nil)
    }

    func test_changeEncryptedToNonEncrypted_read() throws {
        let stream = try encryptedRepo.store(object: singleInputData)
            .flatMapCompletable { _ in self.realmRepo.changeEncryptionSettings(oldKey: encryptionKey, newKey: nil) }
            .andThen(repo.restore())
            .materializeAndBlock()

        XCTAssertEqual(stream, [.next([singleInputData]), .completed])
    }

    func test_changeEncryptedToNonEncrypted_failAtReadEncrypted() throws {
        let stream = try encryptedRepo.store(object: singleInputData)
            .flatMapCompletable { _ in self.realmRepo.changeEncryptionSettings(oldKey: encryptionKey, newKey: nil) }
            .andThen(encryptedRepo.restore())
            .materializeAndBlock()

        XCTAssertEqual(stream.count, 1)
        XCTAssert(stream.first?.error != nil)
    }

}

class SomeDatasetRealmModel: RealmSaveModel<SomeDataset> {

    @objc dynamic var someString = ""
    @objc dynamic var someInt = 0
    var someArray = List<Int>()

    @objc dynamic var someOptional: String?

    override func create() -> SomeDataset {
        return SomeDataset(identifier: nil, someString: "", someInt: 0, someArray: [], someOptional: nil)
    }

    override func populate(from: SomeDataset) {
        super.populate(from: from)
        someString = from.someString
        someInt = from.someInt
        someArray.removeAll()
        someArray.append(objectsIn: from.someArray)
        someOptional = from.someOptional
    }

    override var model: SomeDataset {
        var m = super.model
        m.someArray = Array(someArray)
        m.someString = someString
        m.someInt = someInt
        m.someOptional = someOptional
        return m
    }
}

class SomeDatasetRepo: RealmDataRepo<SomeDatasetRealmModel, SomeDataset> {

    override func createSaveModel() -> SomeDatasetRealmModel {
        return SomeDatasetRealmModel()
    }

    init() {
        super.init(filenameSalt: "TestRepo", schemaVersion: 0)
    }
}

let encryptionKey = Data(hex: "a0f9ca456622c28dba9827d75f78de541dd05e4a807770bbe2cf8dedd307d45d3b754e78ebc5345efacfaa6ed815c4930bf4f8a9ed8069ff88956b6696e4767a")
class SomeDatasetEncryptedRepo: RealmDataRepo<SomeDatasetRealmModel, SomeDataset> {

    override func createSaveModel() -> SomeDatasetRealmModel {
        return SomeDatasetRealmModel()
    }

    init() {
        super.init(filenameSalt: "TestRepo", schemaVersion: 0, encryptionKey: encryptionKey)
    }
}
