import XCTest
import RealmSwift
import RxSwift
import RxBlocking
import RxTest
@testable import Luca

class MemoryDataRepoTests: BaseDataRepoTests {

    // Both repos work on the same file, so accessing the same file should always produce errors as the encryption settings are different
    var memoryRepo: SomeDatasetMemoryRepo!

    override func setUpWithError() throws {
        memoryRepo = SomeDatasetMemoryRepo()
        repo = memoryRepo

        continueAfterFailure = false
    }

    func test_serializeDeserialize() throws {
        let stream = try memoryRepo.store(object: singleInputData)
            .map { $0 == self.singleInputData }
            .filter { $0 }
            .asObservable()
            .map { _ in try self.memoryRepo.serialize() }
            .map { data -> SomeDatasetMemoryRepo in
                let repo = SomeDatasetMemoryRepo()
                try repo.deserialize(data: data)
                return repo
            }
            .flatMap { $0.restore() }
            .materializeAndBlock()

        XCTAssertEqual(stream, [.next([singleInputData]), .completed])
    }

    func test_generatingNewId() throws {
        var data = SomeDataset(identifier: nil, someString: "asdsdf", someInt: 1231, someArray: [3245, 3], someOptional: "Optional string")

        let stream: [Event<[SomeDataset]>] = try repo.store(object: data)
            .asObservable()
            .flatMap { _ in self.repo.restore() }
            .materializeAndBlock()
        let savedObject = stream.compactMap { $0.element }.flatMap { $0 }.first
        XCTAssertNotNil(savedObject)
        XCTAssertNotNil(savedObject?.identifier)
        data.identifier = savedObject?.identifier
        XCTAssertEqual(data, savedObject)
    }
}

class SomeDatasetMemoryRepo: MemoryDataRepo<SomeDataset> {
}
