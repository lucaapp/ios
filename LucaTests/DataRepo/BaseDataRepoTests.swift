import XCTest
import RealmSwift
import RxSwift
import RxBlocking
import RxTest
@testable import Luca

class BaseDataRepoTests: XCTestCase {

    var repo: DataRepo<SomeDataset>!
    let singleInputData = SomeDataset(identifier: 0, someString: "some string", someInt: 1234, someArray: [2345, 456, 235], someOptional: "ih9")
    let multipleInputData = [
        SomeDataset(identifier: 0, someString: "some string", someInt: 1234, someArray: [2345, 456, 235], someOptional: "ih9"),
        SomeDataset(identifier: 1, someString: "some string 123 ", someInt: 213, someArray: [2345, 235], someOptional: nil)
    ]

    func test_writeAndReadSingleDataSet_loadedDataSetEqualsOrigin() throws {
        try XCTSkipIf(repo == nil, "Repo not initialised")
        try writeAndReadSingleDataSet_loadedDataSetEqualsOrigin(with: repo)
    }

    func test_writeAndReadMultipleDataSet_loadedDataSetEqualsOrigin() throws {
        try XCTSkipIf(repo == nil, "Repo not initialised")
        try writeAndReadMultipleDataSet_loadedDataSetEqualsOrigin(with: repo)
    }

    func test_writeAndRemoveAllData_restoresEmptyArray() throws {
        try XCTSkipIf(repo == nil, "Repo not initialised")
        try writeAndRemoveAllData_restoresEmptyArray(with: repo)
    }

    func test_writeMultipleDataAndRemoveOneObject_restoresSingleItem() throws {
        try XCTSkipIf(repo == nil, "Repo not initialised")
        try writeMultipleDataAndRemoveOneObject_restoresSingleItem(with: repo)
    }

    func writeAndReadSingleDataSet_loadedDataSetEqualsOrigin(with selectedRepo: DataRepo<SomeDataset>) throws {
        let stream = try selectedRepo.store(object: singleInputData)
            .map { $0 == self.singleInputData }
            .filter { $0 }
            .asObservable()
            .flatMap { _ in selectedRepo.restore() }
            .materializeAndBlock()

        XCTAssertEqual(stream, [.next([singleInputData]), .completed])
    }

    func writeAndReadMultipleDataSet_loadedDataSetEqualsOrigin(with selectedRepo: DataRepo<SomeDataset>) throws {
        let stream = try selectedRepo.store(objects: multipleInputData)
            .map { $0 == self.multipleInputData }
            .filter { $0 }
            .asObservable()
            .flatMap { _ in selectedRepo.restore() }
            .map { Set($0) }
            .materializeAndBlock()

        XCTAssertEqual(stream, [.next(Set(multipleInputData)), .completed])
    }

    func writeAndRemoveAllData_restoresEmptyArray(with selectedRepo: DataRepo<SomeDataset>) throws {
        let stream = try selectedRepo.store(objects: multipleInputData)
            .flatMapCompletable { _ in selectedRepo.removeAll() }
            .andThen(selectedRepo.restore())
            .materializeAndBlock()

        XCTAssertEqual(stream, [.next([]), .completed])
    }

    func writeMultipleDataAndRemoveOneObject_restoresSingleItem(with selectedRepo: DataRepo<SomeDataset>) throws {
        let stream = try selectedRepo.store(objects: multipleInputData)
            .flatMapCompletable { _ in selectedRepo.remove(identifiers: [0]) }
            .andThen(selectedRepo.restore())
            .materializeAndBlock()

        XCTAssertEqual(stream, [.next([multipleInputData[1]]), .completed])
    }

}

struct SomeDataset: DataRepoModel, Equatable, Codable, Hashable {

    var identifier: Int?

    var someString: String
    var someInt: Int
    var someArray: [Int]
    var someOptional: String?

}
