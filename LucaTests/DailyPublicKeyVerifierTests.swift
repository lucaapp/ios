import Quick
import Nimble
import Cuckoo
import Foundation
import RxNimble
import RxSwift
import DependencyInjection

@testable import Luca

class DailyPublicKeyVerifierTests: QuickSpec {

    // swiftlint:disable:next function_body_length
    override func spec() {
        describe("Daily Public Key") {

            var dailyVerifier: DailyPublicKeyVerifier! = nil
            let timeProvider = MockedTimeProvider()
            let timeSync = MockTimeSyncService()
            var dailyKeyRepo: DailyPubKeyHistoryRepository!
            var keyValueRepo: KeyValueRepoWithUnderlying!
            var backend: MockBackendDailyKeyV4!

            let enableDailyKeyVerificationDate = Calendar.current.date(from: DateComponents(year: 2022, month: 9))!

            beforeEach {
                do {
                    try KeyStorage.purge()
                } catch let error {
                    fail("KeyStorage failed to purge: \(error)")
                }

                let requestDaily = MockAsyncDataOperation<BackendError<CurrentDailyKeyError>, CurrentDailyKey>()
                let requestIssuers = MockAsyncDataOperation<BackendError<FetchIssuerKeysError>, [IssuerKeys]>()
                backend = MockBackendDailyKeyV4()
                keyValueRepo = KeyValueRepoWithUnderlying(underlying: MemoryDataRepo<KeyValueRepoEntry>())
                dailyKeyRepo = DailyPubKeyHistoryRepository()
                dailyKeyRepo.removeAll()

                DependencyContext[\.timeProvider] = SharedProvider(value: timeProvider)
                DependencyContext[\.timeSync] = SharedProvider(value: timeSync)
                DependencyContext[\.dailyKeyRepository] = SharedProvider(value: dailyKeyRepo)
                DependencyContext[\.keyValueRepo] = SharedProvider(value: keyValueRepo)
                DependencyContext[\.backendDailyKeyV4] = SharedProvider(value: backend)

                stub(timeSync) { m in
                    when(m).isInSync().thenReturn(.just(true))
                }
                stub(requestDaily) { m in
                    when(m.execute(completion: any(), failure: any())).then {
                        // swiftlint:disable:next force_try
                        let retVal = try! JSONDecoder().decode(CurrentDailyKey.self, from: self.currentDailyPayload.data(using: .utf8)!)
                        $0.0(retVal)
                        return {}
                    }
                }
                stub(requestIssuers) { m in
                    when(m.execute(completion: any(), failure: any())).then {
                        // swiftlint:disable:next force_try
                        let retVal = try! JSONDecoder().decode([IssuerKeys].self, from: self.issuersPayload.data(using: .utf8)!)
                        $0.0(retVal)
                        return {}
                    }
                }
                stub(backend) { m in
                    when(m.fetchAllIssuerKeys()).thenReturn(requestIssuers)
                    when(m.fetchDailyPubKey()).thenReturn(requestDaily)
                }
                timeProvider.valueToEmit = Calendar.current.date(from: DateComponents(year: 2021, month: 12, day: 6, hour: 4))!

                let root = Single.from { Bundle.main.url(forResource: "dev_ca_root", withExtension: "pem") }
                    .unwrapOptional()
                    .map { try String(contentsOf: $0) }

                let intermediate = Single.from { Bundle.main.url(forResource: "dev_ca_intermediate", withExtension: "pem") }
                    .unwrapOptional()
                    .map { try String(contentsOf: $0) }
                dailyVerifier = DailyPublicKeyVerifier(
                    rootCertificateSource: root,
                    intermediateCertificatesSource: intermediate.map { [$0] }
                )
            }
            it("Happy case") {
                expect(dailyVerifier.isKeyValid.asObservable().take(1)).array.to(equal([false]))
                // TODO: Postpone the daily key verification until the backend supports it again
                if Date() > enableDailyKeyVerificationDate {
                    expect(dailyVerifier.fetchAndVerifyIfNeeded()).array.to(equal([Never]()))
                    expect(dailyVerifier.isKeyValid.asObservable().take(1)).array.to(equal([true]))
                }
            }
            context("Negative case") {
                it("Time is not in sync") {
                    stub(timeSync) { m in
                        when(m.isInSync()).thenReturn(.just(false))
                    }
                    expect(dailyVerifier.fetchAndVerifyIfNeeded().asObservable().materialize().asSingle())
                        .array
                        .to(equal([Event<Never>.error(DailyKeyVerifierError.systemClockAltered)]))
                }
                it("Daily is not valid yet - date < iat") {
                    timeProvider.valueToEmit = Calendar.current.date(from: DateComponents(year: 2021, month: 11, day: 28, hour: 4))!
                    expect(dailyVerifier.fetchAndVerifyIfNeeded().asObservable().materialize().asSingle())
                        .array
                        .to(equal([Event<Never>.error(DailyKeyVerifierError.dailyKeyNotValid)]))
                }
                it("Daily is not valid anymore - date > iat + 7 days") {
                    timeProvider.valueToEmit = Calendar.current.date(byAdding: .day, value: 8, to: timeProvider.valueToEmit)!
                    expect(dailyVerifier.fetchAndVerifyIfNeeded().asObservable().materialize().asSingle())
                        .array
                        .to(equal([Event<Never>.error(DailyKeyVerifierError.dailyKeyNotValid)]))
                }
                it("Certificate chain is not trusted") {

                    let root = Single.from { Bundle.main.url(forResource: "dev_ca_root", withExtension: "pem") }
                        .unwrapOptional()
                        .map { try String(contentsOf: $0) }
                    dailyVerifier = DailyPublicKeyVerifier(
                        rootCertificateSource: root,
                        intermediateCertificatesSource: .just([])
                    )
                    expect(dailyVerifier.fetchAndVerifyIfNeeded().asObservable().materialize().asSingle())
                        .array
                        .to(equal([Event<Never>.error(DailyKeyVerifierError.certificateChainNotTrusted)]))
                }
                it("Key validated and saved successfully - loaded after 8 days should be invalid") {
                    // TODO: Postpone the daily key verification until the backend supports it again
                    if Date() >= enableDailyKeyVerificationDate {
                        expect(dailyVerifier.fetchAndVerifyIfNeeded()).array.to(equal([Never]()))
                        timeProvider.valueToEmit = Calendar.current.date(byAdding: .day, value: 8, to: timeProvider.valueToEmit)!
                        expect(dailyVerifier.isKeyValid.asObservable().take(1)).array.to(equal([false]))
                    }
                }
            }
        }
    }
    // swiftlint:disable line_length
    let issuersPayload = """
[{"issuerId":"f929a574-c358-43d7-b5b1-7b06c19f4ef7","publicCertificate":null,"signedPublicHDEKP":null,"signedPublicHDSKP":null},{"issuerId":"ab54ae9c-88b7-43f2-97a0-0e01cad19b22","publicCertificate":null,"signedPublicHDEKP":null,"signedPublicHDSKP":null},{"issuerId":"36c44d6a-a8c5-4df1-8cc9-cd6bb99f1620","publicCertificate":null,"signedPublicHDEKP":null,"signedPublicHDSKP":null},{"issuerId":"4875032b-4439-4c99-a179-e77f5065bab7","publicCertificate":null,"signedPublicHDEKP":null,"signedPublicHDSKP":null},{"issuerId":"d229e28b-f881-4945-b0d8-09a413b04e00","publicCertificate":"-----BEGIN CERTIFICATE-----\nMIIF8jCCA9qgAwIBAgIUNraRTy+ykuT/pXzk+DfiBqHaPsEwDQYJKoZIhvcNAQEN\nBQAwbTELMAkGA1UEBhMCREUxDzANBgNVBAgTBkJlcmxpbjEPMA0GA1UEBxMGQmVy\nbGluMREwDwYDVQQKEwhsdWNhIERldjEpMCcGA1UEAxMgbHVjYSBEZXYgQ2x1c3Rl\nciBJbnRlcm1lZGlhdGUgQ0EwHhcNMjEwNzA5MTgxODAwWhcNMjIwNzA5MTgxODAw\nWjCBgTELMAkGA1UEBhMCREUxDzANBgNVBAgTBkJlcmxpbjEPMA0GA1UEBxMGQmVy\nbGluMREwDwYDVQQKEwhsdWNhIERldjEmMCQGA1UEAxMdRGV2IENsdXN0ZXIgSGVh\nbHRoIERlcGFydG1lbnQxFTATBgNVBAUTDENTTTAyNjA3MDkzOTCCAiIwDQYJKoZI\nhvcNAQEBBQADggIPADCCAgoCggIBAKow1660WFqNEgMpFaRqXOLgw8bIx4h8Zttk\nhWafkOCbNLW93Dlu7L+yvPzmWTXJ97pjIA4zABljJ17yh/K+7R2QjMWIFirHXbli\nOyn+maymTMrYAgb73QUCfzSBoTW9wGglmJMvpYW/uFNB+yFM/BemdR5CKtoKFtjY\nScIBbTfqrtZp8x815X6J0Ts5Iy0ltQKRQLrmq3CvDVCZnhzyC6LYyfAPTrSYunac\nYOWpyg0q9OXYqCskEGnuQN7ypMAbw9ku6hhdNmfKci+pO47Yy2IUcSa7ViAe9psU\nmEK8slkAtaKo0PoAZhCM4Rso2Ml6ah4xyyvloyFgzpyuZjWLyQK5So0Dv4uBUhXn\nY7ha5a2Ypxv7Qnv0AV8mUVfSRDM7FGRiO09v/S+8SJ+iszFQz3VxT6Nhp3cBhgz4\nplSokoLW+03efIiJOm5mQUx/5h1CQdAynbMJFiHa2DLRyOj2RDN9m8Rwo5nOWsVU\nF7M9N7zPwtHyRnTxa9FLb2xUytzEykibarTzcI7QqjJdALuxIvKeHnWT70LC8TCX\nMfIFh7Z6ZojXQTvfrJKeCtpRv8rBKmU4/GSIzDOH7vq5CLHnppj2ZXuypECYoWX7\nqoyvy8lxk0bYAGk/hndo9FzPLKWvnCmxovg3sMCtfG7Pt/006mZFFzhDoDULzKMd\nzOtChvEhAgMBAAGjdTBzMA4GA1UdDwEB/wQEAwIEsDATBgNVHSUEDDAKBggrBgEF\nBQcDAjAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBQ2RdKX3FHzxyFgUsKV/8jnsB2o\n9jAfBgNVHSMEGDAWgBRMdbAGNCq//hXC9wYRlmcqAit07zANBgkqhkiG9w0BAQ0F\nAAOCAgEASlhUUeuZQAXabDqihPYeIAu5Ok3VhVtI2uEz1vlq20p7Ri3KQHUDFPu3\nwSELUr5rjmUhwDdB8Xsx9D+T+WzGKznIbx3m805Mp3ExDZ7qqyRbmWTE/6mi6R5A\nrGOzVtxkpbk0uukASRUf/PIDFasKo2XKkqJkNW905fSAncrRvQQBeIJQvK0HF2Pj\nv3n7Zxl2y+vT8oqSsoTfB+9IWJMtecHMjqe8qj3GB/uPyNYcuHi0/o3QW2wQB1Xn\nEeffrAjGk669gGUKuB2zcAcfBsQcfPQcRZEe7L+ExFHUklUujOeiMRFqm4qTlDyc\nabg1OiOaX48twR4CtXwuM40pQBOkj9e0NbhWmEWzP96rMtSRNlU/K4B2lbbJ3zWp\natBdAmv97xQd/3XC1SafxbtWXZo2s4AX7SzoQ4yIiae2RP1nC8/GxEApM6KXA5SD\nyxvtINpKU7cLAzP4cDMXc8/vDD7JOIzEwxRASo4pdQIaZBT+jRQ6BRRLxpYJyx2i\ng3vSCwENPv/Rpj4kobc46GsD/azmJ2ezMPVEEpJ63xFhEEHSNysGbq5JfrLHrQdB\n+bpxOFtliMb+QiLfiW4Lr+giq4OenJUb2TIPLjVnoJUjQLqQkrKIYccr0mXWzpUq\ntMk6sJ4QPw5+WeR/tceU56ekQzN/5ROeTTMtzAU8LENp+mpI42A=\n-----END CERTIFICATE-----\n","signedPublicHDEKP":"eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJkMjI5ZTI4Yi1mODgxLTQ5NDUtYjBkOC0wOWE0MTNiMDRlMDAiLCJpc3MiOiI2NzY2ZWE3ZTQzMjI2MjI4ZDVhOGVjYjA5NWI2ZTQzZjI2NWE4MjZkIiwibmFtZSI6Ikdlc3VuZGhlaXRzYW10IERldiIsImtleSI6IkJBV0pyanZzbytJMW1yT0hybGFHanhGRFRZK2JveWRHMmw3RGRmS3hxYkJBenhRSzJRVjlzZEFCc0F0aDNFVWUya2lUUXhWMDlhWnpsd0xaY25oa1NhTT0iLCJ0eXBlIjoicHVibGljSERFS1AiLCJpYXQiOjE2Mzg0NTEyMDJ9.bPZZVkMzAJeJk1ZRNZm9zmsjHWsL3f4Urlb-JKljSh3EMgZZwIQSEVRD7Yi_xJ5w5yjXtoh3h-Tl3WCEKJujlvgaW4s0q049N56q8N5URHB4tCDRPS3LOMti1fotCsBlvVb7WhuiExwZaDlB6Mn4EruXw_Vfgn87twSrCg7s6QpWju3sH-L-cx_UyG7uwLYWVeiMz4gk17jiU84De1NXevkYgqeHNRGD3_W0vKU6aMv6do9V6zyEQZlbSNbFzWIyZZ0dxFI_-ZzNibDHR0KAUXPNY_OOarqNA0332FvFncW9Brbs9kN-QiRhXzmITys9T3iyJHNGuzYzK9_ujz77fAZKFmCRQaw8HnqewrdB5WtUAJS4w1QJqJEn5HRBECLydUsTYK8KtuC_sSrdNFI5tf47TAiwismwrd2SGiT-wNjRz36HMX9MVlfVgOzOl9SbBHvv5eYjdtI7hhtEIy3wUNS2wt8GZ_B8rOCafmhgRJtfp1D8xv6nM7YvooftlO_iglOvLhP1YRVDuwzh-6ACSDvn457cxzsXDcyrYG-R96Qe2NQVXhOSvwkSUG2IN4Hm0FL0tLMxJlEegqk94MFnVAegKprpDha4OMOYClunDqxSkAXnts7AYk6DCwT-4mOJb1ede0Dyr9iSGhieH8BulfWiOIKJe_fze9rEVFPVmvU","signedPublicHDSKP":"eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJkMjI5ZTI4Yi1mODgxLTQ5NDUtYjBkOC0wOWE0MTNiMDRlMDAiLCJpc3MiOiI2NzY2ZWE3ZTQzMjI2MjI4ZDVhOGVjYjA5NWI2ZTQzZjI2NWE4MjZkIiwibmFtZSI6Ikdlc3VuZGhlaXRzYW10IERldiIsImtleSI6IkJNV21NUkxTaWRYTHJVd0ZRcjlWd1lCKzN6ckFtblV4T0xUWDl5OWxOR21HTzMxU3JCWEFJOXdOZklhNzVicTlWNVRJa1VVd21xOE1ONG9HRDBveUduST0iLCJ0eXBlIjoicHVibGljSERTS1AiLCJpYXQiOjE2Mzg0NTEyMDF9.RtKV6mmN7Y3aulRW8zPINvA18sw0OiC4z_rJjffq2cNF7e23br-9rT_tI3iMj6QkjST_8RwHeNI8rTMNY8i5EdTOnVbf6r1vLVJejn46TmUJ8uIoCthxJGXKkkBf8DeN2GR4pQiTvBujz_Ba21Xc0P4DXVe6W4NUXyJpEc8i7w20VlPUSgTd8JKysGxF19TmzKYGzT9PC_BUGXlbm48G7bUHcU4_fta1x5zZnPat6oLqPnot1tcp_3IvUYnep50WtkacPmOG3RLMdR-4nV2a02hK5PPHo-2-PGk2EODJVCWpQ81gT25zWyhorMc4OE9COD8wOWhNLwtmxX8lH_c6iVd1ljvy8d5KArbHCaVBZh-iVosgAg7wbhVzQBfIDvOOJwCIOjAU5ZIB3Uxt8l1VN5UvzCY_cJi30HJpoy7HebP3ypiduslqEDWmVK-KRzRvfXyZN56mJ4R4TfopUdP1qlr9dDcGARF0FAc6mR4NgLiBc9vzi1USjX2ThfK5j-csXUi_oBMMkIHlReHHff610qwhXvKo5Rct7xsPpbEKhxSSo684qt09WhR4Yt4m0SEhQn1fFg5bsnNawqlyyzVxCROBO6NYb-vKwi7lhBR-R5fXYYCjPy_q2noOBBOC_Hvo-g8EoLr52qsQ6qZuTrVNmHOzi_gRbk96DlA4wt5ldkk"}]
""".replacingOccurrences(of: "\n", with: "\\n")

    let currentDailyPayload = """
{"signedPublicDailyKey":"eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoicHVibGljRGFpbHlLZXkiLCJpc3MiOiJkMjI5ZTI4Yi1mODgxLTQ5NDUtYjBkOC0wOWE0MTNiMDRlMDAiLCJrZXlJZCI6MjMsImtleSI6IkJQZEEvSmVYZVpTaUtXVzAxcFFJK0hBcUdSbVdjdmVNc0ZuUmVidHBRSUhVT2ZNalZKMWtXcmZUc2RCYkFUNm9HbDBuYytBZTZUWDJGdmZNOTdwN3gyND0iLCJpYXQiOjE2MzgyMDAzMDh9.fSTh8YvwTvUWQlS56BlPT4uwFT4c-e3-qhAPyJsBt4wSc_pzamSrzDKAl0M_EOBYSUsbFEVeQACDaesyA_hXkg"}
"""
}
